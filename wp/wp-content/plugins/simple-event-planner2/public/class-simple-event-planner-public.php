<?php
/**
 * Simple_Event_Planner_Public Class
 * 
 * The public-facing functionality of the plugin. Defines the plugin name, version,
 * and two examples hooks for how to enqueue the admin-specific stylesheet and JavaScript.
 *
 * @link       https://wordpress.org/plugins/simple-event-planner/
 * @since      1.0.0
 *
 * @package    Simple_Event_Planner
 * @subpackage Simple_Event_Planner/public
 * @author     PressTigers <support@presstigers.com>
 */

class Simple_Event_Planner_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $simple_event_planner    The ID of this plugin.
     */
    private $simple_event_planner;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since   1.0.0
     * @param   string  $simple_event_planner   The name of the plugin.
     * @param   string  $version                The version of this plugin.
     */
    public function __construct( $simple_event_planner, $version ) {
        $this->simple_event_planner = $simple_event_planner;
        $this->version = $version;        
        
        // Hook -> Includes template function
        add_action('after_setup_theme', array($this, 'include_template_functions'), 11);
        
        /** 
         * This file is responsible for the calender and event listing typography/css.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-simple-event-planner-typography.php';
       }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Simple_Event_Planner_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Simple_Event_Planner_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->simple_event_planner . '-front-end', plugin_dir_url(__FILE__) . 'css/simple-event-planner-public.css', array(), $this->version, 'all', TRUE);
        wp_enqueue_style( $this->simple_event_planner."-fontawsome", '//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', array( ), '4.6.3', 'all' );
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Simple_Event_Planner_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Simple_Event_Planner_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        
        // Get Settings Map APIKey 
        $sep_event_options = get_option('sep_event_options');
        $api_key = ( !empty($sep_event_options['sep_event_gmap_key'])) ? 'key='. trim($sep_event_options['sep_event_gmap_key']) . '&' :'';
            
        wp_register_script($this->simple_event_planner . '-google-api', 'http://maps.googleapis.com/maps/api/js?'. $api_key .'v=3.24&libraries=places', '', '', TRUE);
        wp_register_script($this->simple_event_planner . '-frontend', plugin_dir_url(__FILE__) . 'js/simple-event-planner-public.js', array('jquery'), $this->version, TRUE);
        wp_register_script($this->simple_event_planner . '-jquery-map', plugin_dir_url(__FILE__) . 'js/jquery-map.js', '', $this->version);
        wp_register_script($this->simple_event_planner . '-jquery-geocomplete', plugin_dir_url(__FILE__) . 'js/jquery.geocomplete.min.js', '', $this->version, TRUE);
        wp_register_script($this->simple_event_planner . '-jquery-event-calendar-min', plugin_dir_url(__FILE__) . 'js/jquery.eventCalendar.min.js', '', $this->version, TRUE);
        wp_register_script($this->simple_event_planner . '-jquery-moment', plugin_dir_url(__FILE__) . 'js/moment.min.js', '', $this->version, TRUE);
        wp_register_script($this->simple_event_planner . '-jquery-plugin', plugin_dir_url(__FILE__) . 'js/jquery.plugin.min.js', '', $this->version, TRUE);
        wp_register_script($this->simple_event_planner . '-jquery-countdown', plugin_dir_url(__FILE__) . 'js/jquery.countdown.min.js', '', $this->version, TRUE);
        
    }

    /**
     * Load Evnet Planner Templates Functions
     * 
     * @since 1.1.0
     */
    public function include_template_functions() {
        include( 'partials/simple-event-planner-template-functions.php' );
    }

}