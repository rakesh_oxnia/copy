/* 
 *  SEP JS File For Front-end
 *  v1.1.0
 */

(function ($) {
    'use strict';

    if ($('#countdown-underconstruction').length) {

        // Event Counter Localize Data
        var year = event.year;
        var month = event.month;
        var day = event.day;
        var hours = event.hours;
        var minutes = event.minutes;
    }

    $(window).load(function () {
        var item_id = $('.submit-location').attr('id');

        //Search Location Address Autocomplete
        var location_address = $('#loc-addres' + item_id);

        if (location_address.length) {
            location_address.geocomplete();
        }

        // On Hover Read Address
        $('.submit-location').live('hover', function () {
            var item_val = location_address.val();
            location_address.val(item_val);
        });

        // Event Counter 
        var countdown_underconstruction = $('#countdown-underconstruction');
        if (countdown_underconstruction.length) {

            var austDay = new Date();
            austDay = new Date(year, month - 1, day, hours, minutes);
            countdown_underconstruction.countdown({
                until: austDay,
                format: 'wdhms',
                layout: '<div class="main-digit-wrapp"><span class="digit-wrapp"><span class="cs-digit">{w10}</span><span class="cs-digit">{w1}</span></span><span class="countdown-period">W</span></div>' +
                        '<div class="main-digit-wrapp"><span class="digit-wrapp"><span class="cs-digit">{d10}</span><span class="cs-digit">{d1}</span></span><span class="countdown-period">D</span></div>' +
                        '<div class="main-digit-wrapp"><span class="digit-wrapp"><span class="cs-digit">{h10}</span><span class="cs-digit">{h1}</span></span><span class="countdown-period">H</span></div>' +
                        '<div class="main-digit-wrapp"><span class="digit-wrapp"><span class="cs-digit">{m10}</span><span class="cs-digit">{m1}</span></span><span class="countdown-period">M</span></div>' +
                        '<div class="main-digit-wrapp"><span class="digit-wrapp"><span class="cs-digit">{s10}</span><span class="cs-digit">{s1}</span></span><span class="countdown-period">S</span></div>'
            });
        }
    });

    // Search Events
    var show_ = false;
    window.search_events = function (admin_url, id) {
        if (show_) {
            return false;
        } else {
            show_ = true;
        }
        var loc_address = $('#loc-addres' + id).val();
        var event_category = $('#event-cat' + id).val();

        var dataString = 'loc_address=' + loc_address + '&event_category=' + event_category + '&action=search_events';
        $('#simple-event-calendar' + id).animate({opacity: '.4'}, 300);
        $.ajax({
            type: 'POST',
            url: admin_url,
            data: dataString,
            success: function (response) {
                $('#simple-event-calendar' + id).html(response);
                $('#simple-event-calendar' + id).animate({opacity: '1'}, 300);
                show_ = false;
            }
        });
    }

    // Expand Right Col. on Hidden Event Detail
    if (!($('.sep-single-event-details')).children().length) {
        $('.sep-single-event-details').parent().next().removeClass('sep-col-md-7 sep-col-sm-6');
        $('.sep-single-event-details').parent().next().addClass('sep-col-md-12 sep-col-sm-12');
        $('.sep-single-event-details').parent().remove();
        $('.sep-single-event-details').remove();

    }

})(jQuery);