<?php if (!defined('ABSPATH')) { exit; } // Exit if accessed directly
/**
 * Simple_Event_Planner_Meta_Box_Event_Options class
 *
 * This is used to define event options meta box.
 *
 * This meta box is designed to store:
 * 
 * - Event Options
 * - Location Options 
 *
 * @link        https://wordpress.org/plugins/simple-event-planner/
 * @since       1.1.0
 * 
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/admin/partials/meta-boxes
 * @author      PressTigers <support@presstigers.com>
 */

class Simple_Event_Planner_Meta_Box_Event_Options {    

    /**
     * Event Options Meta box.
     * 
     * @since   1.1.0
     */
    public static function sep_meta_box_output() {
        global $post, $xml_object;

        // Add a nonce field so we can check for it later.
        wp_nonce_field('sep_event_meta_box', 'sep_event_meta_box_nonce');

        // Use get_post_meta() to retrieve an existing value
        $seg_array = get_post_meta($post->ID, 'add_segment', TRUE);
        $seg_array = unserialize($seg_array);
        $event_xml = get_post_meta($post->ID, 'event_meta', TRUE);

        if ($event_xml <> '') {
            $xml_object = new SimpleXMLElement($event_xml);
            $start_date = isset($xml_object->start_date) ? $xml_object->start_date : '';
            $end_date = isset($xml_object->end_date) ? $xml_object->end_date : '';
            $start_time = isset($xml_object->start_time) ? $xml_object->start_time : '';
            $end_time = isset($xml_object->end_time) ? $xml_object->end_time : '';
            $event_organiser = isset($xml_object->event_organiser) ? $xml_object->event_organiser : '';
            $organiser_email = isset($xml_object->organiser_email) ? $xml_object->organiser_email : '';
            $organiser_contact = isset($xml_object->organiser_contact) ? $xml_object->organiser_contact : '';            
            $event_date = isset($xml_object->event_date) ? $xml_object->event_date : '';
            $event_org = isset($xml_object->event_org) ? $xml_object->event_org : '';
            $event_email = isset($xml_object->event_email) ? $xml_object->event_email : '';
            $org_contact = isset($xml_object->org_contact) ? $xml_object->org_contact : '';
            $event_time = isset($xml_object->event_time) ? $xml_object->event_time : '';
            $event_segment = isset($xml_object->event_segment) ? $xml_object->event_segment : '';
            $even_time_zone = isset($xml_object->sep_time_zone) ? $xml_object->sep_time_zone : '';
            $location_map = isset($xml_object->location_map) ? $xml_object->location_map : '';
        } else {
            $start_date = $end_date = $start_time = $end_time = $event_address = $event_organiser = $organiser_email = $event_type = $organiser_contact = $event_date = $event_time = $event_segment = $event_org = $event_email = $org_contact = $location_map = '';
        }
        ?>
        
        <!-- Event Options Meta Box -->
        <div class="page-elements c-post-<?php echo intval($post->ID); ?>">
            <div class="page-wrap">

                <!-- Event Planner Tabs-->
                <div class="vertical-tabs">
                    <div class="tab-options">
                        <ul>
                            <li class="active"><a id="tab-event-options"><i class="fa fa-hand-o-right"></i> <?php _e('Event Options', 'simple-event-planner') ?> </a></li>                           
                            <li><a id="location-tab" ><i class="fa fa-hand-o-right"></i> <?php _e('Venue Address & Map', 'simple-event-planner') ?> </a></li>                    
                        </ul>
                    </div>
                    <div class="detail-tab">

                        <!-- Events Options-->
                        <div id="tab-event-options" style="display:block;">
                            <header class="tab-head">
                                <h5><?php _e('Event Options', 'simple-event-planner') ?> </h5>
                            </header>
                            <ul class="form-elements">
                                <li class="field-label full-width">
                                    <input type="checkbox"   name="custom[event_date]" <?php echo $event_date; ?>  /> 
                                    <label><?php _e("Don't show event date", 'simple-event-planner') ?> </label>
                                <li class="field-label">
                                    <label><?php _e('Event Date', 'simple-event-planner') ?> </label>
                                </li>
                                <li class="element-field">
                                    <div class="input-sec">
                                        <input type="text" id="to-date" class="date-picker" name="custom[start_date]" value="<?php echo $start_date; ?>"/>
                                        <label><?php _e('Start Date', 'simple-event-planner') ?> </label>
                                    </div>
                                    <div class="input-sec">
                                        <input id="from-date" class="date-picker" type="text" name="custom[end_date]" value="<?php echo $end_date; ?>"/>
                                        <label> <?php _e('End Date', 'simple-event-planner') ?> </label>
                                    </div>
                                </li>
                            </ul>
                            <ul class="form-elements">
                                <li class="field-label full-width">
                                    <input type="checkbox"   name="custom[event_time]" <?php echo $event_time; ?>  /> 
                                    <label><?php _e("Don't show event time", 'simple-event-planner') ?> </label> 
                                </li>
                                <li class="field-label">
                                    <label><?php _e('Event Time', 'simple-event-planner') ?> </label>
                                </li>
                                <li class="element-field">
                                    <div class="input-sec">
                                        <input id="to-time" class="time-picker" type="text" name="custom[start_time]" value="<?php echo $start_time; ?>"/>
                                        <label><?php _e('Start Time', 'simple-event-planner') ?> </label>
                                    </div>
                                    <div class="input-sec">
                                        <input id="from-time" class="time-picker" type="text" name="custom[end_time]" value="<?php echo $end_time; ?>"/>
                                        <label><?php _e('End Time', 'simple-event-planner') ?></label>
                                    </div> 
                                    
                                    <!-- Time Zones -->
                                    <div class="input-sec">
                                        <select name="custom[sep_time_zone]">
                                            <option value=""<?php if ('' == isset($even_time_zone)) echo 'selected'; ?>><?php _e('Select Time Zone', 'simple-event-planner') ?></option>
                                            <option timeZoneId="1" gmtAdjustment="UTC-12:00"  value="-12"<?php if (isset($even_time_zone) AND "-12" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC-12:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="2" gmtAdjustment="UTC-11:00"  value="-11"<?php if (isset($even_time_zone) AND "-11" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC-11:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="3" gmtAdjustment="UTC-10:00"  value="-10"<?php if (isset($even_time_zone) AND "-10" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC-10:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="4" gmtAdjustment="UTC-09:00"  value="-9"<?php if (isset($even_time_zone) AND "-9" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC-09:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="5" gmtAdjustment="UTC-08:00"  value="-8"<?php if (isset($even_time_zone) AND "-8" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC-08:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="6" gmtAdjustment="UTC-07:00"  value="-7"<?php if (isset($even_time_zone) AND "-7" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC-07:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="7" gmtAdjustment="UTC-06:00"  value="-6"<?php if (isset($even_time_zone) AND "-6" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC-06:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="8" gmtAdjustment="UTC-05:00"  value="-5"<?php if (isset($even_time_zone) AND "-5" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC-05:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="9" gmtAdjustment="UTC-04:00"  value="-4"<?php if (isset($even_time_zone) AND "-4" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC-04:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="10" gmtAdjustment="UTC-03:00" value="-3"<?php if (isset($even_time_zone) AND "-3" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC-03:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="11" gmtAdjustment="UTC-02:00" value="-2"<?php if (isset($even_time_zone) AND "-2" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC-02:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="12" gmtAdjustment="UTC-01:00" value="-1"<?php if (isset($even_time_zone) AND "-1" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC-01:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="13" gmtAdjustment="UTC+00:00" value="0"<?php if (isset($even_time_zone) AND "0" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC-00:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="14" gmtAdjustment="UTC+01:00" value="+1"<?php if (isset($even_time_zone) AND "+1" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC+01:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="15" gmtAdjustment="UTC+02:00" value="+2"<?php if (isset($even_time_zone) AND "+2" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC+02:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="16" gmtAdjustment="UTC+03:00" value="+3"<?php if (isset($even_time_zone) AND "+3" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC+03:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="17" gmtAdjustment="UTC+04:00" value="+4"<?php if (isset($even_time_zone) AND "+4" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC+04:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="18" gmtAdjustment="UTC+05:00" value="+5"<?php if (isset($even_time_zone) AND "+5" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC+05:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="19" gmtAdjustment="UTC+06:00" value="+6"<?php if (isset($even_time_zone) AND "+6" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC+06:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="20" gmtAdjustment="UTC+07:00" value="+7"<?php if (isset($even_time_zone) AND "+7" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC+07:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="21" gmtAdjustment="UTC+08:00" value="+8"<?php if (isset($even_time_zone) AND "+8" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC+08:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="22" gmtAdjustment="UTC+09:00" value="+9"<?php if (isset($even_time_zone) AND "+9" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC+09:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="23" gmtAdjustment="UTC+10:00" value="+10"<?php if (isset($even_time_zone) AND "+10" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC+10:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="24" gmtAdjustment="UTC+11:00" value="+11"<?php if (isset($even_time_zone) AND "+11" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC+11:00)', 'simple-event-planner') ?></option>
                                            <option timeZoneId="25" gmtAdjustment="UTC+12:00" value="+12"<?php if (isset($even_time_zone) AND "+12" == $even_time_zone) echo 'selected'; ?>><?php _e('(UTC+12:00)', 'simple-event-planner') ?></option>
                                        </select>											
                                        <label><?php _e('Time Zone', 'simple-event-planner') ?></label>
                                    </div>       
                                </li>
                            </ul>
                            <ul class="form-elements">
                                <li class="field-label full-width">
                                    <input type="checkbox"   name="custom[event_org]"  <?php echo $event_org; ?>  />
                                    <label> <?php _e("Don't show organizer name", 'simple-event-planner') ?>  </label>
                                </li>
                                <li class="field-label">
                                    <label> <?php _e('Organizer', 'simple-event-planner') ?>  </label>
                                </li>
                                <li class="element-field">
                                    <input type="text" id="event-organiser" name="custom[event_organiser]" value="<?php echo $event_organiser; ?>" />
                                    <label><?php _e('Organizer of Event', 'simple-event-planner') ?></label>
                                </li>
                            </ul>
                            <ul class="form-elements">
                                <li class="field-label full-width">
                                    <input type="checkbox"   name="custom[event_email]"  <?php echo $event_email; ?>  />
                                    <label><?php _e("Don't show organizer email", 'simple-event-planner') ?>  </label>
                                </li>
                                <li class="field-label">
                                    <label><?php _e('Email', 'simple-event-planner') ?>  </label>
                                </li>
                                <li class="element-field">
                                    <input type="email" id="event-organiser-email" name="custom[organiser_email]" value="<?php echo $organiser_email; ?>" />
                                    <label><?php _e('Organizer Email', 'simple-event-planner') ?> </label>
                                    <span class="sep-invalid-email"> <?php _e('A valid email address is required.', 'simple-event-planner') ?> </span>
                                </li>
                            </ul>
                            <ul class="form-elements">
                                <li class="field-label full-width">
                                    <input type="checkbox"   name="custom[org_contact]" <?php echo $org_contact; ?>  />
                                    <label><?php _e("Don't show contact", 'simple-event-planner') ?>  </label>
                                </li>
                                <li class="field-label">
                                    <label> <?php _e('Phone Number', 'simple-event-planner') ?>  </label>
                                </li>
                                <li class="element-field">
                                    <input type="tel" id="event-organiser-contact" name="custom[organiser_contact]" pattern="^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$" title="A valid phone address is required" value="<?php echo $organiser_contact; ?>" />
                                    <label><?php _e('Phone Number', 'simple-event-planner') ?></label>
                                    <span class="sep-invalid-phone"> <?php _e('A valid phone address is required.', 'simple-event-planner') ?> </span>
                                </li>
                            </ul>

                            <!-- Event Segments !-->
                            <ul class="form-elements">
                                <li class="field-label full-width">
                                    <input type="checkbox"   name="custom[event_segment]" <?php echo $event_segment; ?>  />
                                    <label><?php _e("Don't show event segments", 'simple-event-planner') ?>  </label>
                                </li>
                                <li class="field-label">
                                    <label> <?php _e('Add Segments', 'simple-event-planner') ?></label>
                                </li>
                                <li class="element-field">
                                    <?php
                                    $segment_size = sizeof($seg_array);
                                    if (is_null($seg_array) && 0 == $segment_size) {
                                        ?>
                                        <div id="itemRows">
                                            <input type="text" name="custom[add_seg][]" value="" />
                                        </div>  
                                    <?php } if (1 == $segment_size) { ?>
                                        <div id="itemRows">
                                            <input type="text" name="custom[add_seg][]" value="<?php echo trim($seg_array[0]); ?>" />
                                        </div>  
                                        <?php
                                    } else {
                                        $row_count = 1;
                                        ?>                          
                                        <div id="itemRows"> 
                                            <?php foreach ($seg_array as $key => $value) { ?>
                                                <p id="rowNum<?php echo $row_count; ?>">
                                                    <input type="text" name="custom[add_seg][]"  value="<?php echo trim($value); ?>" />   
                                                    <input type="button"  id="<?php echo $row_count; ?>" value="<?php _e('Remove', 'simple-event-planner') ?>" onclick="removeRow('<?php echo $row_count; ?>');">
                                                </p>
                                                <?php
                                                $row_count++;
                                            }
                                            ?>
                                        </div>  
                                    <?php } ?>
                                    <input onclick="addRow('itemRows');" type="button" value="<?php _e('Add more', 'simple-event-planner') ?>" />
                                </li>
                            </ul>
                            <input  class="reset-button-primary"  type="button" id="reset" value="<?php _e('Reset Options', 'simple-event-planner') ?>"/>
                        </div> 

                        <!-- Location Map Options-->
                        <div id="location-tab"  style="display:none;">
                            <div class="page-event-registration">
                                <header class="tab-head">
                                    <h5> <?php _e('Venue Address & Map', 'simple-event-planner') ?> </h5>
                                </header> 
                                <?php
                                
                                // Enqueueing Scripts for Map 
                                wp_enqueue_script('simple-event-planner' . '-jquery-google-map-api');
                                global $post, $xml_object;
                                $loc_address = isset($xml_object->event_address) ? $xml_object->event_address : '';
                                $is_location_map = isset($xml_object->loc_map) ? $xml_object->loc_map : 'checked';
                                $loc_lat = isset($xml_object->loc_lat) ? $xml_object->loc_lat : '37.6';
                                $loc_long = isset($xml_object->loc_long) ? $xml_object->loc_long : '-95.665';
                                $loc_zoom = isset($xml_object->loc_zoom) ? $xml_object->loc_zoom : '6';
                                $event_loc = isset($xml_object->event_loc) ? $xml_object->event_loc : '';

                                // Check Map Visibility
                                $map_visibility = ('checked' == $is_location_map) ? 'none' : 'block'; 
                                ?>
                                
                                <ul class="form-elements">
                                    <li class="field-label">
                                        <input type="checkbox"   id="eve_location" name="custom[event_loc]" <?php echo $event_loc; ?>  />
                                        <?php _e("Don't Show venue", 'simple-event-planner') ?>
                                    </li>
                                </ul>
                                <ul class="form-elements">                            
                                    <li class="field-label">
                                        <label> <?php _e('Address', 'simple-event-planner') ?> </label>
                                    </li>
                                    <li class="element-field">
                                        <input type="text"   id="loc-address" name="event_address" value="<?php echo $loc_address; ?>"  />
                                        <input type="hidden" id="loc-add"     name="event-address" value="<?php echo $loc_address; ?>"  />
                                        <label><?php _e('Enter Your Address', 'simple-event-planner') ?> </label>
                                    </li>
                                </ul>
                                <ul class="form-elements">                            
                                    <li class="field-label">
                                        <input type="checkbox"  id="location-map" name="loc_map" <?php echo $is_location_map; ?>  /> <?php _e("Don't Show Map", 'simple-event-planner') ?>
                                    </li>
                                </ul>
                                <fieldset class="gllpLatlonPicker" style="display:<?php echo $map_visibility ?>"> 
                                    <div class="clear"></div>
                                    <input type="hidden" value="<?php echo $loc_address; ?>"  class="gllpSearchField">
                                    <input type="button" id="map-search-btn" class="gllpSearchButton button-primary" value="<?php _e("Search Map", 'simple-event-planner') ?>">                            
                                    <br/><br/>
                                    <div id="event-map-convas" class="gllpMap"> <?php _e('Google Maps', 'simple-event-planner') ?> </div>
                                    <div class="clear"></div>
                                    <input type="hidden" class="gllpLatitude"  name="loc_lat"  value="<?php echo $loc_lat; ?>"/>
                                    <input type="hidden" class="gllpLongitude" name="loc_long" value="<?php echo $loc_long; ?>"/>
                                    <input type="hidden" class="gllpZoom"      name="loc_zoom" value="<?php echo $loc_zoom; ?>"/>
                                </fieldset>
                            </div>                            
                        </div>                
                        <input type="hidden" name="event_meta_form" value="1" />
                    </div>
                    <div class = "clear"></div>
                </div>
            </div>
        </div>
        <?php
    }

    /**
     * Update Event Meta. 
     *
     * @since   1.0.0
     * 
     * @param   $post_id    Event post id 
     */
    public static function sep_save_event_listing_meta($post_id) {
        global $post;
        
        $sxe = new SimpleXMLElement('<simple_event_planner></simple_event_planner>');
        Simple_Event_Planner_Meta_Box_Event_Options::save_google_map($sxe, $post_id);

        // Checkbox for event date
        $_POST['custom']['event_date'] = (!empty($_POST['custom']['event_date'])) ? "checked" : "";

        // Checkbox for event time
        $_POST['custom']['event_time'] = (!empty($_POST['custom']['event_time'])) ? "checked" : "";

        // Checkbox for organizer name
        $_POST['custom']['event_org'] = (!empty($_POST['custom']['event_org'])) ? "checked" : "";

        // Checkbox for organizer email
        $_POST['custom']['event_email'] = (!empty($_POST['custom']['event_email'])) ? "checked" : "";

        // Checkbox for organizer contact
        $_POST['custom']['org_contact'] = (!empty($_POST['custom']['org_contact'])) ? "checked" : "";

        // Checkbox for event location
        $_POST['custom']['event_loc'] = (!empty($_POST['custom']['event_loc'])) ? "checked" : "";

        // Checkbox for event segment
        $_POST['custom']['event_segment'] = (!empty($_POST['custom']['event_segment'])) ? "checked" : "";

        // Adding child element to the XML node.
        if (isset($_POST['custom'])) {
            foreach ($_POST['custom'] as $key => $value) {
                if ($key !== "add_seg") {
                    $value = sanitize_text_field($value);
                    $sxe->addChild($key, empty($value) ? '' : $value );
                }
            }
        }
        
        // Event Start Date & Time
        if (isset($_POST['custom']['start_date']) && isset($_POST['custom']['start_time'])) {

            // Add or update event start time.
            update_post_meta($post_id, 'event_start_date_time', strtotime($_POST['custom']['start_date'] . ' ' . $_POST['custom']['start_time']));
        }

        // Add segment array in postmeta for event type post.
        $add_seg = (empty($_POST['custom']['add_seg'])) ? $_POST['custom']['add_seg'] = " " : $_POST['custom']['add_seg'];
        $serialized_add_seg = serialize($add_seg);
        update_post_meta($post_id, 'add_segment', $serialized_add_seg);

        // Saving whole child node in one metadata.
        update_post_meta($post_id, 'event_meta', $sxe->asXML());
    }

    /**
     * Update Event's Location Map. 
     *
     * @since   1.1.0
     * 
     * @param   $sxe    XML Object
     * @param   $id     Event post id  
     * @return  void
     */
    private static function save_google_map($sxe, $id = '') {
        
        /**
         *  Set Event Meta Default Values
         */
        if (empty($_POST["event_address"])) {
            $_POST["event_address"] = "";
        }

        if (empty($_POST["loc_lat"])) {
            $_POST["loc_lat"] = "";
        }

        if (empty($_POST["loc_long"])) {
            $_POST["loc_long"] = "";
        }

        if (empty($_POST["loc_zoom"])) {
            $_POST["loc_zoom"] = "";
        }
        
        $_POST["loc_map"] = (!empty($_POST["loc_map"])) ? "checked" : "";
        
        // Save Event Location Meta
        $sxe->addChild('event_address', esc_attr($_POST["event_address"]));
        $sxe->addChild('loc_map', esc_attr($_POST["loc_map"]));
        $sxe->addChild('loc_lat', esc_attr($_POST["loc_lat"]));
        $sxe->addChild('loc_long', esc_attr($_POST["loc_long"]));
        $sxe->addChild('loc_zoom', esc_attr($_POST["loc_zoom"]));

        update_post_meta($id, 'event_address', esc_attr($_POST["event_address"]));
    }

}