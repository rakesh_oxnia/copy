<?php if (!defined('ABSPATH')) { exit; } // Exit if accessed directly
/**
 * Simple_Event_Planner_Settings_Init Class
 * 
 * This is used to includes admin settings for:
 *
 * - General Settings.
 * - GMAP API Key Settings.
 *  
 * @link        https://wordpress.org/plugins/simple-event-planner/
 * @since       1.1.0
 * 
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/includes
 * @author      PressTigers <support@presstigers.com>
 */

class Simple_Event_Planner_Settings_Init {

    /**
     * Initialize the class and set its properties.
     *
     * @since   1.1.0
     */
    public function __construct() {

        /**
         * Calling file containing functionalty of general settings.
         */
        require_once plugin_dir_path(__FILE__) . '/settings/class-simple-event-planner-general-settings.php';

        // Check if General Settings Class Exists
        if (class_exists('Simple_Event_Planner_General_Settings')) {

            // Initialize General Settings Object  
            new Simple_Event_Planner_General_Settings();
        }

        /**
         * Calling file containing functionalty of Gmap API key settings.
         */
        require_once plugin_dir_path(__FILE__) . '/settings/class-simple-event-planner-settings-api-key.php';

        // Check if Event Calendar Settings Class Exists
        if (class_exists('Simple_Event_Planner_Settings_Api_Key')) {

            // Initialize Event Calendar Settings Object  
            new Simple_Event_Planner_Settings_Api_Key();
        }

        // Hook -> Add Settings Menu
        add_action('admin_menu', array($this, 'admin_menu'));
    }

    /**
     * Event Planner Settings Page.
     *
     * @since     1.1.0
     * 
     * @return    void
     */
    public function admin_menu() {
        add_submenu_page('edit.php?post_type=event_listing', __('Settings', 'simple-event-planner'), __('Settings', 'simple-event-planner'), 'manage_options', 'event-planner-settings', array($this, 'settings_tab_menu'));
    }

    /**
     * Settings tabs for event listing and calendar settings.
     * 
     * @since    1.1.0
     * 
     * @global   Object   $post               Post Object
     * @global   array    $sep_event_options  WP options Settings for Event Planner. 
     */
    public function settings_tab_menu() {
        global $post, $sep_event_options;
        $sep_event_options = get_option('sep_event_options');
        $sep_version = new Simple_Event_Planner();
        ?> 

        <!-- Event Planner Settings Form -->
        <div class="sep-wrap">

            <!-- Event Planner Settings Form -->
            <form id="optioin_frm" method="post" action="javascript:event_option_save('<?php echo admin_url('admin-ajax.php'); ?>', '<?php echo get_template_directory_uri() ?>');">
                <div class="loading_div">
                    <div class="loading"><i class="fa fa-spin fa-spinner"></i></div>
                </div>
                <div class="sep-container-fluid pt-wrapper-bg">
                    <div class="sep-col-lg-2 sep-col-md-3 sep-col-sm-4 sep-col-xs-1" style="padding:0;">

                        <!-- Settings Saved Notification -->
                        <div class="form-msg"><?php _e('Setting have been saved.', 'simple-event-planner'); ?></div>

                        <!-- Settings Tabs -->
                        <div class="sep-sidebar">
                            <div class="branding">
                                <strong>Simple Event Planner</strong><br />
                                    <?php echo $sep_version->get_version(); ?>
                            </div>
                            <div class="main-nav">
                                <ul class="sub-menu categoryitems" style="display:block">
                                    <?php
                                    /**
                                     * Filter the Settings Tab Menus. 
                                     * 
                                     * @since 1.1.0 
                                     * 
                                     * @param array (){
                                     *     @type array Tab Id => Settings Tab Name
                                     * }
                                     */
                                    $settings_tabs = apply_filters('sep_settings_tab_menus', array());
                                    $count = 1;
                                    foreach ($settings_tabs as $key => $tab_name) {
                                        $active_tab = ( 1 === $count ) ? 'active' : '';
                                        ?>
                                        <li class="<?php echo $active_tab ?>">
                                            <a href="#<?php echo $key; ?>-settings" onClick="toggleDiv(this.hash);
                                                    return false;">
                                                <?php if ('General Settings' == $tab_name) { ?>
                                                    <i class="fa fa-cogs"></i><?php echo $tab_name; ?> 

                                                <?php } elseif ('GMAP API Key Settings' == $tab_name) { ?>
                                                    <i class="fa fa-map" aria-hidden="true"></i><?php echo $tab_name; ?> 

                                                <?php } ?>

                                            </a>
                                        </li>
                                        <?php
                                        $count++;
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="sep-col-lg-10 sep-col-md-9 sep-col-sm-8 sep-col-xs-11" style="padding: 0;">
                        <!-- Settings Sections -->
                        <div class="save-options">
                            <input type="submit" id="submit_btn" name="submit_btn" class="topbtn" value="<?php _e('Save Changes', 'simple-event-planner'); ?>" />
                        </div>
                        <div class="main-content">
                            <!-- General Settings Sections-->
                            <div id="general-settings"><?php do_action('sep_general_settings'); ?></div>
                            <!-- GMap API Settings Sections-->
                            <div id="api-key-settings" style="display:none;"><?php do_action('sep_api_key_settings'); ?></div>
                        </div>
                        <div class="save-options">
                            <input type="submit" id="submit_btn" name="submit_btn" class="topbtn" value="<?php _e('Save Changes', 'simple-event-planner'); ?>" />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <input type="hidden" name="action" value="event_option_save"/>
                </div>
            </form>
        </div>
        <?php
    }

}

new Simple_Event_Planner_Settings_Init();