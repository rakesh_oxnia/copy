<?php if (!defined('ABSPATH')) { exit; } // Exit if accessed directly
/**
 * Simple_Event_Planner_Admin Class
 * 
 * The admin-specific functionality of the plugin.
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and javaScript
 * 
 * @link       https://wordpress.org/plugins/simple-event-planner/
 * @since      1.0.0
 * 
 * @package    Simple_Event_Planner
 * @subpackage Simple_Event_Planner/admin
 * @author     PressTigers <support@presstigers.com>
 */

class Simple_Event_Planner_Admin {

    /**
     * The ID of this plugin.
     *
     * @since   1.0.0
     * @access  private
     * @var     string  $simple_event_planner   The ID of this plugin.
     */
    private $simple_event_planner;

    /**
     * The version of this plugin.
     *
     * @since   1.0.0
     * @access  private
     * @var     string  $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since   1.0.0
     * @param   string  $simple_event_planner   The name of this plugin.
     * @param   string  $version                The version of this plugin.
     */
    public function __construct($simple_event_planner, $version) {
        $this->simple_event_planner = $simple_event_planner;
        $this->version = $version;

         /**
         * The class responsible for defining all the meta options under custom post type in the admin area.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-simple-event-planner-admin-meta-boxes-init.php';
        
        /**
         * The class is responsible for defining all the plugin settings that occurs in event setting.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-simple-event-planner-admin-settings-init.php';


        /**
         * The class is responsible for creating shortcode builder.
         */
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-simple-event-planner-admin-shortcodes-generator.php';
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since   1.0.0
     * 
     * @version 1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Simple_Event_Planner_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Simple_Event_Planner_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        if (is_admin()) {
            wp_enqueue_style( $this->simple_event_planner."-opensans", 'https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,600italic,700,700italic,800,800italic)', array( ), 'all' );
            wp_enqueue_style( $this->simple_event_planner."-fontawsome", '//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', array( ), '4.6.3', 'all' );
            wp_enqueue_style($this->simple_event_planner . '-admin-styles', plugin_dir_url(__FILE__) . 'css/simple-event-planner-admin.css', array('wp-color-picker'), $this->version, 'all');
            wp_enqueue_media();
        }
    }

    /**
     * Register the JavaScript for the admin area.  
     *
     * @since   1.0.0
     * @since   1.1.0  Added new JS file  simple-event-planner-functions.js
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Simple_Event_Planner_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Simple_Event_Planner_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        if (is_admin()) {
            $sep_event_options = get_option('sep_event_options');
            $api_key = ( !empty($sep_event_options['sep_event_gmap_key'])) ? 'key='. trim($sep_event_options['sep_event_gmap_key']) . '&' :'';
            
            wp_register_script($this->simple_event_planner . '-jquery-google-map-api', 'http://maps.googleapis.com/maps/api/js?'. $api_key .'v=3.24&libraries=places', '', $this->version, FALSE);
            wp_enqueue_script($this->simple_event_planner . '-jquery-gmaps-latlon-picker', plugin_dir_url(__FILE__) . 'js/jquery-gmaps-latlon-picker.js', '', $this->version, FALSE);
            wp_enqueue_script($this->simple_event_planner . '-jquery-geocomplete', plugin_dir_url(__FILE__) . 'js/jquery.geocomplete.min.js', '', $this->version, FALSE);
            wp_enqueue_script($this->simple_event_planner . '-jquery-datepicker', plugin_dir_url(__FILE__) . 'js/jquery.datetimepicker.js', '', $this->version, FALSE);
            wp_enqueue_script($this->simple_event_planner . '-admin-scripts', plugin_dir_url(__FILE__) . 'js/simple-event-planner-admin.js', array('jquery', 'wp-color-picker'), $this->version, FALSE);
        }
    }

}