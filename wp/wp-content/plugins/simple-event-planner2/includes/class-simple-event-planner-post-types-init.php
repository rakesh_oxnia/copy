<?php if (!defined('ABSPATH')) { exit; } // Exit if accessed directly
/**
 * Simple_Event_Planner_Post_Types_Init class
 *
 * Define Custom Post Types.
 * 
 * This class is used to include the file of custom post type. 
 * 
 * @link       http://www.presstigers.com
 * @since      1.0.0
 * 
 * @package    Simple_Event_Planner
 * @subpackage Simple_Event_Planner/includes
 * @author     PressTigers <support@presstigers.com>
 */

class Simple_Event_Planner_Post_Types_Init {

    /**
     * Initialize the class and set its properties.
     *
     * @since   1.0.0
     * 
     * @return  void
     */
    public function __construct() {

        // Including Event Listing Custom Post Type
        require_once plugin_dir_path(__FILE__) . 'posttypes/class-simple-event-planner-post-type-event-listing.php';
        
        // Check if Event Listing Post Type Class Exists
        if (class_exists('Simple_Event_Planner_Post_Type_Event_Listing')) {
            new Simple_Event_Planner_Post_Type_Event_Listing ();
        }
    }

}

new Simple_Event_Planner_Post_Types_Init();