<?php if (!defined('ABSPATH')) { exit; } // Exit if accessed directly
/**
 * Simple_Event_Planner_Shortcode_Event_Calendar class
 *
 * Event Calendar Shortcode.
 * 
 * This class shows the event calendar on frontend for [event_calendar] 
 * shortcode. It displays calendar with all upcoming events listing.
 * 
 * @link        https://wordpress.org/plugins/simple-event-planner/
 * @since       1.1.0
 * 
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/includes/shortcodes
 * @author      PressTigers <support@presstigers.com>
 */

class Simple_Event_Planner_Shortcode_Event_Calendar
{
    /**
     * The ID of this plugin.
     *
     * @since    1.1.0
     * @access   private
     * @var      string    $simple_event_planner    The ID of this plugin.
     */
    private $simple_event_planner;

    /**
     * The version of this plugin.
     *
     * @since    1.1.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.1.0
     * @param    string    $simple_event_planner       The name of the plugin.
     * @param    string    $version    The version of this plugin.
     */
    public function __construct($simple_event_planner, $version) {
        $this->simple_event_planner = $simple_event_planner;
        $this->version = $version;

        // Hook-> Event Calendar Shortcode
        add_shortcode('event_calendar', array($this, 'simple_event_planner_calendar'));
    }

    /**
     * Event Calander Shortcode
     *
     * @since   1.0.0
     * 
     * @param   string  $attr Calendar Shortcode Parameters 
     * @return  void 
     */
    public function simple_event_planner_calendar( $atts, $content ) {
        ob_start();
        
        // Shortcode Default Array
        $shortcode_args = array(
            'event_category' => '',
            'search'         => 'true',
            'address'        => '',
            'events_limit'   => '5'
        );

        //Combines user shortcode attributes with known attributes
        $shortcode_args = shortcode_atts($shortcode_args, $atts);

        date_default_timezone_set('UTC');
        $current_time = strtotime(current_time('Y-m-d H:i:s', $gmt = 0));
        $rand_id = rand(999, 9999);

        // Enqueueing Scripts for Event Calendar
        wp_enqueue_script($this->simple_event_planner . '-frontend');
        wp_enqueue_script($this->simple_event_planner . '-jquery-event-calendar-min');
        wp_enqueue_script($this->simple_event_planner . '-jquery-moment');
        wp_enqueue_script($this->simple_event_planner . '-jquery-geocomplete');

        $count = 1;
        $seprator = ',';

        // Default WP_Query Arguments 
        $args = array(
            'posts_per_page' => $shortcode_args['events_limit'],
            'post_type'      => 'event_listing',
            'post_status'    => 'publish',
            'meta_key'       => 'event_start_date_time',
            'meta_value'     => $current_time,
            'meta_compare'   => '>=',
            'orderby'        => 'meta_value',
            'order'          => 'ASC',
        );

        // Extending Argument Array for Event Category
        if (isset($shortcode_args['event_category']) && '' !== $shortcode_args['event_category']) {
            $event_category = explode(",", $shortcode_args['event_category']);
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'event_listing_category',
                    'field'    => 'slug',
                    'terms'    => $event_category,
                ),
            );
        }

        // Search Address Meta Query 
        $meta_query = array('relation' => 'OR',);
        if (isset($shortcode_args['address']) && '' !== trim($shortcode_args['address'])) {
            $search_address = explode(",", $shortcode_args['address']);
            $meta_query[] = array(
                'key'     => 'event_address',
                'value'   => $search_address[0],
                'compare' => 'LIKE'
            );
        }

        // Extending Argument Array for Event Search
        $args['meta_query'] = $meta_query;
        $custom_query = new WP_Query($args);
        $published_posts = $custom_query->post_count;
        $rand_id = rand(999, 9999);
        ?>
        <div class="sep-wrap">
            <?php
            // Display Calendar Search Bar 
            if ('false' !== strtolower($shortcode_args['search']) && '' !== strtolower($shortcode_args['search']))
            {
                // Enqueueing Script for Google API
                wp_enqueue_script($this->simple_event_planner . '-google-api');

                /**
                 * Template -> Calendar Search:
                 * 
                 * - Event Location Search.
                 * 
                 * Search calendar event listing by its location.
                 */
                get_simple_event_planner_template('search/calendar-search.php', array('rand_id' => $rand_id, 'event_category' => $shortcode_args['event_category'],));
            }

            // Calendar Json Encoding
            $event_calendar = '[';
            if ($custom_query->have_posts() <> "") {
                while ($custom_query->have_posts()): $custom_query->the_post();
                    global $post;
                    $loc_address = get_post_meta(get_the_ID(), 'event_address', TRUE);
                    $event_xml = get_post_meta($post->ID, 'event_meta', true);
                    $xml_object = new SimpleXMLElement($event_xml);
                    $start_date = isset($xml_object->start_date) ? $xml_object->start_date : '';
                    $start_time = isset($xml_object->start_time) ? $xml_object->start_time : '';
                    $dateformat = date('Y-m-d', strtotime($start_date));
                    $timeformat = date('H:i:s', strtotime($start_time));
                    $event_calendar .= '{"date":"' . $dateformat . ' ' . $timeformat . '","type":"","title":"' . get_the_title() . '","description":"' . $loc_address . '","url":"' . get_permalink() . '"}';
                    if ($count != $published_posts) {
                        $event_calendar .= $seprator;
                    }
                    $count++;
                endwhile;
            }
            $event_calendar .= ']';
            wp_reset_postdata();
            ?>

            <!-- Event Calendar -->    
            <div class="simple-event-calendar" id="simple-event-calendar<?php echo $rand_id; ?>">
                <div class="event-calendar" id="event-calendar-limit"></div>
            </div>

            <!-- Script for Displaying Event Calendar -->
            <script type="text/javascript">
                (function ($) {
                    'use strict';
                    $(document).ready(function ($) {
                        $("#event-calendar-limit").eventCalendar({
                            jsonData:<?php echo $event_calendar; ?>,
                            jsonDateFormat: 'human',
                            txt_noEvents: 'No results found.',
                            showDescription: true,
                            eventsLimit: <?php echo $shortcode_args['events_limit']; ?>,
                            txt_NextEvents: '<?php echo 'Events'; ?>',
                            monthNames: [
                                "January,",
                                "February,",
                                "March,",
                                "April,",
                                "May,",
                                "June,",
                                "July,",
                                "August,",
                                "September,",
                                "October,",
                                "November,",
                                "December"
                            ]
                        });
                    });
                })(jQuery);
            </script>
        </div>
        <?php
        $html = ob_get_clean();
        
        /**
         * Filter -> Modify the Event Calendar Shortcode
         * 
         * @since   1.1.0
         * 
         * @param   HTML    $html    Event Calendar HTML Structure.
         */
        return apply_filters( 'sep_event_calendar_shortcode', $html . do_shortcode($content));
    }
}