<?php if (!defined('ABSPATH')) { exit; } // Exit if accessed directly
/**
 * Simple_Event_Planner_Shortcode_Event_Listing class
 *
 * Event Listing Shortcode.
 *
 * This class lists the events on frontend for [event_listing] shortcode. It 
 * lists all upcoming events in the list with event search bar.
 * 
 * @link        https://wordpress.org/plugins/simple-event-planner/
 * @since       1.1.0
 * 
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/includes/shortcodes
 * @author      PressTigers <support@presstigers.com>
 */

class Simple_Event_Planner_Shortcode_Event_Listing {

    /**
     * Initialize the class and set its properties.
     *
     * @since   1.1.0
     * 
     * @return  void
     */
    public function __construct() {

        // Hook-> Event Listing Shortcode
        add_shortcode('event_listing', array($this, 'simple_event_planner_listing'));
    }

    /**
     * Event Listing Shortcode
     *
     * @since   1.0.0
     * 
     * @param   string  $attr   Shortcode Parameters
     * @return  void 
     */
    public function simple_event_planner_listing($atts, $content) {
        ob_start();
        
        global $event_query;
        
        // Shortcode Default Array
        $shortcode_args = array(
            'type'           => 'upcoming',
            'search'         => 'true',
            'event_category' => '',
            'events_limit'   => '-1',
        );

        // Combines user shortcode attributes with known attributes
        $shortcode_args = shortcode_atts($shortcode_args, $atts);

        // Default Date-Time-Zone
        date_default_timezone_set('UTC');
        $current_time = strtotime(current_time('Y-m-d H:i', $gmt = 0));
        
        // Get paged variable.
        if (get_query_var('paged')) {
            $paged = get_query_var('paged');
        } elseif (get_query_var('page')) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }
        
        // Default WP_Query Arguments 
        $args = array(
            'posts_per_page' => $shortcode_args['events_limit'],
            'post_type'      => 'event_listing',
            'post_status'    => 'publish',
            'meta_key'       => 'event_start_date_time',
            'meta_value'     => '',
            'orderby'        => 'meta_value',
            'order'          => 'ASC',
            'paged'          => $paged,
        );

        // Extending Argument Array for Event Category
        if (!empty($shortcode_args['event_category'])) {
            $event_category = explode(",", $shortcode_args['event_category']);
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'event_listing_category',
                    'field'    => 'slug',
                    'terms'    => $event_category,
                ),
            );
        }

        // Extending Argument Array Event Search by Title
        $args['s'] = ( !empty($_GET['search_keyword']) ) ? $_GET['search_keyword'] : '';

        // Extending Argument Array for Event Type
        if ('upcoming' == $shortcode_args['type']) {
            $args['meta_value'] = $current_time;
            $args['meta_compare'] = '>=';
        } elseif ('past' == $shortcode_args['type']) {
            $args['meta_value'] = $current_time;
            $args['meta_compare'] = '<=';
        }

        // Event Query
        $event_query = new WP_Query($args);

        /**
         * Template -> Event Lisiting Start:
         * 
         * - Event Start Wrapper
         */
        get_simple_event_planner_template('event-listing/event-listings-start.php');

        // Display Keyword Search Bar
        if ('false' !== strtolower($shortcode_args['search']) && '' !== strtolower($shortcode_args['search'])) {

            /**
             * Template -> Event Search:
             * 
             * - Event Keyword Search
             * 
             * Search event by its title.
             */
            get_simple_event_planner_template('search/event-search.php');
        }

        if ( $event_query->have_posts() ) :
            
            while ($event_query->have_posts()): $event_query->the_post();
                /**
                 * Template -> Event Archive listing:
                 * 
                 * - Event Archive Lisiting Template
                 */
                get_simple_event_planner_template('content-event-listing.php');
            endwhile;
            
            /**
             * Template -> Event Pagination:
             * 
             * - Add Pagination to Resulted Events.
             */
            get_simple_event_planner_template('event-listing/event-pagination.php');
        else :

            /**
             * Template -> Event Not Found:
             * 
             * - Will Triger When Event Will Not Found.
             */
            get_simple_event_planner_template('event-listing/content-no-events-found.php');
        endif;
        wp_reset_postdata();
        
        /**
         * Template -> Event lisiting End:
         * 
         * - Event End Wrapper
         */
        get_simple_event_planner_template('event-listing/event-listings-end.php');        
        
         $html = ob_get_clean();
        
        /**
         * Filter -> Modify the Event Listing Shortcode
         * 
         * @since   1.1.0
         * 
         * @param   HTML    $html   Event Listing HTML Structure.
         */
        return apply_filters( 'sep_event_listing_shortcode', $html . do_shortcode( $content ) );
    }

}