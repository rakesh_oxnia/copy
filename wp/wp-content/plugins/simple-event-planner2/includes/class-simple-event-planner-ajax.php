<?php if (!defined('ABSPATH')) { exit; } // Exit if accessed directly
/**
 * Simple_Event_Planner_Ajax Class
 *
 * This file is used to save event planner settings options. Also for searching
 * events in calendar listing.
 * 
 * @link        https://wordpress.org/plugins/simple-event-planner/
 * @since       1.1.0
 * 
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/includes
 * @author      PressTigers <support@presstigers.com>
 */

Class Simple_Event_Planner_Ajax {

    /**
     * Initialize the class and set its properties.
     * 
     * @since   1.1.0
     */
    public function __construct() {

        // Hook -> Ajax Call -> Saving Event's Settings
        add_action( 'wp_ajax_nopriv_event_option_save', array( $this, 'event_option_save' ) );
        add_action( 'wp_ajax_event_option_save', array(&$this, 'event_option_save' ) );

        // Hook -> Ajax Call -> Search Events on Calendar Search Box
        add_action( 'wp_ajax_nopriv_search_events', array( $this, 'search_events' ) );
        add_action( 'wp_ajax_search_events', array( $this, 'search_events' ) );
    }

    /**
     * Saving Color Values into Wp Options 
     * 
     * @since    1.1.0
     * 
     * @global   array $_POST typography colors for event calendar & listing.
     * 
     * return    void
     */
    public function event_option_save() {
        
        // Make sure $_POST are set before
        $setting_options = isset($_POST) ? (array) $_POST : array();

        // Sanitize the Settings Options Array 
        $setting_options = array_map( 'esc_attr', $setting_options );
        
        // Save Settings Options
        update_option( 'sep_event_options', $setting_options );
        die();
    }

    /**
     * Search Events on Calendar Search Box 
     *
     * @since   1.0.0
     * 
     * @global  array $_POST  containig type , event category and address indexes.
     * 
     * @return  void 
     */
    public function search_events() {
        foreach ($_POST as $key => $val) {
            $$key = $val;
        }
        
        // Display Searched Event Results 
        echo $html = do_shortcode('[event_calendar type="' . $type . '" event_category=>"' . $event_category . '" search="false" address="' . $loc_address . '"]');
        die();
    }

}

new Simple_Event_Planner_Ajax ();