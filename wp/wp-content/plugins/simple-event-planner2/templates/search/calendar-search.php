<?php
/**
 * The template containig event location search for calendar event listing page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/search/calendar-search.php
 * 
 * @version     1.0.0
 * @since       1.1.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/search
 */
global $post;
?>

<!-- Start Calendar Search
================================================== -->
<div class="sep-search">

    <!-- Search Form -->
    <form class="search-form-events" method="post" action="javascript:search_events('<?php echo admin_url("admin-ajax.php") ?>','<?php echo $rand_id; ?>')" id="search-form<?php echo $rand_id; ?>">
        <input class="sep-input" type="text" placeholder="Search Location" id="loc-addres<?php echo $rand_id; ?>" name="loc-addres<?php echo $rand_id; ?>" value=""/>
        <button class="sep-button-cal submit-location" type="submit" id="<?php echo $rand_id; ?>"></button>
        <span class="glyphicons glyphicons-search"></span>
        <input type="hidden" id="location-address<?php echo $rand_id; ?>" name="location-address<?php echo $rand_id; ?>" value="" />
        <input type="hidden" id="loc-search<?php echo $rand_id; ?>" name="loc-search<?php echo $rand_id; ?>" value="false" />
        <input type="hidden" id="event-cat<?php echo $rand_id; ?>" name="event-cat<?php echo $rand_id; ?>" value="<?php echo trim($event_category); ?>" />
    </form>
</div>
<!-- ==================================================
End Calendar Search -->