<?php
/**
 * The template containig search by event title for event listing page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/search/event-search.php
 * 
 * @version     1.0.0
 * @since       1.1.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/search
 */
global $post;
?>

<!-- Start Event Keyword Search
================================================== -->
<div class="sep-search">
    <?php    
    // Get Current Page Slug           
    $slugs = sep_get_slugs();
    $page_slug = $slugs[0];
    $slug = ( !empty( $page_slug ) ) ? $page_slug : '';
    $search_keyword = isset($_GET['search_keyword']) ? esc_attr( $_GET['search_keyword'] ) : '';
    ?>    
    <form action="<?php echo esc_url(home_url('/')) . $slug ; ?>" method="get">
        <input class="sep-input" type="text" name="search_keyword" value="<?php echo $search_keyword; ?>" placeholder="<?php _e('Search by Event Title', 'simple-event-planner') ?>" />

        <?php
        // Append Query string With Page ID When Permalinks are not Set.
        if (!get_option('permalink_structure') && !is_front_page() && !is_home()) {
            ?>
            <input type="hidden" value="<?php echo get_the_ID(); ?>" name="page_id" >
        <?php } ?>
            
        <button class="sep-button" type="submit"></button>
        <span class="glyphicons glyphicons-search"></span>
    </form>
</div>
<!-- ==================================================
End Event Keyword Search -->