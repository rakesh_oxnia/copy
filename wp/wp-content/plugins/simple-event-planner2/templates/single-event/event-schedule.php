<?php
/**
 * This part of template displaying counter section of the event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-schedule.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */
global $post;
if ( '' != sep_get_the_event_start_date() || '' != sep_get_the_event_end_date() ) { ?>

<!-- Start Event Schedule 
================================================== -->
<div class="sep-event-schedule">
    <?php
    /**
     * Template -> event-counter:
     * 
     * - Event Counter
     */
    get_simple_event_planner_template('single-event/event-counter.php'); 
    ?>
</div>
<!-- ==================================================
End Event Schedule section -->

<?php }