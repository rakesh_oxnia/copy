<?php
/**
 * This template contains category and organiser's detail. Which are getting display on event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-details.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */
global $post;

if ('' <> sep_get_the_event_organizer() || '' <> sep_get_the_organizer_contact() || '' <> sep_get_the_organizer_email()) {
    ?>
    <!-- Start Event Organizer Details 
    ================================================== -->
    <div class="sep-event-organizer">
        <h3><?php _e('Details:', 'simple-event-planner'); ?></h3>
        <table class="sep-table-responsive">
            <tbody>
                <?php
                /**
                 * Template -> name:
                 * 
                 * - Event Organizer Name
                 */
                get_simple_event_planner_template('single-event/event-details/name.php');

                /**
                 * Template -> contact:
                 * 
                 * - Event Organizer's Contact
                 */
                get_simple_event_planner_template('single-event/event-details/contact.php');

                /**
                 * Template -> email:
                 * 
                 * - Event Organizer Email 
                 */
                get_simple_event_planner_template('single-event/event-details/email.php');

                /**
                 * Template -> category:
                 * 
                 * - Event Category
                 */
                get_simple_event_planner_template('single-event/event-details/category.php');
                ?>
            </tbody>
        </table>
    </div>
    <!--End Event Organizer Details 
    ==================================================-->
    
<?php }

if ('' != sep_get_the_event_start_date() || '' != sep_get_the_event_end_date() || '' != sep_get_the_event_start_time() || '' <> sep_get_the_event_end_time()) { ?>
    
    <!-- Start Event Schedule
    ==================================================-->
    <div class="sep-single-event-time">       
        <h3> <?php _e('Schedule', 'simple-event-planner'); ?> </h3>

        <?php
        /**
         * Template -> event-time:
         * 
         * - Event start and end time
         */
        get_simple_event_planner_template('single-event/event-time.php');
        ?>
    </div>
    <!-- ==================================================
    End Event Schedule -->

<?php   }