<?php
/**
 * Template displaying featured image and descirption of event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-detail/category.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event/event-detail
 */
global $post;

if ($event_categories = sep_get_the_event_category($post)) { ?> 

    <!-- Start Event category 
    ================================================== -->
    <tr>
        <th scope="row">
            <?php
            $category_title = sizeof($event_categories) > 1 ? __('Categories', 'simple-event-planner') : __('Category', 'simple-event-planner');
            echo '<h4>' . $category_title . '</h4>'
            ?>
        </th>
        <td>
            <?php
            sep_the_event_category();
            ?>
        </td>
    </tr>
    <!-- ==================================================
    End Event Category -->
    
<?php }