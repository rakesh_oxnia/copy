<?php
/**
 * This template contains event organizer's email.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-detail/email.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event/event-detail
 */
global $post;

if ('' <> sep_get_the_organizer_email()) { ?> 

    <!-- Start Event Organizer's Email 
    ================================================== -->
    <tr>
        <th scope="row">
            <h4> <?php _e('Email', 'simple-event-planner'); ?> </h4>
        </th>
        <td>
            <span><a href="mailto:<?php echo sep_get_the_organizer_email(); ?> "> <?php echo sep_get_the_organizer_email(); ?> </a></span>
        </td>
    </tr>
    <!-- ==================================================
    End Event Organizer's Email -->
    
<?php }