<?php
/**
 * Template displayng featured image and descirption of event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-description.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */
global $post;
?>

<!-- Start Event Title, Featured Image & Description 
================================================== -->
<div class="sep-event-description">

    <?php    
    /**
     * Template -> Featured Image:
     * 
     * - Event Features Image
     */
    get_simple_event_planner_template('single-event/featured-image.php'); ?>
    
    <!-- Event Title -->
    <div class="sep-event-title">
        <h2><?php echo apply_filters('sep_single_event_detail_page_title', get_the_title()); ?></h2>
    </div>

    <!-- Event Description -->
    <div class="single-event-description">
        <?php the_content(); ?> 
    </div>
</div>
<!-- ==================================================
End Event Title, Featured Image & Description-->