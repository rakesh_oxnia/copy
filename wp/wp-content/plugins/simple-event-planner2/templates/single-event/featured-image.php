<?php
/**
 * Template displaying featured image of event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/featured-image.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */
global $post;
if( sep_get_the_event_image() ) {
?>

<!-- Start Event Feature Image 
================================================== -->
<div class="single-event-image">
    <?php sep_the_event_image('', '', 557, 300, $post); ?>  
</div>
<!-- ==================================================
End Event Feature Image  -->

<?php }