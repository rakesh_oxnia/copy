<?php
/**
 * Displaying venue and map of event of event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-venue.php
 * 
 * @author      PressTigers
 * @version     1.0.0
 * @since       1.1.0
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */
global $post;

if ('' <> sep_get_the_event_venue() || sep_get_the_event_venue_map()) { ?>

    <!-- Start Event Venue and Map 
    ================================================== -->
    <div class="sep-event-venue">
        
        <!-- Event Location -->
        <?php if ('' <> sep_get_the_event_venue()) { ?>
            <h3> <?php _e('Venue:', 'simple-event-planner'); ?> </h3>
            <div class="sep-event-info">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <span><?php echo sep_get_the_event_venue(); ?></span>
            </div>
        <?php } ?> 
            
        <!-- Event Venue Map -->
        <?php if (sep_get_the_event_venue_map()) { ?>
            <div class="sep-venue-map">
                
                <?php echo sep_get_the_event_venue_map(); ?>                   
            </div>
        <?php } ?>
    </div>
<?php } ?>

<!-- ==================================================
End Event Venue and Map -->

<?php
$segments = sep_get_event_segment();
if (is_array($segments) && '' <> $segments[0]) { ?>

    <!-- Start Event Segments
    ================================================== -->
    <div class="sep-single-segments">
        <?php
        /**
         * Template -> Segments:
         * 
         * - Event segment
         */
        get_simple_event_planner_template('single-event/event-details/segments.php');
        ?>
    </div>
    <!-- ==================================================
    End Event Segments -->

<?php }