<?php
/**
 * This part of template displaying event start date on top left corner of event detail page.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/single-event/event-date.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/single-event
 */
global $post;

/**
 * Template -> event-date:
 * 
 * - Event Start Date
 */
$start_date = sep_get_the_event_start_date();
$split_date = explode(',', $start_date);

if (!empty($start_date)) { ?>
    <strong>
        <?php
        $date_array = array();
        $date_array = explode(' ', $split_date[0]);
        echo $date_array[1] . '</br>';
        ?>
    </strong>
    <?php
    echo $date_array[0] . $split_date[1];
}