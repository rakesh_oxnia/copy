<?php
/**
 * The template invoking event's featured image, title & description template.
 * 
 * Override this template by copying it to yourtheme/simple_event_planner/content-single-event-listing.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates
 */
?>

<!-- Start Event Description
  ================================================== -->
<div class="single-event-listing">    
    <?php
    /**
     * Template -> Event Description:
     * 
     * - Event Featured Image
     * - Event Title
     * - Event Description
     */
    get_simple_event_planner_template('single-event/event-description.php');
    ?>
</div>
<!-- ==================================================
 End Event Description -->