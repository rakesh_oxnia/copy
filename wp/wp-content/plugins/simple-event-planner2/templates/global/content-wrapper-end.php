<?php
/**
 * Content wrappers
 *
 * Override this template by copying it to yourtheme/simple_event_planner/global/content-wrapper-end.php
 * 
 * @since       1.1.0
 * @version     1.0.0
 * @author 	PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/global
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$template = get_option('template');
switch ($template) {
    case 'twentyeleven':
        echo '</div></div>';
        break;
    case 'twentytwelve':
        echo '</div></div>';
        break;
    case 'twentythirteen':
        echo '</div></div>';
        break;
    case 'twentyfourteen':
        echo '</div></div></div>';
        get_sidebar('content');
        break;
    case 'twentyfifteen':
        echo '</div></div></div>';
        break;
    case 'twentysixteen':
        echo '</div></main>  ';
        break;
    default :
        echo '</div></div>';
        break;
}