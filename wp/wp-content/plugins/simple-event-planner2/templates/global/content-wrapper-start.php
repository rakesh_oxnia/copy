<?php
/**
 * Content wrappers
 *
 * Override this template by copying it to yourtheme/simple_event_planner/global/content-wrapper-start.php
 * 
 * @since       1.1.0
 * @version     1.0.0
 * @author 	PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/global
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$template = get_option('template');
switch ($template) {
    case 'twentyeleven' :
        echo '<div id="primary"><div role="main">';
        break;
    case 'twentytwelve' :
        echo '<div id="primary"><div id="content" role="main" class="twentytwelve">';
        break;
    case 'twentythirteen' :
        echo '<div id="primary" class="site-content"><div id="content" role="main" class="entry-content twentythirteen">';
        break;
    case 'twentyfourteen' :
        echo '<div id="primary" class="content-area"><div id="content" role="main" class="site-content twentyfourteen"><div class="tfwc">';
        break;
    case 'twentyfifteen' :
        echo '<div id="primary" role="main" class="content-area twentyfifteen"><div id="main" class="site-main t15wc">';
        break;
    case 'twentysixteen' :
        echo '<div id="primary" class="content-area twentysixteen"> <main id="main" class="site-main" role="main">';
        break;
    default :
        echo '<div class="container" id="container"><div id="content" class="sep-content" role="main">';
        break;
}