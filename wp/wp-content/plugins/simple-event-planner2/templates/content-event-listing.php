<?php
/**
 * The template for displaying event venue, schedual, featured image and event description.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/content-event-listing.php
 * 
 * @version     1.0.0
 * @since       1.1.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates
 */
global $post;
?>

<!-- Start Event Listing
================================================== -->
<div class="sep-event-listing">
    <article class="event-list">
        <div class="sep-row">            
            <?php
            /**
             * Template -> Featured Image:
             * 
             * - Event Featured Image
             */
            get_simple_event_planner_template('event-listing/featured-image.php');

            /**
             * Template -> Title:
             * 
             * - Event Title
             */
            ?>
            <div class="sep-content">
                <div class="sep-details">
                    <?php
                    get_simple_event_planner_template('event-listing/title.php');

                    /**
                     * Template -> Schedule:
                     * 
                     * - Event Schedule
                     */
                    get_simple_event_planner_template('event-listing/schedule.php');

                    /**
                     * Template -> Short Descrioption:
                     * 
                     * - Event Descrioption
                     */
                    ?>    
                </div>
                <div class="sep-map">
                    <div class="sep-text-center map-style">
                        <i class="fa fa-map" aria-hidden="true"></i>
                        <a target="blank" href="<?php sep_the_event_venue_google_map_link(); ?> "> <span><?php echo '&nbsp;' . __('View on map', 'simple-event-planner'); ?></span></a>
                    </div>
                </div>
            </div>
        </div>
    </article>        
</div> 
<!-- ==================================================
End Event Listing -->