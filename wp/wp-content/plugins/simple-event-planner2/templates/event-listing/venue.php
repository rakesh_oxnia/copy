<?php
/**
 * The template for displaying event venue google link.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/event-listing/venue.php
 * 
 * @version     1.0.0
 * @since       1.1.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/event-listing
 */
global $post;

if ( sep_get_the_event_venue() <> '' ) { ?>

    <!-- Start Event Venue Google Map Link
    ================================================== -->
    <a target="blank" href="<?php sep_the_event_venue_google_map_link(); ?> "><?php sep_the_event_venue(); ?></a>
    <!-- ==================================================
    End Event Venue Google Map Link -->
    <?php
}