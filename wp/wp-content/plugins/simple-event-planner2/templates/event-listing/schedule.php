<?php
/**
 * The template for displaying event event schedule detail and venue.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/event-listing/schedule.php
 * 
 * @version     1.0.0
 * @since       1.1.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/event-listing
 */
global $post;
?>

<!-- Start Event Start & End Date
================================================== -->
<div class="event-plan">
    <div class="event-schedule">
        <?php
        /**
         * Template -> Venue:
         * 
         * - Event Venue.
         */
        get_simple_event_planner_template('event-listing/venue.php');
        ?>
    </div>
    <div class="event-schedule">
        <?php
        /**
         * Template -> Date:
         * 
         * - Event Start & End Date.
         */
        get_simple_event_planner_template('event-listing/date.php');
        ?>
    </div>
</div>
<!-- ==================================================
End Event Start & End Date -->