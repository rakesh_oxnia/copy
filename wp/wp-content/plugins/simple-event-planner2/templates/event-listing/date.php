<?php
/**
 * The template for displaying event start & end date.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/event-listing/date.php
 * 
 * @version     1.0.0
 * @since       1.1.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/event-listing
 */
global $post;

if ('' <> sep_get_the_event_start_date() && '' <> sep_get_the_event_end_date()) {
?>                            
<time datetime="<?php echo sep_get_the_event_start_date() . __(' - to - ', 'simple-event-planner') . sep_get_the_event_end_date(); ?>"><?php echo sep_get_the_event_start_date() . __(' - to - ', 'simple-event-planner') . sep_get_the_event_end_date(); ?></time>
<?php } elseif ('' <> sep_get_the_event_start_date()) { ?>
<time datetime="<?php echo sep_get_the_event_start_date(); ?>"><?php echo sep_get_the_event_start_date(); ?></time>
<?php }