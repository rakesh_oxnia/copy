<?php
/**
 * Displayed when no events are found, matching the current query.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/event-listing/content-no-events-found.php
 *
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/event-listing
 */
 _e( 'No events found.', 'simple-event-planner' );