<?php 
/**
 * Event list end
 *
 * Override this template by copying it to yourtheme/simple_event_planner/event-listing/event-listing-start.php
 * 
 * @version     1.0.0
 * @since       1.1.0
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/event-listing
 */
?>
<div class="clear"></div>