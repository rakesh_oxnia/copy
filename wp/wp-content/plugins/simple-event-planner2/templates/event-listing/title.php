<?php
/**
 * The template for displaying event title.
 *
 * Override this template by copying it to yourtheme/simple_event_planner/event-listing/title.php
 * 
 * @version     1.0.0
 * @since       1.1.0 
 * @author      PressTigers
 * @package     Simple_Event_Planner
 * @subpackage  Simple_Event_Planner/templates/event-listing
 */
global $post;
?>

<!-- Start Event Title
================================================== -->
<h2 class="sep-post-title">
    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
</h2>
<!-- ==================================================
End Event Title -->