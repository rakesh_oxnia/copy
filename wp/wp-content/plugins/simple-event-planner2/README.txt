=== Simple Event Planner ===
Contributors: PressTigers
Donate link: http://www.presstigers.com
Tags: event, events, event planner, event listing, event listings, calendar, event calendar, venue, calendar, wedding, seminar, presentation, party
Requires at least: 4.5
Tested up to: 4.6
Stable tag: 1.2.2
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

A powerful & flexible plugin to create event listing and event calendar on your website in simple & elegant way.

== Description ==
= Are you looking for easy, user-friendly and robust event management plugin? = 

Simple Event Planner by <a href="http://www.presstigers.com">PressTigers</a> is a next generation, lightweight event management plugin that list events and calendar to your WordPress website.

This plugin is used to manage & display various events within the site. It has various options, including events from different categories. Whether you have; a single event or multiple events, you can display it as a list or in calendar format by simply inserting shortcode i.e. [event_listing], [event_calendar]. 

Additionally, you can add event calendar on your WordPress website by simply inserting shortcode [event_calendar], making it extremely powerful and flexible. 

The plugin allows to have a specific number of upcoming events arranged in calendar list along with search feature to search events by event location.

For advance level of integration, please [contact us](http://market.presstigers.com)

= Plugin Features =

* Create Events Quickly
* Time Zone Settings for Events
* Unlimited Event Segments
* Show/Hide Event's Options
* List View
* Calender View
* Search Event by Title (List View)
* Search Event by Location (Calender View)
* Events Categories (Taxonomies)
* Responsive Layout
* Localization (Translation Ready)
* Unlimited Color Combinations
* Shortcode Builder
* Google Maps for Event Location

= Event Planner Templating = 

With event planner templating exciting feature you can change the following file templates.

* content-wrapper-start.php
* content-wrapper-end.php
* event-listings-start.php
* event-listings-end.php
* content-event-listing.php
* content-no-events-found.php
* content-single-event-listing.php
* event-schedule.php
* event-description.php
* event-details.php
* event-venue.php
* single-event-listing.php
* event-search.php
* calendar-search.php
* archive-event-listing.php
* event-pagination.php

1. To change template, please add "simple_event_planner" folder at default theme root directory.
1. Add above mentioned file from plugin simple-event-planner >templates folder keeping the same file directory structure and customize it based on your needs.

= Can you contribute? =
If you are an awesome contributor for translations or plugin development, please contact us at support@presstigers.com

== Installation ==

1. Upload `simple-event-planner.zip` to the `/wp-content/plugins/` directory to your web server.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Add a standard WordPress page or post and use [event_listing] shortcode in the  editor to make it events list.
4. Add a standard WordPress page or post and use [event_calendar] shortcode in the  editor to make it event calendar.


== Frequently Asked Questions ==

= How to create event list? =
In your WordPress admin panel, go to "Event Planner" menu and add a new event. All the event listing will be shown in the admin panel and on the front-end.

= How to show event list on the front-end? = 
To list all the event list, add  [event_listing] shortcode in an existing page or add a new page and write shortcode anywhere in the page editor.

= How to show event calendar on the front-end? = 
To list event calendar , add [event_calendar] shortcode in an existing page or add a new page and write shortcode anywhere in the page editor.

= What language files are available? =
You can view (and contribute) translations via the <a target='_blank' href="https://translate.wordpress.org/projects/wp-plugins/simple-event-planner"> translate.wordpress.org </a>.

= Can I display event list for particular "category" using a shortcode? = 
Yes, you can use shortcode on post/page i.e [event_listing event_category="category-slug"]

= Can I show event list of Calendar for particular "category" using a shortcode? = 
Yes, you can use a shortcode on post page i.e , [event_calendar event_category="category-slug"]

= Can I show only 5 latest events on front-end with pagination? =
Yes, you can show any number of events on your website with pagination feature by using shortcode with "events_limit" attribute i.e. [event_listing events_limit="5"]

= Can I show only 5 latest events of calendar on front-end? = 
Yes, you can show any number of events on your website by using shortcode with "events_limit" attribute i.e [event_calendar events_limit="5"]

= Can I turn off calendar search bar? = 
Yes, you turnoff search bar with "search" attribute i.e [event_calendar search="false"]

= Can I turn off event list search bar? = 
Yes, you can turn off search bar with "search" attribute i.e [event_listing search="false"]

== Changelog ==

= 1.2.2 =
* Fix - Resolved minor calendar layout issues.
* Fix - Trimmed content in different labels & descriptions.
* Fix - Resolved front-end map loading issue.

= 1.2.1 =
* Fix - Critical issue resolved in event listing & calendar shortcode.

= 1.2.0 =
* Feature - General settings for event listing, event detail page & calendar to change colors.
* Feature - Revamped the event listing page design.
* Feature - Revamped the event calendar page design.

= 1.1.0 =
* Feature - Added Google map for event location.    
* Feature - Introduced templating.
* Feature - Introduced settings.
* Feature - Typography settings for event listing, event detail page & calendar.
* Feature - GMap API key settings.
* Feature - Added event title search for event listing.
* Feature - Added event detail and archive template pages. 
* Feature - Event's all detail can hide or show from admin site.
* Feature - Added time zone for events.
* Feature - Added multiple segments for a event.
* Feature - Revised the event listing and event detail page design.
* Feature - Revised the event calendar page design. 
* Feature - Added event planner shortcodes generator in TinyMCE editor.
* Feature - Ready for translation.
* Feature - Added pagination for event listing.
* Fix - Closed the jQuery datepicker for event start & end date after selecting date.
* Fix - Closed the jQuery timepicker list for event start & end time after selecting time.

= 1.0.0 =
* First release

== Upgrade Notice ==

= 1.2.0 =
The version have new design/layout & general color scheme settings for all templates. Upgrade immediately. 

= 1.2.1 =
Critical issue resolved in event listing & calendar shortcode. Upgrade immediately! 

= 1.2.2 =
Critical issue resolved for calendar on event detail page. Upgrade immediately! 

== Screenshots ==

1. **Event Options** - Let the user to fill details of event.
2. **Event Location** - Let the user to fill event location and set its map.
3. **Event Categories** - List of Categories (Taxonomies)
4. **Event List Creation** - Allow users to create event list with ease by using a shortcode. 
5. **Event Calendar Creation** - Allow users to create event calendar with ease by using a shortcode.
6. **Event General Settings** -  Customize color scheme of all three templates. 
7. **Event API Key Settings** -  Add API key to use GMap for location address.
8. **Event Listing** - Front-end view of all upcoming events.
9. **Event Detail Page** - Event detail/single page. Event details related to event location and its organizer are placed on it.
10. **Event Calendar** - Front-end view of calendar with upcoming events listing.