<?php
/**
 * @package Tusi
 */

get_header(); 
?>
<div class="site-main section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-8">
                <div class="page-404-content">
                    <h1><?php _e( 'Oops! That page can&rsquo;t be found.', 'tusi' ); ?></h1>
                    <p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'tusi' ); ?></p>
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>