<?php
/**
 * @package Tusi
 */

get_header(); 
?>
<?php if ( have_posts() ){ ?>
<div class="archive-banner">
    <div class="container">
        <?php
            the_archive_title( '<h1 class="archive-page-title">', '</h1>' );
            the_archive_description( '<div class="taxonomy-description">', '</div>' );
        ?>
    </div>
</div>
<?php } ?>
<div class="site-main section-padding page-archive-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-8">
                <div class="blog-posts">
                    <?php
                    if ( have_posts() ) :
                        
                        while ( have_posts() ) : the_post();
                            get_template_part( 'template-parts/post/content');
                        ?> 
                       
                        <?php endwhile;
                        the_posts_pagination(array(    
                            'mid_size'  => 2,
                            'prev_text' => __( 'Prev', 'tusi' ),
                            'next_text' => __( 'Next', 'tusi' )
                            )
                        );
                    
                    endif;
                    ?>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <?php get_sidebar();?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>