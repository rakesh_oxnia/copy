Options -Indexes
RewriteEngine on

<IfModule mod_rewrite.c>
    RewriteCond %{REQUEST_URI} !^/backend/web/(assets|css)/
    RewriteCond %{REQUEST_URI} admin
    RewriteRule .* backend/web/index.php [L]

    RewriteCond %{REQUEST_URI} !^/(frontend|backend)/web/(assets|css)/
    RewriteCond %{REQUEST_URI} !admin
    RewriteRule .* frontend/web/index.php [L]
</IfModule>

http://stackoverflow.com/questions/28118691/yii2-htaccess-how-to-hide-frontend-web-and-backend-web-completely