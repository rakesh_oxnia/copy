<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\UnauthorizedHttpException;
use yii\base\InvalidParamException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use api\common\components\BaseController;

use common\models\User;
use common\models\LiveChat_API;
use frontend\models\Invoices;
use common\models\Packages;
use common\models\PackageUser;
use common\models\LeadSeen;
use common\models\UserCategory;
use app\models\Leads;
use common\models\ForwardingEmailAddress;
use common\models\ForwardingEmailTeam;
use common\models\EmailLog;
use app\models\BusinessCategory;
use app\models\AppsCountries;
/********
Site Controller API
@author Webcubictechnologies.com <ravindra@webcubictechnologies.com>
*********/

class SiteController extends BaseController
{
	public $modelClass = 'common\models\User';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors() {
        $behaviors = parent::behaviors();
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        return $actions;
    }

    public function actionTest()
    {
        print_r("Api ready"); exit;
    }

		public function actionLogin()
		{

			$email = Yii::$app->request->post('email');
			$password = Yii::$app->request->post('password');

			if(!$email){
	      throw new BadRequestHttpException("Email cannot be left blank");
			}

			if(!$password){
				throw new BadRequestHttpException("Password cannot be left blank");
			}

			$userModel = User::findOne(['email' => $email]);

			if($userModel->email_verified != 1){
				throw new BadRequestHttpException("Email not verified");
			}

			if($userModel){
				if($userModel->validatePassword($password)){
					$userArray = User::find()
															->select([
															'username',
															'auth_key',
															'email',
															'status',
															'website_url',
															'role',
															'user_group',
															'created_at',
															'updated_at',
															'send_transcript',
															'your_name',
															'parent_id'])
															->where(['id' => $userModel->id])->asArray()->one();
					$users_groups = User::find()->select(['user_group',
					'website_url'])->where(['parent_id' => $userModel->id])->andFilterWhere(['!=', 'role', User::ROLE_OUTLET])->asArray()->all();
					$response = ['success' => 1, 'message' => 'Login successful', 'data' => $userArray ,'group' =>$users_groups, 'statuses' => User::$statuses];
				}else{
					$response = ['success' => 0, 'message' => 'Password is wrong'];
				}
			}else{
				$response = ['success' => 0, 'message' => 'Email not registered'];
			}

			return $response;

		}

		//=====================================Lead=====================================================//
		public function actionLeads()
		{
			function packageInfo($user_id){
	       $invoiceData = Invoices::find()->where(['user_id' => $user_id])->all();
				 $payment_date = '';
				 $number_of_leads = '';
				 $pack_type = '';
				 if(!empty( $invoiceData )){
					 foreach( $invoiceData as $key => $invoice){
						 if($invoice['package_id'] > 0){
							 $package_id = $invoice['package_id'];
							 $packageData = Packages::findOne( $package_id );
							  //print_r($packageData); exit;
							 if( !empty( $packageData ) ){
								 if($packageData->package_name == 'Custom' && ($pack_type == '' || $pack_type == 'Gold' || $pack_type == 'Startup')){
									 $packageUser = PackageUser::findOne(['user_id' => $user_id]);
									 if($packageUser){
											 $payment_date = $invoice->payment_date;
											 $number_of_leads = $packageUser->number_of_leads;
											 $pack_type = 'Custom';
									 }
								 }elseif ($packageData->package_name == 'Gold' && ($pack_type == '' || $pack_type == 'Startup')) {
									 $payment_date = $invoice->payment_date;
									 $number_of_leads = $packageData->number_of_leads;
									 $pack_type = 'Gold';
								 }elseif ($packageData->package_name == 'Startup' && $pack_type == '') {
									 $payment_date = $invoice->payment_date;
									 $number_of_leads = $packageData->number_of_leads;
									 $pack_type = 'Startup';
								 }
							 }
						 }
					 }
				 }
				 //=====================================================================================///
				 if($pack_type != ''){
					return  [$payment_date => $number_of_leads];
				 }
				}
			 //=========================================================================================//
			 //========================================================================================//
				function leadMerge($chats, $user, $packageInfo)
				{
					$leads      = [];
					$transcripts = $chats->chats;
			    $total = $chats->total;
			    $pages = $chats->pages;

					// lead populate array //
					//return $transcripts;
		      $leads = Leads::filterLeadPageData( $transcripts, $leads );
					//print_r($transcripts); exit;

		      for($page = 2; $page <= $pages; $page++){
		        $API      = new LiveChat_API();
		        $_GET['page'] = $page;
		        $chats      = $API->chats->get($page,$_GET);
		        $transcripts  = $chats->chats;
		        $leads      = Leads::filterLeadPageData( $transcripts, $leads );
		      }

		      $leads = array_reverse( $leads );
		      $leads = Leads::getAllLeadsData($leads, $packageInfo, $user);
		      $leads = array_reverse( $leads );

					// lead populate array //

					// additional filtering //
					foreach ($leads as $key => $chat) {

						// unset messages //
						unset($leads[$key]['messages']);
						// unset messages //

						// mark lead as seen functionality
						$leadModel = Leads::findOne(['chat_id' => $chat['id'], 'new_lead' => 1]);
						if($leadModel){
							$leadSeen = LeadSeen::findOne(['user_id' => $user->id, 'leads_id' => $leadModel->id]);
							if(!$leadSeen){
								$leadSeen = new LeadSeen();
								$leadSeen->user_id = $user->id;
								$leadSeen->leads_id = $leadModel->id;
								$leadSeen->save();
							}

							// add status //
							$leads[$key]['status'] = $leadModel->status;
							// add status //
						}
						// mark lead as seen functionality

						$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
						// filter leads for particular outlet //
						if($user->role == User::ROLE_OUTLET){
							if(!$leadModel || $leadModel->outlet_id != $user->id || $leadModel->outlet_id == 0){
								unset($leads[$key]);
								continue;
							}
						}
						// filter leads for particular outlet //

						//category part
						if(isset($_GET['category']) && $_GET['category'] != ''){
							//$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
							if($leadModel && $leadModel->category_id != NULL){
								if($_GET['category'] == $leadModel->category_id){
									$catModel = UserCategory::findOne($leadModel->category_id);
									if($catModel){
										$leads[$key]['category'] = ['id' => $catModel->id, 'name' => $catModel->category_name];
									}
								}else{
									unset($leads[$key]);
									continue;
								}
							}else{
								unset($leads[$key]);
								continue;
							}
						}else{
							$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
							if($leadModel && $leadModel->category_id != NULL){
								$catModel = UserCategory::findOne($leadModel->category_id);
								if($catModel){
									$leads[$key]['category'] = ['id' => $catModel->id, 'name' => $catModel->category_name];
								}
							}
						}
						//category part

						//filter outlet from url get param
						if($user->role == User::ROLE_FRANCHISE){
							if(isset($_GET['outlet']) && $_GET['outlet'] != ''){
								//$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
								if($leadModel && $leadModel->outlet_id != NULL){
									if($_GET['outlet'] != $leadModel->outlet_id){
										unset($leads[$key]);
										continue;
									}
								}else{
									unset($leads[$key]);
									continue;
								}
							}
						}
						//filter outlet from url get param

						if(isset($_GET['status']) && $_GET['status'] != ''){
							//$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
							if($leadModel && $leadModel->status != NULL){
								if($_GET['status'] != $leadModel->status){
									unset($leads[$key]);
									continue;
								}
							}else{
								unset($leads[$key]);
								continue;
							}
						}
					}
					// additional filtering //
					$response = ['success' => 1, 'data' => $leads];
					return $response;
				}
			 //========================================================================================//

			 // crange //
			 function crange(){
	 		 $API = new LiveChat_API();
	 		 $date_from = Yii::$app->request->get('date_from');
	 		 $date_to = Yii::$app->request->get('date_to');
			 if(!$date_from){
				 throw new BadRequestHttpException("From date cannot be left blank");
			 }
			 if(!$date_to){
				 throw new BadRequestHttpException("To date cannot be left blank");
			 }
	 		 $headers = Yii::$app->request->headers;
	 		 $accept = $headers->get('User-Auth-token');
	 		 $user = User::findOne(['auth_key'=>$accept]);
	 		 if(!$user){
	 						 throw new BadRequestHttpException("Invalid User Data");
	 		 }
	 		 $group_id = Yii::$app->request->get('group_id');
	 		 $user_id = $user->id;
	 		 $group_by = "day";
	 		 //=======================================================================================//
			 $API = new LiveChat_API();
			 $page = '';
			 $user_id = $user->id;
			 $packageInfo = packageInfo($user_id);
			 $chats = $API->chats->get($page,
																 ['date_from' =>$date_from,
																 'date_to'=>$date_to,
																 'tag[]'=>'lead',
																 'group'=> $group_id,
																 'group_by'=>$group_by,
																 'page' => '1'
																 ]);
			 return leadMerge($chats, $user, $packageInfo);
	 		 //========================================================================================//
	 	 }
			 // crange //

			 // last 30 //
			 function last30()
			 {
				$date_from = date('Y-m-d',strtotime("-29 days"));
	 			$date_to = date('Y-m-d');
				$group_by = 'day';
				$group_id = Yii::$app->request->get('group_id');
				$API = new LiveChat_API();
				$page = '';
				$headers = Yii::$app->request->headers;
				$accept = $headers->get('User-Auth-token');
	      $user = User::findOne(['auth_key'=>$accept]);
				$user_id = $user->id;
			  $packageInfo = packageInfo($user_id);
				$chats = $API->chats->get($page,
																	['date_from' =>$date_from,
																	'date_to'=>$date_to,
																	'tag[]'=>'lead',
																	'group'=> $group_id,
																	'group_by'=>$group_by,
																	'page' => '1'
																	]);
				return leadMerge($chats, $user, $packageInfo);
			 }
			 // last 30 //

			 // last 7 //
			 function last7()
			 {
		 			$API = new LiveChat_API();
		 			$date_from = date('Y-m-d',strtotime("-6 days"));
		 			$date_to = date('Y-m-d');
		 			$headers = Yii::$app->request->headers;
		 			$accept = $headers->get('User-Auth-token');
		      $user = User::findOne(['auth_key'=>$accept]);
		 			if(!$user){
		 	            throw new BadRequestHttpException("Invalid User Data");
		 			}
		 			$group_id = Yii::$app->request->get('group_id');

		 			if(!$group_id){
		 	       throw new BadRequestHttpException("Group id cannot be left blank");
		 			}

		 			$user_id = $user->id;
		 			$group_by = "day";
		 			//=======================================================================================//
					$API = new LiveChat_API();
					$page = '';
					$user_id = $user->id;
				  $packageInfo = packageInfo($user_id);
					$chats = $API->chats->get($page,
																		['date_from' =>$date_from,
																		'date_to'=>$date_to,
																		'tag[]'=>'lead',
																		'group'=> $group_id,
																		'group_by'=>$group_by,
																		'page' => '1'
																		]);
					return leadMerge($chats, $user, $packageInfo);
		 			//========================================================================================//
		 		}
			 // last 7 //

			 // yesterday //
			 function yesterday()
			 {
		 			$API = new LiveChat_API();
		 			$date_from = date('Y-m-d',strtotime("-1 days"));
		 			$date_to = date('Y-m-d',strtotime("-1 days"));
		 			$headers = Yii::$app->request->headers;
		 			$accept = $headers->get('User-Auth-token');
		       $user = User::findOne(['auth_key'=>$accept]);
		 			if(!$user){
		 	            throw new BadRequestHttpException("Invalid User Data");
		 			}
		 			$group_id = Yii::$app->request->get('group_id');
		 			$user_id = $user->id;
		 			$group_by = "hour";
		 			//=======================================================================================//
					$API = new LiveChat_API();
					$page = '';
					$user_id = $user->id;
				  $packageInfo = packageInfo($user_id);
					$chats = $API->chats->get($page,
																		['date_from' =>$date_from,
																		'date_to'=>$date_to,
																		'tag[]'=>'lead',
																		'group'=> $group_id,
																		'group_by'=>$group_by,
																		'page' => '1'
																		]);
					return leadMerge($chats, $user, $packageInfo);
		 			//========================================================================================//
		 		}
			 // yesterday //

			 // today //
			 function today()
			 {
		 			$API = new LiveChat_API();
		 			$date_from = date('Y-m-d');
		 			$date_to = date('Y-m-d');
		 			$headers = Yii::$app->request->headers;
		 			$accept = $headers->get('User-Auth-token');
		      $user = User::findOne(['auth_key'=>$accept]);
		 			if(!$user){
		 	       throw new BadRequestHttpException("Invalid User Data");
		 			}
		 			$group_id = Yii::$app->request->get('group_id');
		 			$user_id = $user->id;
		 			$group_by = "hour";
		 			//=======================================================================================//
					$API = new LiveChat_API();
					$page = '';
					$user_id = $user->id;
				  $packageInfo = packageInfo($user_id);
					$chats = $API->chats->get($page,
																		['date_from' =>$date_from,
																		'date_to'=>$date_to,
																		'tag[]'=>'lead',
																		'group'=> $group_id,
																		'group_by'=>$group_by,
																		'page' => '1'
																		]);
					return leadMerge($chats, $user, $packageInfo);
		 		}
			 // today //

			 //=========================================================================================//
			 //========================================Case For API ===================================//
			 $filter = Yii::$app->request->get('filter');
			 $headers = Yii::$app->request->headers;
			 $group_id = Yii::$app->request->get('group_id');
			 if(!$group_id){
				 throw new BadRequestHttpException("Group id cannot be left blank");
			 }
			 if($headers->has('User-Auth-token')){
				 switch($filter){
						case "last30":
				 		return last30();
					 	break;

					 	case "last7":
						return  last7();
					 	break;

					 	case "today":
						return today();
					 	break;

					 case "yesterday":
					 return   yesterday();
					 break;

					 case "crange":
					 return  crange();
					 break;

					 default:
					 $response = ['success' => 0, 'message' => 'Invalid Filter'];
					 return $response;
				 }
		 	}else{
			 	$response = ['success' => 0, 'message' => 'Headers are missing'];
			 	return $response;
		 	}
			 //=======================================================================================//
		}
		//=====================================Lead=====================================================//
		//==========================================================================================//
		//=============================================End-Login===================================//
		//=========================================================================================//
		public function actionDashboard()
		{
			//======================================Admin Function================================//
	    function forAdmin($API,$date_from,$date_to,$headers,$accept,$user,$group_id,$group_by){

			$allLeads = $API->reports->get("chats/total_chats",
																		['date_from' =>$date_from,
																		'date_to'=>$date_to,
																		'tag[]'=>'lead',
																		'group_by'=>$group_by
																		]);
			$allChats = $API->reports->get("chats/ratings",
																		['date_from' =>$date_from,
																		'date_to'=>$date_to,
																		'group_by'=>$group_by
																		]);
			$chatCount = 0;
			foreach ($allChats as $d) {
			$chatCount += $d->chats;
			}
			$chatCount;
			$leadCount = 0;
		 foreach ($allLeads as $key => $value) {
			 $leadCount = $leadCount + $value->chats;
		 }
		 $leadCount;
		 $clientRevenue = $leadCount * 200;
		 $chatmetricsRevenue = 10 * $leadCount;
		 $users = User::getCount();
		return	 $response = ['success' => 1,
		   'data' => [
			'leadCount' => $leadCount,
			'clientRevenue'=>$clientRevenue,
			'client'=>$users,
			'chatmetricsRevenue'=>$chatmetricsRevenue,
			'chat'=>$chatCount
		  ]
		 ];
			}
			//================================Function Admin =================================//
			//================================Function User ==================================//
			function forUser($API,$date_from,$date_to,$headers,$accept,$user,$group_id,$group_by,$user_id){
				//=================================================================================//
				  try {
				$allLeads = $API->reports->get("chats/total_chats",
																			['date_from' =>$date_from,
																			'date_to'=>$date_to,
																			'tag[]'=>'lead',
																			'group'=> $group_id,
																			'group_by'=>$group_by
																			]);
				$allChats = $API->reports->get("chats/ratings",
																			['date_from' =>$date_from,
																			'date_to'=>$date_to,
																			'group'=> $group_id,
																			'group_by'=>$group_by
																			]);
								}
				        catch (Exception $e) {
									$response = ['success' => 0, 'message' => $e->getMessage()];
								 return $response;
				        }
			 //==================================================================================//
				$chatCount = 0;
				foreach ($allChats as $d) {
				$chatCount += $d->chats;
				}
				$chatCount;
				$leadCount = 0;
			 foreach ($allLeads as $key => $value) {
				 $leadCount = $leadCount + $value->chats;
			 }
			 $leadCount;
			 $clientRevenue = $leadCount * 200;
			 $chatmetricsRevenue = 10 * $leadCount;
			 $users = User::getCount();
			 $chatmetricsRevenue;
			 //============================================Last Month Investment Start===================================================//
			 $month_start = strtotime('first day of last month');
			 $month_end = strtotime('last day of last month');
			 $lastMonthBill = 0;
			 $invoices = Invoices::find()->where(['user_id' => $user_id])->all();
			 foreach ($invoices as $invoice) {
				 if( strtotime($invoice->payment_date) >=  $month_start && strtotime($invoice->payment_date) <= $month_end){
					 $lastMonthBill = $lastMonthBill + $invoice->amount;
				 }
			 }
			 $lastMonthBill = round($lastMonthBill);
			 //==============================================Last Month Investment End====================================================//
			 ///==================================================
			 return	 $response = ['success' => 1,
		 	   'data' => [
				 'group_id' => $group_id,
		 		 'leadCount' => $leadCount,
		 		 'revenueOpportunities'=>$clientRevenue,
		 		 'lastMonthBill'=>$lastMonthBill,
		 		 'chat'=>$chatCount
		 	  ]
		 	 ];
		   //=================================================================================//
			}
			//================================================================================//
			//=====================================Last30 End=================================//
	    function last30(){
				$API = new LiveChat_API();
				$date_from = date('Y-m-d',strtotime("-29 days"));
				$date_to = date('Y-m-d');
				$headers = Yii::$app->request->headers;
				$accept = $headers->get('User-Auth-token');
	      $user = User::findOne(['auth_key'=>$accept]);
				if(!$user){
		            throw new BadRequestHttpException("Invalid User Data");
				}
				$group_id = Yii::$app->request->get('group_id');
				$user_id = $user->id;
				$group_by = "day";
				//=======================================================================================//
				if($user->role == User::ROLE_ADMIN){
					return  forAdmin($API,$date_from,$date_to,$headers,$accept,$user,$group_id,$group_by);
				}else{
					if($group_id){
	        	return  forUser($API,$date_from,$date_to,$headers,$accept,$user,$group_id,$group_by,$user_id);
					}else{
						$response = ['success' => 0, 'message' => 'Invalid Filter'];
						return $response;
					}
				}
				//========================================================================================//
			}
			//=====================================End Last 30===================================================//
	    //======================================Last7===================================================//
			function last7(){
				$API = new LiveChat_API();
				$date_from = date('Y-m-d',strtotime("-6 days"));
				$date_to = date('Y-m-d');
				$headers = Yii::$app->request->headers;
				$accept = $headers->get('User-Auth-token');
	      $user = User::findOne(['auth_key'=>$accept]);
				if(!$user){
		            throw new BadRequestHttpException("Invalid User Data");
				}
				$group_id = Yii::$app->request->get('group_id');

				if(!$group_id){
		       throw new BadRequestHttpException("Group id cannot be left blank");
				}

				$user_id = $user->id;
				$group_by = "day";
				//=======================================================================================//
				if($user->role == User::ROLE_ADMIN){
					return  forAdmin($API,$date_from,$date_to,$headers,$accept,$user,$group_id,$group_by);
				}else{
					if($group_id){
	        return  forUser($API,$date_from,$date_to,$headers,$accept,$user,$group_id,$group_by,$user_id);
				}else{
					$response = ['success' => 0, 'message' => 'Invalid Filter'];
					return $response;
				}
				}
				//========================================================================================//
			}
			//======================================Last7 End===================================================//
	    //======================================Today=====================================================//
			function today(){
				$API = new LiveChat_API();
				$date_from = date('Y-m-d');
				$date_to = date('Y-m-d');
				$headers = Yii::$app->request->headers;
				$accept = $headers->get('User-Auth-token');
	      $user = User::findOne(['auth_key'=>$accept]);
				if(!$user){
		            throw new BadRequestHttpException("Invalid User Data");
				}
				$group_id = Yii::$app->request->get('group_id');
				$user_id = $user->id;
				$group_by = "hour";
				//=======================================================================================//
				if($user->role == User::ROLE_ADMIN){
					return  forAdmin($API,$date_from,$date_to,$headers,$accept,$user,$group_id,$group_by);
				}else{
					if($group_id){
	        return  forUser($API,$date_from,$date_to,$headers,$accept,$user,$group_id,$group_by,$user_id);
				}else{
					$response = ['success' => 0, 'message' => 'Invalid Filter'];
					return $response;
				}
				}
				//========================================================================================//
			}
	    //======================================Today End=====================================================//
			//======================================yesterday======================================================//
			function yesterday(){
				$API = new LiveChat_API();
				$date_from = date('Y-m-d',strtotime("-1 days"));
				$date_to = date('Y-m-d',strtotime("-1 days"));
				$headers = Yii::$app->request->headers;
				$accept = $headers->get('User-Auth-token');
	      $user = User::findOne(['auth_key'=>$accept]);
				if(!$user){
		            throw new BadRequestHttpException("Invalid User Data");
				}
				$group_id = Yii::$app->request->get('group_id');
				$user_id = $user->id;
				$group_by = "hour";
				//=======================================================================================//
				if($user->role == User::ROLE_ADMIN){
					return  forAdmin($API,$date_from,$date_to,$headers,$accept,$user,$group_id,$group_by);
				}else{
					if($group_id){
	        return  forUser($API,$date_from,$date_to,$headers,$accept,$user,$group_id,$group_by,$user_id);
				}else{
					$response = ['success' => 0, 'message' => 'Invalid Filter'];
					return $response;
				}
				}
				//========================================================================================//
			}
			//======================================yesterday End======================================================//
			//=======================================crange===========================================================//
			function crange(){
			 $API = new LiveChat_API();
			 $date_from = Yii::$app->request->get('date_from');
			 $date_to = Yii::$app->request->get('date_to');
			 $headers = Yii::$app->request->headers;
			 $accept = $headers->get('User-Auth-token');
			 $user = User::findOne(['auth_key'=>$accept]);
			 if(!$user){
							 throw new BadRequestHttpException("Invalid User Data");
			 }
			 $group_id = Yii::$app->request->get('group_id');
			 $user_id = $user->id;
			 $group_by = "day";
			 //=======================================================================================//
			 if($user->role == User::ROLE_ADMIN){
				 return  forAdmin($API,$date_from,$date_to,$headers,$accept,$user,$group_id,$group_by);
			 }else{
				 if($group_id){
					 return  forUser($API,$date_from,$date_to,$headers,$accept,$user,$group_id,$group_by,$user_id);
				 }else{
					 $response = ['success' => 0, 'message' => 'Invalid Filter'];
					 return $response;
				 }
			 }
			 //========================================================================================//
		 }
			//=======================================crange===========================================================//
			//========================================Case For API ===================================//
			$filter = Yii::$app->request->get('filter');
			$headers = Yii::$app->request->headers;
			if($headers->has('User-Auth-token')){
			switch($filter){
	      case "last30":
			return last30();
				break;
				case "last7":
	    return  last7();
				break;
				case "today":
	    return today();
				break;
				case "yesterday":
	  return   yesterday();
				break;
				case "crange":
	  return  crange();
				break;
		  default:
			$response = ['success' => 0, 'message' => 'Invalid Filter'];
			return $response;
			}
		}else{
			$response = ['success' => 0, 'message' => 'Headers are missing'];
			return $response;
		}
			//=======================================================================================//
		}

		public function actionGetChatMessages()
		{
			$accept = Yii::$app->request->headers->get('User-Auth-token');
			$user = User::findOne(['auth_key'=>$accept]);
			if(!$user){
				throw new BadRequestHttpException("Invalid User Data");
			}
			$chat_id =  Yii::$app->request->headers->get('chat_id');
			if(!$chat_id){
				throw new BadRequestHttpException("Chat id cannot be left blank");
			}
			$API = new LiveChat_API();
			$chatData = $API->chats->getSingleChat($chat_id);
			//print_r($chatData); exit;
			$messages = $chatData->messages;
			return $messages;
		}

		public function actionGetForwardingList()
		{
			$accept = Yii::$app->request->headers->get('User-Auth-token');
			$user = User::findOne(['auth_key'=>$accept]);
			if(!$user){
				throw new BadRequestHttpException("Invalid User Data");
			}

			$forwardingAddress = ForwardingEmailAddress::findAll(['user_id' => $user->id]);
			$data = [];
			foreach ($forwardingAddress as $fa) {
				$team = [];
				$teamModel = ForwardingEmailTeam::findOne($fa->team);
				if($teamModel){
					$team = ['id' => $teamModel->id, 'name' => $teamModel->name];
				}
				$data[] = ['name' => $fa->name, 'email' => $fa->email, 'team' => $team];
			}

			return $data;
		}

		public function actionForwardEmail()
		{
			$accept = Yii::$app->request->headers->get('User-Auth-token');
			$user = User::findOne(['auth_key'=>$accept]);
			if(!$user){
				throw new BadRequestHttpException("Invalid User Data");
			}

			$chat_id =  Yii::$app->request->get('chat_id');
			if(!$chat_id){
				throw new BadRequestHttpException("Chat id cannot be left blank");
			}

			$forwardList = Yii::$app->request->get('forward_list');
			if(!$forwardList){
				throw new BadRequestHttpException("Forward list cannot be left blank");
			}

			$showLead = Yii::$app->request->get('show_lead');
			if(!$showLead){
				throw new BadRequestHttpException("Show lead cannot be left blank");
			}

			$leadModel = Leads::find()->where(['chat_id' => $chat_id])->one();
			if(!$leadModel){
				return ['success' => 0, 'message' => 'Lead does not exist'];
			}

			try {
				$API = new LiveChat_API();
				$chat2 = $API->chats->getSingleChat($chat_id);
			}
			catch (Exception $e) {
				throw new NotFoundHttpException($e->getMessage());
			}

			//$showLead = 1; // hardcode
			$name = $leadModel->visitor_name;
			$email = $leadModel->visitor_email;

			$count = count($forwardList);
			$sent_count = 0;
			$sent_email_string = '';

			foreach ($forwardList as $forwardEmail) {
				$sent = Leads::sendForwardingEmail($user, $chat2, $showLead, $name, $email, $leadModel, $forwardEmail);
				$sent_email_string .= $forwardEmail . ', ';
				if($sent){
					$sent_count++;
				}

			}

			if($sent_email_string != ''){
				$emailLog = new EmailLog();
				$emailLog->leads_id = $leadModel->id;
				$emailLog->chat_id = $leadModel->chat_id;
				$emailLog->sent_to = substr($sent_email_string, 0, -2);
				$emailLog->sent_via = EmailLog::VIA_FORWARDING_EMAIL;
				$tempUser = User::findOne(['user_group' => $leadModel->user_group]);
				if($tempUser){
					$emailLog->web_url = $tempUser->website_url;
				}
				$emailLog->save();
				return ['success' => 1, 'message' => 'Email sent successfully'];
			}
		}

		public function actionSendToMe()
		{
			$accept = Yii::$app->request->headers->get('User-Auth-token');
			$user = User::findOne(['auth_key'=>$accept]);
			if(!$user){
				throw new BadRequestHttpException("Invalid User Data");
			}
			$chat_id =  Yii::$app->request->get('chat_id');
			if(!$chat_id){
				throw new BadRequestHttpException("Chat id cannot be left blank");
			}

			$showLead = Yii::$app->request->get('show_lead');
			if(!$showLead){
				throw new BadRequestHttpException("Show lead cannot be left blank");
			}

			$user_email = $user->email;

			$leadModel = Leads::find()->where(['chat_id' => $chat_id])->one();
			if(!$leadModel){
				return ['success' => false, 'message' => 'Lead does not exist'];
			}

			try {
				$API = new LiveChat_API();
				$chat2 = $API->chats->getSingleChat($chat_id);
			}
			catch (Exception $e) {
				throw new NotFoundHttpException($e->getMessage());
			}

			//$showLead = 1; // hardcode
			$name = $leadModel->visitor_name;
			$email = $leadModel->visitor_email;
			$forwardEmail = $user_email;

			$sent = Leads::sendForwardingEmail($user, $chat2, $showLead, $name, $email, $leadModel, $forwardEmail);

			if($sent){
				$emailLog = new EmailLog();
				$emailLog->leads_id = $leadModel->id;
				$emailLog->chat_id = $leadModel->chat_id;
				$emailLog->sent_to = $forwardEmail;
				$emailLog->sent_via = EmailLog::VIA_SEND_TO_ME;
				$tempUser = User::findOne(['user_group' => $leadModel->user_group]);
				if($tempUser){
					$emailLog->web_url = $tempUser->website_url;
				}
				$emailLog->save();
				return ['success' => true, 'message' => 'Email has been sent successfully to ' . $user->email];
			}
			return ['success' => false, 'message' => 'Some problem occurred! Please try again later after sometime'];
		}

		public function actionQuickSend()
		{
			$accept = Yii::$app->request->headers->get('User-Auth-token');
			$user = User::findOne(['auth_key'=>$accept]);
			if(!$user){
				throw new BadRequestHttpException("Invalid User Data");
			}
			$chat_id =  Yii::$app->request->get('chat_id');
			if(!$chat_id){
				throw new BadRequestHttpException("Chat id cannot be left blank");
			}

			$quick_email = Yii::$app->request->get('quick_email');
			if(!$quick_email){
				throw new BadRequestHttpException("Quick email cannot be left blank");
			}

			$showLead = Yii::$app->request->get('show_lead');
			if(!$showLead){
				throw new BadRequestHttpException("Show lead cannot be left blank");
			}

			$leadModel = Leads::find()->where(['chat_id'=>$chat_id])->one();
			if(!$leadModel){
				return ['success' => 0, 'message' => 'Lead does not exist'];
			}

			try {
				$API = new LiveChat_API();
				$chat2 = $API->chats->getSingleChat($chat_id);
			}
			catch (Exception $e) {
				throw new NotFoundHttpException($e->getMessage());
			}

			//$showLead = 1; // hardcode
			$name = $leadModel->visitor_name;
			$email = $leadModel->visitor_email;
			$forwardEmail = $quick_email;

			$sent = Leads::sendForwardingEmail($user, $chat2, $showLead, $name, $email, $leadModel, $forwardEmail);

			if($sent){
				$emailLog = new EmailLog();
				$emailLog->leads_id = $leadModel->id;
				$emailLog->chat_id = $leadModel->chat_id;
				$emailLog->sent_to = $forwardEmail;
				$emailLog->sent_via = EmailLog::VIA_QUICK_SEND;
				$tempUser = User::findOne(['user_group' => $leadModel->user_group]);
				if($tempUser){
					$emailLog->web_url = $tempUser->website_url;
				}
				$emailLog->save();
				return ['success' => 1, 'message' => 'Email has been sent successfully to ' . $quick_email];
			}
		}

		public function actionGetCategories()
		{
			$accept = Yii::$app->request->headers->get('User-Auth-token');
			$user = User::findOne(['auth_key' => $accept]);
			if(!$user){
				throw new BadRequestHttpException("Invalid User Data");
			}

			$categories = UserCategory::findAll(['user_id' => $user->id]);
			foreach ($categories as $key => $category) {
				if($category->parent_id == NULL)
				{
					$categories[$key]->parent_id = 0;
				}
				unset($categories[$key]->created_at);
				unset($categories[$key]->updated_at);
			}
			return $categories;
		}

		public function actionPayPerLead()
		{
			//require_once("../../vendor/stripe/stripe-php/init.php");
			\Stripe\Stripe::setApiKey('sk_test_oaqcMN6OMR5bS83JUGwNBKLP');

			$customer_id = Yii::$app->user->identity->stripeToken;

			$charge = \Stripe\Charge::create(array(
			  "amount" => 50,
			  "currency" => "usd",
			  "description" => "Pay Per Lead",
			  //"source" => $token,
			  "customer" => $customer_id
			));

			$invoice = \Stripe\Invoiceitem::create(array(
			  "customer" => $customer_id,
			  "amount" => 50,
			  "currency" => "usd",
			  "description" => "Pay Per Lead",
			));

			$model = new Invoices;
			$model->user_id = Yii::$app->user->id;
			$model->amount = $charge->amount;
			$model->transaction_id = $charge->balance_transaction;
			$model->invoice_id = $invoice->id;
			$model->package_id = 0;
			$model->chat_id = $id;
			$model->status = 1;
			$model->payment_date = date('Y-m-d H:i:s');
			$model->save(false);
		}

		public function actionCreateAccount()
		{
			$full_name = Yii::$app->request->post('full_name');
			$email = Yii::$app->request->post('email');
			$website_url = Yii::$app->request->post('website_url');
			$password = Yii::$app->request->post('password');
			$role = Yii::$app->request->post('role');
			$business_name = Yii::$app->request->post('business_name');
			$chat_purpose_sales = Yii::$app->request->post('chat_purpose_sales');
			$chat_purpose_e_commerce = Yii::$app->request->post('chat_purpose_e_commerce');
			$industry = Yii::$app->request->post('industry');
			$number_of_employees = Yii::$app->request->post('number_of_employees');
			$country = Yii::$app->request->post('country');
			$b2b = Yii::$app->request->post('b2b');
			$b2c = Yii::$app->request->post('b2c');
			$internal_use = Yii::$app->request->post('internal_use');

			if(!$full_name){
				throw new BadRequestHttpException("Full name cannot be left blank.");
			}
			if(!$email){
				throw new BadRequestHttpException("Email cannot be left blank.");
			}
			if(!$website_url){
				throw new BadRequestHttpException("Website cannot be left blank.");
			}
			if(!$password){
				throw new BadRequestHttpException("Password cannot be left blank.");
			}
			if(!$role){
	  		 throw new BadRequestHttpException("Role cannot be left blank.");
	  	}
			if(!$business_name){
	  		 throw new BadRequestHttpException("Business-name cannot be left blank.");
	  	}
			if($chat_purpose_sales==''){
				throw new BadRequestHttpException("chat purpose sales cannot be left blank.");
			}
			if($chat_purpose_e_commerce==''){
				throw new BadRequestHttpException("chat purpose e-commerce cannot be left blank.");
			}
			if($industry==''){
				throw new BadRequestHttpException("Industry cannot be left blank.");
			}
			if($number_of_employees==''){
				throw new BadRequestHttpException("Number of Employees cannot be left blank.");
			}
			if($country==''){
				throw new BadRequestHttpException("country cannot be left blank.");
			}
			if($b2b==''){
				throw new BadRequestHttpException("B2b name cannot be left blank.");
			}
			if($b2c == ''){
				throw new BadRequestHttpException("B2c cannot be left blank.");
			}
			if($internal_use==''){
				throw new BadRequestHttpException("Internal use cannot be left blank.");
			}

			if (User::find()
	 		->where( [ 'business_name' => $business_name ] )
	 		->exists() ) {
	 		return $response = ['success' => 0, 'message' => 'Business Name Already exist'];
	 		}
		//===================Validation==========================//
	 		if (User::find()
	 		->where( [ 'email' => $email ] )
	 		->exists() ) {
	 		return $response = ['success' => 0, 'message' => 'Email Already exist'];
	 		}
	 	//========================Create Group Dynamically=================================//
		try {
		$API = new LiveChat_API();
		$allAgents = $API->agents->getAllAgents();
	  }
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}
		$emailArr = [];
		foreach ($allAgents as $agent) {
		  $emailArr[] = $agent->login;
		}
		$response = $API->groups->addGroup($business_name, $emailArr);
		$response = json_decode($response);
		if(!$response->id){
			throw new BadRequestHttpException("Failed to Create Group");
		}
    //===============================================================================//
			$user = new User();
			$user->setDefaultValues();
			$user->your_name = $full_name;
			$user->email = $email;
			$user->website_url = $website_url;
			$user->send_transcript = 0;
			$user->business_name = $business_name;
		  $user->role = $role;
			$user->user_group = $response->id;
			$user->email_verified = 0;
			$user->chat_purpose_sales = $chat_purpose_sales;
			$user->chat_purpose_e_commerce = $chat_purpose_e_commerce;
			$user->industry = $industry;
			$user->number_of_employees = $number_of_employees;
			$user->country = $country;
			$user->b2b = $b2b;
			$user->b2c = $b2c;
			$user->internal_use = $internal_use;

			$user->setPassword($password);
			$user->generateAuthKey();
      $user->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
			$link = 'https://'.$_SERVER['SERVER_NAME'].'/frontend/web/index.php?r=site/activate-user&token='.$user->password_reset_token;
			if($user->validate() && $user->save()){
				//Mail start//
			\Yii::$app->mailer->compose(['html' => 'signupVerificationEmail-html', 'text' => 'signupVerificationEmail-text'], ['user' => $user,'link' => $link,'password'=>$password])
                        ->setFrom([\Yii::$app->params['supportEmail'] => 'Chat Metrics'])
                        ->setTo('rajdeep@webcubictechnologies.com')
                        ->setSubject('Chatmetrics Verification') // user create mail subject
                        ->send();
				//Mail End//
			return $response = ['success' => 1, 'message' => 'Please Check your email'];
			}else{
				return $this->errorResponse($user->getErrors());
			}
		}

}
