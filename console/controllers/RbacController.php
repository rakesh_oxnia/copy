<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\User;

/* Assign appropriate roles to all the existing users */ 
class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->getRole('admin');
        $users = User::findAll([
            'role' => User::ROLE_ADMIN,
        ]);
        foreach ($users as $user) {            
            $auth->assign($admin, $user->getId());
        }

        $client = $auth->getRole('client');
        $users = User::findAll([
            'role' => User::ROLE_USER,
        ]);
        foreach ($users as $user) {            
            $auth->assign($client, $user->getId());
        }

        $franchise = $auth->getRole('franchise');
        $users = User::findAll([
            'role' => User::ROLE_FRANCHISE,
        ]);
        foreach ($users as $user) {            
            $auth->assign($franchise, $user->getId());
        }

        $outlet = $auth->getRole('outlet');
        $users = User::findAll([
            'role' => User::ROLE_OUTLET,
        ]);
        foreach ($users as $user) {            
            $auth->assign($outlet, $user->getId());
        }

        $service_account = $auth->getRole('service_account');
        $users = User::findAll([
            'role' => User::ROLE_SERVICE_ACCOUNT,
        ]);
        foreach ($users as $user) {            
            $auth->assign($service_account, $user->getId());
        }

        $agency = $auth->getRole('agency');
        $users = User::findAll([
            'role' => User::ROLE_AGENCY,
        ]);
        foreach ($users as $user) {            
            $auth->assign($agency, $user->getId());
        }
    }
}