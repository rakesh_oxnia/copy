<?php

use yii\db\Schema;
use yii\db\Migration;

class m180119_035610_alter_table_category_add_value_column extends Migration
{
    public function up()
    {
      $this->addColumn("user_category", "value", "FLOAT AFTER `category_name`");
    }

    public function down()
    {

    }
}
