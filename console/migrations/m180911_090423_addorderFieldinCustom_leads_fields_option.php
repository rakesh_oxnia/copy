<?php

use yii\db\Schema;
use yii\db\Migration;

class m180911_090423_addorderFieldinCustom_leads_fields_option extends Migration
{
    public function up()
    {
      $this->addColumn('custom_leads_fields_option', 'order',$this->string());
    }

    public function down()
    {
        echo "m180911_090423_addorderFieldinCustom_leads_fields_option cannot be reverted.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
