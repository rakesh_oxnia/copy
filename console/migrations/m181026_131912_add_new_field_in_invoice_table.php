<?php

use yii\db\Schema;
use yii\db\Migration;

class m181026_131912_add_new_field_in_invoice_table extends Migration
{
    public function up()
    {
      $this->addColumn('invoice', 'single_day_charge',$this->float(4));
    }

    public function down()
    {
        echo "m181026_131912_add_new_field_in_invoice_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
