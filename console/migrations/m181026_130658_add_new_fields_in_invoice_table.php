<?php

use yii\db\Schema;
use yii\db\Migration;

class m181026_130658_add_new_fields_in_invoice_table extends Migration
{
    public function up()
    {
      $this->addColumn('invoice', 'remaining_days', $this->integer());
      $this->addColumn('invoice', 'remaining_days_total_amount',$this->integer());
    }

    public function down()
    {
        echo "m181026_130658_add_new_fields_in_invoice_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
