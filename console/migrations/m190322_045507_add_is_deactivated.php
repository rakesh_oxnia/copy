<?php

use yii\db\Schema;
use yii\db\Migration;

class m190322_045507_add_is_deactivated extends Migration
{
    public function up()
    {
        $this->addColumn('subscription', 'is_deactivated', $this->smallInteger()->defaultValue(0));
    }

    public function down()
    {
        echo "m190322_045507_add_is_deactivated cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
