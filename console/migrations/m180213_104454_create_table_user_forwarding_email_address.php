<?php

use yii\db\Schema;
use yii\db\Migration;

class m180213_104454_create_table_user_forwarding_email_address extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%forwarding_email_address}}', [
            'id' => $this->primaryKey(),
            'user_id' =>  $this->integer(),
            'name' =>  $this->string(),
            'email' => $this->string(),
            'team' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_forwarding_email_address_user', 'forwarding_email_address', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        
    }
}
