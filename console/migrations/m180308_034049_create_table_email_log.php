<?php

use yii\db\Schema;
use yii\db\Migration;

class m180308_034049_create_table_email_log extends Migration
{
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%email_log}}', [
          'id' => $this->primaryKey(),
          'leads_id' => $this->integer(),
          'chat_id' => $this->string(),
          'sent_to' =>  $this->text(),
          'sent_via' => $this->smallInteger(1),

          'created_at' => $this->integer(),
          'updated_at' => $this->integer(),
      ], $tableOptions);

      $this->addForeignKey('fk_email_log_leads', 'email_log', 'leads_id', 'leads', 'id', 'SET NULL', 'SET NULL');
    }

    public function down()
    {
      $this->dropForeignKey('fk_email_log_leads' ,'email_log');
      $this->dropTable('email_log');
    }
}
