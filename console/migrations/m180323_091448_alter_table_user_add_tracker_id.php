<?php

use yii\db\Schema;
use yii\db\Migration;

class m180323_091448_alter_table_user_add_tracker_id extends Migration
{
    public function safeUp()
    {
      $this->addColumn('user', 'tracker_id', 'INTEGER(11) AFTER `id`');
      $this->addColumn('user', 'business_name', 'VARCHAR(255) AFTER `role`');
    }

    public function safeDown()
    {

    }
}
