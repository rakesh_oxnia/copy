<?php

use yii\db\Schema;
use yii\db\Migration;

class m180404_061905_create_new_table_Service_Email extends Migration
{

  public function up()
  {

      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%service_chat_emails}}', [
          'id' => $this->primaryKey(),
          'user_id' =>  $this->integer(),
          'email' => $this->string(),
          'created_at' => $this->integer(),
          'updated_at' => $this->integer(),
      ], $tableOptions);

      $this->addForeignKey('fk_service_chat_emails_user', 'service_chat_emails', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
  }

  public function down()
  {

  }

}
