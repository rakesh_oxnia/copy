<?php

use yii\db\Schema;
use yii\db\Migration;

class m180919_070706_addGlobalSettingToAdminSetting extends Migration
{
    public function up()
    {
       $this->addColumn('admin_setting', 'global_per_lead_cost', $this->integer());
       $this->addColumn('admin_setting', 'global_per_lead_cost_currency', $this->string());
    }

    public function down()
    {
        echo "m180919_070706_addGlobalSettingToAdminSetting cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
