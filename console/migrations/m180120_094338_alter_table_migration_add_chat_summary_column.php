<?php

use yii\db\Schema;
use yii\db\Migration;

class m180120_094338_alter_table_migration_add_chat_summary_column extends Migration
{
    public function up()
    {
      $this->addColumn("leads", "chat_summary", "TEXT AFTER `tags`");
    }

    public function down()
    {
        
    }
}
