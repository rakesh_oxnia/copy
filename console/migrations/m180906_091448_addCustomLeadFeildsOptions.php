<?php

use yii\db\Schema;
use yii\db\Migration;

class m180906_091448_addCustomLeadFeildsOptions extends Migration
{
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%custom_leads_fields_option}}', [
          'id' => $this->primaryKey(),
          'custom_leads_field_id' =>  $this->integer(),
          'option'   =>   $this->string(),
          'created_at' => $this->integer(),
          'updated_at' => $this->integer(),
      ], $tableOptions);

      $this->addForeignKey('fk_custom_leads_fields_id', 'custom_leads_fields_option', 'custom_leads_field_id', 'custom_leads_fields', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m180906_091448_addCustomLeadFeildsOptions cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
