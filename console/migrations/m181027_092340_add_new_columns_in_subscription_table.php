<?php

use yii\db\Schema;
use yii\db\Migration;

class m181027_092340_add_new_columns_in_subscription_table extends Migration
{
    public function up()
    {
      $this->addColumn('subscription', 'number_of_leads',$this->integer());
      $this->addColumn('subscription', 'amount',$this->decimal());
      $this->addColumn('subscription', 'currency',$this->string());
    }

    public function down()
    {
        echo "m181027_092340_add_new_columns_in_subscription_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
