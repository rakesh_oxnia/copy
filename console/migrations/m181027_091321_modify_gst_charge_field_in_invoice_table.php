<?php

use yii\db\Schema;
use yii\db\Migration;

class m181027_091321_modify_gst_charge_field_in_invoice_table extends Migration
{
    public function up()
    {
      $this->alterColumn('invoice', 'gst_charge', $this->decimal(10,2));
    }

    public function down()
    {
        echo "m181027_091321_modify_gst_charge_field_in_invoice_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
