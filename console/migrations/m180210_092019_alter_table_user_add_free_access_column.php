<?php

use yii\db\Schema;
use yii\db\Migration;

class m180210_092019_alter_table_user_add_free_access_column extends Migration
{
    public function up()
    {
      $this->addColumn('user', 'free_access',  $this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {

    }

}
