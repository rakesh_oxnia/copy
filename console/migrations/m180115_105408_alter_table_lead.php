<?php

use yii\db\Schema;
use yii\db\Migration;

class m180115_105408_alter_table_lead extends Migration
{
    public function up()
    {
      $this->addColumn('leads', 'visitor_email', 'VARCHAR(255) AFTER chat_id');
      $this->addColumn('leads', 'visitor_name', 'VARCHAR(255) AFTER visitor_email');
      $this->addColumn('leads', 'visitor_phone', 'VARCHAR(255) AFTER visitor_name');
      $this->addColumn('leads', 'category_id', 'VARCHAR(255) AFTER visitor_phone');
      $this->addColumn('leads', 'outlet_id', 'VARCHAR(255) AFTER category_id');
      $this->addColumn('leads', 'tags', 'VARCHAR(255) AFTER outlet_id');
    }

    public function down()
    {
      $this->dropColumn('leads', 'visitor_email');
      $this->dropColumn('leads', 'visitor_name');
      $this->dropColumn('leads', 'visitor_phone');
      $this->dropColumn('leads', 'category_id');
      $this->dropColumn('leads', 'outlet_id');
      $this->dropColumn('leads', 'tags');
    }
}
