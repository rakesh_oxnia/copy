<?php

use yii\db\Schema;
use yii\db\Migration;

class m180123_085805_alter_table_user_add_column_franchise_name_outlet_name extends Migration
{
    public function up()
    {
      $this->addColumn('user', 'name', 'VARCHAR(255) AFTER `password_reset_token`');
    }

    public function down()
    {

    }
}
