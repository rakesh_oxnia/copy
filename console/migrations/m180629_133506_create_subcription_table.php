<?php

use yii\db\Schema;
use yii\db\Migration;

class m180629_133506_create_subcription_table extends Migration
{
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%subscription}}', [
          'id' => $this->primaryKey(),
          'user_id' =>  $this->integer(),
          'package_name' => $this->string(),
          'package_id' => $this->string(),
          'extra_per_lead_amount' => $this->integer(),
          'advance_payment' => $this->smallInteger(),
          'automatic_charge' => $this->smallInteger(),
          'created_at' => $this->integer(),
          'updated_at' => $this->integer(),
      ], $tableOptions);

      $this->addForeignKey('fk_subscription_user', 'subscription', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m180629_133506_create_subcription_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
