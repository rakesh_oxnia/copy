<?php

use yii\db\Schema;
use yii\db\Migration;

class m180718_100544_addColumnToPackages extends Migration
{
    public function up()
    {
        $this->addColumn('packages', 'user_id', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        echo "m180718_100544_addColumnToPackages cannot be reverted.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
