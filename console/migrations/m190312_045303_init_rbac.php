<?php

use yii\db\Schema;
use yii\db\Migration;

class m190312_045303_init_rbac extends Migration
{
    public function up()
    {
        $auth = Yii::$app->authManager;

        $manageClients = $auth->createPermission('manageClients');
        $manageClients->description = 'Manage Clients';
        $auth->add($manageClients);

        $manageFranchises = $auth->createPermission('manageFranchises');
        $manageFranchises->description = 'Manage Franchises';
        $auth->add($manageFranchises);

        $manageOutlets = $auth->createPermission('manageOutlets');
        $manageOutlets->description = 'Manage Outlets';
        $auth->add($manageOutlets); 

        $manageServiceAccounts = $auth->createPermission('manageServiceAccounts');
        $manageServiceAccounts->description = 'Manage Service Accounts';
        $auth->add($manageServiceAccounts); 
       

        $manageCategories = $auth->createPermission('manageCategories');
        $manageCategories->description = 'Manage Categories';
        $auth->add($manageCategories);


        $manageFeedbacks = $auth->createPermission('manageFeedbacks');
        $manageFeedbacks->description = 'Manage Feedbacks';
        $auth->add($manageFeedbacks);


        $manageEmailLogs = $auth->createPermission('manageEmailLogs');
        $manageEmailLogs->description = 'Manage Email Logs';
        $auth->add($manageEmailLogs);   

        $manageZapierLogs = $auth->createPermission('manageZapierLogs');
        $manageZapierLogs->description = 'Manage Zapier Logs';
        $auth->add($manageZapierLogs);  

        $managePackages = $auth->createPermission('managePackages');
        $managePackages->description = 'Manage Packages';
        $auth->add($managePackages);  

        $manageInvoices = $auth->createPermission('manageInvoices');
        $manageInvoices->description = 'Manage Invoices';
        $auth->add($manageInvoices);  

        $viewReport = $auth->createPermission('viewReport');
        $viewReport->description = 'View Report';
        $auth->add($viewReport);  

        $manageBusinessCategories = $auth->createPermission('manageBusinessCategories');
        $manageBusinessCategories->description = 'Manage Business Categories';
        $auth->add($manageBusinessCategories);  

        $manageCoupons = $auth->createPermission('manageCoupons');
        $manageCoupons->description = 'Manage Coupons';
        $auth->add($manageCoupons);  

        $manageLeads = $auth->createPermission('manageLeads');
        $manageLeads->description = 'Manage Leads';
        $auth->add($manageLeads); 

        $manageServiceChats = $auth->createPermission('manageServiceChats');
        $manageServiceChats->description = 'Manage Service Chats';
        $auth->add($manageServiceChats); 

        $manageChats = $auth->createPermission('manageChats');
        $manageChats->description = 'Manage Chats';
        $auth->add($manageChats); 

        $manageForwardingEmails = $auth->createPermission('manageForwardingEmails');
        $manageForwardingEmails->description = 'Manage Forwarding Emails';
        $auth->add($manageForwardingEmails);


        $manageSubscription = $auth->createPermission('manageSubscription');
        $manageSubscription->description = 'Manage Subscription';
        $auth->add($manageSubscription); 


        /* Create Admin Role and assign permissions */
        $admin = $auth->createRole('admin');
        $auth->add($admin);        
        $auth->addChild($admin, $manageClients);
        $auth->addChild($admin, $manageFranchises);
        $auth->addChild($admin, $manageOutlets);
        $auth->addChild($admin, $manageServiceAccounts);
        $auth->addChild($admin, $manageFeedbacks);
        $auth->addChild($admin, $manageEmailLogs);
        $auth->addChild($admin, $manageZapierLogs);
        $auth->addChild($admin, $managePackages);
        $auth->addChild($admin, $manageInvoices);
        $auth->addChild($admin, $viewReport);
        $auth->addChild($admin, $manageBusinessCategories);
        $auth->addChild($admin, $manageCoupons);

        /* Create Client Role and assign permissions */
        $client = $auth->createRole('client');
        $auth->add($client);    
        $auth->addChild($client, $manageLeads);
        $auth->addChild($client, $manageServiceChats);
        $auth->addChild($client, $manageChats);
        $auth->addChild($client, $manageForwardingEmails);
        $auth->addChild($client, $viewReport);
        $auth->addChild($client, $manageInvoices);
        $auth->addChild($client, $manageCategories);      
        $auth->addChild($client, $manageOutlets);

        /* Create Franchise Role and assign permissions */
        $franchise = $auth->createRole('franchise');
        $auth->add($franchise);        
        $auth->addChild($franchise, $manageLeads);
        $auth->addChild($franchise, $manageServiceChats);
        $auth->addChild($franchise, $manageChats);
        $auth->addChild($franchise, $viewReport);
        $auth->addChild($franchise, $manageInvoices);        
        $auth->addChild($franchise, $manageSubscription);
        $auth->addChild($franchise, $manageCategories);
        $auth->addChild($franchise, $manageOutlets); 
        $auth->addChild($franchise, $manageServiceAccounts);

        /* Create Outlet Role and assign permissions */
        $outlet = $auth->createRole('outlet');
        $auth->add($outlet);        
        $auth->addChild($outlet, $manageLeads);
        $auth->addChild($outlet, $manageServiceChats);
        $auth->addChild($outlet, $viewReport);
        $auth->addChild($outlet, $manageInvoices);        
        $auth->addChild($outlet, $manageCategories);

        /* Create Service account Role and assign permissions */
        $service_account = $auth->createRole('service_account');
        $auth->add($service_account);        
        $auth->addChild($service_account, $manageServiceChats);
       

        /* Create Agency Role and assign permissions */
        $agency = $auth->createRole('agency');
        $auth->add($agency);        
       // $auth->addChild($agency, $viewDashboard);
       // $auth->addChild($agency, $updateSettings);

        $sub_admin = $auth->createRole('sub_admin');
        $auth->add($sub_admin);     

        $assit = $auth->createRole('assit');
        $auth->add($assit);      
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }
}
