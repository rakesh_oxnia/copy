<?php

use yii\db\Schema;
use yii\db\Migration;

class m181201_052123_modify_amount_and_percent_decimal_for_package extends Migration
{
    public function up()
    {
        $this->alterColumn('subscription', 'amount', $this->decimal(10,2));
        $this->alterColumn('subscription', 'gst_percent', $this->decimal(10,2));
        $this->alterColumn('invoice', 'advanced_payment', $this->decimal(10,2));
        $this->alterColumn('invoice', 'total_amount', $this->decimal(10,2));
        $this->alterColumn('invoice', 'gst_percent', $this->decimal(10,2));
    }

    public function down()
    {
        echo "m181201_052123_modify_amount_and_percent_decimal_for_package cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
