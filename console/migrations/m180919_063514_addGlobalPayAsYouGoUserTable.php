<?php

use yii\db\Schema;
use yii\db\Migration;

class m180919_063514_addGlobalPayAsYouGoUserTable extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'per_lead_cost_currency', $this->string());
    }

    public function down()
    {
        echo "m180919_063514_addGlobalPayAsYouGoUserTable cannot be reverted.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
