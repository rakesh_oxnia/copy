<?php

use yii\db\Schema;
use yii\db\Migration;

class m181027_033952_modify_single_day_charge_field_in_invoice_table extends Migration
{
    public function up()
    {
      $this->alterColumn('invoice', 'single_day_charge', $this->decimal(10,3));
    }

    public function down()
    {
        echo "m181027_033952_modify_single_day_charge_field_in_invoice_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
