<?php

use yii\db\Schema;
use yii\db\Migration;

class m181203_052820_add_new_fields_in_packages_table extends Migration
{
    public function up()
    {
        $this->addColumn('packages', 'show_previous_lead',$this->smallInteger()->defaultValue(0));
        $this->addColumn('packages', 'package_creation_date',$this->string());
        $this->addColumn('packages', 'custom_activation',$this->smallInteger()->defaultValue(0));
    }

    public function down()
    {
        echo "m181203_052820_add_new_fields_in_packages_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
