<?php

use yii\db\Schema;
use yii\db\Migration;

class m180307_051005_create_table_forwarding_email_team extends Migration
{
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }
      
      $this->createTable('{{%forwarding_email_team}}', [
          'id' => $this->primaryKey(),
          'user_id' => $this->integer(),
          'user_group' => $this->integer(),
          'name' =>  $this->string(),

          'created_at' => $this->integer(),
          'updated_at' => $this->integer(),
      ], $tableOptions);

      $this->addForeignKey('fk_forwarding_email_team_user', 'forwarding_email_team', 'user_id', 'user', 'id', 'SET NULL', 'SET NULL');
    }

    public function down()
    {
        $this->dropForeignKey('fk_forwarding_email_team_user' ,'forwarding_email_team');
        $this->dropTable('forwarding_email_team');
    }
}
