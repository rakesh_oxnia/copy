<?php

use yii\db\Schema;
use yii\db\Migration;

class m180912_092352_addRequiredStatustocustomfields extends Migration
{
    public function up()
    {
        $this->addColumn('custom_leads_fields', 'required',$this->smallInteger(1)->defaultValue(0));
        $this->dropColumn('custom_leads_fields', 'order');
        $this->dropColumn('custom_leads_fields_option', 'order');
    }

    public function down()
    {
        echo "m180912_092352_addRequiredStatustocustomfields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
