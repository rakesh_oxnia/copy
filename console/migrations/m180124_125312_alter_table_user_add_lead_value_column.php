<?php

use yii\db\Schema;
use yii\db\Migration;

class m180124_125312_alter_table_user_add_lead_value_column extends Migration
{
    public function up()
    {
      $this->addColumn('user', 'average_lead_value', 'float AFTER `parent_id`');
    }

    public function down()
    {

    }

}
