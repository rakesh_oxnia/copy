<?php

use yii\db\Schema;
use yii\db\Migration;

class m180709_090922_create_invoice_table extends Migration
{
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%invoice}}', [
          'id' => $this->primaryKey(),
          'user_id' =>  $this->integer(),
          'advanced_payment'=>$this->integer(),
          'package_id' => $this->string(),
          'stripe_payment_id' => $this->string(),
          'allocated_lead'=>$this->integer(),
          'extra_per_lead_amount' => $this->integer(),
          'extra_lead_consumed' => $this->integer(),
          'advance_payment_status' => $this->smallInteger(),
          'transaction_currency'=>$this->string(),
          'total_amount'=>$this->integer(),
          'invoice_status' => $this->smallInteger(),
          'automatic_charge_status' => $this->smallInteger(),
          'created_at' => $this->integer(),
          'update_at' => $this->integer()->defaultValue(null),
          'billing_date'=>$this->integer(),
          'payment_date'=>$this->integer(),
      ], $tableOptions);

      $this->addForeignKey('fk_invoice_user', 'invoice', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        echo "m180709_090922_create_invoice_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
