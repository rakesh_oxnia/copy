<?php

use yii\db\Schema;
use yii\db\Migration;

class m190319_071536_change_is_lead_column_in_leads_table extends Migration
{
    public function up()
    {
        $this->alterColumn('leads', 'is_lead', $this->smallInteger()->defaultValue(0));
    }

    public function down()
    {
        echo "m190319_071536_change_is_lead_column_in_leads_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
