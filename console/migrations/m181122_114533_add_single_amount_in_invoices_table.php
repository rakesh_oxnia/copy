<?php

use yii\db\Schema;
use yii\db\Migration;

class m181122_114533_add_single_amount_in_invoices_table extends Migration
{
    public function up()
    {
        $this->addColumn('invoices', 'single_amount',$this->decimal(10,2));
    }

    public function down()
    {
        echo "m181122_114533_add_single_amount_in_invoices_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
