<?php

use yii\db\Schema;
use yii\db\Migration;

class m181201_062601_modify_amount_column_in_packages_table extends Migration
{
    public function up()
    {
        $this->alterColumn('packages', 'amount', $this->decimal(10,2));
    }

    public function down()
    {
        echo "m181201_062601_modify_amount_column_in_packages_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
