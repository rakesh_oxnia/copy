<?php

use yii\db\Schema;
use yii\db\Migration;

class m180906_063241_addCustomLeadsFields extends Migration
{
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%custom_leads_fields}}', [
          'id' => $this->primaryKey(),
          'user_id' =>  $this->integer(),
          'type'=> $this->string(),
          'label'=> $this->string(),
          'created_at' => $this->integer(),
          'updated_at' => $this->integer(),
      ], $tableOptions);

      $this->addForeignKey('fk_custom_leads_fields_user', 'custom_leads_fields', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        echo "m180906_063241_addLeadSenderForm cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
