<?php

use yii\db\Schema;
use yii\db\Migration;

class m180928_060449_create_zapier_logs_table extends Migration
{
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%zapier_logs}}', [
          'id' => $this->primaryKey(),
          'lead_id' =>  $this->integer(),
          'chat_id'=>$this->integer(),
          'created_at' => $this->integer(),
          'update_at' => $this->integer()->defaultValue(null)
      ], $tableOptions);

    }

    public function down()
    {
        echo "m180928_060449_create_zapier_logs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
