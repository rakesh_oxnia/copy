<?php

use yii\db\Schema;
use yii\db\Migration;

class m180611_070807_addcolumnDifferentEmailtonotification extends Migration
{
    public function up()
    {
       $this->addColumn('notification', 'different_email', $this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {
        echo "m180611_070807_addcolumnDifferentEmailtonotification cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
