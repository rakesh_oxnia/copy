<?php

use yii\db\Schema;
use yii\db\Migration;

class m180911_090845_addCustomFielsInLeads extends Migration
{
    public function up()
    {
       $this->addColumn('leads', 'custom_fields',$this->text());
    }

    public function down()
    {
        echo "m180911_090845_addCustomFielsInLeads cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
