<?php

use yii\db\Schema;
use yii\db\Migration;

class m180131_061537_create_table__package_user extends Migration
{
    public function up()
    {
      $this->insert('packages', ['package_name' => 'Custom', 'number_of_leads' => 0, 'number_of_chats' => 0, 'amount' => 0, 'validity' => 0]);

      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%package_user}}', [
          'id' => $this->primaryKey(),
          'user_id' =>  $this->integer(),
          'number_of_leads' => $this->integer(),
          'number_of_chats' => $this->integer(),
          'amount' => $this->float(),
          'validity' => $this->integer(),

          'created_at' => $this->integer(),
          'updated_at' => $this->integer(),
      ], $tableOptions);

      $this->addForeignKey('fk_package_user_user', 'package_user', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
      $this->dropForeignKey('fk_package_user_user', 'package_user');
      $this->dropTable('package_user');
    }
}
