<?php

use yii\db\Schema;
use yii\db\Migration;

class m181016_124630_modify_status_field_in_leads extends Migration
{
    public function up()
    {
      $this->alterColumn('leads', 'status', "ENUM('approved', 'rejected', 'pending', 'in-progress')");
    }

    public function down()
    {
        echo "m181016_124630_modify_status_field_in_leads cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
