<?php

use yii\db\Schema;
use yii\db\Migration;

class m180628_093717_add_USD__interval_strip_product_id_to_packages_table extends Migration
{
    public function up()
    {
      $this->addColumn('packages', 'currency', $this->string());
      $this->addColumn('packages', 'interval', $this->string());
      $this->addColumn('packages', 'stripe_product_id', $this->text());
      $this->addColumn('packages', 'created_at', $this->string());
      $this->dropColumn('packages', 'number_of_chats');
      $this->dropColumn('packages', 'validity');
    }

    public function down()
    {
        echo "m180628_093717_add_USD__interval_strip_product_id_to_packages_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
