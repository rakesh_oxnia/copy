<?php

use yii\db\Schema;
use yii\db\Migration;

class m181011_054617_rename_update_at_field_in_zapier_logs_table extends Migration
{
    public function up()
    {
        $this->renameColumn('zapier_logs', 'update_at', 'updated_at');
    }

    public function down()
    {
        echo "m181011_054617_rename_update_at_field_in_zapier_logs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
