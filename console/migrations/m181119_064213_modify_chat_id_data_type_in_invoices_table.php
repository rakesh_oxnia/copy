<?php

use yii\db\Schema;
use yii\db\Migration;

class m181119_064213_modify_chat_id_data_type_in_invoices_table extends Migration
{
    public function up()
    {
        $this->alterColumn('invoices', 'chat_id', $this->text());
    }

    public function down()
    {
        echo "m181119_064213_modify_chat_id_data_type_in_invoices_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
