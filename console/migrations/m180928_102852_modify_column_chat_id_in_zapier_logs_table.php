<?php

use yii\db\Schema;
use yii\db\Migration;

class m180928_102852_modify_column_chat_id_in_zapier_logs_table extends Migration
{
    public function up()
    {
        $this->alterColumn('zapier_logs', 'chat_id', $this->string());
    }

    public function down()
    {
        echo "m180928_102852_modify_column_chat_id_in_zapier_logs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
