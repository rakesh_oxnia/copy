<?php

use yii\db\Schema;
use yii\db\Migration;

class m180717_115604_addColumnTopackages extends Migration
{
    public function up()
    {
        $this->addColumn('packages', 'custom_status', $this->smallInteger()->defaultValue(0));
    }

    public function down()
    {
        echo "m180717_115604_addColumnTopackages cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
