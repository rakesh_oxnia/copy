<?php

use yii\db\Schema;
use yii\db\Migration;

class m181030_070949_create_stripe_logs_table extends Migration
{
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%stripe_logs}}', [
          'id' => $this->primaryKey(),
          'stripe_token' =>  $this->string(),
          'amount' =>  $this->integer(),
          'currency' =>  $this->string(),
          'user_id' => $this->integer(),
          'status' => $this->integer(),
          'log' => $this->text(),
          'created_at' => $this->integer(),
          'update_at' => $this->integer()->defaultValue(null)
      ], $tableOptions);
    }

    public function down()
    {
        echo "m181030_070949_create_stripe_logs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
