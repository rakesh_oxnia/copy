<?php

use yii\db\Schema;
use yii\db\Migration;

class m180816_071358_addColumnToInvoiceTable extends Migration
{
    public function up()
    {
      $this->addColumn('invoice', 'pending_invoice', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        echo "m180816_071358_addColumnToInvoiceTable cannot be reverted.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
