<?php

use yii\db\Schema;
use yii\db\Migration;

class m180824_102613_addColumnToInvoicePaymentStatus extends Migration
{
    public function up()
    {
        $this->addColumn('invoice', 'failed_invoice', $this->integer()->defaultValue(0));
        $this->addColumn('invoice', 'message', $this->text());
        $this->addColumn('invoice', 'charge', $this->text());
        $this->addColumn('invoice', 'decline_code', $this->text());
        $this->addColumn('invoice', 'doc_url', $this->text());
        $this->addColumn('invoice', 'code', $this->text());
        $this->addColumn('invoice', 'type', $this->text());
    }

    public function down()
    {
        echo "m180824_102613_addColumnToInvoicePaymentStatus cannot be reverted.\n";
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
