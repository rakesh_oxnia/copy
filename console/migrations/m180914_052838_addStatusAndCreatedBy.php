<?php

use yii\db\Schema;
use yii\db\Migration;

class m180914_052838_addStatusAndCreatedBy extends Migration
{
    public function up()
    {
      $this->addColumn('custom_leads_fields', 'created_by',$this->integer());
      $this->addColumn('custom_leads_fields', 'status',$this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {
        echo "m180914_052838_addStatusAndCreatedBy cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
