<?php

use yii\db\Schema;
use yii\db\Migration;

class m190131_041503_add_chat_data_column_in_zapier_request_logs_table extends Migration
{
    public function up()
    {
        $this->addColumn('zapier_request_logs', 'chat_data', $this->text());
    }

    public function down()
    {
        echo "m190131_041503_add_chat_data_column_in_zapier_request_logs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
