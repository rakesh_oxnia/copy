<?php

use yii\db\Schema;
use yii\db\Migration;

class m190301_112923_create_chatstats_table extends Migration
{
    public function up()
    {
        $this->createTable('chatstats', [
            'id' => $this->primaryKey(),
            'business_name' => $this->string(),
            'group' => $this->integer()->notNull(),
            'invites' =>  $this->integer()->notNull()->defaultValue(0),
            'chats' =>  $this->integer()->notNull()->defaultValue(0),                        
            'leads' =>  $this->integer()->notNull()->defaultValue(0),
            'greeting_chats' =>  $this->integer()->notNull()->defaultValue(0),
            'visitor_chats' =>  $this->integer()->notNull()->defaultValue(0),
            'record_date' => $this->date()->notNull()
        ]);
       
    }

    public function down()
    {
        $this->dropTable('chatstats');      
    }
}