<?php

use yii\db\Schema;
use yii\db\Migration;

class m181026_040924_add_new_fields_in_invoice_table extends Migration
{
    public function up()
    {
      $this->addColumn('invoice', 'subscription_id', $this->integer());
      $this->addColumn('invoice', 'cron_added',$this->smallInteger(1)->defaultValue(0));
      $this->addColumn('invoice', 'cron_updated',$this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {
        echo "m181026_040924_add_new_fields_in_invoice_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
