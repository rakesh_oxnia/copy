<?php

use yii\db\Schema;
use yii\db\Migration;

class m180914_071351_addSoftdeletetoCustomfields extends Migration
{
    public function up()
    {
        $this->addColumn('custom_leads_fields', 'soft_delete',$this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {
        echo "m180914_071351_addSoftdeletetoCustomfields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
