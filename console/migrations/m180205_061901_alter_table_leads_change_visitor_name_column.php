<?php

use yii\db\Schema;
use yii\db\Migration;

class m180205_061901_alter_table_leads_change_visitor_name_column extends Migration
{
    public function up()
    {
      $this->addColumn('leads', 'visitor_last_name', 'VARCHAR(255) AFTER `visitor_name`');
    }

    public function down()
    {

    }
}
