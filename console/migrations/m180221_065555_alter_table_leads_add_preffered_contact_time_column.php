<?php

use yii\db\Schema;
use yii\db\Migration;

class m180221_065555_alter_table_leads_add_preffered_contact_time_column extends Migration
{
    public function up()
    {
        $this->addColumn("leads", 'preferred_contact_time', 'VARCHAR(255) AFTER visitor_phone');
    }

    public function down()
    {
        
    }
}
