<?php

use yii\db\Schema;
use yii\db\Migration;

class m180716_120708_addColumnToInvoice extends Migration
{
    public function up()
    {
         $this->addColumn('invoice', 'gst_percent', $this->integer());
         $this->addColumn('invoice', 'gst_charge', $this->integer());
    }

    public function down()
    {
        echo "m180716_120708_addColumnToInvoice cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
