<?php

use yii\db\Schema;
use yii\db\Migration;

class m181105_050150_modify_extra_lead_price_field_in_invoice_table extends Migration
{
    public function up()
    {
        $this->alterColumn('invoice', 'extra_per_lead_amount', $this->decimal(10,2));
    }

    public function down()
    {
        echo "m181105_050150_modify_extra_lead_price_field_in_invoice_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
