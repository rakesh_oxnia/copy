<?php

use yii\db\Schema;
use yii\db\Migration;

class m180201_102226_create_table_lead_seen_alter_table_leads_new_lead_flag extends Migration
{
    public function up()
    {
      // alter table leads, add column(new_lead)
      $this->addColumn('leads', 'new_lead',  $this->smallInteger(1)->defaultValue(0));

      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%lead_seen}}', [
          'id' => $this->primaryKey(),
          'user_id' =>  $this->integer(),
          'leads_id' =>  $this->integer(),

          'created_at' => $this->integer(),
          'updated_at' => $this->integer(),
      ], $tableOptions);

      $this->addForeignKey('fk_lead_seen_user', 'lead_seen', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
      $this->addForeignKey('fk_lead_seen_leads', 'lead_seen', 'leads_id', 'leads', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
      $this->dropForeignKey('fk_lead_seen_leads', 'lead_seen');
      $this->dropForeignKey('fk_lead_seen_user', 'lead_seen');
      $this->dropTable('lead_seen');
    }
}
