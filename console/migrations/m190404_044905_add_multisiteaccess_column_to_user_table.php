<?php

use yii\db\Schema;
use yii\db\Migration;

class m190404_044905_add_multisiteaccess_column_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'multisiteaccess', $this->boolean());
    }

    public function down()
    {
    
        $this->dropColumn('user', 'multisiteaccess');
    }
}
