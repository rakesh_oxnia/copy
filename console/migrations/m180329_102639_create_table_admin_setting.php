<?php

use yii\db\Schema;
use yii\db\Migration;

class m180329_102639_create_table_admin_setting extends Migration
{
    public function safeUp()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%admin_setting}}', [
          'id' => $this->primaryKey(),
          'manager_email' => $this->string(),
          'login_iframe_tag' => $this->text(),
          'login_bg_color' => $this->string(),
          'help_me_iframe_tag' => $this->text(),
      ], $tableOptions);

      $this->insert('admin_setting',[
                        'id' => 1,
                        'manager_email' => 'pal@oxnia.com',
                        'login_iframe_tag' => '',
                        'login_bg_color' => '',
                        'help_me_iframe_tag' => '',
      ]);
    }

    public function safeDown()
    {

    }
}
