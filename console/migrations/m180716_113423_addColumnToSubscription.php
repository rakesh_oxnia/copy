<?php

use yii\db\Schema;
use yii\db\Migration;

class m180716_113423_addColumnToSubscription extends Migration
{
    public function up()
    {
         $this->addColumn('subscription', 'gst_status', $this->smallInteger()->defaultValue(0));
         $this->addColumn('subscription', 'gst_percent', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        echo "m180716_113423_addColumnToSubscription cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
