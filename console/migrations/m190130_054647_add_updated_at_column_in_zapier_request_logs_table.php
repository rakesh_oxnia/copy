<?php

use yii\db\Schema;
use yii\db\Migration;

class m190130_054647_add_updated_at_column_in_zapier_request_logs_table extends Migration
{
    public function up()
    {
        $this->addColumn('zapier_request_logs', 'updated_at', $this->integer()->defaultValue(null));
    }

    public function down()
    {
        echo "m190130_054647_add_updated_at_column_in_zapier_request_logs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
