<?php

use yii\db\Schema;
use yii\db\Migration;

class m190130_040228_create_zapier_pull_request_log extends Migration
{
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%zapier_request_logs}}', [
          'id' => $this->primaryKey(),
          'type' =>  $this->string(),
          'api_key'=>$this->string(),
          'username'=>$this->string(),
          'password'=>$this->string(),
          'authenticated'=>$this->smallInteger(1)->defaultValue(0),
          'user_group' => $this->integer()->defaultValue(0),
          'created_at' => $this->integer()
      ], $tableOptions);
    }

    public function down()
    {
        echo "m190130_040228_create_zapier_pull_request_log cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
