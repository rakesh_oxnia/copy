<?php

use yii\db\Schema;
use yii\db\Migration;

class m180202_054639_alter_table_user_category_add_parent_id_column extends Migration
{
    public function up()
    {
      $this->addColumn('user_category', 'parent_id', 'INTEGER(11) AFTER `user_id`');
      $this->addForeignKey('fk_user_category_parent', 'user_category', 'parent_id', 'user_category', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
      $this->dropForeignKey('fk_user_category_parent', 'user_category');
      $this->dropColumn('user_category', 'parent_id');
    }
}
