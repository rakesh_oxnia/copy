<?php

use yii\db\Schema;
use yii\db\Migration;

class m180315_101535_create_table_chats extends Migration
{
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $chatMessageTableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_bin ENGINE=InnoDB';
      }

      $this->createTable('{{%chat}}', [
          'id' => $this->primaryKey(),
          'chat_id' => $this->string(),
          'name' => $this->string(),
          'email' => $this->string(),
          'city' => $this->string(),
          'region' => $this->string(),
          'country' => $this->string(),
          'agent_email' => $this->string(),
          'agent_name' => $this->string(),

          'type' => $this->smallInteger(1),
          'rate' => $this->string(),
          'duration' => $this->integer(),
          'chat_start_url' => $this->text(),
          'user_group' => $this->integer(),
          'tags' => $this->string(),

          'is_lead' => $this->smallInteger(1),

          'started_timestamp' => $this->integer(),
          'ended_timestamp' => $this->integer(),
      ], $tableOptions);

      $this->createTable('{{%chat_message}}', [
          'id' => $this->primaryKey(),
          'chat_id' => $this->integer(),

          'author_name' => $this->string(),
          'text' => $this->text(),
          'date' => $this->string(),
          'timestamp' => $this->integer(),
          'user_type' => $this->string(),
      ], $chatMessageTableOptions);

      $this->addForeignKey('fk_chat_message_chat', 'chat_message', 'chat_id', 'chat', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
      $this->dropForeignKey('fk_chat_message_chat' ,'chat_message');
      $this->dropTable('chat_message');
      $this->dropTable('chat');
    }

}
