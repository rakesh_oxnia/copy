<?php

use yii\db\Schema;
use yii\db\Migration;

class m180827_095715_addColumnToUser extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'payment_status', $this->integer()->defaultValue(0));
        $this->addColumn('user', 'disable_payment_status', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        echo "m180827_095715_addColumnToUser cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
