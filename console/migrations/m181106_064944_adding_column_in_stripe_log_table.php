<?php

use yii\db\Schema;
use yii\db\Migration;

class m181106_064944_adding_column_in_stripe_log_table extends Migration
{
    public function up()
    {
      $this->addColumn('stripe_logs', 'error_type',$this->string());
    }

    public function down()
    {
        echo "m181106_064944_adding_column_in_stripe_log_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
