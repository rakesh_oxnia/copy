<?php

use yii\db\Schema;
use yii\db\Migration;

class m181027_034822_modify_columns_in_invoice_table extends Migration
{
    public function up()
    {
      $this->alterColumn('invoice', 'single_day_charge', $this->decimal(10,2));
      $this->alterColumn('invoice', 'remaining_days_total_amount', $this->decimal(10,2));
    }

    public function down()
    {
        echo "m181027_034822_modify_columns_in_invoice_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
