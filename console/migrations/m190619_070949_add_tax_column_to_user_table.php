<?php

use yii\db\Migration;

/**
 * Handles adding tax to table `{{%user}}`.
 */
class m190619_070949_add_tax_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'tax', $this->float());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'tax');
    }
}
