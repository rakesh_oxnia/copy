<?php

use yii\db\Schema;
use yii\db\Migration;

class m180208_063909_alter_table_leads_add_from_agent_app_column extends Migration
{
    public function up()
    {
      $this->addColumn('leads', 'from_agent_app',  $this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {

    }
}
