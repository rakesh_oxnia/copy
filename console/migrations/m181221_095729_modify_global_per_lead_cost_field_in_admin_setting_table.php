<?php

use yii\db\Schema;
use yii\db\Migration;

class m181221_095729_modify_global_per_lead_cost_field_in_admin_setting_table extends Migration
{
    public function up()
    {
        $this->alterColumn('admin_setting', 'global_per_lead_cost', $this->decimal(10,2)->defaultValue(0));
    }

    public function down()
    {
        echo "m181221_095729_modify_global_per_lead_cost_field_in_admin_setting_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
