<?php

use yii\db\Schema;
use yii\db\Migration;

class m180628_103801_add_stripe_plan_id extends Migration
{
    public function up()
    {
        $this->addColumn('packages', 'stripeEmail_plan_id', $this->text());
    }

    public function down()
    {
        echo "m180628_103801_add_stripe_plan_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
