<?php

use yii\db\Schema;
use yii\db\Migration;

class m180129_061420_alter_table_user_add_average_lead_cost extends Migration
{
    public function up()
    {
      $this->addColumn('user', 'average_lead_cost', 'float AFTER `parent_id`');
    }

    public function down()
    {

    }
}
