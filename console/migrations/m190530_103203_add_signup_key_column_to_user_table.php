<?php

use yii\db\Migration;

/**
 * Handles adding signup_key to table `{{%user}}`.
 */
class m190530_103203_add_signup_key_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'signup_key', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'signup_key');
    }
}
