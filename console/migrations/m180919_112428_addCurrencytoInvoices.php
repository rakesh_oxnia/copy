<?php

use yii\db\Schema;
use yii\db\Migration;

class m180919_112428_addCurrencytoInvoices extends Migration
{
    public function up()
    {
         $this->addColumn('invoices', 'currency', $this->string());
    }

    public function down()
    {
        echo "m180919_112428_addCurrencytoInvoices cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
