<?php

use yii\db\Schema;
use yii\db\Migration;

class m190403_120627_add_outlet_column_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'outlet', $this->integer());
    }

    public function down()
    {
    
        $this->dropColumn('user', 'outlet');
    }
}
