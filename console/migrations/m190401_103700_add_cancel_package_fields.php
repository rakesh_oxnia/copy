<?php

use yii\db\Schema;
use yii\db\Migration;

class m190401_103700_add_cancel_package_fields extends Migration
{
    public function up()
    {
        $this->addColumn('subscription', 'is_cancelled', $this->smallinteger()->defaultValue(0));
        $this->addColumn('subscription', 'cancel_date', $this->integer()->defaultValue(0));
        $this->addColumn('subscription', 'cancel_reason', $this->text());
        $this->addColumn('invoice', 'is_cancelled', $this->smallinteger()->defaultValue(0));
        $this->addColumn('packages', 'is_cancelled', $this->smallinteger()->defaultValue(0));
    }

    public function down()
    {
        echo "m190401_103700_add_cancel_package_fields cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
