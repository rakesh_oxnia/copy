<?php

use yii\db\Schema;
use yii\db\Migration;

class m180217_070136_alter_table_user_add_iframe_tag extends Migration
{
    public function up()
    {
        $this->addColumn("user", 'iframe_tag', 'TEXT');
    }

    public function down()
    {
        
    }
}
