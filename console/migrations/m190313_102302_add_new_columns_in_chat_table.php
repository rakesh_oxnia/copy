<?php

use yii\db\Schema;
use yii\db\Migration;

class m190313_102302_add_new_columns_in_chat_table extends Migration
{
    public function up()
    {
        $this->addColumn('chat', 'country_code',$this->string()->defaultValue(null));
        $this->addColumn('chat', 'timezone',$this->string()->defaultValue(null));
        $this->addColumn('chat', 'referrer',$this->string()->defaultValue(null));
    }

    public function down()
    {
        echo "m190313_102302_add_new_columns_in_chat_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
