<?php

use yii\db\Schema;
use yii\db\Migration;

class m180928_063410_add_data_column_to_zapier_logs_table extends Migration
{
    public function up()
    {
        $this->addColumn('zapier_logs', 'data', $this->text());
    }

    public function down()
    {
        echo "m180928_063410_add_data_column_to_zapier_logs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
