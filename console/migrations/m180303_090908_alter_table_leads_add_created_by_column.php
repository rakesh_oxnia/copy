<?php

use yii\db\Schema;
use yii\db\Migration;

class m180303_090908_alter_table_leads_add_created_by_column extends Migration
{
    public function up()
    {
      $this->addColumn('leads', 'created_by', 'VARCHAR(255) AFTER c_time');
    }

    public function down()
    {
        $this->dropColumn('created_by', 'leads');
    }
}
