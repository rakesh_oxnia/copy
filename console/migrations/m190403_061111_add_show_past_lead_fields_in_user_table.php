<?php

use yii\db\Schema;
use yii\db\Migration;

class m190403_061111_add_show_past_lead_fields_in_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'show_past_lead', $this->smallinteger()->defaultValue(0));
        $this->addColumn('user', 'show_lead_from', $this->integer()->defaultValue(0));
        $this->addColumn('user', 'show_lead_to', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        echo "m190403_061111_add_show_past_lead_fields_in_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
