<?php

use yii\db\Schema;
use yii\db\Migration;

class m180227_034733_alter_table_user_add_login_background_color_column extends Migration
{
    public function up()
    {
      $this->addColumn("user", 'login_bg_color', 'VARCHAR(255) AFTER `iframe_tag`');
    }

    public function down()
    {
      
    }
}
