<?php

use yii\db\Schema;
use yii\db\Migration;

class m180402_073620_alter_user_table_add_email_verified_column extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
      $this->addColumn('user', 'email_verified', $this->smallInteger()->defaultValue(1));
    }

    public function safeDown()
    {
    }

}
