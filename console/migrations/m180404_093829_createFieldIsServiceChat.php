<?php

use yii\db\Schema;
use yii\db\Migration;

class m180404_093829_createFieldIsServiceChat extends Migration
{
  // Use safeUp/safeDown to run migration code within a transaction
  public function safeUp()
  {
    $this->addColumn('leads', 'is_service_chat', $this->smallInteger()->defaultValue(0));
  }

  public function safeDown()
  {
  }
}
