<?php

use yii\db\Schema;
use yii\db\Migration;

class m190206_050652_rename_column_updated_requested_id_in_zapier_logs_table extends Migration
{
    public function up()
    {
        $this->renameColumn('zapier_logs', 'updated_requested_id', 'updated_request_id');
    }

    public function down()
    {
        echo "m190206_050652_rename_column_updated_requested_id_in_zapier_logs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
