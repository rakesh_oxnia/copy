<?php

use yii\db\Schema;
use yii\db\Migration;

class m180308_065850_alter_table_email_log_add_web_url_column extends Migration
{
    public function up()
    {
      $this->addColumn('email_log', 'web_url', 'VARCHAR(255) AFTER `chat_id`');
    }

    public function down()
    {
        $this->dropColumn('web_url', 'email_log');
    }
}
