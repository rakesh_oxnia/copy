<?php

use yii\db\Schema;
use yii\db\Migration;

class m180131_064055_create_table_custom_package_range extends Migration
{
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%custom_package_range}}', [
          'id' => $this->primaryKey(),
          'range_start' => $this->integer(),
          'range_end' => $this->integer(),
          'amount' => $this->float(),

          'created_at' => $this->integer(),
          'updated_at' => $this->integer(),
      ], $tableOptions);
    }

    public function down()
    {
      $this->dropTable('custom_package_range');
    }
}
