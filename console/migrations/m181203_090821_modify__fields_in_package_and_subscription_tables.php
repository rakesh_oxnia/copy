<?php

use yii\db\Schema;
use yii\db\Migration;

class m181203_090821_modify__fields_in_package_and_subscription_tables extends Migration
{
    public function up()
    {
      $this->dropColumn('packages', 'show_previous_lead');
      $this->dropColumn('packages', 'package_creation_date');
      $this->dropColumn('packages', 'custom_activation');

      $this->addColumn('subscription', 'show_previous_lead',$this->smallInteger()->defaultValue(0));
      $this->addColumn('subscription', 'package_creation_date',$this->string());
      $this->addColumn('subscription', 'custom_activation',$this->smallInteger()->defaultValue(0));
    }

    public function down()
    {
        echo "m181203_090821_modify__fields_in_package_and_subscription_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
