<?php

use yii\db\Schema;
use yii\db\Migration;

class m190206_050222_add_updated_requested_id_column_in_zapier_logs_table extends Migration
{
    public function up()
    {
        $this->addColumn('zapier_logs', 'updated_requested_id', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        echo "m190206_050222_add_updated_requested_id_column_in_zapier_logs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
