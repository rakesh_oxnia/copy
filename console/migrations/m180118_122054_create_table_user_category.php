<?php

use yii\db\Schema;
use yii\db\Migration;

class m180118_122054_create_table_user_category extends Migration
{
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }

      $this->createTable('{{%user_category}}', [
          'id' => $this->primaryKey(),
          'user_id' =>  $this->integer(),
          'category_name' => $this->string(),

          'created_at' => $this->integer(),
          'updated_at' => $this->integer(),
      ], $tableOptions);

      $this->addForeignKey('fk_user_category_user', 'user_category', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_user_category_user', 'user_category');
        $this->dropTable('user_category');
    }
}
