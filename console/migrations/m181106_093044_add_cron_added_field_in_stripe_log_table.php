<?php

use yii\db\Schema;
use yii\db\Migration;

class m181106_093044_add_cron_added_field_in_stripe_log_table extends Migration
{
    public function up()
    {
        $this->addColumn('stripe_logs', 'cron_added',$this->integer());
    }

    public function down()
    {
        echo "m181106_093044_add_cron_added_field_in_stripe_log_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
