<?php

use yii\db\Schema;
use yii\db\Migration;

class m180518_052033_create_notification_table extends Migration
{
    public function up()
    {
      $this->createTable('{{%notification}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->string(),
            'notify_lead' => $this->smallInteger(1),
            'notify_chat' => $this->smallInteger(1),
            'notify_email' => $this->text(),
        ]);
    }

    public function down()
    {
        echo "m180518_052033_create_notification_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
