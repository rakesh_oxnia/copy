<?php

use yii\db\Schema;
use yii\db\Migration;

class m180907_075932_addPerLeadCharge extends Migration
{
    public function up()
    {
      $this->addColumn('user', 'per_lead_cost',$this->integer());
    }

    public function down()
    {
        echo "m180907_075932_addPerLeadCharge cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
