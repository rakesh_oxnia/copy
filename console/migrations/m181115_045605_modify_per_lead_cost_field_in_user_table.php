<?php

use yii\db\Schema;
use yii\db\Migration;

class m181115_045605_modify_per_lead_cost_field_in_user_table extends Migration
{
    public function up()
    {
      $this->alterColumn('user', 'per_lead_cost', $this->decimal(10,2));
    }

    public function down()
    {
        echo "m181115_045605_modify_per_lead_cost_field_in_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
