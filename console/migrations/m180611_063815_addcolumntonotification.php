<?php

use yii\db\Schema;
use yii\db\Migration;

class m180611_063815_addcolumntonotification extends Migration
{
    public function up()
    {
      $this->addColumn('notification', 'notify_lead_email', $this->text());
      $this->addColumn('notification', 'notify_chat_email', $this->text());
    }

    public function down()
    {
        echo "m180611_063815_addcolumntonotification cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
