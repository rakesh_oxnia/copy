<?php

use yii\db\Schema;
use yii\db\Migration;

class m190201_054920_add_user_group_column_in_zapier_logs_table extends Migration
{
    public function up()
    {
        $this->addColumn('zapier_logs', 'user_group', $this->integer()->defaultValue(0));
    }

    public function down()
    {
        echo "m190201_054920_add_user_group_column_in_zapier_logs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
