<?php

use yii\db\Schema;
use yii\db\Migration;

class m180316_021338_alter_table_chat_add_leads_id_column extends Migration
{
    public function safeUp()
    {
      $this->addColumn('chat', 'leads_id', 'INTEGER(11) AFTER `chat_id`');
      $this->addForeignKey('fk_chat_leads', 'chat', 'leads_id', 'leads', 'id', 'SET NULL', 'SET NULL');
    }

    public function safeDown()
    {
      $this->dropForeignKey('fk_chat_leads', 'chat');
      $this->dropColumn('leads_id', 'chat');
    }
}
