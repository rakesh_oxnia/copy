<?php

use yii\db\Schema;
use yii\db\Migration;

class m181011_050922_add_new_fields_in_zapier_logs_table extends Migration
{
    public function up()
    {
        $this->addColumn('zapier_logs', 'email', $this->string());
        $this->addColumn('zapier_logs', 'type', "ENUM('lead', 'service_chat')");
    }

    public function down()
    {
        echo "m181011_050922_add_new_fields_in_zapier_logs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
