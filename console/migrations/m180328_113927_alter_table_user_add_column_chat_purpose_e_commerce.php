<?php

use yii\db\Schema;
use yii\db\Migration;

class m180328_113927_alter_table_user_add_column_chat_purpose_e_commerce extends Migration
{
    public function up()
    {
      $this->addColumn("user", 'chat_purpose_e_commerce',   $this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {

    }
}
