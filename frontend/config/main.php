<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

use \yii\web\Request;
//$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());
//$baseUrl = 'chatapp/frontend/web';
return [
    'id' => 'app-frontend',
    'name' => 'Chat Metrics',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
	'timezone' => 'Australia/Melbourne',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [

		'request' => [
            //'baseUrl' => $baseUrl,
        ],

        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
// 		'stripe' => [
// 			'class' => 'ruskid\stripe\Stripe',
// 			'publicKey' => "pk_test_b3k2SOT8WW4yPPqKR7vOKENi",
// 			'privateKey' => "sk_test_MgHgUmHaPVW9JoLUKG6hzsaP",
           'stripe' => [
    			'class' => 'ruskid\stripe\Stripe',
    			'publicKey' => $params['stripePublicKey'],
    			'privateKey' => $params['stripeKey'],
		             ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        /*'errorHandler' => [
            'errorAction' => 'site/error',
        ],*/
		 /*
		'urlManager' => [
			'baseUrl' => $baseUrl,
			'class' => 'yii\web\UrlManager',
			'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
			//'enableStrictParsing' => true,
			 'rules' => array(
				'http://<id:\w+>.chatapptest.oxnia.com/user/index' => 'user/index',
                'http://<id:\w+>.chatapptest.oxnia.com/site/index' => 'site/index',
                'http://<id:\w+>.chatapptest.oxnia.com/site/login' => 'site/login',
			),
		],
     */
    ],
    'params' => $params,
	'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['127.0.0.1', '::1', '182.75.35.82', '192.168.32.254','182.72.254.246', '139.59.23.34'],
            //'password' => '123456'
        ]
    ],
];
