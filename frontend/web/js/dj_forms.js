var delay = (function(){
  var timer = 0;
  return function(callback, ms){
  clearTimeout (timer);
  timer = setTimeout(callback, ms);
 };
})();

function submit_me(){
    this_action = $("#w0").attr('action');
    // get the form data
    // there are many ways to get this data using jQuery (you can use the class or id also)
    var formData = $("#w0").serialize();
    //console.log(formData)
    // process the form
    $.ajax({
        type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
        url         : this_action, // the url where we want to POST
        data        : formData, // our data object
        dataType    : 'html' // what type of data do we expect back from the server
        //encode      : true
    })
    // using the done promise callback
    .done(function(data) {
        $(".imsaved").show()
        delay(function(){
            $(".imsaved").hide()
        }, 1000 );
    });
}
function i_d(d){
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < d; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}
function replace_subsection_html(html_, sid, tid){
    var re = new RegExp(sid,"g");
    newstr = html_.replace(re, tid);
    return newstr;
}
function replace_field_html(html_, sid, tid){    
    trg_id = tid.split('_');
    src_id = sid.split('_');
    var sec0 = src_id[0] //source section // hab50
    var sub0 = src_id[1] //source subsection // vju2zo
    var sec1 = trg_id[0] //target section // hab50
    var sub1 = trg_id[1] //target subsection // 3rsrf6    
    var re = new RegExp(sub0,"g");
    var re2 = new RegExp(sec0,"g");
    newstr = html_.replace(re, sub1).replace(re2, sec1);    
    return newstr;
}
function key_up(){
    $("input").keyup(function(w, e){
        var me_val = $(this).val()
        var me_ = this
        delay(function(){
            submit_me();
            $(me_).attr('value', me_val)
        }, 1000 );
    })
}
var sid_ = '';
var html_ = '';
function drag_drop() {
    $(".list-group").sortable({
        connectWith: ".list-group",
        start:function(event, ui){
            sid_ = event.target.id;            
        },
        receive:function(event, ui){
            var tid_ = $(this).parent('ul').context.id;
            var html_ = $(this)[0].innerHTML;
            var upd_dt = replace_field_html($.trim(html_), sid_, tid_);
            $(this)[0].innerHTML = upd_dt;
            key_up();
            drag_drop();
            submit_me();
        }
    });    
    $("section ul.col-md-10").sortable({
        connectWith: "section ul.col-md-10",        
        receive: function( event, ui ) {
            var sid_ = $(this).find('ul.list-group').attr('id').split('_')
            sid_ = sid_[0];
            var tid_ = $(this)[0].getAttribute("foo-class");
            var html_ = $(this)[0].innerHTML;
            var upd_dt = replace_subsection_html($.trim(html_), sid_, tid_)
            $(this)[0].innerHTML = upd_dt;
            key_up();
            drag_drop();
            submit_me();
        },
    });
    $(".all_sections").sortable({
        connectWith: ".all_sections",
        receive:function(event,ui){
            key_up();
            drag_drop();
            submit_me();
        }
    });
}
key_up();
drag_drop();
$("#my_dj_form").click(function(){
    submit_me();
    delay(function(){
        location.reload();    
    }, 1000 );
})
$(document).ready(function(){   
    /*--ADD DRAG & DROP SECTION--*/
    // get sections list
    function get_sections(){
        var all_sections = $("form#w0").find('section').length;
        //console.log("all_sections : "+all_sections)
        var sec_html = '';
        var length_ = parseInt(all_sections)+1;
        for (var i = 1; i < length_; i++) {            
            var sec = $(".section_"+i).val();
            sec_html +='<option value="'+sec+'">Section'+i+'</option>';
        };        
        $("#section_").html(sec_html)        
    }
    // get subsections list
    function get_subsections(sid){        
        var all_subs = $("#section"+sid).find('.subsec').length;        
        var length_ = parseInt(all_subs)+1;
        var sub_html = '';
        for (var i = 1; i < length_; i++) {
            ////console.log("#section"+sid+" .subsection_"+i)
            var sub = $("#section"+sid+" .subsection_"+i).val();
            ////console.log("this_subs : "+all_subs, 'sid : '+sid, 'i :'+i, 'length : '+length_)
            sub_html +='<option value="'+sub+'">Subsection'+i+'</option>';            
        };        
        $("#subsection_").html(sub_html)
    }
    var sec_id = $(".section_1").val();
    get_sections();
    get_subsections(sec_id);
    $("#section_").change(function(){
        sec_id = $(this).val();
        get_subsections(sec_id)  
    })
    /*--ADD MAIN SECTION--*/
    $(document).on('click', '.add_more', function(){
        var s_d = i_d(5) //section
        var u_d = i_d(6) //subsection
        var cc0 = $("form").find('section').length;
        var cc1 = parseInt(cc0)+1;
        //console.log(cc0)
        var section_html = '<section id="section'+s_d+'" class="section'+s_d+'">'+
            '<input type="hidden" class="section_'+cc1+'" value="'+s_d+'">'+
            '<div class="input-group">'+
                '<span class="input-group-addon">Section</span>'+
                '<input type="text" required class="form-control" name="section['+s_d+'][label]" placeholder="" />'+
                '<span class="input-group-addon"><span title = "Any Help for users about this section?" foo-id="section'+s_d+'" class="help_u glyphicon glyphicon-question-sign"></span></span>'+
                '<span class="input-group-addon"><span title = "Assign to which user?" foo-id = "section'+s_d+'" class="show_u glyphicon glyphicon-user"></span></span>'+
                '<span class="input-group-addon"><span title = "Want to make this section duplicate?" foo-id="section'+s_d+'" class="duplicate_u glyphicon glyphicon-duplicate"></span></span>'+
                '<span class="input-group-addon"><span title = "How many column you want in this section?" foo-id="section'+s_d+'" class="column_u glyphicon glyphicon-th"></span></span>'+
                '<span class="input-group-addon"><span title = "Want to delete?" foo-id="section'+s_d+'" class="dlt_sec glyphicon glyphicon-trash"></span></span>'+
            '</div>'+
            '<div class="hide_u column_u_section'+s_d+'">'+
                '<select class="form-control" name="section['+s_d+'][column]">'+
                    '<option value="0" >Select Column</option>'+
                    '<option value="1" >Column 1</option>'+
                    '<option value="2" >Column 2</option>'+
                    '<option value="3" >Column 3</option>'+
                    '<option value="4" >Column 4</option>'+
                '</select>'+
            '</div>'+
            '<div class="hide_u duplicate_u_section'+s_d+'">'+
                '<select class="form-control" name="section['+s_d+'][duplicate]">'+
                    '<option value="1">Yes</option>'+
                    '<option value="0" selected>No</option>'+
                '</select>'+
            '</div>'+
            '<div class="hide_u help_u_section'+s_d+'">'+
                '<input type="text" class="form-control" value="" placeholder="Help Text" name="section['+s_d+'][help]">'+
            '</div>'+
            '<div class="pull-right hide_u u_section'+s_d+'">'+
                '<table>'+
                    '<tr>'+
                        '<th></th>'+
                        '<th><span class="glyphicon glyphicon-eye-open"></span></th>'+
                        '<th><span class="glyphicon glyphicon-pencil"></span></th>'+
                    '</tr>'+
                    '<tbody>'+
                        '<tr>'+
                            '<th>CC</th>'+
                            '<td><input name="section['+s_d+'][uacl][cc][v]" type="checkbox"></td>'+
                            '<td><input name="section['+s_d+'][uacl][cc][e]" type="checkbox"></td>'+
                        '</tr>'+
                        '<tr>'+
                            '<th>CG</th>'+
                            '<td><input name="section['+s_d+'][uacl][cg][v]" type="checkbox"></td>'+
                            '<td><input name="section['+s_d+'][uacl][cg][e]" type="checkbox"></td>'+
                        '</tr>'+
                    '</tbody>'+
                '</table>'+
            '</div><!-- /.col-lg-6 --><br>'+
            '<ul foo-class="'+s_d+'" class="col-md-10 col-md-offset-1">'+
                '<div class="subsection'+u_d+' subsec">'+
                    '<input type="hidden" class="subsection_1" value="'+u_d+'">'+
                    '<div class="input-group">'+
                        '<span class="input-group-addon">Subsection</span>'+
                        '<input type="text" required class="form-control" name="section['+s_d+'][subsections]['+u_d+'][label]" placeholder="" />'+
                        '<span class="input-group-addon">'+
                            '<span foo-sec="section'+s_d+'" foo-sub-sec="subsection'+u_d+'" class="help_u_sb glyphicon glyphicon-question-sign"></span>'+
                        '</span>'+
                        '<span class="input-group-addon">'+
                            '<span foo-sec="section'+s_d+'" foo-sub-sec="subsection'+u_d+'" class="show_u_sb glyphicon glyphicon-user"></span>'+
                        '</span>'+
                        '<span class="input-group-addon">'+
                            '<span foo-sec="section'+s_d+'" foo-sub-sec="subsection'+u_d+'" class="duplicate_u_sb glyphicon glyphicon-duplicate"></span>'+
                        '</span>'+            
                        '<span class="input-group-addon">'+
                            '<span foo-sec="section'+s_d+'" foo-sub-sec="subsection'+u_d+'" class="column_u_sb glyphicon glyphicon-th"></span>'+
                        '</span>'+
                        ' <span class="input-group-addon">'+
                            ' <span foo-sec="section' + s_d + '" foo-sub-sec="subsection' + u_d + '" class="dlt_sub_sec glyphicon glyphicon-trash"></span>'+
                        ' </span>'+
                    ' </div>'+
                    ' <div class="hide_u column_u_section'+s_d+'_subsection'+u_d+'">'+
                        '<select class="form-control" name="section['+s_d+'][subsections]['+u_d+'][column]">'+
                            '<option value="0" >Select Column</option>'+
                            '<option value="1" >Column 1</option>'+
                            '<option value="2" >Column 2</option>'+
                            '<option value="3" >Column 3</option>'+
                            '<option value="4" >Column 4</option>'+
                        '</select>'+
                    ' </div>'+
                    '<div class="hide_u duplicate_u_section'+s_d+'_subsection'+u_d+'">'+
                        '<select class="form-control" name="section['+s_d+'][subsections]['+u_d+'][duplicate]">'+
                            '<option value="1">Yes</option>'+
                            '<option value="0" selected>No</option>'+
                        '</select>'+
                    '</div>'+
                    '<div class="hide_u help_u_sub section'+s_d+'_subsection'+u_d+'">'+
                        '<input type="text" class="form-control" value="" placeholder="Help Text" name="section['+s_d+'][subsections]['+u_d+'][help]">'+
                    '</div>'+
                    '<div class="pull-right hide_u hide_u_sb u_section'+s_d+'_subsection'+u_d+'">'+
                        '<table>'+
                            '<tr>'+
                                '<th></th>'+
                                '<th><span class="glyphicon glyphicon-eye-open"></span></th>'+
                                '<th><span class="glyphicon glyphicon-pencil"></span></th>'+
                            '</tr>'+
                            '<tbody>'+
                                '<tr>'+
                                    '<th>CC</th>'+
                                    '<td><input name="section['+s_d+'][subsections]['+u_d+'][uacl][cc][v]" type="checkbox"></td>'+
                                    '<td><input name="section['+s_d+'][subsections]['+u_d+'][uacl][cc][e]" type="checkbox"></td>'+
                                '</tr>'+
                                '<tr>'+
                                    '<th>CG</th>'+
                                    '<td><input name="section['+s_d+'][subsections]['+u_d+'][uacl][cg][v]" type="checkbox"></td>'+
                                    '<td><input name="section['+s_d+'][subsections]['+u_d+'][uacl][cg][e]" type="checkbox"></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '</table>'+
                    '</div><!-- /.col-lg-6 -->'+
                    '<ul id="'+s_d+'_'+u_d+'" subsection="subsection'+u_d+'" section="section'+s_d+'" class="list-group"></ul>'+
                '</div>'+
                '<button type="button" class="add_sub btn btn-danger">AddSub</button>'+
            '</ul>'+
            '<div class="clearfix"></div>'+
        '</section>';
        //console.log("section#section"+$(".section_"+cc0).val())
        $("section#section"+$(".section_"+cc0).val()).after(section_html);
        get_sections();
        key_up();
        drag_drop();
        submit_me();		
    })

    /*--ADD SUB SECTION--*/
    $(document).on('click', '.add_sub', function(){
        var prv_id = $(this).prev().find('ul.list-group').attr('id');
        //console.log("prv_id : "+prv_id)
        var sec_sub = prv_id.split('_');
        var length_ = $("#section"+sec_sub[0]).find('.subsec').length;
        length_ =  parseInt(length_)+1;
        //console.log("length_ : "+length_)
        var s_d = sec_sub[0]
        var u_d = i_d(6)
        var cc2 = parseInt(sec_sub[1]) + 1;
        var cc3 = $(this).parent('ul').attr('foo-class');   //section
        //console.log(cc2, cc3);
        var data_b = '<div class="subsection'+u_d+' subsec ui-sortable-handle">'+
            '<input type="hidden" class="subsection_'+length_+'" value="'+u_d+'">'+
            '<div class="input-group">'+
                '<span class="input-group-addon">Subsection</span>'+
                '<input type="text" required class="form-control" name="section['+s_d+'][subsections]['+u_d+'][label]" placeholder="" />'+
                '<span class="input-group-addon">'+
                    '<span foo-sec="section'+s_d+'" foo-sub-sec="subsection'+u_d+'" class="help_u_sb glyphicon glyphicon-question-sign"></span>'+
                '</span>'+
            '<span class="input-group-addon">'+
                '<span foo-sec="section'+s_d+'" foo-sub-sec="subsection'+u_d+'" class="show_u_sb glyphicon glyphicon-user"></span>'+
            '</span>'+
            '<span class="input-group-addon">'+
                '<span foo-sec="section'+s_d+'" foo-sub-sec="subsection'+u_d+'" class="duplicate_u_sb glyphicon glyphicon-duplicate"></span>'+
            '</span>'+            
            '<span class="input-group-addon">'+
                '<span foo-sec="section'+s_d+'" foo-sub-sec="subsection'+u_d+'" class="column_u_sb glyphicon glyphicon-th"></span>'+
            '</span>'+
            ' <span class="input-group-addon">'+
                ' <span foo-sec="section' + s_d + '" foo-sub-sec="subsection' + u_d + '" class="dlt_sub_sec glyphicon glyphicon-trash"></span>'+
            ' </span>'+
        ' </div>'+
        ' <div class="pull-right hide_u_sb u_section'+s_d+'_subsection'+u_d+'">'+
            ' <table>'+
                ' <tr>'+
                    ' <th></th>'+
                    ' <th><span class="glyphicon glyphicon-eye-open"></span></th>'+
                    ' <th><span class="glyphicon glyphicon-pencil"></span></th>'+
                ' </tr>'+
                ' <tbody>'+
                    ' <tr>'+
                        ' <th>CC</th>'+
                        ' <td><input name="section['+s_d+'][subsections]['+u_d+'][uacl][cc][v]" type="checkbox"></td>'+
                        ' <td><input name="section['+s_d+'][subsections]['+u_d+'][uacl][cc][e]" type="checkbox"></td>'+
                    ' </tr>'+
                    ' <tr>'+
                        ' <th>CG</th>'+
                            ' <td><input name="section['+s_d+'][subsections]['+u_d+'][uacl][cg][v]" type="checkbox"></td>'+
                            ' <td><input name="section['+s_d+'][subsections]['+u_d+'][uacl][cg][e]" type="checkbox"></td>'+
                    ' </tr>'+
                    ' </tbody>'+
                ' </table>'+
            ' </div><!-- /.col-lg-6 -->'+
            ' <div class="hide_u column_u_section'+s_d+'_subsection'+u_d+'">'+
                '<select class="form-control" name="section['+s_d+'][subsections]['+u_d+'][column]">'+
                    '<option value="0" >Select Column</option>'+
                    '<option value="1" >Column 1</option>'+
                    '<option value="2" >Column 2</option>'+
                    '<option value="3" >Column 3</option>'+
                    '<option value="4" >Column 4</option>'+
                '</select>'+
            ' </div>'+
            ' <div class="hide_u duplicate_u_section'+s_d+'_subsection'+u_d+'">'+
                ' <select class="form-control" name="section['+s_d+'][subsections]['+u_d+'][duplicate]">'+
                    ' <option value="1" selected="">Yes</option>'+
                    ' <option value="0" selected="selected">No</option>'+
                ' </select>'+
            ' </div>'+
            ' <div class="hide_u help_u_sub section'+s_d+'_subsection'+u_d+'">'+
                ' <input type="text" class="form-control" value="" placeholder="Help Text" name="section['+s_d+'][subsections]['+u_d+'][help]">'+
            ' </div>'+
            ' <ul data-draggable="target" id="'+s_d+'_'+u_d+'" subsection="subsection'+u_d+'" section="'+s_d+'" class="list-group"></ul>'+
        ' </div>';        
        $('.section'+cc3+' .add_sub').before(data_b);
        get_subsections(cc3);
        key_up();
        drag_drop();
        submit_me();
    });
        
    /*--DELETE SUBSECTION--*/
    /*--subsection options--*/
    $(document).on('click', '.dlt_sub_sec', function(){        
        var sec = $(this).attr('foo-sec');        
        var sub = $(this).attr('foo-sub-sec');        
        var result = confirm("Want to delete the Subsection.?");
        if(result){
            $("."+sec+" ."+sub+".subsec").remove()
            get_sections();
            get_subsections();
            submit_me();
        }
    })     
    $(document).on('click', '.help_u_sb', function(){
        var sec = $(this).attr('foo-sec');        
        var sub = $(this).attr('foo-sub-sec');
        //console.log(sec, sub)
        $(".help_u_sub."+sec+"_"+sub).toggle();        
    })
    $(document).on('click', '.show_u_sb', function(){
        $(".hide_u_sb.u_"+$(this).attr("foo-sec")+'_'+$(this).attr("foo-sub-sec")).toggle();
    })
    $(document).on('click', '.duplicate_u_sb', function(){
        //console.log(".duplicate_u_"+$(this).attr("foo-sec")+'_'+$(this).attr("foo-sub-sec"))
        $(".duplicate_u_"+$(this).attr("foo-sec")+'_'+$(this).attr("foo-sub-sec")).toggle();
    })
    $(document).on('click', '.column_u_sb', function(){        
        $(".column_u_"+$(this).attr("foo-sec")+'_'+$(this).attr("foo-sub-sec")).toggle();
    })

    /*--Main Section Options--*/
    /*--DELETE SECTION--*/
    $(document).on('click', '.dlt_sec', function(){        
        var foo_lbl = $(this).attr('foo-id');        
        var result = confirm("Want to delete the Section.?");
        if(result){            
            $("section."+foo_lbl).remove()
            get_sections();
            get_subsections();
        }
    })
    $(document).on('click', '.help_u', function(){        
        $(".help_u_"+$(this).attr("foo-id")).toggle();
    })
    $(document).on('click', '.duplicate_u', function(){        
        $(".duplicate_u_"+$(this).attr("foo-id")).toggle();
    })
    $(document).on('click', '.column_u', function(){        
        $(".column_u_"+$(this).attr("foo-id")).toggle();
    })
    $(document).on('click', '.show_u', function(){
        //console.log($(this).attr("foo-id"))
        $(".hide_u.u_"+$(this).attr("foo-id")).toggle();
    })

    /*--Fields Options--*/
    /*--DELETE FIELDS--*/
    $(document).on('click', '.dlt_me', function(){
        var result = confirm("Want to delete the field.?");
        if(result){
            $(this).parent().closest('li').remove();
            get_sections();
            get_subsections();
            submit_me();
        }
    })

    $("span.v_duplicate_u_sub").click(function(){    
        var sec = $(this).attr("foo-sec");
        var sub = $(this).attr("foo-sub-sec");
        var this_sec = $("."+sec+" ul ."+sub)[0].outerHTML;        
        sec = sec.split('section');
        sub = sub.split('subsection');
        //console.log(this_sec, i_d(6), sub[1]);
        //console.log('-------------')
        var new_str = replace_subsection_html($.trim(this_sec), sub[1], i_d(6))
        var new_str2 = replace_subsection_html($.trim(newstr), 'v_duplicate_u_sub glyphicon glyphicon-duplicate', 'v_delete_u_sub glyphicon glyphicon-trash')
        //console.log(new_str2)
        //console.log($("."+sec+" ul ."+sub)[0].outerHTML)        
        $('.subsections.subsectiont8k5to:nth-child(1)').after(new_str2)        
    })
    $(".view_help_u").hover(function(){        
        $(".view_help_u_"+$(this).attr('foo-id')).toggle();
    })    
    $(".show_help").hover(function(){        
        $(".show_help_"+$(this).attr('foo-li')).toggle();
    })
    $(".view_help_u_sub").hover(function(){        
        $(".view_help_u_"+$(this).attr('foo-sec')+'_'+$(this).attr('foo-sub-sec')).toggle();
    })
    $(window).scroll(function() {
        if ($(this).scrollTop() > 400){  
            $('#dj_form .fields_').addClass("sticky");
        }
        else{
            $('#dj_form .fields_').removeClass("sticky");
        }
    });
    $(function() {
        /*--DATE--*/
        $( ".dj_date" ).datepicker();        
    });
})
/*-- get parent table--*/
function get_parent_table(){
    var cc = 1;
    $('input[type="hidden"]').each(function(){
        if($(this).attr('name')){
            //console.log($(this).attr('name'))            
            var input_name = $(this).attr('name');
            if ( input_name.indexOf('table') > -1 )  {
                if(cc == 1){
                    console.log($(this).val())                    
                }
                cc += 1;
            }
        }
    })
}

/*--new js--*/
$(".addfield").click(function(){
    var sec_ = $("#section_").val();
    var alltables_ = $("#alltables_").val();
    if(alltables_ == ''){
        alert('Select table')
        $("#alltables_").focus();
        return false        
    }
    if(field == ''){
        alert('Empty field');
        return false
    }
    var parent_table = $("#parent_table").val()
    if(parent_table == ''){
        $("#parent_table").val(alltables_);
    }
    var subsec_ = $("#subsection_").val();
    var field = $("#field_").val();
    var fieldtype = $("#fieldtype").val();
    var li_ = $.now();
    //console.log(sec_, subsec_, field, fieldtype, alltables_)
    $("#field_ option[value="+field+"]").remove();
    var fields = '<li data-draggable="item" class="list-group-item" draggable="true">';
    fields += '<label>'+field+'('+fieldtype+')</label>';
    fields += '<div foo-li="'+li_+'" class="glyphicon glyphicon-cog pull-right show"></div>';
    fields += '<div foo-li="'+li_+'" class="glyphicon glyphicon-trash pull-right dlt_me"></div>';    
    fields += '<div class="hide_me show_'+li_+'">';
        fields += '<div class="input-group">';
            fields += '<span class="input-group-addon">Label</span>';            
            fields += '<input class="form-control" type="text" name="section['+sec_+'][subsections]['+subsec_+'][fields]['+li_+'][label]" value="'+field+'">';
        // fields += '</div>';
        // fields += '<div class="input-group">';
            fields += '<span class="input-group-addon">IsRequired</span>'+
            '<select class="form-control" name="section['+sec_+'][subsections]['+subsec_+'][fields]['+li_+'][isrequired]" placeholder="isrequired">'+
                '<option value="">Select</option>'+
                '<option value="yes">Yes</option>'+
                '<option value="no">No</option>'+
            '</select>';
            fields += '</div>';
        fields += '<div class="input-group">';
            fields += '<span class="input-group-addon">HelpText</span>';
            fields += '<input type="text" class="form-control" name="section['+sec_+'][subsections]['+subsec_+'][fields]['+li_+'][help]" value="">';
        fields += '</div>';
        fields += '<input type="hidden" name="section['+sec_+'][subsections]['+subsec_+'][fields]['+li_+'][type]" value="'+fieldtype+'">';
        fields += '<input type="hidden" name="section['+sec_+'][subsections]['+subsec_+'][fields]['+li_+'][table]" value="'+alltables_+'">';
        fields += '<input type="hidden" name="section['+sec_+'][subsections]['+subsec_+'][fields]['+li_+'][field]" value="'+field+'">';
    fields += '</div></li>';
    $('#'+sec_+'_'+subsec_).append(fields);    
    key_up();
    get_parent_table();
    drag_drop();
    submit_me();
})

/*--show-hide--*/
$(document).on('click', '.show', function(){
    $('.show_'+$(this).attr('foo-li')).toggle();    
})

$(document).on('click', '.v_delete_u_sub', function(){
    var sec = $(this).attr('foo-sec');        
    var sub = $(this).attr('foo-sub-sec');        
    var result = confirm("Want to delete the Subsection.?");
    if(result){
        console.log(sec, sub, "."+sec+"."+sub)
        $(".subsections."+sub).remove()
        get_sections();
        get_subsections();
    }
})

function show_data_type(this_val){
    console.log(this_val);
    $("#fieldtype > option").hide()
    if ( this_val.indexOf('int') > -1 )  { //if datatype contains int
        $('#fieldtype > option[value="number"]').show()
        $('#fieldtype > option[value="select"]').show()
    }else if(this_val.indexOf('char') > -1){
        $('#fieldtype > option[value="text"]').show()
        $('#fieldtype > option[value="textarea"]').show()
    }else if(this_val.indexOf('text') > -1){
        $('#fieldtype > option[value="text"]').show()
        $('#fieldtype > option[value="textarea"]').show()
    }
}

$("#field_").change(function(e){
    //console.log(this.options[this.selectedIndex].getAttribute('foo-type'), '<<<<<<<<<<<<')
    show_data_type(this.options[this.selectedIndex].getAttribute('foo-type'));
})

jQuery("#alltables_").change(function(){
    $("#field_, #fieldtype, #section_, #subsection_").attr('disabled', 'disabled');
    var request = jQuery.ajax({
      url: "getfields",
      method: "GEt",
      data: { tbl : jQuery(this).val() },
      dataType: "html"
    });     
    request.done(function( flds ) {
        $("#field_, #fieldtype, #section_, #subsection_").removeAttr('disabled');
        $("#field_").html(flds)
        var this_type = $("#field_ option:nth-child(1)").attr('foo-type')
        //console.log(this_type)
        show_data_type(this_type);
    });     
    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
    });
})