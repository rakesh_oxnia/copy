<?php
	use yii\helpers\Html;
	use yii\helpers\Url;

	/* @var $this yii\web\View */
	/* @var $user common\models\User */

	?>
<style type="text/css">	body{background-image:url('http://www.webstack.com.au/wp/wp-content/plugins/myMail/assets/img/bg/climpek.png');background-repeat:repeat-y no-repeat;background-position:top center;}
	#outlook a{padding:0;}
	body{width:100% !important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0;}
	.ExternalClass{width:100%;}
	.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}
	.bodytbl{margin:0;padding:0;width:100% !important;}
	img{outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;display:block;max-width:100%;}
	a img{border:none;}
	p{margin:1em 0;}
	table{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}
	table td{border-collapse:collapse;}
	.o-fix table,.o-fix td{mso-table-lspace:0pt;mso-table-rspace:0pt;}
	body{background-color:#F3F4F4/*Background Color*/;}
	table{font-family:Helvetica,Arial,sans-serif;font-size:12px;color:#585858;}
	td,p{line-height:24px;color:#585858/*Text*/;}
	td,tr{padding:0;}
	ul,ol{margin-top:24px;margin-bottom:24px;}
	li{line-height:24px;}
	a{color:#5ca8cd/*Contrast*/;text-decoration:none;padding:2px 0px;}
	a:link{color:#5ca8cd;}
	a:visited{color:#5ca8cd;}
	a:hover{color:#5ca8cd;}
	.h1{font-family:Helvetica,Arial,sans-serif;font-size:26px;letter-spacing:-1px;margin-bottom:16px;margin-top:2px;line-height:30px;}
	.h2{font-family:Helvetica,Arial,sans-serif;font-size:20px;letter-spacing:0;margin-top:2px;line-height:30px;}
	h1,h2,h3,h4,h5,h6{font-family:Helvetica,Arial,sans-serif;font-weight:normal;}
	h1{font-size:20px;letter-spacing:-1px;margin-bottom:16px;margin-top:4px;line-height:24px;}
	h2{font-size:18px;margin-bottom:12px;margin-top:2px;line-height:24px;}
	h3{font-size:14px;margin-bottom:12px;margin-top:2px;line-height:24px;}
	h4{font-size:14px;font-weight:bold;}
	h5{font-size:12px;}
	h6{font-size:12px;font-weight:bold;}
	h1 a,h2 a,h3 a,h4 a,h5 a,h6 a{color:#5ca8cd;}
	h1 a:active,h2 a:active,h3 a:active,h4 a:active,h5 a:active,h6 a:active{color:#5ca8cd !important;}
	h1 a:visited,h2 a:visited,h3 a:visited,h4 a:visited,h5 a:visited,h6 a:visited{color:#5ca8cd !important;}
	.wrap.header{border-top:1px solid #FEFEFE/*Content Border*/;}
	.wrap.footer{border-bottom:1px solid #FEFEFE;}
	.wrap.body,.wrap.header,.wrap.footer{background-color:#FFFFFF/*Body Background*/;border-right:1px solid #FEFEFE;border-left:1px solid #FEFEFE;}
	.padd{width:24px;}
	.small{font-size:11px;line-height:18px;}
	.separator{border-top:1px dotted #E1E1E1/*Separator Line*/;}
	.btn{margin-top:10px;display:block;}
	.btn img{display:inline;}
	.subline{line-height:18px;font-size:16px;letter-spacing:-1px;}
	table.textbutton td{background:#efefef/*Text Button Background*/;padding:1px 14px 4px 14px;color:#585858;display:block;height:22px;border:1px solid #FEFEFE;vertical-align:top;}
	table.textbutton a{color:#585858;font-size:13px;font-weight:normal;line-height:22px;width:100%;display:inline-block;}
	div.preheader{line-height:0px;font-size:0px;height:0px;display:none !important;display:none;visibility:hidden;}
	@media only screen and (max-device-width: 480px) {
	body{-webkit-text-size-adjust:120% !important;-ms-text-size-adjust:120% !important;}
	table[class=bodytbl] .subline{float:left;}
	table[class=bodytbl] .padd{width:12px !important;}
	table[class=bodytbl] .wrap{width:470px !important;}
	table[class=bodytbl] .wrap table{width:100% !important;}
	table[class=bodytbl] .wrap img{max-width:100% !important;height:auto !important;}
	table[class=bodytbl] .wrap .m-100{width:100% !important;}
	table[class=bodytbl] .m-0{width:0;display:none;}
	table[class=bodytbl] .m-b{display:block;width:100% !important;}
	table[class=bodytbl] .m-b-b{margin-bottom:24px !important;}
	table[class=bodytbl] .m-1-2{max-width:264px !important;}
	table[class=bodytbl] .m-1-3{max-width:168px !important;}
	table[class=bodytbl] .m-1-4{max-width:120px !important;}
	table[class=bodytbl] .m-1-2 img{max-width:264px !important;}
	table[class=bodytbl] .m-1-3 img{max-width:168px !important;}
	table[class=bodytbl] .m-1-4 img{max-width:120px !important;}
	}
	@media only screen and (max-device-width: 320px) {
	table[class=bodytbl] .wrap{width:310px !important;}
	}
	.preheader{display:none!important; visibility:hidden; opacity: 0; color: transparent; height: 0; width: 0;}
	.preheader{
	display:none;
	}

	.panel-default {
    border-color: #ddd;
}

.panel {
	margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;

    box-shadow: 0 1px 1px rgba(0,0,0,0.05);
}
.fa-comments:before {
    content: "\f086";
}
.panel-default > .panel-heading {
    background-color: #03a9f4;
    border-color: #03a9f4;
    color: #FFFFFF;
    border-radius: 0;
    background-clip: padding-box;
}
.panel-default>.panel-heading {
    color: #333;
    background-color: #f5f5f5;
    border-color: #ddd;
}
.panel-heading {
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
}
.fa-fw {
    width: 1.28571429em;
    text-align: center;
}
.fa {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}

.panel-body:before, .panel-body:after {
    content: " ";
    display: table;
}

</style>
<div class="new lead">
	<table class="bodytbl" width="100%" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td background="http://oxnia.com/chat/wp-content/plugins/myMail/assets/img/bg/climpek.png" align="center">
					<modules class="ui-sortable">
						<module custom="" active="" label="Full Size Image" class="active ui-sortable-handle" block=""></module>
						<module custom="" active="" label="Full Size Image" class="active ui-sortable-handle" block=""></module>
						<module label="1/1 Column" auto="" class="active ui-sortable-handle" style="display: block;">
							<table width="600" cellspacing="0" cellpadding="0" class="wrap body">
								<tbody>
									<tr>
										<td height="12" colspan="3"></td>
									</tr>
									<tr>
										<td style="display:none !important; visibility:hidden; opacity: 0; color: transparent; height: 0; width: 0;" colspan="3" >
											<span style="display:none !important;" class="preheader">View a full list of leads in client dashboard</span>
										</td>
									</tr>
									<tr>
										<td width="24" class="padd">&nbsp;</td>
										<td valign="top" align="left">
											<multi label="Body">
												<div style="background: #f5f5f5; padding-bottom: 2em;">
													<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
														<tbody>
															<tr>
																<!-- <td>&nbsp;</td> -->
																<td style="width: 530px; max-width: 530px;" align="center" width="530">
																	<table style="max-width: 530px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
																		<tbody>

																			<tr>
																				<td bgcolor="#ffffff">
																					<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
																						<tbody>
																							<tr>
																								<td style="margin: 0px; padding: 0px; text-align: left; color: #2b2b2b; line-height: 1.5em; letter-spacing: 0.03em; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 300; vertical-align: top;"><a href="http://chatapptest.oxnia.com/images/reallogo.png" target="_blank"><img class="alignnone" title="LiveChat - The fastest way to reach your customers" src="http://chatapptest.oxnia.com/images/reallogo.png" alt="&nbsp;LiveChat&nbsp;" width="360" height="100" border="0"></a></td>

																							</tr>

																							<tr>
																								<!-- <td>&nbsp;</td> -->
																								<td style="max-width: 480px;" width="480">
																									<table style="max-width: 480px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
																										<tbody>
																											<tr>
																												<td width="30">&nbsp;</td>
																												<td style="margin: 0px; padding: 0px; text-align: left; color: #2b2b2b; line-height: 1.5em; letter-spacing: 0.03em; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 22px; font-weight: 400;" align="left">
																													Your have a new lead
																												</td>
																												<td width="30">&nbsp;</td>
																											</tr>
																											<tr>
																												<td width="30">&nbsp;</td>
																												<td style="margin: 0px; padding: 0px; text-align: left; color: #2b2b2b; line-height: 1.5em; letter-spacing: 0.03em; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 22px; font-weight: 400;" align="left">
																													<table style="width: 392px;">
																														<tbody>
																														 <?php


																																		//$len  = floor(strlen($name)/2);


																																	?>
																															<?php //echo '<pre>';print_r($lead);echo '</pre>';die;?>
																															<?php if(true){ ?>
																															<tr>
																																<td style="width: 100px;">Name : &nbsp;</td>
																																<td style="width: 296px;">

																																<?php echo 'Ravindra';?></td>
																															</tr>
																															<tr>
																																<td style="width: 100px;">Email : &nbsp;</td>
																																<td style="width: 296px;"><?php
																																					echo 'ravindra@webcubictechnologies.com'
																																					?></td>
																															</tr>

																															<tr>
																																<td style="width: 100px;">Geolocation:&nbsp;</td>
																																<td style="width: 296px;"><?='Mumbai'?>,<?= 'Maharashtra'?>,<?='India'?></td>
																															</tr>
																														</tbody>
																													</table>
																												</td>
																												<td width="30">&nbsp;</td>
																											</tr>
																											<tr>
																												<td colspan="3" height="20">&nbsp;</td>
																											</tr>
																											<tr>
																												<td width="30">&nbsp;</td>
																												<td style="margin: 0px; padding: 0px; color: #2b2b2b; line-height: 1.5em; letter-spacing: 0.03em; font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size: 13px; font-weight: 400; text-align: left;" align="right">
																													<?php $url = 'http://livechat.webautodev.com/enduser/';?>
																													Chat started on:<a href="<?='http://livechat.webautodev.com/enduser/'?>"><?='http://livechat.webautodev.com/enduser/'?></a>&nbsp;
																												</td>
																												<td width="30">&nbsp;</td>
																											</tr>
																											<tr>
																												<td colspan="3" height="30">&nbsp;</td>
																											</tr>
																										<?php if( true ){?>
																											<tr>
																												<td colspan="3">
																													<table>
																														<tr>
																															<td width="487" height="20"><span style="margin: 30px; padding: 0px; text-align: left; color: #2b2b2b; line-height: 1.5em; letter-spacing: 0.03em; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 22px; font-weight: 400;">Transcript</span></td>
																													  </tr>
																														<tr>
																															<td>
<div class="chatwind col-md-12">

    <div class="chat-panel panel panel-default" style="border-color: #ddd; margin-bottom: 20px; background-color: #fff; border: 1px solid transparent;  border-radius: 4px; box-shadow: 0 1px 1px rgba(0,0,0,0.05);">
		<div class="panel-heading" style="background-color: #fff; border-color: #FAAF52; color: #FFFFFF; border-radius: 0;  background-clip: padding-box;
     padding: 10px 15px; border-bottom: 1px solid #fff;  border-top-right-radius: 3px; border-top-left-radius: 3px; ">
			<i class="fa fa-comments fa-fw"></i></div>
        <!-- /.panel-heading -->
		<div class="panel-body" style="padding:15px 10px; border: 1px solid #E4EBF1; border-top: 0px;">
			<ul class="chat" style="  list-style: none; padding: 0px; margin: 0px;">
				<?php if(false){ ?>
				<?php foreach($chat->messages as $messages){ ?>
				<?php if($messages->user_type !== 'visitor') { ?>
				<li class="left clearfix" style="list-style: none;  margin-bottom: 10px;  padding-bottom: 20px;  border-bottom: 1px dotted #B3A9A9;">
					<span class="chat-img pull-left" style="float: left !important;">
						<img src="http://oxnia.com/externalassets/agent-icon.png" alt="User Avatar" class="img-circle" style="border-radius: 50%;">
					</span>
					<div class="chat-body clearfix" style="margin-left: 60px;">
						<div class="header-chat">
							<strong class="primary-font"><?= $messages->author_name;?> (Agent)</strong>
							<small class="pull-right text-muted" style="float: right !important;">
								<i class="fa fa-clock-o fa-fw"></i><?= $messages->date;?>
							</small>
						</div>
						<p style="margin: 0; color: #777777; line-height: 24px;">
							<?= $messages->text;?>
						</p>
					</div>
				</li>
				<?php } else{ ?>
				<li class="right clearfix" style="list-style: none; margin-bottom: 10px; padding-bottom: 20px;border-bottom: 1px dotted #B3A9A9;">
					<span class="chat-img pull-right" style="float: right !important;">
						<img src="http://oxnia.com/externalassets/customer-icon.png" alt="User Avatar" class="img-circle" style="border-radius: 50%;">
					</span>
					<div class="chat-body clearfix" >
						<div class="header-chat">
							<small class=" text-muted">
								<i class="fa fa-clock-o fa-fw"></i> <?= $messages->date;?></small>
							<strong class="pull-right primary-font" style="float: right !important;"><?= $messages->author_name;?> (Visitor)</strong>
						</div>
						<p style="margin: 0;  color: #777777; line-height: 24px;">
							<?= $messages->text;?>
						</p>
					</div>
				</li>
				<?php } //end if ?>
				<?php } // end foreach?>
				<?php } // end if?>
			</ul>
		</div>
		<!-- /.panel-body -->
	</div>

</div>
																															</td>
																														</tr>
																													</table>
																												</td>
																											</tr>

																										<?php }?>












																											<tr>
																												<td colspan="3" height="40">&nbsp;</td>
																											</tr>
																											<tr>
																												<td width="30">&nbsp;</td>
																												<td align="center">
																													<table width="300px">
																														<tr>
																															<td style="margin: 5px auto; padding: 0.8em 0.5em; border-radius: 4px; border: 0px; width: 200px; color: #FFFFFF; text-decoration: none; text-align: center; line-height: 18px; letter-spacing: 0.12em; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 13px; font-weight: 300; display: block; background-color: #f7931e; mso-hide: all; -webkit-text-size-adjust: none;">
																																<!-- [if mso]>
																																	<v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://www.livechatinc.com/blog/irate-customer/" style="height:50px;v-text-anchor:middle;width:200px;margin:0 auto;" arcsize="4%" fillcolor="#EB586F" stroke="false">
																																	    <w:anchorlock></w:anchorlock>
																																	    <center style="color:#fff;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;letter-spacing:0.04em;">READ THE POST</center>
																																	</v:roundrect>
																																	<![endif]-->
																																	<?php
																																	   if( true ){
																																		   $textBtn = 'Go to dashboard';
																																		   $url = '';
																																	   }else{
																																		   $textBtn = 'View full transcript';
																																	   }
																																	?>

																																	<a href="<?= $url;?>" style="color:#FFFFFF;height:40px;width:198px;display:block; text-decoration:none;"><span style="color: #ffffff; text- decoration: none"><font color="#FFF"><?= $textBtn;?></font></span></a>
																															</td>
																														</tr>
																													</table>
																												</td>
																												<td width="30">&nbsp;</td>
																											</tr>
																											<tr>
																												<td colspan="3" height="40">&nbsp;</td>
																											</tr>
																										</tbody>
																									</table>
																								</td>
																								<td>&nbsp;</td>
																							</tr>
																						</tbody>
																					</table>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
																<td>&nbsp;</td>
															</tr>
														</tbody>
													</table>
												</div>
												<p></p>
											</multi>
											<div class="btn">
											</div>
										</td>
										<td width="24" class="padd">&nbsp;</td>
									</tr>
									<tr>
										<td height="12" colspan="3"></td>
									</tr>
								</tbody>
							</table>
						</module>
					</modules>
					<table width="600" cellspacing="0" cellpadding="0" class="wrap footer">
						<tbody>
							<tr>
								<td width="24" class="padd">&nbsp;</td>
								<td valign="top" align="center">
									<table width="100%" cellpadding="0" cellspacing="0" class="o-fix">
										<tbody>
											<tr>
												<td valign="top" align="left">
													<table width="360" cellpadding="0" cellspacing="0" align="left">
														<tbody>
															<tr>
																<td class="small m-b" align="left" valign="top">
																</td>
															</tr>
														</tbody>
													</table>
													<table width="168" cellpadding="0" cellspacing="0" align="right">
														<tbody>
															<tr>
																<td class="small" align="right" valign="top">
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
								</td>
								<td width="24" class="padd">&nbsp;</td>
							</tr>
							<tr>
								<td height="24" colspan="3"></td>
							</tr>
						</tbody>
					</table>
					<table cellpadding="0" cellspacing="0" class="wrap m-b-b">
						<tbody>
							<tr>
								<td valign="top">&nbsp;</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	<?php } ?>
</div>
