<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class KosmoLoginAsset extends AssetBundle
{
    public $basePath = "@webroot";
    public $baseUrl = "@web";
    public $css = [
    	"libs/bootstrap/css/bootstrap.min.css",
    	"kosmoassets/fonts/line-awesome/css/line-awesome.min.css",
    	"kosmoassets/fonts/open-sans/styles.css",
    	"libs/tether/css/tether.min.css",
    	"kosmoassets/styles/common.min.css",
    	"kosmoassets/styles/pages/auth.min.css",
    	"css/styles.css",
    	"https://fonts.googleapis.com/css?family=Open+Sans",
    ];
    public $js = [
		"libs/tether/js/tether.min.js",
		"libs/bootstrap/js/bootstrap.min.js",
        'https://unpkg.com/sweetalert/dist/sweetalert.min.js',
    ];
    public $depends = [
        "yii\web\YiiAsset",
        "yii\bootstrap\BootstrapAsset",
    ];
}
