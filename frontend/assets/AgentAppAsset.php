<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AgentAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'//cdn.livechatinc.com/boilerplate/1.0.css',
    ];
    public $js = [
        //'//code.jquery.com/jquery-2.2.4.min.js',
        //'//cdn.livechatinc.com/boilerplate/1.0.js',
        //'//cdn.livechatinc.com/accounts/accounts-sdk.min.js'
    ];

    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
