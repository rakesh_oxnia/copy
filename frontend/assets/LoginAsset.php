<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'cube/css/bootstrap/bootstrap.min.css',
        'cube/css/libs/font-awesome.css',
        'cube/css/libs/nanoscroller.css',
        'cube/css/compiled/theme_styles.css',
    ];
    public $js = [
		//'js/bootstrap.min.js',
        'cube/js/jquery.nanoscroller.min.js',
        'cube/js/demo.js',
        'cube/js/scripts.js',
        //'js/custom.js',
        'js/spin.min.js',
        'js/ladda.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
