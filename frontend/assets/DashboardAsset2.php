<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashboardAsset2 extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
		'adminT/bower_components/bootstrap/dist/css/bootstrap.min.css',
		'adminT/bower_components/metisMenu/dist/metisMenu.min.css',
		'adminT/dist/css/timeline.css',
		'adminT/dist/css/sb-admin-2.css',
		'adminT/bower_components/morrisjs/morris.css',
		'adminT/bower_components/font-awesome/css/font-awesome.min.css',
    ];
    public $js = [
		'adminT/bower_components/bootstrap/dist/js/bootstrap.min.js',
		'adminT/bower_components/metisMenu/dist/metisMenu.min.js',
		'adminT/bower_components/raphael/raphael-min.js',
		//'adminT/bower_components/morrisjs/morris.min.js',
		//'adminT/js/morris-data.js',
		'adminT/dist/js/sb-admin-2.js',
    ];
    public $depends = [
        
    ];
}
