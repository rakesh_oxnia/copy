<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'cube/css/bootstrap/bootstrap.min.css',
        'css/site.css',
		'css/wizard.css',
		'css/colorpicker.css'
        /* 'css/bootstrap-theme.css',
        'css/style.css',
        'css/bootstrap.css',
        'css/hover.css',
        'css/jquery.bxslider.css',
        'css/hover-min.css',
        'css/jquery-ui.css',
        'css/dj_forms.css',
        'css/trius-style.css',
        'css/ladda-themeless.min.css', */
    ];
    public $js = [
		'js/bootstrap.min.js',
		 
		'js/colorpicker.js',
        /* //'js/jquery.bxslider.js',
        'js/jquery-ui.js',
        'js/dj_forms.js',
        //'js/custom.js',
        'js/spin.min.js',
        'js/ladda.min.js', */
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
