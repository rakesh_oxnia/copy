<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashboardnAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
	public $depends = [
        //'yii\web\YiiAsset',
       // 'yii\bootstrap\BootstrapAsset',
    ];
    public $css = [
		'cube/css/bootstrap/bootstrap.min.css',
		'cube/css/libs/font-awesome.css',
		'cube/css/libs/nanoscroller.css',
		'cube/css/compiled/theme_styles.css',
		'cube/css/libs/daterangepicker.css',
		'cube/css/libs/jquery-jvectormap-1.2.2.css',
		'cube/css/libs/weather-icons.css',
		'css/jquery-ui.css',
    ];
    public $js = [
		//'cube/js/jquery.js',
		//'cube/js/bootstrap.js',
		'cube/js/jquery.nanoscroller.min.js',
		'cube/js/moment.min.js',
		'cube/js/jquery-jvectormap-1.2.2.min.js',
		'cube/js/jquery-jvectormap-world-merc-en.js',
		'cube/js/gdp-data.js',
		'cube/js/flot/jquery.flot.min.js',
		'cube/js/flot/jquery.flot.resize.min.js',
		"cube/js/flot/jquery.flot.time.min.js",
		"cube/js/flot/jquery.flot.threshold.js",
		"cube/js/flot/jquery.flot.axislabels.js",
		"cube/js/jquery.sparkline.min.js",
		"cube/js/skycons.js",
		"cube/js/scripts.js",
		"cube/js/pace.min.js",
		"adminT/bower_components/metisMenu/dist/metisMenu.min.js",
		'js/jquery-ui.js',
    ];
}
