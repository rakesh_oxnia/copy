<?php
use ruskid\stripe\StripeCheckout;
?>
<?= 
StripeCheckout::widget([
    'action' => '/',
    'name' => $packageData['package_name'],
    'description' => "Leads : " . $packageData['number_of_leads'] . " Chats : " . $packageData['number_of_chats'],
    'amount' => $packageData['amount'],
	'image' => '/128x128.png',
    
]);
?>