<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use ruskid\stripe\StripeCheckout;
?>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/pricing/subscriptions.min.css">

 <div class="ks-page-header">
            <section class="ks-title">
                <h3>Subscriptions</h3>

                <div class="ks-controls">
                    <nav class="breadcrumb ks-default">
                        <a class="breadcrumb-item ks-breadcrumb-icon" href="#">
                            <span class="la la-home ks-icon"></span>
                        </a>
                        <a class="breadcrumb-item" href="#">Subscriptions</a>
                        <span class="breadcrumb-item active" href="#">Coupons</span>
                    </nav>
                </div>
            </section>
        </div>
        <div class="ks-page-content">
            <div class="ks-page-content-body ks-full-height">
                <div class="ks-nav-body-wrapper">
					<?php if (Yii::$app->session->hasFlash('success')){?>
							<div class="alert alert-success">
								<?php echo Yii::$app->session->getFlash('success');?>
							</div>
					<?php }?>

					<div class="alert alert-warning" style="text-align : center; margin-top : 10px;">
            <?php
              if($packageName == 'Custom'){
            ?>
              <p>You are currently subscribed to <?php echo $packageName?> with <?=$customPackageLeads?> leads .</p>
            <?php
          }else{
        ?>
              <p>You are currently subscribed to <?php echo $packageName?>.</p>
        <?php
          }
            ?>

					</div>

                    <div class="ks-pricing-subscriptions-page">

						<div class="ks-header">
							  <h3 class="ks-name">Coupon</h3>
						</div>
							<div class="alert alert-warning">
								<p>Coupons only apply to monthly packages not pay as you go.</p>
							</div>

							<?php if( $error ){?>
								<div class="alert alert-danger">
									<?php echo $error;?>
								</div>
							<?php }?>
							<?php if( $success ){?>
								<div class="alert alert-success">
									<?php echo $success;?>
								</div>
							<?php }?>

							<?php if(!isset(Yii::$app->session['discount'])){?>
								<?php $form = ActiveForm::begin(); ?>

									<div class="form-group">
										<input placeholder="Apply Coupon" type="text" name="coupon" value="" class="form-control">
									</div>
									<div class="form-group">
										<input type="submit" name="apply" value="Apply" class="btn btn-primary btn-block">
									</div>
								 <?php ActiveForm::end(); ?>
							<?php }else{?>
									<div class="alert alert-warning">
										Coupon Applied. Discount : <?php echo Yii::$app->session['discount'] . "%";?>
									</div>
							<?php }?>

						<hr>

                        <div class="ks-header">
                            <h3 class="ks-name">Pricing</h3>
                            <div class="ks-description">Plans that cater to everyone</div>
                        </div>
                        <div class="ks-subscriptions">

						<div class="ks-subscription ks-active">
										<div class="ks-header">
											<span class="ks-name">Pay as you go</span>
											<span class="ks-price">
												<span class="ks-amount">$5</span>/per lead
											</span>
										</div>
										<div class="ks-body">
											 <ul>
												<li class="ks-item">
													<span class="ks-icon la la-briefcase"></span>
													<span class="ks-text">Leads</span>
													<span class="ks-amount">Unlimited</span>
												</li>

												<li class="ks-item">
													<span class="ks-icon la la-comments"></span>
													<span class="ks-text">Chats</span>
													<span class="ks-amount">Unlimited</span>
												</li>





											</ul>
											<a href="<?php echo Url::to(['/package/payasyougo']);?>" class="btn btn-info btn-block">Pay as you go</a>


										</div>
									</div>


                             <?php if( !empty( $packages ) ) : ?>
								<?php foreach($packages as $package) : ?>
									<?php
											$amount = $package['amount'];
											if(Yii::$app->session['discount']){
												$discount = ($amount * Yii::$app->session['discount'])/100;
												$amount  = $amount - $discount;
											}
									?>

										<div class="ks-subscription">
										<div class="ks-header">
											<span class="ks-name"><?php echo $package['package_name'];?></span>

                      <?php
                        if($package['package_name'] == 'Custom'){
                      ?>
                          <div class="custom-price-container">
                            <div class="form-group">
                              <input placeholder="Enter number of leads" type="text" name="numberOfLeads" id='numberOfLeads' value="" class="form-control">
                            </div>
                            <div class="form-group">
                              <button class='btn btn-info calculate-price'>Calculate Price</button>
                            </div>

                            <span class="ks-price">
                              <span class="ks-amount-custom"></span>
                            </span>
                            <br>
                            <a class='pay-button-custom' data-pack-name='<?=$package['package_name']?>' > <button name='pay' class='btn btn-primary' value='Pay'>Pay</button> </a>
                          </div>

                      <?php
                        }else{
                      ?>
                          <span class="ks-price">
                            <span class="ks-amount">$<?php echo $amount;?></span>/-
                          </span>
                      <?php
                        }
                      ?>

										</div>
										<div class="ks-body">
											<ul>
												<li class="ks-item">
													<span class="ks-icon la la-briefcase"></span>
													<span class="ks-text">Leads</span>
													<span class="ks-amount ks-leads"><?php echo $package['number_of_leads'];?></span>
												</li>

												<li class="ks-item">
													<span class="ks-icon la la-comments"></span>
													<span class="ks-text">Chats</span>
													<span class="ks-amount ks-chats"><?php echo $package['number_of_chats'];?></span>
												</li>





											</ul>
											<!--<a href="<?php echo Url::to(['/package/paynow', 'id' => $package['id']]);?>" class="btn btn-info btn-block">Pay with Paypal</a>-->

												<?php
													if(Yii::$app->user->identity->stripeToken == ''){
														echo
														StripeCheckout::widget([
															'action' => Url::to(['/invoice/success', 'id' => $package['id']]),
															'name' => $package['package_name'],
															'description' => "Leads : " . $package['number_of_leads'] . " Chats : " . $package['number_of_chats'],
															'amount' => $amount * 100,
															'image' => Yii::$app->request->baseUrl . '/cube/img/logo.png',

														]);
													}else{
													?>
                          <?php
                            if($package['package_name'] != 'Custom'){
                          ?>
                              <a class="pay-button" data-pack-name="<?=$package['package_name']?>" href="<?php echo Yii::$app->urlManager->createUrl(['invoice/paybysavedcard', 'id' => $package['id'] ])?>"  data-remaining-leads='47'   > <button name="pay" class="btn btn-primary" value="Pay">Pay</button> </a>
                              <?php }?>
                          <?php
                            }
                          ?>

										</div>
									</div>

								<?php endforeach;?>
							<?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php

$this->registerCss("
  .pay-button-custom{
    display: none;
    margin-top: 10px;
  }
");

$this->registerJs("

  $.ajaxSetup({
      data:  ".\yii\helpers\Json::encode([
          \yii::$app->request->csrfParam => \yii::$app->request->csrfToken,
      ])."
  });

  $('.pay-button').off('click').on('click', function(e){
    e.preventDefault();
    button_pack = $(this).attr('data-pack-name');
    current_package = '".$packageName."';

    if(current_package == 'Gold'){
      swal('Already Subscribed to Gold Package', 'Sorry! you cannot suscribe to this package.', 'error');
    }else if(current_package == 'Startup' && button_pack == 'Startup'){
      swal('Already Subscribed to Startup Package', 'Sorry! you cannot suscribe to this package again.', 'error');
    }
    else if(current_package == 'Startup' && button_pack == 'Gold'){
      window.location = $(this).attr('href');
    }
    else{
      window.location = $(this).attr('href');
    }

    /*swal({
      title: 'Are you sure ?',
      text: 'You have ' + leads + ' leads remaining, if you buy this package 50 more leads will be added',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        swal('Your package has been activated successfully', {
          icon: 'success',
        });
      } else {
        swal('Package not suscribed!');
      }
    });*/

  });

  $('.calculate-price').off('click').on('click', function(e){
    e.preventDefault();
    number_of_leads = $('#numberOfLeads').val();
    if(number_of_leads == ''){
      swal('Validation Error', 'Number of leads cannot be left blank', 'error');
      return false;
    }
    if(isNaN(number_of_leads)){
      swal('Validation Error', 'Please provide valid number of leads', 'error');
      return false;
    }
    parent_div = $(this).parent().parent().parent().parent();

    $.ajax({
      type: 'POST',
      url: '".Url::to(['package/calculate-package'])."',
      data: {number_of_leads: number_of_leads},
      success: function(response){
        response = JSON.parse(response);
        if(response.success == true){
          if(response.range_id != false){
            amount = response.amount;
            priceHtml = \"<div class='ks-header'><span class='ks-price'> <span class='ks-amount'>$\" + amount + \"</span>/- </span></div>\";
            $('.ks-amount-custom').text('$' + amount + '/-');
            parent_div.find('.ks-leads').text(response.number_of_leads);
            parent_div.find('.ks-chats').text(response.number_of_chats);
            $('.pay-button-custom').attr('data-range-id', response.range_id);
            $('.pay-button-custom').show();
          }else{
            swal('No package available', 'Sorry! No package available for the number of leads you have entered', 'error')
          }


        }else{
          swal('Some Problem Occured!', 'Please try again later after sometime', 'error');
        }
      },
      error: function(data){
        console.log(data);
      }
    })
  });

  $('.pay-button-custom').off('click').on('click', function(e){
    e.preventDefault();
    range_id = $(this).attr('data-range-id');
    if (typeof range_id !== typeof undefined && range_id !== false) {
      window.location = '".Url::to(['invoice/pay-custom-package-by-saved-card'])."&id=' + range_id;
    }else{
      swal('Custom package price is not calculated', 'Please enter number of leads and calculate price for custom package', 'error');
    }
  });

");

?>
