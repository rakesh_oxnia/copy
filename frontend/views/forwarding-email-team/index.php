<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var frontend\models\ForwardingEmailTeamSearch $searchModel
 */

$this->title = 'Forwarding Email Teams';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ks-page-header">
    <section class="ks-title">
        <h3><?= Html::encode($this->title) ?></h3>
    </section>
</div>

<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">
                                 <h5>You can add upto 2 teams.</h5>

                                  <p>
                                      <?php
                                        if($teamCount < 2){
                                            echo Html::a('Add team', ['create'], ['class' => 'btn btn-success']);
                                        }
                                      ?>
                                  </p>

                                  <?= GridView::widget([
                                      'dataProvider' => $dataProvider,
                                      'filterModel' => $searchModel,
                                      'columns' => [
                                          ['class' => 'yii\grid\SerialColumn'],

                                          'name',
                                          'created_at:datetime',
                                          'updated_at:datetime',

                                          //['class' => 'yii\grid\ActionColumn'],
                                          [
                                          'class' => 'yii\grid\ActionColumn',
                                                  'header'=>'Action',
                                                  'template'=>'{view}{update}{delete}',
                                                  'buttons' => [
                                                      'delete' => function ($url, $model) {
                                                            return Html::a('<span class="ks-icon la la-trash"></span>',
                                                                $url, [
                                                                  'data-confirm' => 'Are you sure you want to delete this Email Address ?',
                                                                  'data-method' => 'post'
                                                            ]);

                                                      },
                                                      'view' => function ($url, $model) {
                                                          return Html::a('<span class="ks-icon la la-eye"></span>',
                                                              $url);
                                                      },
                                                      'update' => function ($url, $model) {
                                                          return Html::a('<span class="ks-icon la la-pencil"></span>',
                                                              $url);
                                                      },
                                                  ],
                                          ],
                                      ],
                                  ]); ?>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  <?php
    $this->registerCss("

      .table td{
        padding: 10px !important;
        font-size: 12px !important;
      }

      .ks-icon{
        font-size: 20px !important;
        padding-left: 7px;
      }

      .action-column{
        width: 140px;
      }

      .ks-icon:hover{
        color: #f00;
      }

    ");
