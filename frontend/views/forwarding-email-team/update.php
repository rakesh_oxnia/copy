<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\ForwardingEmailTeam $model
 */

$this->title = 'Update Team: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Forwarding Email Teams', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="ks-page-header">
    <section class="ks-title">
        <h3><?= Html::encode($this->title) ?></h3>
    </section>
</div>
<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">

                              <h5><?= Html::encode($this->title) ?></h5>

                              <?= $this->render('_form', [
                                  'model' => $model,
                              ]) ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
