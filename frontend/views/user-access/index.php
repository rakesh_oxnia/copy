<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ChatstatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Access Control';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--New Design----->
<div class="ks-page-header">
            <section class="ks-title">
                <h3><?= Html::encode($this->title) ?></h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
                                      <?php Pjax::begin(['id' => 'pjax_widget']); ?>

                                        <p>
                                            <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
                                        </p>


                                        <?php  
                                          if (Yii::$app->user->can(User::ADMIN_ROLE)
                                                              || Yii::$app->user->can(User::SUBADMIN_ROLE)
                                          ) { 
                                         ?>

                                        <?= GridView::widget ([
                                            'dataProvider' => $dataProvider,
                                            'id' => 'pjax_widget',
                                            'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],
                                                'name',
                                                'email', 
                                                [
                                                    'label' => 'Role',
                                                    'value' => function ($model) {
                                                        $roles = Yii::$app->authManager->getRolesByUser($model->id);
                                                        $default_roles = [User::SUBADMIN_ROLE, User::ASSIST_ROLE, User::OUTLET_ROLE, User::SERVICE_ACCOUNT_ROLE];
                                                        foreach ($default_roles as $default_role) {
                                                          if(array_key_exists($default_role, $roles)) {
                                                            unset($roles[$default_role]);
                                                          }
                                                        }
                                                        $pure_roles = array();
                                                        $arry_roles = array_keys($roles);
                                                        foreach ($arry_roles as $role) {
                                                          $rolearr = explode('_', $role);
                                                          if(count($rolearr) > 1)
                                                            $pure_roles[] = $rolearr[1];
                                                        }
                                                      return implode(', ', $pure_roles);
                                                    }
                                                ],    
                                               // 'business_name',                                         
                                                [ 'class' => 'yii\grid\ActionColumn',
                                                  'header'=>'Action',
                                                  'headerOptions' => ['class' => 'action-column'],
                                                  'template'=>'{user}{view}{update}{delete}',
                                                  'buttons' => [
                                                      'user' => function ($url, $model) {
                                                          $url = Url::to(['user/switch-user', 'id' => $model->id]);
                                                          if (Yii::$app->user->can(User::ADMIN_ROLE)
                                                            || Yii::$app->user->can(User::SUBADMIN_ROLE)
                                                        ) {
                                                            return Html::a('<span class="ks-icon la la-user"></span>',
                                                                $url);
                                                          } else {
                                                            return '';
                                                          }
                                                      },

                                                      'view' => function ($url, $model) {
                                                                    return Html::a('<span class="ks-icon la la-eye"></span>',
                                                                        $url);
                                                                },
                                                      'update' => function ($url, $model) {
                                                                    return Html::a('<span class="ks-icon la la-pencil"></span>',
                                                                        $url);
                                                                },
                                                      'delete' => function ($url, $model) {
                                                          return Html::a('<span class="ks-icon la la-trash"></span>',
                                                              $url, [
                                                                    'data-confirm' => 'Are You sure you want to delete this User ?',
                                                                    'data-method' => 'post'
                                                                  ]);
                                                      },
                                                  ],
                                                ],
                                              ]
                                        ]); ?>

                                        <?php } else { ?>

                                        <?= GridView::widget ([
                                            'dataProvider' => $dataProvider,
                                          //  'filterModel' => $searchModel,
                                            'id' => 'pjax_widget',
                                            'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                                            'columns' => [
                                                ['class' => 'yii\grid\SerialColumn'],
                                                'name',
                                                'email', 
                                                [
                                                    'label' => 'Role',
                                                    'value' => function ($model) {
                                                        $roles = Yii::$app->authManager->getRolesByUser($model->id);
                                                        $default_roles = [User::SUBADMIN_ROLE, User::ASSIST_ROLE, User::OUTLET_ROLE, User::SERVICE_ACCOUNT_ROLE];
                                                        foreach ($default_roles as $default_role) {
                                                          if(array_key_exists($default_role, $roles)) {
                                                            unset($roles[$default_role]);
                                                          }
                                                        }
                                                        $pure_roles = array();
                                                        $arry_roles = array_keys($roles);
                                                        foreach ($arry_roles as $role) {
                                                          $rolearr = explode('_', $role);
                                                          if(count($rolearr) > 1)
                                                            $pure_roles[] = $rolearr[1];
                                                        }
                                                      return implode(', ', $pure_roles);
                                                    }
                                                ],    
                                                [
                                                    'label' => 'Access',
                                                    'value' => function ($model) {
                                                     if ($model->role == User::ROLE_ASSIST) { 
                                                        if (!empty($model->multisiteaccess)) {  
                                                            return 'All Websites';
                                                         } 
                                                         if (!empty($model->outlet)) {
                                                          $outletuser = User::findOne(['id' => $model->outlet]);
                                                          return $outletuser->business_name;
                                                         }
                                                         return $model->website_url;
                                                      }
                                                  }
                                                ],
                                               // 'business_name',                                         
                                                [ 'class' => 'yii\grid\ActionColumn',
                                                  'header'=>'Action',
                                                  'headerOptions' => ['class' => 'action-column'],
                                                  'template'=>'{view}{update}{delete}',
                                                  'buttons' => [

                                                      'view' => function ($url, $model) {
                                                                    return Html::a('<span class="ks-icon la la-eye"></span>',
                                                                        $url);
                                                                },
                                                      'update' => function ($url, $model) {
                                                                    return Html::a('<span class="ks-icon la la-pencil"></span>',
                                                                        $url);
                                                                },
                                                      'delete' => function ($url, $model) {
                                                          return Html::a('<span class="ks-icon la la-trash"></span>',
                                                              $url, [
                                                                    'data-confirm' => 'Are You sure you want to delete this User ?',
                                                                    'data-method' => 'post'
                                                                  ]);
                                                      },
                                                  ],
                                                ],
                                            ],
                                        ]); ?>
                                        <?php } ?>

                                     <?php Pjax::end(); ?>
                                   </div>
                              </div>

                          </div>

                      </div>
                  </div>
              </div>
          </div>
      </div>
<!--New Design----->
<?php
$this->registerCss("
  .ks-icon{
    font-size: 20px !important;
    padding-left: 7px;
  }
  .ks-icon:hover{
    color: #f00;
  }
");
$this->registerJs('
    $.pjax.reload({container:"#pjax_widget"});
    jQuery("document").ready(function(){
      jQuery("#dateto,#datefrom").datepicker( {dateFormat: "yy-mm-dd",changeMonth:true,changeYear:true} );
    });
');
?>
