<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\Packages */

$this->title = 'Update User';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss("
  .capitalised_text{
    text-transform: capitalize;
  }
");
?>

<div class="ks-page-header">
            <section class="ks-title">
                <h3><?= Html::encode($this->title) ?></h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
                                      <div class="packages-create">
                                          <?php 
                                            $form = ActiveForm::begin([
                                                'id' => 'login-form',
                                                'options' => ['class' => 'form-horizontal'],
                                            ]) 
                                          ?>

                                           <?= $form->field($model, 'name')->textInput(['value'=>$model->name]) ?>
                                           <?= $form->field($model, 'email')->input('email', ['value'=>$model->email,'readonly'=> true]) ?>


                                          <div class="col-md-3" style="padding-left: 0; padding-bottom: 10px;">
                                            <?= Html::label('Role', ['class' => 'label name']) ?>
                                            <select id='role' name="role" class='form-control'  >              
                                                <option value=''>Select Role</option>
                                                <?php foreach ($roles as $role) { 
                                                  $format_name = explode('_', $role['name']);
                                                  if (!empty($user_roles) && $user_roles[0] == $role['name'])
                                                    echo "<option value='$role[name]' selected>$format_name[1]</option>";
                                                  else
                                                    echo "<option value='$role[name]'>$format_name[1]</option>";       
                                                } 
                                                ?>
                                             </select>
                                          </div>

                                    <?php  
                                      if (Yii::$app->user->can(User::CLIENT_ROLE)
                                                          || Yii::$app->user->can(User::FRANCHISE_ROLE)
                                      ) { 
                                     ?>

                                      <?php if (!empty($website_users) || !empty($outlets)) { ?>
                                          <div style="margin-top: 10px">
                                            <p><strong>Optional</strong><br/>
                                            <span><i>Use the below options to restrict access only to a specific Website or Outlet.</i></span></p>
                                          </div>                          
                                        <?php } ?>

                                      <?php if (!empty($website_users) && count($website_users) > 1) {  ?> 
                                      <div class="col-md-3" style="padding-left: 0; padding-bottom: 10px;">
                                        <?= Html::label('Select Website', ['class' => 'label name']) ?>  
                                          <select id='website_selecter' name="website_user" class='form-control'>              
                                              <option value=''>None</option>
                                              <?php foreach ($website_users as $website_user) {
                                                  $selected = '';
                                                  if ($website_user->user_group == $model->user_group 
                                                    && empty($model->multisiteaccess) && empty($model->outlet))
                                                    $selected = 'selected';                                                 
                                                  echo "<option value='$website_user->user_group' $selected>$website_user->website_url</option>";              
                                              } 
                                              ?>
                                           </select>
                                      </div> 
                                      <?php } ?>

                                          <?php if (!empty($outlets)) { ?>                                            
                                         <div class="col-md-3" style="padding-left: 0; padding-bottom: 10px;">
                                             <?= Html::label('Select Outlet', ['class' => 'label name']) ?>  
                                            <select id='outlet_selecter' name="outlet_user" class='form-control'>
                                               <option value=''>None</option>
                                              <?php foreach ($outlets as $outlet) {
                                                  $selected = '';
                                                  if ($outlet['id'] == $model->outlet)
                                                    $selected = 'selected';
                                                  echo "<option value='$outlet[id]' $selected>$outlet[name] ($outlet[website_url])</option>";
                                              } 
                                              ?>
                                           </select>  
                                            </div> 
                                           <?php } ?>   

                                          <?php } ?>    

                                            <div class="form-group">
                                                <div class="col-lg-offset-1 col-lg-11">
                                                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
                                                </div>
                                            </div>
                                          <?php ActiveForm::end() ?>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
