<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\Packages */

$this->title = 'View User';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss("
  .capitalised_text {
    text-transform: capitalize;
    padding:0;
    margin:0;
  }
");
?>

<div class="ks-page-header">
            <section class="ks-title">
                <h3><?= Html::encode($this->title) ?></h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
                                      <div class="packages-create">
                                        <table class="table table-striped table-bordered detail-view">
                                          <tr>
                                            <th>Name</th>
                                            <td><?= $model->name ?></td>                                        
                                          </tr>
                                          <tr>
                                            <th>Email</th>
                                            <td><?= $model->email ?></td>                                      
                                          </tr>
                                          <tr>
                                            <th>Roles</th>
                                            <td>
                                              <ul class="capitalised_text" style="list-style: none;">
                                              <?php
                                                foreach ($roles as $role) {
                                                  $rolearr = explode('_', $role);
                                                  echo "<li>$rolearr[1]</li>";
                                                }
                                              ?>
                                              </ul>
                                            </td>                                        
                                          </tr>
                                          <!--
                                          <?php if ($model->role == User::ROLE_ASSIST) {  ?> 
                                            <tr>
                                              <th>Group</th>
                                              <td><?= $model->user_group ?></td>
                                            </tr>
                                          <?php } ?>
                                          -->
                                           <?php if ($model->role == User::ROLE_ASSIST) { 
                                              if (!empty($model->multisiteaccess)) {  ?>
                                                <tr>
                                                  <th>Access</th>
                                                  <td>All Websites</td>                                      
                                                </tr>
                                              <?php } else if (empty($model->outlet)) { ?>
                                                 <tr>
                                                  <th>Access</th>
                                                  <td><?= $model->website_url ?></td>
                                                </tr>  
                                              <?php } ?>

                                             <?php if (!empty($model->outlet)) {
                                              $outletuser = User::findOne(['id' => $model->outlet]);
                                               ?>
                                                <tr>
                                                  <th>Access</th>
                                                  <td><?= $outletuser->business_name ?></td>                                  
                                                </tr>
                                              <?php } ?>

                                          <?php } ?>

                                        </table>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
