<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var frontend\models\CouponSearch $searchModel
 */

$this->title = 'Coupons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ks-page-header">
    <section class="ks-title">
        <h3><?php echo $this->title;?></h3>
    </section>
</div>

<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">

                                <p>
                                    <?= Html::a('Create Coupons', ['create'], ['class' => 'btn btn-success']) ?>
                                </p>


                                <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        'id',
                                        'name',
                                        'code',
                                        'discount',
                                        [
                                            'attribute' => 'status',
                                            'value' => function ($model) {
                                                if($model->status == 1){
                                                    return 'Active';
                                                }else{
                                                    return 'Inactive';
                                                }
                                            },
                                            'filter' => Html::activeDropDownList($searchModel,'status',[1 => 'Active', 0 => 'Inactive'],[
                                                'class' => 'form-control',
                                                'prompt' => 'All'
                                            ])
                                        ],
                                        [   'class' => 'yii\grid\ActionColumn',
                                            'header'=>'Action',
                                            'headerOptions' => ['class' => 'action-column'],
                                            'template'=>'{view}{update}{delete}',
                                            'buttons' => [
                                                'view' => function ($url, $model) {
                                                    return Html::a('<span class="ks-icon la la-eye"></span>',
                                                        $url);
                                                },
                                                'update' => function ($url, $model) {
                                                    return Html::a('<span class="ks-icon la la-pencil"></span>',
                                                        $url);
                                                },
                                                'delete' => function ($url, $model) {
                                                    return Html::a('<span class="ks-icon la la-trash"></span>',
                                                        $url, [
                                                            'data-confirm' => 'Are You sure you want to delete this coupon ?',
                                                            'data-method' => 'post'
                                                        ]);
                                                },
                                            ],
                                        ],
                                    ],
                                ]); ?>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerCss("

    .table td{
      padding: 10px !important;
      font-size: 12px !important;
    }

    .ks-icon{
      font-size: 20px !important;
      padding-left: 7px;
    }

    .action-column{
      width: 140px;
    }

    .ks-icon:hover{
      color: #f00;
    }
  ");
?>

<!-- <div class="coupons-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Coupons', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'code',
            'discount',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div> -->
