<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use common\models\User;
?>

<style>
.mb0px {
  margin-bottom: 0px !important;
}
.alert {
    color: #FF0000;
    font-size: 15px;
}
</style>

<div class="ks-page-header">
    <section class="ks-title">
        <h3>Unmark Chat as Lead</h3>
    </section>
</div>

<div class="ks-page-content">
    <div class="ks-page-content-body">
        <?php if(Yii::$app->session->hasFlash('success')):?>
          <div class="alert alert-success">
    				<b>Success!</b> <?php echo Yii::$app->session->getFlash('success'); ?>
    			</div>
       <?php endif; ?>
       <?php if(Yii::$app->session->hasFlash('error')):?>
         <div class="alert alert-danger">
   				<b>Failed!</b> <?php echo Yii::$app->session->getFlash('error'); ?>
   			</div>
       <?php endif; ?>
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <?php
                if(Yii::$app->user->identity->role == User::ROLE_ADMIN){
                ?>
                <div class="row">
                      <div class="col-xl-12 ks-draggable-column">
                          <div id="ks-bar-chart-panel" class="card panel">
                              <h5 class="card-header">
                                Unmark Chat as Lead
                              </h5>
              								<div class="card">
              									<div class="card-block">
                                  <?php $form = ActiveForm::begin([
                                    'id'=>'frm_unmark_chat'
                                  ]); ?>
                                  <div class='row'>
                                    <div class='col-md-6'>
                                      <label class="form-control-label"><b>Chat Id:&nbsp;</b>
                                      </label>
                                      <input name="chat_id" id="chat_id" value="" class="form-control" type="text" required maxlength="15">
                                    </div>
                                  </div>
                                  <div class='row mb0px'>
                                    <div class='col-md-6'>
                  										<fieldset class="form-group">
                                         <button class="btn btn-primary"
                                         name="btn_unmark_lead"
                                         id="btn_unmark_lead">Unmark</button>
                  										</fieldset>
                                    </div>
                                  </div>
                                  <?php ActiveForm::end(); ?>
              									</div>
              								</div>
                          </div>
                      </div>

                  </div>
                <?php
                  }
                ?>
            </div>
        </div>
    </div>
</div>

<?php

$this->registerJs(
    "$('document').ready(function(){
        $('#btn_unmark_lead').click(function(e){
          e.preventDefault();
          var chat_id = $.trim($('#chat_id').val());
          
          if(chat_id != '') {
            swal({
                icon: 'warning',
                title: 'Are you sure?',
                text: 'You will not be able to undo this change!',
                buttons: true,
                dangerMode: true
              })
              .then((willDelete) => {
                if (willDelete) {
                  $('#frm_unmark_chat').submit();
                } else {
                  return false;
                }
              });
          }else {
            swal('Please enter Chat Id');
          }
        });
    })"
);
