<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii2assets\printthis\PrintThis;
use ruskid\stripe\StripeCheckout;
use frontend\models\Invoices;
use app\models\Leads;
use common\models\User;
use common\models\ForwardingEmailAddress;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
//echo $freeLeads;exit;
$this->title = 'Chats';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
#ks-datatable_paginate{
	padding-bottom : 200px;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/swiper/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/widgets/tables.min.css">

		<div class="ks-page-header">
            <section class="ks-title">
                <h3>Chat History</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="container-fluid ks-rows-section">
                    <div class="row">
                        <div class="col-lg-12 ks-panels-column-section">
                            <div class="card panel ks-information ks-light">
                                <h5 class="card-header">


							<div class="btn-group">
							<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
								<?php
								if(isset($_GET["filter"])){
									if($_GET["filter"] == 'crange')
										echo 'Custom Range';
									elseif($_GET["filter"] == 'last7')
										echo 'Last 7days';
									elseif($_GET["filter"] == 'last30')
										echo 'Last 30days';
									else
										echo $_GET["filter"];
								}
								else{
									echo "Last 7days";
								}
								?>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-left" role="menu">
								<?php $today = date('Y-m-d');?>
								<li><a href="<?=Url::to(['chat/index'])?>&filter=today&date_from=<?=$today ;?>&date_to=<?=$today ;?>&group_by=hour">Today</a></li>
								<?php $yesterday = date('Y-m-d');?>
								<?php $last75 = date('Y-m-d',strtotime("-1 days"));?>
								<li>
									<a href="<?=Url::to(['chat/index'])?>&filter=yesterday&date_from=<?=$last75 ;?>&date_to=<?=$last75 ;?>&group_by=hour">Yesterday</a>
								</li>
								<?php $last7 = date('Y-m-d',strtotime("-6 days"));?>
								<li>
									<a href="<?=Url::to(['chat/index'])?>&filter=last7&date_from=<?=$last7 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 7days</a>
								</li>
								<?php $last30 = date('Y-m-d',strtotime("-29 days"));?>
								<li>
						<a href="<?=Url::to(['chat/index'])?>&filter=last30&date_from=<?=$last30 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 30days</a>
								</li>
								<li>
									<a href="<?=Url::to(['chat/index'])?>&filter=crange" id="c_range">Custom Range</a>
								</li>
							</ul>

							<form id="range-form" style="display:none;margin-left : 10px;" class="top-formsearch">
								 <div class="row">
									<div class="col-sm-4">
										<input type="text" placeholder="Date From" class="form-control" id="datefrom" name="date_from">
									</div>
									<div class="col-sm-4">
										<input type="text" placeholder="Date To" class="form-control" id="dateto" name="date_to">
									</div>
									<input type="hidden" name="filter" value="crange">
									<input type="hidden" name="group_by" value="day">
									<div class="col-sm-4">
										<input type="submit" class="btn btn-info" value="Filter">
									</div>
								</div>
							</form>
							<div style="margin-left:10px;">
								 <?php
									echo PrintThis::widget([
										'htmlOptions' => [
											'id' => 'PrintThis',
											'btnClass' => 'btn btn-primary',
											'btnId' => 'btnPrintThis',
											'btnText' => 'Print',
											'btnIcon' => 'fa fa-print'

										],
										'options' => [
											'debug' => false,
											'importCSS' => true,
											'importStyle' => false,
											'loadCSS' => "cube/css/bootstrap/bootstrap.min.css",
											'pageTitle' => "",
											'removeInline' => false,
											'printDelay' => 333,
											'header' => null,
											'formValues' => true,
											'canvas' => true,
										]
									]);
									?>
							</div>
						</div>

						 <!--Total Leads : <?= $total; ?>-->

							<?php if($filter != ''){?>
								<a href="<?php echo Url::to(['/chat/export', 'filter' => $filter, 'date_from' => $date_from, 'date_to' => $date_to, 'group_by' => $group_by]);?>" class="btn btn-primary-outline ks-light pull-right">Download CSV</a>
						<?php }else{?>
								<a href="<?php echo Url::to(['/chat/export']);?>" class="btn btn-primary-outline ks-light pull-right">Download CSV</a>
						<?php }?>


                                </h5>
								<?php $unpaidLeads = 1; // by ravi ?>
								<?php if($unpaidLeads > 0){?>
									<!-- <div class="alert alert-warning">
										<b>Alert!</b> There are new chats Available. Please Buy another Package.
									</div> -->
								<?php }?>

                                <div class="card-block ks-datatable" style="margin-top : 50px;">
                                    <table id="ks-datatable" class="table table-bordered" style="width:100%" width="100%">
                                        <thead>
                                           <tr>
												<th>name</th>
												<th>email</th>
												<!--th>agents</th-->
												<th>city</th>
												<th>state</th>
												<th>country</th>
                        <th>Date</th>
												<th>Transcript</th>
												<th>View Chat</th>
											</tr>
                                        </thead>
                                        <tbody>

                                           <?php

											foreach($chats as $key => $chat){?>

												<tr <?php if($chat['tags'] == 'lead'){?>class="table-warning"<?php }?>>
														<td>
															<?php echo $chat['name'];?>
														</td>

														<td>
															<?php echo $chat['email'];?>
														</td>
														<td><?= $chat['city']; ?></td>
														<td><?= $chat['region']; ?></td>
														<td><?= $chat['country']; ?></td>
														<td><?= date('Y-m-d',$chat['started_timestamp']); ?></td>
                            
													<td>
													<button class="btn btn-primary-outline" data-toggle="modal" data-target="#ks-izi-modal-large-<?php echo $key;?>">Quick View</button>
													<?php if( !$chat['showLead'] ){?>

																	<div id="ks-izi-modal-large-<?php echo $key;?>" class="modal fade" role="dialog">
																		<div class="modal-dialog">

																			<!-- Modal content-->
																			<div class="modal-content">
																			  <div class="modal-header">
																				<h4 class="modal-title">Buy</h4>
																				<button type="button" class="close" data-dismiss="modal">&times;</button>
																			  </div>
																			  <div class="modal-body">
																				 	Please Buy This Lead To View The Content.
																			  </div>
																			  <div class="modal-footer">
																				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																			  </div>
																			</div>

																		  </div>
																	</div>

															<?php }else{?>
																	<div id="ks-izi-modal-large-<?php echo $key;?>" class="modal fade" role="dialog">
																		<div class="modal-dialog">

																			<!-- Modal content-->
																			<div class="modal-content">
																			  <div class="modal-header">
																				<h4 class="modal-title">Chat Transcript</h4>
																				<button type="button" class="close" data-dismiss="modal">&times;</button>
																			  </div>
																			  <div class="modal-body">
																				<table>
																				 <?php if(isset($chat['messages'])){ ?>
																						<?php foreach($chat['messages'] as $messages){ ?>
																						<?php if($messages->user_type !== 'visitor') { ?>
																							<tr>
																							<td>

																								<img src="http://oxnia.com/externalassets/agent-icon.png" alt="User Avatar" class="img-circle">



																									<strong class="primary-font"><?= $messages->author_name;?> (Agent)</strong>
																									<small class="pull-left text-muted">
																										<i class="fa fa-clock-o fa-fw"></i><?= $messages->date;?>
																									</small>

																							</td>
																							<td>
																								<p>
																									<?= $messages->text;?>
																								</p>
																							</td>
																						 </tr>
																						<?php } else{ ?>
																						 <tr>
																							<td>

																									<img src="http://oxnia.com/externalassets/customer-icon.png" alt="User Avatar" class="img-circle">



																									<small class=" text-muted">
																										<i class="fa fa-clock-o fa-fw"></i> <?= $messages->date;?></small>
																									<strong class="pull-left primary-font"><?= $messages->author_name;?> (Visitor)</strong>

																							</td>
																							<td>
																								<p>
																									<?= $messages->text;?>
																								</p>
																							</td>
																						</tr>
																						<?php } //end if ?>
																						<?php } // end foreach?>
																						<?php } // end if?>
																				</table>
																			  </div>
																			  <div class="modal-footer">
																				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																			  </div>
																			</div>

																		  </div>
																	</div>
															<?php }?>
													</td>
													<?php if( $chat['showLead'] || Yii::$app->user->identity->free_access || (Yii::$app->user->identity->role == User::ROLE_OUTLET && Yii::$app->user->identity->parent->free_access)){?>
															<td><a class="btn btn-primary-outline" href="<?= Yii::$app->urlManager->createUrl(['leads/view-chat', 'id' => $chat['id'] ])?>"> View Chat </a></td>
													<?php }
													else{?>
															<td>
																<?php
																	if(Yii::$app->user->identity->stripeToken == ''){
																		echo
																		StripeCheckout::widget([
																			'action' => Url::to(['/invoice/payperlead', 'id' => $chat['id']]),
																			'name' => 'Pay Per Lead',
																			'description' => " Chats id : " . $chat['id'],
																			'amount' => 5 * 100,
																			'image' => Yii::$app->request->baseUrl . '/cube/img/logo.png',

																		]);
																	}else{
																?>
																<a href="<?= Yii::$app->urlManager->createUrl(['invoice/payperleadbysavedcard', 'id' => $chat['id'] ])?>"> <button name="pay" class="btn btn-primary-outline" value="Pay">Pay</button> </a>
																<?php
																	}
																?>
															</td>
													<?php }?>
												</tr>
												<?php
												//}

											}
											?>

                                        </tbody>
                                    </table>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Forward multiple email Modal -->

        <div id="forwardingModal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
              <h4 class="modal-title">Forwarding Emails</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
               <table class="table">
                <thead>
                  <th class="select-all-email">
                    <label class="custom-control custom-checkbox">
                      <input id='select_all_email' type="checkbox" class="custom-control-input select-all-leads">
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description"></span>
                    </label>
                  </th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Team</th>
                </thead>
                <tbody class='forwarding-table-body'>

                  <?php
                    $forwardingEmailAddress = ForwardingEmailAddress::findAll(['user_id' => Yii::$app->user->identity->id]);
                    foreach ($forwardingEmailAddress as $value) {
                  ?>
                      <tr>
                        <td>
                          <label class="custom-control custom-checkbox">
                            <input data-team="<?=$value->team?>" type="checkbox" value="<?=$value->id?>" class="custom-control-input forward-email-check">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-description"></span>
                          </label>
                        </td>
                        <td><?=ucfirst($value->name)?></td>
                        <td><?=$value->email?></td>
                        <td><?= isset($value->forwardingTeam->name) ? $value->forwardingTeam->name : '' ?></td>
                      </tr>
                  <?php
                    }
                  ?>
                  <input type="hidden" name="forwardChatId" id="forwardChatId" value="0">

                </tbody>
               </table>

               <div class="row">
                <div class="col-md-12">

                

                </div>
               </div>
              </div>
              <div class="modal-footer">
              <button type="button" class="btn btn-primary send-forwarding-email">Send email</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
            </div>
        </div>

        <!-- Quick Send Modal -->

        <div id="quickSendModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
              <h4 class="modal-title">Quick Send</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
                <label>Email:</label>
               <input type="email" required name="quickEmailInput" id="quickEmail" class="form-control">
              </div>
              <div class="modal-footer">
              <button type="button" class="btn btn-primary quick-send-email" data-url="<?=Url::to(['leads/quick-send'])?>" >Send email</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>

            </div>
        </div>

      <!-- Quick Send Modal -->

<?php


$this->registerCss("
	.table td{
		padding: 5px 10px !important;
	}

	.table .transcript-col{
		width: 55px !important;
	}

	.table .status-col{
		width: 55px !important;
	}
	.sendEmail:hover{
		color: darkred;
		font-weight: bold;
	}

	span.view-icon:hover{
		color: #f00;
		cursor: pointer;

	}
	button.btn-primary-outline:hover{
		border: solid 1px #f00;
		color: #f00;
		cursor: pointer;
	}

	a.btn-primary-outline:hover{
		border: solid 1px #f00;
		color: #f00;
		cursor: pointer;
	}

	.btn>.ks-icon{
		width: 43px !important;
	}

	.table-container{
		overflow: scroll;
	}

	.pay-all-selected{
		margin: 10px 0 0 10px
	}

	.filter-form{
		padding-left: 30px !important;
	}

	.pay-all-selected:hover{
		cursor: pointer;
	}

	.email-action:hover{
		cursor: pointer;
	}

	.email-action{
		//padding: 0 10px 0 10px !important;
	}

	ul.email-action-ul li{
		color: blue;
	}

	ul.email-action-ul li:hover{
		color: #f00;
		cursor: pointer;
	}

	.custom-checkbox{
		margin: 0px !important;
	}

	.select-all-email{
		width: 10% !important;
		padding-right: 0px !important;
		padding-left: 10px !important;
	}
");
//echo "<pre>"; print_r(Yii::$app->user->identity); exit;
$this->registerJs(
    'jQuery("document").ready(function(){
		jQuery("#dateto,#datefrom").datepicker( {dateFormat: "yy-mm-dd",changeMonth:true,changeYear:true} );

		jQuery("#c_range").click(function(e){
			e.preventDefault();
			$("#range-form").show();
		})
	});'
, \yii\web\View::POS_END);


$this->registerJs("

$('#range-form').on('submit', function(e){
	from = $('#datefrom').val();
		to = $('#dateto').val();
	console.log('From: ' + from);
	console.log('To: ' + to);
	url = '".Url::to(['chat/index'])."&filter=crange&date_from=' + from + '&date_to=' + to + '&group_by=day';
	window.location = url;
	return false;
});
  function validateEmail(email)
  {
   if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
    {
      return (true)
    }
      return (false)
  }


 	$('.email-option').off('click').on('click', function(e){
 		$('#forwardChatId').val($(this).attr('data-chat-id'));
 	});

 	$('#select_all_email').off('click').on('click', function(e){
 		check_count = 0;
 		action = 0;
 		if($(this).prop('checked') == true){
 			action = 1;
 		}

 		$('.forward-email-check').each(function(index, checkboxObject) {
		    if($(this).prop('checked') != true){
		    	if(action){
		    		$(this).prop('checked', true);
		    	}else{
		    		$(this).prop('checked', false);
		    	}

		    }else{
		    	if(action){
		    		$(this).prop('checked', true);
		    	}else{
		    		$(this).prop('checked', false);
		    	}
		    }
		});
 	});

 	$('.send-forwarding-email').off('click').on('click', function(e){
 		emailArray = [];
 		chat_id = $('#forwardChatId').val();

 		if(chat_id == 0){
 			swal('Some problem occured', 'Please try again later after sometime', 'error');
 			return false;
 		}

 		$('.forward-email-check').each(function(index, checkboxObject) {
		    if($(this).prop('checked') == true){
		    	//console.log($(this).val());
		    	emailArray.push($(this).val())
		    }
		});

		if(emailArray.length == 0){
			swal('No email selected', 'Please select alteast 1 email address to send', 'error');
		}else{
			$.ajax({
				type: 'POST',
				url: '".Url::to(['leads/forward-email'])."',
				data: {emailArray: emailArray, chat_id: chat_id},
				success: function(response){
					response = JSON.parse(response);
					console.log(response);
					if(response.success == true){
						swal('Email sent successfully', 'Lead email has been sent', 'success');
					}
				},
				error: function(data){
					console.log(data)
				}
			});
		}
 	});

 	$('.send-to-me').off('click').on('click', function(e){
    url = $(this).attr('data-url');
    email = $(this).attr('data-email');
 		chat_id = $(this).attr('data-chat-id');
 		swal({
	      title: 'Are you sure ?',
	      text: 'Send an email to ' + email,
	      icon: 'warning',
	      buttons: true,
	      dangerMode: true,
        closeOnConfirm: false,
	    })
	    .then((willDelete) => {
	      if (willDelete) {
          $.ajax({
    				type: 'POST',
    				url: url,
    				data: {chat_id: chat_id},
    				success: function(response){
    					response = JSON.parse(response);
    					console.log(response);
    					if(response.success == true){
    						swal('Email sent successfully', 'Recepient:  ' + email, 'success');
    					}
    				},
    				error: function(data){
    					console.log(data)
    				}
    			});
	      } else {
	        swal('Email not sent!');
	      }
	    });
 	});

 	$('.quick-send-email').off('click').on('click', function(e){
 		email = $('#quickEmail').val();
 		chat_id = $('#forwardChatId').val();

 		if(chat_id == 0){
 			swal('Some problem occured', 'Please try again later after sometime', 'error');
 			return false;
 		}

 		url = $(this).attr('data-url') + '&chat_id=' + chat_id + '&quick_email=' + email;
 		if(email == ''){
 			swal('Email cannot be left blank', 'Please provide email address', 'error');
 			return false;
 		}

 		if(validateEmail(email)){
 			$.ajax({
				type: 'POST',
				url: url,
				data: {quick_email: email, chat_id: chat_id},
				success: function(response){
					response = JSON.parse(response);
					console.log(response);
					if(response.success == true){
						swal('Email sent successfully', 'Recepient:  ' + email, 'success');
					}
				},
				error: function(data){
					console.log(data)
				}
			});
 		}else{
 			swal('Email not valid', 'Please provide a valid email address', 'error');
 			return false;
 		}
 	});

 	$('.select-team').off('click').on('click', function(e){
 		team_id = $(this).attr('data-id');
 		check = $(this).prop('checked');
 		$('.forward-email-check').each(function(index, checkboxObject) {
 			if(check){
 				if($(this).attr('data-team') == team_id){
			    	$(this).prop('checked', true);
			    }
 			}else{
 				if($(this).attr('data-team') == team_id){
			    	$(this).prop('checked', false);
			    }
 			}

		});
 	});

");
