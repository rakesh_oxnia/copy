<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reports';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="chatwind col-md-12">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<form >
		Date From<input type="text" id="datefrom" name="date_from">
		Date TO<input type="text" id="dateto" name="date_to">
		<input type="submit" value="submit">
	</form>
<br>

    <?php 
		
		if(count($reports)){ ?>
			
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>DATE FROM</th>
						<th>DATE TO</th>
						<th>Chats</th>
						<th>Missed chats</th>
					</tr>
				</thead>
				<tbody>
					<?php
					//echo '<pre>';print_r($chats->chats[0]);echo '</pre>';
					foreach($reports as $report){ ?>
						<tr>
							<td><?= $report->begin; ?></td>
							<td><?= $report->end; ?></td>
							<td><?= $report->chats; ?></td>
							<td><?= $report->missed_chats; ?></td>
						</tr>
						<?php 
					}
					}	
					?>
				</tbody>
			</table>
			<?php
		
	?>

</div>
<?php 

$this->registerJs(
    '$("document").ready(function(){ jQuery("#dateto,#datefrom").datepicker( {dateFormat: "yy-mm-dd",changeMonth:true,changeYear:true} ); });'
, \yii\web\View::POS_END);