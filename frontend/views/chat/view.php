<?php use yii\helpers\Html;

use common\models\User;

$this->title = "Chats Transcript"; 
 
?>

<div class="ks-page-header">
            <section class="ks-title">
                <h3>Chats Transcript</h3>

                <button class="btn btn-primary-outline ks-light ks-activity-block-toggle" data-block-toggle=".ks-activity-dashboard > .ks-activity">Activity</button>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-dashboard-tabbed-sidebar">
                    <div class="ks-dashboard-tabbed-sidebar-widgets">
					 <div class="chatwind col-md-12">

    <div class="chat-panel panel panel-default">
		<div class="panel-heading">
			<i class="fa fa-comments fa-fw"></i>
			Chat History
		</div>
        <!-- /.panel-heading -->
		<div class="panel-body">
			 
				<?php if(isset($chat->messages)){ ?>
				<?php foreach($chat->messages as $messages){ ?>
				<?php if($messages->user_type !== 'visitor') { ?>
				 
					<span class="chat-img pull-left">
						<img src="http://oxnia.com/externalassets/agent-icon.png" alt="User Avatar" class="img-circle"><!-- Chat Icon visitor-->
					</span>
					<div class="chat-body clearfix">
						<div class="header-chat">
							<strong class="primary-font"><?= $messages->author_name;?> (Agent)</strong>
							<small class="pull-right text-muted">
								<i class="fa fa-clock-o fa-fw"></i><?= $messages->date;?>
							</small>
						</div>
						<p>
							<?= $messages->text;?>
						</p>
					</div>
				 
				<?php } else{ ?>
				 
					<span class="chat-img pull-right">
						<img src="http://oxnia.com/externalassets/customer-icon.png" alt="User Avatar" class="img-circle"><!-- Chat Icon Agent-->
					</span>
					<div class="chat-body clearfix">
						<div class="header-chat">
							<small class=" text-muted">
								<i class="fa fa-clock-o fa-fw"></i> <?= $messages->date;?></small>
							<strong class="pull-right primary-font"><?= $messages->author_name;?> (Visitor)</strong>
						</div>
						<p>
							<?= $messages->text;?>
						</p>
					</div>
				 
				<?php } //end if ?>
				<?php } // end foreach?>
				<?php } // end if?>
			 
		</div>
		<!-- /.panel-body -->
	</div>

</div>
                        
 
                    </div>
                   
                </div>
            </div>
        </div>
	 