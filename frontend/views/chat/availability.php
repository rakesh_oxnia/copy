<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chat Availability';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="chatwind col-md-12">
	<div class="row">
		<div class="col-lg-12">
			<div id="content-header" class="clearfix">
				<div class="pull-left">
					<ol class="breadcrumb">
						<li><a href="<?= Yii::$app->homeUrl;?>">Home</a></li>
						<li class="active"><span><?= Html::encode($this->title) ?></span></li>
					</ol>
					<h1><?= Html::encode($this->title) ?></h1>
				</div>
			</div>
		</div>
	</div>
    <div class="row">
		<div class="col-md-12">
			<div class="main-box">
				<div class="main-box-body clearfix">
					<div class="row">
						<br>
						Filter By <div class="btn-group">
							<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
								<?php 
								if(isset($_GET["filter"])){
									if($_GET["filter"] == 'crange')
										echo 'Custom Range';
									elseif($_GET["filter"] == 'last7')
										echo 'Last 7days';
									elseif($_GET["filter"] == 'last30')
										echo 'Last 30days';
									else
										echo $_GET["filter"];
								}
								else{
									echo "Last 7days";
								}
								?>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<?php $today = date('Y-m-d');?>
								<li><a href="?filter=today&date_from=<?=$today ;?>&date_to=<?=$today ;?>&group_by=hour">Today</a></li>
								<?php $yesterday = date('Y-m-d');?>
								<?php $last75 = date('Y-m-d',strtotime("-1 days"));?>
								<li><a href="?filter=yesterday&date_from=<?=$last75 ;?>&date_to=<?=$last75 ;?>&group_by=hour">Yesterday</a></li>
								<?php $last7 = date('Y-m-d',strtotime("-6 days"));?>
								<li><a href="?filter=last7&date_from=<?=$last7 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 7days</a></li>
								<?php $last30 = date('Y-m-d',strtotime("-29 days"));?>
								<li><a href="?filter=last30&date_from=<?=$last30 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 30days</a></li>
								<li><a href="?filter=crange" id="c_range">Custom Range</a></li>
							</ul>
						</div>					
						<div class="daterun">
							<form id="range-form" style="display:none;" class="top-formsearch">
								<ul> 
									<li><label>Date From</label><input type="text" id="datefrom" name="date_from"></li>
									<li><label>Date TO</label><input type="text" id="dateto" name="date_to"></li>
									<input type="hidden" name="filter" value="crange">
									<input type="hidden" name="group_by" value="day">
									<li><input type="submit" value="Filter"></li>
								</ul>								
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
<?php

function convertmin($total_availablity){
	$return = '';
	//echo $total_availablity."--";
	$avblk = explode('.',$total_availablity);
	if($avblk[0]){
		$return .= floor($avblk[0] / 60).".";
		$return .= ($avblk[0] % 60);
	}else{
		$return = '0.0';
	}
	
	return $return;
}

function converthours($total_availablity){
	$return = '';
	if($total_availablity){
		$avblk = explode('.',$total_availablity);
		$return .=$avblk[0].".";
		if(isset($avblk[1])){
			$return .=sprintf('%0d',($avblk[1]*60)/100);
		}else{
			$return .="0";
		}
	}else{
		$return = '0.0';
	}
	
	return $return;
}

$data_str = '';

$total_availablity = 0;
$num_m = 0;
$num_m2 = 0;
$divide = 1;
$show_bars = 1;
$show_lines = 0;
$show_points = 0;
foreach($availability as $key => $ds){
	if(isset($_GET['group_by']) && $_GET['group_by']=='hour'){
		$data_str .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.convertmin($ds->minutes).'],';
		$total_availablity += $ds->minutes;
		$axisLabel = 'Minutes';
		$num_m = 1;
		$divide = 24;
	}else{
		$data_str .= '['.strtotime($key)*1000 .','.converthours($ds->hours).'],';
		$total_availablity += $ds->hours;
		$axisLabel = 'Hours';
		$num_m++;
		$num_m2++;
	}
}
if($num_m2 > 10){
	$show_bars = 0;
	$show_lines = 1;
	$show_points = 1;
}

$show_bars = 0;
$show_lines = 1;
$show_points = 1;

?>	

	<div class="row">
		<div class="col-md-12">
			<div class="main-box">
				<header class="main-box-header clearfix">
					<h2 class="pull-left">Chat availability</h2>
				</header>
				<div class="main-box-body clearfix">
					<div class="row">
						<div class="col-md-9">
							<div id="graph-bar" style="height: 240px; padding: 0px; position: relative;"></div>
						</div>
						<div class="col-md-3">
							<?php if(isset($_GET['group_by']) && $_GET['group_by']=='hour'){
								$avblk = explode('.',$total_availablity);
								echo 'availability: ';
								if($avblk[0]){
									echo $hours = floor($avblk[0] / 60);
    echo " h ".$minutes = ($avblk[0] % 60)." min";
								}else{
									echo '0 h 0 min';
								}
							}else{
								$avblk = explode('.',$total_availablity);
								echo "availability: ".$avblk[0]." h ";
								if(isset($avblk[1])){
									echo sprintf('%2d',($avblk[1]*60)/100) ." min ";
								}
								else{
									echo "0 min";
								}
							} ?>
							<p><?php //= $total_availablity.' '. $axisLabel;?> </p>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>

</div>

<?php 

$this->registerJs(
    "$('document').ready(function(){
		jQuery('#dateto,#datefrom').datepicker( {dateFormat: 'yy-mm-dd',changeMonth:true,changeYear:true} );
		
		$('#c_range').click(function(e){
			e.preventDefault();
			$('#range-form').show();
		})
		function gd(year, day, month) {
			return new Date(year, month, day).getTime();
		}
		if ($('#graph-bar').length) {
			var data1 = [
			    $data_str
			];
			
			
			var series = new Array();
			
			series.push({
				data: data1,
				bars: {
					show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000 / parseInt($divide),
					lineWidth: 1 / parseInt($num_m),
					fill: 1,
					align: 'center'
				},
				label: 'Availability',
				lines: {
					show : $show_lines,
					//fill: 1,
				},
				points: { 
					show: $show_points 
				}
			});
			
			$.plot('#graph-bar', series, {
				colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
				ticks : 10,
				grid: {
					tickColor: '#f2f2f2',
					//borderWidth: 0,
					hoverable: true,
					//clickable: true
				},
				legend: {
					noColumns: 1,
					// labelBoxBorderColor: '#000000',
					// position: 'ne'     
				},
				shadowSize: 0,
				xaxis: {
					mode: 'time',
					//timeformat: '%d %b',
					//timeformat:'%S'
				},
				yaxis:{
					axisLabel : '$axisLabel',
					tickDecimals:true,
					minTickSize:1
				}
			});

			var previousPoint = null;
			$('#graph-bar').bind('plothover', function (event, pos, item) {
				if (item) {
					if (previousPoint != item.dataIndex) {

						previousPoint = item.dataIndex;

						$('#flot-tooltip').remove();
						var x = item.datapoint[0],
						y = item.datapoint[1];
						
						showTooltip(item.pageX, item.pageY, item.series.label, y.toString() );
					}
				}
				else {
					$('#flot-tooltip').remove();
					previousPoint = [0,0,0];
				}
			});
			function showTooltip(x, y, label, data) {
				
				if(data2 = data.replace('.', ' h ')){
					var restr = data2.match(/h/g);
					if(restr){
						data22 = data2+' min';
					}
					else{
						data22 = data+' h';
					}
				}else{
					data22 = data+' h';
				}
				$('<div id=\"flot-tooltip\">' + '<b>' + label + ': </b><i>' + data22 + '</i>' + '</div>').css({
					top: y + 5,
					left: x + 20
				}).appendTo('body').fadeIn(200);
			}
		}
		
	});"
, \yii\web\View::POS_END);