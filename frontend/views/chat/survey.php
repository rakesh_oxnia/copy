<?php use yii\helpers\Html;

use common\models\User;

$this->title = "Chats Survey"; 
 
?>

<div class="ks-page-header">
            <section class="ks-title">
                <h3>Chats Survey</h3>

                <button class="btn btn-primary-outline ks-light ks-activity-block-toggle" data-block-toggle=".ks-activity-dashboard > .ks-activity">Activity</button>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-dashboard-tabbed-sidebar">
                    <div class="ks-dashboard-tabbed-sidebar-widgets">
					 <div class="chatwind col-md-12">

    <div class="panel panel-default">
		<div class="panel-heading">
			<i class="fa fa-comments fa-fw"></i>
			Prechat Survey
		</div>
        <!-- /.panel-heading -->
		<div class="panel-body">
			 
				<?php if(isset($chat->prechat_survey)&& !empty($chat->prechat_survey)){ 
				foreach($chat->prechat_survey as $prechat_survey){ ?>
				 
					<p><b>Question : </b><?= $prechat_survey->key;?></p>
					<p><b>Answer : </b><?= $prechat_survey->value;?></p>
				 
				<?php } }else{
				?>
			 
					<p>Survey Not found</p>
				 
				<?php
}				// end foreach?>
			 
		</div>
		<!-- /.panel-body -->
	</div>

    <div class="panel panel-default">
		<div class="panel-heading">
			<i class="fa fa-comments fa-fw"></i>
			Postchat Survey
		</div>
        <!-- /.panel-heading -->
		<div class="panel-body">
			 
				<?php if(isset($chat->postchat_survey) && !empty($chat->postchat_survey)){ 
				foreach($chat->postchat_survey as $postchat_survey){ ?>
				 
					<p><b>Question : </b><?= $postchat_survey->key;?></p>
					<p><b>Answer : </b><?= $postchat_survey->value;?></p>
					
				 
				<?php }
                ?>
				<p>&nbsp</p>
				<p><b>Rating : </b><?php
									 if($chat->rate == 'rated_bad'){
										echo '<span class="text-danger fa-stack"><i class="fa fa-thumbs-down fa-stack-2x"></i></span>';
									} elseif($chat->rate == 'rated_good'){
										echo '<span class="text-success fa-stack"><i class="fa fa-thumbs-up fa-stack-2x"></i></span>';
									} else{
										echo '--';
									};
								?></p>
				<?php
				}else{
				?>
				 
					<p>Survey Not found</p>
				 
				<?php
}		 // end foreach?>
		 
		</div>
		<!-- /.panel-body -->
	</div>

</div>
                        
 
                    </div>
                   
                </div>
            </div>
        </div>
	 