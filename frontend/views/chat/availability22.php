<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reports';
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= Html::encode($this->title) ?></h1>
<div class="chatwind col-md-12">

    <div class="row">
		<div class="col-md-12">
			<div class="main-box">
				<div class="main-box-body clearfix">
					<div class="row">
						<br>
						Filter By <div class="btn-group">
							<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
								<?php 
								if(isset($_GET["filter"])){
									if($_GET["filter"] == 'crange')
										echo 'Custom Range';
									elseif($_GET["filter"] == 'last7')
										echo 'Last 7days';
									elseif($_GET["filter"] == 'last30')
										echo 'Last 30days';
									else
										echo $_GET["filter"];
								}
								else{
									echo "Last 7days";
								}
								?>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<?php $today = date('Y-m-d');?>
								<li><a href="?filter=today&date_from=<?=$today ;?>&date_to=<?=$today ;?>&group_by=hour">Today</a></li>
								<?php $yesterday = date('Y-m-d',strtotime("-1 days"));?>
								<li><a href="?filter=yesterday&date_from=<?=$yesterday ;?>&date_to=<?=$yesterday ;?>&group_by=hour">Yesterday</a></li>
								<?php $last7 = date('Y-m-d',strtotime("-7 days"));?>
								<li><a href="?filter=last7&date_from=<?=$last7 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 7days</a></li>
								<?php $last30 = date('Y-m-d',strtotime("-30 days"));?>
								<li><a href="?filter=last30&date_from=<?=$last30 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 30days</a></li>
								<li><a href="?filter=crange" id="c_range">Custom Range</a></li>
							</ul>
						</div>					
						<div class="daterun">
							<form id="range-form" style="display:none;" class="top-formsearch">
								<ul> 
									<li><label>Date From</label><input type="text" id="datefrom" name="date_from"></li>
									<li><label>Date TO</label><input type="text" id="dateto" name="date_to"></li>
									<input type="hidden" name="filter" value="crange">
									<input type="hidden" name="group_by" value="day">
									<li><input type="submit" value="Filter"></li>
								</ul>								
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
	<?php

	function convertmin($total_availablity){
		$return = '';
		//echo $total_availablity."--";
		$avblk = explode('.',$total_availablity);
		if($avblk[0]){
			$return .= floor($avblk[0] / 60).".";
			$return .= ($avblk[0] % 60);
		}else{
			$return = '0.0';
		}
		
		return $return;
	}

	function converthours($total_availablity){
		$return = '';
		if($total_availablity){
			$avblk = explode('.',$total_availablity);
			echo '<pre>';print_r($avblk);echo '</pre>';die;
			$return .=$avblk[0].".";
			if($avblk[1]){
				$return .=sprintf('%0d',($avblk[1]*60)/100);
			}else{
				$return .="0";
			}
		}else{
			$return = '0.0';
		}
		
		return $return;
	}

	$data_str = '';

	$total_availablity = 0;
	$num_m = 0;
	$num_m2 = 0;
	$divide = 1;
	$show_bars = 1;
	$show_lines = 0;
	$show_points = 0;
	foreach($availability as $key => $ds){
		if(isset($_GET['group_by']) && $_GET['group_by']=='hour'){
			$data_str .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.convertmin($ds->minutes).'],';
			$total_availablity += $ds->minutes;
			$axisLabel = 'Minutes';
			$num_m = 1;
			$divide = 24;
		}else{
			$data_str .= '['.strtotime($key)*1000 .','.converthours($ds->hours).'],';
			$total_availablity += $ds->hours;
			$axisLabel = 'Hours';
			$num_m++;
			$num_m2++;
		}
	}
	if($num_m2 > 10){
		$show_bars = 0;
		$show_lines = 1;
		$show_points = 1;
	}

	$show_bars = 0;
	$show_lines = 1;
	$show_points = 1;

	?>
	<div class="row">
		<div class="col-md-12">
			<div class="main-box">
				<header class="main-box-header clearfix">
					<h2 class="pull-left">Chat availability</h2>
				</header>
				<div class="main-box-body clearfix">
					<div class="row">
						<?php echo '<pre>';print_r($availability);echo '</pre>'; ?>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>