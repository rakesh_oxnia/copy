<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>
<div class="ks-page-header">
          <section class="ks-title">
              <h3>Chat History</h3>
          </section>
      </div>
      <div class="ks-page-content">
        <div class="ks-page-content-body">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                  <div class="row">
                                    <div class="col-md-2">
                                      <div class="btn-group">
                                      <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
                                        <?php
                                        if(isset($_GET["filter"])){
                                          if($_GET["filter"] == 'crange')
                                            echo 'Custom Range';
                                          elseif($_GET["filter"] == 'last7')
                                            echo 'Last 7days';
                                          elseif($_GET["filter"] == 'last30')
                                            echo 'Last 30days';
                                          else
                                            echo $_GET["filter"];
                                        }
                                        else{
                                          echo "Last 7days";
                                        }
                                        ?>
                                        <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu pull-left" role="menu">
                                        <?php $today = date('Y-m-d');?>
                                        <li>
                                <a href="<?=Url::to(['chat/index2'])?>&filter=today&date_from=<?=$today ;?>&date_to=<?= $today ;?>&group_by=hour">Today</a></li>
                                        <?php $yesterday = date('Y-m-d');?>
                                        <?php $last75 = date('Y-m-d',strtotime("-1 days"));?>
                                        <li>
                                          <a href="<?=Url::to(['chat/index2'])?>&filter=yesterday&date_from=<?=$last75 ;?>&date_to=<?=$last75 ;?>&group_by=hour">Yesterday</a>
                                        </li>
                                        <?php $last7 = date('Y-m-d',strtotime("-6 days"));?>
                                        <li>
                                          <a href="<?=Url::to(['chat/index2'])?>&filter=last7&date_from=<?=$last7 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 7days</a>
                                        </li>
                                        <?php $last30 = date('Y-m-d',strtotime("-29 days"));?>
                                        <li>
                                      <a href="<?=Url::to(['chat/index2'])?>&filter=last30&date_from=<?=$last30 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 30days</a>
                                        </li>
                                        <li>
                                          <a href="?filter=crange" id="c_range">Custom Range</a>
                                        </li>
                                      </ul>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <form id="range-form" style="display:none;margin-left : 10px;" class="top-formsearch">
                        								 <div class="row">
                        									<div class="col-sm-4">
                        										<input type="text" placeholder="Date From" class="form-control" id="datefrom" autocomplete="off" name="date_from">
                        									</div>
                        									<div class="col-sm-4">
                        										<input type="text" placeholder="Date To" class="form-control" id="dateto" autocomplete="off" name="date_to">
                        									</div>
                        									<input type="hidden" name="filter" value="crange">
                        									<input type="hidden" name="group_by" value="day">
                        									<div class="col-sm-4">
                        										<input type="submit" class="btn btn-info" value="Filter">
                        									</div>
                        								</div>
                        							</form>
                                    </div>
                                    <div class="col-md-4">
                                      <?php if(isset($_GET['filter']) != ''){?>
                                        <a href="<?php echo Url::to(['/chat/export-chat', 'filter' => $_GET['filter'], 'date_from' => $_GET['date_from'], 'date_to' => $_GET['date_to'], 'group_by' => $_GET['group_by']]);?>" class="btn btn-primary-outline ks-light pull-right">Download CSV</a>
                                    <?php }else{?>
                                        <a href="<?php echo Url::to(['/chat/export-chat']);?>" class="btn btn-primary-outline ks-light pull-right">Download CSV</a>
                                    <?php }?>
                                    </div>
                                  </div>
                                  <div class="card-block ks-datatable">
                                    <div class="table-responsive">
                                      <table id="ks-datatable" class="table table-bordered">
                                          <thead>
                                             <tr>
                                              <th>Name</th>
                       												<th>Email</th>
                       												<!--th>agents</th-->
                       												<th>City</th>
                       												<th>State</th>
                                              <th>Country</th>
                       												<th>Date</th>
                       												<th>Transcript</th>
                       												<th>View Chat</th>
                                          </thead>
                                          <tbody>
                                            <?php
                                            foreach($chats as $chat)
                                            {
                                              ?>
                                            <tr>
                                              <td><?php if(isset($chat->leads)){
                                                echo $chat->leads->visitor_name;
                                              }else{
                                                echo '-';
                                              };?></td>
                                              <td><?php if(isset($chat->leads)){
                                                echo $chat->leads->visitor_email;
                                              }else{
                                                echo '-';
                                              };?></td>
                                              <td><?php echo $chat->city;?></td>
                                              <td><?php echo $chat->region;?></td>
                                              <td><?php echo $chat->country;?></td>
                                              <td><?php echo date('Y-m-d',  $chat->ended_timestamp	);?></td>
                                              <td><button class="btn btn-primary-outline view-transcript" data-toggle="modal" data-target="#ks-izi-modal-large-chat" data-id="<?= $chat->id ?>">Quick View</button></td>
                                              <td><a class="btn btn-primary-outline" href="<?= Yii::$app->urlManager->createUrl(['leads/view-chat', 'id' => $chat->chat_id ])?>"> View Chat </a></td>
                                            </tr>
                                            <?php
                                          }
                                            ?>
                                          </tbody>
                                        </table>
                                    </div>
                                      </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>

                  <!---Model--->
                  <div id="ks-izi-modal-large-chat" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                        <h4 class="modal-title chat-title"></h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body chat-message">
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                      </div>
                  </div>
                  <!---Model--->

      <?php
      $this->registerJs("
      $('#ks-datatable').off('click').on('click','.view-transcript', function(e){
        e.preventDefault();
        id = $(this).attr('data-id');
        $('.chat-message').empty();
        $('.chat-message').append('<h4>Loading transcript . . . . </h4>');
        $.ajax({
          type: 'post',
          url: '".Url::to(['lead-list/load-chat-message'])."',
          data: {id: id},
          success: function(response){

            response = JSON.parse(response);
            console.log(response.data);

            if(response.success == true)
            {
              if(response.data != 'Expired'){
              message_array = response.data;
              tr_str = '';
              for(i = message_array.length - 1; i >= 0 ; i--)
              {
                author_name = message_array[i].author_name;
                text = message_array[i].text;
                date = message_array[i].date;
                user_type = message_array[i].user_type;
                console.log('author_name: ' + author_name);
                if(user_type == 'visitor'){
                  tr_str = \" <tr><td>  <img src='http://oxnia.com/externalassets/customer-icon.png' alt='User Avatar' class='img-circle'>   <small class=' text-muted'>   <i class='fa fa-clock-o fa-fw'></i> \" + date + \" </small>   <strong class='pull-left primary-font'>\" + author_name + \" (Visitor)</strong> </td>   <td> <p> \" + text + \" </p>   </td>  </tr> \" + tr_str;
                }else{
                  tr_str = \"<tr><td><img src='http://oxnia.com/externalassets/agent-icon.png' alt='User Avatar' class='img-circle'>  <strong class='primary-font'> \"  + author_name + \"  (Agent)</strong>  <small class='pull-left text-muted'> <i class='fa fa-clock-o fa-fw'></i> \" + date + \"  </small> </td><td>    <p>  \" + text +\"  </p>  </td> </tr>\" + tr_str;
                }
              }
              $('.chat-message').empty();
              $('.chat-title').empty();
              $('.chat-title').text('Chat Transcript');
              $('.chat-message').append('<table class=\' table dataTable no-footer \'> ' + tr_str + ' </table>');
              //alert('Table added');
              //console.log(response.data);
            }else{
              $('.chat-message').empty();
              $('.chat-title').empty();
              $('.chat-title').text('Buy');
              $('.chat-message').text('Please Buy This Lead To View The Content.');
            }
            }
          },
          error: function(data){
            console.log(data);
          }
        })
      });
      
////////////////////////////////////////
$('#range-form').on('submit', function(e){
			from = $('#datefrom').val();
				to = $('#dateto').val();
			console.log('From: ' + from);
			console.log('To: ' + to);
			url = '".Url::to(['chat/index2'])."&filter=crange&date_from=' + from + '&date_to=' + to + '&group_by=day';
			window.location = url;
			return false;
		});
////////////////////////////////////////////////
jQuery('#dateto,#datefrom').datepicker( {dateFormat: 'yy-mm-dd',changeMonth:true,changeYear:true} );
jQuery('#c_range').click(function(e){
  e.preventDefault();
  $('#range-form').show()
})
///////////////////////////////////////////////
      ");
      ?>
