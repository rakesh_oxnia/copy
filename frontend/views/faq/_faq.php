<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?> 
<li>
	<h3 class="title">
	<a href="#">
		<?php echo $model->topic;?>
	</a>
	</h3>
	<div class="clearfix">						
		<div class="meta-info">								 
			<div class="desc">
			<?php echo $model->answer;?>
			</div>
		</div>
	</div>
</li>