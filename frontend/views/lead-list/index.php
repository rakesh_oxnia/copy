<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii2assets\printthis\PrintThis;
use ruskid\stripe\StripeCheckout;
use app\models\Leads;
use frontend\models\Invoices;
use common\models\User;
use common\models\ForwardingEmailAddress;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\LeadListSearch $searchModel
 */

 $this->title = 'Leads';
 $this->params['breadcrumbs'][] = $this->title;
 ?>
 <style>
 #ks-datatable_paginate{
 	padding-bottom : 200px;
 }
 </style>
 <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/swiper/css/swiper.min.css">
 <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/widgets/tables.min.css">



 		<div class="ks-page-header">
             <section class="ks-title">
                 <h3>Leads</h3>

             </section>
         </div>

         <div class="ks-page-content">
             <div class="ks-page-content-body">

                 <div class="">
                     <div class="row">

                         <div class="col-lg-12 ks-panels-column-section">
                             <div class="card panel ks-information ks-light">
                                 <h5 class="card-header">

 								<div class="btn-group">
 									<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
 										<?php
 										if(isset($_GET["filter"])){
 											if($_GET["filter"] == 'crange')
 												echo 'Custom Range';
 											elseif($_GET["filter"] == 'last7')
 												echo 'Last 7days';
 											elseif($_GET["filter"] == 'last30')
 												echo 'Last 30days';
 											else
 												echo $_GET["filter"];
 										}
 										else{
 											echo "Last 7days";
 										}
 										?>
 										<span class="caret"></span>
 									</button>
 									<ul class="dropdown-menu pull-left" role="menu">
 										<?php $today = date('Y-m-d');?>
 										<li><a href="<?=Url::to(['leads/index'])?>&filter=today&date_from=<?=$today ;?>&date_to=<?=$today ;?>&group_by=hour">Today</a></li>
 										<?php $yesterday = date('Y-m-d');?>
 										<?php $last75 = date('Y-m-d',strtotime("-1 days"));?>
 										<li><a href="<?=Url::to(['leads/index'])?>&filter=yesterday&date_from=<?=$last75 ;?>&date_to=<?=$last75 ;?>&group_by=hour">Yesterday</a></li>
 										<?php $last7 = date('Y-m-d',strtotime("-6 days"));?>
 										<li><a href="<?=Url::to(['leads/index'])?>&filter=last7&date_from=<?=$last7 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 7days</a></li>
 										<?php $last30 = date('Y-m-d',strtotime("-29 days"));?>
 										<li><a href="<?=Url::to(['leads/index'])?>&filter=last30&date_from=<?=$last30 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 30days</a></li>
 										<li><a href="<?=Url::to(['leads/index'])?>&filter=crange" id="c_range">Custom Range</a></li>
 									</ul>

 									&nbsp;
 									&nbsp;


 									<form id="range-form" style="display:none;margin-left : 10px;" class="top-formsearch">
 										 <div class="row">
 											<div class="col-sm-4">
 												<input type="text" placeholder="Date From" class="form-control" id="datefrom" name="date_from">
 											</div>
 											<div class="col-sm-4">
 												<input type="text" placeholder="Date To" class="form-control" id="dateto" name="date_to">
 											</div>
 											<input type="hidden" name="filter" value="crange">
 											<input type="hidden" name="group_by" value="day">
 											<div class="col-sm-4">
 												<input type="submit" class="btn btn-info" value="Filter">
 											</div>
 										</div>
 									</form>
 									<div style="margin-left:10px;">
 										 <?php
 											echo PrintThis::widget([
 												'htmlOptions' => [
 													'id' => 'PrintThis',
 													'btnClass' => 'btn btn-primary',
 													'btnId' => 'btnPrintThis',
 													'btnText' => 'Print',
 													'btnIcon' => 'fa fa-print'

 												],
 												'options' => [
 													'debug' => false,
 													'importCSS' => true,
 													'importStyle' => false,
 													'loadCSS' => "cube/css/bootstrap/bootstrap.min.css",
 													'pageTitle' => "",
 													'removeInline' => false,
 													'printDelay' => 333,
 													'header' => null,
 													'formValues' => true,
 													'canvas' => true,
 												]
 											]);
 											?>
 									</div>
 								</div>


 						 <!--Total Leads : <?php //echo $total; ?>-->

 						 <?php
              $filter = '';
 						 	$cat_param = isset($_GET['category']) ? '&category=' . $_GET['category'] : '&category=';
 						 	$status_param = isset($_GET['status']) ? '&status=' . $_GET['status'] : '&status=';
 						 	$outlet_param = isset($_GET['outlet']) ? '&outlet=' . $_GET['outlet'] : '&outlet=';
 							$blank_filter = '&filter=&date_from=&date_to=&group_by=&page=';
 						 ?>

 							<?php if($filter != ''){?>
 								<a data-count="" href="<?php echo Url::to(['/leads/export', 'filter' => $filter, 'date_from' => $date_from, 'date_to' => $date_to, 'group_by' => $group_by]) . $cat_param . $status_param . $outlet_param;?>" class="btn btn-primary-outline ks-light pull-right download-csv-btn">Download CSV</a>
 						<?php }else{?>
 								<a data-count="" href="<?php echo Url::to(['/leads/export']). $blank_filter . $cat_param . $status_param . $outlet_param ;?>" class="btn btn-primary-outline ks-light pull-right download-csv-btn">Download CSV</a>
 						<?php }?>


                     </h5>

 					<?php if(Yii::$app->getSession()->hasFlash('success')){?>
 						<div class="alert alert-success">
 							<?php echo Yii::$app->getSession()->getFlash('success');?>
 						</div>
 					<?php }?>


                 	<div class="row">
                 		<?php
                 			$showPayAllButton = 1;
                 			if(Yii::$app->user->identity->free_access){
 								        $showPayAllButton = 0;
 								    }elseif (Yii::$app->user->identity->role == User::ROLE_OUTLET && Yii::$app->user->identity->parent->free_access) {
 								        $showPayAllButton = 0;
 								    }

 						    if($showPayAllButton){
 						?>
 								<div class="col-md-3 filter-form">
 									<button class="btn btn-default pay-all-selected">Pay for Selected Leads</button>
 								</div>
 						<?php
 						    }
                 		?>

                 		<div class="col-md-2 filter-form">
                 			<br>
                 			<?php
                 				$def_status = isset($_GET['status']) ? $_GET['status'] : '';
                 			?>
 							<select id='status_select' class='form-control' >
 								<option value=''>Select Status</option>
 								<option value='pending' <?=$def_status == 'pending' ? 'selected' : ''?> >Pending</option>
 								<option value='approved' <?=$def_status == 'approved' ? 'selected' : ''?> >Approved</option>
 								<option value='rejected' <?=$def_status == 'rejected' ? 'selected' : ''?> >Rejected</option>
 							</select>
 						</div>

 					<?php
 						if(count($categories) > 0){
 					?>
 						<div class="col-md-3 filter-form">
                 			<br>
 							<select id='cat_select' class='form-control' >
 								<option value=''>Select Category</option>
 								<?php
 								  $def_cat = isset($_GET['category']) ? $_GET['category'] : '';
 									foreach ($categories as $category) {
 										$selected = ($def_cat == $category->id) ? 'selected' : '';
 										//echo "<option value='$category->id' $selected > $category->category_name ($$category->value) </option>";
 										echo "<option value='$category->id' $selected > $category->category_name (".(isset($category->value) ? '$' . $category->value : 'No value set').") </option>";
 									}
 								?>
 							</select>
 						</div>

 					<?php
 						}
 					?>

 						<?php
 							if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE){
 						?>
 								<div class="col-md-3 filter-form">
 									<br>
 									<select id='outlet_select' class='form-control' >
 										<option value=''>Select Outlet</option>
 										<?php
 											$def_outlet = isset($_GET['outlet']) ? $_GET['outlet'] : '';
 											foreach ($franchiseOutlets as $outlet) {
 												$selected = ($def_outlet == $outlet->id) ? 'selected' : '';
 												echo "<option value='$outlet->id' $selected > $outlet->name </option>";
 											}
 										?>
 									</select>
 								</div>
 						<?php
 							}
 						?>
 					</div>


                   <div class="card-block table-container ks-datatable" style="margin-top : 16px;">
                     <table id="ks-datatable" class="table">
                         <thead>
                             <tr>
 															<th>
 																<label class="custom-control custom-checkbox">
 																		<input id='select_all' style="padding:11px 30px 11px 7px !important;" type="checkbox" class="custom-control-input select-all-leads">
 																		<span class="custom-control-indicator"></span>
 																		<span class="custom-control-description"></span>
 																</label>
 															</th>
 															<th>Name</th>
 															<!-- <th>Last Name</th> -->
 															<th>Email</th>
 															<th>Date</th>
 															<th>Category</th>
 															<?php
 															 	if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE || Yii::$app->user->identity->role == User::ROLE_OUTLET){
 															?>
 																	<th>Outlet</th>
 															<?php
 																}
 															?>
 															<th>City</th>
 															<th>State</th>
 															<th>Country</th>
 															<th class='transcript-col'>Transcript</th>
 															<th class='status-col'>Status</th>
 															<th>Email</th>
 			    										<th>Action</th>
 														</tr>
                         </thead>
                         <tbody>
                           <?php Pjax::begin(); ?>

                           <?= ListView::widget([
                               'dataProvider' => $dataProvider,
                               'itemOptions' => ['class' => 'item'],
                               'itemView' => function ($model, $key, $index, $widget) {
                                   $showLead = 1;
                                   $td_outlet = '';
                                   if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE || Yii::$app->user->identity->role == User::ROLE_OUTLET){
                                     $td_outlet =  "<td> Not Set </td>";
    															 }
                                   $status_span = "<span style='padding:5px 5px 5px 5px;height:30px!important;width:95px;text-align:center' class='btn btn-warning ks-rounded'>Pending</span>";

                                   $td_status = "<td class='status-col'>" . $status_span . "</td>";

                                   $tr = "<tr id=leadid_$model->chat_id>
                                           <td>
                                             <label class='custom-control custom-checkbox'>
                                                 <input type='checkbox' name='chatCheckbox[]' value='$model->chat_id' ($showLead == 1 ? 'disabled' : '' ) class='custom-control-input lead-checkbox'>
                                                 <span class='custom-control-indicator'></span>
                                                 <span class='custom-control-description'></span>
                                             </label>
                                           </td>
                                           <td>
                															$model->name
                													 </td>
                                           <td>
                															$model->email
                													 </td>

                                           <td> ".date('Y-m-d',$model->ended_timestamp)." </td>

                                           <td> Category 1</td>
                                           $td_outlet
                                           <td>$model->city</td>
              												 		 <td>$model->region </td>
              														 <td> $model->country </td>
                                           <td class='transcript-col'>
                                            <button class='btn btn-primary-outline' style='padding:5px 5px 5px 5px;height:30px!important;width:95px;text-align:center' data-toggle='modal' >Quick View</button>
                                           </td>
                                           $td_status
                                           <td> Action </td>
                                            <td> Pay/Edit </td>
                                            </tr>
                                   ";
                                   return $tr;
                               },
                           ]) ?>

                           <?php Pjax::end(); ?>

                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

 		<div id="loader"><div class="loadingoverlay" style="background-color: rgb(255, 255, 255); display: flex; flex-direction: column; align-items: center; justify-content: center; z-index: 99999; background-image: url(&quot;http://chatapptest.oxnia.com/kosmoassets/img/loaders/svg-loaders/three-dots.svg&quot;); background-position: center center; background-repeat: no-repeat; position: fixed; top: 0px; left: 0px; width: 100%; height: 100%; background-size: auto auto;"><span id="loaderText" style="font-size : 20px; padding-bottom : 50px;">Processing Payment..</span></div></div>


 		<!-- Forward multiple email Modal -->

 		<div id="forwardingModal" class="modal fade" role="dialog">
 			<div class="modal-dialog">
 				<!-- Modal content-->
 				<div class="modal-content">
 				  <div class="modal-header">
 					<h4 class="modal-title">Forwarding Emails</h4>
 					<button type="button" class="close" data-dismiss="modal">&times;</button>
 				  </div>
 				  <div class="modal-body">
 					 <table class="table">
 					 	<thead>
 					 		<th class="select-all-email">
 					 			<label class="custom-control custom-checkbox">
 									<input id='select_all_email' type="checkbox" class="custom-control-input select-all-leads">
 									<span class="custom-control-indicator"></span>
 									<span class="custom-control-description"></span>
 								</label>
 					 		</th>
 					 		<th>Name</th>
 					 		<th>Email</th>
 					 		<th>Team</th>
 					 	</thead>
 					 	<tbody class='forwarding-table-body'>

 					 		<?php
 					 			$forwardingEmailAddress = ForwardingEmailAddress::findAll(['user_id' => Yii::$app->user->identity->id]);
 					 			foreach ($forwardingEmailAddress as $value) {
 					 		?>
 					 				<tr>
 							 			<td>
 							 				<label class="custom-control custom-checkbox">
 												<input data-team="<?=$value->team?>" type="checkbox" value="<?=$value->id?>" class="custom-control-input forward-email-check">
 												<span class="custom-control-indicator"></span>
 												<span class="custom-control-description"></span>
 											</label>
 										</td>
 								 		<td><?=ucfirst($value->name)?></td>
 								 		<td><?=$value->email?></td>
 								 		<td><?= isset($value->forwardingTeam->name) ? $value->forwardingTeam->name : '' ?></td>
 							 		</tr>
 					 		<?php
 					 			}
 					 		?>
 					 		<input type="hidden" name="forwardChatId" id="forwardChatId" value="0">

 					 	</tbody>
 					 </table>

 					 <div class="row">
 					 	<div class="col-md-12">

 					 		<?php
 					 			foreach ($forwardingEmailTeams as $team) {
 					 		?>
 					 				<label class="custom-control custom-checkbox">
 										<input data-id="<?=$team->id?>" type="checkbox" class="custom-control-input select-team">
 										<span class="custom-control-indicator"></span>
 										<span class="custom-control-description"><?=ucwords($team->name)?></span>
 									</label>

 									&nbsp;
 									&nbsp;
 					 		<?php
 					 			}
 					 		?>

 					 	</div>
 					 </div>
 				  </div>
 				  <div class="modal-footer">
 					<button type="button" class="btn btn-primary send-forwarding-email">Send email</button>
 					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 				  </div>
 				</div>
 			  </div>
 		</div>

 		<!-- Quick Send Modal -->

 		<div id="quickSendModal" class="modal fade" role="dialog">
 			<div class="modal-dialog">

 				<!-- Modal content-->
 				<div class="modal-content">
 				  <div class="modal-header">
 					<h4 class="modal-title">Quick Send</h4>
 					<button type="button" class="close" data-dismiss="modal">&times;</button>
 				  </div>
 				  <div class="modal-body">
 				  	<label>Email:</label>
 					 <input type="email" required name="quickEmailInput" id="quickEmail" class="form-control">
 				  </div>
 				  <div class="modal-footer">
 					<button type="button" class="btn btn-primary quick-send-email" data-url="<?=Url::to(['leads/quick-send'])?>" >Send email</button>
 					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
 				  </div>
 				</div>

 			  </div>
 		</div>

 	<!-- Quick Send Modal -->


 <?php

 $this->registerCss("

 	.table td{
 		 padding:5px 5px 5px 5px !important;
 		font-size:12px !important;
 	}

 	.table th:first-child{
 		 padding-left: 15px !important;
 		 padding-right:30px !important;
 	}

 	.table td:first-child{
 		 padding-left: 15px !important;
 		 padding-right:30px !important;
 	}

 	.table .transcript-col{
 		width: 55px !important;
 	}

 	.table .status-col{
 		width: 55px !important;
 	}
 	.sendEmail:hover{
 		color: darkred;
 		font-weight: bold;
 	}

 	span.view-icon:hover{
 		color: #f00;
 		cursor: pointer;

 	}
 	button.btn-primary-outline:hover{
 		border: solid 1px #f00;
 		color: #f00;
 		cursor: pointer;
 	}

 	.btn>.ks-icon{
 		width: 43px !important;
 	}

 	.table-container{
 		overflow: scroll;
 	}

 	.pay-all-selected{
 		margin: 10px 0 0 10px
 	}

 	.filter-form{
 		padding-left: 30px !important;
 	}

 	.pay-all-selected:hover{
 		cursor: pointer;
 	}

 	.email-action:hover{
 		cursor: pointer;
 	}

 	.email-action{
 		//padding: 0 10px 0 10px !important;
 	}

 	ul.email-action-ul li{
 		color: blue;
 	}

 	ul.email-action-ul li:hover{
 		color: #f00;
 		cursor: pointer;
 	}

 	.custom-checkbox{
 		margin: 0px !important;
 	}

 	.select-all-email{
 		width: 10% !important;
 		padding-right: 0px !important;
 		padding-left: 10px !important;
 	}


 ");


 $this->registerJs('

     function queryStringUrlReplacement(url, param, value)
 	{
 	    var re = new RegExp("[\\?&]" + param + "=([^&#]*)"), match = re.exec(url), delimiter, newString;

 	    if (match === null) {
 	        // append new param
 	        var hasQuestionMark = /\?/.test(url);
 	        delimiter = hasQuestionMark ? "&" : "?";
 	        newString = url + delimiter + param + "=" + value;
 	    } else {
 	        delimiter = match[0].charAt(0);
 	        newString = url.replace(re, delimiter + param + "=" + value);
 	    }

 	    return newString;
 	}

 	function validateEmail(email)
 	{
 	   if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
 	    {
 	      return (true)
 	    }
 	      return (false)
 	}

     jQuery("document").ready(function(){


 		$("#loader").hide();
 		jQuery("#dateto,#datefrom").datepicker( {dateFormat: "yy-mm-dd",changeMonth:true,changeYear:true} );

 		jQuery("#c_range").click(function(e){
 			e.preventDefault();
 			$("#range-form").show();
 		})
 		jQuery("body").on("click", ".payBySavedCard" ,function(e){

 			e.preventDefault();

 			$("#loaderText").text("Processing Payment..");
 			$("#loader").show();

 			var url = $(this).attr("href");
 			var id = $(this).attr("chatid");
 			jQuery.ajax({
 				url : url,
 				success : function( html ){
 					jQuery("#leadid_" + id).html(html);
 					$("#loader").hide();
 				}

 			})

 		});

 		jQuery("body").on("click", ".sendEmail" ,function(e){

 			e.preventDefault();

 			$("#loaderText").text("Sending Email..");
 			$("#loader").show();

 			var url = $(this).attr("href");
 			var id = $(this).attr("chatid");
 			jQuery.ajax({
 				url : url,
 				success : function( html ){
 					//jQuery("#leadid_" + id).html(html);
 					jQuery("#mailStatus" + id).css("color" , "green");
 					jQuery("#mailStatus" + id).text("Sent");
 					$("#loader").hide();
 				}

 			})

 		});

 		$("#range-form").on("submit", function(e){
 			from = $("#datefrom").val();
 		  	to = $("#dateto").val();
 			console.log("From: " + from);
 			console.log("To: " + to);
 			url = "'.Url::to(['leads/index']).'&filter=crange&date_from=" + from + "&date_to=" + to + "&group_by=day";
 			window.location = url;
 			return false;
 		});

 		$("#cat_select").select2();

 		$("#cat_select").on("change", function(e){
 			current_url = window.location.href;
 			id = $(this).val();
 			search_url = queryStringUrlReplacement(current_url, "category", id);
 			window.location = search_url;
 		});

 		$("#outlet_select").select2();

 		$("#outlet_select").on("change", function(e){
 			current_url = window.location.href;
 			id = $(this).val();
 			search_url = queryStringUrlReplacement(current_url, "outlet", id);
 			window.location = search_url;
 		});

 		$("#status_select").select2();

 		$("#status_select").on("change", function(e){
 			current_url = window.location.href;
 			id = $(this).val();
 			search_url = queryStringUrlReplacement(current_url, "status", id);
 			window.location = search_url;
 		});

 	});'
 , \yii\web\View::POS_END);


 $this->registerJs("

  //$.pjax.reload({container:'#lead_page'});

 	$('.pay-all-selected').off('click').on('click', function(e){
 		e.preventDefault();
 		var values = new Array();
 		$.each($(\"input[name='chatCheckbox[]']:checked\"), function() {
 		  values.push($(this).val());
 		});
 		console.log(values);
 		leadCount = values.length;

 		if(leadCount == 0){
 		 swal('Please select atleast 1 lead to pay');
 		 return false;
 		}

 		swal({
 	      title: 'Are you sure you?',
 	      text: 'Payment will be done for all ' + leadCount + ' selected leads',
 	      icon: 'warning',
 	      buttons: true,
 	      dangerMode: true,
 				showLoaderOnConfirm: true,
 	    })
 	    .then((payAll) => {
 	      if (payAll) {
 					$.ajax({
 						type: 'POST',
 						url: '".Url::to(['ajax/pay-multiple-leads'])."',
 						data: {chat_ids: values},
 						beforeSend: function(){
 							$('#loaderText').text('Processing Payment');
 							$('#loader').show();
 						},
 						success: function(response){
 							$('#loader').hide();
 							response = JSON.parse(response)
 							if(response.success == true){
 								swal('Payment Successfuly', 'You can now view the leads', 'success');
 								setTimeout(function(){
 									location.reload();
 								}, 600);
 							}
 						},
 						error: function(data){
 							console.log(data);
 						},
 					})
 	      } else {
 	        swal('Payment Cancelled!');
 	      }
 	    });
 	});

 	$('#select_all').change(function() {
 		if($(this).is(':checked')){
 			$(\"input[type=checkbox][name='chatCheckbox[]']\").each(function () {
  			  	if( ! $(this).is(':disabled') ){
  					$(this).prop('checked', true);
  				}
  			});
 		}else{
 			$(\"input[type=checkbox][name='chatCheckbox[]']\").each(function () {
 			  	if( ! $(this).is(':disabled') ){
 					$(this).prop('checked', false);
 				}
 			});
 		}
  	});

  	$('.email-option').off('click').on('click', function(e){
  		$('#forwardChatId').val($(this).attr('data-chat-id'));
  	});

  	$('#select_all_email').off('click').on('click', function(e){
  		check_count = 0;
  		action = 0;
  		if($(this).prop('checked') == true){
  			action = 1;
  		}

  		$('.forward-email-check').each(function(index, checkboxObject) {
 		    if($(this).prop('checked') != true){
 		    	if(action){
 		    		$(this).prop('checked', true);
 		    	}else{
 		    		$(this).prop('checked', false);
 		    	}

 		    }else{
 		    	if(action){
 		    		$(this).prop('checked', true);
 		    	}else{
 		    		$(this).prop('checked', false);
 		    	}
 		    }
 		});
  	});

  	$('.send-forwarding-email').off('click').on('click', function(e){
  		emailArray = [];
  		chat_id = $('#forwardChatId').val();

  		if(chat_id == 0){
  			swal('Some problem occured', 'Please try again later after sometime', 'error');
  			return false;
  		}

  		$('.forward-email-check').each(function(index, checkboxObject) {
 		    if($(this).prop('checked') == true){
 		    	//console.log($(this).val());
 		    	emailArray.push($(this).val())
 		    }
 		});

 		if(emailArray.length == 0){
 			swal('No email selected', 'Please select alteast 1 email address to send', 'error');
 		}else{
 			$.ajax({
 				type: 'POST',
 				url: '".Url::to(['leads/forward-email'])."',
 				data: {emailArray: emailArray, chat_id: chat_id},
 				success: function(response){
 					response = JSON.parse(response);
 					console.log(response);
 					if(response.success == true){
 						swal('Email sent successfully', 'Lead email has been sent', 'success');
 					}
 				},
 				error: function(data){
 					console.log(data)
 				}
 			});
 		}
  	});

  	/*$('.send-to-me').off('click').on('click', function(e){
  		url = $(this).attr('data-url');
  		email = $(this).attr('data-email');
  		swal({
 	      title: 'Are you sure ?',
 	      text: 'Send an email to ' + email,
 	      icon: 'warning',
 	      buttons: true,
 	      dangerMode: true,
 	    })
 	    .then((willDelete) => {
 	      if (willDelete) {
 	        window.location = url;
 	      } else {
 	        swal('Email not sent!');
 	      }
 	    });
  	});*/

 	$('.send-to-me').off('click').on('click', function(e){
     url = $(this).attr('data-url');
 		email = $(this).attr('data-email');
     show_lead = $(this).attr('data-show-lead');
  		chat_id = $(this).attr('data-chat-id');
  		swal({
 	      title: 'Are you sure ?',
 	      text: 'Send an email to ' + email,
 	      icon: 'warning',
 	      buttons: true,
 	      dangerMode: true,
         closeOnConfirm: false,
 	    })
 	    .then((confirm) => {
 	      if (confirm) {
           $.ajax({
     				type: 'POST',
     				url: url,
     				data: {chat_id: chat_id, show_lead: show_lead},
     				success: function(response){
     					response = JSON.parse(response);
     					console.log(response);
     					if(response.success == true){
     						swal('Email sent successfully', 'Recepient:  ' + email, 'success');
     					}
     				},
     				error: function(data){
     					console.log(data)
     				}
     			});
 	      } else {
 	        swal('Email not sent!');
 	      }
 	    });
  	});

  	$('.quick-send-email').off('click').on('click', function(e){
  		email = $('#quickEmail').val();
  		chat_id = $('#forwardChatId').val();

  		if(chat_id == 0){
  			swal('Some problem occured', 'Please try again later after sometime', 'error');
  			return false;
  		}

  		url = $(this).attr('data-url') + '&chat_id=' + chat_id + '&quick_email=' + email;
  		if(email == ''){
  			swal('Email cannot be left blank', 'Please provide email address', 'error');
  			return false;
  		}

  		if(validateEmail(email)){
  			$.ajax({
 				type: 'POST',
 				url: url,
 				data: {quick_email: email, chat_id: chat_id},
 				success: function(response){
 					response = JSON.parse(response);
 					console.log(response);
 					if(response.success == true){
 						swal('Email sent successfully', 'Recepient:  ' + email, 'success');
 					}
 				},
 				error: function(data){
 					console.log(data)
 				}
 			});
  		}else{
  			swal('Email not valid', 'Please provide a valid email address', 'error');
  			return false;
  		}
  	});

  	$('.select-team').off('click').on('click', function(e){
  		team_id = $(this).attr('data-id');
  		check = $(this).prop('checked');
  		$('.forward-email-check').each(function(index, checkboxObject) {
  			if(check){
  				if($(this).attr('data-team') == team_id){
 			    	$(this).prop('checked', true);
 			    }
  			}else{
  				if($(this).attr('data-team') == team_id){
 			    	$(this).prop('checked', false);
 			    }
  			}

 		});
  	});

 	$('.download-csv-btn').off('click').on('click', function(e){
 		e.preventDefault();
 		count = $(this).attr('data-count');
 		if(count == 0){
 			swal('There are no leads to download report');
 			return false;
 		}
 		location = $(this).attr('href');
 	})

 ");
