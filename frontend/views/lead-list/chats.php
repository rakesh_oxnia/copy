<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use common\models\User;
use app\models\Leads;
use ruskid\stripe\StripeCheckout;
use common\models\ForwardingEmailAddress;
use yii\widgets\ActiveForm;
use yii2assets\printthis\PrintThis;
use common\models\UserCategory;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\LeadListSearch $searchModel
 */

$this->title = 'Chats Archives';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/swiper/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/widgets/tables.min.css">

<div class="ks-page-header">
    <section class="ks-title">
        <h3><?= Html::encode($this->title) ?></h3>
        <div class="col-md-3">
          <?php if (!empty($website_users)) { 
          $form = ActiveForm::begin([
                  'method' => 'post',
          ]); ?>          
          <select id='website_selecter' name="website_filter" class='btn btn-multisite' onchange="this.form.submit()">              
              <option value='all'>All Websites</option>
              <?php foreach ($website_users as $website_user) { 
                $remove = array("http://","https://");
                $urlweb = str_replace($remove,"",$website_user->website_url);
                if ($website_user->user_group == Yii::$app->session->get('website_filter')) {
                  echo "<option value='$website_user->user_group' selected>$urlweb</option>";
                } else {
                  echo "<option value='$website_user->user_group'>$urlweb</option>";
                }                
              } 
              ?>
           </select>
           <?php ActiveForm::end(); ?>
          <?php } ?>
         </div>      
    </section>
</div>

<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">
                              <div class="row">
                                <div class="col-md-2">
                                  <div class="btn-group date-filter-section">
                                    <button type="button" class="btn btn-danger dropdown-toggle filter-disp-btn" data-toggle="dropdown">
                                      <?php
                                      if(isset($_GET["LeadListSearch"])){
                                        if(isset($_GET["LeadListSearch"]['filter'])){
                                          if($_GET["LeadListSearch"]['filter'] == 'c_range')
                                            echo 'Custom Range';
                                          elseif($_GET["LeadListSearch"]['filter'] == 'last7')
                                            echo 'Last 7days';
                                          elseif($_GET["LeadListSearch"]['filter'] == 'last30')
                                            echo 'Last 30days';
                                          elseif($_GET["LeadListSearch"]['filter']=='')
                                            echo "Last 7days";
                                          else
                                            echo $_GET["LeadListSearch"]['filter'];
                                        }else{
                                          echo "Last 7days";
                                        }
                                      }
                                      else{
                                        echo "Last 7days";
                                      }
                                      ?>
                    										<span class="caret"></span>
                    									</button>
                                      <?php
                                      if(isset($_GET["LeadListSearch"])){
                                        if(isset($_GET["LeadListSearch"]['filter'])){
                                          if($_GET["LeadListSearch"]['filter'] == 'c_range')
                                            echo "<input type='hidden' id='hidden_filter' value='c_range' /><input type='hidden' id='hidden_from' value='".$_GET['LeadListSearch']['date_from']."' /><input type='hidden' id='hidden_to' value='".$_GET['LeadListSearch']['date_to']."' />";
                                          elseif($_GET["LeadListSearch"]['filter'] == 'last7')
                                            echo "<input type='hidden' id='hidden_filter' value='last7' />";
                                          elseif($_GET["LeadListSearch"]['filter'] == 'last30')
                                            echo "<input type='hidden' id='hidden_filter' value='last30' />";
                                          elseif($_GET["LeadListSearch"]['filter']=='')
                                            echo "<input type='hidden' id='hidden_filter' value='last7' />";
                                          else
                                          echo "<input type='hidden' id='hidden_filter' value='".$_GET['LeadListSearch']['filter']."' />";
                                        }else{
                                          echo "<input type='hidden' id='hidden_filter' value='last7' />";
                                        }
                                        if(isset($_GET["LeadListSearch"]['category_id'])){
                                          echo "<input type='hidden' id='hidden_category' value='".$_GET['LeadListSearch']['category_id']."' />";
                                        }
                                        if(isset($_GET["LeadListSearch"]['outlet_id'])){
                                          echo "<input type='hidden' id='hidden_outlet' value='".$_GET['LeadListSearch']['outlet_id']."' />";
                                        }
                                      }
                                      else{
                                        echo "<input type='hidden' id='hidden_filter' value='last7' />";
                                      }
                                      ?>
                    									<ul class="dropdown-menu filter-option-dropdown pull-left" role="menu">
                    										<li><a href="#" class='filter-option' data-text='Today' data-opt='today'>Today</a></li>
                    										<li><a href="#" class='filter-option' data-text='Yesterday' data-opt='yesterday'>Yesterday</a></li>
                    										<li><a href="#" class='filter-option' data-text='Last 7 days' data-opt='last7'>Last 7 days</a></li>
                    										<li><a href="#" class='filter-option' data-text='Last 30 days' data-opt='last30'>Last 30 days</a></li>
                    										<li><a href="#" class='filter-option' data-text='Custom Range' data-opt='c_range'>Custom Range</a></li>
                    									</ul>
                                  <!--Pjax Form--->
                            <?php $form = ActiveForm::begin([
                            'action' => ['chats'],
                            'id' => 'filter_form',
                            'method' => 'get',
                            ]);?>

                            <?= $form->field($searchModel, 'ended_timestamp')->hiddenInput(['class' => 'ended-timestamp'])->label(false) ?>
                            <?= $form->field($searchModel, 'filter')->hiddenInput(['class' => 'filter-input'])->label(false) ?>
                            <?= $form->field($searchModel, 'date_from')->hiddenInput(['class' => 'date-from-input'])->label(false) ?>
                            <?= $form->field($searchModel, 'date_to')->hiddenInput(['class' => 'date-to-input'])->label(false) ?>
                            <?= $form->field($searchModel, 'category_id')->hiddenInput(['class' => 'category-id'])->label(false) ?>
                            <?= $form->field($searchModel, 'outlet_id')->hiddenInput(['class' => 'outlet-id'])->label(false) ?>
                            <?php ActiveForm::end(); ?>
                                </div>
                                  <!--Pjax Form--->
                                </div>
                                <div class="col-md-6">
                                  <!---From Date--->
                                  <form id="range-form" style="display:none;margin-left : 10px;" class="top-formsearch">
                                  <div class="row">
                                  <div class="col-sm-4">
                                  <input type="text" placeholder="Date From" class="form-control" id="datefrom" name="date_from">
                                  </div>
                                  <div class="col-sm-4">
                                  <input type="text" placeholder="Date To" class="form-control" id="dateto" name="date_to">
                                  </div>
                                  <input type="hidden" name="filter" value="crange">
                                  <input type="hidden" name="group_by" value="day">
                                  <div class="col-sm-4">
                                  <input type="submit" class="btn btn-info crange-submit" value="Filter">
                                  </div>
                                  </div>
                                  </form>
                                  <!---From Date--->
                                </div>
                                <div class="col-md-4">
                                  <div>
                                    <?php
echo "<button data-provider='".json_encode(Yii::$app->request->getQueryParams())."' data-count='".$dataProvider->getTotalCount()."' class='btn btn-primary-outline ks-light pull-right print-btn'><i class='fa fa-print'></i> Print</button>";
                                  ?>
                									</div>
                                  <?php

//echo '<pre>';print_r($dataProvider->getTotalCount());exit;
if($dataProvider->getTotalCount()>0){
$csvdata = json_encode(Yii::$app->request->getQueryParams());
echo "<a data-count='".$dataProvider->getTotalCount()."' href='".Url::to(['lead-list/export-chats','dataprovider' => $csvdata])."' class='btn btn-primary-outline ks-light pull-right download-csv-btn'>Download CSV</a>";
}
                                  ?>
                                </div>
                              </div>
                              <!--
                               <div class='row'>
                                <div class="col-md-3">
                                  <select id="bulk_select" class='form-control'>
                                  <option value="">Action</option>
                                  <?php
                                  //if($showPayAsYouGo){
                                    echo  '<option value="bulk_pay">Bulk Pay</option>';
                                  //}
                                  ?>
                                  <option value="bulk_status">Change Status</option>
                                  </select>
                                </div>
                                <div class="col-md-2">
                                  <select style="display:none" id="bulk_status" class='form-control'>
                                  <option value="">Select Status</option>
                                  <option value="pending">Pending</option>
                                  <option value="approved">Complete</option>
                                  <option value="rejected">In progress</option>
                                  </select>
                                </div>
                                <div class="col-md-2">
                                  <button style="display:none" class="btn btn-default change-all-selected">Change Status of selected chats</button>
                                </div>
                              </div>
                              <br />
                              <div class="col-md-12">
                                <?php if(Yii::$app->session->hasFlash('error')):?>
                                  <div class="alert alert-danger">
                                    <b>Failed!</b> <?php echo Yii::$app->session->getFlash('error'); ?>
                                  </div>
                                <?php endif; ?>
                              </div> -->
                                                          
                              <br />
                              <?php Pjax::begin(['id' => 'pjax_widget']); ?>

                              <?= GridView::widget([
                                  'dataProvider' => $dataProvider,
                                  'filterModel' => $searchModel,
                                  'rowOptions'=> function($model){
                                    if(isset($model->leads)){
                                      if($model->leads->is_lead==1 && $model->leads->new_lead){
                                       return ['class' => 'table-warning'];
                                    }
                                  }
                                  },
                                  'columns' => [
                                      ['class' => 'yii\grid\SerialColumn'],
                                      [
                                        'attribute' => 'email',
                                        'value' => function($model){
                                          $showlead = Leads::showleads($model->chat_id);
                                          if($showlead==1){
                                            if(isset($model->leads->visitor_email)){
                                              return $model->leads->visitor_email;
                                            }
                                          }else{
                                            if(isset($model->leads->visitor_email)){
                                              return Leads::obfuscate_name($model->leads->visitor_email);
                                            }
                                          }
                                        },
                                      ],
                                      [
                                        'attribute' => 'category_name',
                                        'value' => function($model){
                                          //echo '<pre>';print_r($model->leads->category);
                                          if(isset($model->leads)){
                                         if(isset($model->leads->category)){
                                           return $model->leads->category->category_name;
                                         }else{
                                           return 'Not Set';
                                         }
                                         }
                                        },
                                      ],
                                      [
                                        'class' => 'yii\grid\ActionColumn',
                                        'header' =>'Visitor Details',
                                        'template'=>'{view}',             
                                        'headerOptions' => ['class' => 'icon-header'],                   
                                        'contentOptions' => ['class' => 'icon-column'],
                                        'buttons' => [
                                          'view' => function ($url, $model) {
                                              return Html::a('<span class="la la-eye td-icon"></span>',
                                                '', ['class' => 'visitor_details', 'data-toggle' => 'modal', 'data-target' => '#ks-izi-modal-visitor-details', 'data-id' => $model->chat_id]);                                            
                                          },
                                        ],
                                      ],
                                      [
                                        'class' => 'yii\grid\ActionColumn',
                                        'header' =>'Transcript',
                                        'template'=>'{view}',
                                        'headerOptions' => ['class' => 'icon-header'],            
                                        'contentOptions' => ['class' => 'icon-column'],
                                        'buttons' => [
                                          'view' => function ($url, $model) {
                                            return Html::a('<span class="la la-eye td-icon"></span>',
                                                '', ['class' => 'view-transcript', 'data-toggle' => 'modal', 'data-target' => '#ks-izi-modal-large-chat', 'data-id' => $model->chat_id]);
                                          },
                                        ],
                                      ],
                                      [
                                        'label' => 'Status',
                                        'attribute' => 'status',
                                        'filter' => Leads::$statuses,
                                        'format' => 'html',
                                        'filterInputOptions' => ['class' => 'select-status form-control', 'id' => null, 'prompt' => 'ANY'],
                                        'headerOptions' => ['class' => 'status-column'],
                                        'contentOptions' => ['class' => 'icon-column'],
                                        'value' => function($data){
                                          if(isset($data->leads)){
                                            if($data->leads->status=='pending')
                                            {
                                              return '<span style="padding:5px;height:25px!important;width:70px;text-align:center" class="btn btn-warning ks-rounded">Pending</span>';
                                            }else if($data->leads->status=='approved')
                                            {
                                              return '<span style="padding:5px 5px 5px 5px;height:30px!important;width:95px;text-align:center" class="btn btn-success ks-rounded">Complete</span>';
                                            }else{
                                              return '<span style="padding:5px 5px 5px 5px;height:30px!important;width:95px;text-align:center" class="btn btn-danger ks-rounded">In progress</span>';
                                            }
                                          }
                                        },
                                      ],
                                      [
                                          'label' => 'Date',
                                          'contentOptions' => ['class' => 'two-line-td'],
                                          'value' => function($data){
                                            return  date("d/m/Y h:i:s A", $data->ended_timestamp);
                                          },
                                      ],
                                      [
                                        'attribute' => 'Email Sent',
                                        'format' => 'html',
                                        'headerOptions' => ['class' => 'icon-header'],                   
                                        'contentOptions' => ['class' => 'icon-column'],
                                        'value' => function($model){
                                           $email = Leads::getMail($model->chat_id);
                                           if($email=='1'){
                                             return '<span id="mailStatus' . $model->chat_id . '" class="la la-check-circle td-icon"></span>';
                                           }else{
                                             return '<span id="mailStatus' . $model->chat_id . '" class="la la-times-circle td-icon-red"></span>';
                                           }
                                        },
                                      ],                                     

                                      //['class' => 'yii\grid\ActionColumn'],
                                      [
                                        'class' => 'yii\grid\ActionColumn',
                                        'header'=>'Action',
                                        'template'=>'{action} {update}',
                                        'buttons' => [
                                            'delete' => function ($url, $model) {
                                                  return Html::a('<span class="ks-icon la la-trash"></span>',
                                                      $url, [
                                                        'data-confirm' => 'Are you sure you want to delete this Email Address ?',
                                                        'data-method' => 'post'
                                                  ]);

                                            },
                                            'action' => function ($url, $model) {
                                             $showlead = Leads::showleads($model->chat_id);
                                             if($showlead==1){
                                                 
                                               if(Yii::$app->user->can('manageForwardingEmails')){
                                                 $forwardlisting = '<li class="forwarding-list"><a data-toggle="modal" data-target="#forwardingModal" class="email-option" data-chat-id="'.$model->chat_id.'">Forwarding list</a></li>';
                                               }else{
                                                 $forwardlisting = '';
                                               }
                                               return  '<div class="dropdown dropdown-mail"><button class="btn btn-primary-outline email-action" data-toggle="dropdown"><span class="ks-icon la la-envelope"></span></button>
                                                 <ul class="dropdown-menu email-action-ul">
                                                 '.$forwardlisting.'<li class="quick-send"><a data-toggle="modal" data-target="#quickSendModal" class="email-option" data-chat-id="'.$model->chat_id.'">Quick Send</a></li>
                                                 <li class="send-to-me" data-show-lead="'.$showlead.'" data-email="'.Yii::$app->user->identity->email.'" data-chat-id="'.$model->chat_id.'" data-url="'.Url::to(['leads/send-to-me']).'"><a class="email-option">Send to me</a></li>
                                               </ul></div> ';
                                             }else{
                                               return Html::a('<button class="btn btn-primary-outline"><span class="ks-icon la la-envelope"></span></button> ',
                                                   '', ['class' => 'view-transcript', 'data-toggle' => 'modal', 'data-target' => '#ks-izi-modal-large-chat', 'data-id' => $model->chat_id]);
                                             }
                                            },                                            
                                            'update' => function ($url, $model) {
                                                $showlead = Leads::showleads($model->chat_id);
                                                if($showlead==1){
                                                  $url = Url::to(['leads/view-chat', 'id' => $model->chat_id]);
                                                  return Html::a('<button class="btn btn-primary-outline" ><span class="ks-icon la la-pencil"></span></button>',
                                                      $url);
                                                }else{
                                                  if(Yii::$app->user->identity->stripeToken == ''){
                                                    // return StripeCheckout::widget([
                                                    //  'action' => Url::to(['/invoice/payperlead', 'id' => $model->chat_id]),
                                                    //  'name' => 'Pay Per Lead',
                                                    //  'description' => " Chats id : " . $model->chat_id,
                                                    //  'image' => Yii::$app->request->baseUrl . '/cube/img/logo.png',
                                                    // ]);
                                                    return '<button class="btn btn-default pay-selected-lead" data-chat-id="'.$model->chat_id.'">Pay</button>';
                                                  }else{
                                                     return '<button class="btn btn-default pay-selected-lead" data-chat-id="'.$model->chat_id.'">Pay</button>';
                                                  }
                                                }
                                            },
                                        ],
                                      ],

                                  ],
                              ]); ?>

                              <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--print-->
<div id="print-test" style="display:none">
  <center>
    <img class="cm-logo" width="300px" height="100px" src="<?php echo Yii::$app->request->baseUrl;?>/images/cm-logo-new.png">
  </center>
  <br />
  <table style="border:1px solid #999">
    <thead>
      <tr>
      <th style="border:1px solid #999">Name</th>
      <th style="border:1px solid #999">Email</th>
      <th style="border:1px solid #999">Phone</th>
    <th style="border:1px solid #999">City</th>
      <th style="border:1px solid #999">State</th>
      <th style="border:1px solid #999">Country</th>
      <th style="border:1px solid #999">Date</th>
      <th style="border:1px solid #999">Status</th>
      </tr>
    </thead>
    <tbody  id="print-test-body">
    </tbody>
  </table>
</div>

<!--print-->
<!---All Model --->
<div id="ks-izi-modal-large-buy-lead" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title">Buy</h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        Please Buy This Lead To View The Content.
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="ks-izi-modal-visitor-details" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <!--<div class="modal-header">
      <h4 class="modal-title visitor-title"></h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>-->
      <div class="modal-body visitor-message">
      </div>
       <div class="modal-body custom-fields" style="padding-top:0;">
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    </div>
</div>


<div id="ks-izi-modal-large-chat" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title chat-title"></h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body chat-message">
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    </div>
</div>

<div id="forwardingModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title">Forwarding Emails</h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
       <table class="table">
        <thead>
          <th class="select-all-email">
            <label class="custom-control custom-checkbox">
              <input id='select_all_email' type="checkbox" class="custom-control-input select-all-leads">
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description"></span>
            </label>
          </th>
          <th>Name</th>
          <th>Email</th>
          <th>Team</th>
        </thead>
        <tbody class='forwarding-table-body'>
        </tbody>
       </table>
       <div class="row">
        <div class="col-md-12 forwarding-team-body">
        </div>
       </div>
      </div>
      <div class="modal-footer">
      <input type="hidden" name="forwardChatId" id="forwardChatId" value="0">
      <button type="button" class="btn btn-primary send-forwarding-email">Send email</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    </div>
</div>

<!-- Quick Send Modal -->

<div id="quickSendModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      <h4 class="modal-title">Quick Send</h4>
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <label>Email:</label>
       <input type="email" required name="quickEmailInput" id="quickEmail" class="form-control">
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary quick-send-email" data-url="<?=Url::to(['leads/quick-send-service-chat'])?>" >Send email</button>
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    </div>
</div>

<!---All Model --->
  <?php

    $this->registerCss("


      .pay-selected-lead {
        padding: 10px !important;
        color: #25628f !important;
        border: solid 1px !important;
      }
      .status-column {
        width: 125px;
      }
       .select-status {
          padding: 10px !important;
       }
       .dropdown-mail {
        float:left;
       }
      .dropdown-mail .dropdown-menu {
        min-width: 100px;
        padding:5px !important;
      }
      .two-line-td {
        white-space: normal !important;        
        width:90px;
        max-width: 90px;
      }
      .icon-header {
        white-space: normal !important;
        width: 80px;
        max-width: 105px;
      }     
      .icon-column {
        white-space: normal !important;
        text-align: center;
        max-width: 90px;
      }
      .btn-primary-outline1 {
        padding: 5px !important;
        height: 25px !important;
        width: 70px !important;        
      }      
      .btn.btn-sm {
        padding: 5px !important;
      }
      .td-icon {
        font-size: 24px !important;
        color: green;
      }
      .td-icon-red {
        font-size: 24px !important;
        color: red;
      }      
      .table td{
        padding: 10px !important;
        font-size: 12px !important;
      }
      .print-btn{
        margin-left: 10px;
        color: #fff !important;
        background-color: #1d4c6f !important;
        border-color: #1d4c6f !important;
      }
      .
      .ks-icon{
        font-size: 20px !important;
        padding-left: 7px;
        margin-left: 0px !important;
      }

      .action-column{
        width: 140px;
      }

      .ks-icon:hover{
        color: #f00;
      }

      .table td {
          vertical-align: middle;
          border-top: none;
          border-right: none;
          border-bottom: 1px solid rgba(57,80,155,.1);
      }

      .table td:first-child {
          padding-left: 15px !important;
          padding-right: 30px !important;
      }

      .table {
          border: none;
      }

      .sendEmail:hover{
    		color: darkred;
    		font-weight: bold;
    	}

    	span.view-icon:hover{
    		color: #f00;
    		cursor: pointer;

    	}
      .btn-primary-outline {
      color: #25628f;
      border: solid 1px #25628f;
      background: #fff;
      margin-left:3px;
      }
    	button.btn-primary-outline:hover{
    		border: solid 1px #f00;
    		color: #f00;
    		cursor: pointer;
    	}

    	.btn>.ks-icon{
    		width: 43px !important;
        margin-left: 0px !important;
    	}

    	.table-container{
    		overflow: scroll;
    	}

    	.pay-all-selected{
    		//margin: 10px 0 0 10px
    	}

    	.filter-form{
    		//padding-left: 30px !important;
    	}

    	.pay-all-selected:hover{
    		cursor: pointer;
    	}

    	.email-action:hover{
    		cursor: pointer;
    	}

    	.email-action{
    		//padding: 0 10px 0 10px !important;
    	}

    	ul.email-action-ul li{
    		color: blue;
    	}

    	ul.email-action-ul li:hover{
    		color: #f00;
    		cursor: pointer;
    	}

    	.custom-checkbox{
    		margin: 0px !important;
    	}

    	.select-all-email{
    		width: 10% !important;
    		padding-right: 0px !important;
    		padding-left: 10px !important;
    	}

      ul.dropdown-menu li a{
        font-weight: bold;
      }

      ul.dropdown-menu li a:hover{
        color: #f00;
      }

      .date-filter-section{
        margin-bottom: 14px !important;
      }

      .btn:hover{
        cursor: pointer !important;
      }
      @media print {
    .noprint {display:none !important;}
    a:link:after, a:visited:after {
      display: none;
      content: '';
    }
}

    ");

    $this->registerJs('

      function queryStringUrlReplacement(url, param, value)
    	{
    	    var re = new RegExp("[\\?&]" + param + "=([^&#]*)"), match = re.exec(url), delimiter, newString;

    	    if (match === null) {
    	        // append new param
    	        var hasQuestionMark = /\?/.test(url);
    	        delimiter = hasQuestionMark ? "&" : "?";
    	        newString = url + delimiter + param + "=" + value;
    	    } else {
    	        delimiter = match[0].charAt(0);
    	        newString = url.replace(re, delimiter + param + "=" + value);
    	    }

    	    return newString;
    	}

    	function validateEmail(email)
    	{
    	   if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
    	    {
    	      return (true)
    	    }
    	      return (false)
    	}

      jQuery("document").ready(function(){

    		$("#loader").hide();
    		jQuery("#dateto,#datefrom").datepicker( {dateFormat: "yy-mm-dd",changeMonth:true,changeYear:true} );

    		jQuery("body").on("click", ".payBySavedCard" ,function(e){

    			e.preventDefault();

    			$("#loaderText").text("Processing Payment..");
    			$("#loader").show();

    			var url = $(this).attr("href");
    			var id = $(this).attr("chatid");
    			jQuery.ajax({
    				url : url,
    				success : function( html ){
    					jQuery("#leadid_" + id).html(html);
    					$("#loader").hide();
    				}

    			})

    		});

    		jQuery("body").on("click", ".sendEmail" ,function(e){

    			e.preventDefault();

    			$("#loaderText").text("Sending Email..");
    			$("#loader").show();

    			var url = $(this).attr("href");
    			var id = $(this).attr("chatid");
    			jQuery.ajax({
    				url : url,
    				success : function( html ){
    					//jQuery("#leadid_" + id).html(html);
    					jQuery("#mailStatus" + id).css("color" , "green");
    					jQuery("#mailStatus" + id).text("Sent");
    					$("#loader").hide();
    				}

    			})

    		});

    		// $("#range-form").on("submit", function(e){
    		// 	from = $("#datefrom").val();
    		//   	to = $("#dateto").val();
    		// 	console.log("From: " + from);
    		// 	console.log("To: " + to);
    		// 	url = "'.Url::to(['leads/index']).'&filter=crange&date_from=" + from + "&date_to=" + to + "&group_by=day";
    		// 	window.location = url;
    		// 	return false;
    		// });

    		// $("#cat_select").on("change", function(e){
    		// 	current_url = window.location.href;
    		// 	id = $(this).val();
    		// 	search_url = queryStringUrlReplacement(current_url, "category", id);
    		// 	window.location = search_url;
    		// });

        $("#cat_select").select2();
        $("#outlet_select").select2();

        $("#cat_select").on("change", function(e){
    			id = $(this).val();
          filter = $("#hidden_filter").val();
          if(filter=="c_range"){
            datefrom = $("#hidden_from").val();
            dateto = $("#hidden_to").val();
            $(".date-from-input").val(datefrom);
            $(".date-to-input").val(dateto);
          }
          outlet_id = $("#hidden_outlet").val();
          $(".outlet-id").val(outlet_id);
          $(".category-id").val(id);
          $(".filter-input").val(filter);
          $("#filter_form").submit();
    		});

        $("#outlet_select").on("change", function(e){
          id = $(this).val();
          filter = $("#hidden_filter").val();
          if(filter=="c_range"){
            datefrom = $("#hidden_from").val();
            dateto = $("#hidden_to").val();
            $(".date-from-input").val(datefrom);
            $(".date-to-input").val(dateto);
          }
          category_id = $("#hidden_category").val();
          $(".category-id").val(category_id);
          $(".outlet-id").val(id);
          $(".filter-input").val(filter);
          $("#filter_form").submit();
       });

       $("#bulk_select").on("change",function(e){
              val = $(this).val();
              if(val!=""){
                if(val=="bulk_pay"){
                    $(".pay-all-selected").show();
                    $("#bulk_status").hide();
                    $(".change-all-selected").hide();
                    $("#bulk_status").val("");
                }
                if(val=="bulk_status"){
                   $(".pay-all-selected").hide();
                   $("#bulk_status").show();
                }
              }else{
                $(".pay-all-selected").hide();
                $("#bulk_status").hide();
                $(".change-all-selected").hide();
              }
       });
       $("#bulk_status").on("change",function(e){
               val = $(this).val();
               if(val!=""){
                   $(".change-all-selected").show();
               }else{
                   $(".change-all-selected").hide();
               }
       });


    		// $("#outlet_select").on("change", function(e){
    		// 	current_url = window.location.href;
    		// 	id = $(this).val();
    		// 	search_url = queryStringUrlReplacement(current_url, "outlet", id);
    		// 	window.location = search_url;
    		// });

    		// $("#status_select").select2();
    		// $("#status_select").on("change", function(e){
    		// 	current_url = window.location.href;
    		// 	id = $(this).val();
    		// 	search_url = queryStringUrlReplacement(current_url, "status", id);
    		// 	window.location = search_url;
    		// });

    	});'
    , \yii\web\View::POS_END);

    $this->registerJs("

    	function bindEvent()
    	{
    		// bind send to me here
    		$('.send-to-me').off('click').on('click', function(e){
    	    url = $(this).attr('data-url');
    			email = $(this).attr('data-email');
    	    show_lead = $(this).attr('data-show-lead');
    	 		chat_id = $(this).attr('data-chat-id');
    	 		swal({
    		      title: 'Are you sure ?',
    		      text: 'Send an email to ' + email,
    		      icon: 'warning',
    		      buttons: true,
    		      dangerMode: true,
    	        closeOnConfirm: false,
    		    })
    		    .then((confirm) => {
    		      if (confirm) {
    	          $.ajax({
    	    				type: 'POST',
    	    				url: url,
    	    				data: {chat_id: chat_id, show_lead: show_lead},
    	    				success: function(response){
    	    					response = JSON.parse(response);
    	    					console.log(response);
    	    					if(response.success == true){
    	    						swal('Email sent successfully', 'Recepient:  ' + email, 'success');
    	    					}
    	    				},
    	    				error: function(data){
    	    					console.log(data)
    	    				}
    	    			});
    		      } else {
    		        swal('Email not sent!');
    		      }
    		    });
    	 	});
    		// bind quick send here
    	 	$('.quick-send-email').off('click').on('click', function(e){
    	 		email = $('#quickEmail').val();
    	 		chat_id = $('#forwardChatId').val();

    	 		if(chat_id == 0){
    	 			swal('Some problem occured', 'Please try again later after sometime', 'error');
    	 			return false;
    	 		}

    	 		url = $(this).attr('data-url') + '?chat_id=' + chat_id + '&quick_email=' + email;
            console.log(url);
    	 		if(email == ''){
    	 			swal('Email cannot be left blank', 'Please provide email address', 'error');
    	 			return false;
    	 		}

    	 		if(validateEmail(email)){
    	 			$.ajax({
    					type: 'POST',
    					url: url,
    					data: {quick_email: email, chat_id: chat_id},
    					success: function(response){
    						response = JSON.parse(response);
    						console.log(response);
    						if(response.success == true){
    							swal('Email sent successfully', 'Recepient:  ' + email, 'success');
    						}
    					},
    					error: function(data){
    						console.log(data)
    					}
    				});
    	 		}else{
    	 			swal('Email not valid', 'Please provide a valid email address', 'error');
    	 			return false;
    	 		}
    	 	});
    		// bind email option //
    		// $('.email-option').off('click').on('click', function(e){
    	 	// 	$('#forwardChatId').val($(this).attr('data-chat-id'));
    	 	// });

    		// bind send forwarding email option //
    		$('.send-forwarding-email').off('click').on('click', function(e){
    	 		emailArray = [];
    	 		chat_id = $('#forwardChatId').val();

    	 		if(chat_id == 0){
    	 			swal('Some problem occured', 'Please try again later after sometime', 'error');
    	 			return false;
    	 		}

    	 		$('.-check').each(function(index, checkboxObject) {
    			    if($(this).prop('checked') == true){
    			    	//console.log($(this).val());
    			    	emailArray.push($(this).val())
    			    }
    			});

    			if(emailArray.length == 0){
    				swal('No email selected', 'Please select alteast 1 email address to send', 'error');
    			}else{
    				$.ajax({
    					type: 'POST',
    					url: '".Url::to(['leads/forward-email-service-chat'])."',
    					data: {emailArray: emailArray, chat_id: chat_id},
    					success: function(response){
    						response = JSON.parse(response);
    						console.log(response);
    						if(response.success == true){
    							swal('Email sent successfully', 'Lead email has been sent', 'success');
    						}
    					},
    					error: function(data){
    						console.log(data)
    					}
    				});
    			}
    	 	});



        function PrintElem(elem)
        {
         var contents = document.getElementById(elem).innerHTML;
         var frame1 = document.createElement('iframe');
         frame1.name = 'frame1';
         frame1.style.position = 'absolute';
            frame1.style.top = '-1000000px';
            document.body.appendChild(frame1);
            var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
            frameDoc.document.open();
            frameDoc.document.write('<html><head><title>Chat Metrics</title>');
            frameDoc.document.write('</head><body>');
            frameDoc.document.write(contents);
            frameDoc.document.write('</body></html>');
            frameDoc.document.close();
            // window.frames['frame1'].focus();
            // window.frames['frame1'].print();
            // document.body.removeChild(frame1);
            setTimeout(function () {
              window.frames['frame1'].focus();
              window.frames['frame1'].print();
              document.body.removeChild(frame1);
            }, 500);
            return false;
        }

        $('.print-btn').off('click').on('click', function(e){
          count = $(this).attr('data-count');
      		dataprovider = $(this).attr('data-provider');
      		if(count == 0){
      			swal('There are no Service Chats to print report');
      			return false;
      		}
          var url_string = window.location.href;
          var url = new URL(url_string);
          var page = url.searchParams.get('page');
          if(page==null){
            page = 1;
          }
          $('#print-test-body').empty();
          $.ajax({
            type: 'POST',
            url: '".Url::to(['lead-list/print-chats'])."',
            data:{dataprovider:dataprovider,page:page},
            success: function(response){
              res = JSON.parse(response);
              if(res.success==true){
                //console.log(res.data);
                data_array = res.data;
                //console.log(data_array.length);
                arr = '';
                $.each(data_array,function(i,val){
                  arr  += \"<tr><td style='border-right:1px solid #999' cellspacing='0' cellpadding='0'>\"+data_array[i].name+\"</td><td style='border-right:1px solid #999' cellspacing='0' cellpadding='0'>\"+data_array[i].email+\"</td><td style='border-right:1px solid #999' cellspacing='0' cellpadding='0'>\"+data_array[i].phone+\"</td><td style='border-right:1px solid #999' cellspacing='0' cellpadding='0'>\"+data_array[i].city+\"</td><td style='border-right:1px solid #999' cellspacing='0' cellpadding='0'>\"+data_array[i].state+\"</td><td style='border-right:1px solid #999' cellspacing='0' cellpadding='0'>\"+data_array[i].country+\"</td><td style='border-right:1px solid #999' cellspacing='0' cellpadding='0'>\"+ data_array[i].date +\"</td><td style='border-right:1px solid #999' cellspacing='0' cellpadding='0'>\"+data_array[i].action_status;+\"</td></tr>\";
                });
                $('#print-test-body').html(arr);
                elem = 'print-test';
                var task  = PrintElem(elem);
                $('#print-test-body').empty();
              }else{
                $('#print-test-body').empty();
              }
            },
            error: function(data){
              console.log(data);
            }
          });
          });

        $('.download-csv-btn').off('click').on('click', function(e){
      		e.preventDefault();
      		count = $(this).attr('data-count');
      		dataprovider = $(this).attr('data-provider');
      		if(count == 0){
      			swal('There are no Service-Chats to download report');
      			return false;
      		}
          location = $(this).attr('href');
      	});

       //--- Visitor Details---//
        $('.visitor_details').off('click').on('click', function(e){
          e.preventDefault();
          chat_id = $(this).attr('data-id');
          $('.visitor-message').empty();
          $('.custom-fields').empty();
          $('.visitor-message').append('<h4>Loading Visitor Details . . . . </h4>');
          $.ajax({
            type: 'post',
            url: '".Url::to(['lead-list/load-visitor-details'])."',
            data: {chatid: chat_id},
            success: function(response){
              response = JSON.parse(response);
              if(response.success)
              { 
                rows = '';
                data = response.data;
                $.each(data, function( index, value ) {
                   rows += '<tr><th>' + index + '</th><td>' + value + '</td></tr>';
                });
                 $('.visitor-message').empty();
                 // $('.visitor-title').empty();
                //  $('.visitor-title').text('Visitor Details');
                 $('.visitor-message').append('<h4>Visitor Details</h4><table id=\'visitor-msg-body\' class=\'table dataTable no-footer\'>' + rows + '</table>');                 
              }
             
              if(response.success == true)
              {
               field_arrays = response.customdata;
               field_str = '';
               for(i = field_arrays.length - 1; i >= 0 ; i--){
                  label =  field_arrays[i].label;
                  field_value = field_arrays[i].field_value;
                 field_str = \"<tr><th>\" +label+ \"</th><td>\"+field_value+\"</td></tr>\" + field_str;
               }
                 $('.custom-fields').empty();
                 $('.custom-fields').append('<h4>Custom Fields</h4><table style=\'width:70%;\' class=\' table dataTable no-footer \'> ' + field_str + ' </table>');
              }else if(response.success == 'Expired'){
                 $('.custom-fields').empty();
                 $('.custom-fields').text('Please Buy This Lead To View The Content.');
              }else{
                $('.custom-fields').empty();
                $('.custom-fields').text('There are no custom fields avaliable for this chat');
              }

            },
            error: function(data){
              console.log(data+'123');
            }
          })
        });
        //---Visitor Details---//

        //---Forwarding Email---//
        $('.email-option').off('click').on('click', function(e){
          e.preventDefault();
          $('.forwarding-table-body').empty();
          $('.forwarding-team-body').empty();
          $('.forwarding-table-body').html('<tr><td colspan=2><h4>Loading Forwarding List . . . . </h4></td></tr>');
          $('#forwardChatId').val($(this).attr('data-chat-id'));
          console.log($(this).attr('data-chat-id'));
          $.ajax({
            type: 'GET',
            url: '".Url::to(['lead-list/forward-email-list'])."',
            success: function(response){
            response = JSON.parse(response);
                console.log(response);
              if(response.success == true)
              {
              $('.forwarding-table-body').empty();
              $('.forwarding-table-body').html(response.data);
              $('.forwarding-team-body').html(response.team);
            }else{
              $('.forwarding-table-body').empty();
              $('.forwarding-team-body').empty();
              $('.forwarding-table-body').html('<tr><td colspan=2>'+response.data+'</td></tr>');
            }
            },
            error: function(data){
              console.log(data);
            }
          });
        });
        //---Forwarding Email---//

        // ----- VIEW TRANSCRIPT IN MODAL CODE HERE ----- //

        $('.view-transcript').off('click').on('click', function(e){
          e.preventDefault();
          chatid = $(this).attr('data-id');
          $('.chat-message').empty();
          $('.chat-message').append('<h4>Loading transcript . . . . </h4>');
          $.ajax({
            type: 'post',
            url: '".Url::to(['lead-list/load-chat-message'])."',
            data: {chatid: chatid},
            success: function(response){

              response = JSON.parse(response);
              console.log(response.data);

              if(response.success == true)
              {
                if(response.data != 'Expired'){
                message_array = response.data;
                tr_str = '';
                for(i = message_array.length - 1; i >= 0 ; i--)
                {
                  author_name = message_array[i].author_name;
                  text = message_array[i].text;
                  date = message_array[i].date;
                  user_type = message_array[i].user_type;
                  console.log('author_name: ' + author_name);
                  if(user_type == 'visitor'){
                    tr_str = \" <tr><td>  <img src='http://oxnia.com/externalassets/customer-icon.png' alt='User Avatar' class='img-circle'>   <small class=' text-muted'>   <i class='fa fa-clock-o fa-fw'></i> \" + date + \" </small>   <strong class='pull-left primary-font'>\" + author_name + \" (Visitor)</strong> </td>   <td> <p> \" + text + \" </p>   </td>  </tr> \" + tr_str;
                  }else{
                    tr_str = \"<tr><td><img src='http://oxnia.com/externalassets/agent-icon.png' alt='User Avatar' class='img-circle'>  <strong class='primary-font'> \"  + author_name + \"  (Agent)</strong>  <small class='pull-left text-muted'> <i class='fa fa-clock-o fa-fw'></i> \" + date + \"  </small> </td><td>    <p>  \" + text +\"  </p>  </td> </tr>\" + tr_str;
                  }
                }
                $('.chat-message').empty();
                $('.chat-title').empty();
                $('.chat-title').text('Chat Transcript');
                $('.chat-message').append('<table class=\' table dataTable no-footer \'> ' + tr_str + ' </table>');
                //alert('Table added');
                //console.log(response.data);
              }else{
                $('.chat-message').empty();
                $('.chat-title').empty();
                $('.chat-title').text('Buy');
                $('.chat-message').text('Please Buy This Lead To View The Content.');
              }
              }
            },
            error: function(data){
              console.log(data);
            }
          })
        });

        // ----- END OF VIEW TRANSCRIPT IN MODAL CODE HERE ----- //
    	}

      // --------------  end of function bindEvent()--------------------- //


      bindEvent(); // call to function to bind event on page load


      $(document).on('pjax:success', function() {
          bindEvent(); // call to function to bind event on ajax refresh
      });

      $('.change-all-selected').off('click').on('click', function(e){
        e.preventDefault();
        var bulk_status = $('#bulk_status').val();
        var values = new Array();
        $.each($(\"input[name='selection[]']:checked\"), function() {
    		  values.push($(this).val());
    		});
        leadCount = values.length;
        if(leadCount == 0){
    		 swal('Please select atleast 1 lead to Change Status');
    		 return false;
    		}

        swal({
    	      title: 'Are you sure you?',
    	      text: 'Status will be Change  as '+ bulk_status +' for ' + leadCount + ' selected Service Chat',
    	      icon: 'warning',
    	      buttons: true,
    	      dangerMode: true,
    				showLoaderOnConfirm: true,
    	    })
          .then((changeAll) => {
            if (changeAll) {
              $.ajax({
                type: 'POST',
                url: '".Url::to(['leads/change-multiple-leads-status'])."',
                data: {chat_ids: values,bulk_status:bulk_status},
                beforeSend: function(){
                  $('#loaderText').text('Processing Payment');
                  $('#loader').show();
                },
                success: function(response){
                  $('#loader').hide();
                  response = JSON.parse(response)
                  if(response.success == true){
                    swal('Status Update Successfuly');
                    setTimeout(function(){
                      location.reload();
                    }, 600);
                  }
                },
                error: function(data){
                  console.log(data);
                },
              })
            }else {
    	        swal('Update Status Cancelled!');
    	      }
          });

      });

      $('.pay-selected-lead').off('click').on('click', function(e){
        e.preventDefault();
        var chat_id = $(this).attr('data-chat-id');
        swal({
            title: 'Are you sure you?',
            text: 'Payment will be done for selected leads',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
          }).then((changeAll) => {
            if (changeAll) {
              $.ajax({
                type: 'POST',
                url: '".Url::to(['ajax/pay-per-lead-by-saved-card'])."',
                data: {chat_id: chat_id},
                beforeSend: function(){
                  $('.pay-selected-lead').prop('disabled', true);
                },
                success: function(response){
                  $('#loader').hide();
                  response = JSON.parse(response)
                  if(response.success == true){
                    swal(response.data);
                    setTimeout(function(){
                      location.reload();
                    }, 600);
                  }else if(response.success == 'setting'){
                    swal(response.data).then((locationreload) => {
                     if (locationreload) {
                           window.location = '".Url::to(['settings/index','data'=>'pay_as_you_go','#'=>'card-detail'])."';

                     }
                   });
                  }else{
                    swal(response.data);
                    $('.pay-selected-lead').prop('disabled', false);
                  }
                },
                error: function(data){
                  console.log(data);
                },
              })
            }else {
              swal('Payment Cancelled');
            }
          });

      });

    	$('.pay-all-selected').off('click').on('click', function(e){
    		e.preventDefault();
    		var values = new Array();
    		$.each($(\"input[name='selection[]']:checked\"), function() {
    		  values.push($(this).val());
    		});
    		//console.log(values);
    		leadCount = values.length;

    		if(leadCount == 0){
    		 swal('Please select atleast 1 lead to pay');
    		 return false;
    		}

    		swal({
    	      title: 'Are you sure you?',
    	      text: 'Payment will be done for ' + leadCount + ' selected Service Chat',
    	      icon: 'warning',
    	      buttons: true,
    	      dangerMode: true,
    				showLoaderOnConfirm: true,
    	    })
    	    .then((payAll) => {
    	      if (payAll) {
    					$.ajax({
    						type: 'POST',
    						url: '".Url::to(['ajax/pay-multiple-leads'])."',
    						data: {chat_ids: values},
    						beforeSend: function(){
    							$('#loaderText').text('Processing Payment');
    							$('#loader').show();
    						},
    						success: function(response){
    							$('#loader').hide();
    							response = JSON.parse(response)
    							if(response.success == true){
    								swal('Payment Successfuly', 'You can now view the Service Chat', 'success');
    								setTimeout(function(){
    									location.reload();
    								}, 600);
    							}
    						},
    						error: function(data){
    							console.log(data);
    						},
    					})
    	      } else {
    	        swal('Payment Cancelled!');
    	      }
    	    });
    	});

    	$('#select_all').change(function() {
    		if($(this).is(':checked')){
    			$(\"input[type=checkbox][name='chatCheckbox[]']\").each(function () {
     			  	if( ! $(this).is(':disabled') ){
     					$(this).prop('checked', true);
     				}
     			});
    		}else{
    			$(\"input[type=checkbox][name='chatCheckbox[]']\").each(function () {
    			  	if( ! $(this).is(':disabled') ){
    					$(this).prop('checked', false);
    				}
    			});
    		}
     	});

     	$('#select_all_email').off('click').on('click', function(e){
     		check_count = 0;
     		action = 0;
     		if($(this).prop('checked') == true){
     			action = 1;
     		}

     		$('.forward-email-check').each(function(index, checkboxObject) {
    		    if($(this).prop('checked') != true){
    		    	if(action){
    		    		$(this).prop('checked', true);
    		    	}else{
    		    		$(this).prop('checked', false);
    		    	}

    		    }else{
    		    	if(action){
    		    		$(this).prop('checked', true);
    		    	}else{
    		    		$(this).prop('checked', false);
    		    	}
    		    }
    		});
     	});

     	$('.select-team').off('click').on('click', function(e){
     		team_id = $(this).attr('data-id');
     		check = $(this).prop('checked');
     		$('.forward-email-check').each(function(index, checkboxObject) {
     			if(check){
     				if($(this).attr('data-team') == team_id){
    			    	$(this).prop('checked', true);
    			    }
     			}else{
     				if($(this).attr('data-team') == team_id){
    			    	$(this).prop('checked', false);
    			    }
     			}

    		});
     	});


      $('.filter-option').off('click').on('click', function(e){
          filter = $(this).attr('data-opt');
          temp_text = $(this).attr('data-text');
          $('.filter-disp-btn').text(temp_text);
          if(filter == 'c_range'){
            $('#range-form').show();
            $('.filter-option-dropdown').trigger('click');
            return false;
          }
          $('.filter-input').val(filter);
          $('#filter_form').submit();
      });

      // $('.crange-submit').off('click').on('click', function(e){
      //   e.preventDefault();
      //
      //   filter = 'c_range';
      //   date_from = $('#datefrom').val();
      //   date_to = $('#dateto').val();
      //
      //   if(date_from == '')
      //   {
      //     swal('Invalid input' ,'Date from cannot be left blank', 'error');
      //     return false;
      //   }
      //
      //   if(date_to == '')
      //   {
      //     swal('Invalid input' ,'Date to cannot be left blank', 'error');
      //     return false;
      //   }
      //
      //   $('.filter-input').val(filter);
      //   $('.date-from-input').val(date_from);
      //   $('.date-to-input').val(date_to);
      //   $('#filter_form').submit();
      // });

      $('#range-form').on('submit', function(e){
        e.preventDefault();

        filter = 'c_range';
        date_from = $('#datefrom').val();
        date_to = $('#dateto').val();

        if(date_from == '')
        {
          swal('Invalid input' ,'Date from cannot be left blank', 'error');
          return false;
        }

        if(date_to == '')
        {
          swal('Invalid input' ,'Date to cannot be left blank', 'error');
          return false;
        }

        $('.filter-input').val(filter);
        $('.date-from-input').val(date_from);
        $('.date-to-input').val(date_to);
        $('#filter_form').submit();
        return false;
      });

      $('#filter_form_pjax_widget').on('pjax:end', function() {
  			$.pjax.reload({container:'#pjax_widget'});  //Reload GridView
  		});
      $('#filter_form_pjax_widget1').on('pjax:end', function() {
  			$.pjax.reload({container:'#pjax_widget'});  //Reload GridView
  		});
      $('#filter_form_pjax_widget2').on('pjax:end', function() {
  			$.pjax.reload({container:'#pjax_widget'});  //Reload GridView
  		});
    ");
