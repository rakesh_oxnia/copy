<?php
/* Leads Detail View page */
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\Chat $model
 */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Chats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chat-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'chat_id',
            'leads_id',
            'name',
            'email:email',
            'city',
            'region',
            'country',
            'agent_email:email',
            'agent_name',
            'rate',
            'duration',
            'chat_start_url:ntext',
            'user_group',
            'tags',
            'is_lead',
            'started_timestamp:datetime',
            'ended_timestamp:datetime',
        ],
    ]) ?>

</div>
