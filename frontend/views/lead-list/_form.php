<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\Chat $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="chat-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'duration')->textInput() ?>

    <?= $form->field($model, 'user_group')->textInput() ?>

    <?= $form->field($model, 'is_lead')->textInput() ?>

    <?= $form->field($model, 'started_timestamp')->textInput() ?>

    <?= $form->field($model, 'ended_timestamp')->textInput() ?>

    <?= $form->field($model, 'leads_id')->textInput() ?>

    <?= $form->field($model, 'chat_start_url')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'chat_id')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'region')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'country')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'agent_email')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'agent_name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'rate')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'tags')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
