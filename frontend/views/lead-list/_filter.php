<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\Chat $model
 * @var yii\widgets\ActiveForm $form
 */
?>

          <div class="col-md-4">
            &nbsp;
          </div>
        <div class="col-md-3">
          <?php if (!empty($website_users)) { 
          $form = ActiveForm::begin([
                  'method' => 'post',
          ]); ?>        
          <select id='website_selecter' name="website_filter" class='btn btn-multisite' onchange="this.form.submit()">              
              <option value='all'>All Websites</option>
              <?php foreach ($website_users as $website_user) { 
                $remove = array("http://","https://");
                $urlweb = str_replace($remove,"",$website_user->website_url);
                if ($website_user->user_group == Yii::$app->session->get('website_filter')) {
                  echo "<option value='$website_user->user_group' selected>$urlweb</option>";
                } else {
                  echo "<option value='$website_user->user_group'>$urlweb</option>";
                }                
              } 
              ?>
           </select>
           <?php ActiveForm::end(); ?>
          <?php } ?>
         </div> 
          <div class="col-md-4">
            <?php if (!empty($outlets)) { 
            $form = ActiveForm::begin([
                    'method' => 'post',
            ]); ?>   
            <select id='outlet_selecter' name="outlet_filter" class='btn btn btn-multisite'   onchange="this.form.submit()">
              <option value='all'>All Outlets</option>
              <?php foreach ($outlets as $outlet) {
                $remove = array("http://","https://");
                $urlweb = str_replace($remove,"",$outlet[website_url]);
                if ($outlet['id'] == Yii::$app->session->get('outlet_filter')) {
                  echo "<option value='$outlet[id]' selected>$outlet[name] ($urlweb)</option>";
                } else {
                  echo "<option value='$outlet[id]'>$outlet[name] ($urlweb)</option>";
                }
              } 
              ?>
           </select>
           <?php ActiveForm::end(); ?>
           <?php } ?>
           </div> 
