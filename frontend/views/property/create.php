<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Property */

$this->title = 'Property Details';
$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="layout-header"></div>
<div class="layout-content">

<div class="col-md-4 layout-left" style="min-height:657px;">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="col-md-8 layout-right" style="">
    <?= $this->render('_form', [
        'model' => $model,
        'model2' => $model2,
        'propertyall' => $propertyall,
    ]) ?>
</div>
</div>
