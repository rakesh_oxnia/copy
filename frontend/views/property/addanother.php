<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Property */

$this->title = 'Select Property';
$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_formanother', [
        'model' => $model,
        'model2' => $model2,
    ]) ?>

</div>
