<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Property */
/* @var $form yii\widgets\ActiveForm */
?>
<?php //echo '<pre>';print_r();die;?>
<div class="property-form">

    <?php $cats = ArrayHelper::map($model2,'house_no','house_no');?>
    <?php $cats2 = ArrayHelper::map($model2,'postcode','postcode');?>
<?php //echo '<pre>';print_r($cats);die;'action' => ['room/create'] ?>
	
	<?php $form = ActiveForm::begin(); ?>
	
	<?= $form->field($model, 'house_no',['options'=>['class'=>'col-sm-12']])->dropDownList($cats, ['prompt' => '--- Select Property ---'])->label('Select Property') ?>
	
	<?= $form->field($model, 'postcode',['options'=>['class'=>'col-sm-12']])->dropDownList($cats2, ['prompt' => '--- Select Property ---'])->label('Select Postcode') ?>
	
	<div class="col-sm-12">
        <?= Html::submitButton($model->isNewRecord ? 'Select' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
	
	<?php ActiveForm::end(); ?>

</div>