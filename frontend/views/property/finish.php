<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Property */

$this->title = 'Thankyou';
$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['finish']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="layout-header"></div>
<div class="property-finish layout-content">
	<div class="col-md-4 layout-left" style="min-height:414px;">
		<h1>Congratulations</h1>
		<p>Here's Your Qr code For</p>
		<p><?php echo $model->house_no.' '.$model->street_name.' '. $model->city.' ,'.$model->country.' ,'. $model->postcode;?></p>
	</div>
	<?php $path = Yii::getAlias('@web');?>
	<div class="col-md-8 layout-right" style="">
		<img src="<?php echo $path.'/allusers/'.Yii::$app->user->identity->username.'/qrcode/code.png';?>" width="200"/><br>
		<a href="<?php echo $path.'/allusers/'.Yii::$app->user->identity->username.'/qrcode/code.png';?>" target='_Blank'>Start Download</a>
		<br>
		<br>
		<a href="<?php echo Url::toRoute('property/create');?>"><button class="btn btn-success">Add Another Property</button></a>
		<a href="<?php echo Url::toRoute('property/addanother');?>"><button class="btn btn-success">Add Another Room</button></a>
	</div>
</div>
