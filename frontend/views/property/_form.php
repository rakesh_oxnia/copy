<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Property */
/* @var $form yii\widgets\ActiveForm */
?>
<?php //echo '<pre>';print_r();die;?>
<div class="property-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<?php //= $form->errorSummary(array($model,$model2)); ?>

    <?= $form->field($model, 'property_ref',['options'=>['class'=>'col-sm-6']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'house_no',['options'=>['class'=>'col-sm-6']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'street_name',['options'=>['class'=>'col-sm-12']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city',['options'=>['class'=>'col-sm-12']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country',['options'=>['class'=>'col-sm-12']])->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'postcode',['options'=>['class'=>'col-sm-6']])->textInput(['maxlength' => true]) ?>
	
	<div class="clear" style="clear:both;"></div>

    <?php //= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'no_of_room',['options'=>['class'=>'col-sm-6']])->textInput(['value'=>'1','readonly' => true]) ?>
	
    <?= $form->field($model2, 'room_name',['options'=>['class'=>'col-sm-12']])->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'created')->textInput() ?>

    <?php //= $form->field($model, 'modified')->textInput() ?>

    <div class="col-sm-12">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>