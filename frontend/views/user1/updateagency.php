<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User2 */

$this->title = 'Update Agency: ' . ' ' . $model->agency_name;
$this->params['breadcrumbs'][] = ['label' => 'User2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
	<div class="col-lg-12">
		<div id="content-header" class="clearfix">
			<div class="pull-left">
				<ol class="breadcrumb">
					<li><a href="<?= Yii::$app->homeUrl;?>">Home</a></li>
					<li><a href="<?= Yii::$app->homeUrl;?>user/indexagency">Agency Listing</a></li>
					<li class="active"><span><?= Html::encode($this->title) ?></span></li>
				</ol>
				<h1><?= Html::encode($this->title) ?></h1>
			</div>
		</div>
	</div>
</div>
<div class="user2-update">

    

    <?= $this->render('_formagency', [
        'model' => $model,
    ]) ?>

</div>
