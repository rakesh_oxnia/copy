<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User2 */

$this->title = $model->agency_name;
$this->params['breadcrumbs'][] = ['label' => 'Agency Listing', 'url' => ['indexagency']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-lg-12">
		<div id="content-header" class="clearfix">
			<div class="pull-left">
				<ol class="breadcrumb">
					<li><a href="<?= Yii::$app->homeUrl;?>">Home</a></li>
					<li><a href="<?= Yii::$app->homeUrl;?>user/indexagency">Agency Listing</a></li>
					<li class="active"><span><?= Html::encode($this->title) ?></span></li>
				</ol>
				<h1><?= Html::encode($this->title) ?></h1>
			</div>
		</div>
	</div>
</div>
<div class="user2-view user-index">

    <p>
        <?= Html::a('Update', ['updateagency', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
				<?= Html::a('Change Password', ['admin-change-password', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?= Html::a('Manage Service Accounts', ['service-account', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
				<?php /* = Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */ ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'agency_name',
            //'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            //'status',
			[                      // the owner name of the model
				'attribute'=>'status',
				'format'=>'raw',
				'value' => ($model->status==10) ? "Active" : "Inactive",
			],
            //'role',
			/* [                      // the owner name of the model
				'attribute'=>'role',
				'format'=>'raw',
				'value' => ($model->role==50) ? "Admin" : "User",
			], */
            'website_url:url',
            'user_group',
            'created_at:datetime',
            //'updated_at:datetime',
        ],
    ]) ?>

</div>
