<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\User;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Add Custom Field';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ks-page-header">
            <section class="ks-title">
                <h3><?php echo $this->title;?></h3>
            </section>
        </div>
        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-lg-12 ks-panels-column-section">
                              <div class="card">
                                  <div class="card-block">
                                    <?php $form = ActiveForm::begin(); ?>
                                  <table class="table table-striped table-bordered detail-view">
                                  <tbody class="div_options">
                                    <tr>
                                      <th>Select Field Type</th>
                                      <td>
                                        <select name="field_type" id="field_type" class="form-control" required>
               														<option value="">Choose Field Type</option>
                                          <option value="textbox">Text-Field</option>
                                          <option value="dropdown" >Dropdown</option>
                                          <option value="multiple_choice">Multiple Choice</option>
               													</select>
                                      </td>
                                    </tr>
                                    <tr>
                                      <th>Label</th>
                                      <td>
                                        <input type="text" value="<?=$customLeadFields->label?>" id="field_label">
                                      </td>
                                    </tr>
                                    <tr>
                                      <th>Status</th>
                                      <td>
                                        <label class="ks-checkbox-slider">
                                          <input type="checkbox" id="field_status" value="" <?php if($customLeadFields->status==1){ echo "checked";}?>>
                                        <span class="ks-indicator"></span>
                                      </label>
                                      </td>
                                    </tr>
                                    <tr>
                                      <th>Required</th>
                                      <td>
                                        <label class="ks-checkbox-slider">
                                         <input type="checkbox" id="field_required" value=""<?php if($customLeadFields->required==1){ echo "checked";}?>>
                                         <span class="ks-indicator"></span>
                                         <input type="hidden" id="user_id" value="<?= $customLeadFields->user_id ?>" />
                                        </label>
                                      </td>
                                    </tr>
                                    <?php
                                    if($customLeadFields->customFieldsOptions){
                                      foreach($customLeadFields->customFieldsOptions as $key => $customFieldsOption){
                                        if($key == 0){
                                      ?>
                                      <tr class="first_option">
                                        <th>Option</th>
                                        <td>
                                          <input type="text" class="field_option" data-id="<?=$customFieldsOption->id?>" value="<?php echo $customFieldsOption['option']?>" required> <img alt="Add More" title="Add More" class="add_option_button" src="images/add-icon.png" />
                                        </td>
                                      </tr>
                                      <?php
                                       }else{
                                      ?>
                                      <tr class="first_option">
                                        <th>Option</th>
                                        <td>
                                          <input type="text" class="field_option" data-id="<?=$customFieldsOption->id?>" value="<?php echo $customFieldsOption['option']?>" required> <img alt='Remove' title='Remove' src='images/remove-icon.png' class='remove_option'/>
                                        </td>
                                      </tr>
                                      <?php
                                       }
                                     }
                                   }else{
                                      ?>
                                      <tr class="first_option" style="display:none">
                                       <th>Option</th>
                                       <td><input type="text" class="field_option" required> <img alt="Add More" title="Add More" class="add_option_button" src="images/add-icon.png" /></td>
                                     </tr>
                                      <?php
                                    }
                                    ?>
                                  <tr class="button_tr">
                                    <th>
                                      <?= Html::a('Back', ['edit-lead-sender','id'=>$customLeadFields->user_id], ['class' => 'btn btn-primary']) ?>
                                    </th>
                                    <td>
                                      <?= Html::submitButton('Update Field',[
                                        'class'=>'btn btn-primary add_to_form'
                                      ]) ?>
                                    </td>
                                  </tr>
                                  </tbody>
                                  </table>
                                  <?php ActiveForm::end(); ?>
                                  </div>
                                </div>
                          </div>
                    </div>
               </div>
            </div>
        </div>
        <?php
$this->registerCss("");
$this->registerJs("
$('#field_type').select2();
$('#field_type').val('$customLeadFields->type').trigger('change');
var max_fields_common = 15;
var x = 1; //initlal text box count
$('.add_option_button').click(function(e){ //on add input button click
    e.preventDefault();
    if(x < max_fields_common){ //max input box allowed
        x++; //text box increment
var option = \"<tr class='option_tr'><th>Option</th><td><input type='text' class='field_option' required>  <img alt='Remove' title='Remove' src='images/remove-icon.png' class='remove_option'/></td></tr>\";
           $('.button_tr').before(option);
    }
});
$('.div_options').on('click','.remove_option', function(e){
    e.preventDefault();
    $(this).closest(\"tr\").remove();
    x--;
});
$('#field_type').on('change',function(e){
        var field_type = $(this).val();
        if(field_type =='textbox'){
            $('.first_option').hide();
            $('.option_tr').hide();
        }
        if(field_type != 'textbox'){
            $('.first_option').show();
        }
        if(field_type ==''){
            $('.first_option').hide();
            $('.option_tr').show();
        }
});
$('.add_to_form').off('click').on('click', function(e){
  e.preventDefault();
  var field_type = $('#field_type').val();
  var field_label = $('#field_label').val();
  if(field_label.length>50){
    swal('field Length must be less than 50');
    return false
  }
  var required = 0;
  if($('#field_required').prop('checked')){
     required = 1;
  }
  var field_status = 0;
  if($('#field_status').prop('checked')){
     field_status = 1;
  }
  if(field_type==''){
    swal('Please select Field Type');
    return false
  }
  if(field_label==''){
    swal('Please select Field label');
    return false
  }
  if(field_type=='textbox'){
     var field_option = '';
  }else{
     empty = 0;
    var field_option = $('.field_option').map(function(){
      if($(this).val()==''){
         empty = 1
      }
      if($(this).val()!=''){
        return $(this).val();
      }
    }).get();
    var field_option_id = $('.field_option').map(function(){
      if($(this).val()!=''){
        return {id:$(this).data('id'),name:$(this).val()}
      }
    }).get();
    console.log(field_option)
    if(field_option.length===0){
      swal('Option cannot be blank');
      return false
    }
    if(empty==1){
      swal('Option cannot be blank');
      return false
    }
  }
  var user_id = $('#user_id').val();
  var field_id = '$customLeadFields->id';
  //console.log(field_option_id);
  //return false;
  $.ajax({
    type: 'POST',
    url: '".Url::to(['agent/update-fields-to-agent-app'])."',
    data: {
           field_label:field_label,
           required:required,
           field_option:field_option,
           field_option_id:field_option_id,
           user_id:user_id,
           field_type:field_type,
           field_id:field_id,
           field_status:field_status
         },
    beforeSend: function() {
      //$('.add_to_form').prop('disabled', true);
    },
    success: function(response){
      response = JSON.parse(response)
      if(response.success == true){
        swal(response.data).then((locationreload) => {
         if (locationreload) {
               location.reload();
         }
       });
      }
      if(response.success == false){
         swal(response.data);
         //$('.add_to_form').prop('disabled', false);
         return false;
      }
    },
    error: function(data){
      console.log(data);
      swal('Some problem occured!', 'Please try again later after sometime.');
      return false;
    },
  });
});
");
        ?>
