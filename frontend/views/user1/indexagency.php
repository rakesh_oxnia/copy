<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agency Listing';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-lg-12">
		<div id="content-header" class="clearfix">
			<div class="pull-left">
				<ol class="breadcrumb">
					<li><a href="<?= Yii::$app->homeUrl;?>">Home</a></li>
					<li class="active"><span><?= Html::encode($this->title) ?></span></li>
				</ol>
				<h1><?= Html::encode($this->title) ?></h1>
			</div>
		</div>
	</div>
</div>
<div class="user-index col-lg-12">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Agency', ['createagency'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'layout' => '{summary}<div class="table-responsive">{items}</div>{pager}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'agency_name',
            //'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            'user_group',
			[
				'attribute' => 'status',
				'value' => function ($model) {
                    if($model->status == 10){
                        return 'Active';
                    }else{
                        return 'Inactive';
                    }
                },
				'filter' => Html::activeDropDownList($searchModel,'status',['0' => 'Inactive','10' => 'Active'],[
					'class' => 'form-control',
					'prompt' => 'All'
				]),
			],
            //'role',
            /* [
				'attribute' => 'role',
				'value' => function ($model) {
                    if($model->role == 50){
                        return 'Admin';
                    }else{
                        return 'User';
                    }
                },
				'filter' => Html::activeDropDownList($searchModel,'role',[''=>'','50' => 'Admin','10' => 'User'],[
					'class' => 'form-control'
				]),
			], */
            'website_url:url',
            'created_at:datetime',
            // 'updated_at',

            //['class' => 'yii\grid\ActionColumn'],

			[   'class' => 'yii\grid\ActionColumn',
                'header'=>'Action',
                'template'=>'{user}{view}{update}{delete}',
                'buttons' => [
										'user' => function ($url, $model) {https://testapp.chatmetrics.net/frontend/web/index.php?r=user%2Fviewagency&id=351
												return '<a href="'.Url::to(['user/switch-user', 'id' => $model->id]).'" title="Switch to User" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-user"></span></a>';
										},
                    'view' => function ($url, $model) {https://testapp.chatmetrics.net/frontend/web/index.php?r=user%2Fviewagency&id=351
											//return '<a href="/user/viewagency?id='.$model->id.'" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>';
                        return '<a href="'.Url::to(['user/viewagency', 'id' => $model->id]).'" title="View" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span></a>';
                    },
                    'update' => function ($url, $model) {
											//return '<a href="/user/updateagency?id='.$model->id.'" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>';
                        return '<a href="'.Url::to(['user/switch-user', 'id' => $model->id]).'" title="Update" aria-label="Update" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>';
                    },
                    /*'update' => function ($url, $model) {
                        return Html::a('', ['updateagency', 'id' => $model->id], ['class' => 'glyphicon glyphicon-pencil']);
                    },  */
                    'delete' => function ($url, $model) {
											//return '<a href="/user/deleteagency?id='.$model->id.'" data-confirm="Are You sure you want to delete this Agency ?" data-method="post"><i class="fa fa-trash-o"></i></a>';
											return '<a href="/user/deleteagency?id='.$model->id.'" data-confirm="Are You sure you want to delete this Agency ?" data-method="post"><i class="fa fa-trash-o"></i></a>';
											/*Html::a('<i class="fa fa-trash-o"></i>',
                            $url, [
								'data-confirm' => 'Are You sure you want to delete this Agency ?',
								'data-method' => 'post'
                        ]);*/
                    },
                ],
            ],
        ],
    ]); ?>

</div>

<!-- 'update' => function ($url, $model) {
                        return Html::a('', ['updateagency', 'id' => $model->id], ['class' => 'glyphicon glyphicon-pencil']);
                    },   -->
