<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User2 */
/* @var $form yii\widgets\ActiveForm */

$bl = array();
if(isset($_GET['id']) && $_GET['id'] != '')
{
    $bl = array();
    $connection = Yii::$app->getDb();
    $command = $connection->createCommand('SELECT *
    FROM  `user_associate` 
    WHERE  `user_id` ='.$_GET['id']);
    $bl = $command->queryAll();   
}
?>

<div class="user2-form user-index">

    <?php $form = ActiveForm::begin(); ?>
	
	<?php if($model->isNewRecord):?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true,'placeholder'=>'User Name']) ?>
	
	<?php endif; ?>

    <?php //= $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'password_reset_token')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true,'placeholder'=>'Email']) ?>

     <?php 
    if(count($bl) > 0)
    {
        $i = 0;
        foreach ($bl as $value) {
            
            if($i == 0):?>
            <div class="input_fields_wrap2">
                <div style="float: right;">Add More</div>
                <div class="clear"></div>
                <img style="float: right;" alt="Add More" title="Add More" class="add_field_button2" src="<?= Yii::$app->homeUrl;?>frontend/web/images/add-icon.png" />
                <div class="clear"></div>
                <div class="help-block"></div>            
            
            <?php
            endif;
            ?>

            <div>
                <div class="form-group"><label>Other Email</label>
                    <input type="text" class="form-control" name="User2[email2][]" value="<?php echo $value['email'];?>">                             
                </div>          
               
                <?php if($i != 0) { ?>
                    <a href="#" class="remove_field2" style="float:right;"><img alt="Remove" title="Remove" src="<?= Yii::$app->homeUrl;?>frontend/web/images/remove-icon.png" /></a>
                <?php } ?>
            </div>
            <div class="clear"></div><div class="help-block"></div>

            <?php
             
            $i++;
        }
        ?>
        </div>
        <?php 

    }else
    {
        ?>
        <div class="input_fields_wrap2">
            <div style="float: right;">Add More</div>
                <div class="clear"></div>
            <img style="float: right;" alt="Add More" title="Add More" class="add_field_button2" src="<?= Yii::$app->homeUrl;?>frontend/web/images/add-icon.png" />
            <div class="clear"></div>
            <div class="help-block"></div>
        
        </div>
        <?= $form->field($model, 'email2[]')->textInput(['maxlength' => true]) ?>
       
        
    <?php
    }
    ?>
	
	<?php if(!$model->isNewRecord):?>

    <?= $form->field($model, 'status')->dropdownList(['10'=>'Active','0'=>'Inactive'],["prompt"=>"-- Select Status --"]) ?>
	
	<?php endif; ?>

    <?php //= $form->field($model, 'role')->textInput() ?>

    <?= $form->field($model, 'user_group')->textInput(['maxlength' => true,'placeholder'=>'User Group']) ?>
	
    <?= $form->field($model, 'website_url')->textInput(['maxlength' => true,'placeholder'=>'Website Url']) ?>
	

    <?php //= $form->field($model, 'created_at')->textInput() ?>

    <?php //= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		
		<a href="<?= Yii::$app->homeUrl;?>user/index" class="btn btn-primary">Cancel</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
 $this->registerJs('
    $(document).ready(function() {

    var cntoptions = $("#cntoptions").html();
    
    var max_fields_common = 10;
   
    var wrapper2         = $(".input_fields_wrap2"); 
    var add_button2      = $(".add_field_button2"); 
    
    var x = 1; //initlal text box count
    $(add_button2).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields_common){ //max input box allowed
            x++; //text box increment
            $(wrapper2).append(\'<div><div class="form-group"><label>Other Email</label><input type="text" class="form-control" name="User2[email2][]"></div><a href="#" class="remove_field2" style="float:right;"><img alt="Remove" title="Remove" src="'.Yii::$app->homeUrl.'frontend/web/images/remove-icon.png" /></a></div><div class="clear"></div><div class="help-block"></div>\');
        }
    });
    
    $(wrapper2).on("click",".remove_field2", function(e){ 
        e.preventDefault(); $(this).parent(\'div\').remove(); x--;
    })   

    
});');
?>