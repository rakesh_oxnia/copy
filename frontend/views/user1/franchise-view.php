<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User2 */

$this->title = 'Franchise owner: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'User2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ks-page-header">
            <section class="ks-title">
                <h3><?php echo $this->title;?></h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		<?= empty($model->parent_id) ? Html::a('Manage Websites', ['website-index', 'id' => $model->id], ['class' => 'btn btn-primary']) : ''; ?>
    <?= Html::a('Change Password', ['admin-change-password', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Manage Outlets', ['user/outlet-index', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
   <?= Html::a('Manage Service Accounts', ['service-account', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
   <?= Html::a('Allocate Package', ['allocate-plan-view', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
   <?= Html::a('Custom Fields', ['edit-lead-sender', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php /* = Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */ ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'username',
            'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            //'status',
			[                      // the owner name of the model
				'attribute'=>'status',
				'format'=>'raw',
				'value' => ($model->status==10) ? "Active" : "Inactive",
			],
            //'role',
			/* [                      // the owner name of the model
				'attribute'=>'role',
				'format'=>'raw',
				'value' => ($model->role==50) ? "Admin" : "User",
			], */
            'website_url:url',
            'user_group',
            'created_at:datetime',
            //'updated_at:datetime',
        ],
    ]) ?>

   </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
