<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\User;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Custom Fields';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ks-page-header">
            <section class="ks-title">
                <h3><?php echo $this->title;?></h3>
            </section>
        </div>
        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-lg-12 ks-panels-column-section">
                              <div class="card">
                                  <div class="card-block">
                                    <?php $form = ActiveForm::begin(); ?>
                                  <table class="table table-striped table-bordered detail-view">
                                  <tbody class="div_options">
                                    <tr>
                                      <th>Select Field Type</th>
                                      <td>
                                        <select name="field_type" id="field_type" class="form-control" required>
               														<option value="">Choose Field Type</option>
                                          <option value="textbox">Text-Field</option>
                                          <option value="dropdown" >Dropdown</option>
                                          <option value="multiple_choice">Multiple Choice</option>
               													</select>
                                      </td>
                                    </tr>
                                    <tr>
                                      <th>Label</th>
                                      <td>
                                        <input type="text" id="field_label">
                                      </td>
                                    </tr>
                                    <tr>
                                      <th>Status</th>
                                      <td>
                                        <label class="ks-checkbox-slider">
                                          <input type="checkbox" id="field_status" value="">
                                        <span class="ks-indicator"></span>
                                      </label>
                                      </td>
                                    </tr>
                                    <tr>
                                      <th>Required</th>
                                      <td>
                                        <label class="ks-checkbox-slider">
                                        <input type="checkbox" id="field_required" value="">
                                        <span class="ks-indicator"></span>
                                         <input type="hidden" id="user_id" value="<?= $user_id ?>" />
                                        </label>
                                      </td>
                                    </tr>
                                    <tr class="first_option" style="display:none">
                                      <th>Option</th>
                                      <td><input type="text" class="field_option"><img style="float: right;" alt="Add More" title="Add More" class="add_option_button" src="images/add-icon.png" /></td>
                                    </tr>
                                  <tr class="button_tr">
                                    <th>
                                      <?= Html::a('Back', ['view','id'=>$user_id], ['class' => 'btn btn-primary']) ?>
                                    </th>
                                    <td>
                                      <?= Html::submitButton('Add To Form',[
                                        'class'=>'btn btn-primary add_to_form'
                                      ]) ?>
                                    </td>
                                  </tr>
                                  </tbody>
                                  </table>
                                  <?php ActiveForm::end(); ?>
                                  </div>
                                </div>
                          </div>
                    </div>
               </div>
               <div class="container-fluid">
                   <div class="row">
                       <div class="col-lg-12 ks-panels-column-section">
                           <div class="card">
                               <div class="card-block">
                                 <button type="button" class="btn btn-danger btn-sm delete_field"><span class="la la-trash"></span></button>
                                 <div class="card-block ks-datatable">
                                     <table id="ks-datatable" class="table table-bordered table-responsive">
                                         <thead>
                                            <tr>
                                             <th></th>
                                             <th>Id</th>
                                             <th>Field Type</th>
                                              <th>Label</th>
                                             <th>Created Date/Time</th>
                                             <th>Created by</th>
                                             <th>Status</th>
                                             <th>Action</th>
                                           </tr>
                                         </thead>
                                         <tbody>
                                           <?php
                                           foreach($customLeadFields as $customLeadField){
                                             ?>
                                           <tr>
                                             <td><input type="checkbox" id="delete_checkbox" class="delete_checkbox" value="<?= $customLeadField->id;?>"></td>
                                             <td><?php echo $customLeadField->id;?></td>
                                             <td><?php echo $customLeadField->type;?></td>
                                             <td><?php echo $customLeadField->label?></td>
                                             <td><?php echo date('Y-m-d H:i A',  $customLeadField->created_at)?></td>
                                             <td><?php echo $customLeadField->createdBy['email']?></td>
                                             <td><?php if($customLeadField->status==1){
                                               echo 'Active';
                                             }else{
                                               echo 'Inactive';
                                             }?>
                                            </td>
                                             <td><?= Html::a('Edit', ['update-lead-sender','id'=>$customLeadField->id], ['class' => 'btn btn-primary']) ?></td>
                                           </tr>
                                           <?php
                                         }
                                           ?>
                                         </tbody>
                                       </table>
                                     </div>
                                   </div>
                               </div>
                             </div>
                           </div>
                         </div>
            </div>
        </div>
        <?php
$this->registerCss("");
$this->registerJs("
  $('#field_type').select2();
  ///////////////////////////////
  var max_fields_common = 15;
  var x = 1; //initlal text box count
  $('.add_option_button').click(function(e){ //on add input button click
      e.preventDefault();
      if(x < max_fields_common){ //max input box allowed
          x++; //text box increment
var option = \"<tr class='option_tr'><th>Option</th><td><input type='text' class='field_option' >  <img alt='Remove' title='Remove' src='images/remove-icon.png' class='remove_option'/></td></tr>\";
             $('.button_tr').before(option);
      }
  });
  $('.div_options').on('click','.remove_option', function(e){
      e.preventDefault();
      $(this).closest(\"tr\").remove();
      x--;
  });

  $('#field_type').on('change',function(e){
    var field_type = $(this).val();
    //console.log(field_type);
    if(field_type !='textbox'){
        $('.first_option').show();
    }
    if(field_type =='textbox'){
        $('.first_option').hide();
        $('.option_tr').remove();
    }
    if(field_type ==''){
        $('.first_option').hide();
        $('.option_tr').remove();
    }
  });

  $('.add_to_form').off('click').on('click', function(e){
    e.preventDefault();
     var field_type = $('#field_type').val();
     var field_label = $('#field_label').val();
     if(field_label.length>50){
       swal('field Length must be less than 50');
       return false
     }
     var required = 0;
     if($('#field_required').prop('checked')){
        required = 1;
     }
     var field_status = 0;
     if($('#field_status').prop('checked')){
        field_status = 1;
     }
     if(field_type==''){
       swal('Please select Field Type');
       return false
     }
     if(field_label==''){
       swal('Please select Field label');
       return false
     }
     if(field_type=='textbox'){
        var field_option = '';
     }else{
         empty = 0;
         field_option = $('.field_option').map(function(){
           if($(this).val()==''){
              empty = 1
           }
           if($(this).val()!=''){
            return $(this).val();
           }
         }).get();
         if(field_option.length===0){
           swal('Option cannot be blank');
           return false
         }
         if(empty==1){
           swal('Option cannot be blank');
           return false
         }
     }
     var user_id = $('#user_id').val();
     $.ajax({
       type: 'POST',
       url: '".Url::to(['agent/add-fields-to-agent-app'])."',
       data: {
              field_label:field_label,
              required:required,
              field_option:field_option,
              user_id:user_id,
              field_type:field_type,
              field_status:field_status
            },
       beforeSend: function() {
         $('.add_to_form').prop('disabled', true);
       },
       success: function(response){
         response = JSON.parse(response)
         if(response.success == true){
           swal(response.data).then((locationreload) => {
            if (locationreload) {
                  location.reload();
            }
          });
         }
         if(response.success == false){
            swal(response.data);
            $('.add_to_form').prop('disabled', false);
            return false;
         }
       },
       error: function(data){
         console.log(data);
         swal('Some problem occured!', 'Please try again later after sometime.');
         return false;
       },
     });
  });
  $('.delete_field').off('click').on('click',function(e){
      e.preventDefault;
      var delete_checkbox_array = [];
  $('.delete_checkbox').each(function(index, checkboxObject) {
      if($(this).prop('checked') == true){
        delete_checkbox_array.push($(this).val())
      }
  });
      if(delete_checkbox_array.length==0){
        swal('No Fields selected', 'Please select alteast 1 field  to delete');
        return false;
      }
      //var user_id = $('#user_id').val();
      swal({
          title: 'Are you sure you?',
          text: delete_checkbox_array.length +' Fields will be deleted',
          icon: 'warning',
          buttons: true,
          dangerMode: true,
          showLoaderOnConfirm: true,
        }).then((deletefield)=>{
          if(deletefield){
            $.ajax({
              type: 'POST',
              url: '".Url::to(['agent/delete-fields-from-agent-app'])."',
              data: {
                     delete_checkbox_array:delete_checkbox_array,
                   },
              beforeSend: function() {

              },
              success: function(response){
                response = JSON.parse(response)
                if(response.success == true){
                   swal(response.data).then((locationreload) => {
                    if (locationreload) {
                          location.reload();
                    }
                  });

                }
                if(response.success == false){
                   swal(response.data);

                   return false;
                }
              },
              error: function(data){
                console.log(data);
                swal('Some problem occured!', 'Please try again later after sometime.');
                return false;
              },
            });
          }else{
            swal('Delete Cancelled');
          }
        });
  });
");
        ?>
