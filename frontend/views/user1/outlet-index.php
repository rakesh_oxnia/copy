<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ucfirst($franchiseModel->name) . ' Outlet Listing';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ks-page-header">
            <section class="ks-title">
                <h3><?php echo $this->title;?></h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Outlet', ['outlet-create', 'id' => $id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		'layout' => '{summary}<div class="table-responsive">{items}</div>{pager}',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'username',
            'name',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            //'user_group',
            'email:email',
            [
              'attribute' => 'user_group',
              'label' => 'User Group',
              'headerOptions' => ['style' => 'width:40px'],
            ],
			[
				'attribute' => 'status',
				'value' => function ($model) {
                    if($model->status == 10){
                        return 'Active';
                    }else{
                        return 'Inactive';
                    }
                },
				'filter' => Html::activeDropDownList($searchModel,'status',['0' => 'Inactive','10' => 'Active'],[
					'class' => 'form-control',
					'prompt' => 'All'
				]),
			],
            //'role',
            /* [
				'attribute' => 'role',
				'value' => function ($model) {
                    if($model->role == 50){
                        return 'Admin';
                    }else{
                        return 'User';
                    }
                },
				'filter' => Html::activeDropDownList($searchModel,'role',[''=>'','50' => 'Admin','10' => 'User'],[
					'class' => 'form-control'
				]),
			], */
            'website_url:url',
            //'created_at:datetime',
            // 'updated_at',

            //['class' => 'yii\grid\ActionColumn'],

			[   'class' => 'yii\grid\ActionColumn',
                'header'=>'Action',
                'headerOptions' => ['class' => 'action-column'],
                'template'=>'{user}{view}{update}{delete}',
                'buttons' => [

                    'user' => function ($url, $model) {https://testapp.chatmetrics.net/frontend/web/index.php?r=user%2Fviewagency&id=351
                        //return '<a class="switch-user" href="'.Url::to(['user/switch-user', 'id' => $model->id]).'" title="Switch to User" aria-label="View" data-pjax="0"><span class="glyphicon glyphicon-user"></span></a>';
                        $url = Url::to(['user/switch-user', 'id' => $model->id]);
                        if(Yii::$app->user->identity->role == User::ROLE_ADMIN){
                          return Html::a('<span class="ks-icon la la-user"></span>',
                              $url);
                        }else{
                          return '';
                        }

                    },

          					'view' => function ($url, $model) {
                                  $url = Url::to(['user/outlet-view', 'id' => $model->id]);
                                  return Html::a('<span class="ks-icon la la-eye"></span>',
                                      $url);
                              },
          					'update' => function ($url, $model) {
                                  return Html::a('<span class="ks-icon la la-pencil"></span>',
                                      $url);
                              },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="ks-icon la la-trash"></span>',
                            $url, [
                  								'data-confirm' => 'Are You sure you want to delete this Outlet ?',
                  								'data-method' => 'post'
                                ]);
                            },
                ],
            ],
        ],
    ]); ?>
</div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
  $this->registerCss("
      .table td{
        padding: 10px !important;
        font-size: 12px !important;
      }

      .ks-icon{
        font-size: 20px !important;
        padding-left: 7px;
      }

      .action-column{
        width: 140px;
      }

      .ks-icon:hover{
        color: #f00;
      }
  ");
?>
