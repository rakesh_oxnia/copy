<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User2 */
/* @var $form yii\widgets\ActiveForm */


$items = array(''=>'Default', 'theme-white' => 'White/Green', 'theme-blue-gradient' => 'Gradient', 'theme-turquoise' => 'Green Sea', 'theme-amethyst' => 'Amethyst', 'theme-blue' => 'Blue', 'theme-red' => 'Red', 'theme-whbl' => 'White/Blue' )
?>

<div class="user2-form user-index">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?php echo $form->field($model, 'name')->hiddenInput(['value' => ' - '])->label(false); ?>

    <?= $form->field($model, 'agency_name')->textInput(['maxlength' => true,'placeholder'=>'Ageny Name']) ?>
    <?php
    /*$id= yii::$app->user->identity->id;
    echo $form->field($model, 'parent_id')->hiddenInput(['value' => $id])->label(false);*/ ?>

	<?php if($model->isNewRecord):?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true,'placeholder'=>'User Name']) ?>

	<?php endif; ?>




    <?php //= $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'password_reset_token')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true,'placeholder'=>'Email']) ?>

	<?php if(!$model->isNewRecord):?>

    <?= $form->field($model, 'status')->dropdownList(['10'=>'Active','0'=>'Inactive'],["prompt"=>"-- Select Status --"]) ?>

	<?php endif; ?>

    <?php //= $form->field($model, 'role')->textInput() ?>

    <?= $form->field($model, 'user_group')->textInput(['maxlength' => true,'placeholder'=>'User Group']) ?>

    <?= $form->field($model, 'website_url')->textInput(['maxlength' => true,'placeholder'=>'Website Url']) ?>

    <!-- <div>LOGO image dimenstions : 283x60 pixels</div> -->
    <?= $form->field($model, 'agency_logo')->fileInput() ;?>

    <?php
                    if($model->agency_logo != '') {
                        echo '<div class="help-block"></div><div class="form-group"><label></label>
                             <img src="' . \yii::$app->request->BaseUrl.'/logo_images/' .$model->agency_logo .'" width="100px"> &nbsp;&nbsp;&nbsp;&nbsp;';
                         echo Html::a('Delete',['user/deletelogo','id'=>$model->id ,],['class'=>'btn btn-danger']).'</div>';
                    } ?>

    <?php
    echo $form->field($model, 'theme_color')
     ->dropDownList( $items );?>

    <?php //= $form->field($model, 'created_at')->textInput() ?>

    <?php //= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

		<a href="<?= Yii::$app->homeUrl;?>user/index" class="btn btn-primary">Cancel</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<!-- <ul id="skin-colors" class="clearfix">
    <li>
        <a class="skin-changer active" data-skin="" data-toggle="tooltip" title="" style="background-color: #34495e;" data-original-title="Default">
    </a>
    </li>
    <li>
        <a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="" style="background-color: #2ecc71;" data-original-title="White/Green">
    </a>
    </li>
    <li>
        <a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="" data-original-title="Gradient">
    </a>
    </li>
    <li>
        <a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="" style="background-color: #1abc9c;" data-original-title="Green Sea">
    </a>
    </li>
    <li>
        <a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="" style="background-color: #9b59b6;" data-original-title="Amethyst">
    </a>
    </li>
    <li>
        <a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="" style="background-color: #2980b9;" data-original-title="Blue">
    </a>
    </li>
    <li>
        <a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="" style="background-color: #e74c3c;" data-original-title="Red">
    </a>
    </li>
    <li>
        <a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="" style="background-color: #3498db;" data-original-title="White/Blue">
    </a>
    </li>
</ul> -->
