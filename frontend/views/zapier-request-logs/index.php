<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\ZapierRequestLogs;
use yii\widgets\Pjax;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var frontend\models\RequestLogSearch $searchModel
 */

$this->title = 'Zapier Request Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ks-page-header">
    <section class="ks-title">
        <h3><?= Html::encode($this->title) ?></h3>
    </section>
</div>
<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">

                              <div class="row">
           											<div class="col-md-4">
           												<input type="text" placeholder="Filter Date" class="form-control" id="datefrom" name="date_from" autocomplete="off">
           											</div>
                                <div class="col-md-2">
           												<?=Html::a('Go', ['index'], ['class' => 'btn btn-primary search-btn'])?>
           											</div>
                              </div>

                              <br>

                              <?php Pjax::begin(['id' => 'zapier_request_log_pjax']); ?>

                              <?= GridView::widget([
                                  'dataProvider' => $dataProvider,
                                  'filterModel' => $searchModel,
                                  'columns' => [
                                      ['class' => 'yii\grid\SerialColumn'],
                                      [
                                        'attribute' => 'Client',
                                        'value' => function($model){
                                          return ZapierRequestLogs::getUserBusinessName($model->user_group,$model->api_key,$model->username);
                                        },
                                      ],
                                      'api_key',
                                      'username',
                                      'user_group',
                                      [
                                          'header' => 'Requested At',
                                          'attribute' => 'created_at',
                                          'format' => ['datetime'],
                                      ]
                                  ],
                              ]); ?>
                              <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
  $this->registerCss("

    .table td{
      padding: 10px !important;
      font-size: 12px !important;
    }

    .ks-icon{
      font-size: 20px !important;
      padding-left: 7px;
    }

    .action-column{
      width: 140px;
    }

    .ks-icon:hover{
      color: #f00;
    }

  ");

    $this->registerJs("
        $('select').select2();

        $.pjax.reload({container:'#zapier_request_log_pjax'});

        jQuery('#dateto,#datefrom').datepicker( {dateFormat: 'yy-mm-dd',changeMonth:true,changeYear:true} );

        $('.search-btn').off('click').on('click', function(e){
          e.preventDefault();
          date_from = $('#datefrom').val();
          // if(date_from == ''){
          //   swal('Date cannot be left blank', '', 'error');
          //   return false;
          // }

          chat_id = $(\"input[name='ZapierRequestLogSearch[chat_id]']\").val();
          lead_id = $(\"input[name='ZapierRequestLogSearch[lead_id]']\").val();
          created_at = date_from;
          search_url = '&ZapierRequestLogSearch[chat_id]=' + chat_id + '&ZapierRequestLogSearch[lead_id]=' + lead_id + '&ZapierRequestLogSearch[created_at]=' + created_at +'&_pjax=%23zapier_log_pjax';
          url = '".Url::to(['zapier-request-logs/index'])."' + search_url;
          location = url;
        });
    ");

?>
