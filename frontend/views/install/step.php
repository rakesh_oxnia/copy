<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>
  <form class="form-container">
		<div class="ks-text-center ks-avatar">
			<img src="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/img/avatars/Chgat Metrics-Senae-avatar.jpg" width="150" height="150">
		</div>
		<br>
		<h4 class="ks-header">Hello <?= Yii::$app->user->identity->your_name;?></h4>

		<div class="ks-text-center">
			<?php if(isset($_GET['wp-signup'])) { ?>
				<h2>Activated!</h2>

				<p>Hey! I am Senae from Team Chat Metrics and we are excited to have you onboard with us.</p>

				<p>I am here to help you setup your Chat Metrics account and get you more leads from your current website traffic</p>				
			<?php } else { ?>
				<h2>Let's get you setup!!</h2>

				<p>Hey! I am Senae from Team Chat Metrics and we are excited to have you onboard with us.</p>

				<p>I am here to help you setup your Chat Metrics account and get you more leads from your current website traffic</p>
			<?php } ?>
		</div>
		<div class="ks-text-center">
			<a href="<?php echo Url::to(['/install/step2']);?>">
					<input type="button" value="Okay" class="btn btn-primary" style="background-color: #00bae1; border-color :  #00bae1;">
			</a>
		</div>


	</form>
