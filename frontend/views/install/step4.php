<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>

<div class="row">
<?php if($success){?>
						<div class="alert alert-success">
							<?php echo $success;?>
						</div>
					<?php }?>
					<?php if( !empty( $error ) ){?>
							<div class="alert alert-danger">
								<?php
										foreach($error as $err){
											echo $err;
										}
								?>
							</div>
					<?php }?>
	<div class="col-md-6">
		<img src="<?php echo Yii::$app->request->baseUrl;?>/images/cm-browser-mockup-crop-u973.png" class="img-responsive" width="700">
	</div>
	<div class="col-md-6">

			<div class="clearfix grpelem" id="pu884"><!-- column -->

				<div class="clearfix colelem" id="u902"><!-- group -->
				 <!-- m_editable region-id="editable-static-tag-U899-BP_infinity" template="5.html" data-type="html" data-ice-options="disableImageResize,link" -->
				 <div class="clearfix grpelem" id="u899-5" data-muse-uid="U899" data-muse-type="txt_frame"><!-- content -->
				  <p style="font-size : 20px;"><span style="font-weight : bold">Install Chat Metrics</span> on your website and we are ready to get you more leads!</p>
				 </div>
				 <!-- /m_editable -->
				</div>
				<div class="clearfix colelem" id="u918"><!-- group -->
				 <!-- m_editable region-id="editable-static-tag-U921-BP_infinity" template="5.html" data-type="html" data-ice-options="disableImageResize,link" -->
				 <div class="clearfix grpelem" id="u921-6" data-muse-uid="U921" data-muse-type="txt_frame"><!-- content -->
				  <p>Last step towards boosting your website conversions by upto 10 times. Simply install this chat snippet on your website and let's go!<br/>
				  <br/></p>
				 </div>
				 <!-- /m_editable -->
				</div>
				<div class="clearfix colelem" id="pu924"><!-- group -->
				 <!-- m_editable region-id="editable-static-tag-U924" template="5.html" data-type="html" data-ice-options="clickable" data-ice-editable="link" -->
				 <div class="pointer_cursor rounded-corners clearfix grpelem" id="u924" data-muse-uid="U924"><!-- group -->
				 <label>Copy and Install below code on your website, just before the </head> tag</label>
<textarea rows="5" cols="50">
<!-- Start of Chatcode -->
<script type="text/javascript" src="https://chat-application.com/embed/index.php?tracker_id=<?=Yii::$app->user->identity->tracker_id?>"></script>
<!-- End of Chatcode -->
</textarea>
				  <!--<input type="button" value="Install Chat Metrics" class="btn btn-primary">-->
				  <!-- /m_editable -->
				 </div>

				</div>
				<div class="clearfix colelem" style="margin-top : 20px;"><!-- group -->
				 <div class="clearfix grpelem" id="u946"><!-- column -->
				  <!-- m_editable region-id="editable-static-tag-U943-BP_infinity" template="5.html" data-type="html" data-ice-options="disableImageResize,link" -->
				  <div class="clearfix colelem" id="u943-4" data-muse-uid="U943" data-muse-type="txt_frame"><!-- content -->
				   <p>Not sure how to do it? Send instructions to your website manager/developer....</p>
				  </div>
				  <!-- /m_editable -->
				  <div class="rounded-corners clearfix colelem" id="u949"><!-- group -->
				   <!-- m_editable region-id="editable-static-tag-U952-BP_infinity" template="5.html" data-type="html" data-ice-options="disableImageResize,link" -->
				   <div class="clearfix grpelem" id="u952-4" data-muse-uid="U952" data-muse-type="txt_frame"><!-- content -->

					<input type="button" style="background-color: #00bae1; border-color :  #00bae1;" value="Send Instructions" class="btn btn-primary" data-toggle="modal" data-target=".send-instructions-form">
				   </div>
				   <!-- /m_editable -->
				  </div>
				 </div>
				 <div class="clip_frame grpelem" id="u1227"><!-- svg -->
				  <img class="svg" id="u1228" src="images/arrow-01.svg?crc=63869891" alt="" data-mu-svgfallback="images/arrow-01_poster_.png?crc=4090411978" data-image-width="83" data-image-height="50"/>
				 </div>
				</div>
				<div class="clearfix colelem" id="pu958" style="margin-top : 20px;" ><!-- group -->
				 <div class="clearfix grpelem" id="u958"><!-- column -->
				  <!-- m_editable region-id="editable-static-tag-U955-BP_infinity" template="5.html" data-type="html" data-ice-options="disableImageResize,link" -->
				  <div class="clearfix colelem" id="u955-4" data-muse-uid="U955" data-muse-type="txt_frame"><!-- content -->
				   <p>Or, you can hire our support team (developers) to do it for free for you!</p>
				  </div>
				  <!-- /m_editable -->
				  <div class="rounded-corners clearfix colelem" id="u964"><!-- group -->

				   <div class="clearfix grpelem" id="u961-4" data-muse-uid="U961" data-muse-type="txt_frame"><!-- content -->

					<form action="<?=Url::to(['install/step4'])?>&let_chat_metrics_do_it='Let Chat Metrics Do It'">
						<input type="button" name="let_chat_metrics_do_it" value="Let Chat Metrics Do It" class="btn btn-danger" data-toggle="modal" data-target=".let-chat-metrics-do-it">
					</form>
				   </div>
				   <!-- /m_editable -->
				  </div>
				 </div>

				</div>

				<div class="clearfix colelem" style="margin-top : 20px;"><!-- content -->
				 <p>You can also chat with our support team via livechat on this page for help</p>
				</div>
				<!-- /m_editable -->
				<a href="<?php echo Url::to(['/site/launch']);?>">
					<input type="button" value="Start My Dashboard" class="btn btn-success">
				</a>
				<br />
				<br />
			   </div>
	</div>
</div>

<div class="modal fade send-instructions-form" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Send installation instructions to your developer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-container']]); ?>

                    <div class="form-group">
                        <label for="example-email-input" class="col-12 col-form-label">Your developer's email address</label>
                        <div class="col-12">
                            <input name="email" class="form-control" type="email" required id="example-email-input">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="example-url-input" class="col-12 col-form-label">Your Message</label>
                        <div class="col-12">
<textarea id="summernote"  name="message" class="form-control" rows="10" cols="50">
Dear Developer,

This is <?= Yii::$app->user->identity->your_name?> here.

We have just signed up for Chat Metrics for our website <?= Yii::$app->user->identity->website_url?>. Can you please assist us to install the tracking code in order to get Chat Metrics up and running on our website.

Please install the following code before the </header> of all pages or the master page of our website.

<script type="text/javascript" src="https://chat-application.com/embed/index.php?tracker_id=<?=Yii::$app->user->identity->tracker_id?>"></script></textarea>

                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="offset-sm-2 col-sm-12">
                            <button type="submit" name="send_email" class="btn btn-primary">Send Instructions</button>
                        </div>
                    </div>

				   <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade let-chat-metrics-do-it" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Let Chat Metrics Do It</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="la la-close"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <p>
					Your Request has been received by Chat Metrics and our developers will get in touch with you shortly.
                     </p>
                    <div class="form-group row">
                        <div class="col-sm-12">
        				<a href="<?=Url::to(['install/step4'])?>&let_chat_metrics_do_it=true">
        					<input type="button" value="Go to My Dashboard" class="btn btn-success">
        				</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs("
	$(function(){
		alert('script running');
	})
");
