<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>	
	<div class="body-content">
		<div class="main-box-body clearfix">
				<div id="myWizard" class="wizard">
						<div class="wizard-inner">
							<ul class="steps">
							<li data-target="#step1"><span class="badge">1</span>Step 1<span class="chevron"></span></li>
							<li data-target="#step2"><span class="badge">2</span>Step 2<span class="chevron"></span></li>
							<li data-target="#step3"><span class="badge">3</span>Step 3<span class="chevron"></span></li>
							<li data-target="#step4"><span class="badge">4</span>Step 4<span class="chevron"></span></li>
							<li data-target="#step5" class="active"><span class="badge badge-primary">5</span>Step 5<span class="chevron"></span></li>
							<li data-target="#step6"><span class="badge">6</span>Step 6<span class="chevron"></span></li>
							<li data-target="#step7"><span class="badge">7</span>Step 7<span class="chevron"></span></li>
							<li data-target="#step8"><span class="badge">8</span>Step 8<span class="chevron"></span></li>
							</ul>
							<div class="actions">
								<a href="<?php echo Url::to(['/install/step4']);?>">	
									<button type="button" class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Prev</button>
								</a>
								<a href="<?php echo Url::to(['/install/step6']);?>">
									<button type="button" class="btn btn-success btn-mini btn-next" data-last="Finish">Next<i class="icon-arrow-right"></i></button>
								</a>
							</div>
						</div>
						<div class="step-content">
							<div class="step-pane active" id="step1">
								<br/>
								<h1>Last Step to get Drift Up and Running</h1>
								<p>Can you install the code yourself or should you ask a teammate?</p>
								<div class="main-box-body clearfix">
									<div class="profile-box-content clearfix">
										<ul class="list-group">
											<li class="list-group-item">
												<a href="<?php echo Url::to(['/install/step6']);?>">
													<span class="badge">J</span>
													JavaScript
												</a>
											</li>
											<li class="list-group-item">
												<a href="<?php echo Url::to(['/install/step6']);?>">
													<span class="badge badge-danger">W</span>
													Wordpress
												</a>
											</li>
											<li class="list-group-item">
												<a href="<?php echo Url::to(['/install/step6']);?>">
													<span class="badge badge-success">Z</span>
													Zapier
												</a>
											</li>
											 
										</ul>
									</div>
								</div>
							</div>

						</div>
				</div>
		</div>
	</div>
				 