<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>
	 	 <?php $form = ActiveForm::begin(['options' => ['class' => 'form-container']]); ?>

					<?php if($success){?>
							<div class="alert alert-success">
								<?php echo $success;?>
							</div>
					<?php }?>
					<?php if( $error ){?>
							<div class="alert alert-danger">
								<?php echo $error;?>
							</div>
					<?php }?>

					<h2 style="text-align:center" class="ks-header">Your Website</h2>

            <div class="form-group">
								<div class="input-icon icon-left icon-lg icon-color-primary">
										<?= $form->field($model, 'business_name')->textInput([ 'class' => 'form-control', 'placeholder' => 'Business name', 'required' => true]) ?>
								</div>
                <div class="input-icon icon-left icon-lg icon-color-primary">
									<?= $form->field($model, 'website_url')->textInput([ 'class' => 'form-control', 'placeholder' => 'Website address', 'required' => true]) ?>
                </div>
            </div>

					<div class="form-group">
                       <b>Chat Purpose<b>
					   <br><br>
					   <label class="ks-checkbox-slider">
							<input type="checkbox" name="chat_purpose_sales" value="1" <?php if($model->chat_purpose_sales){ echo "checked";}?>>
							<span class="ks-indicator"></span>

						</label>
						Lead generation

					   <label class="ks-checkbox-slider">
							<input type="checkbox" name="chat_purpose_support" value="1" <?php if($model->chat_purpose_support){ echo "checked";}?>>
							<span class="ks-indicator"></span>

						</label>

						Support/Service

						<br>

						<label class="ks-checkbox-slider">
						 <input type="checkbox" name="chat_purpose_e_commerce" value="1" <?php if($model->chat_purpose_e_commerce){ echo "checked";}?>>
						 <span class="ks-indicator"></span>

					 </label>

					 E-commerce

                    </div>

                    <div class="form-group">
                        <button type="submit" name="save" class="btn btn-primary btn-block">Continue</button>
                    </div>
                    <div class="ks-text-center">
                        <span class="text-muted">By creating account you agree to </span> <a href="https://chatmetrics.com/terms/">Terms Of Service</a>
                    </div>


     <?php ActiveForm::end(); ?>
