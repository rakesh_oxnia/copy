<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>	
<style>
.alert alert-danger{
	color : red;
}
</style>
	 <?php $form = ActiveForm::begin(['options' => ['class' => 'form-container']]); ?>
	 
					<?php if($success){?>
						<div class="alert alert-success">
							<?php echo $success;?>
						</div>
					<?php }?>
					<?php if( !empty( $error ) ){?>
							<div class="alert alert-danger" style="color : red;">
								<?php 
										foreach($error as $err){
											echo $err;
										}
								?>
							</div>	
					<?php }?>
									
                    <h2 style="text-align:center" class="ks-header">Your Company</h2>
                  
                    <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
							<select name="industry" class="form-control">
								<option value="">Choose Industry</option>
								<?php if( !empty( $categories ) ){?>
										<?php foreach($categories as $country){?>
												<option <?php if($model->industry == $country['id']){echo "selected";}?> value="<?php echo $country['id'];?>"><?php echo $country['name'];?></option>
										<?php }?>
								<?php }?>
								
							</select>
                        </div>
                    </div>
					
					<div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
							<select name="number_of_employees" class="form-control">
								<option value="">Number of Employees</option>
								<option <?php if($model->number_of_employees == '1'){echo "selected";}?> value="1">1 - 10 Employees</option>
								<option <?php if($model->number_of_employees == '5'){echo "selected";}?> value="5">10 - 100 Employees</option>
								<option <?php if($model->number_of_employees == '10'){echo "selected";}?> value="10">100 plus Employees</option>
							</select>
                        </div>
                    </div>
					
					<div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
							<select name="country" class="form-control">
								<option value="">Country</option>
								<?php if( !empty( $countries ) ){?>
										<?php foreach($countries as $country){?>
												<option <?php if($model->country == $country['id']){echo "selected";}?> value="<?php echo $country['id'];?>"><?php echo $country['country_name'];?></option>
										<?php }?>
								<?php }?>
							</select>
                        </div>
                    </div>
					
					<div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                            <input type="text" value="<?php echo $model->telephone;?>" name="telephone" class="form-control" placeholder="Mobile Phone Number">
                        </div>
                    </div>
					
					
					
					<div class="form-group">
                       Audience
					   <br>
					   <label class="ks-checkbox-slider">
							<input type="checkbox" <?php if($model->b2b){echo "selected";}?> name="b2b" value="1">
							<span class="ks-indicator"></span>
							
						</label>
						B2B
						
					   <label class="ks-checkbox-slider">
							<input type="checkbox" <?php if($model->b2c){echo "selected";}?> name="b2c" value="1">
							<span class="ks-indicator"></span>
							
						</label>
						
						B2C
						
						<label class="ks-checkbox-slider">
							<input type="checkbox" <?php if($model->internal_use){echo "selected";}?> name="internal_use" value="1">
							<span class="ks-indicator"></span>
							
						</label>
						
						Internal Use
                    </div>
					
                    
					
                    <div class="form-group">
                        <button type="submit" name="save" class="btn btn-danger btn-block">Create Account</button>
                    </div>
                    <div class="ks-text-center">
                        <span class="text-muted">By creating account you agree to </span> <a href="https://chatmetrics.com/terms/">Terms Of Service</a>
                    </div>
                    
                    
     <?php ActiveForm::end(); ?>