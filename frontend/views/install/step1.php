<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>	
	 
	 <?php $form = ActiveForm::begin(['options' => ['class' => 'form-container']]); ?>
	 
					<?php if($success){?>
							<div class="alert alert-success">
								<?php echo $success;?>
							</div>
					<?php }?>
					<?php if( $error ){?>
							<div class="alert alert-danger">
								<?php echo $error;?>
							</div>	
					<?php }?>
								
								
                    
					<h2 style="text-align:center" class="ks-header">Let's get Started!</h2>
                  
                    <div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">                            
							<?= $form->field($model, 'email')->textInput([ 'class' => 'form-control', 'placeholder' => 'Email']) ?>
                        </div>
                    </div>
					
					<div class="form-group">
                        <div class="input-icon icon-left icon-lg icon-color-primary">
                            <?= $form->field($model, 'your_name')->textInput([ 'class' => 'form-control', 'placeholder' => 'Full Name']) ?>
                        </div>
                    </div>
					
                   
					
                    <div class="form-group">
                        <button type="submit" name="save" class="btn btn-primary btn-block">Continue</button>
                    </div>
                    <div class="ks-text-center">
                        <span class="text-muted">By creating account you agree to </span> <a href="https://chatmetrics.com/terms/">Terms Of Service</a>
                    </div>
                    
                    
   <?php ActiveForm::end(); ?>