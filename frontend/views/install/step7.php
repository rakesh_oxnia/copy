<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>
<div class="body-content">
		<div class="main-box-body clearfix">
				<div id="myWizard" class="wizard">
						<div class="wizard-inner">
							<ul class="steps">
							<li data-target="#step1"><span class="badge">1</span>Step 1<span class="chevron"></span></li>
							<li data-target="#step2"><span class="badge">2</span>Step 2<span class="chevron"></span></li>
							<li data-target="#step3"><span class="badge">3</span>Step 3<span class="chevron"></span></li>
							<li data-target="#step4"><span class="badge">4</span>Step 4<span class="chevron"></span></li>
							<li data-target="#step5"><span class="badge">5</span>Step 5<span class="chevron"></span></li>
							<li data-target="#step6"><span class="badge">6</span>Step 6<span class="chevron"></span></li>
							<li data-target="#step7" class="active"><span class="badge badge-primary">7</span>Step 7<span class="chevron"></span></li>
							<li data-target="#step8"><span class="badge">8</span>Step 8<span class="chevron"></span></li>
							</ul>
							<div class="actions">
								<a href="<?php echo Url::to(['/install/step6']);?>">	
									<button type="button" class="btn btn-default btn-mini btn-prev"> <i class="icon-arrow-left"></i>Prev</button>
								</a>
								<a href="<?php echo Url::to(['/install/step8']);?>">
									<button type="button" class="btn btn-success btn-mini btn-next" data-last="Finish">Next<i class="icon-arrow-right"></i></button>
								</a>
							</div>
						</div>
						<div class="step-content">
							<?php $form = ActiveForm::begin(); ?>
								<div class="step-pane active" id="step1">
									<br/>
									<h1>Install Code on Your Site.</h1>
									
									  
									<div class="form-group">		
										<textarea class="form-control" placeholder="Code"></textarea>
									</div>
									
									<div class="form-group">		
										<input value="Install" name="save" type="submit" class="btn btn-primary">
									</div>
								</div>
							<?php ActiveForm::end(); ?>
						</div>
				</div>
		</div>
	</div>
				 