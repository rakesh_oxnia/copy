<?php

use yii\helpers\Html;
use yii\grid\GridView;

use app\models\Leads;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Leads';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-index2 col-md-12">
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<div class="row">
		<div class="col-lg-12">
			<div id="content-header" class="clearfix">
				<div class="pull-left">
					<ol class="breadcrumb">
						<li><a href="<?= Yii::$app->homeUrl;?>">Home</a></li>
						<li class="active"><span><?= Html::encode($this->title) ?></span></li>
					</ol>
					<h1><?= Html::encode($this->title) ?></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="main-box">
				<div class="main-box-body clearfix">
					<div class="row">
						<br>
						Filter By <div class="btn-group">
							<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
								<?php 
								if(isset($_GET["filter"])){
									if($_GET["filter"] == 'crange')
										echo 'Custom Range';
									elseif($_GET["filter"] == 'last7')
										echo 'Last 7days';
									elseif($_GET["filter"] == 'last30')
										echo 'Last 30days';
									else
										echo $_GET["filter"];
								}
								else{
									echo "Last 7days";
								}
								?>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<?php $today = date('Y-m-d');?>
								<li><a href="?filter=today&date_from=<?=$today ;?>&date_to=<?=$today ;?>&group_by=hour">Today</a></li>
								<?php $yesterday = date('Y-m-d');?>
								<?php $last75 = date('Y-m-d',strtotime("-1 days"));?>
								<li><a href="?filter=yesterday&date_from=<?=$last75 ;?>&date_to=<?=$last75 ;?>&group_by=hour">Yesterday</a></li>
								<?php $last7 = date('Y-m-d',strtotime("-6 days"));?>
								<li><a href="?filter=last7&date_from=<?=$last7 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 7days</a></li>
								<?php $last30 = date('Y-m-d',strtotime("-29 days"));?>
								<li><a href="?filter=last30&date_from=<?=$last30 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 30days</a></li>
								<li><a href="?filter=crange" id="c_range">Custom Range</a></li>
							</ul>
						</div>					
						<div class="daterun">
							<form id="range-form" style="display:none;" class="top-formsearch">
								<ul> 
									<li><label>Date From</label><input type="text" id="datefrom" name="date_from"></li>
									<li><label>Date TO</label><input type="text" id="dateto" name="date_to"></li>
									<input type="hidden" name="filter" value="crange">
									<input type="hidden" name="group_by" value="day">
									<li><input type="submit" value="Filter"></li>
								</ul>								
							</form>
						</div>
							<span class="">Total Leads : <?= $total; ?></span>
					</div>
				</div>
			</div>
		</div>
	</div> 
	<div class="row">
    <?php 
		
		/* echo '<pre>';
		print_r($chats);
		echo '</pre>'; */
		if(count($transcripts)){ ?>
			<div class="table-responsive">			
				<table class="table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Region</th>
							<!--th>agents</th-->
							<th>Country</th>
							<th>Date</th>
							<th>Rate</th>
							<th>Action Status</th>
							<th>View Survey</th>
							<th>View transcript</th>
							<th>Update</th>
							<th>Email</th>
						</tr>
					</thead>
					<tbody>
						<?php
						//echo '<pre>';print_r($transcripts);echo '</pre>';die;
						foreach($transcripts as $chat){
						//echo '<pre>';print_r($chat->visitor->email);echo '</pre>';die;
							//if($chat->chat_start_url == Yii::$app->user->identity->website_url){ ?>
							<tr>
								<td><?= $chat->visitor->name; ?></td>
								<td><?php if(isset($chat->visitor->email)){
									echo $chat->visitor->email;
								}else{
									echo '--';
								} ?></td>
								<td><?= $chat->visitor->region; ?></td>
								<!--td>
									<?php
									/* foreach($chat->agents as $agent){
										echo $agent->display_name;
										echo "(".$agent->email.")";
										echo '<br>';
									} */
									?>
								</td-->
								<td><?= $chat->visitor->country; ?></td>
								<td><?= date('Y-m-d',$chat->started_timestamp); ?></td>
								<td>
									<?php
										if($chat->rate == 'rated_bad'){
											echo '<span class="text-danger fa-stack"><i class="fa fa-thumbs-down fa-stack-2x"></i></span>';
										} elseif($chat->rate == 'rated_good'){
											echo '<span class="text-success fa-stack"><i class="fa fa-thumbs-up fa-stack-2x"></i></span>';
										} else{
											echo '--';
										};
									?>
								</td>
								<td><?php 
								$satus = Leads::getStatus($chat->id); 
								//$emailsatus = Leads::checkEmail($chat->id); 
								//$userdata = $app->user->identity;
								//echo '<pre>';print_r($userdata->email);echo '</pre>';die('ghj');
								/* if($emailsatus == 0 || $emailsatus == ''){
									$sent = Leads::sendEmail($userdata);
									if($sent){
										$model = Leads::find()->where(['chat_id'=>$chat->id])->one();
										$model->email = 1;
										$model->save();
									}
								} */
								if($satus=='pending'):
									echo '<span class="label label-warning">Pending</span>';
								elseif($satus=='approved'):
									echo '<span class="label label-success">Approved</span>';
								else:
									echo '<span class="label label-danger">Rejected</span>';
								endif;
								
								?></td>
								<td><a href="<?= Yii::$app->urlManager->createUrl(['leads/survey', 'id' => $chat->id ])?>"> View Survey </a></td>
								<td><a href="<?= Yii::$app->urlManager->createUrl(['leads/view', 'id' => $chat->id ])?>"> View transcript </a></td>
								<td><a href="<?= Yii::$app->urlManager->createUrl(['leads/update', 'id' => $chat->id ])?>"> Update </a></td>
								<td><?php
								
								$email = Leads::getMail($chat->id);
								
								if($email=='1'):
									echo '<span class="label label-success">Sent</span>';
								else:
									echo '<span class="label label-danger">Not Sent</span>';
								endif;
								
								?>  <strong><a href="<?= Yii::$app->homeurl;?>leads/sendmail?id=<?= $chat->id;?>">Send Mail</a></strong></td>
							</tr>
							<?php
							//}
						}
						?>
					</tbody>
				</table>
			</div>
			
			<?php 
			
			if(isset($_GET["filter"])){
				//index?filter=last30&date_from=2016-04-27&date_to=2016-05-26&group_by=day
				$page = '?filter='.$_GET['filter'].'&date_from='.$_GET['date_from'].'&date_to='.$_GET['date_to'].'&group_by='.$_GET['group_by'].'&page=';
			}
			else{
				$page = "?page=";
			}
			
			?>
			
			<?php if($pages>1){ ?>
				<ul class = "pagination pagination-sm">
				   <li><a href = "<?= $page;?>1">&laquo;</a></li>
				   <?php for($i=1;$i<=$pages;$i++){ ?>
				   <li><a href = "<?= $page;?><?= $i;?>"><?= $i;?></a></li>
				   <?php } ?>
				   <li><a href = "<?= $page;?><?php echo $pages;?>">&raquo;</a></li>
				</ul>
			<?php } ?>
			
			<?php
		}
		else{ ?>
			<div class="col-md-12">No leads found</div>
		<?php }
		
	?>

</div>
</div>

<?php 

$this->registerJs(
    'jQuery("document").ready(function(){
		jQuery("#dateto,#datefrom").datepicker( {dateFormat: "yy-mm-dd",changeMonth:true,changeYear:true} );
		
		jQuery("#c_range").click(function(e){
			e.preventDefault();
			$("#range-form").show();
		})
	});'
, \yii\web\View::POS_END);


