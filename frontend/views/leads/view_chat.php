<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;
use app\models\Leads;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\CustomLeadFields;
use app\models\CustomLeadFieldsOption;

$this->title = "View Leads";

?>

		<div class="ks-page-header">
            <section class="ks-title">
                <h3>Chats</h3>

                <button class="btn btn-primary-outline ks-light ks-activity-block-toggle" data-block-toggle=".ks-activity-dashboard > .ks-activity">Activity</button>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-dashboard-tabbed-sidebar">
                    <div class="ks-dashboard-tabbed-sidebar-widgets">

						<div class="chatwind col-md-12">

						 <div class="row">
							<div class="col-xl-4">
								<div class="ks-dashboard-widget ks-widget-amount-statistics ks-info">
									<div class="ks-statistics">
										<span class="ks-text">Chat Reference #</span>
										<span class="ks-amount"><?= $chat->id;?></span>
									</div>

								</div>
							</div>
							<div class="col-xl-4">
								<div class="ks-dashboard-widget ks-widget-amount-statistics ks-success">
									<div class="ks-statistics">
										<span class="ks-text">Duration of chat</span>
										<span class="ks-amount"><?= $chat->duration;?> Seconds</span>
									</div>

								</div>
							</div>

							 <div class="col-xl-4">
								<div class="ks-dashboard-widget ks-widget-amount-statistics ks-purple">
									<div class="ks-statistics">
										<span class="ks-text">Visitor Frequency</span>
										<span class="ks-amount">0</span>
									</div>

								</div>
							</div>

						</div>

						 <div class="row">
							<div class="col-xl-6">
							<?php
								$satus = Leads::getStatus($chat->id);
								if($satus=='pending'):
									$class="warning";
								elseif($satus=='approved'):
									$class="success";
								else:
									$class="danger";
								endif
							?>
										<div class="ks-dashboard-widget ks-widget-amount-statistics ks-<?php echo $class;?>">
											<div class="ks-statistics">
												<span class="ks-text">
													Chats Status
												</span>
												<span class="ks-amount"><?php echo $satus = Leads::getStatus($chat->id);?></span>
											</div>

										</div>
										<br>
								 <div id="ks-bar-chart-panel" class="card panel">
									<h5 class="card-header">
										INFORMATION
									</h5>
									<div class="card-block">

										<fieldset class="form-group">
												<label class="form-control-label">Geolocation: <label>
												<?php echo $chat->visitor->city . ", " . $chat->visitor->region . ", " . $chat->visitor->country;?>
										</fieldset>

										<?php
											$leadModel = Leads::findOne(['chat_id' => $chat->id]);
											$visitor_name = isset($chat->visitor->name) ? $chat->visitor->name : NULL;
											$visitor_email = isset($chat->visitor->email) ? $chat->visitor->email : NULL;
											$visitor_phone = 'Not provided';
											if($leadModel){
												if($visitor_name == NULL){
													$visitor_name = $leadModel->visitor_name;
												}
												if($visitor_email == NULL){
													$visitor_email = $leadModel->visitor_email;
												}
												if($leadModel->visitor_phone != NULL){
													$visitor_phone = $leadModel->visitor_phone;
												}
											}

											if($visitor_name == NULL){
												$visitor_name = 'Not provided';
											}

											if($visitor_email == NULL){
												$visitor_email = 'Not provided';
											}

										?>

										<!-- <fieldset class="form-group">
												<label class="form-control-label">CONTACT NAME: <label>
                          <?php // echo $chat->visitor->name;?>
													<?=$visitor_name?>
										</fieldset>

										<fieldset class="form-group">
												<label class="form-control-label">EMAIL ADDRESS: <label>
                          <?php // echo $chat->visitor->email; ?>
													<?=$visitor_email?>
										</fieldset>

										<fieldset class="form-group">
												<label class="form-control-label">Phone: <label>
													<?=$visitor_phone?>
										</fieldset> -->

										<?php $form = ActiveForm::begin(['action' => Url::to(['leads/update', 'id' => $model->chat_id])]); ?>

								    <?= $form->field($model, 'visitor_name')->textInput(['maxlength' => true]); ?>

								    <?= $form->field($model, 'visitor_email')->textInput(['maxlength' => true]); ?>

										<?= $form->field($model, 'visitor_phone')->textInput(['maxlength' => true]); ?>

										<?= $form->field($model, 'category_id')->dropdownList($categories, ["prompt" => "Select Category", 'class' => 'select form-control']); ?>
										<?php
											if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE){
												$outlets = User::findAll(['role' => User::ROLE_OUTLET, 'parent_id' => Yii::$app->user->identity->id]);
												$outlets = ArrayHelper::map($outlets, 'id', 'name');
												echo $form->field($model, 'outlet_id')->dropdownList($outlets, ["prompt" => "Select Outlet", 'class' => 'select form-control']);
											}
										?>


										<?= $form->field($model, 'chat_summary')->textarea(['rows' => 6]); ?>

								    <div class="form-group">
								        <?= Html::submitButton($model->isNewRecord ? 'Update' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
								    </div>

								    <?php ActiveForm::end(); ?>

									</div>
								</div>
							</div>
							<div class="col-xl-6">
								<div id="ks-bar-chart-panel" class="card panel">
									<h5 class="card-header">
										Chat Actions
									</h5>
									<div class="card-block">
										<!-- <fieldset class="form-group">
											<a href="<?php echo Url::to(['/feedback/add']);?>">
												<input type="button" class="btn btn-primary btn-lg btn-block" name="add_feedback" value="Give Feedback">
											</a>
										</fieldset>

										<fieldset class="form-group">
											<a href="<?php echo Url::to(['/faq/add']);?>">
												<input type="button" class="btn btn-primary-outline btn-lg btn-block" name="add_faq" value="Add a FAQ">
											</a>
										</fieldset> -->

										<?php if($satus!=='pending'){ ?>
											<fieldset class="form-group">
												<a href="<?php echo Url::to(['/leads/pending', 'id' => $chat->id]);?>">
													<input type="button" class="btn btn-warning btn-lg btn-block" name="add_faq" value="Mark Pending">
												</a>
											</fieldset>
										<?php }?>

										<?php if($satus!=='approved'){ ?>
											<fieldset class="form-group">
												<a href="<?php echo Url::to(['/leads/approved', 'id' => $chat->id]);?>">
													<input type="button" class="btn btn-success btn-lg btn-block" name="add_faq" value="Complete">
												</a>
											</fieldset>
										<?php }?>
										<?php if($satus!=='rejected'){ ?>
											<fieldset class="form-group">
												<a href="<?php echo Url::to(['/leads/rejected', 'id' => $chat->id]);?>">
													<input type="button" class="btn btn-danger btn-lg btn-block" name="add_faq" value="In progress">
												</a>
											</fieldset>
										<?php }?>
									</div>
								</div>

								<?php
								if(!empty($leadModel->custom_fields)){ ?>
									<div id="ks-bar-chart-panel" class="card panel">
										<h5 class="card-header">
											Custom Fields
										</h5>
										<div class="card-block">
										<?php									
								          $custom_fields = json_decode($leadModel->custom_fields,true);
								          $customdata = [];
								          $multipleChoiceString = '';
								          foreach($custom_fields as $key => $custom_field){
								            $custom_table = CustomLeadFields::findOne($custom_field['field_id']);
								            if($custom_field['field_type']=='textbox') {
								              $fieldvalue = $custom_field['field_value'];
								              echo '<fieldset class="form-group">
								              	<label>' . htmlspecialchars_decode($custom_table['label']) . ' : </label>'. urldecode($fieldvalue);	
								            }elseif ($custom_field['field_type']=='dropdown'){
								              $dropDownOption = CustomLeadFieldsOption::findOne($custom_field['field_value']);
								              $fieldvalue2 = $dropDownOption['option'];
								              echo '<fieldset class="form-group">
								              	<label>' . htmlspecialchars_decode($custom_table['label']) . ' : </label>'. urldecode($fieldvalue2);
								            }elseif ($custom_field['field_type']=='multiple_choice'){
								              $custom_field_id_arrays = explode(',', urldecode($custom_field['field_value']));
								              $multipleChoiceOptions = CustomLeadFieldsOption::findAll($custom_field_id_arrays);
								              foreach($multipleChoiceOptions as $multipleChoiceOption){
								                $multipleChoiceString .= $multipleChoiceOption['option'].',';
								              }
								              //print_r($multipleChoiceOption);exit;
								               $fieldvalue3 = rtrim($multipleChoiceString,', ');
												echo '<fieldset class="form-group">
													<label>' . htmlspecialchars_decode($custom_table['label']) . ' : </label>'. urldecode($fieldvalue3);
								            }
								          }							    
										?>
										</div>
									</div>
								<?php } ?>

							</div>
						</div>
	<br><br>
    <div class="panel panel-default">
		<div class="panel-heading">
			<i class="fa fa-comments fa-fw"></i>
			Prechat Survey
		</div>
        <!-- /.panel-heading -->
		<div class="panel-body">

				<?php if(isset($chat2->prechat_survey)&& !empty($chat2->prechat_survey)){
				foreach($chat2->prechat_survey as $prechat_survey){ ?>

					<p><b>Question : </b><?= $prechat_survey->key;?></p>
					<p><b>Answer : </b><?= $prechat_survey->value;?></p>

				<?php } }else{
				?>

					<p>Survey Not found</p>

				<?php
}				// end foreach?>

		</div>
		<!-- /.panel-body -->
	</div>
<br>
    <div class="panel panel-default">
		<div class="panel-heading">
			<i class="fa fa-comments fa-fw"></i>
			Postchat Survey
		</div>
        <!-- /.panel-heading -->
		<div class="panel-body">

				<?php if(isset($chat2->postchat_survey) && !empty($chat2->postchat_survey)){
				foreach($chat2->postchat_survey as $postchat_survey){ ?>

					<p><b>Question : </b><?= $postchat_survey->key;?></p>
					<p><b>Answer : </b><?= $postchat_survey->value;?></p>


				<?php }
                ?>
				<p>&nbsp</p>
				<p><b>Rating : </b><?php
									 if($chat2->rate == 'rated_bad'){
										echo '<span class="text-danger fa-stack"><i class="fa fa-thumbs-down fa-stack-2x"></i></span>';
									} elseif($chat2->rate == 'rated_good'){
										echo '<span class="text-success fa-stack"><i class="fa fa-thumbs-up fa-stack-2x"></i></span>';
									} else{
										echo '--';
									};
								?></p>
				<?php
				}else{
				?>

					<p>Survey Not found</p>

				<?php
}		 // end foreach?>

		</div>
		<!-- /.panel-body -->
	</div>

</div>
<br>
<div class="chatwind col-md-12">

    <div class="chat-panel panel panel-default">
		<div class="panel-heading">
			<i class="fa fa-comments fa-fw"></i>
			Chat Transcript
		</div>
        <!-- /.panel-heading -->
		<div class="panel-body">
			<table class="table table-bordered table-hover">
				<?php if(isset($chat->messages)){ ?>
				<?php foreach($chat->messages as $messages){ ?>
				<?php if($messages->user_type !== 'visitor') { ?>
				   <tr class="table-warning">
						<td>
							<img src="http://oxnia.com/externalassets/agent-icon.png" alt="User Avatar" class="img-circle">
						</td>
						<td width="20%">
							<strong class="primary-font"><?= $messages->author_name;?></strong><br>
							<small class="text-muted">
								<i class="fa fa-clock-o fa-fw"></i><?= $messages->date;?>
							</small>
						</td>
						<td>
							<?= $messages->text;?>
						</td>

				    </tr>
				<?php } else{ ?>
				    <tr>
						<td>
								<img src="http://oxnia.com/externalassets/customer-icon.png" alt="User Avatar" class="img-circle">
						</td>
						<td width="20%">
								<strong class="primary-font"><?= $messages->author_name;?> (Visitor)</strong><br>
								<small class=" text-muted">
									<i class="fa fa-clock-o fa-fw"></i> <?= $messages->date;?>
								</small>

						</td>
						<td>
							<?= $messages->text;?>
						</td>

				   </tr>
				<?php } //end if ?>
				<?php } // end foreach?>
				<?php } // end if?>
			</table>

		</div>
		<!-- /.panel-body -->
	</div>

</div>


                    </div>

                </div>
            </div>
        </div>

<?php
    $this->registerCss("
        .card-header:first-child {
        	background-color: #ebeef5 !important;
        }	
        .panel-heading {
        	background-color: #ebeef5 !important;
        }	       
	    .select {
	    	max-width: 82%;
	    }
		label
		{
	    text-transform:capitalize;
		}
    ");
	$this->registerJs("
		$('.select').select2();
	");
?>
