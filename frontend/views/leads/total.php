<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii2assets\printthis\PrintThis;
use yii\helpers\Url;
use common\models\User;

/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Total Leads';
$this->params['breadcrumbs'][] = $this->title;

//echo '<pre>';print_r($goals);echo '</pre>';die;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/swiper/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/widgets/tables.min.css">

		<div class="ks-page-header">
            <section class="ks-title">
                <h3>Total Leads</h3>
		         <?php        
		          echo $this->render("../lead-list/_filter", ['website_users' => $website_users, 'outlets' => $outlets]); 
		        ?>                
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="container-fluid ks-rows-section">
                    <div class="row">
                        <div class="col-lg-12 ks-panels-column-section">
                            <div class="card panel ks-information ks-light">
                                <h5 class="card-header">


							<div class="btn-group">
							<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
								<?php
								if(isset($_GET["filter"])){
									if($_GET["filter"] == 'crange')
										echo 'Custom Range';
									elseif($_GET["filter"] == 'last7')
										echo 'Last 7days';
									elseif($_GET["filter"] == 'last30')
										echo 'Last 30days';
									else
										echo $_GET["filter"];
								}
								else{
									echo "Last 7days";
								}
								?>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-left" role="menu">
								<?php $today = date('Y-m-d');?>
								<li><a href="<?=Url::to(['leads/total'])?>&filter=today&date_from=<?=$today ;?>&date_to=<?=$today ;?>&group_by=hour">Today</a></li>
								<?php $yesterday = date('Y-m-d');?>
								<?php $last75 = date('Y-m-d',strtotime("-1 days"));?>
								<li><a href="<?=Url::to(['leads/total'])?>&filter=yesterday&date_from=<?=$last75 ;?>&date_to=<?=$last75 ;?>&group_by=hour">Yesterday</a></li>
								<?php $last7 = date('Y-m-d',strtotime("-6 days"));?>
								<li><a href="<?=Url::to(['leads/total'])?>&filter=last7&date_from=<?=$last7 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 7days</a></li>
								<?php $last30 = date('Y-m-d',strtotime("-29 days"));?>
								<li><a href="<?=Url::to(['leads/total'])?>&filter=last30&date_from=<?=$last30 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 30days</a></li>
								<li><a href="?filter=crange" id="c_range">Custom Range</a></li>
							</ul>

							<form id="range-form" style="display:none;margin-left : 10px;" class="top-formsearch">
								 <div class="row">
									<div class="col-sm-4">
										<input type="text" placeholder="Date From" class="form-control" id="datefrom" name="date_from">
									</div>
									<div class="col-sm-4">
										<input type="text" placeholder="Date To" class="form-control" id="dateto" name="date_to">
									</div>
									<input type="hidden" name="filter" value="crange">
									<input type="hidden" name="group_by" value="day">
									<div class="col-sm-4">
										<input type="submit" class="btn btn-info" value="Filter">
									</div>
								</div>
							</form>

							<div style="margin-left:10px;">
								 <?php
									echo PrintThis::widget([
										'htmlOptions' => [
											'id' => 'PrintThis',
											'btnClass' => 'btn btn-primary',
											'btnId' => 'btnPrintThis',
											'btnText' => 'Print',
											'btnIcon' => 'fa fa-print'

										],
										'options' => [
											'debug' => false,
											'importCSS' => true,
											'importStyle' => false,
											'loadCSS' => "cube/css/bootstrap/bootstrap.min.css",
											'pageTitle' => "",
											'removeInline' => false,
											'printDelay' => 333,
											'header' => null,
											'formValues' => true,
											'canvas' => true,
										]
									]);
									?>
							</div>

                           <?php
                             if(count($siteOutlets) > 0 && sizeOf($userData) == 1){
                           ?>
                               <div style="margin-left: 20px;">
                                 <select id='outlet_select' class='form-control'>
                                   <option value=''>Select Outlet</option>
                                   <?php
               											$def_outlet = Yii::$app->session->has('outlet_filter') ? Yii::$app->session->get('outlet_filter') : '';
               											foreach ($siteOutlets as $outlet) {
               												$selected = ($def_outlet == $outlet->id) ? 'selected' : '';
               												echo "<option value='$outlet->id' $selected > $outlet->name </option>";
               											}
               										?>
                                 </select>
                               </div>
                           <?php
                             }
                           ?>

						</div>

			</h5>


                                <div class="card-block ks-datatable">
    <div class="row" id="PrintThis">
		<div class="col-md-12">
			<div class="main-box">
				<header class="main-box-header clearfix">
					<h2 class="pull-left">Total Leads : <?=$total;?></h2>
				</header>
				<div class="main-box-body clearfix">
					<div class="row">
						<div class="col-md-9">
							<div id="graph-bar" style="height: 240px; padding: 0px; position: relative;"></div>
						</div>
						<div class="col-md-3">
							<p><?=$total;?> Leads</p>
							<p><?=$total2;?> Pending</p>
							<p><?=$total4;?> Complete</p>
							<p><?=$total3;?> In progress</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

	<?php

$show_bars = 0;
$show_lines = 1;
$show_points = 1;
$divide = 1;
$num_m = 1;

$this->registerJs(
    "$('document').ready(function(){

			function queryStringUrlReplacement(url, param, value)
			{
			  var re = new RegExp('[\\?&]' + param + '=([^&#]*)'), match = re.exec(url), delimiter, newString;

			  if (match === null) {
			      // append new param
			      var hasQuestionMark = /\?/.test(url);
			      delimiter = hasQuestionMark ? '&' : '?';
			      newString = url + delimiter + param + '=' + value;
			  } else {
			      delimiter = match[0].charAt(0);
			      newString = url.replace(re, delimiter + param + '=' + value);
			  }

			  return newString;
			}

		jQuery('#dateto,#datefrom').datepicker( {dateFormat: 'yy-mm-dd',changeMonth:true,changeYear:false} );

		$('#outlet_select').select2();

		$('#outlet_select').on('change', function(e){
			current_url = window.location.href;
			id = $(this).val();
			search_url = queryStringUrlReplacement(current_url, 'outlet', id);
			window.location = search_url;
		});


		$('#c_range').click(function(e){
			e.preventDefault();
			$('#range-form').show();
		})
		$('#range-form').on('submit', function(e){
			from = $('#datefrom').val();
				to = $('#dateto').val();
			console.log('From: ' + from);
			console.log('To: ' + to);
			url = '".Url::to(['leads/total'])."&filter=crange&date_from=' + from + '&date_to=' + to + '&group_by=day';
			window.location = url;
			return false;
		});
		function gd(year, day, month) {
			return new Date(year, month, day).getTime();
		}
		if ($('#graph-bar').length) {
			var data1 = [
			    $data_str
			];var data2 = [
			    $data_str2
			];var data3 = [
			    $data_str3
			];var data4 = [
			    $data_str4
			];


			var series = new Array();

			series.push({
				data: data1,
				bars: {
					show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000 / parseInt($divide),
					lineWidth: 1 / parseInt($num_m),
					fill: 1,
					align: 'center'
				},
				label: 'Total Leads',
				lines: {
					show : $show_lines,
				},
				points: {
					show: $show_points
				}
			});

			series.push({
				data: data2,
				bars: {
					show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000 / parseInt($divide),
					lineWidth: 1 / parseInt($num_m),
					fill: 1,
					align: 'center'
				},
				label: 'Pending Leads',
				lines: {
					show : $show_lines,
				},
				points: {
					show: $show_points
				}
			});

			series.push({
				data: data4,
				bars: {
					show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000 / parseInt($divide),
					lineWidth: 1 / parseInt($num_m),
					fill: 1,
					align: 'center'
				},
				label: 'Complete Leads',
				lines: {
					show : $show_lines,
				},
				points: {
					show: $show_points
				}
			});

			series.push({
				data: data3,
				color: '#ff0000',
				bars: {
					show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000 / parseInt($divide),
					lineWidth: 1 / parseInt($num_m),
					fill: 1,
					align: 'center'
				},
				label: 'In progress Leads',
				lines: {
					show : $show_lines,
				},
				points: {
					show: $show_points
				}
			});

			$.plot('#graph-bar', series, {
				colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
				ticks : 10,
				grid: {
					tickColor: '#f2f2f2',
					//borderWidth: 0,
					hoverable: true,
					//clickable: true
				},
				legend: {
					noColumns: 1,
					// labelBoxBorderColor: '#000000',
					// position: 'ne'
				},
				shadowSize: 0,
				xaxis: {
					mode: 'time',
					//timeformat: '%d %b',
					//timeformat:'%S'
				},
				yaxis:{
					tickDecimals:false,
					minTickSize:1
				}
			});

			var previousPoint = null;
			$('#graph-bar').bind('plothover', function (event, pos, item) {
				if (item) {
					if (previousPoint != item.dataIndex) {

						previousPoint = item.dataIndex;

						$('#flot-tooltip').remove();
						var x = item.datapoint[0],
						y = item.datapoint[1];

						showTooltip(item.pageX, item.pageY, item.series.label, y );
					}
				}
				else {
					$('#flot-tooltip').remove();
					previousPoint = [0,0,0];
				}
			});
			function showTooltip(x, y, label, data) {
				$('<div id=\"flot-tooltip\">' + '<b>' + label + ': </b><i>' + data + '</i>' + '</div>').css({
					top: y + 5,
					left: x + 20
				}).appendTo('body').fadeIn(200);
			}
		}

	});"
, \yii\web\View::POS_END);
