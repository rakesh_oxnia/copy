<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'View Transcript';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
	<div class="col-lg-12">
		<div id="content-header" class="clearfix">
			<div class="pull-left">
				<ol class="breadcrumb">
					<li><a href="<?= Yii::$app->homeUrl;?>">Home</a></li>
					<li><a href="<?= Yii::$app->homeUrl;?>leads/index">leads</a></li>
					<li class="active"><span><?= Html::encode($this->title) ?></span></li>
				</ol>
				<h1><?= Html::encode($this->title) ?></h1>
			</div>
		</div>
	</div>
</div>
<div class="chatwind col-md-12">

    <div class="chat-panel panel panel-default">
		<div class="panel-heading">
			<i class="fa fa-comments fa-fw"></i>
			Chat History
		</div>
        <!-- /.panel-heading -->
		<div class="panel-body">
			<ul class="chat">
				<?php if(isset($chat->messages)){ ?>
				<?php foreach($chat->messages as $messages){ ?>
				<?php if($messages->user_type !== 'visitor') { ?>
				<li class="left clearfix">
					<span class="chat-img pull-left">
						<img src="http://placehold.it/50/55C1E7/fff" alt="User Avatar" class="img-circle">
					</span>
					<div class="chat-body clearfix">
						<div class="header-chat">
							<strong class="primary-font"><?= $messages->author_name;?> (Agent)</strong>
							<small class="pull-right text-muted">
								<i class="fa fa-clock-o fa-fw"></i><?= $messages->date;?>
							</small>
						</div>
						<p>
							<?= $messages->text;?>
						</p>
					</div>
				</li>
				<?php } else{ ?>
				<li class="right clearfix">
					<span class="chat-img pull-right">
						<img src="http://placehold.it/50/FA6F57/fff" alt="User Avatar" class="img-circle">
					</span>
					<div class="chat-body clearfix">
						<div class="header-chat">
							<small class=" text-muted">
								<i class="fa fa-clock-o fa-fw"></i> <?= $messages->date;?></small>
							<strong class="pull-right primary-font"><?= $messages->author_name;?> (Visitor)</strong>
						</div>
						<p>
							<?= $messages->text;?>
						</p>
					</div>
				</li>
				<?php } //end if ?>
				<?php } // end foreach?>
				<?php } // end if?>
			</ul>
		</div>
		<!-- /.panel-body -->
	</div>

</div>
