<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

use app\models\Leads;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Leads';
$this->params['breadcrumbs'][] = $this->title;

?>

<a href="<?php echo Url::to(['/leads/export']);?>" target="_new">download</a>