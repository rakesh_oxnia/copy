<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'View Survey';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row">
	<div class="col-lg-12">
		<div id="content-header" class="clearfix">
			<div class="pull-left">
				<ol class="breadcrumb">
					<li><a href="<?= Yii::$app->homeUrl;?>">Home</a></li>
					<li><a href="<?= Yii::$app->homeUrl;?>leads/index">leads</a></li>
					<li class="active"><span><?= Html::encode($this->title) ?></span></li>
				</ol>
				<h1><?= Html::encode($this->title) ?></h1>
			</div>
		</div>
	</div>
</div>
<div class="chatwind col-md-12">

    <div class="panel panel-default">
		<div class="panel-heading">
			<i class="fa fa-comments fa-fw"></i>
			Prechat Survey
		</div>
        <!-- /.panel-heading -->
		<div class="panel-body">
			<ul class="">
				<?php if(isset($chat->prechat_survey)&& !empty($chat->prechat_survey)){ 
				foreach($chat->prechat_survey as $prechat_survey){ ?>
				<li class="">
					<p><b>Question : </b><?= $prechat_survey->key;?></p>
					<p><b>Answer : </b><?= $prechat_survey->value;?></p>
				</li>
				<?php } }else{
				?>
				<li class="">
					<p>Survey Not found</p>
				</li>
				<?php
}				// end foreach?>
			</ul>
		</div>
		<!-- /.panel-body -->
	</div>

    <div class="panel panel-default">
		<div class="panel-heading">
			<i class="fa fa-comments fa-fw"></i>
			Postchat Survey
		</div>
        <!-- /.panel-heading -->
		<div class="panel-body">
			<ul class="">
				<?php if(isset($chat->postchat_survey) && !empty($chat->postchat_survey)){ 
				foreach($chat->postchat_survey as $postchat_survey){ ?>
				<li class="">
					<p><b>Question : </b><?= $postchat_survey->key;?></p>
					<p><b>Answer : </b><?= $postchat_survey->value;?></p>
					
				</li>
				<?php }
                ?>
				<p>&nbsp</p>
				<p><b>Rating : </b><?php
									 if($chat->rate == 'rated_bad'){
										echo '<span class="text-danger fa-stack"><i class="fa fa-thumbs-down fa-stack-2x"></i></span>';
									} elseif($chat->rate == 'rated_good'){
										echo '<span class="text-success fa-stack"><i class="fa fa-thumbs-up fa-stack-2x"></i></span>';
									} else{
										echo '--';
									};
								?></p>
				<?php
				}else{
				?>
				<li class="">
					<p>Survey Not found</p>
				</li>
				<?php
}		 // end foreach?>
			</ul>
		</div>
		<!-- /.panel-body -->
	</div>

</div>
