<?php

use yii\helpers\Html;
use app\models\Leads;
use yii\widgets\ActiveForm;
use common\models\User;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Leads */

$this->title = 'Update Lead ';// . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Leads', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

?>

<?php
	$satus = Leads::getStatus($chat->id);
	if($satus=='pending'):
		$class="warning";$st = 'Pending';
	elseif($satus=='approved'):
		$class="success";$st = 'Approved';
	else:
		$class="danger";$st = 'Rejected';
	endif
?>
<div class="ks-page-header">
            <section class="ks-title">
                <h3>Leads</h3>

                <button class="btn btn-primary-outline ks-light ks-activity-block-toggle" data-block-toggle=".ks-activity-dashboard > .ks-activity">Activity</button>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-dashboard-tabbed-sidebar">
                    <div class="ks-dashboard-tabbed-sidebar-widgets">

						<div class="chatwind col-md-12">

	<div class="row">

		<div class="col-md-4">

			<div class="ks-dashboard-widget ks-widget-amount-statistics ks-<?= $class; ?>">
				<div class="ks-statistics">
					<span class="ks-text">Lead Status</span>
					<span class="ks-amount"><?= $st; ?></span>
				</div>
			</div>
			<br>
			<div class="ks-dashboard-widget ks-widget-amount-statistics ks-purple">
				<div class="ks-statistics">
					<span class="ks-text">Chat Duration</span>
					<span class="ks-amount"><?= $chat->duration;?> Sec</span>
				</div>
			</div>

		</div>

		<div class="col-md-4">

			<div class="main-box clearfix">
				<header class="main-box-header clearfix">
					<h2 class="pull-left">Lead Details</h2>
				</header>
				<!-- <div class="main-box-body clearfix">
					<p><b>name : </b>
					<?php if(isset($chat->visitor->name)){echo $chat->visitor->name;} ?></p>

					<p>
						<b>email : </b>
						<?php if(isset($chat->visitor->email)){echo $chat->visitor->email;} ?>
					</p>

					<p>
						<b>city : </b>
						<?php if(isset($chat->visitor->city)){echo $chat->visitor->city;} ?>
					</p>

					<p>
						<b>region : </b>
						<?php if(isset($chat->visitor->region)){echo $chat->visitor->region;} ?>
					</p>
					<p>
						<b>country : </b>
						<?php if(isset($chat->visitor->country)){echo $chat->visitor->country;} ?>
					</p>
				</div> -->

				<?php $form = ActiveForm::begin(); ?>

		    <?= $form->field($model, 'visitor_name')->textInput(['maxlength' => true]); ?>

		    <?= $form->field($model, 'visitor_email')->textInput(['maxlength' => true]); ?>

				<?= $form->field($model, 'visitor_phone')->textInput(['maxlength' => true]); ?>

				<?= $form->field($model, 'category_id')->dropdownList($categories, ["prompt" => "Select Category"]); ?>
				<?php
					if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE){
						$outlets = User::findAll(['role' => User::ROLE_OUTLET, 'parent_id' => Yii::$app->user->identity->id]);
						$outlets = ArrayHelper::map($outlets, 'id', 'name');
						echo $form->field($model, 'outlet_id')->dropdownList($outlets, ["prompt" => "Select Outlet"]);
					}
				?>


				<?= $form->field($model, 'chat_summary')->textarea(['rows' => 6]); ?>

		    <div class="form-group">
		        <?= Html::submitButton($model->isNewRecord ? 'Update' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		    </div>

		    <?php ActiveForm::end(); ?>

			</div>

		</div>

		<div class="col-md-4">

			<div class="main-box clearfix">
				<header class="main-box-header clearfix">
					<h2 class="pull-left">Lead Action</h2>
				</header>

				<div class="main-box-body clearfix">
					<?php if($satus!=='pending'){ ?>
					<a href="<?= Yii::$app->homeUrl;?>leads/pending?id=<?=$chat->id;?>"><button type="button" class="btn btn-warning btn-block">Mark Pending</button></a><br>
					<?php } ?>

					<?php if($satus!=='approved'){ ?>
					<a href="<?= Yii::$app->homeUrl;?>leads/approved?id=<?=$chat->id;?>"><button type="button" class="btn btn-success btn-block">Mark Approved</button></a><br>
					<?php } ?>

					<?php if($satus!=='rejected'){ ?>
					<a href="<?= Yii::$app->homeUrl;?>leads/rejected?id=<?=$chat->id;?>"><button type="button" class="btn btn-danger btn-block">Mark Rejected</button></a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>



   </div>

			</div>

		</div>
	</div>
</div>

<?php
	$this->registerCss("
		.help-block{
			color: red;
		}
	");
?>
