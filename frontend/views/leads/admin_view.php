<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;
use app\models\Leads;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = "View Leads";

?>

		<div class="ks-page-header">
            <section class="ks-title">
                <h3>Leads</h3>

                <button class="btn btn-primary-outline ks-light ks-activity-block-toggle" data-block-toggle=".ks-activity-dashboard > .ks-activity">Activity</button>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-dashboard-tabbed-sidebar">
                    <div class="ks-dashboard-tabbed-sidebar-widgets">

						<div class="chatwind col-md-12">

						 <div class="row">
							<div class="col-xl-4">
								<div class="ks-dashboard-widget ks-widget-amount-statistics ks-info">
									<div class="ks-statistics">
										<span class="ks-text">Chat Reference #</span>
										<span class="ks-amount"><?= $chat->id;?></span>
									</div>

								</div>
							</div>
							<div class="col-xl-4">
								<div class="ks-dashboard-widget ks-widget-amount-statistics ks-success">
									<div class="ks-statistics">
										<span class="ks-text">Duration of chat</span>
										<span class="ks-amount"><?= $chat->duration;?> Seconds</span>
									</div>

								</div>
							</div>

							 <div class="col-xl-4">
								<div class="ks-dashboard-widget ks-widget-amount-statistics ks-purple">
									<div class="ks-statistics">
										<span class="ks-text">Visitor Frequency</span>
										<span class="ks-amount">0</span>
									</div>

								</div>
							</div>

						</div>

						 <div class="row">
							<div class="col-xl-6">
							<?php
								$satus = Leads::getStatus($chat->id);
								if($satus=='pending'):
									$class="warning";
								elseif($satus=='approved'):
									$class="success";
								else:
									$class="danger";
								endif
							?>
										<div class="ks-dashboard-widget ks-widget-amount-statistics ks-<?php echo $class;?>">
											<div class="ks-statistics">
												<span class="ks-text">
													Lead Status
												</span>
												<span class="ks-amount"><?php echo $satus = Leads::getStatus($chat->id);?></span>
											</div>

										</div>
										<br>

								 <div id="ks-bar-chart-panel" class="card panel">
									<h5 class="card-header">
										INFORMATION
									</h5>
									<div class="card-block">

										<fieldset class="form-group">
												<label class="form-control-label">Geolocation: <label>
												<?php echo $chat->visitor->city . ", " . $chat->visitor->region . ", " . $chat->visitor->country;?>
										</fieldset>

										<?php
											$leadModel = Leads::findOne(['chat_id' => $chat->id]);
											$visitor_name = isset($chat->visitor->name) ? $chat->visitor->name : NULL;
											$visitor_email = isset($chat->visitor->email) ? $chat->visitor->email : NULL;
											$visitor_phone = 'Not provided';
											if($leadModel){
												if($visitor_name == NULL){
													$visitor_name = $leadModel->visitor_name;
												}
												if($visitor_email == NULL){
													$visitor_email = $leadModel->visitor_email;
												}
												if($leadModel->visitor_phone != NULL){
													$visitor_phone = $leadModel->visitor_phone;
												}
											}

											if($visitor_name == NULL){
												$visitor_name = 'Not provided';
											}

											if($visitor_email == NULL){
												$visitor_email = 'Not provided';
											}

										?>

										<!-- <fieldset class="form-group">
												<label class="form-control-label">CONTACT NAME: <label>
                          <?php // echo $chat->visitor->name;?>
													<?=$visitor_name?>
										</fieldset>

										<fieldset class="form-group">
												<label class="form-control-label">EMAIL ADDRESS: <label>
                          <?php // echo $chat->visitor->email; ?>
													<?=$visitor_email?>
										</fieldset>

										<fieldset class="form-group">
												<label class="form-control-label">Phone: <label>
													<?=$visitor_phone?>
										</fieldset> -->

										<?php $form = ActiveForm::begin(['action' => Url::to(['leads/admin-update', 'id' => $model->chat_id])]); ?>

								    <?= $form->field($model, 'visitor_name')->textInput(['maxlength' => true]); ?>

								    <?= $form->field($model, 'visitor_email')->textInput(['maxlength' => true]); ?>

										<?= $form->field($model, 'visitor_phone')->textInput(['maxlength' => true]); ?>

										<?= $form->field($model, 'category_id')->dropdownList($categories, ["prompt" => "Select Category", 'class' => 'select form-control']); ?>
										<?php
											if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE){
												$outlets = User::findAll(['role' => User::ROLE_OUTLET, 'parent_id' => Yii::$app->user->identity->id]);
												$outlets = ArrayHelper::map($outlets, 'id', 'name');
												echo $form->field($model, 'outlet_id')->dropdownList($outlets, ["prompt" => "Select Outlet", 'class' => 'select form-control']);
											}
										?>


										<?= $form->field($model, 'chat_summary')->textarea(['rows' => 6]); ?>

								    <?php if(isset($model->custom_fields)){
											$showbutton = "display:none";
										}else{
											$showbutton = "";
										}
                    ?>
										<!--Hidden Submit --->
										<div style="<?=$showbutton?>" class="form-group">
										    <?= Html::submitButton($model->isNewRecord ? 'Update' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary save_lead']) ?>
										</div>

								    <?php ActiveForm::end(); ?>
                                     <?php if(isset($model->custom_fields)){ ?>
											<div class="custom_input_fields">
								      </div>
											<button class='btn submit-btn btn-primary'>Update</button>
										<?php }?>
									</div>
								</div>

							</div>
							<div class="col-xl-6">
								<div id="ks-bar-chart-panel" class="card panel">
									<h5 class="card-header">
										LEAD ACTIONS
									</h5>
									<div class="card-block">
										<!--<fieldset class="form-group">-->
										<!--	<a href="<?php echo Url::to(['/feedback/add']);?>">-->
										<!--		<input type="button" class="btn btn-primary btn-lg btn-block" name="add_feedback" value="Give Feedback">-->
										<!--	</a>-->
										<!--</fieldset>-->

										<!--<fieldset class="form-group">-->
										<!--	<a href="<?php echo Url::to(['/faq/add']);?>">-->
										<!--		<input type="button" class="btn btn-primary-outline btn-lg btn-block" name="add_faq" value="Add a FAQ">-->
										<!--	</a>-->
										<!--</fieldset>-->

										<?php if($satus!=='pending'){ ?>
											<fieldset class="form-group">
												<a href="<?php echo Url::to(['/leads/pending', 'id' => $chat->id]);?>">
													<input type="button" class="btn btn-warning btn-lg btn-block" name="add_faq" value="Mark Pending">
												</a>
											</fieldset>
										<?php }?>

										<?php if($satus!=='approved'){ ?>
											<fieldset class="form-group">
												<a href="<?php echo Url::to(['/leads/approved', 'id' => $chat->id]);?>">
													<input type="button" class="btn btn-success btn-lg btn-block" name="add_faq" value="Complete">
												</a>
											</fieldset>
										<?php }?>
										<?php if($satus!=='rejected'){ ?>
											<fieldset class="form-group">
												<a href="<?php echo Url::to(['/leads/rejected', 'id' => $chat->id]);?>">
													<input type="button" class="btn btn-danger btn-lg btn-block" name="add_faq" value="In progress">
												</a>
											</fieldset>
										<?php }?>
									</div>
								</div>

							</div>
						</div>
	<br><br>
    <div class="panel panel-default">
		<div class="panel-heading">
			<i class="fa fa-comments fa-fw"></i>
			Prechat Survey
		</div>
        <!-- /.panel-heading -->
		<div class="panel-body">

				<?php if(isset($chat2->prechat_survey)&& !empty($chat2->prechat_survey)){
				foreach($chat2->prechat_survey as $prechat_survey){ ?>

					<p><b>Question : </b><?= $prechat_survey->key;?></p>
					<p><b>Answer : </b><?= $prechat_survey->value;?></p>

				<?php } }else{
				?>

					<p>Survey Not found</p>

				<?php
}				// end foreach?>

		</div>
		<!-- /.panel-body -->
	</div>
<br>
    <div class="panel panel-default">
		<div class="panel-heading">
			<i class="fa fa-comments fa-fw"></i>
			Postchat Survey
		</div>
        <!-- /.panel-heading -->
		<div class="panel-body">

				<?php if(isset($chat2->postchat_survey) && !empty($chat2->postchat_survey)){
				foreach($chat2->postchat_survey as $postchat_survey){ ?>

					<p><b>Question : </b><?= $postchat_survey->key;?></p>
					<p><b>Answer : </b><?= $postchat_survey->value;?></p>


				<?php }
                ?>
				<p>&nbsp</p>
				<p><b>Rating : </b><?php
									 if($chat2->rate == 'rated_bad'){
										echo '<span class="text-danger fa-stack"><i class="fa fa-thumbs-down fa-stack-2x"></i></span>';
									} elseif($chat2->rate == 'rated_good'){
										echo '<span class="text-success fa-stack"><i class="fa fa-thumbs-up fa-stack-2x"></i></span>';
									} else{
										echo '--';
									};
								?></p>
				<?php
				}else{
				?>

					<p>Survey Not found</p>

				<?php
}		 // end foreach?>

		</div>
		<!-- /.panel-body -->
	</div>

</div>
<br>
<div class="chatwind col-md-12">

    <div class="chat-panel panel panel-default">
		<div class="panel-heading">
			<i class="fa fa-comments fa-fw"></i>
			Chat Transcript
		</div>
        <!-- /.panel-heading -->
		<div class="panel-body">
			<table class="table table-bordered table-hover">
				<?php if(isset($chat->messages)){ ?>
				<?php foreach($chat->messages as $messages){ ?>
				<?php if($messages->user_type !== 'visitor') { ?>
				   <tr class="table-warning">
						<td>
							<img src="http://oxnia.com/externalassets/agent-icon.png" alt="User Avatar" class="img-circle">
						</td>
						<td width="20%">
							<strong class="primary-font"><?= $messages->author_name;?></strong><br>
							<small class="text-muted">
								<i class="fa fa-clock-o fa-fw"></i><?= $messages->date;?>
							</small>
						</td>
						<td>
							<?= $messages->text;?>
						</td>

				    </tr>
				<?php } else{ ?>
				    <tr>
						<td>
								<img src="http://oxnia.com/externalassets/customer-icon.png" alt="User Avatar" class="img-circle">
						</td>
						<td width="20%">
								<strong class="primary-font"><?= $messages->author_name;?> (Visitor)</strong><br>
								<small class=" text-muted">
									<i class="fa fa-clock-o fa-fw"></i> <?= $messages->date;?>
								</small>

						</td>
						<td>
							<?= $messages->text;?>
						</td>

				   </tr>
				<?php } //end if ?>
				<?php } // end foreach?>
				<?php } // end if?>
			</table>

		</div>
		<!-- /.panel-body -->
	</div>

</div>


                    </div>

                </div>
            </div>
        </div>

<?php
    $this->registerCss("
	label
{
    text-transform:capitalize;
}
");
	$this->registerJs("
	    $('#w0').on('keyup keypress', function(e) {
		var keyCode = e.keyCode || e.which;
		if (keyCode === 13) {
		e.preventDefault();
		return false;
		}
		});
		$('.select').select2();
		var form_custom_fields = [];
		function validate_form_custom_fields()
		{
			//console.log('Starting validation');
			//console.log(form_custom_fields);
			if(form_custom_fields.length > 0)
			{
				var error = false;
				for(var i=0; i<form_custom_fields.length; i++)
				{
						var obj = form_custom_fields[i];
						if(obj.required == 1)
						{
							//If not empty && not null && not undefined
							if(!$('#'+obj.field).val() || $('#'+obj.field).val() == '')
							{
								error = true;
								swal('Validatation Error','\"'+obj.label+'\" field is required','error');
								break;
							}
						}
				}
				if(error == true) {
					return false;
				}else {
					return true;
				}
			}else {
				//no fields, no validations
				return true;
			}
		}
		$('.submit-btn').off('click').on('click', function(e){
			e.preventDefault();
			var valid_custom_fields = validate_form_custom_fields();
			if(valid_custom_fields == false)
			{
				return false;
			}
			var custom_filled_data = [];
			if($('.cust_input_elem').length > 0)
			{
				$('.cust_input_elem').each(function(){
					var custom_field_id = $(this).data('cust-id');
					var custom_field_val = $(this).val();
					var custom_field_required = $(this).data('cust-required');
					var custom_field_type = $(this).data('cust-field-type');

					//console.log(custom_field_id+ '    ' + custom_field_val);
					custom_filled_data.push({'field_id':custom_field_id,'field_required':custom_field_required,'field_type':custom_field_type,'field_value':encodeURIComponent(custom_field_val)});
				});
			}
			//console.log(custom_filled_data);
			var chat_id = '$model->chat_id';
			$.ajax({
				type: 'POST',
				url:'".Url::to(['lead-list/save-custom-fields'])."',
				data:{custom_filled_data:custom_filled_data,chat_id:chat_id},
				success: function(response){
					response = JSON.parse(response);
					if(response.success == true){
					$('.save_lead').submit();
                //swal(response.message);
					}else{
               swal(response.message);
					}
				},error: function(data){
					swal(data);
				}
			});
		});
		$.ajax({
			type: 'POST',
			url: '".Url::to(['leads/update-agent-app','chat_id'=>$model->chat_id,'group_id'=>$model->user_group])."',
			success: function(response){
				response = JSON.parse(response);
	      var custom_input_elements = response.custom_input_elements;
	      form_custom_fields = [];
        for (var cust_el in custom_input_elements) {
					var el = custom_input_elements[cust_el];
					if(el.type == 'textbox')
					{
						var tx_box_html = '<div class=\"form-group\"><label class=\"control-label\">'+el.label+':</label><input type=\"text\" class=\"form-control cust_input_elem cust_input_textbox\" name=\"custom_lead_field_'+cust_el+'\" id=\"custom_lead_field_'+cust_el+'\" data-cust-id=\"'+cust_el+'\" data-cust-required='+el.required+' data-cust-field-type=\"textbox\"/></div>';

						$('.custom_input_fields').append(tx_box_html);
					}else if(el.type == 'dropdown')
					{
						var dp_down_html = '<div class=\"form-group\"><label class=\"control-label\">'+el.label+':</label><select class=\"form-control select-input  cust_input_elem cust_input_dropdown\" name=\"custom_lead_field_'+cust_el+'\" id=\"custom_lead_field_'+cust_el+'\" data-cust-id=\"'+cust_el+'\" data-cust-required='+el.required+' data-cust-field-type=\"dropdown\"><option value=\"\">Select</option>';

						for(var opt in el.options)
						{
							dp_down_html += '<option value=\"'+el.options[opt].option_id+'\">'+el.options[opt].text+'</option>';
						}

						dp_down_html += '</select></div>';

						$('.custom_input_fields').append(dp_down_html);
						//console.log('INNI DROP');
					}else if(el.type == 'multiple_choice')
					{
						var dp_down_html = '<div class=\"form-group\"><label class=\"control-label\">'+el.label+':</label><select class=\"form-control select-input  cust_input_elem  cust_input_multiple_choice\" multiple name=\"custom_lead_field_'+cust_el+'\"  id=\"custom_lead_field_'+cust_el+'\" data-cust-id=\"'+cust_el+'\" data-cust-required='+el.required+'  data-cust-field-type=\"multiple_choice\"><option value=\"\">Select</option>';

						for(var opt in el.options)
						{
							dp_down_html += '<option value=\"'+el.options[opt].option_id+'\">'+el.options[opt].text+'</option>';
						}

						dp_down_html += '</select></div>';

						$('.custom_input_fields').append(dp_down_html);
						//console.log('INNI MULTI');
					}
					form_custom_fields.push({'type':el.type,'label':el.label,'required':el.required,'field':'custom_lead_field_'+cust_el});
				}
				$('.select-input').select2();
        var custom_fields_response = JSON.parse(response.custom_fields_response);
				if(custom_fields_response && custom_fields_response.length > 0)
				{
					for(var j=0;j<custom_fields_response.length;j++)
					{
						//console.log('IN LOOP: '+custom_fields_response[j].field_id+'   '+custom_fields_response[j].field_value);
						if(custom_fields_response[j].field_type == 'textbox')
						{
								$('#custom_lead_field_'+custom_fields_response[j].field_id).val(decodeURIComponent(custom_fields_response[j].field_value));
						}
						else if(custom_fields_response[j].field_type == 'dropdown')
						{
								$('#custom_lead_field_'+custom_fields_response[j].field_id).val(custom_fields_response[j].field_value);
								$('#custom_lead_field_'+custom_fields_response[j].field_id).trigger('change');
						}
						else if(custom_fields_response[j].field_type == 'multiple_choice')
						{

							//console.log(chatAgentData.custom_fields[j].field_value);
							if(custom_fields_response[j].field_value && custom_fields_response[j].field_value != '')
							{
								//var multi_ch_data = custom_fields_response[j].field_value;
								var multi_ch_data = decodeURIComponent(custom_fields_response[j].field_value);
								for( var i = 0; i < multi_ch_data.length-1; i++){
									 if ( multi_ch_data[i] === '') {
										 multi_ch_data.splice(i, 1);
									 }
								}
								//alert(multi_ch_data);
								//alert('#custom_lead_field_'+custom_fields_response[j].field_id)
								//alert(chatAgentData.custom_fields[j].field_value+' == '+multi_ch_data);

								multi_ch_data = JSON.parse(\"[\" + multi_ch_data + \"]\");

								$('#custom_lead_field_'+custom_fields_response[j].field_id).val(multi_ch_data);
								$('#custom_lead_field_'+custom_fields_response[j].field_id).trigger('change');

							}

						}
					}
				}
			},
			error: function(data){
	      console.log(data);
	    },
		});
	");
?>
