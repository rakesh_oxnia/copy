<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;
use common\widgets\Menu1;
use common\widgets\Menu2;
use common\widgets\Menu3;
use common\widgets\FranchiseMenu;
use common\widgets\CustomMenu;
use frontend\assets\KosmoAsset;
use app\models\Leads;
use common\models\LeadSeen;

KosmoAsset::register($this);
$email = Yii::$app->user->identity->email;
$userData = User::find()->where(['email' => $email])->all();
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">

<!-- BEGIN HEAD -->
<head>
    <meta charset="UTF-8">
    <title>Chat Metrics</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php $this->head() ?>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
     <link rel="shortcut icon" href="https://chatmetrics.com/wp-content/uploads/2018/04/CM-new-favicon.png">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/fonts/line-awesome/css/line-awesome.min.css">
    <!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/fonts/montserrat/styles.css">

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/jscrollpane/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/common.min.css">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/themes/primary.min.css">
    <!-- <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/themes/sidebar-black.min.css"> -->
    <!-- END THEME STYLES -->

<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/datatables-net/media/css/dataTables.bootstrap4.min.css"> <!-- original -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/datatables-net/extensions/responsive/css/responsive.bootstrap4.min.css"> <!-- original -->
<!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/<?php echo Yii::$app->request->baseUrl;?>/libs/datatables-net/datatables.min.css">  --><!-- customization -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/select2/css/select2.min.css"> <!-- Original -->
<!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/<?php echo Yii::$app->request->baseUrl;?>/libs/select2/select2.min.css"> --> <!-- Customization -->
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
<!-- <link href="<?php echo Yii::$app->request->baseUrl;?>/cube/css/<?php echo Yii::$app->request->baseUrl;?>/libs/font-awesome.css" rel="stylesheet"> -->

<?php echo Html::csrfMetaTags(); ?>

<style>
.alert.alert-success {
    border-color: #c0f0d4;
    font-size: 18px;
    color: green;
    margin: 10px;
}
</style>
<!-- Start of Chatcode -->
<script type="text/javascript">
var __lc = {};
__lc.license = 6354551;
window.__lc.ga_version
window.__lc.chat_between_groups = false;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of Chatcode -->

</head>
<!-- END HEAD -->
<?php $this->beginBody() ?>
<body class="ks-navbar-fixed ks-sidebar-default ks-sidebar-position-fixed ks-page-header-fixed ks-theme-primary ks-page-loading">
<!-- <body class="ks-navbar-fixed ks-sidebar-default ks-sidebar-position-fixed ks-theme-primary ks-page-loading"> -->
    <!-- remove ks-page-header-fixed to unfix header -->

    <!-- BEGIN HEADER -->
<nav class="navbar ks-navbar">
    <!-- BEGIN HEADER INNER -->
    <!-- BEGIN LOGO -->
    <div href="index.html" class="navbar-brand">
        <!-- BEGIN RESPONSIVE SIDEBAR TOGGLER -->
        <a href="#" class="ks-sidebar-toggle"><i class="ks-icon la la-bars" aria-hidden="true"></i></a>
        <a href="#" class="ks-sidebar-mobile-toggle"><i class="ks-icon la la-bars" aria-hidden="true"></i></a>
        <!-- END RESPONSIVE SIDEBAR TOGGLER -->

        <div class="ks-navbar-logo">
            <!-- <a href="" class="ks-logo">Chat Metrics</a> -->
            <a href="<?php echo Url::to(['/site/index']);?>" >
                <img class="cm-logo" src="images/CM Logo - White.png">
            </a>

            <!-- END GRID NAVIGATION -->
        </div>
    </div>
    <!-- END LOGO -->

    <!-- BEGIN MENUS -->
    <div class="ks-wrapper">
        <nav class="nav navbar-nav">
            <!-- BEGIN NAVBAR MENU -->
            <div class="ks-navbar-menu">
                <!--<form class="ks-search-form">
                    <a class="ks-search-open" href="#">
                        <span class="la la-search"></span>
                    </a>
                    <div class="ks-wrapper">
                        <div class="input-icon icon-right icon icon-lg icon-color-primary">
                            <input id="input-group-icon-text" type="text" class="form-control" placeholder="Search...">
                            <span class="icon-addon">
                                <span class="la la-search ks-icon"></span>
                            </span>
                        </div>
                        <a class="ks-search-close" href="#">
                            <span class="la la-close"></span>
                        </a>
                    </div>
                </form>-->
                <a class="nav-item nav-link" href="<?php echo Url::to(['/site/index']);?>">Dashboard</a>

                <?php if(sizeOf( $userData ) > 1){?>
                    <div class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            Websites
                        </a>
                        <div class="dropdown-menu ks-info" aria-labelledby="Preview">
                            <?php foreach($userData as $key => $value){?>
                                <a class="dropdown-item <?php if( $value['website_url'] == Yii::$app->user->identity->website_url ){?>ks-active<?php }?>" href="<?php echo Url::to(['/site/switch', 'id' => $value['id']]);?>"><?php echo $value['website_url'];?></a>
                            <?php }?>

                        </div>
                    </div>
                <?php }?>

            </div>
            <!-- END NAVBAR MENU -->

            <!-- BEGIN NAVBAR ACTIONS -->
            <div class="ks-navbar-actions">




                <!-- BEGIN NAVBAR NOTIFICATIONS -->
                <!--<div class="nav-item dropdown ks-notifications">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="la la-bell ks-icon" aria-hidden="true">
                            <span class="badge badge-pill badge-info">7</span>
                        </span>
                        <span class="ks-text">Notifications</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Preview">
                        <ul class="nav nav-tabs ks-nav-tabs ks-info" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#" data-toggle="tab" data-target="#navbar-notifications-all" role="tab">All</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="tab" data-target="#navbar-notifications-activity" role="tab">Activity</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="tab" data-target="#navbar-notifications-comments" role="tab">Comments</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane ks-notifications-tab active" id="navbar-notifications-all" role="tabpanel">
                                <div class="ks-wrapper ks-scrollable">
                                    <a href="#" class="ks-notification">
                                        <div class="ks-avatar">
                                            <img src="<?php echo Yii::$app->request->baseUrl;?>/kosmo<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/img/avatars/avatar-3.jpg" width="36" height="36">
                                        </div>
                                        <div class="ks-info">
                                            <div class="ks-user-name">Emily Carter <span class="ks-description">has uploaded 1 file</span></div>
                                            <div class="ks-text"><span class="la la-file-text-o ks-icon"></span> logo vector doc</div>
                                            <div class="ks-datetime">1 minute ago</div>
                                        </div>
                                    </a>
                                    <a href="#" class="ks-notification">
                                        <div class="ks-action">
                                            <span class="ks-default">
                                                <span class="la la-briefcase ks-icon"></span>
                                            </span>
                                        </div>
                                        <div class="ks-info">
                                            <div class="ks-user-name">New project created</div>
                                            <div class="ks-text">Dashboard UI</div>
                                            <div class="ks-datetime">1 minute ago</div>
                                        </div>
                                    </a>
                                    <a href="#" class="ks-notification">
                                        <div class="ks-action">
                                            <span class="ks-error">
                                                <span class="la la-times-circle ks-icon"></span>
                                            </span>
                                        </div>
                                        <div class="ks-info">
                                            <div class="ks-user-name">File upload error</div>
                                            <div class="ks-text"><span class="la la-file-text-o ks-icon"></span> logo vector doc</div>
                                            <div class="ks-datetime">10 minutes ago</div>
                                        </div>
                                    </a>
                                </div>

                                <div class="ks-view-all">
                                    <a href="#">Show more</a>
                                </div>
                            </div>
                            <div class="tab-pane ks-empty" id="navbar-notifications-activity" role="tabpanel">
                                There are no activities.
                            </div>
                            <div class="tab-pane ks-empty" id="navbar-notifications-comments" role="tabpanel">
                                There are no comments.
                            </div>
                        </div>
                    </div>
                </div>-->
                <!-- END NAVBAR NOTIFICATIONS -->

                <?php
                  if(Yii::$app->user->identity->role == User::ROLE_OUTLET){
                    $newLeads = Leads::findAll(['outlet_id' => Yii::$app->user->identity->id, 'user_group' => Yii::$app->user->identity->user_group, 'new_lead' => 1]);
                  }else{
                    $newLeads = Leads::findAll(['user_group' => Yii::$app->user->identity->user_group, 'new_lead' => 1]);
                  }
                  $unseen_count = 0;
                  //echo "<pre>"; print_r($newLeads); exit;
                  foreach ($newLeads as $lead) {
                    $seenLead = LeadSeen::findOne(['leads_id' => $lead->id, 'user_id' => Yii::$app->user->identity->id]);
                    if(!$seenLead){
                      //echo "<pre>"; print_r($seenLead); exit;
                      $unseen_count++;
                    }
                  }
                ?>

                <div class="nav-item ks-notifications">
                    <!-- <a class="nav-link dropdown-toggle unseen-link" href="<?=Url::to(['leads/mark-lead-seen'])?>" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="la la-envelope ks-icon" aria-hidden="true">
                            <span class="badge badge-pill messagebox_bage"><?=$unseen_count?></span>
                        </span>
                        <span class="ks-text">Leads</span>
                    </a> -->
                </div>

                        <div class="nav-item nav-link btn-action-block">
                    <!--<a class="btn btn-danger" href="<?php echo Url::to(['/site/launch']);?>">-->
                    <!--    <span class="ks-action header_helpbox">HELP</span>-->
                    <!--    <span class="ks-description">always here</span>-->
                    <!--</a>-->
                </div>

                <!-- BEGIN NAVBAR USER -->
                <div class="nav-item dropdown ks-user">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="ks-avatar">
                            <img src="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/img/avatars/avatar-13.jpg" width="36" height="36">
                        </span>
                        <span class="ks-info">
                            <span class="ks-name">Hi <?php echo Yii::$app->user->identity->email;?></span>
                        </span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="Preview">

                        <?php
                          $session = Yii::$app->session;
                          if(isset($session['admin_id'])){
                        ?>
                            <a class='dropdown-item' href="<?=Url::to(['user-category/switch-to-admin'])?>" >
                                <span class='la la-user ks-icon'></span>
                                <span><?php echo "Access Dashboard <br> " . $session['admin_email']. '';?></span>
                            </a>
                        <?php
                          }
                        ?>

                        <a class="dropdown-item" href="#">
                            <span class="la la-user ks-icon"></span>
                            <span><?php echo Yii::$app->user->identity->email;?></span>
                        </a>
                        <a class="dropdown-item" href="<?php echo Url::to(['/settings/index']);?>">
                            <span class="la la-wrench ks-icon" aria-hidden="true"></span>
                            <span>Settings</span>
                        </a>

                        <a class="dropdown-item" href="<?php echo Url::to(['/site/logout']);?>">
                            <span class="la la-sign-out ks-icon" aria-hidden="true"></span>
                            <span>Logout</span>
                        </a>
                    </div>
                </div>
                <!-- END NAVBAR USER -->
            </div>
            <!-- END NAVBAR ACTIONS -->
        </nav>

        <!-- BEGIN NAVBAR ACTIONS TOGGLER -->
        <nav class="nav navbar-nav ks-navbar-actions-toggle">
            <a class="nav-item nav-link" href="#">
                <span class="la la-ellipsis-h ks-icon ks-open"></span>
                <span class="la la-close ks-icon ks-close"></span>
            </a>
        </nav>
        <!-- END NAVBAR ACTIONS TOGGLER -->

        <!-- BEGIN NAVBAR MENU TOGGLER -->
        <nav class="nav navbar-nav ks-navbar-menu-toggle">
            <a class="nav-item nav-link" href="#">
                <span class="la la-th ks-icon ks-open"></span>
                <span class="la la-close ks-icon ks-close"></span>
            </a>
        </nav>
        <!-- END NAVBAR MENU TOGGLER -->
    </div>
    <!-- END MENUS -->
    <!-- END HEADER INNER -->
</nav>
<!-- END HEADER -->






<div class="ks-page-container ks-dashboard-tabbed-sidebar-fixed-tabs">

    <!-- BEGIN DEFAULT SIDEBAR -->
<div class="ks-column ks-sidebar ks-info">
    <div class="ks-wrapper ks-sidebar-wrapper">
        <?php
            //echo Yii::$app->user->identity->id;
            if(Yii::$app->user->identity->role == 50)
                echo Menu1::widget();
            elseif(Yii::$app->user->identity->role == 30)
                echo Menu3::widget();
            elseif(Yii::$app->user->identity->role == 10)
                echo Menu2::widget();
            elseif(Yii::$app->user->identity->role == 40)
                    echo FranchiseMenu::widget();
            elseif(Yii::$app->user->identity->role == 20)
                    echo Menu2::widget();
          //  elseif(Yii::$app->user->identity->role == 60)
          //        echo ServiceMenu::widget();
            else
                echo CustomMenu::widget();

      // edited by ravindra
      // if(Yii::$app->user->identity->role == 50)
            //  echo Menu1::widget();
            // if(Yii::$app->user->identity->role == 30)
            //  echo Menu2::widget();
            // if(Yii::$app->user->identity->role == 10)
            //  echo Menu3::widget();
        ?>


    </div>
</div>
<!-- END DEFAULT SIDEBAR -->


    <div class="ks-column ks-page">
        <?php echo $content;?>
    </div>
</div>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/jquery/jquery.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/responsejs/response.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/loading-overlay/loadingoverlay.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/tether/js/tether.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/jscrollpane/jquery.jscrollpane.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/jscrollpane/jquery.mousewheel.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/flexibility/flexibility.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/noty/noty.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/velocity/velocity.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/scripts/common.min.js?v=1"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/datatables-net/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/datatables-net/media/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/datatables-net/extensions/responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/datatables-net/extensions/responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/select2/js/select2.min.js"></script>
<script type="application/javascript">
(function ($) {
    $(document).ready(function() {

        jQuery(function($) {
            $("#w1").submit(function(event) {
                //alert("zxx");
                 $form = $(this);
                 Stripe.card.createToken($form, function(status, response){
                    var token = response.id;
                    //alert(token);
                    $form.append($("<input type=\"hidden\" name=\"stripeToken\" id=\"stripeToken\" />").val(token));
                    $form.get(0).submit();
                 });
            });
        });

        var table = $('#ks-datatable').DataTable({
            responsive: false,
             "aaSorting": [],
            "initComplete": function () {
                $('.dataTables_wrapper select').select2({
                    minimumResultsForSearch: Infinity
                });
            }
        });
    });
})(jQuery);
</script>

<div class="ks-mobile-overlay"></div>

<!-- BEGIN SETTINGS BLOCK -->
<div class="ks-settings-slide-block">
    <a class="ks-settings-slide-control">
        <span class="ks-icon la la-cog"></span>
    </a>



    <ul class="ks-settings-list">
        <li>
            <span class="ks-text">Collapsed Sidebar</span>
            <label class="ks-checkbox-slider ks-on-off ks-primary ks-sidebar-checkbox-toggle">
                <input type="checkbox" value="1" checked>
                <span class="ks-indicator"></span>
                <span class="ks-on">On</span>
                <span class="ks-off">Off</span>
            </label>
        </li>
        <li>
            <span class="ks-text">Fixed page header</span>
            <label class="ks-checkbox-slider ks-on-off ks-primary ks-page-header-checkbox-toggle">
                <input type="checkbox" value="0" checked>
                <span class="ks-indicator"></span>
                <span class="ks-on">On</span>
                <span class="ks-off">Off</span>
            </label>
        </li>
        <li>
            <span class="ks-text">Dark/Light Sidebar</span>
            <label class="ks-checkbox-slider ks-on-off ks-primary ks-sidebar-style-checkbox-toggle">
                <input type="checkbox" value="0">
                <span class="ks-indicator"></span>
                <span class="ks-on">On</span>
                <span class="ks-off">Off</span>
            </label>
        </li>
        <li>
            <span class="ks-text">White/Gray Content Background</span>
            <label class="ks-checkbox-slider ks-on-off ks-primary ks-content-bg-checkbox-toggle">
                <input type="checkbox" value="0" checked>
                <span class="ks-indicator"></span>
                <span class="ks-on">On</span>
                <span class="ks-off">Off</span>
            </label>
        </li>
    </ul>
</div>

<?php
  $this->registerCss("
    /*a.unseen-link:hover{
      cursor: pointer;
    }*/

    button:hover{
        cursor: pointer;
    }

    .messagebox_bage{
      background-color:#F53432 !important;
    }

    .cm-logo{
        height: 50px;
    }

    li.nav-item:hover{
        background: #00BAE1 !important;
        //background: #00b5db !important;
    }

  ");

  $this->registerJs("
    $(function(){

        $('body').removeClass('ks-sidebar-compact');
        //$('body').removeClass('ks-page-header-fixed');
    });
");

?>

<!-- END SETTINGS BLOCK -->
</body>
<?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>
