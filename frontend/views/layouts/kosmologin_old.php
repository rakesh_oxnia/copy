<?php
use common\models\User;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Oxnia</title>

    <meta http-equiv="X-UA-Compatible" content=="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/fonts/line-awesome/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/fonts/open-sans/styles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/common.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/pages/auth.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/css/style.css">
<!-- Start of Chatcode -->
<script type="text/javascript">
var __lc = {};
__lc.license = 6354551;
window.__lc.ga_version
window.__lc.chat_between_groups = false;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>

<style>

body{
  background: #fff;
}

.lbl-login{
  color:#00b5db;
  font-weight: 600;
  margin-top: 6%;
}
.wrong-pwd{
  background: #00b9de;
  color:white;
  font-weight:500;
  padding: 4%;
  border-radius:10px;
  margin-bottom: 9%;

}

.ks-logo{
margin: 0 auto;
//padding-top: 7%;
}
.ks-page .ks-page-content .ks-logo{
  margin-bottom: 0px;
}
.ks-header{
  color: #999b9c;
font-size: 30px;
font-family: OpenSans;
}
.ks-page .ks-page-content .ks-panel{
  background-color:none !important;
  border:none !important;
}
.ks-page .ks-page-content .ks-panel .form-control {
      //border-color: rgba(57,81,155,.2);
    border-color:  #00bae1;
    border-radius:9px;
    border:2px solid #00bae173;
    background: #f1f9ff;
    color: #00b5db;
}
.la-check:before {
    content: "\f17b";
    color: #00b5db;
    color: #00b5db;
    font-weight: 700;
}
.la-close:before {

    color: #00b5db;
    color: #00b5db;
    font-weight: 700;
}
.remember_label{
  align-content: center;
      margin-left: 7%;
      font-weight: 700;;
        color: #00b5db;
}
.ks-checkbox-slider{
  height: 30px;
  //width: 80px;
  border-radius: 7px;
  background-color: #b5ecf6b8;
  border:none !important;
  margin-top: 5%;
    margin-bottom: 12%;
}
.ks-checkbox-slider>.ks-indicator{
  width: 18px;
    height: 24px;
    border-radius:5px;
    background: white !important;
}
.ks-page .ks-page-content .ks-panel .form-control:focus{
  border:2px solid #00bae173;
  box-shadow: 1px 1px 1px 1px #00bae173;
}
.input-icon.icon-lg.icon-left .icon-addon {
    //padding-left: 264px;
}
.la-at:before {
    content: "\f128";
}
.loginbtn , .loginbtn:hover{
  background-color: #ff8b46;
  color:white;
  font-weight: 700;
  border:none;
  outline:none;
  width:45%;
  text-transform: uppercase;
  border-radius: 8px;
}

.loginbtn:hover{
  cursor: pointer;
  box-shadow: 1px 1px 1px 1px #cc8b46;
}

.wrong-pwd{
  display: none;
}

.parent-container{
  margin: 0 !important;
  height: 100%;
}

.help-block{
  color: red;
  margin-left: 5px !important;
}

.iframe-content iframe{
  margin-top: 25%;
}

</style>
<!-- End of Chatcode -->
</head>
<body>

  <div class="row parent-container">
    <div class="col-md-4">
      <div class="ks-page">
        <div class="ks-page-content">
          <div class="ks-logo">
          	<img src="<?php echo Yii::$app->request->baseUrl;?>/images/cm-logo-retina.svg" width="360" height="150">
          </div>
          <div class="card panel panel-default ks-light ks-panel ks-login">
            <div class="card-block">
              <?php echo $content;?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php
      $iframeText = '';
      $adminUser = User::findOne(['role' => User::ROLE_ADMIN]);
      $iframeText = $adminUser->iframe_tag;
    ?>

    <div class="col-md-8">
      <div class="iframe-content" style="background: #000; height: 100%;">
        <center>
          <?=$iframeText?>
        </center>
      </div>
    </div>
  </div>

<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/jquery/jquery.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/tether/js/tether.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/bootstrap/js/bootstrap.min.js"></script>
<?php
  $this->registerCss("

  ");
?>
</body>
</html>
