<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\KosmoLoginAsset;
use frontend\widgets\Alert;
use common\models\User;
use yii\web\View;

/* @var $this \yii\web\View */
/* @var $content string */

KosmoLoginAsset::register($this);
$this->title = 'Chat Metrics App';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="https://chatmetrics.com/wp-content/uploads/2018/04/CM-new-favicon.png">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
  <?php $this->beginBody() ?>

    <div class="row parent-container">
      <div class="col-md-3">
        <div class="container">

              <div class="ks-page">
              <div class="ks-page-content">
                <div class="ks-logo">
                  <img src="<?php echo Yii::$app->request->baseUrl;?>/images/cm-logo-retina.svg" height="64">
                </div>
                <div class="card panel panel-default ks-light ks-panel ks-login">
                  <div class="card-block">
                    <?php echo $content;?>
                  </div>
                </div>
              </div>
            </div>

      </div>
      </div>

      <?php
        $iframeText = '';
        $color_code = '#000';
        $adminUser = User::findOne(['role' => User::ROLE_ADMIN]);
        $iframeText = $adminUser->iframe_tag;
        $color_code = $adminUser->login_bg_color;
        //$color_code = '#eee';
      ?>

      <div class="col-md-9">
        <div class="iframe-content" style="background: <?=$color_code?>; height: 100%;">
          <center>
            <?=$iframeText?>
          </center>
        </div>
      </div>
    </div>

  <?php $this->endBody() ?>

  <?php

    $this->registerJs("
       var __lc = {};
        __lc.license = 6354551;
        window.__lc.ga_version
        window.__lc.chat_between_groups = false;
        (function() {
          var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
          lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
        })();
    ", View::POS_HEAD);

    $this->registerCss("
      h4.ks-header{
        font-size: 19px;
      }
    ");

  ?>

</body>
</html>
<?php $this->endPage() ?>
