<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\DashboardnAsset;
use app\models\Managecontent;
use common\models\User;
use common\widgets\Alert;
use common\widgets\Navigation;
use common\widgets\Menu1Old;
use common\widgets\Menu2Old;
use common\widgets\Menu3Old;
use common\widgets\Menu4;
use common\widgets\Menu5;
use common\widgets\Menu6;
use app\models\Homecontent;
//use app\models\GlobalNotifications;

AppAsset::register($this);
DashboardnAsset::register($this);

$role = Yii::$app->user->identity->role;

$theme_color = 'theme-whbl';
$agencylogo = Yii::$app->homeUrl.'cube/img/logo.png';
$agencylogoblack = Yii::$app->homeUrl.'cube/img/logo-black.png';
$agencylogosmall = Yii::$app->homeUrl.'cube/img/logo-small.png';
if(User::ROLE_AGENCY == $role)
{
	$theme_color = Yii::$app->user->identity->theme_color;
	$agencylogoimg = Yii::$app->user->identity->agency_logo;
	if( $agencylogoimg != '')
	{
		$agencylogo = Yii::$app->homeUrl.'logo_images/'.$agencylogoimg;
		$agencylogoblack = Yii::$app->homeUrl.'logo_images/'.$agencylogoimg;
		$agencylogosmall = Yii::$app->homeUrl.'logo_images/'.$agencylogoimg;
	}
	
}
else if(User::ROLE_USER == $role)
{
	$parent_id = Yii::$app->user->identity->parent_id;
	if($parent_id != '')
	{
		$row = (new \yii\db\Query())
	    ->from('user')
	    ->where(['id' => $parent_id])
	    ->one();

		$theme_color = $row['theme_color'];
		$agencylogoimg = $row['agency_logo'];
		if( $agencylogoimg != '')
		{
			$agencylogo = Yii::$app->homeUrl.'logo_images/'.$agencylogoimg;
			$agencylogoblack = Yii::$app->homeUrl.'logo_images/'.$agencylogoimg;
			$agencylogosmall = Yii::$app->homeUrl.'logo_images/'.$agencylogoimg;
		}
	}
}
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
	<script>
		var SiteUrl = '<?php echo Yii::getAlias('@web');?>';
		var ImagesUrl = '<?php echo Yii::getAlias('@web');?>/images';
	</script>

</head>

<body class="pace-done <?= $theme_color;?>">
	<?php $this->beginBody() ?>

	<div id="theme-wrapper">
	<?php $c_user_id = Yii::$app->user->identity->id;?>
	
		<header class="navbar" id="header-navbar">
			<div class="container">
				<a href="<?= Yii::$app->homeUrl;?>" id="logo" class="navbar-brand">
					<img src="<?= $agencylogo;?>" alt="" class="normal-logo logo-white"/>
					<img src="<?= $agencylogoblack;?>" alt="" class="normal-logo logo-black"/>
					<img src="<?= $agencylogosmall;?>" alt="" class="small-logo hidden-xs hidden-sm hidden"/>
				</a>
				
				<div class="clearfix">
				<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="fa fa-bars"></span>
				</button>
				
				<div class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">
					<ul class="nav navbar-nav pull-left">
						<li>
							<a class="btn" id="make-small-nav">
								<i class="fa fa-bars"></i>
							</a>
						</li>
						<!--li class="dropdown hidden-xs">
							<a class="btn dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-bell"></i>
								<span class="count">8</span>
							</a>
							<ul class="dropdown-menu notifications-list">
								<li class="pointer">
									<div class="pointer-inner">
										<div class="arrow"></div>
									</div>
								</li>
								<li class="item-header">You have 6 new notifications</li>
								<li class="item">
									<a href="#">
										<i class="fa fa-comment"></i>
										<span class="content">New comment on ‘Awesome P...</span>
										<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
									</a>
								</li>
								<li class="item">
									<a href="#">
										<i class="fa fa-plus"></i>
										<span class="content">New user registration</span>
										<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
									</a>
								</li>
								<li class="item">
									<a href="#">
										<i class="fa fa-envelope"></i>
										<span class="content">New Message from George</span>
										<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
									</a>
								</li>
								<li class="item">
									<a href="#">
										<i class="fa fa-shopping-cart"></i>
										<span class="content">New purchase</span>
										<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
									</a>
								</li>
								<li class="item">
									<a href="#">
										<i class="fa fa-eye"></i>
										<span class="content">New order</span>
										<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
									</a>
								</li>
								<li class="item-footer">
									<a href="#">
										View all notifications
									</a>
								</li>
							</ul>
						</li>
						<li class="dropdown hidden-xs">
							<a class="btn dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-envelope-o"></i>
								<span class="count">16</span>
							</a>
							<ul class="dropdown-menu notifications-list messages-list">
								<li class="pointer">
									<div class="pointer-inner">
										<div class="arrow"></div>
									</div>
								</li>
								<li class="item first-item">
									<a href="#">
										<img src="img/samples/messages-photo-1.png" alt=""/>
										<span class="content">
											<span class="content-headline">
												George Clooney
											</span>
											<span class="content-text">
												Look, just because I don't be givin' no man a foot massage don't make it 
												right for Marsellus to throw...
											</span>
										</span>
										<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
									</a>
								</li>
								<li class="item">
									<a href="#">
										<img src="img/samples/messages-photo-2.png" alt=""/>
										<span class="content">
											<span class="content-headline">
												Emma Watson
											</span>
											<span class="content-text">
												Look, just because I don't be givin' no man a foot massage don't make it 
												right for Marsellus to throw...
											</span>
										</span>
										<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
									</a>
								</li>
								<li class="item">
									<a href="#">
										<img src="img/samples/messages-photo-3.png" alt=""/>
										<span class="content">
											<span class="content-headline">
												Robert Downey Jr.
											</span>
											<span class="content-text">
												Look, just because I don't be givin' no man a foot massage don't make it 
												right for Marsellus to throw...
											</span>
										</span>
										<span class="time"><i class="fa fa-clock-o"></i>13 min.</span>
									</a>
								</li>
								<li class="item-footer">
									<a href="#">
										View all messages
									</a>
								</li>
							</ul>
						</li>
						<li class="dropdown hidden-xs">
							<a class="btn dropdown-toggle" data-toggle="dropdown">
								New Item
								<i class="fa fa-caret-down"></i>
							</a>
							<ul class="dropdown-menu">
								<li class="item">
									<a href="#">
										<i class="fa fa-archive"></i> 
										New Product
									</a>
								</li>
								<li class="item">
									<a href="#">
										<i class="fa fa-shopping-cart"></i> 
										New Order
									</a>
								</li>
								<li class="item">
									<a href="#">
										<i class="fa fa-sitemap"></i> 
										New Category
									</a>
								</li>
								<li class="item">
									<a href="#">
										<i class="fa fa-file-text"></i> 
										New Page
									</a>
								</li>
							</ul>
						</li>
						<li class="dropdown hidden-xs">
							<a class="btn dropdown-toggle" data-toggle="dropdown">
								English
								<i class="fa fa-caret-down"></i>
							</a>
							<ul class="dropdown-menu">
								<li class="item">
									<a href="#">
										Spanish
									</a>
								</li>
								<li class="item">
									<a href="#">
										German
									</a>
								</li>
								<li class="item">
									<a href="#">
										Italian
									</a>
								</li>
							</ul>
						</li-->
					</ul>
				</div>
				
				<div class="nav-no-collapse pull-right" id="header-nav">
					<ul class="nav navbar-nav pull-right">
						<!--li class="mobile-search">
							<a class="btn">
								<i class="fa fa-search"></i>
							</a>
							
							<div class="drowdown-search">
								<form role="search">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Search...">
										<i class="fa fa-search nav-search-icon"></i>
									</div>
								</form>
							</div>
							
						</li-->
						<li class="dropdown profile-dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<!--img src="img/samples/scarlet-159.png" alt=""/-->
								<span class="hidden-xs"><?= Yii::$app->user->identity->username;?></span> <b class="caret"></b>
							</a>
							<ul class="dropdown-menu dropdown-menu-right">
								<!--li><a href="user-profile.html"><i class="fa fa-user"></i>Profile</a></li>
								<li><a href="#"><i class="fa fa-cog"></i>Settings</a></li>
								<li><a href="#"><i class="fa fa-envelope-o"></i>Messages</a></li-->
								<li><a href="<?= Yii::$app->homeUrl;?>user/changepassword"><i class="fa fa-user"></i>Change Password</a></li>
								<li><a href="<?= Yii::$app->homeUrl;?>site/logout"><i class="fa fa-power-off"></i>Logout</a></li>
							</ul>
						</li>
						<li class="hidden-xxs">
							<a href="<?= Yii::$app->homeUrl;?>site/logout" class="btn">
								<i class="fa fa-power-off"></i>
							</a>
						</li>
					</ul>
				</div>
				</div>
			</div>
		</header>
		<div id="page-wrapper" class="container">
			<div class="row">
				<div id="nav-col">
					<section id="col-left" class="col-left-nano">
						<div id="col-left-inner" class="col-left-nano-content">
							<div id="user-left-box" class="clearfix hidden-sm hidden-xs dropdown profile2-dropdown">
								<!--img alt="" src="img/samples/scarlet-159.png" /-->
								<div class="user-box">
									<span class="name">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">
											<?= Yii::$app->user->identity->username;?> 
											<i class="fa fa-angle-down"></i>
										</a>
										<ul class="dropdown-menu">
											<!--li><a href="user-profile.html"><i class="fa fa-user"></i>Profile</a></li>
											<li><a href="#"><i class="fa fa-cog"></i>Settings</a></li>
											<li><a href="<?= Yii::$app->homeUrl;?>faq/add"><i class="fa fa-envelope-o"></i>Faq</a></li-->
											<li><a href="<?= Yii::$app->homeUrl;?>user/changepassword"><i class="fa fa-user"></i>Change Password</a></li>
											<li><a href="<?= Yii::$app->homeUrl;?>site/logout"><i class="fa fa-power-off"></i>Logout</a></li>
										</ul>
									</span>
									<span class="status">
										<i class="fa fa-circle"></i> Online
									</span>
								</div>
							</div>
							<div class="collapse navbar-collapse navbar-ex1-collapse" id="sidebar-nav">	
								<?php
									if(Yii::$app->user->identity->role == 50)
										echo Menu1Old::widget();
									if(Yii::$app->user->identity->role == 30)
										echo Menu3Old::widget();
									if(Yii::$app->user->identity->role == 10)
										echo Menu2Old::widget();
								?>
							</div>
						</div>
					</section>
					<div id="nav-col-submenu"></div>
				</div>
				<div id="content-wrapper">
					<div class="row">
						<div class="col-sm-12"><?= Alert::widget() ?></div>
						<div class="col-sm-12"><?= $content ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
		
	<?php $this->endBody() ?>
	<script type="text/javascript">
		var active_added = false;
		$(function(){

			var url = window.location.pathname, 
				urlRegExp = new RegExp(url.replace(/\/$/,'') + "$"); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
				// now grab every link from the navigation
				$('.nav a').each(function(){
					// and test its normalized href against the url pathname regexp
					if(urlRegExp.test(this.href.replace(/\/$/,''))){
						$(this).addClass('active');
						$(this).closest('.nav > li').addClass('active');
						$('#side-menu > li:first-child').removeClass('active');
						active_added = true;
					}
					else{
						if(!active_added){
							$('#side-menu > li:first-child').addClass('active');
						}
					}
				});

		});
	</script>

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<!--script>
		$(document).ready(function(){
			$("#nav-toggle").click(function(){
				$("#main-nav").toggle();
			});
		});
		
		$(document).ready(function () {  
			$('.dropdown-toggle').dropdown();  
		}); 
	</script-->
</body>
</html>
<?php $this->endPage() ?>
