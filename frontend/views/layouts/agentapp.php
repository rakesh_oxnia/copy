<?php
use yii\helpers\Html;
use frontend\assets\AgentAppAsset;
use common\models\user;

/* @var $this \yii\web\View */
/* @var $content string */

AgentAppAsset::register($this);
$this->title = 'Agent App';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script src="//code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="//cdn.livechatinc.com/boilerplate/1.0.js"></script>
    <script src="//cdn.livechatinc.com/accounts/accounts-sdk.min.js"></script>
    <link rel="stylesheet" href="//cdn.livechatinc.com/boilerplate/1.0.css">
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>

				<?= $content ?>

    <?php $this->endBody() ?>
    <script>
      $(function(){

        $('.submit-btn').off('click').on('click', function(e){
          e.preventDefault();
          var category_id = $('#category_input').val();

          if(category_id == 0){
            alert('Please choose a category before submitting.');
            return false;
          }

          var outlet_id = $('#outlet_input').val();

          if(outlet_id == 0){
            alert('Please choose an outlet before submitting.');
            return false;
          }

          chat_id = getPropertyValue(chat_data.chat, 'id');
          tags = "{'lead', 'sales'}" // hardcoded
          visitor_name = $('#visitorName').val();
          visitor_email = $('#visitorEmail').val();
          visitor_phone = $('#visitorPhone').val();

          request_data = {
            visitor_name: visitor_name,
            visitor_email: visitor_email,
            visitor_phone: visitor_phone,
            category_id: category_id,
            outlet_id: outlet_id,
            tags: tags,
            chat_id: chat_id,
          }

          console.log(request_data);
          //alert('check data in console');
          //return false;

          $.ajax({
            type: 'POST',
            data: request_data,
            url: 'https://chatmetrics.webautodev.com/frontend/web/index.php?r=leads/push-new-lead',
            success: function(response){
              console.log(response);
              response = JSON.parse(response);
              if(response.success == true){
                if(response.action == 'Created'){
                  alert('New lead created successfully');
                }
                if(response.action == 'Updated'){
                  alert('Lead data updated successfully');
                }
              }else{
                console.log(response);
              }
            },
            error: function(data){
              console.log(data);
            }
          });

        });

        var instance = AccountsSDK.init({
          client_id: 'cf032a3393efbf851cba1ac152aed988',
          onIdentityFetched: (error, data) => {
            if (data) {
              console.log('User authorized!');
              console.log('License number: ' + data.license);
            }
          }
        });
      });
    </script>
</body>
</html>
<?php $this->endPage() ?>
