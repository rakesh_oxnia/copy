<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\DashboardnAsset;
use app\models\Managecontent;
use common\models\User;
use common\widgets\Alert;
 
use app\models\Homecontent;
 

AppAsset::register($this);
DashboardnAsset::register($this);

$role = Yii::$app->user->identity->role;

$theme_color = 'theme-whbl';
$agencylogo = Yii::$app->homeUrl.'cube/img/logo.png';
$agencylogoblack = Yii::$app->homeUrl.'cube/img/logo-black.png';
$agencylogosmall = Yii::$app->homeUrl.'cube/img/logo-small.png';
if(User::ROLE_AGENCY == $role)
{
	$theme_color = Yii::$app->user->identity->theme_color;
	$agencylogoimg = Yii::$app->user->identity->agency_logo;
	if( $agencylogoimg != '')
	{
		$agencylogo = Yii::$app->homeUrl.'logo_images/'.$agencylogoimg;
		$agencylogoblack = Yii::$app->homeUrl.'logo_images/'.$agencylogoimg;
		$agencylogosmall = Yii::$app->homeUrl.'logo_images/'.$agencylogoimg;
	}
	
}
else if(User::ROLE_USER == $role)
{
	$parent_id = Yii::$app->user->identity->parent_id;
	if($parent_id != '')
	{
		$row = (new \yii\db\Query())
	    ->from('user')
	    ->where(['id' => $parent_id])
	    ->one();

		$theme_color = $row['theme_color'];
		$agencylogoimg = $row['agency_logo'];
		if( $agencylogoimg != '')
		{
			$agencylogo = Yii::$app->homeUrl.'logo_images/'.$agencylogoimg;
			$agencylogoblack = Yii::$app->homeUrl.'logo_images/'.$agencylogoimg;
			$agencylogosmall = Yii::$app->homeUrl.'logo_images/'.$agencylogoimg;
		}
	}
}
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
	<script>
		var SiteUrl = '<?php echo Yii::getAlias('@web');?>';
		var ImagesUrl = '<?php echo Yii::getAlias('@web');?>/images';
	</script>

</head>
<style>
#content-wrapper{margin-left : 0px;!important}
</style>
<body class="pace-done <?= $theme_color;?>">
	<?php $this->beginBody() ?>

	<div id="theme-wrapper">
	<?php $c_user_id = Yii::$app->user->identity->id;?>
	
		<header class="navbar" id="header-navbar">
			<div class="container">
				<a href="<?= Yii::$app->homeUrl;?>" id="logo" class="navbar-brand">
					<img src="<?= $agencylogo;?>" alt="" class="normal-logo logo-white"/>
					<img src="<?= $agencylogoblack;?>" alt="" class="normal-logo logo-black"/>
					<img src="<?= $agencylogosmall;?>" alt="" class="small-logo hidden-xs hidden-sm hidden"/>
				</a>
				
				<div class="clearfix">
				<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="fa fa-bars"></span>
				</button>
				
			 
				
				<div class="nav-no-collapse pull-right" id="header-nav">
					<ul class="nav navbar-nav pull-right">
						 
						<li class="dropdown profile-dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<!--img src="img/samples/scarlet-159.png" alt=""/-->
								<span class="hidden-xs"><?= Yii::$app->user->identity->username;?></span> <b class="caret"></b>
							</a>
							<ul class="dropdown-menu dropdown-menu-right">
								 
								<li><a href="<?= Yii::$app->homeUrl;?>user/changepassword"><i class="fa fa-user"></i>Change Password</a></li>
								<li><a href="<?= Yii::$app->homeUrl;?>site/logout"><i class="fa fa-power-off"></i>Logout</a></li>
							</ul>
						</li>
						<li class="hidden-xxs">
							<a href="<?= Yii::$app->homeUrl;?>site/logout" class="btn">
								<i class="fa fa-power-off"></i>
							</a>
						</li>
					</ul>
				</div>
				</div>
			</div>
		</header>
		<div id="page-wrapper" class="container">
			<div class="row">
				
				<div id="content-wrapper">
					<div class="row">
						<div class="col-sm-12"><?= Alert::widget() ?></div>
						<div class="col-sm-12"><?= $content ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
		
	<?php $this->endBody() ?>
	 <script>
	 $('#colorpicker').ColorPicker();
	 </script>
 
</body>
</html>
<?php $this->endPage() ?>
