<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Oxnia</title>

    <meta http-equiv="X-UA-Compatible" content=="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/fonts/line-awesome/css/line-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/fonts/open-sans/styles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/tether/css/tether.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/common.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/pages/auth.min.css">
  <!-- Start of Chatcode -->
  <script type="text/javascript">
    var __lc = {};
    __lc.license = 6354551;
    window.__lc.ga_version
    window.__lc.chat_between_groups = false;
    (function() {
      var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
      lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
  </script>
  <!-- End of Chatcode -->
  <style>
    .btn:hover{
      cursor: pointer;
    }
  </style>
</head>
<body>

<div class="ks-page">

    <div class="ks-page-content">
        <div class="ks-logo">
			<img src="<?php echo Yii::$app->request->baseUrl;?>/images/cm-logo-retina.svg" width="360" height="150">
		</div>

        <div class="card panel panel-default ks-light ks-panel ks-login">
            <div class="card-block">
               <?php echo $content;?>
            </div>
        </div>
    </div>

</div>

<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/jquery/jquery.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/tether/js/tether.min.js"></script>
<script src="<?php echo Yii::$app->request->baseUrl;?>/libs/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
