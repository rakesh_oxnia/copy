<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\assets\DashboardAsset2;
use app\models\Managecontent;
use common\models\User;
use common\widgets\Alert;
use common\widgets\Navigation;
use common\widgets\Menu1;
use common\widgets\Menu2;
use common\widgets\Menu3;
use common\widgets\Menu4;
use common\widgets\Menu5;
use common\widgets\Menu6;
use app\models\Homecontent;
//use app\models\GlobalNotifications;

AppAsset::register($this);
DashboardAsset2::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
	<script>
		var SiteUrl = '<?php echo Yii::getAlias('@web');?>';
		var ImagesUrl = '<?php echo Yii::getAlias('@web');?>/images';
	</script>

</head>

<body>
	<?php $this->beginBody() ?>
	
	<div id="wrapper">

	
	<?php $c_user_id = Yii::$app->user->identity->id;?>
	
	<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top nav-topbar" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--a class="navbar-brand" href="index.html">SB Admin v2.0</a-->
				<a class="navbar-brand" href="<?php echo Yii::getAlias('@web');?>/site/index">
					
					<?php
					/* $site_url2 = Yii::$app->params['domain_url'];
					$HTTP_HOST = $_SERVER['HTTP_HOST'];        
					$subdomain = rtrim(str_replace($site_url2,'',$HTTP_HOST),'.');                        
					$user_id = User::findAll(['username'=>$subdomain]);                        
					$uu_id = $user_id[0]->id; */
					
					$uu_id = false;
					if($uu_id){                            
						$models = managecontent::findAll(['user_id'=>$uu_id]);
						//print_r($models[0]->content);                           
						if($models[0]->title){
							echo '<title>'.$models[0]->title.'</title>';                                
						}
						if($models[0]->file){
							echo '<img src="/uploads/'.$models[0]->file.'" class="img-responsive" >';
						}
					}else{                            
					?>
					<img src="<?php echo Yii::getAlias('@web');?>/images/logo.png" class="img-responsive" >
					<?php } ?>
				</a>
            </div>
            <!-- /.navbar-header -->

            <ul id="tnav" class="nav navbar-top-links navbar-right">
                <!--li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                </li-->
                <!-- /.dropdown -->
                <!--li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <span class="pull-right text-muted">40% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 2</strong>
                                        <span class="pull-right text-muted">20% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 3</strong>
                                        <span class="pull-right text-muted">60% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 4</strong>
                                        <span class="pull-right text-muted">80% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul-->
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <!--li class="dropdown li-bg-parent">
                    <a class="dropdown-toggle" id="nav-notify" data-uid="<?= $c_user_id; ?>" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>
						<span class="label label-warning">
							<?//= GlobalNotifications::getNoficationCount($c_user_id); ?>
						</span>&nbsp;
						<i class="fa fa-caret-down"> 
						</i>
                    </a>
                    <ul class="dropdown-menu header-list-li dropdown-alerts">
						<?php //$nofications = GlobalNotifications::getNofications($c_user_id); ?>
						<?php //if(!count($nofications)){ ?>
						<?php $nofications = false; ?>
						<?php if(!$nofications){ ?>
							
							<li>
								<div class="text-center">You have not new notifications</div>
							</li>
						<?php } else{ 
							
							foreach($nofications as $nofication):
								
								
								echo '<li><a href="'.$nofication->view_link.'"><i class="fa fa-file-pdf-o"></i><div>';
								
								echo $nofication->notification_message;
								
								echo '</div></a></li>';
								
							endforeach;
							
						} ?>
					
                        <li>
                            <a class="text-center li-btn" href="<?= Yii::$app->homeUrl .'notifications';?>">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
								<div class="clearfix"></div>
                            </a>
                        </li>
                    </ul>
                </li-->
                <!-- /.dropdown-alerts -->
                <!-- /.dropdown -->
                <li class="dropdown li-bg-parent">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user header-list-li2">
                        <!--li>
							<a href="<?= Yii::$app->homeUrl;?>user/view?id=<?= $c_user_id; ?>">
								<i class="fa fa-user fa-fw"></i> <?= Yii::$app->user->identity->username;?>
							</a>
                        </li>
						<li>
							<a href="<?= Yii::$app->homeUrl;?>user/update?id=<?= $c_user_id; ?>"><i class="fa fa-edit fa-fw"></i> Edit Profile</a>
						</li-->
                        <li><a href="<?= Yii::$app->homeUrl;?>site/logout">
							<i class="fa fa-sign-out fa-fw"></i> Logout ( <?= Yii::$app->user->identity->username; ?>)</a>
						</li>
                    </ul>
                </li>
            </ul>
            <!-- /.navbar-top-links -->
			
			<!--****** Side Navigation ******* -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
					<?php
			
						if(Yii::$app->user->identity->role == 50)
							echo Menu1::widget();
						if(Yii::$app->user->identity->role == 10)
							echo Menu2::widget();
						
						/* if(Yii::$app->user->identity->user_type == 3)
							echo Menu3::widget();
						if(Yii::$app->user->identity->user_type == 4)
							echo Menu4::widget();
						if(Yii::$app->user->identity->user_type == 5)
							echo Menu5::widget();
						if(Yii::$app->user->identity->user_type == 6)
							echo Menu6::widget(); */
					
					?>
                </div>
            </div>
			<!--****** END Side Navigation ******* -->
        </nav>
		
		<div id="page-wrapper" class="page-wraper">
			<div class="row">
				<h1 class="page-header">Dashboard</h1>
				
				<?= Alert::widget() ?>
				
				<?= $content ?>
			</div>
		</div>
		</div>
		
		<footer class="dashbrd">
			<p>Copyright 2016. All Rights reserved</p>
		</footer>
		
		
		
		<?php $this->endBody() ?>
		
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script>
			$(document).ready(function(){
				$("#nav-toggle").click(function(){
					$("#main-nav").toggle();
				});
			});
			
			$(document).ready(function () {  
				$('.dropdown-toggle').dropdown();  
			}); 
		</script>
	</body>
</html>
<?php $this->endPage() ?>
