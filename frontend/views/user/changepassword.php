<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Change Password : ' . ' ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="holder col-sm-11">
    <div class="panel panel-default">
		<div class="panel-heading">
			<?= Html::encode($this->title) ?>
		</div>
		<div class="panel-body">
		
		<?php $date = new DateTime(); ?>
		<div class="user-form">
		    <?php $form = ActiveForm::begin(); ?>
		    <?= $form->field($model, 'username')->hiddenInput(['maxlength' => true, 'readonly'=> true])->label(false) ?>
		    <?= $form->field($model, 'email')->hiddenInput(['maxlength' => true])->label(false) ?>
		    <?= $form->field($model, 'auth_key')->hiddenInput(['maxlength' => true])->label(false) ?>
		    <?= $form->field($model, 'password_hash')->hiddenInput(['maxlength' => true])->label(false) ?>
		    <p><label>New Password</label></p>
		    <p><input type="password" required id="txtPassword" name="new_password" class="form-control" /></p>
		    <p><label>Confirm Password</label></p>
		    <p><input type="password" required id="txtConfirmPassword" class="form-control" /></p>
		    <?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>   		   
		    <?= $form->field($model, 'updated_at')->hiddenInput(['value' =>  $date->getTimestamp()])->label(false) ?>
		    <?= $form->field($model, 'password_reset_token')->hiddenInput(['maxlength' => true])->label(false) ?>
		    <div class="form-group">
		    	<button type="button" class="btn btn-success" onclick="return Validate()">Update Password</button>
		        <?= Html::a('Cancel', ['view', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>      
		    </div>
		    <?php ActiveForm::end(); ?>
		</div>
		 <script type="text/javascript">
		    function Validate() {
		        var password = document.getElementById("txtPassword").value;
		        var confirmPassword = document.getElementById("txtConfirmPassword").value;
		        if (password != confirmPassword) {
		            alert("Passwords do not match.");
		            return false;
		        }else if(password == ''){
		        	alert("Passwords should not blank.");
		            return false;
		        }
		        document.getElementById("w0").submit();
		        return true;
		    }
		</script>
	</div>
</div>
</div>
