<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use common\models\LiveChat_API;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\User2 */
/* @var $form yii\widgets\ActiveForm */

$bl = array();
if(isset($_GET['id']) && $_GET['id'] != '')
{
    $bl = array();
    $connection = Yii::$app->getDb();
    $command = $connection->createCommand('SELECT *
    FROM  `user_associate`
    WHERE  `user_id` ='.$_GET['id']);
    $bl = $command->queryAll();
    $franchiseId = $_GET['id'];
    $API = new LiveChat_API();
    $response = $API->groups->get($model->user_group);

}else{
  $response = '';
}
$API = new LiveChat_API();
$allAgents = $API->agents->getAllAgents();
$agentArray = [];
$emailArr = [];
foreach ($allAgents as $agent) {
  $agentArray[$agent->login] = $agent->name . " ($agent->login)";
  $emailArr[] = $agent->login;
}

$perLeadCurrencies = array();
$stripeUserCurrencies = User::$stripeUserCurrencies;
foreach ($stripeUserCurrencies as $currency => $arr_info) {
  $perLeadCurrencies[$currency] = $arr_info["name"];
}
?>

<div class="user2-form user-index">

  <?php $form = ActiveForm::begin(); ?>

  <?= $form->field($model, 'business_name')->textInput(['placeholder' => 'Business Name','class' => 'business_name form-control'])->label('Business name');?>

  <?php
    if(Yii::$app->controller->action->id == 'franchise-create'){
      echo $form->field($model, 'name')->textInput(['maxlength' => true,'placeholder'=>'Franchise Name'])->label('Franchise name');
    }elseif (Yii::$app->controller->action->id == 'outlet-create'){
      echo $form->field($model, 'name')->textInput(['maxlength' => true,'placeholder'=>'Outlet Name'])->label('Outlet name');
    }elseif (Yii::$app->controller->action->id == 'update'){
      if($model->role == User::ROLE_FRANCHISE){
        echo $form->field($model, 'name')->textInput(['maxlength' => true,'placeholder'=>'Franchise Name'])->label('Franchise name');
      }
      if($model->role == User::ROLE_OUTLET){
        echo $form->field($model, 'name')->textInput(['maxlength' => true,'placeholder'=>'Outlet Name'])->label('Outlet name');
      }

    }
    else{
      echo $form->field($model, 'name')->hiddenInput(['value' => ' - '])->label(false);
    }
  ?>

	<?php if($model->isNewRecord):?>

    <?php echo $form->field($model, 'username')->hiddenInput(['value' => ''])->label(false) ?>

	<?php endif; ?>

    <?php //= $form->field($model, 'auth_key')->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>

    <?php //= $form->field($model, 'password_reset_token')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true,'placeholder'=>'Email']) ?>

     <?php


    if(count($bl) > 0 && Yii::$app->controller->action->id == 'update')
    {
        $i = 0;
        foreach ($bl as $value) {

            if($i == 0):?>
            <div class="input_fields_wrap2">
                <div style="float: right;">Add More</div>
                <div class="clear"></div>
                <img style="float: right;" alt="Add More" title="Add More" class="add_field_button2" src="images/add-icon.png" />
                <div class="clear"></div>
                <div class="help-block"></div>

            <?php
            endif;
            ?>

            <div>
                <div class="form-group"><label>Other Email</label>
                    <input type="text" class="form-control" name="User2[email2][]" value="<?php echo $value['email'];?>">
                </div>

                <?php if($i != 0) { ?>
                    <a href="#" class="remove_field2" style="float:right;"><img alt="Remove" title="Remove" src="images/remove-icon.png" /></a>
                <?php } ?>
            </div>
            <div class="clear"></div><div class="help-block"></div>

            <?php

            $i++;
        }
        ?>
        </div>
        <?php

    }else
    {
        ?>
        <div class="input_fields_wrap2">
            <div style="float: right;">Add More</div>
                <div class="clear"></div>
            <img style="float: right;" alt="Add More" title="Add More" class="add_field_button2" src="images/add-icon.png" />
            <div class="clear"></div>
            <div class="help-block"></div>

        </div>
        <?= $form->field($model, 'email2[]')->textInput(['maxlength' => true]) ?>


    <?php
    }
    ?>

	<?php if(!$model->isNewRecord):?>

    <?= $form->field($model, 'status')->dropdownList(['10'=>'Active','0'=>'Inactive'],["prompt"=>"-- Select Status --"]) ?>

	<?php endif; ?>

    <?php //= $form->field($model, 'role')->textInput() ?>

    <?php
      // if(Yii::$app->controller->action->id == 'franchise-create'){
      //   echo $form->field($model, 'user_group')->hiddenInput(['value' => 0])->label(false);
      // }else{
      //   echo $form->field($model, 'user_group')->textInput(['maxlength' => true,'placeholder'=>'User Group']);
      // }

      if(Yii::$app->controller->action->id == 'outlet-create'){
        $franchiseModel = User::findOne($franchiseId);
        echo $form->field($model, 'user_group')->textInput(['maxlength' => true,'placeholder'=>'User Group', 'value' => $franchiseModel->user_group]);
      }else {
          //echo $form->field($model, 'user_group')->textInput(['maxlength' => true,'placeholder'=>'User Group']);
          if($model->role != User::ROLE_OUTLET){
           
           echo $form->field($model, 'agents')->dropdownList($agentArray,["prompt"=>"-- Select Agent --", "class" => "select form-control agent-dropdown", 'multiple' => true]);
              
          }
      }
    ?>


    <?= $form->field($model, 'website_url')->textInput(['maxlength' => true,'placeholder'=>'Website Url']) ?>
    
    <?php
    if(Yii::$app->controller->action->id != 'outlet-create' && $model->role != User::ROLE_OUTLET){
     echo $form->field($model, 'per_lead_cost')->textInput(['maxlength' => true,'placeholder'=>'Per Lead Cost(Pay as You Go)']);
     echo $form->field($model, 'per_lead_cost_currency')->dropDownList(
            $perLeadCurrencies,
            //  ['AUD' => 'Australian Dollar',
            //   'BRL' => 'Brazilian Real',
            //   'CAD' => 'Canadian Dollar',
            //   'CZK' => 'Czech Koruna',
            //   'DKK' => 'Danish Krone',
            //   'EUR' => 'Euro',
            //   'HKD' => 'Hong Kong Dollar',
            //   'HUF' => 'Hungarian Forint',
            //   'ILS' => 'Israeli New Sheqel',
            //   'MYR' => 'Malaysian Ringgit',
            //   'MXN' => 'Mexican Peso',
            //   'NOK' => 'Norwegian Krone',
            //   'NZD' => 'New Zealand Dollar',
            //   'PHP' => 'Philippine Peso',
            //   'PLN' => 'Polish Zloty',
            //   'GBP' => 'Pound Sterling',
            //   'SGD' => 'Singapore Dollar',
            //   'SEK' => 'Swedish Krona',
            //   'CHF' => 'Swiss Franc',
            //   'TWD' => 'Taiwan New Dollar',
            //   'THB' => 'Thai Baht',
            //   'TRY' => 'Turkish Lira',
            //   'USD' => 'U.S. Dollar'
            // ],
             ['prompt'=>'Select...']
     );
   }
    ?>

    <?= $form->field($model, 'tax')->textInput(['maxlength' => true,'placeholder'=>'Tax Percentage(GST)']) ?>

    <?php

      if(Yii::$app->controller->action->id == 'create')
      {
        echo $form->field($model, 'average_lead_value')->textInput(['maxlength' => true,'placeholder'=>'Average Lead Price($)']);
        echo $form->field($model, 'average_lead_cost')->textInput(['maxlength' => true,'placeholder'=>'Average Lead Cost($)']);
      }

      if(Yii::$app->controller->action->id == 'update' && $model->role == User::ROLE_USER)
      {
        echo $form->field($model, 'average_lead_value')->textInput(['maxlength' => true,'placeholder'=>'Average Lead Price($)']);
        echo $form->field($model, 'average_lead_cost')->textInput(['maxlength' => true,'placeholder'=>'Average Lead Cost($)']);
      }

    if(Yii::$app->controller->action->id == 'update' && ($model->role == User::ROLE_USER || $model->role == User::ROLE_FRANCHISE))
    {
        ?>

        <div class="form-group field-user2-show_past_lead" id="show_past_lead_wrapper">
            <label class="control-label" for="user2-show_past_lead">Show Past Lead (Effective only when already assigned package has been cancelled and user needs to be shown past leads)</label><br/>
            <label class="ks-checkbox-slider">
                <input type="checkbox" name="User2[show_past_lead]" id="show_past_lead">
                <span class="ks-indicator"></span>
            </label>

            <div class="help-block"></div>
        </div>

        <div class="form-group field-user2-show_lead_from" id="show_lead_from_wrapper" style="display:none;">
            <label class="control-label" for="user2-show_lead_from">Show Lead From</label>
            <input type="text" id="user2-show_lead_from" class="form-control" name="User2[show_lead_from]" readonly="true" <?php if(!$model->isNewRecord && $model->show_lead_from > 0) {?> value="<?php echo date('Y-m-d', $model->show_lead_from);?>" <?php } ?> />

            <div class="help-block"></div>
        </div>

        <div class="form-group field-user2-show_lead_to" id="show_lead_to_wrapper" style="display:none;">
            <label class="control-label" for="user2-show_lead_from">Show Lead To</label>
            <input type="text" id="user2-show_lead_to" class="form-control" name="User2[show_lead_to]" readonly="true" <?php if(!$model->isNewRecord && $model->show_lead_to > 0) {?> value="<?php echo date('Y-m-d', $model->show_lead_to);?>" <?php } ?> />

            <div class="help-block"></div>
        </div>

    <?php
    }

    ?>

    <?php echo $form->field($model, 'send_transcript')->checkBox(['label' =>  'Send transcript in leads', 'uncheck' => 0, 'selected' => 1]); ?>

    <?php echo $form->field($model, 'free_access')->checkBox(['label' =>  'Free Access']); ?>
    
    <?php echo $form->field($model, 'disable_payment_status')->checkBox(['uncheck' => 0, 'selected' => 1]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

		<a href="<?= URL::to(['client-franchise-index']) ?>" class="btn btn-primary">Cancel</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php

if($model->isNewRecord)
{
  $this->registerJs("
  $(function(){
    $('.agent-dropdown').val(".json_encode($emailArr).")
  })

  ");
}else {
    if($model->show_past_lead == 1) {
        $this->registerJs("
            $('#show_past_lead').prop('checked',true);
            $('#show_lead_from_wrapper').show();
            $('#show_lead_to_wrapper').show();
        ");
    }

    $this->registerJs("
        $('#show_past_lead').change(function() {
             if($(this).is(':checked')) {
                 $('#show_lead_from_wrapper').show();
                 $('#show_lead_to_wrapper').show();
             }else{
                 $('#show_lead_from_wrapper').hide();
                 $('#show_lead_to_wrapper').hide();
             }
         });
         
         $('document').ready(function(){
            $('#user2-show_lead_from').datepicker({
                 dateFormat: 'yy-mm-dd',
                 changeMonth: true,
                 changeYear: true,
                 stepMonths: 0,
                 maxDate: new Date(),
                 onClose: function( selectedDate )
                    {
                        $('#user2-show_lead_to').datepicker('option', 'minDate', selectedDate );
                    }
            });
            
            $('#user2-show_lead_to').datepicker({
                 dateFormat: 'yy-mm-dd',
                 changeMonth: true,
                 changeYear: true,
                 stepMonths: 0,
                 maxDate: new Date(),
                 onClose: function( selectedDate )
                    {
                        $('#user2-show_lead_from').datepicker('option', 'maxDate', selectedDate );
                    }
            });
          });
    ");

}

if($response!=''){
  $this->registerJs("
    $(function(){
      $('.agent-dropdown').val(".(isset($response->agents)?json_encode($response->agents):'').")
      $('.business_name').val(".(isset($response->name)?json_encode($response->name):'').")
    })

  ");
}

  $this->registerCss("
    span.select2.select2-container{
      width: 100% !important;
    }
  ");

 $this->registerJs('
 
 
    $("#user2-per_lead_cost").change(function(){
     var per_lead_cost  = $(this).val();
     if(per_lead_cost==""){
       $("#user2-per_lead_cost").next(".help-block").text("");
       $("#user2-per_lead_cost_currency").next(".help-block").text("");
     }
   });
   $("#user2-per_lead_cost_currency").change(function(){
    var per_lead_cost_currency  = $(this).val();
    if(per_lead_cost_currency==""){
      console.log("yes");
      $("#user2-per_lead_cost").next(".help-block").text("");
      $("#user2-per_lead_cost_currency").next(".help-block").text("");
    }
   });
    $(document).ready(function() {

      $(".select").select2();

    var cntoptions = $("#cntoptions").html();

    var max_fields_common = 10;

    var wrapper2         = $(".input_fields_wrap2");
    var add_button2      = $(".add_field_button2");

    var x = 1; //initlal text box count
    $(add_button2).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields_common){ //max input box allowed
            x++; //text box increment
            $(wrapper2).append(\'<div><div class="form-group"><label>Other Email</label><input type="text" class="form-control" name="User2[email2][]"></div><a href="#" class="remove_field2" style="float:right;"><img alt="Remove" title="Remove" src="images/remove-icon.png" /></a></div><div class="clear"></div><div class="help-block"></div>\');
        }
    });

    $(wrapper2).on("click",".remove_field2", function(e){
        e.preventDefault(); $(this).parent(\'div\').remove(); x--;
    })


});');
?>
