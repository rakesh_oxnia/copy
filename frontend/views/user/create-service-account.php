<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User2 */

$this->title = 'Create Service Account';
$this->params['breadcrumbs'][] = ['label' => 'User2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
.help-block{
	color : red;
}
</style>
  <div class="ks-page-header">
      <section class="ks-title">
          <h3><?php echo $this->title;?></h3>
      </section>
  </div>

  <div class="ks-page-content">
      <div class="ks-page-content-body">
          <div class="ks-nav-body-wrapper">
              <div class="container-fluid">
                  <div class="row">
                      <div class="col-lg-12 ks-panels-column-section">
                          <div class="card">
                              <div class="card-block">
              									<?= $this->render('_service_account_form', [
              										'model' => $model,
              									]) ?>
            									</div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
