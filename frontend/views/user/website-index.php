<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = empty($clientModel->name) ? ucfirst($clientModel->business_name) : ucfirst($clientModel->name);
$this->title .= ' : Website Listing';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ks-page-header">
    <section class="ks-title">
        <h3><?php echo $this->title;?></h3>
    </section>
</div>

<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">



                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                                <p>
                                    <?= Html::a('Create Website', ['addwebsite','id' => $clientModel->id], ['class' => 'btn btn-success']) ?>
                                </p>

                                <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    //'layout' => '{summary}<div class="table-responsive">{items}</div>{pager}',
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],
                                        'website_url:url',
                                        'user_group',
                                        [
                                            'attribute' => 'status',
                                            'value' => function ($model) {
                                                if($model->status == 10){
                                                    return 'Active';
                                                }else{
                                                    return 'Inactive';
                                                }
                                            },
                                            'filter' => Html::activeDropDownList($searchModel,'status',['0' => 'Inactive','10' => 'Active'],[
                                                'class' => 'form-control',
                                                'prompt' => 'All'
                                            ]),
                                        ],
                                        [
                                            'attribute' => 'username',
                                            'label' => 'Package',
                                            'filter' => false,
                                            'value' => function ($model) {
                                                return $model->getPackageText();
                                            },
                                        ],                                                                                
                                        [   'class' => 'yii\grid\ActionColumn',
                                            'header'=>'Action',
                                            'headerOptions' => ['class' => 'action-column'],
                                            'template'=>'{user}{view}{update}',
                                            'buttons' => [

                                                'user' => function ($url, $model) {
                                                    $url = Url::to(['user/switch-user', 'id' => $model->id]);
                                                    return Html::a('<span class="ks-icon la la-user"></span>',
                                                        $url);
                                                },

                                                'view' => function ($url, $model) {
                                                    if($model->role == 10) {
                                                        $url = Url::to(['user/view', 'id' => $model->id]);
                                                    }else if($model->role == 40) {
                                                        $url = Url::to(['user/franchise-view', 'id' => $model->id]);
                                                    }
                                                    return Html::a('<span class="ks-icon la la-eye"></span>',
                                                        $url);
                                                },

                                                'update' => function ($url, $model) {
                                                    return Html::a('<span class="ks-icon la la-pencil"></span>',
                                                        $url);
                                                },
                                            ],
                                        ],
                                    ],
                                ]); ?>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerCss("

    .table td{
      padding: 10px !important;
      font-size: 12px !important;
    }

    .ks-icon{
      font-size: 20px !important;
      padding-left: 7px;
    }

    .action-column{
      width: 140px;
    }

    .ks-icon:hover{
      color: #f00;
    }
  ");
?>
