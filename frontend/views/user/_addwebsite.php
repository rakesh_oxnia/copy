<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="user2-form user-index">

    <?php $form = ActiveForm::begin(); ?>
	
	  

    <?= $form->field($model, 'user_group')->textInput(['maxlength' => true,'placeholder'=>'User Group']) ?>
	
    <?= $form->field($model, 'website_url')->textInput(['maxlength' => true,'placeholder'=>'Website Url']) ?>
	

    
    <div class="form-group">
        <input type="submit" name="add" value="Add Website" class="btn btn-primary">
		
		<a href="<?= Yii::$app->homeUrl;?>user/index" class="btn btn-primary">Cancel</a>
    </div>

    <?php ActiveForm::end(); ?>

</div>
