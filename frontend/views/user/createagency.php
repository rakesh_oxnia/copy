<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User2 */

$this->title = 'Create Agency';
$this->params['breadcrumbs'][] = ['label' => 'User2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="row">
	<div class="col-lg-12">
		<div id="content-header" class="clearfix">
			<div class="pull-left">
				<ol class="breadcrumb">
					<li><a href="<?= Yii::$app->homeUrl;?>">Home</a></li>
					<li class="active"><span><?= Html::encode($this->title) ?></span></li>
				</ol>
				<h1><?= Html::encode($this->title) ?></h1>
			</div>
		</div>
	</div>
</div>
<div class="user2-create user-index">

   

    <?= $this->render('_formagency', [
        'model' => $model,
    ]) ?>

</div>
