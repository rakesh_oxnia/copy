<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use common\models\User;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Allocate Plan';
$this->params['breadcrumbs'][] = $this->title;
$stripeUserCurrencies = User::$stripeUserCurrencies;
?>
<div class="ks-page-header">
            <section class="ks-title">
                <h3><?php echo $this->title;?></h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                      <div class="row">
                          <div class="col-lg-12 ks-panels-column-section">
                              <div class="card">
                                  <div class="card-block">
                                    <?php $form = ActiveForm::begin(); ?>
                                  <table class="table table-bordered detail-view">
                                  <tbody>
                                  <tr>
                                    <th>Packages</th>
                                    <td>
                                      <select name="package" id="packages_select" class="form-control" required>
             														<option value="">Choose Packages</option>
             														<?php if( !empty( $packages ) ){?>
             																<?php foreach($packages as $package){?>
                                              <option value="<?php echo $package['id'];?>"><?php echo $package['package_name'];?>
                                              </option>
             																<?php }?>
             														<?php }?>
                                        <option id="custom_package" value="0">Custom Package</option>
             													</select>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>Number of leads</th>
                                    <td>
                                      <span id="number_of_leads" ></span>
                                      <input type="number" style="display:none" id="custom_lead">
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>Amount</th>
                                    <td><span id="amount"></span>
                                      <input style="display:none" type="text" id="custom_amount">
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>Currency</th>
                                    <td><span id="currency"></span>
                                      <!-- Currently our code does not support zero-decimal currency.
                                      Zero-decimal currencies are those which does not support decimal values, so when making a payment if we multiple them by 100 [as we usually do because stripe accepts payment in smallest unit of currency] then as it does not support decimal values it will actually charge 100 times more than required.
                                      Before adding new currency please check below links: https://stripe.com/docs/currencies#zero-decimal
                                      https://stripe.com/docs/currencies#presentment-currencies -->
                                      <select style="display:none" name="custom_currency" id="custom_currency" class="form-control">
                                    		  <option value="">Select Currency</option>
                                          <?php
                                          foreach ($stripeUserCurrencies as $currency => $arr_info) {
                                          ?>
                                            <option value="<?php echo $currency;?>">
                                              <?php echo $arr_info["name"];?>
                                            </option>
                                          <?php
                                          }
                                          ?>
                                    		  <!-- <option value="AUD">Australian Dollar</option>
                                    		  <option value="BRL">Brazilian Real </option>
                                    		  <option value="CAD">Canadian Dollar</option>
                                    		  <option value="CZK">Czech Koruna</option>
                                    		  <option value="DKK">Danish Krone</option>
                                    		  <option value="EUR">Euro</option>
                                    		  <option value="HKD">Hong Kong Dollar</option>
                                    		  <option value="HUF">Hungarian Forint </option>
                                    		  <option value="ILS">Israeli New Sheqel</option>
                                    		  <option value="MYR">Malaysian Ringgit</option>
                                    		  <option value="MXN">Mexican Peso</option>
                                    		  <option value="NOK">Norwegian Krone</option>
                                    		  <option value="NZD">New Zealand Dollar</option>
                                    		  <option value="PHP">Philippine Peso</option>
                                    		  <option value="PLN">Polish Zloty</option>
                                    		  <option value="GBP">Pound Sterling</option>
                                    		  <option value="SGD">Singapore Dollar</option>
                                    		  <option value="SEK">Swedish Krona</option>
                                    		  <option value="CHF">Swiss Franc</option>
                                    		  <option value="TWD">Taiwan New Dollar</option>
                                    		  <option value="THB">Thai Baht</option>
                                    		  <option value="TRY">Turkish Lira</option>
                                    		  <option value="USD">U.S. Dollar</option> -->
                                      </select>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>Interval</th>
                                    <td><span id="interval">month</span></td>
                                  </tr>
                                  <tr>
                                    <th>Extra Amout Per-Lead</th>
                                    <td>
                                      <input type="text" id="extra_per_lead_amount" required>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>Enable Advanced Payment</th>
                                    <td>
                                      <label class="ks-checkbox-slider">
                                       <input type="checkbox" name="advance_payment" id="advance_payment">
                                       <span class="ks-indicator"></span>
                                     </label>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>Automatic Stripe Charge</th>
                                    <td>
                                      <label class="ks-checkbox-slider">
                                       <input type="checkbox" name="automatic_charge" id="automatic_charge">
                                       <span class="ks-indicator"></span>
                                     </label>
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>GST</th>
                                    <td>
                                      <label class="ks-checkbox-slider">
                                       <input type="checkbox" name="gst_status" id="gst_status">
                                       <span class="ks-indicator"></span>
                                     </label>
                                    </td>
                                  </tr>
                                  <tr id="gst_percent_table" style="display:none">
                                    <th>GST Percent</th>
                                    <td>
                                      <input type="text" value="10" id="gst_percent">
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>Change Activation Date</th>
                                    <td>
                                      <label class="ks-checkbox-slider">
                                       <input type="checkbox" name="change_activation_date" id="change_activation_date">
                                       <span class="ks-indicator"></span>
                                     </label>
                                    </td>
                                  </tr>
                                  <tr id="plan_activation_table" style="display:none">
                                    <th>Plan Activation Date</th>
                                    <td>
                                      <input type="text" placeholder="Activation Date"  id="plan_activation_date" name="plan_activation_date" readonly="true">
                                    </td>
                                  </tr>
                                  <tr>
                                    <th>Past Lead Access</th>
                                    <td>
                                      <label class="ks-checkbox-slider">
                                       <input type="checkbox" name="show_previous_lead" id="show_previous_lead">
                                       <span class="ks-indicator"></span>
                                     </label>
                                    </td>
                                  </tr>
                                  <tr id="tr-action-row">
                                    <th>
                                      <?= Html::a('Cancel', ['view', 'id' => $user_id], ['class' => 'btn btn-primary']) ?>
<input type="hidden" id="user_id" value="<?= $user_id ?>" />
                                    </th>
                                    <td>
                                      <?= Html::submitButton('Allocate plan',[
                                        'class'=>'btn btn-primary allocate_plan'
                                      ]) ?>
                                    </td>
                                  </tr>
                                  <tr id="tr_cancel_package">
                                      <td>&nbsp;</td>
                                      <td>
                                            <button
                                                    class="btn btn-primary"
                                                    id="cancel_package"
                                                    data-user-id="<?php echo $user_id; ?>">
                                                    Cancel Package
                                                </button>
                                        </td>
                                  </tr>

                                  </tbody>
                                  </table>
                                  <?php ActiveForm::end(); ?>
                                  </div>
                                </div>
                          </div>
                    </div>
               </div>
            </div>
        </div>
        <?php
$this->registerCss("
/* For Firefox */
input[type='number'] {
    -moz-appearance:textfield;
}
/* Webkit browsers like Safari and Chrome */
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
}
");
$this->registerJs("

   $('#gst_status').change(function() {
        if($(this).is(':checked')) {
            $('#gst_percent_table').show();
        }else{
            $('#gst_percent_table').hide();
        }
    });

    $('#change_activation_date').change(function() {
         if($(this).is(':checked')) {
             $('#plan_activation_table').show();
         }else{
             $('#plan_activation_table').hide();
         }
     });

   $('#custom_lead').keypress(function(e) {
    if (e.which < 48 || e.which > 57) {
        //showAdvice(this, 'Integer values only');
        return(false);
    }
});

  $('#packages_select').select2();
  var user_id = $('#user_id').val();
/////////////////////////////////////////////////
  $.ajax({
    type: 'GET',
    url: '".Url::to(['allocate-package/allocate-plan-data'])."',
    data: {user_id:user_id},
    success: function(response){
      response = JSON.parse(response)
      if(response.success == true){
  $('#extra_per_lead_amount').val(response.data.extra_per_lead_amount);
  (response.data.advance_payment===1)?$('#advance_payment').prop('checked',true):'';
  (response.data.automatic_charge===1)?$('#automatic_charge').prop('checked',true):'';
  (response.data.gst_status==1)?$('#gst_status').prop('checked',true):'';
  (response.data.show_previous_lead==1)?$('#show_previous_lead').prop('checked',true):'';
  (response.data.custom_activation==1)?$('#change_activation_date').prop('checked',true):'';
  console.log(response.data.gst_status);
  if(response.data.gst_status===1){
     $('#gst_percent_table').show();
     $('#gst_percent').val(response.data.gst_percent);
    }
    if(response.data.custom_activation==1){
       $('#plan_activation_table').show();
       $('#plan_activation_date').val(response.data.created_at);
      }
    if(response.data.custom_status===1){
      $('#custom_package').val(response.data.package_id);
    }
    
    $('#tr_cancel_package').show();
    $('#tr-action-row').hide();
    
    $('#packages_select').val(response.data.package_id).trigger('change');
  }else{
    $('#tr_cancel_package').hide();
    $('#tr-action-row').show();
    
    $('#advance_payment').prop('checked',true);
    $('#automatic_charge').prop('checked',true);
  }
    },
    error: function(data){
      console.log(data);
      swal('Some problem occured!', 'Please try again later after sometime.', 'error');
      return false;
    },
  });
  ////////////////////////////////////////////////////
  $('#packages_select').on('change', function(e){
        var package_id  = $(this).val();
        if(package_id!=''){
          if(package_id!='0'){
            $('#number_of_leads').show();
            $('#amount').show();
            $('#currency').show();
            //hide
            $('#custom_lead').hide();
            $('#custom_amount').hide();
            $('#custom_currency').hide();
          $.ajax({
            type: 'POST',
            url: '".Url::to(['user/get-single-plan'])."',
            data: {package_id: package_id},
            success: function(response){
              response = JSON.parse(response)
              if(response.success == true){
                if(response.custom_status==0){
                  $('#number_of_leads').text(response.number_of_leads);
                  $('#amount').text(response.amount);
                  $('#interval').text(response.interval);
                  $('#currency').text(response.currency);
                }else if(response.custom_status==1){
                  $('#interval').text(response.interval);
                  $('#custom_lead').val(response.number_of_leads);
                  $('#custom_amount').val(response.amount);
                  $('#custom_currency').val(response.currency);
                  $('#custom_lead').show();
                  $('#custom_amount').show();
                  $('#custom_currency').show();
                  $('#number_of_leads').hide();
                  $('#amount').hide();
                  $('#currency').hide();
                }
               }
            },
            error: function(data){
              console.log(data);
              swal('Some problem occured!', 'Please try again later after sometime.', 'error');
              return false;
            },
          });
        }else{
          //hide package
          $('#number_of_leads').hide();
          $('#amount').hide();
          $('#currency').hide();
          //display custom fields
          $('#custom_lead').show();
          $('#custom_amount').show();
          $('#custom_currency').show();
        }
      }else{
        //hide
        $('#custom_lead').hide();
        $('#custom_amount').hide();
        $('#custom_currency').hide();
        //hide package
        $('#number_of_leads').hide();
        $('#amount').hide();
        $('#currency').hide();
      }
  });
  
  $('#cancel_package').off('click').on('click',function(e) {    
      e.preventDefault();
      swal({
          title: 'Package will be cancelled !!',
          text: 'Are you sure you want to go ahead ?',
          icon: 'warning',
          buttons: true,
          dangerMode: true,
        })
        .then((cancel) => {
          if (cancel) {
            var user_id = $('#cancel_package').attr('data-user-id');
            $.ajax({
                type: 'POST',
                url: '".Url::to(['user/cancel-plan'])."',
                data: {user_id: user_id},
                success: function(response) {
                  response = JSON.parse(response);
                  if(response.success == true) {
                    swal('Plan cancelled successfully', {
                      icon: 'success',
                    }).then((value) => {
                      window.location = window.location.href;
                    });
                  }
                }
            });
          } else {
            //no action
          }
        });
  });
  
  $('.allocate_plan').off('click').on('click', function(e){
    e.preventDefault();
    var gst_status = ($('#gst_status').prop('checked'))?1:0;
    var gst_percent = $('#gst_percent').val();

    var change_plan_activation = ($('#change_activation_date').prop('checked'))?1:0;
    var plan_activation_date = $('#plan_activation_date').val();

    if(gst_status==1){
        if(gst_percent=='' || gst_percent==''){
          swal('Please choose a GST Percent');
          return false
        }
        if(isNaN(gst_percent) || gst_percent <= 0) {
          swal('Please enter valid gst percent');
          return false;
        }
    }else{
      var gst_percent = 0;
    }

    if(change_plan_activation==1){
        if(!plan_activation_date || plan_activation_date == '') {
          swal('Please enter plan activation date');
          return false;
        }
    }else{
      var plan_activation_date = '';
    }

    var packages_select = $('#packages_select').val();
    var extra_per_lead_amount = $('#extra_per_lead_amount').val();
    var advance_payment = ($('#advance_payment').prop('checked'))?1:0;
    var automatic_charge = ($('#automatic_charge').prop('checked'))?1:0;
    var show_previous_lead = ($('#show_previous_lead').prop('checked'))?1:0;

    // if(advance_payment==0 && automatic_charge==0){
    //   swal('Automatic Packages Cannot be disable when Advanced Package is disable');
    //   return false
    // }

    if(packages_select==''){
      swal('Please choose a Plan');
      return false
    }

    custom_package_id = 0;
    if($('#custom_package').is(':selected')){
      var custom_lead = $('#custom_lead').val();
      if(custom_lead==''){
        swal('Please choose a Custom-lead');
        return false;
      }
      var custom_amount = $('#custom_amount').val();
      if(custom_amount==''){
        swal('Please choose a Custom-amount');
        return false;
      }
      if(isNaN(custom_amount) || custom_amount <= 0) {
        swal('Please enter valid amount');
        return false;
      }
      var custom_currency = $('#custom_currency').val();
      if(custom_currency==''){
        swal('Please choose a Custom-currency');
        return false;
      }
     custom_package_id = $('#custom_package').val();
    }

    if(extra_per_lead_amount == '' || isNaN(extra_per_lead_amount) || extra_per_lead_amount <= 0) {
      swal('Please enter valid Extra Per Lead Amount');
      return false
    }

    var user_id = $('#user_id').val();
    if($('#custom_package').is(':selected')){
      var request = {
        advance_payment:advance_payment,
        automatic_charge:automatic_charge,
        extra_per_lead_amount:extra_per_lead_amount,
        packages_select: packages_select,
        custom_package_id:custom_package_id,
        gst_status:gst_status,
        gst_percent:gst_percent,
        user_id:user_id,
        custom_lead:custom_lead,
        custom_amount:custom_amount,
        custom_currency:custom_currency,
        show_previous_lead:show_previous_lead,
        change_plan_activation:change_plan_activation,
        plan_activation_date:plan_activation_date
      };
    }else{
      var request = {
        advance_payment:advance_payment,
        automatic_charge:automatic_charge,
        extra_per_lead_amount:extra_per_lead_amount,
        packages_select: packages_select,
        gst_status:gst_status,
        gst_percent:gst_percent,
        user_id:user_id,
        show_previous_lead:show_previous_lead,
        change_plan_activation:change_plan_activation,
        plan_activation_date:plan_activation_date
      }
    }
    $.ajax({
      type: 'POST',
      url: '".Url::to(['allocate-package/allocate-plan'])."',
      data: request,
      beforeSend: function() {
        $('.allocate_plan').prop('disabled', true);
      },
      success: function(response){
        response = JSON.parse(response)
        if(response.success == true){
          console.log(response);
          swal(response.data)
          .then((value) => {
              window.location = window.location.href;
            });
        }
        if(response.success == false){
          swal(response.data);
          $('.allocate_plan').prop('disabled', false);
          return false;
        }
      },
      error: function(data){
        console.log(data);
        swal('Some problem occured!', 'Please try again later after sometime.', 'error');
        return false;
      },
    });
  });

  $('document').ready(function(){
    $('#plan_activation_date').datepicker({
         dateFormat: 'yy-mm-dd',
         changeMonth: false,
         changeYear: false,
         stepMonths: 0
    }).focus(function() {
      $('.ui-datepicker-prev, .ui-datepicker-next').remove();
    });
  });

");
?>
