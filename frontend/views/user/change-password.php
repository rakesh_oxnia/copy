<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\User2 */

$this->title = str_replace(array('http://','https://'), '', $model->website_url);
?>
<div class="user2-view user-index">

	<div class="row">
		<div class="col-xl-12 ks-draggable-column">
				<div id="ks-bar-chart-panel" class="card panel">
						<h5 class="card-header">
							 Change User Password : <?= trim($this->title, '/') ?>
						</h5>
						<div class="card">
							<div class="card-block">

								<?php $form = ActiveForm::begin([
									'action' => Url::to(['settings/change-agency-user-password']),
									'id'=>'change-agency-user-password-form',

								]); ?>
								<input type="hidden" name="id" value="<?= Yii::$app->request->get('id')?>" />
								<div class="row">
									<div class="col-md-6">
										<fieldset class="form-group">
												<input name="email" readonly="true" value="<?php echo $model->email;?>" class="form-control" type="text">
											</label></label>
										</fieldset>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<?= $form->field($changeAgencyUserPassModel,'newpass',['inputOptions'=>[
											'placeholder'=>'New Password'
										]])->passwordInput(['class' => 'form-control']) ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<?= $form->field($changeAgencyUserPassModel,'repeatnewpass',['inputOptions'=>[
											'placeholder'=>'Repeat New Password'
										]])->passwordInput(['class' => 'form-control']) ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="notify-checkbox">
											<?= $form->field($changeAgencyUserPassModel, 'emailNotification')->checkbox(); ?>
										</div>
									</div>
								</div>

								<?= $form->field($changeAgencyUserPassModel, 'user')->hiddenInput(['value' => $model->id])->label(false)?>

									<div class="form-group">
										<div class="col-lg-offset-2 col-lg-11">
											<?= Html::submitButton('Change password',[
												'class'=>'btn btn-primary'
											]) ?>
										</div>
									</div>
								<?php ActiveForm::end(); ?>

							</div>
						</div>
					</div>
			</div>
	</div>
	<!-- Change password for a particular user -->

</div>
