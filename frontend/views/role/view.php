<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Packages */

$this->title = 'View Role';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss("
  .capitalised_text {
    text-transform: capitalize;
  }
");
?>

<div class="ks-page-header">
            <section class="ks-title">
                <h3><?= Html::encode($this->title) ?></h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
                                      <div class="packages-create">
                                        <table class="table table-striped table-bordered detail-view">
                                          <tr>
                                            <th>Name</th>
                                            <td><?php
                                              $name = explode('_', $name);
                                              echo $name[1];
                                            ?></td>                                        
                                          </tr>
                                          <tr>
                                            <th>Permissions</th>
                                            <td>
                                              <ul class="capitalised_text">
                                              <?php
                                                foreach ($permissions as $permission) {
                                                  echo "<li>$permission</li>";
                                                }
                                              ?>
                                              </ul>
                                            </td>                                        
                                          </tr>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
