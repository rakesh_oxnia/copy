<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Packages */

$this->title = 'Create Role';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss("
  .capitalised_text{
    text-transform: capitalize;
  }
");
?>

<div class="ks-page-header">
            <section class="ks-title">
                <h3><?= Html::encode($this->title) ?></h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
                                      <div class="packages-create">
                                          <?php 
                                          $form = ActiveForm::begin([
                                                'id' => 'login-form',
                                                'options' => ['class' => 'form-horizontal'],
                                            ]) 
                                          ?>

                                          <?= Html::label('Name', ['class' => 'label name']) ?>
                                          <?= Html::textInput('name','',array('class'=>'form-control')) ?>
                                          <br/>
                                           <?= Html::label('Select Permissions', 'permissions', ['class' => 'label permissions']) ?>
                                           <?= Html::checkboxList('permissions', null, $permissions, ['class' => 'capitalised_text', 'separator' => '<br/>']) ?>
                                            <div class="form-group">
                                                <div class="col-lg-offset-1 col-lg-11">
                                                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
                                                </div>
                                            </div>
                                          <?php ActiveForm::end() ?>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
