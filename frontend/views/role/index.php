<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ChatstatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Roles';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--New Design----->
<div class="ks-page-header">
            <section class="ks-title">
                <h3><?= Html::encode($this->title) ?></h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
                                      <?php Pjax::begin(['id' => 'pjax_widget']); ?>

                                        <p>
                                            <?= Html::a('Create Role', ['create'], ['class' => 'btn btn-success']) ?>
                                        </p>

                                        <?= GridView::widget ([
                                            'dataProvider' => $dataProvider,
                                            'id' => 'pjax_widget',
                                            'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                                            'columns' => [
                                                  ['class' => 'yii\grid\SerialColumn'],        [
                                                  'attribute' => 'name',
                                                  'value' => function ($data, $key, $index, $column) {
                                                    $name = explode('_', $data['name']);
                                                    return $name[1];
                                                  },
                                                  'format' => 'raw',
                                                ],
                                                [ 'class' => 'yii\grid\ActionColumn',
                                                  'header'=>'Action',
                                                  'headerOptions' => ['class' => 'action-column'],
                                                  'template'=>'{view}{update}{delete}',
                                                  'urlCreator' => function ($action, $model, $key, $index) {
                                                        return Url::to([$action, 'name' => $key]);
                                                  },
                                                  'buttons' => [

                                                      'view' => function ($url, $model) {
                                                                    return Html::a('<span class="ks-icon la la-eye"></span>',
                                                                        $url);
                                                                },
                                                      'update' => function ($url, $model) {
                                                                    return Html::a('<span class="ks-icon la la-pencil"></span>',
                                                                        $url);
                                                                },
                                                      'delete' => function ($url, $model) {
                                                          return Html::a('<span class="ks-icon la la-trash"></span>',
                                                              $url, [
                                                                    'data-confirm' => 'Are You sure you want to delete this Role ?',
                                                                    'data-method' => 'post'
                                                                  ]);
                                                      },
                                                  ],
                                                ],
                                            ],
                                        ]); ?>

                                     <?php Pjax::end(); ?>
                                   </div>
                              </div>

                          </div>

                      </div>
                  </div>
              </div>
          </div>
      </div>
<!--New Design----->
<?php
$this->registerCss("
  .ks-icon{
    font-size: 20px !important;
    padding-left: 7px;
  }
  .ks-icon:hover{
    color: #f00;
  }
");
$this->registerJs('
    $.pjax.reload({container:"#pjax_widget"});
    jQuery("document").ready(function(){
      jQuery("#dateto,#datefrom").datepicker( {dateFormat: "yy-mm-dd",changeMonth:true,changeYear:true} );
    });
');
?>
