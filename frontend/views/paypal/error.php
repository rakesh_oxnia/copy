<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>	
	<div class="body-content">
		<div class="main-box-body clearfix">
				<div class="alert alert-danger">
					<b>Error!!</b> Some Problem has occured please try later.
				</div>
				
				
				<div class="col-xs-12">
					<p class="social-text">
						<a href="<?php echo Url::to(['/site/index']);?>">
							Return to Dashboard
						</a>
					</p>
				</div>
		</div>
	</div>
				 