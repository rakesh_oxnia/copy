<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>
<div class="ks-page-header">
          <section class="ks-title">
              <h3>Invoices</h3>
          </section>
      </div>
      <div class="ks-page-content">
        <div class="ks-page-content-body">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                  <div class="card-block ks-datatable">
                                      <table id="ks-datatable" class="table table-bordered table-responsive" style="width:100%" width="100%">
                                          <thead>
                                             <tr>
                                              <th>Order</th>
                       												<th>Email</th>
                       												<!--th>agents</th-->
                       												<th>Advanced Payment</th>
                       												<th>Transaction Id</th>
                                              <th>Allocated Leads</th>
                       												<th>Extra leads Consumed</th>
                       												<th>Extra Lead Amount ($)</th>
                       												<th>Advanced Payment Status</th>
                       												<th>Transaction Currency</th>
                       												<!-- <th>Invoice Status</th> -->
                                              <th>Pending Invoice</th>
                       												<th>Automatic Charge Status</th>
                       												<th>Total Charge ($)</th>
                                              <th>Past Lead Access</th>
                       												<th>Package Activation Date</th>
                       												<th>Created At</th>
                       												<th>Billing Date</th>
                       												<!-- <th>Payment Date</th> -->
                       												<th>Download Receipt</th>
                                          </thead>
                                          <tbody>
                                            <?php
                                            foreach($invoices as $invoice){
                                              ?>
                                            <tr>
                                              <td><?php echo $invoice->id;?></td>
                                              <td><?php echo $invoice->userModel->email;?></td>
                                          <td><?php echo $invoice->advanced_payment;?></td>
                                              <td><?php echo $invoice->stripe_payment_id;?></td>
                                              <td><?php echo $invoice->allocated_lead;?></td>
                                              <td><?php echo $invoice->extra_lead_consumed;?></td>
                                              <td><?php echo $invoice->extra_per_lead_amount;?></td>
                                              <td><?php if($invoice->advance_payment_status==1){
                                                echo 'Active';
                                              }else{
                                                echo 'Inactive';
                                              }?></td>
                                              <td><?php echo $invoice->transaction_currency;?></td>
                                              <!-- <td><?php  if($invoice->invoice_status==1){
                                                echo "Paid";
                                              }else{
                                                echo "Pending";
                                              }?></td> -->
                                              <td><?php  if($invoice->pending_invoice==0){
                                                echo "No";
                                              }else{
                                                echo "Yes";
                                              }?></td>
                                              <td><?php if($invoice->automatic_charge_status==1){
                                                echo "Automatic";
                                              }else{
                                                echo "Billing";
                                              }?></td>
                                              <td><?php echo $invoice->total_amount ?></td>
                                              <td><?php  if($invoice->subscriptionDetail->show_previous_lead==1){
                                                echo "Active";
                                              }else{
                                                echo "Inactive";
                                              }?></td>
                                              <td><?php echo date('Y-m-d',  $invoice->subscriptionDetail->created_at);?></td>
                                              <td><?php echo date('Y-m-d H:i:s',  $invoice->created_at);?></td>
                                              <!-- <td><?php  echo date('Y-m-d H:i:s',  $invoice->created_at);?></td> -->
                                              <td><?php echo date('Y-m-d H:i:s',  $invoice->billing_date);?></td>
                                              <!-- <td><?php if($invoice->payment_date==0){
                                                echo "Not-Paid";
                                              }else{
                                                echo date('Y-m-d H:i:s',  $invoice->payment_date);
                                              } ?></td> -->
                                              <td>
                                                <?php
                                                  echo '<a href="'.Url::to(['invoice/print-pdf', 'id' =>$invoice->id]).'" target="_blank"><button type="button"  class="btn btn-default btn-sm"><i class="fa fa-download"></i></button></a>';
                                              ?>
                                              </td>
                                            </tr>
                                            <?php
                                          }
                                            ?>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>




      <?php
      $this->registerJs("

      ");
      ?>
