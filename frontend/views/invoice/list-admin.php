<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>
<div class="ks-page-header">
          <section class="ks-title">
              <h3>Invoices</h3>
          </section>
      </div>
      <div class="ks-page-content">
        <div class="ks-page-content-body">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                  <div class="card-block ks-datatable">
                                      <table id="ks-datatable" class="table table-bordered table-responsive">
                                          <thead>
                                             <tr>
                                              <th>Order</th>
                       												<th>Email</th>
                       												<!--th>agents</th-->
                       												<th>Advanced Payment</th>
                       												<th>Transaction Id</th>
                                              <th>Allocated Leads</th>
                       												<th>Extra leads Consumed</th>
                       												<th>Extra Lead Amount ($)</th>
                       												<th>Advanced Payment Status</th>
                       												<th>Transaction Currency</th>
                       												<!-- <th>Invoice Status</th> -->
                                              <th>Pending Invoice</th>
                       												<th>Automatic Charge Status</th>
                       												<th>Total Charge ($)</th>
                                              <th>Past Lead Access</th>
                       												<th>Package Activation Date</th>
                                              <th>Created Date</th>
                       												<th>Billing Date</th>
                       												<!-- <th>Payment Date</th> -->
                       												<th>Download Receipt</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            <?php
                                            foreach($invoices as $invoice){
                                              ?>
                                            <tr>
                                              <td><?php echo $invoice->id;?></td>
                                              <td><?php echo $invoice->userModel->email;?></td>
                                              <td><?php echo $invoice->advanced_payment;?></td>
                                              <td><?php echo $invoice->stripe_payment_id;?></td>
                                              <td><?php echo $invoice->allocated_lead;?></td>
                                              <td><?php echo $invoice->extra_lead_consumed;?></td>
                                              <td><?php echo $invoice->extra_per_lead_amount;?></td>
                                              <td><?php if($invoice->advance_payment_status==1){
                                                echo 'Active';
                                              }else{
                                                echo 'Inactive';
                                              }?></td>
                                              <td><?php echo $invoice->transaction_currency;?></td>
                                              <!-- <td><?php  if($invoice->invoice_status==1){
                                                echo "Paid";
                                              }else{
                                                echo "Pending";
                                              }?></td> -->
                                              <td><?php  if($invoice->pending_invoice==0){
                                                echo "No";
                                              }else{
                                                echo "Yes";
                                              }?></td>
                                              <td><?php if($invoice->automatic_charge_status==1){
                                                echo "Automatic";
                                              }else{
                                                echo "Billing";
                                              }?></td>
                                              <td><?php echo $invoice->total_amount ?></td>
                                              <td><?php
                                              if($invoice->subscriptionDetail['show_previous_lead']==1){
                                                echo "Active";
                                              }else{
                                                echo "Inactive";
                                              }?></td>
                                              <td><?php echo date('Y-m-d',  $invoice->subscriptionDetail['created_at']);?></td>
                                              <td><?php echo date('Y-m-d H:i:s',  $invoice->created_at);?></td>
                                              <td><?php echo date('Y-m-d H:i:s',  $invoice->billing_date);?></td>
                                              <!-- <td><?php if($invoice->payment_date==0){
                                                echo "Not-Paid";
                                              }else{
                                                echo date('Y-m-d H:i:s',  $invoice->payment_date);
                                              } ?></td> -->
                                              <td><?php
                                              if($invoice->automatic_charge_status==1){
                                                  echo '<a href="'.Url::to(['invoice/print-pdf', 'id' =>$invoice->id]).'" target="_blank"><button type="button"  class="btn btn-default btn-sm"><i class="fa fa-download"></i></button></a>';
                                              }else{
                                              /*echo  '<div class="row">
                                  <button type="button" data-toggle="modal" data-target="#update_invoice" data-id="'.$invoice->id.'" class="btn btn-default btn-sm update_invoice">update</button>
                                                <a href="'.Url::to(['invoice/print-pdf', 'id' =>$invoice->id]).'" target="_blank"><button type="button"  class="btn btn-default btn-sm"><i class="fa fa-download"></i></button></a>
                                                </div>';*/
                                                echo  '<a href="'.Url::to(['invoice/print-pdf', 'id' =>$invoice->id]).'" target="_blank"><button type="button"  class="btn btn-default btn-sm"><i class="fa fa-download"></i></button></a>';
                                              }
                                              ?></td>
                                            </tr>
                                            <?php
                                          }
                                            ?>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>

                  <!---Model--->
                  <div id="update_invoice" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                        <h4 class="modal-title">Change Invoice Status</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                          <label>Status:</label>
                        <select id="paid_status" class="form-control" >
                            <option value="3">-Select Status</option>
                          <option value="1" >Paid</option>
                          <option value="0" >Pending</option>
                        </select>
                        <br />
                        <label>Payment Date</label>
                         <input type="text" placeholder="Paid ON" class="form-control" id="paid_on">
                        </div>
                        <input type="hidden" id="invoice_id" value="">
                        <div class="modal-footer">
                        <button type="button" class="btn btn-primary change_invoice_status">Change Status</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                      </div>
                  </div>
                  <!---Model--->

      <?php
      $this->registerJs("

       jQuery('#paid_on').datepicker( {
         dateFormat: 'yy-mm-dd',
         changeMonth:true,
         changeYear:true}
      );

      $('.update_invoice').off('click').on('click', function(e){
        e.preventDefault();
        $('#invoice_id').val($(this).data('id'));
      });

      // $('.pdf_invoice').off('click').on('click',function(e){
      //          e.preventDefault();
      //          var invoice_id = $(this).data('id');
      // });

      $('.change_invoice_status').off('click').on('click', function(e){
        e.preventDefault();
        var invoice_id = $('#invoice_id').val();
        if(invoice_id==''){
          swal('Internal Error Please Try again latter');
          return false;
        }
        var paid_status =  $('#paid_status').val();
        var paid_on =  $('#paid_on').val();
        if(paid_status==''){
          swal('Please choose a Status');
          return false
        }
        if(paid_status=='3'){
                      swal('Please choose a Status');
                      return false
        }
        if(paid_on==''){
          swal('Please choose a Payment Date');
          return false
        }
        if(paid_status==0){
           paid_on = 0;
         }
        $.ajax({
          type: 'POST',
          url: '".Url::to(['invoice/update-invoice-status'])."',
          data: {paid_status:paid_status,
                  paid_on:paid_on
                ,invoice_id:invoice_id},
          success: function(response){
            response = JSON.parse(response)
            if(response.success == true){
              console.log(response);
              location.reload();
            }
            if(response.success == false){
              swal(response.data, 'error');
              return false;
            }
          },
          error: function(data){
            console.log(data);
            swal('Some problem occured!', 'Please try again later after sometime.', 'error');
            return false;
          },
        });
      });
      ");
      ?>
