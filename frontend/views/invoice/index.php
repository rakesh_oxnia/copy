<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/datatables-net/media/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/datatables-net/extensions/responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/libs/datatables-net/datatables.min.css"> -->
  <div class="ks-page-header">
            <section class="ks-title">
                <h3>Pay As You Go Invoice</h3>
            </section>
        </div>

        <div class="ks-page-content">
          <div class="ks-page-content-body">
              <div class="ks-nav-body-wrapper">
                  <div class="container-fluid">
                      <div class="row">
                          <div class="col-lg-12 ks-panels-column-section">
                              <div class="card">
                                  <div class="card-block">
                                    <div class="card-block ks-datatable">
                                        <table id="ks-datatable" class="table table-bordered table-responsive">
                                            <thead>
                                              <tr>
                                                  <th>Order</th>
                                                  <th>Email</th>
                                                  <th>Chat Id</th>
                                                  <th>Status</th>
                                                  <th>Amount</th>
                                                  <th>Currency</th>
                                                  <th>Transaction</th>
                                                  <th>Issue Date</th>
                                                  <th>Invoice</th>
                                                  <th>Download Receipt</th>
                                              </tr>
                                            </thead>
                                            <tbody>
                                              <?php
                                              foreach($invoices as $invoice){?>
                                									<tr>
                                                    <td>
                                                      <?php echo $invoice['id'];?>
                                                    </td>
                                                    <td>
                                                      <?php echo $invoice->userModel->email;?>
                                                    </td>
                                                    <td>
                                                      <?php
                                                      $all_chat_id =  explode(',', $invoice['chat_id']);
                                                      echo implode(', ',$all_chat_id);
                                                      ?>
                                                    </td>
                                										<td>
                                											<span class="badge ks-circle badge-success ks-text-success">Paid</span>
                                										</td>
                                                    <td><?php echo $invoice['amount'];?></td>
                                										<td><?php echo $invoice['currency'];?></td>
                                										<td><?php echo $invoice['transaction_id'];?></td>
                                										<td><?php echo date('M d, Y', strtotime($invoice['payment_date']));?></td>
                                										<td><?php echo $invoice['invoice_id'];?></td>
                                                    <td>
                                                      <?php
                                                        echo '<a href="'.Url::to(['invoice/print-pay-as-you-go-pdf', 'id' =>$invoice->id]).'"  target="_blank"><button type="button"  class="btn btn-default btn-sm"><i class="fa fa-download"></i></button></a>';
                                                    ?>
                                                    </td>
                                									</tr>
                                              <?php }?>
                                            </tbody>
                                          </table>
                                        </div>
                                      </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
