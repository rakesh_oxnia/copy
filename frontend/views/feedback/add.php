<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>	


 <div class="ks-page-header">
            <section class="ks-title">
                <h3>Add Feedback</h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
                                        <h5 class="card-title">Add</h5>
                                        <?php $form = ActiveForm::begin(); ?>
										
										<?php if($success){?>
												<div class="alert alert-success">
													<?php echo $success;?>
												</div>
										<?php }?>
										<?php if( isset($_POST['save']) && $success == '' ) : ?>
											<div class="alert alert-danger">
												<?= $form->errorSummary($model); ?>
											</div>	
										<?php endif;?> 
										
                                           <?= $form->field($model, 'feedback')->textArea(['maxlength' => true, 'class' => 'form-control']) ?>
											
											<div class="form-group">
													<input type="submit" name="save" value="Save" class="btn btn-primary">
											</div>
									
                                        <?php ActiveForm::end(); ?>
                                    </div>
                                </div>
                                
                            </div>
                             
                        </div>
                    </div>
                </div>
            </div>
        </div>