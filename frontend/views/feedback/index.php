<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii2assets\printthis\PrintThis;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/misc/faq.min.css">

		<div class="ks-page-header">
            <section class="ks-title">
                <h3>Feedback</h3>
				<div class="pull-right">
					<a  href="<?php echo Url::to(['/feedback/add']);?>">
						<input type="button" value="Add" class="btn btn-primary">
					</a>

					<?php
						echo PrintThis::widget([
							'htmlOptions' => [
								'id' => 'PrintThis',
								'btnClass' => 'btn btn-info',
								'btnId' => 'btnPrintThis',
								'btnText' => 'Print',
								'btnIcon' => 'fa fa-print'
							],
							'options' => [
								'debug' => false,
								'importCSS' => true,
								'importStyle' => false,
								'loadCSS' => "cube/css/bootstrap/bootstrap.min.css",
								'pageTitle' => "",
								'removeInline' => false,
								'printDelay' => 333,
								'header' => null,
								'formValues' => true,
								'canvas' => true,
							]
						]);
						?>
				</div>
            </section>
        </div>

        <div class="ks-page-content" id="PrintThis">
            <div class="ks-page-content-body ks-body-wrap">
                <div class="ks-body-wrap-container">
                    <div class="container-fluid ks-faq-container">
                        <div class="ks-faq-header">
                            <h3>Feedback</h3>

                        </div>

                        <div class="ks-faq-body">
                            <div class="row">
                                <div class="col-lg-12">

                                    <ul class="ks-questions-container">

									<?php foreach($feedbacks as $feedback){?>

										 <li>
											<a class="ks-question-block">Given By - <?php echo isset($users[$feedback['given_by']]) ? $users[$feedback['given_by']] : 'NA';?></a>
											<div class="ks-answer-block">
												<?php echo $feedback['feedback'];?>
											</div>
										</li>

                                    <?php }?>
                                    </ul>


                                </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
