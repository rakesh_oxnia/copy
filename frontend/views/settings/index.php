<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii2assets\printthis\PrintThis;
use ruskid\stripe\StripeCheckoutCustom;
use ruskid\stripe\StripeForm;
use common\models\User;
use app\models\Notification;

$stripeUserCurrencies = User::$stripeUserCurrencies;
?>
<style>
label {
    display: block;
}
.has-error .checkbox, .has-error .checkbox-inline, .has-error .control-label, .has-error .help-block, .has-error .radio, .has-error .radio-inline, .has-error.checkbox label, .has-error.checkbox-inline label, .has-error.radio label, .has-error.radio-inline label {
    color: #ff2a00;
}
</style>



          <!-- BEGIN DASHBOARD HEADER -->
<div class="ks-page-header"> <!-- add "ks-header-with-addon" class if you need an addon -->
    <section class="ks-title">
        <h3>Settings</h3>

        <div class="ks-controls">
            <nav class="breadcrumb ks-default">
                <a class="breadcrumb-item ks-breadcrumb-icon" href="">
                    <span class="la la-home ks-icon"></span>
                </a>
                <span class="breadcrumb-item active" href="#">Settings</span>
            </nav>
        </div>
    </section>

</div>
<!-- END DASHBOARD HEADER -->

<!-- BEGIN DASHBOARD CONTENT -->
<div class="ks-page-content">
    <div class="ks-page-content-body">
    <?php if($success){?>
      <div class="alert alert-success">
        <b>Success!</b> <?php echo $success;?>
      </div>
    <?php }?>
    <?php if(Yii::$app->session->hasFlash('error')):?>
      <div class="alert alert-danger">
        <b>Failed!</b> <?php echo Yii::$app->session->getFlash('error'); ?>
      </div>
   <?php endif; ?>
        <div class="container-fluid ks-rows-section">
            <div class="row">

                <div class="col-xl-6">
                  <?php
                    if(in_array(Yii::$app->user->identity->role, [User::ROLE_USER, User::ROLE_FRANCHISE]) 
                      && empty(Yii::$app->user->identity->parent)
                     ){
                  ?>
                    <div class="row">
                        <div class="col-xl-12 ks-draggable-column">
                            <div id="ks-bar-chart-panel" class="card panel" data-dashboard-widget>
                                <h5 class="card-header">
                                    Business Overview
                                </h5>
                <div class="card">
                  <div class="card-block">
                     <?php $form = ActiveForm::begin(); ?>


                     <fieldset class="form-group">
                       <label class="form-control-label">Business Name:<label>
                       <input name="business_name" value="<?php echo Yii::$app->user->identity->business_name;?>" class="form-control" type="text">
                     </label></label></fieldset>

                     <fieldset class="form-group">
                       <label class="form-control-label">Website Url:<label>
                       <input name="website_url" value="<?php echo Yii::$app->user->identity->website_url;?>" class="form-control" type="text">
                     </label></label>
                   </fieldset>

                   <!--Chat purpose--->
                   <fieldset class="form-group">
                    <label class="form-control-label">Chat Purpose:<label>
                      <label class="ks-checkbox-slider">
                       <input type="checkbox" name="chat_purpose_sales" value="1" <?php if(Yii::$app->user->identity->chat_purpose_sales){ echo "checked";}?>>
                       <span class="ks-indicator"></span>
                     </label>
                     Lead generation

                      <label class="ks-checkbox-slider">
                       <input type="checkbox" name="chat_purpose_support" value="1" <?php if(Yii::$app->user->identity->chat_purpose_support){ echo "checked";}?>>
                       <span class="ks-indicator"></span>
                     </label>
                     Support/Service

                     <label class="ks-checkbox-slider">
                      <input type="checkbox" name="chat_purpose_e_commerce" value="1" <?php if(Yii::$app->user->identity->chat_purpose_e_commerce){ echo "checked";}?>>
                      <span class="ks-indicator"></span>
                    </label>
                    E-commerce
                   </fieldset>
                   <!--Chat purpose--->

                        <fieldset class="form-group">
                          <label class="form-control-label">Overview of services:<label>
                          <textarea name="overview" class="form-control"><?php echo $userData['overview'];?></textarea>
                        </fieldset>

                        <fieldset class="form-group">
                          <label class="form-control-label">Industry<label>
                          <select name="industry" class="form-control">
                            <option value="">Choose Industry</option>
                            <?php if( !empty( $categories ) ){?>
                                <?php foreach($categories as $country){?>
                                    <option <?php if($userData['industry'] == $country['id']){echo "selected";}?> value="<?php echo $country['id'];?>"><?php echo $country['name'];?></option>
                                <?php }?>
                            <?php }?>

                          </select>
                        </fieldset>


                        <fieldset class="form-group">
                          <label class="form-control-label">Employees<label>
                          <select name="number_of_employees" class="form-control">
                            <option value="">Number of Employees</option>
                            <option <?php if($userData['number_of_employees'] == '1'){echo "selected";}?> value="1">1 - 10 Employees</option>
                            <option <?php if($userData['number_of_employees'] == '5'){echo "selected";}?> value="5">10 - 100 Employees</option>
                            <option <?php if($userData['number_of_employees'] == '10'){echo "selected";}?> value="10">100 plus Employees</option>
                          </select>
                        </fieldset>

                        <fieldset class="form-group">
                          <label class="form-control-label">Country<label>
                          <select name="country" class="form-control">
                            <option value="0">Country</option>
                            <?php if( !empty( $countries ) ){?>
                                <?php foreach($countries as $country){?>
                                    <option <?php if($userData['country'] == $country['id']){echo "selected";}?> value="<?php echo $country['id'];?>"><?php echo $country['country_name'];?></option>
                                <?php }?>
                            <?php }?>
                          </select>
                        </fieldset>

                        <fieldset class="form-group">
                          <label class="form-control-label">Audience<label>
                           <label class="ks-checkbox-slider">
                            <input type="checkbox" <?php if(Yii::$app->user->identity->b2b){echo "checked";}?> name="b2b" value="1">
                            <span class="ks-indicator"></span>
                          </label>
                          B2B

                           <label class="ks-checkbox-slider">
                            <input type="checkbox" <?php if(Yii::$app->user->identity->b2c){echo "checked";}?> name="b2c" value="1">
                            <span class="ks-indicator"></span>
                          </label>
                          B2C

                          <label class="ks-checkbox-slider">
                            <input type="checkbox" <?php if(Yii::$app->user->identity->internal_use){echo "checked";}?> name="internal_use" value="1">
                            <span class="ks-indicator"></span>
                          </label>
                          Internal Use

                          </fieldset>

                        <fieldset class="form-group">
                          <input type="submit" class="btn btn-primary" name="add_overview" value="Submit">
                        </fieldset>
                      <?php ActiveForm::end(); ?>
                  </div>
                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                  }
                    ?>
                    <?php
                      if(Yii::$app->user->identity->role == User::ROLE_ADMIN){
                    ?>
                    <div class="row">
                          <div class="col-xl-12 ks-draggable-column">
                              <div id="ks-bar-chart-panel" class="card panel">
                                  <h5 class="card-header">
                                    Global Pay as you Go
                                  </h5>
                                  <div class="card">
                                    <div class="card-block">
                                      <?php $form = ActiveForm::begin([
                                        'id'=>'global_per_lead_setting'
                                      ]); ?>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label class="form-control-label"><b>Global Pay Per Lead</b><label>
                                          <input name="global_per_lead_cost" id="global_per_lead_cost" value="<?php echo $adminSetting->global_per_lead_cost;?>" class="form-control" type="text" required maxlength="6">
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label class="form-control-label"><b>Global Currency</b><label>
                                            <select name="global_per_lead_cost_currency" id="custom_currency" class="form-control" required>
                                                <option value=""></option>
                                                <?php
                                                foreach ($stripeUserCurrencies as $currency => $arr_info) {
                                                ?>
                                                  <option value="<?php echo $currency;?>">
                                                    <?php echo $arr_info["name"];?>
                                                  </option>
                                                <?php
                                                }
                                                ?>
                                                <!-- <option value="AUD">Australian Dollar</option>
                                                <option value="BRL">Brazilian Real</option>
                                                <option value="CAD">Canadian Dollar</option>
                                                <option value="CZK">Czech Koruna</option>
                                                <option value="DKK">Danish Krone</option>
                                                <option value="EUR">Euro</option>
                                                <option value="HKD">Hong Kong Dollar</option>
                                                <option value="HUF">Hungarian Forint </option>
                                                <option value="ILS">Israeli New Sheqel</option> -->
                                                <!-- <option value="JPY">Japanese Yen</option> -->
                                                <!-- <option value="MYR">Malaysian Ringgit</option>
                                                <option value="MXN">Mexican Peso</option>
                                                <option value="NOK">Norwegian Krone</option>
                                                <option value="NZD">New Zealand Dollar</option>
                                                <option value="PHP">Philippine Peso</option>
                                                <option value="PLN">Polish Zloty</option>
                                                <option value="GBP">Pound Sterling</option>
                                                <option value="SGD">Singapore Dollar</option>
                                                <option value="SEK">Swedish Krona</option>
                                                <option value="CHF">Swiss Franc</option>
                                                <option value="TWD">Taiwan New Dollar</option>
                                                <option value="THB">Thai Baht</option>
                                                <option value="TRY">Turkish Lira</option>
                                                <option value="USD">U.S. Dollar</option> -->
                                            </select>
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <fieldset class="form-group">
                                             <input type="submit" class="btn btn-primary" name="add_pay_as_go" id="add_pay_as_go" value="Save">
                                          </fieldset>
                                        </div>
                                      </div>
                                      <?php ActiveForm::end(); ?>
                                    </div>
                                  </div>
                              </div>
                          </div>

                      </div>
                    <?php
                      }
                    ?>
                    <?php
                    if(in_array(Yii::$app->user->identity->role, [User::ROLE_USER, User::ROLE_FRANCHISE]) 
                      && empty(Yii::$app->user->identity->parent)
                     ){

                    ?>
                    <div class="row">
                    <div class="col-xl-12 ks-draggable-column">
                    <div id="ks-bar-chart-panel" class="card panel">
                    <h5 class="card-header">
                    Update New Credit Card Details
                    </h5>
                    <div class="card">
                    <div class="card-block">
                    <?php
                    if(isset($_GET['data'])){
                      $url = Url::to(['invoice/savecard','data'=>'pay_as_you_go']);
                    }else{
                        $url = Url::to(['invoice/savecard']);
                    }
                    $form = StripeForm::begin([
                    'action' => $url,
                    'tokenInputName' => 'stripeToken',
                    'errorContainerId' => 'payment-errors',
                    'brandContainerId' => 'cc-brand',
                    'errorClass' => 'has-error',
                    'applyJqueryPaymentFormat' => true,
                    'applyJqueryPaymentValidation' => true,
                    'options' => ['autocomplete' => 'on']
                    ]);
                    ?>

                    <div class="form-group">
                    <label for="number" class="control-label">Card number</label>
                    <span id="cc-brand"></span>
                    <?= $form->numberInput() ?>
                    </div>

                    <div class="form-group">
                    <label for="cvc" class="control-label">CVC</label>
                    <?= $form->cvcInput() ?>
                    </div>

                    <!-- OR in two separate inputs. -->
                    <div class="form-group">
                    <label for="exp-month" class="control-label">Expiry Date</label>
                    <div class="card_month">Month <?= $form->monthInput() ?></div>
                    <div class="card_year">Year <?= $form->yearInput() ?></div>
                    </div>

                    <!-- <div class="form-group">
                    <label for="exp-year" class="control-label">Year</label>
                    <?= $form->yearInput() ?>
                    </div> -->

                    <div id="payment-errors"></div>

                    <div class="card_sbm">
                    <?= Html::submitButton('Submit'); ?>
                    </div>

                    <?php StripeForm::end(); ?>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    <?php
                    }
                    ?>
<style>
.card_month {
  width: 48% !important;
  float:left;
}
.card_year {
  width: 48% !important;
  float:right;
}
.card_sbm {
  float:left;
  margin-top: 10px;
}

<?PHP if(Yii::$app->user->identity->role != User::ROLE_ASSIST) { ?>
.login_wrapper {
  margin-top: 35px;
}
<?PHP } ?>
</style>
          <div class="row login_wrapper">
                        <div class="col-xl-12 ks-draggable-column">
                            <div id="ks-bar-chart-panel" class="card panel">
                                <h5 class="card-header">
                                   Login Details
                                </h5>
                <div class="card">
                  <div class="card-block">

                    <?php $form = ActiveForm::begin([
                      'id'=>'changepassword-form',
                      'options'=>['class'=>'form-horizontal'],
                      'fieldConfig'=>[
                        'template'=>"{label}\n<div class=\"col-lg-12\">
                              {input}</div>\n<div class=\"col-lg-12\">
                              {error}</div>",
                        'labelOptions'=>['class'=>'col-lg-12 control-label'],
                      ],
                    ]); ?>

                    <fieldset class="form-group">
                      <label class="form-control-label">Email:<label>
                      <input name="email" readonly="true" value="<?php echo Yii::$app->user->identity->email;?>" class="form-control" type="text">
                    </label></label></fieldset>

                      <?= $form->field($model,'oldpass',['inputOptions'=>[
                        'placeholder'=>'Old Password'
                      ]])->passwordInput() ?>

                      <?= $form->field($model,'newpass',['inputOptions'=>[
                        'placeholder'=>'New Password'
                      ]])->passwordInput() ?>

                      <?= $form->field($model,'repeatnewpass',['inputOptions'=>[
                        'placeholder'=>'Repeat New Password'
                      ]])->passwordInput() ?>

                      <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-11">
                          <?= Html::submitButton('Change password',[
                            'class'=>'btn btn-primary'
                          ]) ?>
                        </div>
                      </div>
                    <?php ActiveForm::end(); ?>

                  </div>
                </div>
                            </div>
                        </div>
                    </div>

                <?php
                  if(Yii::$app->user->identity->role == User::ROLE_ADMIN || Yii::$app->user->identity->role == User::ROLE_SUBADMIN){
                ?>
                  <div class="row">
                        <div class="col-xl-12 ks-draggable-column">
                            <div id="ks-bar-chart-panel" class="card panel">
                                <h5 class="card-header">
                                   Iframe
                                </h5>
                                <div class="card">
                                  <div class="card-block">
                                    <fieldset class="form-group">
                                      <label class="form-control-label"><b>Iframe Tag</b><label>
                                      <textarea id="iframeTag" name="iframeTag" class="form-control" rows="5" cols="50"><?=$iframeText?></textarea>
                                    </fieldset>

                                    <fieldset class="form-group">
                                      <?= Html::submitButton('Save',[
                                        'class'=>'btn btn-primary save-iframe-btn'
                                      ]) ?>
                                    </fieldset>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                          <div class="col-xl-12 ks-draggable-column">
                              <div id="ks-bar-chart-panel" class="card panel">
                                  <h5 class="card-header">
                                     Login page background color
                                  </h5>
                                  <div class="card">
                                    <div class="card-block">
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <label class="form-control-label"><b>Color code</b><label>
                                          <input class='form-control' placeholder="#000000" id='loginBgColor' name='loginBgColor' value="<?=$loginColorCode?>"  />
                                        </div>
                                      </div>
                                      <div class='row'>
                                        <div class='col-md-6'>
                                          <fieldset class="form-group">
                                            <?= Html::submitButton('Save',[
                                              'class'=>'btn btn-primary save-bg-color'
                                            ]) ?>
                                          </fieldset>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                <?php
                  }
                ?>


                </div>

         <div class="col-xl-6">

                    <div class="row">
                        <div class="col-xl-12 ks-draggable-column">
                            <div id="ks-bar-chart-panel" class="card panel" data-dashboard-widget>
                                <h5 class="card-header">
                                    Chat Window Settings
                                </h5>
                                <div class="card-block">
                                 <?php $form = ActiveForm::begin(); ?>

                      <fieldset class="form-group">
                        <label class="form-control-label">Website Javascript<label>
<textarea name="website_javascript" class="form-control" rows="5" cols="50">
<?php
   if(Yii::$app->user->identity->tracker_id == NULL){
?>
<!-- Start of Chatcode -->
<script type="text/javascript">
  var __lc = {};
  __lc.license = 6354551;
  window.__lc.ga_version;
  window.__lc.chat_between_groups = false;
  (function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
  })();
</script>
  <!-- End of Chatcode -->
  <?php
  }else{
  ?>
  <!-- Start of Chatcode -->
  <script type="text/javascript" src="https://chat-application.com/embed/index.php?tracker_id=<?=Yii::$app->user->identity->tracker_id?>"></script>
  <!-- End of Chatcode -->
  <?php
  }
  ?>
</textarea>
                      </fieldset>


                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                      if(Yii::$app->user->identity->role != User::ROLE_ADMIN &&  Yii::$app->user->identity->role != User::ROLE_SUBADMIN){
                    ?>
                    <!---=============notification==================--->
                    <div class="row" id="card-detail">
                        <div class="col-xl-12 ks-draggable-column">
                            <div id="ks-bar-chart-panel" class="card panel" data-dashboard-widget>
                                <h5 class="card-header">
                                  Notification Settings
                                  <?php
                                  $notification = Notification::find()->where([
                                      'user_id'=>Yii::$app->user->identity->id
                                  ])->one();
                                  if(!empty($notification)){
                                    if($notification->different_email==1){
                                      $different_email = $notification->different_email;
                                      $leads_emails = explode(", ",$notification->notify_lead_email);
                                      $service_emails = explode(", ",$notification->notify_chat_email);
                                      $notify_lead = $notification->notify_lead;
                                      $notify_chat = $notification->notify_chat;
                                    }else{
                                      $lead_service_emails = explode(", ",$notification->notify_email);
                                      $notify_lead = $notification->notify_lead;
                                      $notify_chat = $notification->notify_chat;
                                      $different_email = 0;
                                      //print_r($notify_lead);exit;
                                    }
                                  }else{
                                    $notify_lead= 1;
                                    $notify_chat= 1;
                                    $different_email = 0;
                                  }
                                  ?>
                              </h5>
                              <div class="card-block">
                                <?php $form = ActiveForm::begin(); ?>
                                <fieldset class="form-group">
                                  All leads are currently send to your login email : <?php echo Yii::$app->user->identity->email; ?>
                                </fieldset>
                                <fieldset class="form-group">
                                  Use a different email for leads and service chat
                                  <label class="ks-checkbox-slider">
                                   <input type="checkbox" name="different_email" <?php if($different_email==1){echo "checked";}?> id="different_email" value="1">
                                   <span class="ks-indicator"></span>
                                 </label>
                                 <div id="different_email_div">
                                <fieldset class="form-group">
                                   <select name="service_email[]" id="service_email"  class="form-control" multiple="multiple">
                                     <?php if( !empty( $service_emails ) ){?>
                                         <?php foreach($service_emails as $service_email){?>
                                             <option value="<?php echo $service_email;?>" selected><?php echo $service_email;?></option>
                                         <?php }?>
                                     <?php }?>
                                   </select>
                                </fieldset>
                                <fieldset class="form-group">
                                   <select name="lead_email[]" id="lead_email"  class="form-control" multiple="multiple">
                                     <?php if( !empty( $leads_emails ) ){?>
                                         <?php foreach($leads_emails as $leads_email){?>
                                             <option value="<?php echo $leads_email;?>" selected><?php echo $leads_email;?></option>
                                         <?php }?>
                                     <?php }?>
                                   </select>
                                </fieldset>
                                 </div>
                                </fieldset>
                                <fieldset class="form-group">
                                  <div id="lead_service_email_div">
                                    <select name="lead_service_email[]" id="lead_service_email"  class="form-control" multiple="multiple" style="display:none;">
                                      <?php if( !empty( $lead_service_emails ) ){?>
                                          <?php foreach($lead_service_emails as $lead_service_email){?>
                                              <option value="<?php echo $lead_service_email;?>" selected><?php echo $lead_service_email;?></option>
                                          <?php }?>
                                      <?php }?>
                                    </select>
                                  </div>
                                </fieldset>
                                <fieldset class="form-group">
                                  <label class="ks-checkbox-slider">
                                   <input type="checkbox" <?php if($notify_lead==1){echo "checked";}?> name="notify_lead" value="1">
                                   <span class="ks-indicator"></span>
                                 </label>
                                 Notify me when a lead is generated
                                </fieldset>
                                <fieldset class="form-group">
                                  <label class="ks-checkbox-slider">
                                   <input type="checkbox" <?php if($notify_chat==1){echo "checked";}?> name="notify_chat" value="1">
                                   <span class="ks-indicator"></span>
                                 </label>
                                 Notify me when a service chat is generated
                                </fieldset>
                                <fieldset class="form-group">
                                  If you would like to change your login email address please send a request to team@chatmetrics.com
                                </fieldset>
                                <fieldset class="form-group">
                                   <input type="submit" class="btn btn-primary" name="notify_email" value="Save Notification">
                                </fieldset>
                                <?php ActiveForm::end(); ?>
                              </div>
                            </div>
                        </div>
                    </div>
                    <!---=============notification==================--->
                    <?php
                  }
                    ?>

                    <?php
                      if(Yii::$app->user->identity->role == User::ROLE_ADMIN){
                    ?>
<!--Managers Email---->
<div class="row">
    <div class="col-xl-12 ks-draggable-column">
        <div id="ks-bar-chart-panel" class="card panel" data-dashboard-widget>
            <h5 class="card-header">
                Manager's Email<small>Note:A New Signup Notification Will be sent to this Email Address</small>
            </h5>
            <div class="card-block">
             <?php $form = ActiveForm::begin(); ?>

             <fieldset class="form-group">
               <label class="form-control-label">Email:<label>
               <input name="email" value="<?php echo $adminSetting->manager_email;?>" class="form-control" type="text">
             </label></label></fieldset>

             <fieldset class="form-group">
                 <input type="submit" class="btn btn-primary" name="manager_email" value="Submit">
             </fieldset>
             <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<!--Managers Email---->
<?php
}
?>
                    <!-- Notification setting -->

          <!-- <div class="row">
                        <div class="col-xl-12 ks-draggable-column">
                            <div id="ks-bar-chart-panel" class="card panel">
                                <h5 class="card-header">
                                   Notification Settings
                                </h5>
                <div class="card">
                  <div class="card-block">
                    <?php $form = ActiveForm::begin(); ?>
                        <fieldset class="form-group">
                          <label class="form-control-label">Email address leads are sent to:<label>
                          <input name="email_for_leads" value="<?php echo $userData['email_for_leads'];?>" type="email" class="form-control">
                        </fieldset>

                        <fieldset class="form-group">
                          <label class="form-control-label">Email support chats are sent to:<label>
                          <input name="email_for_chats" value="<?php echo $userData['email_for_chats'];?>" type="email" class="form-control">
                        </fieldset>

                        <fieldset class="form-group">
                          <input type="submit" class="btn btn-primary" name="notification_settings" value="Submit">
                        </fieldset>
                      <?php ActiveForm::end(); ?>

                  </div>
                </div>
                            </div>
                        </div>
                    </div> -->

                    <!-- Notification setting -->

        <!-- to be deleted static data -->

          <!-- <div class="row">
              <div class="col-xl-12 ks-draggable-column">
                  <div id="ks-bar-chart-panel" class="card panel">
                      <h5 class="card-header">
                         Saved Credit Card Details
                      </h5>
                    <div class="card">
                      <div class="card-block">
                        <div class="alert alert-success">
                          Brand : Test Brand<br>
                          Card Number : XXXXXXXXXXXX4556<br>
                          Expire Month : January<br>
                          Expire Year : 2018<br>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div> -->

        <!-- To be deleted static data -->
                    <?php
                      if(!empty($cu)){
                    ?>
                        <div class="row">
                            <div class="col-xl-12 ks-draggable-column">
                                <div id="ks-bar-chart-panel" class="card panel">
                                    <h5 class="card-header">
                                       Saved Credit Card Details
                                    </h5>
                                    <div class="card">
                                      <div class="card-block">
                                        <div class="alert alert-success">
                                          <?php echo "Brand : " . $cu->default_source->brand . "<br>";?>
                                          <?php echo "Card Number : XXXXXXXXXXXX" . $cu->default_source->last4 . "<br>";?>
                                          <?php echo "Expire Month : " . $cu->default_source->exp_month . "<br>";?>
                                          <?php echo "Expire Year : " . $cu->default_source->exp_year . "<br>";?>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                      }
                    ?>
                    <!-- Change password for particular user -->

                    <?php
                      if(Yii::$app->user->identity->role == User::ROLE_ADMIN || Yii::$app->user->identity->role == User::ROLE_FRANCHISE){
                    ?>
                        <!-- For Admin -->
                        <div class="row">
                          <div class="col-xl-12 ks-draggable-column">
                              <div id="ks-bar-chart-panel" class="card panel">
                                  <h5 class="card-header">
                                    <?php
                                      $userLabel = Yii::$app->user->identity->role == User::ROLE_ADMIN ? 'Change User Password' : 'Change Outlet User Password';
                                      echo $userLabel;
                                    ?>
                                  </h5>
                                  <div class="card">
                                    <div class="card-block">

                                      <?php $form = ActiveForm::begin([
                                        'action' => Url::to(['settings/change-agency-user-password']),
                                        'id'=>'change-agency-user-password-form',
                                        'options'=>['class'=>'form-horizontal'],
                                        'fieldConfig'=>[
                                          'template'=>"{label}\n<div class=\"col-lg-12\">
                                                {input}</div>\n<div class=\"col-lg-12\">
                                                {error}</div>",
                                          'labelOptions'=>['class'=>'col-lg-12 control-label'],
                                        ],
                                      ]); ?>

                                        <?php
                                          $userList = Yii::$app->user->identity->role == User::ROLE_ADMIN ? $allUsersArray : $outletUsersArray;
                                          $fieldLabel = Yii::$app->user->identity->role == User::ROLE_ADMIN ? 'Users List' : 'Outlet Users List';
                                          $promptUser = Yii::$app->user->identity->role == User::ROLE_ADMIN ? 'Select User' : 'Select Outlet User';
                                        ?>

                                        <?php echo $form->field($changeAgencyUserPassModel, 'user')->dropDownList($userList, ['class' => 'form-control', 'prompt' => $promptUser])->label($fieldLabel); ?>

                                        <?= $form->field($changeAgencyUserPassModel,'newpass',['inputOptions'=>[
                                          'placeholder'=>'New Password'
                                        ]])->passwordInput() ?>

                                        <?= $form->field($changeAgencyUserPassModel,'repeatnewpass',['inputOptions'=>[
                                          'placeholder'=>'Repeat New Password'
                                        ]])->passwordInput() ?>

                                        <div class="notify-checkbox">
                                          <?= $form->field($changeAgencyUserPassModel, 'emailNotification')->checkbox(); ?>
                                        </div>

                                        <div class="form-group">
                                          <div class="col-lg-offset-2 col-lg-11">
                                            <?= Html::submitButton('Change password',[
                                              'class'=>'btn btn-primary'
                                            ]) ?>
                                          </div>
                                        </div>
                                      <?php ActiveForm::end(); ?>

                                    </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    <?php
                      }
                    ?>
                    <!-- Change password for a particular user -->


                    <!-- For Custom Package Range -->

                    <!-- For Custom Package Range -->


                </div>
            </div>
        </div>
    </div>
</div>
<?php
/*
$this->registerJs(
    "$('document').ready(function(){
    $('[data-dashboard-widget]').KosmoWidgetControls({
            onRefresh: function (element) {
                var zIndex = 1;

                if (element.hasClass($.fn.KosmoWidgetControls.defaults.fullScreenClass)) {
                    zIndex = 11;
                }

                element.LoadingOverlay("show", {
                    color: 'rgba(255, 255, 255, 0.8)',
                    image: '',
                    fontawesome : "la la-refresh la-spin",
                    zIndex: zIndex
                });

                setTimeout(function () {
                    element.LoadingOverlay("hide");
                }, 2000);
            },
            onFullScreen: function (element, isFullScreen) {
                // Disable sortable on full screen mode
                if (isFullScreen) {
                    $(".ks-draggable-column").sortable('disable');
                } else {
                    $(".ks-draggable-column").sortable('enable');
                }
            },
            onClose: function (element, closeCallback) {
                $.confirm({
                    title: 'Danger!',
                    content: 'Are you sure you want to remove this widget from dashboard?',
                    type: 'danger',
                    buttons: {
                        confirm: {
                            text: 'Yes, remove',
                            btnClass: 'btn-danger',
                            action: function() {
                                closeCallback();
                            }
                        },
                        cancel: function () {}
                    }
                });
            }
        });

        $(".ks-draggable-column").sortable({
            containment: ".ks-page-content-body > .container-fluid",
            handle: ".card-header",
            cancel: ".card-header .ks-controls",
            placeholder: "ks-dashboard-portlet-placeholder",
            connectWith: ".ks-draggable-column",
            revert: true,
            activate: function(event, ui) {
                $('.ks-dashboard-portlet-placeholder').height(ui.item.height() - 2); // 2 - border-width
            },
            change: function () {
                $('.ks-draggable-column').each(function () {
                    if ($(this).find('.ui-sortable-helper').size() && $(this).closest('.row').find('.card').size() == 1) {
                        $(this).closest('.row').css({
                            margin: 0,
                            height: 0
                        });
                    }
                });

                $('.row').each(function () {
                    if ($(this).find('[class*="col-"]').size() == 0) {
                        $(this).remove();
                    }
                });
            }
        });


  });"
, \yii\web\View::POS_END);
*/

$this->registerCss("
  .notify-checkbox{
    margin-left: 20px;
  }

  .select2-results__message {
  display: none !important;
}

  .add-range-btn{
    margin-top: 25px;
  }

  .alert.alert-danger {
    background-color: #f2dede;
    border-color: #ebcccc;
    color: #A94445;
    font-size: 18px;
    margin: 10px;
  }

.select2-container  {
    width: 100% !important;
  }
");

$this->registerJs("
  $('#custom_currency').val('$adminSetting->global_per_lead_cost_currency');
  $('#different_email').on('change', function(e){
    if($(this).prop('checked')==true){
       $('#different_email_div').show();
       $('#lead_service_email_div').hide();
    }else{
      $('#different_email_div').hide();
      $('#lead_service_email_div').hide();
    }
  });
  $('#lead_email').select2({
    placeholder: 'Enter Emails for leads',
    tags: true,
  });
  $('#service_email').select2({
    placeholder: 'Enter Emails for service chat',
    tags: true,
  });
  $('#lead_service_email').select2({
    placeholder: 'Enter Emails for leads and service chat',
    tags: true,
  });
  if($('#different_email').prop('checked')==true){
    $('#different_email_div').show();
    $('#lead_service_email_div').hide();
  }else{
    $('#different_email_div').hide();
    $('#lead_service_email_div').hide();
  }

  $(document).on('submit','#global_per_lead_setting',function(){
     var cost = $('#global_per_lead_cost').val();
     var currency = $('#custom_currency').val();

     if(isNaN(cost)) {
       alert('Global per lead cost must be a valid number');
       return false;
     }

     if(currency == '') {
       alert('Global per lead cost currency is required');
       return false;
     }
  });


  $('.add-range-btn').off('click').on('click', function(e){
    e.preventDefault();
    rowHtml = \"<div class='row'> <div class='col-md-3'> <input class='form-control' name='rangeStart[]' required /> </div> <div class='col-md-3'> <input class='form-control' name='rangeEnd[]' required /> </div> <div class='col-md-3'> <input class='form-control' name='amount[]' required /> </div> <div class='col-md-3  add-btn-container'> <button class='btn btn-danger remove-range-btn'><i class='la la-minus'></i></button> </div> </div>\";
    $('.range-parent-container').append(rowHtml);
    $('.remove-range-btn').off('click').on('click', function(e){
      $(this).parent().parent().remove();
    });
  });

  $('.remove-range-btn').off('click').on('click', function(e){
    $(this).parent().parent().remove();
  });

  $('.save-iframe-btn').off('click').on('click', function(e){

    e.preventDefault();
    iframe_text = $('#iframeTag').val();

    if(iframe_text == ''){
      swal('Iframe cannot be left blank', '', 'error');
      return false;
    }

    $.ajax({
      type: 'POST',
      url: '".Url::to(['settings/save-iframe'])."',
      data: {iframe_text: iframe_text},
      success: function(response){
        response = JSON.parse(response)
        if(response.success == true){
          swal('Iframe saved successfully', '', 'success');
        }
      },
      error: function(data){
        console.log(data);
        swal('Some problem occured!', 'Please try again later after sometime.', 'error');
        return false;
      },
    });

  });

  $('.save-bg-color').off('click').on('click', function(e){
    e.preventDefault();

    color_code = $('#loginBgColor').val();
    if(color_code == ''){
      swal('Color code cannot be left blank', '', 'error');
      return false;
    }
    var isOk  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(color_code);
    if(!isOk){
      swal('Color code is not valid', '', 'error');
      return false;
    }

    $.ajax({
      type: 'POST',
      url: '".Url::to(['settings/save-color-code'])."',
      data: {color_code: color_code},
      success: function(response){
        response = JSON.parse(response)
        if(response.success == true){
          swal('Color code saved successfully', '', 'success');
        }
      },
      error: function(data){
        console.log(data);
        swal('Some problem occured!', 'Please try again later after sometime.', 'error');
        return false;
      },
    });

  })
");

?>
