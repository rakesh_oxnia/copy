<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/**
 * @var yii\web\View $this
 * @var common\models\UserCategory $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="user-category-form">

    <div class="row">
      <div class="col-md-3">
        <h5>Client: &nbsp; <?=$userModel->email?></h5>
       
      </div>
    </div>

    <br>

    <form action="<?=Url::to(['user-category/bulk-create', 'user_id' => $user_id])?>" method="post">

      <div class="category-parent-container">

        <div class="row">
          <div class="col-md-3">
            <label class="control-label" >Category Name</label>
            <input class="form-control" name='categoryName[]' required />
          </div>
          <div class="col-md-3">
            <label class="control-label" >Category Value($)</label>
            <input class="form-control" name='categoryValue[]' type='number' />
          </div>
          <div class="col-md-3">
            <label class="control-label" >Parent Category</label>
            <select name="parentCategory[]" id='initial_parent_category' class='parent-category form-control'>
              <option value='0'>Select Parent Category</option>
              <?php
                foreach ($categories as $key => $value) {
                  echo "<option value='$value->id'>$value->category_name</option>";
                }
              ?>
            </select>
          </div>
          <div class="col-md-3 add-btn-container">
            <button class="btn btn-success add-category-btn"><i class='la la-plus'></i></button>
          </div>
        </div>
      </div>

      <br>

      <div class="row">
          <div class="col-md-3">
             <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />

            <input type="submit" name="submit" value="Submit" class='btn btn-success'>
          </div>
      </div>

    </form>

</div>

<?php
 $this->registerCss("

  .add-category-btn{
    margin-top: 25px;
  }
 
");

 $this->registerJs("

    var categories = '".json_encode($catArray)."';
    categoriesObj = JSON.parse(categories);

    for (var key in categoriesObj) {
      console.log('Key -> ' + key + ' : Value -> ' + categoriesObj[key])
     
    }

    //console.log(categoriesObj);

    $('.add-category-btn').off('click').on('click', function(e){
      e.preventDefault();

      select_input_start = \"<select name='parentCategory[]' class='parent-category form-control'><option value='0'>Select Parent Category</option>\";
      select_input_end = \"</select>\";

      select_options = \"\";

      default_option = $('#initial_parent_category').val();
      console.log(default_option);

      for (var key in categoriesObj) {
        selected = key == default_option ? 'selected' : '';
        select_options = select_options + \"<option value='\" + key + \" ' \" + selected + \" > \" + categoriesObj[key] + \" </option>\";
      }

      select_element = select_input_start + select_options + select_input_end;
 
      rowHtml = \"<div class='row'> <div class='col-md-3'> <input class='form-control' name='categoryName[]' required /> </div> <div class='col-md-3'> <input class='form-control' name='categoryValue[]' type='number' /> </div> <div class='col-md-3'> \" + select_element + \" </div> <div class='col-md-3 add-btn-container'> <button class='btn btn-danger remove-category-btn'><i class='la la-minus'></i></button> </div> </div> \";
      
      $('.category-parent-container').append(rowHtml);

      $('select').select2();
      
      $('.remove-category-btn').off('click').on('click', function(e){
        $(this).parent().parent().remove();
      });
    });

    $('select').select2();
  ");
?>
