<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var common\models\UserCategory $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="user-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
      <div class="col-md-4">
        <?= $form->field($model, 'category_name')->textInput(['maxlength' => 255]) ?>
      </div>
      <div class="col-md-4">
        <?= $form->field($model, 'value')->textInput(['maxlength' => 255]) ?>
      </div>
      <div class="col-md-4">
        <?= $form->field($model, 'parent_id')->dropdownList(ArrayHelper::map($categories, 'id', 'category_name'), ['class' => 'form-control select', 'maxlength' => 255,  'prompt' => 'Select Parent Category']) ?>
      </div>
    </div>

    <?= $form->field($model, 'user_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
  $this->registerCss("
    .user-category-form{
      padding: 20px 0 0 0;
    }
  ");
?>
