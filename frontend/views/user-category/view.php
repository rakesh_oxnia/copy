<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\UserCategory $model
 */

$this->title = $model->category_name;
$this->params['breadcrumbs'][] = ['label' => 'User Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ks-page-header">
    <section class="ks-title">
        <h3><?= Html::encode($this->title) ?></h3>
    </section>
</div>
<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">
                              <p>
                                  <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                                  <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                                      'class' => 'btn btn-danger',
                                      'data' => [
                                          'confirm' => 'Are you sure you want to delete this item?',
                                          'method' => 'post',
                                      ],
                                  ]) ?>
                              </p>

                              <?= DetailView::widget([
                                  'model' => $model,
                                  'attributes' => [
                                      //'id',
                                      [
                                        'attribute' => 'email',
                                        'label' => 'User',
                                        'value' => $model->user->email,
                                      ],
                                      'value',
                                      [
                                        'attribute' => 'parent_id',
                                        'label' => 'Parent Category',
                                        'value' => $model->parent_id == NULL ? NULL : $model->parent->category_name,
                                      ],
                                      'category_name',
                                      'created_at:datetime',
                                      'updated_at:datetime',
                                  ],
                              ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
  $this->registerCss("
    .user-category-view{
      padding: 50px;
    }
  ");
?>
