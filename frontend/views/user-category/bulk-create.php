<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\UserCategory $model
 */

$this->title = 'Create Bulk Categories';
$this->params['breadcrumbs'][] = ['label' => 'User Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ks-page-header">
    <section class="ks-title">
        <h3><?= Html::encode($this->title) ?></h3>
    </section>
</div>

<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">
                                <?= $this->render('_bulk_form', [
                                	'categories' => $categories,
                                	'catArray' => $catArray,
                                	'clientArray' => $clientArray,
                                	'user_id' => $user_id,
                					'userModel' => $userModel,
                                ]); ?>
                            </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
</div>

<?php
  $this->registerCss("
   
  ");

  $this->registerJs("
    $(function(){
      $('.select').select2();
    });
  ");
?>
