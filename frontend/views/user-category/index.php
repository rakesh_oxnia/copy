<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\User;
use yii\helpers\Url;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\UserCategorySearch $searchModel
 */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ks-page-header">
    <section class="ks-title">
        <h3><?= Html::encode($this->title) ?></h3>
    </section>
</div>
<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">
                              <p>
                                  <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
                                      &nbsp;
                                      <a class='btn btn-info delete-cats'>Delete selected categories</a>
                                      &nbsp;
                                       <a class='btn btn-primary create-bulk-categories' data-id="<?= in_array(Yii::$app->user->identity->role, [User::ROLE_ADMIN, User::ROLE_SUBADMIN]) ? 0 : Yii::$app->user->identity->id ; ?>">Create bulk categories</a>


                              </p>

                              <?= GridView::widget([
                                  'dataProvider' => $dataProvider,
                                  'filterModel' => $searchModel,
                                  'columns' => [
                                      ['class' => 'yii\grid\SerialColumn'],
                                      ['class' => 'yii\grid\CheckboxColumn'],

                                      //'id',
                                      //'user_id',
                                      'category_name',
                                      [
                                        'attribute' => 'parent_id',
                                        'label' => 'Parent Category',
                                        'value' => function($data){
                                          return $data->parent_id == NULL ? NULL : $data->parent->category_name;
                                        },
                                        'filter' => ArrayHelper::map($categories, 'id', 'category_name'),
                                      ],
                                      'value',
                                      [
                                        'attribute' => 'user_id',
                                        'label' => 'Client',
                                        'value' => function($data){
                                          return $data->user->email;
                                        },
                                        'filter' => Html::activeDropDownList($searchModel, 'user_id', $clientArray,['class'=>'client-dropdown form-control','prompt' => 'Select Client']),
                                        'visible' => in_array(Yii::$app->user->identity->role, [User::ROLE_ADMIN, User::ROLE_SUBADMIN]) ? true : false,
                                      ],
                                      //'created_at:datetime',
                                      //'updated_at:datetime',

                                      [
                                        'class' => 'yii\grid\ActionColumn',
                                                'header'=>'Action',
                                                'template'=>'{view}{update}{delete}',
                                                'buttons' => [
                                                    'delete' => function ($url, $model) {
                                                        if(in_array(Yii::$app->user->identity->role, [User::ROLE_ADMIN, User::ROLE_SUBADMIN])){
                                                          return Html::a('<span class="ks-icon la la-trash"></span>',
                                                              $url, [
                                                  'data-confirm' => 'Are You sure you want to delete this Category ?',
                                                  'data-method' => 'post'
                                                          ]);
                                                        }else{
                                                          return '';
                                                        }

                                                    },
                                          'view' => function ($url, $model) {
                                                        return Html::a('<span class="ks-icon la la-eye"></span>',
                                                            $url);
                                                    },
                                          'update' => function ($url, $model) {
                                                        return Html::a('<span class="ks-icon la la-pencil"></span>',
                                                            $url);
                                                    },
                                                ],
                                        ],
                                  ],
                              ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
  $this->registerCss("
    .user-category-index{
      padding: 20px;
    }

    .table td{
      padding: 10px !important;
      font-size: 12px !important;
    }

    .ks-icon{
      font-size: 20px !important;
      padding-left: 7px;
    }

    .action-column{
      width: 140px;
    }

    .ks-icon:hover{
      color: #f00;
    }

    .delete-cats, .create-bulk-categories{
      color: #fff !important;
    }

  ");

  $this->registerJs("

    $.ajaxSetup({
        data:  ".\yii\helpers\Json::encode([
            \yii::$app->request->csrfParam => \yii::$app->request->csrfToken,
        ])."
    });

    $('.delete-cats').off('click').on('click', function(e){
      selected_keys = $('#w0').yiiGridView('getSelectedRows');
      if(selected_keys.length == 0){
        swal('No category selected', 'Please chose atleast 1 category to delete', 'error');
        return false;
      }
      //categories = JSON.stringify({selected_keys});
      //categories = JSON.stringify(selected_keys);
      console.log(selected_keys);

      swal({
        title: 'Delete selected categories ?',
        text: selected_keys.length + ' categories will be deleted permanently',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.ajax({
            type: 'POST',
            url: '".Url::to(['user-category/delete-categories'])."',
            data: {categories: selected_keys},
            success: function(response){
              response = JSON.parse(response);
              if(response.success == true){
                swal('Deleted categories successfully', response.count + ' categories deleted successfully', 'success');
                $('.swal-button').off('click').on('click', function(e){
                  location.reload();
                })
              }
            },
            error: function(data){
              console.log(data);
            },
          });
        } else {
          swal('Categories not deleted!');
        }
      });
    });

    $('select').select2();

    $('.create-bulk-categories').off('click').on('click', function(e){
      if($(this).attr('data-id') == 0){

        client_id = $('#usercategorysearch-user_id').val();
        if(client_id != ''){
          window.location = '".Url::to(['user-category/bulk-create'])."&user_id=' + client_id;
        }else{
          swal('Please select a client from select client dropdown');
        }
      }else{
        window.location = '".Url::to(['user-category/bulk-create'])."&user_id=' + $(this).attr('data-id');
      }
    });

  ");

  $this->registerJs("
    var user_id = '".$user_id."';
    if(user_id != 0){
      //swal('select user with id ' + user_id);
      //$('#usercategorysearch-user_id').val(user_id).trigger('change');

    }
  ");
?>
