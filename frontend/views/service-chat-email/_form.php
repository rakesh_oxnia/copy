<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\ServiceChatEmail $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="service-chat-email-form">


   <?php $form = ActiveForm::begin(); ?>

      <div class="row">
        <div class="col-md-6">
          <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
        </div>
      </div>
   <div class="form-group">
        <?= $form->field($model, 'user_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false); ?>
       <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
   </div>

   <?php ActiveForm::end(); ?>

</div>
