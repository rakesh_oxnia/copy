<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\ServiceChatEmailSearch $searchModel
 */

$this->title = 'Service Chat Emails';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ks-page-header">
    <section class="ks-title">
        <h3><?= Html::encode($this->title) ?></h3>
    </section>
</div>
<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">

                              <p>
                                  <?= Html::a('Create Service Chat Email', ['create'], ['class' => 'btn btn-success']) ?>
                              </p>
                              <?php Pjax::begin(['id' => 'pjax_widget']); ?>
                              <?= GridView::widget([
                                  'dataProvider' => $dataProvider,
                                  'filterModel' => $searchModel,
                                  'columns' => [
                                      ['class' => 'yii\grid\SerialColumn'],

                                      //'id',
                                      //'user_id',
                                      'email:email',
                                      'created_at:datetime',
                                      'updated_at:datetime',

                                      ['class' => 'yii\grid\ActionColumn',
                                        'header'=>'Action',
                                        'template'=>'{update}{delete}',
                                        'buttons' => [
                                            'delete' => function ($url, $model) {
                                                  return Html::a('<span class="ks-icon la la-trash"></span>',
                                                      $url, [
                                                        'data-confirm' => 'Are you sure you want to delete this Email Address ?',
                                                        'data-method' => 'post'
                                                  ]);

                                            },
                                            // 'view' => function ($url, $model) {
                                            //     return Html::a('<span class="ks-icon la la-eye"></span>',
                                            //         $url);
                                            // },
                                            'update' => function ($url, $model) {
                                                return Html::a('<span class="ks-icon la la-pencil"></span>',
                                                    $url);
                                            },
                                        ],
                                    ],
                                  ],
                              ]); ?>

                                <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
  $this->registerCss("

    .table td{
      padding: 10px !important;
      font-size: 12px !important;
    }

    .ks-icon{
      font-size: 20px !important;
      padding-left: 7px;
    }

    .action-column{
      width: 140px;
    }

    .ks-icon:hover{
      color: #f00;
    }

  ");

    $this->registerJs("
        $('select').select2();
        $.pjax.reload({container:'#pjax_widget'});
    ");
