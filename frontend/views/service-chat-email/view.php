<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var common\models\ServiceChatEmail $model
 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Service Chat Emails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ks-page-header">
    <section class="ks-title">
        <h3><?= Html::encode($this->title) ?></h3>
    </section>
</div>
<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">

                              <p>
                                  <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                                  <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                                      'class' => 'btn btn-danger',
                                      'data' => [
                                          'confirm' => 'Are you sure you want to delete this item?',
                                          'method' => 'post',
                                      ],
                                  ]) ?>
                              </p>

                              <?= DetailView::widget([
                                  'model' => $model,
                                  'attributes' => [
                                      'id',
                                      'user_id',
                                      'email:email',
                                      'created_at',
                                      'updated_at',
                                  ],
                              ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
