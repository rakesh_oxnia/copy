<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PackagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Packages';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--New Design----->
<div class="ks-page-header">
            <section class="ks-title">
                <h3><?= Html::encode($this->title) ?></h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                                      <p>
                                          <?= Html::a('Create Packages', ['create'], ['class' => 'btn btn-success']) ?>

                                      </p>

                                      <?php Pjax::begin(['id' => 'pjax_widget']); ?>

                                      <?= GridView::widget([
                                          'dataProvider' => $dataProvider,
                                          'filterModel' => $searchModel,
                                          'id' => 'pjax_widget',
                                          'columns' => [
                                              ['class' => 'yii\grid\SerialColumn'],
                                              //'id',
                                              'package_name',
                                              'number_of_leads',
                                              //'number_of_chats',
                                              'amount',
                                              'interval',
                                              'currency',
                                              'created_at:date',
                                              /*['class' => 'yii\grid\ActionColumn',
                                              'header'=>'Action',
                                              'headerOptions' => ['class' => 'action-column'],
                                              'template'=>'{update}{delete}',
                                              'buttons' => [
                                                'update' => function ($url, $model) {
                                                            return Html::a('<span class="ks-icon la la-pencil"></span>',
                                                                $url);
                                                        },
                                              'delete' => function ($url, $model) {
                                                  return Html::a('<span class="ks-icon la la-trash"></span>',
                                                      $url, [
                                            								'data-confirm' => 'Are You sure you want to delete this User ?',
                                            								'data-method' => 'post'
                                                          ]);
                                                      },
                                          ],
                                        ],*/
                                          ],
                                      ]); ?>

                        <?php Pjax::end(); ?>

    </div>
                              </div>

                          </div>

                      </div>
                  </div>
              </div>
          </div>
      </div>
<!--New Design----->
<?php
$this->registerCss("

  .ks-icon{
    font-size: 20px !important;
    padding-left: 7px;
  }
  .ks-icon:hover{
    color: #f00;
  }
");
$this->registerJs("
    $.pjax.reload({container:'#pjax_widget'});
");
?>
