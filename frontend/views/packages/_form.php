<?php

use common\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Packages */
/* @var $form yii\widgets\ActiveForm */

$stripeUserCurrencies = User::$stripeUserCurrencies;
?>

<div class="packages-form">



  <?php $form = ActiveForm::begin([
         'id' => 'packages-form',
         'enableAjaxValidation' => true,
         //'action' => Url::toRoute('user/ajaxregistration'),
        // 'validationUrl' => Url::toRoute(Validate')

 ]); ?>


    <?= $form->field($model, 'package_name')->textInput(['form' => 'packages-form']) ?>

    <?= $form->field($model, 'number_of_leads')->textInput() ?>

     <!-- $form->field($model, 'number_of_chats')->textInput() -->
     <fieldset class="form-group">
       <!-- Currently our code does not support zero-decimal currency.
       Zero-decimal currencies are those which does not support decimal values, so when making a payment if we multiple them by 100 [as we usually do because stripe accepts payment in smallest unit of currency] then as it does not support decimal values it will actually charge 100 times more than required.
       Before adding new currency please check below links: https://stripe.com/docs/currencies#zero-decimal
       https://stripe.com/docs/currencies#presentment-currencies -->
       <select name="currency" id="currency_select" class="form-control" required>
     		  <option value=""></option>
               <?php
               foreach ($stripeUserCurrencies as $currency => $arr_info) {
                   ?>
                   <option value="<?php echo $currency;?>">
                       <?php echo $arr_info["name"];?>
                   </option>
                   <?php
               }
               ?>
<!--     		  <option value="AUD">Australian Dollar</option>-->
<!--     		  <option value="BRL">Brazilian Real </option>-->
<!--     		  <option value="CAD">Canadian Dollar</option>-->
<!--     		  <option value="CZK">Czech Koruna</option>-->
<!--     		  <option value="DKK">Danish Krone</option>-->
<!--     		  <option value="EUR">Euro</option>-->
<!--     		  <option value="HKD">Hong Kong Dollar</option>-->
<!--     		  <option value="HUF">Hungarian Forint </option>-->
<!--     		  <option value="ILS">Israeli New Sheqel</option>-->
<!--     		  <option value="JPY">Japanese Yen</option>-->
<!--     		  <option value="MYR">Malaysian Ringgit</option>-->
<!--     		  <option value="MXN">Mexican Peso</option>-->
<!--     		  <option value="NOK">Norwegian Krone</option>-->
<!--     		  <option value="NZD">New Zealand Dollar</option>-->
<!--     		  <option value="PHP">Philippine Peso</option>-->
<!--     		  <option value="PLN">Polish Zloty</option>-->
<!--     		  <option value="GBP">Pound Sterling</option>-->
<!--     		  <option value="SGD">Singapore Dollar</option>-->
<!--     		  <option value="SEK">Swedish Krona</option>-->
<!--     		  <option value="CHF">Swiss Franc</option>-->
<!--     		  <option value="TWD">Taiwan New Dollar</option>-->
<!--     		  <option value="THB">Thai Baht</option>-->
<!--     		  <option value="TRY">Turkish Lira</option>-->
<!--     		  <option value="USD">U.S. Dollar</option>-->
       </select>
     </fieldset>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>

	<!-- $form->field($model, 'validity')->textInput(['maxlength' => true]) -->

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
