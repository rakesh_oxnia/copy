<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Packages */

$this->title = 'Create Packages';
$this->params['breadcrumbs'][] = ['label' => 'Packages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ks-page-header">
            <section class="ks-title">
                <h3><?= Html::encode($this->title) ?></h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
                                      <div class="packages-create">

                                          <?= $this->render('_form', [
                                              'model' => $model,
                                          ]) ?>

                                      </div>
                                    </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <?php
        $this->registerJs("
        $('#currency_select').select2({
          placeholder: 'Select Currency'
        });

        ");
        ?>
