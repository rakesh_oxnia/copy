<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use ruskid\stripe\StripeCheckout;
?>

<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/pricing/subscriptions.min.css">

 <div class="ks-page-header">
            <section class="ks-title">
                <h3>Subscriptions</h3>
            </section>
        </div>
        <div class="ks-page-content">
            <div class="ks-page-content-body ks-full-height">
                <div class="ks-nav-body-wrapper">
                    <div class="ks-pricing-subscriptions-page">
						<hr>
            <?php if($subscription_type=='pay_as_you_go'){ ?>
                        <div class="ks-subscriptions">
						     <div class="ks-subscription ks-active">
										<div class="ks-header">
											<span class="ks-name">Pay as you go</span>
											<span class="ks-price">
												<span class="ks-amount"><?=$single_charge.' '.$transactionCurrency?></span>/per lead
											</span>
										</div>
										<div class="ks-body">
											 <ul>
												<li class="ks-item">
													<span class="ks-icon la la-briefcase"></span>
													<span class="ks-text">Leads</span>
													<span class="ks-amount">Unlimited</span>
												</li>

												<li class="ks-item">
													<span class="ks-icon la la-comments"></span>
													<span class="ks-text">Chats</span>
													<span class="ks-amount">Unlimited</span>
												</li>
											</ul>
										</div>
									</div>
                        </div>
                        <?php
                          }
                       ?>
                    </div>
                </div>
            </div>
        </div>
<?php

$this->registerCss("
  .pay-button-custom{
    display: none;
    margin-top: 10px;
  }
");

$this->registerJs("");

?>
