<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>	
	<div class="body-content">
		<div class="main-box-body clearfix">
				<div id="myWizard" class="wizard">
						 
						<div class="step-content">
							  <?php $form = ActiveForm::begin(); ?>
								
								<div class="step-pane active" id="step1">
									<br/>
									
									<?php if($success){?>
										<div class="alert alert-success">
											<?php echo $success;?>
										</div>
								<?php }?>
								<?php if( isset($_POST['save']) && $success == '' ) : ?>
									<div class="alert alert-danger">
										<?= $form->errorSummary($model); ?>
									</div>	
								<?php endif;?> 
								 
								
									<h1>Manage Permissions</h1>
									<hr>
									<div class="row" style="margin-left : 20px;margin-bottom : 20px;">
										<div class="col-sm-6">
											<div class="form-group">												
												<?php echo $form->field($model, 'is_lead')->checkbox()->label(false);?>
												Leads										
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
											<?php echo $form->field($model, 'is_chat')->checkbox()->label(false);?>
												
												Chats										
											</div>
										</div>
									</div>
									
									<div class="row" style="margin-left : 20px;margin-bottom : 20px;">
										<div class="col-sm-6">
											<div class="form-group">
											<?php echo $form->field($model, 'is_report')->checkbox()->label(false);?>
												
												Reports										
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
											<?php echo $form->field($model, 'is_faq')->checkbox()->label(false);?>
												
												Faq										
											</div>
										</div>	
									</div>
									
									<div class="row" style="margin-left : 20px;margin-bottom : 20px;">
										<div class="col-sm-6">
											<div class="form-group">
											<?php echo $form->field($model, 'is_package')->checkbox()->label(false);?>
												
												Packages										
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
											<?php echo $form->field($model, 'is_pay_as_you_go')->checkbox()->label(false);?>
												
												Pay as you go										
											</div>
										</div>	
									</div>
									
									<div class="row" style="margin-left : 20px;margin-bottom : 20px;">
										<div class="form-group">
											<div class="col-sm-12">
											<?php echo $form->field($model, 'is_email')->checkbox()->label(false);?>
												Email
											</div>		
										</div>
									</div>
									
									<div class="row" style="margin-left : 20px;margin-top : 20px;">
										<div class="form-group">
											<input type="submit" name="save" value="Save" class="btn btn-primary">
										</div>
									</div>
								</div>
							<?php ActiveForm::end(); ?>
						</div>
				</div>
		</div>
	</div>
				 