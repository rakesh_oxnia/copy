<?php

use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ChatstatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chat Stats Report';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--New Design----->
<div class="ks-page-header">
            <section class="ks-title">
                <h3><?= Html::encode($this->title) ?></h3>
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
                                    <div class="row">
                                      <div class="col-md-8">
                                          <?php
                                            $form = ActiveForm::begin([
                                                    'method' => 'get',
                                                    'action' => Url::to(['chatstat/index']),
                                            ]);
                                          ?>
                                          <div class="row">
                                            <div class="col-sm-4">
                                              <input type="text" placeholder="Date From" class="form-control" id="datefrom" name="date_from">
                                            </div>
                                            <div class="col-sm-4">
                                              <input type="text" placeholder="Date To" class="form-control" id="dateto" name="date_to">
                                            </div>
                                            <div class="col-sm-4">
                                              <input type="submit" class="btn btn-info crange-submit" value="Submit">
                                            </div>
                                          </div>
                                        <?php ActiveForm::end(); ?>
                                      </div>
                                      <div class="col-md-4">
                                        <?php
                                          if ($dataProvider->getTotalCount()>0) {
                                            $csvdata = json_encode(Yii::$app->request->getQueryParams());
                                            echo "<a data-count='".$dataProvider->getTotalCount()."' href='".Url::to(['chatstat/export','dataprovider' => $csvdata])."' class='btn btn-primary-outline ks-light pull-right download-csv-btn'>Download CSV</a>";
                                          }                                                 
                                        ?>
                                      </div>
                                    </div>

                                      <?php Pjax::begin(['id' => 'pjax_widget']); ?>

                                      <?= GridView::widget([
                                          'dataProvider' => $dataProvider,
                                          'id' => 'pjax_widget',
                                          'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                                          'columns' => [
                                              ['class' => 'yii\grid\SerialColumn'],
                                              'business_name',
                                              'group',
                                              'greetings', 
                                              'total_chats',
                                              'total_leads',
                                              'chat_to_lead',
                                              'conversion_rate',
                                              'total_greeting_chats',
                                              'total_visitor_chats'   
                                          ],
                                      ]); ?>

                                    <?php Pjax::end(); ?>

                                   </div>
                              </div>

                          </div>

                      </div>
                  </div>
              </div>
          </div>
      </div>
<!--New Design----->
<?php
$this->registerCss("
  .ks-icon{
    font-size: 20px !important;
    padding-left: 7px;
  }
  .ks-icon:hover{
    color: #f00;
  }
");
$this->registerJs('
    $.pjax.reload({container:"#pjax_widget"});
    jQuery("document").ready(function(){
      jQuery("#dateto,#datefrom").datepicker( {dateFormat: "yy-mm-dd",changeMonth:true,changeYear:true} );
    });
');
?>
