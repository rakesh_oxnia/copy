<?php use yii\helpers\Html;
$this->title = "Dashboard";
?>
<div>
	<!--h1 class="page-header">DashBoard</h1-->
	
	<!-- /.row -->
    <div class="row">
		<?php
		if(Yii::$app->user->identity->role == 150){  // care cordinator 
			//if(Yii::$app->user->identity->valid_user == 1){
			 ?>
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-red">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-cog fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">Form</div>
								<div></div>
							</div>
						</div>
					</div>
					<a href="/site/viewform">
						<div class="panel-footer">
							<span class="pull-left">Go to Setting</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
		<?php //} 
		} ?>
		<?php if(Yii::$app->user->identity->role == 50){ 
			?>
		
		<!--div class="col-lg-3 col-md-6">
			<div class="panel panel-red">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-cog fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">Logo</div>
							<div>Setting</div>
						</div>
					</div>
				</div>
				<a href="/#">
					<div class="panel-footer">
						<span class="pull-left">Go to Setting</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div-->
		
		<!--div class="col-lg-3 col-md-6">
			<div class="panel panel-yellow">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-edit fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">Form</div>
							<div>Manage</div>
						</div>
					</div>
				</div>
				<a href="/forms/ndmanageform">
					<div class="panel-footer">
						<span class="pull-left">Manage Form</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div-->
		
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-green">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-user fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">New</div>
							<div>User</div>
						</div>
					</div>
				</div>
				<a href="<?= Yii::$app->homeUrl;?>user/create">
					<div class="panel-footer">
						<span class="pull-left">Create New User</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-users fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">List</div>
							<div>Users</div>
						</div>
					</div>
				</div>
				<a href="<?= Yii::$app->homeUrl;?>user/index">
					<div class="panel-footer">
						<span class="pull-left">Users Listing</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		
		<!--div class="col-lg-3 col-md-6">
			<div class="panel panel-green">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-file fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">CMS</div>
							<div>Pages</div>
						</div>
					</div>
				</div>
				<a href="/homecontent">
					<div class="panel-footer">
						<span class="pull-left">Manage CMS Pages</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		
		<div class="col-lg-3 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-3">
							<i class="fa fa-list fa-5x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">Resume</div>
							<div>Listing</div>
						</div>
					</div>
				</div>
				<a href="<?= Yii::$app->homeUrl;?>resume/index">
					<div class="panel-footer">
						<span class="pull-left">Resumes Listing</span>
						<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div-->
		
		<?php } ?>
		
		<?php if(Yii::$app->user->identity->role == 10){ ?>		
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-comments fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">Chats</div>
								<div>Listing</div>
							</div>
						</div>
					</div>
					<a href="<?= Yii::$app->homeUrl;?>chat/index">
						<div class="panel-footer">
							<span class="pull-left">Chats Listing</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-green">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-comments fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">Total</div>
								<div>Chats</div>
							</div>
						</div>
					</div>
					<a href="<?= Yii::$app->homeUrl;?>chat/total">
						<div class="panel-footer">
							<span class="pull-left">Total Chats</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			
			<div class="col-lg-3 col-md-6">
				<div class="panel panel-red">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-3">
								<i class="fa fa-cog fa-5x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">Goals</div>
								<div></div>
							</div>
						</div>
					</div>
					<a href="<?= Yii::$app->homeUrl;?>goals/index">
						<div class="panel-footer">
							<span class="pull-left">View Goals</span>
							<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
							<div class="clearfix"></div>
						</div>
					</a>
				</div>
			</div>
			
			
		<?php } ?>
		
	</div>
</div>