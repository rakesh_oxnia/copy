<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>

	<?php $form = ActiveForm::begin(['id' => 'login_form', 'options' => ['class' => 'form-container']]); ?>




	<?php if (Yii::$app->session->hasFlash('success')): ?>
	  <div class="alert alert-success alert-dismissable">
	  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	  <?= Yii::$app->session->getFlash('success') ?>
	  </div>
	<?php endif; ?>
<h4 class="ks-header">Login to Chat Metrics</h4>
<div class="row">
	 	<div class="col-md-12 form-input">
	 		<!-- <label class="lbl-login">Email</label> -->

			<img src="<?=Yii::$app->request->baseUrl?>/images/wrongemail.png" class='wrong-email-bubble'>
		    <?= $form->field($model, 'email', [
			'template' => '<div class="input-icon icon-right icon-lg icon-color-primary">{input}<span class="icon-addon"><span class="la email-input-span " ></span></span></div>{error}{hint}'])->textInput(["placeholder"=>"Email", 'id' => 'emailInput']) ?>
	 	</div>
	</div>

	<div class="row">
	 	<div class="col-md-12 form-input">
	 		<!-- <label class="lbl-login">Password</label> -->
			<img src="<?=Yii::$app->request->baseUrl?>/images/wrongpwd.png" class='wrong-password-bubble'>

			<?= $form->field($model, 'password', [
					'template' => '<div class="input-icon icon-right icon-lg icon-color-primary">{input}<span class="icon-addon"><span class="la password-input-span" ></span></span></div>{error}{hint}'])->passwordInput(["placeholder"=>"Password", 'id' => 'passwordInput']) ?>
	 	</div>
	</div>

	<div class="row">
	 	<div class="col-md-12 form-input">
	 		<label class="ks-checkbox-slider ks-primary">
			    <input type="checkbox" name="rememberInput" id="rememberInput" >
			    <span class="ks-indicator"></span>
			</label>
			<label class="remember_label">remember me</label>
	 	</div>
	</div>


	<!-- <div class="wrong-pwd help-block help-block-error">Sorry,wrong password</div> -->


	<div class="row">
	 	<div class="col-md-12 form-input">
	 		<?php echo Html::submitButton('Login', ['class' => 'btn btn-primary btn-block loginbtn', 'name' => 'login-button']) ?>
	 	</div>
	</div>


<div class="row">
	<div class="col-md-12 form-input">
			<a href="<?=Url::to(['site/request-password-reset'])?>" class="createacclink" >Forgot password?</a> <br>
			<a href="#" class="donthvacclink">Don't  have an account yet?</a><br />
			<a href="#" class="createacclink" >Create a free account</a>
	</div>
</div>

	<!-- <a class="btn btn-primary btn-block loginbtn">Login</a> -->


    <div class="ks-text-center">
     <!--  <?= Html::a('Forgot password?', ['site/request-password-reset']) ?>  -->
	</div>


   <?php ActiveForm::end(); ?>


<?php

	$this->registerCss("

		.card-block{
			position: relative;
		}

		.wrong-email-bubble{
		    height: 45px;
		    margin-left: 98px;
		    position:  absolute;
		    top: 90px;
		    right: -19px;
		    z-index: 10;
		    display: none;
		}

		.wrong-password-bubble{
		    height: 45px;
		    margin-left: 98px;
		    position:  absolute;
		    top: 189px;
		    right: -19px;
		    z-index: 10;
		    display: none;
		}

		.row{
			margin: 0 !important;
		}

		.form-input{
			max-width: 83%;
			margin-left: 11px;
		}

	");

	$this->registerJs("

		$(function(){

			$('.loginbtn').off('click').on('click', function(e){

				e.preventDefault();
				//$('.wrong-email-bubble').hide();
				//$('.wrong-password-bubble').hide();

				email = $('#emailInput').val();
				password = $('#passwordInput').val();
				remember_me = $('#rememberInput').is(':checked');
				console.log('Remember me: ' + remember_me);

				if(email == ''){
					//$('.wrong-email-bubble').hide();
					swal('Validation error', 'Email cannot be left blank', 'error');
					return false;
				}

				if(password == ''){
					//$('.wrong-password-bubble').hide();
					swal('Validation error', 'Password cannot be left blank', 'error');
					return false;
				}

				$.ajax({
					type: 'POST',
					url: '".Url::to(['site/ajax-login'])."',
					data: {email: email, password: password, remember_me: remember_me},
					success: function(response){
						response = JSON.parse(response);
						if(response.error){
							if(response.code == 502){
								$('.email-input-span').addClass('la-check');
								$('.password-input-span').addClass('la-close');
								//swal(response.message, '', 'error');
								//$('.wrong-email-bubble').hide();
								//$('.wrong-password-bubble').show();
								return false;
							}

							if(response.code == 501){
								$('.email-input-span').removeClass('la-check');
								$('.email-input-span').addClass('la-close');
								//$('.wrong-password-bubble').hide();
								//$('.wrong-email-bubble').show();
								//swal(response.message, '', 'error');
								return false;
							}
						}
					},
					error: function(data){
						console.log(data);
					}
				});
			});

			$('#rememberInput').change(function(e){
				if($(this).prop('checked') == true){
					$('.ks-checkbox-slider').css('background-color', '#00bae1');
				}else{
					$('.ks-checkbox-slider').css('background-color', '#b5ecf6b8');
				}
			});
		});

	");
?>
