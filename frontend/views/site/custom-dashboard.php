<?php

use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Dashboard';
?>
<div class="ks-page-header">
    <section class="ks-title">
        <h3>Dashboard</h3>

        <button class="btn btn-primary-outline ks-light ks-activity-block-toggle" data-block-toggle=".ks-activity-dashboard > .ks-activity">Activity</button>
    </section>
</div>
