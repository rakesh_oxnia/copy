<?php use yii\helpers\Html;

use common\models\User;

$this->title = "Dashboard";


//echo '<pre>';print_r($data);echo '</pre>';
$t_bad = 0;
$t_good = 0;
$t_chats = 0;
$leads_total = 0;
if($data){
	foreach($data as $d){
		$t_bad += $d->bad;
		$t_good += $d->good;
		$t_chats += $d->chats;
	}
}

//echo "<pre>"; print_r($leads); exit;

if($leads){
	foreach($leads as $d){
		$leads_total += $d->chats;
	}
}

if(Yii::$app->user->identity->role == User::ROLE_OUTLET || !empty($outletLeadCount)){
	$leads_total = $outletLeadCount;
}

/* echo $t_bad;
echo $t_chats;
echo $t_good;die("Dsds"); */

?>

<div class="ks-page-header">
    <section class="ks-title">
        <h3>Dashboard</h3>

        <button class="btn btn-primary-outline ks-light ks-activity-block-toggle" data-block-toggle=".ks-activity-dashboard > .ks-activity">Activity</button>
    </section>
</div>

<div class="ks-page-content">
  <div class="ks-page-content-body">
    <div class="ks-dashboard-tabbed-sidebar">
      <div class="ks-dashboard-tabbed-sidebar-widgets">
				<div class="row">
					 <div class="btn-group">
						<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
							<?php
							if(isset($_GET["filter"])){
								if($_GET["filter"] == 'crange')
									echo 'Custom Range';
								elseif($_GET["filter"] == 'last7')
									echo 'Last 7days';
								elseif($_GET["filter"] == 'last30')
									echo 'Last 30days';
								else
									echo $_GET["filter"];
							}
							else{
								echo "Last 7days";
							}
							?>
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu">
							<?php $today = date('Y-m-d');?>
							<li><a href="?filter=today&date_from=<?=$today ;?>&date_to=<?=$today ;?>&group_by=hour">Today</a></li>
							<?php $yesterday = date('Y-m-d');?>
							<?php $last75 = date('Y-m-d',strtotime("-1 days"));?>
							<li><a href="?filter=yesterday&date_from=<?=$last75 ;?>&date_to=<?=$last75 ;?>&group_by=hour">Yesterday</a></li>
							<?php $last7 = date('Y-m-d',strtotime("-6 days"));?>
							<li><a href="?filter=last7&date_from=<?=$last7 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 7days</a></li>
							<?php $last30 = date('Y-m-d',strtotime("-29 days"));?>
							<li><a href="?filter=last30&date_from=<?=$last30 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 30days</a></li>
							<li><a href="?filter=crange" id="c_range">Custom Range</a></li>
						</ul>
					</div>
					<div class="daterun">
						<form id="range-form" style="display:none;margin-left : 10px;" class="top-formsearch">
							 <div class="row">
								<div class="col-sm-4">
									<input type="text" placeholder="Date From" class="form-control" id="datefrom" name="date_from">
								</div>
								<div class="col-sm-4">
									<input type="text" placeholder="Date To" class="form-control" id="dateto" name="date_to">
								</div>
								<input type="hidden" name="filter" value="crange">
								<input type="hidden" name="group_by" value="day">
								<div class="col-sm-4">
									<input type="submit" class="btn btn-info" value="Filter">
								</div>
							</div>
						</form>
					</div>
				</div>

				<?php
					if(Yii::$app->user->identity->role == User::ROLE_ADMIN){
				?>

						<div class="row">
							<div class="col-lg-4 col-md-6">
									<div class="card ks-widget-payment-simple-amount-item ks-pink">
											<div class="payment-simple-amount-item-icon-block">
													<span class="ks-icon-combo-chart ks-icon"></span>
											</div>

											<div class="payment-simple-amount-item-body">
													<div class="payment-simple-amount-item-amount">

															<span class="ks-amount"><?php echo "$" . number_format($clientRevenue);?></span>
															<!-- <span class="ks-amount-icon ks-icon-circled-up-right"></span> -->
													</div>
													<div class="payment-simple-amount-item-description">
															<?php echo "Client Lead Revenue";?>
													</div>
											</div>
									</div>
							</div>
							<div class="col-lg-4 col-md-6">
									<div class="card ks-widget-payment-simple-amount-item ks-green">
											<div class="payment-simple-amount-item-icon-block">
													<span class="la la-money ks-icon"></span>
											</div>

											<div class="payment-simple-amount-item-body">
													<div class="payment-simple-amount-item-amount">

															<span class="ks-amount"><?php echo "$".number_format($chatmetricsRevenue);?></span>
															<!-- <span class="ks-amount-icon ks-icon-circled-up-right"></span> -->
													</div>
													<div class="payment-simple-amount-item-description">
															<?php echo "Chat Metrics Revenue";?>
													</div>
											</div>
									</div>
							</div>
							<div class="col-lg-4 col-md-6">
									<div class="card ks-widget-payment-simple-amount-item ks-orange">
											<div class="payment-simple-amount-item-icon-block">
													<span class="la la-star ks-icon"></span>
											</div>

											<div class="payment-simple-amount-item-body">
													<div class="payment-simple-amount-item-amount">

															<span class="ks-amount"><?php echo number_format($leadCount);?></span>
															<!-- <span class="ks-amount-icon ks-icon-circled-up-right"></span> -->
													</div>
													<div class="payment-simple-amount-item-description">
															<?php echo "Total Leads";?>
													</div>
											</div>
									</div>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-4 col-md-6">
									<?php
										$icon = Yii::$app->user->identity->role == 50 ? 'ks-icon-user' : 'la la-star';
									?>
									<div class="card ks-widget-payment-simple-amount-item ks-purple">
											<div class="payment-simple-amount-item-icon-block">
													<span class="<?=$icon?> ks-icon"></span>
											</div>

											<div class="payment-simple-amount-item-body">
													<div class="payment-simple-amount-item-amount">

															<span class="ks-amount"><?php if(Yii::$app->user->identity->role == 50){ echo $users;}else{ echo $leads_total;}?></span>
															<span class="ks-amount-icon ks-icon-circled-up-right"></span>
													</div>
													<div class="payment-simple-amount-item-description">
															<?php if(Yii::$app->user->identity->role == 50){ echo "Client";}else{ echo "Leads";}?>
													</div>
											</div>
									</div>
							</div>
							<div class="col-lg-4 col-md-6">
									<div class="card ks-widget-payment-simple-amount-item ks-purple">
											<div class="payment-simple-amount-item-icon-block">
													<span class="la la-wechat ks-icon"></span>
											</div>

											<div class="payment-simple-amount-item-body">
													<div class="payment-simple-amount-item-amount">
															<span class="ks-amount"><?= $t_chats;?></span>
															<span class="ks-amount-icon ks-icon-circled-up-right"></span>
													</div>
													<div class="payment-simple-amount-item-description">
															Chats
													</div>
											</div>
									</div>
							</div>
							<!-- <div class="col-lg-3 col-md-6">
									<div class="card ks-widget-payment-simple-amount-item ks-green">
											<div class="payment-simple-amount-item-icon-block">
													<span class="la la-thumbs-up ks-icon"></span>
											</div>

											<div class="payment-simple-amount-item-body">
													<div class="payment-simple-amount-item-amount">
															<span class="ks-amount"><?= $t_good;?></span>
															<span class="ks-amount-icon ks-icon-circled-up-right"></span>
													</div>
													<div class="payment-simple-amount-item-description">
															Rate Good
													</div>
											</div>
									</div>
							</div>
							<div class="col-lg-3 col-md-6">
									<div class="card ks-widget-payment-simple-amount-item ks-pink">
											<div class="payment-simple-amount-item-icon-block">
													<span class="la la-thumbs-down ks-icon"></span>
											</div>

											<div class="payment-simple-amount-item-body">
													<div class="payment-simple-amount-item-amount">
															<span class="ks-amount"><?= $t_bad;?></span>
															<span class="ks-amount-icon ks-icon-circled-down-left"></span>
													</div>
													<div class="payment-simple-amount-item-description">
															Rate Bad
													</div>
											</div>
									</div>
							</div> -->
						</div>

				<?php
					}else{
				?>
					<div class="row">
						<div class="col-lg-3 col-md-6">
								<div class="card ks-widget-payment-simple-amount-item ks-purple">
										<div class="payment-simple-amount-item-icon-block">
												<span class="la la-wechat ks-icon"></span>
										</div>

										<div class="payment-simple-amount-item-body">
												<div class="payment-simple-amount-item-amount">
														<span class="ks-amount"><?= $t_chats;?></span>
														<span class="ks-amount-icon ks-icon-circled-up-right"></span>
												</div>
												<div class="payment-simple-amount-item-description">
														Chats
												</div>
										</div>
								</div>
						</div>
							<!-- <div class="col-lg-3 col-md-6">
									<div class="card ks-widget-payment-simple-amount-item ks-green">
											<div class="payment-simple-amount-item-icon-block">
													<span class="la la-thumbs-up ks-icon"></span>
											</div>

											<div class="payment-simple-amount-item-body">
													<div class="payment-simple-amount-item-amount">
															<span class="ks-amount"><?= $t_good;?></span>
															<span class="ks-amount-icon ks-icon-circled-up-right"></span>
													</div>
													<div class="payment-simple-amount-item-description">
															Rate Good
													</div>
											</div>
									</div>
							</div>
							<div class="col-lg-3 col-md-6">
									<div class="card ks-widget-payment-simple-amount-item ks-pink">
											<div class="payment-simple-amount-item-icon-block">
													<span class="la la-thumbs-down ks-icon"></span>
											</div>

											<div class="payment-simple-amount-item-body">
													<div class="payment-simple-amount-item-amount">
															<span class="ks-amount"><?= $t_bad;?></span>
															<span class="ks-amount-icon ks-icon-circled-down-left"></span>
													</div>
													<div class="payment-simple-amount-item-description">
															Rate Bad
													</div>
											</div>
									</div>
							</div> -->
							<div class="col-lg-3 col-md-6">
								  <?php
										$icon = Yii::$app->user->identity->role == 50 ? 'ks-icon-user' : 'la la-star';
									?>
									<div class="card ks-widget-payment-simple-amount-item ks-orange">
											<div class="payment-simple-amount-item-icon-block">
													<span class="la <?=$icon?> ks-icon"></span>
											</div>

											<div class="payment-simple-amount-item-body">
													<div class="payment-simple-amount-item-amount">

															<span class="ks-amount"><?php if(Yii::$app->user->identity->role == 50){ echo $users;}else{ echo $leads_total;}?></span>
															<span class="ks-amount-icon ks-icon-circled-up-right"></span>
													</div>
													<div class="payment-simple-amount-item-description">
															<?php if(Yii::$app->user->identity->role == 50){ echo "Client";}else{ echo "Leads";}?>
													</div>
											</div>
									</div>
							</div>

						<div class="col-lg-3 col-md-6">
							<div class="card ks-widget-payment-simple-amount-item ks-green">
									<div class="payment-simple-amount-item-icon-block">
											<span class="la la-money ks-icon"></span>
									</div>

									<div class="payment-simple-amount-item-body">
											<div class="payment-simple-amount-item-amount">
													<span class="ks-amount"><?= "$".number_format($leads_total * $clientLeadValue);?></span>
													<span class="ks-amount-icon ks-icon-circled-up-right"></span>
											</div>
											<div class="payment-simple-amount-item-description">
													Revenue Opportunities
											</div>
									</div>
							</div>
						</div>
						<!--<div class="col-lg-3 col-md-6">-->
						<!--	<div class="card ks-widget-payment-simple-amount-item ks-purple">-->
						<!--			<div class="payment-simple-amount-item-icon-block">-->
						<!--					<span class="la la-file-text ks-icon"></span>-->
						<!--			</div>-->

						<!--			<div class="payment-simple-amount-item-body">-->
						<!--					<div class="payment-simple-amount-item-amount">-->
						<!--							<span class="ks-amount"></span>-->
						<!--							<span class="ks-amount-icon ks-icon-circled-up-right"></span>-->
						<!--					</div>-->
						<!--					<div class="payment-simple-amount-item-description" >-->
						<!--							Last Month's <br> Investment-->
						<!--					</div>-->
						<!--			</div>-->
						<!--	</div>-->
						<!--</div>-->
					</div>



				<?php
					}
				?>

        <div class="row">
            <div class="col-xl-12">
                <div class="card ks-card-widget ks-widget-chart-orders">
                    <h5 class="card-header">
                        Total Chats Chart


                    </h5>
                    <div class="card-block">
                      <div id="graph-bar" style="height: 240px; padding: 0px; position: relative;"></div>
                    </div>
                </div>
            </div>
        </div>


                        <!--<div class="row">-->
                        <!--    <div class="col-lg-12">-->
                        <!--        <div class="card ks-card-widget ks-payment-widget-table-and-map">-->
                        <!--            <h5 class="card-header">-->
                        <!--              <center>Map Module Under Development</center>-->

                                        <!-- <div class="ks-controls">
                                            <a href="#" class="ks-control-link">This year</a>
                                        </div> -->
                                    </h5>
                                    <!--<div class="card-block">
                                        <div class="row">
                                            <div class="col-lg-5">
                                                <table class="table ks-payment-widget-table-and-map-table">
												<tr>
													<th></th>
													<th>
														Location
													</th>
													<th>
														Chats
													</th>
												</tr>

													<?php $row = 0;?>
													<?php
														$mapData = [];
														$latitude = 37.8136;
														$longitude = 144.9631;

														$notFound = [];
														$gotoCount = 0;
													 ?>
													<?php foreach($finalSurveyData as $key => $value){

														?>

														<tr>
															<td class="ks-flag" width="20">
																<img src="<?php echo Yii::$app->request->baseUrl?>/kosmoassets/img/flags/24/<?php $countryData = explode(", ", $key);echo $countryData[2];?>.png" class="ks-flag">
															</td>
															<td>
																<?php echo $key;?>
															</td>
															<td class="text-right">
																<?php echo $value;?>
															</td>

														</tr>

														<?php

															//if($row == 0){
															  findLoc:
																$address = $key; // Google HQ
															  $prepAddr = str_replace(' ','+',$address);
																$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
																$output= json_decode($geocode,true);
																/*echo "<pre> Output is .. .. "; print_r($output); exit;*/
                                //echo '<pre>',print_r($output['results'][0]['geometry']['location']['lat'],1),'</pre>';

																//$latitude = $output->results[0]->geometry->location->lat;
																//$longitude = $output->results[0]->geometry->location->lng;
																//$latitude = 19.0116260;
																//$longitude = 72.8243610;

																$latitude = isset($output['results'][0]['geometry']['location']['lat']) ? $output['results'][0]['geometry']['location']['lat'] : -37.8136;
																$longitude= isset($output['results'][0]['geometry']['location']['lng']) ? $output['results'][0]['geometry']['location']['lng'] : 144.9631;

																if(isset($output['results'][0]['geometry']['location']['lat'])){
																	//$latitude = $output['results'][0]['geometry']['location']['lat'];
																}else{
																	//$notFound[$key] = $value;
																	$gotoCount++;
                                  if($gotoCount < 10){
                                    goto findLoc;
                                  }
																}

																$mapData[$row]['lat']	= $latitude;
																$mapData[$row]['lon']	= $longitude;
																$mapData[$row]['title']	= $key;
																$mapData[$row]['zoom'] = 10;
															//}
														?>

														<?php $row++;?>
													<?php }?>

													<?php // echo "<pre>"; print_r($notFound); exit;	?>

                                                </table>
                                            </div>
                                            <div class="col-lg-7">

                                                <div id="ks-payment-widget-table-and-map-map" data-height="450"></div>
                                            </div>
                                        </div>
                                    </div>col -->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</div>-->



                    </div>

                </div>
            </div>
        </div>


<?php

$data_str = '';
$data_str2 = '';
$data_str3 = '';
$data_str4 = '';

$num_m = 0;
$num_m2 = 0;
$divide = 1;
$show_bars = 1;
$show_lines = 0;
$show_points = 0;
?>
<script>
	var g_admin = false;
</script>
<?php
if(User::isUserAdmin(Yii::$app->user->identity->username)){ ?>
	<script>
		var g_admin = true;
	</script>
<?php }
//echo '<pre>';print_r(array($leads));echo '</pre>';
if($data2){
	foreach($data2 as $key => $ds){
		if(isset($_GET['group_by']) && $_GET['group_by']=='hour'){
			$data_str .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.$ds->chats.'],';
			$num_m = 1;
			$divide = 24;
		}else{
			$data_str .= '['.strtotime($key)*1000 .','.$ds->chats.'],';
			$num_m++;
			$num_m2++;
		}
	}
}

if($data){
	foreach($data as $key => $ds){
		if(isset($_GET['group_by']) && $_GET['group_by']=='hour'){
		$data_str2 .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.$ds->good.'],';
		$num_m = 1;
		$divide = 24;
		}else{
			$data_str2 .= '['.strtotime($key)*1000 .','.$ds->good.'],';
			$num_m++;
			$num_m2++;
		}
	}
}

if($data){
	foreach($data as $key => $ds){
		if(isset($_GET['group_by']) && $_GET['group_by']=='hour'){
		$data_str4 .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.$ds->bad.'],';
		$num_m = 1;
		$divide = 24;
		}else{
			$data_str4 .= '['.strtotime($key)*1000 .','.$ds->bad.'],';
			$num_m++;
			$num_m2++;
		}
	}
}

if($leads){
	foreach($leads as $key => $ds){
		if(isset($_GET['group_by']) && $_GET['group_by']=='hour'){
		$data_str3 .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.$ds->chats.'],';
		$num_m = 1;
		$divide = 24;
		}else{
			$data_str3 .= '['.strtotime($key)*1000 .','.$ds->chats.'],';
			$num_m++;
			$num_m2++;
		}
	}
}

if($num_m2 > 10){
	$show_bars = 0;
	$show_lines = 1;
	$show_points = 1;
}

$show_bars = 0;
$show_lines = 1;
$show_points = 1;

//[gd(2015, 1, 1), 838], [gd(2015, 1, 2), 749], [gd(2015, 1, 3), 634], [gd(2015, 1, 4), 1080], [gd(2015, 1, 5), 850], [gd(2015, 1, 6), 465], [gd(2015, 1, 7), 453], [gd(2015, 1, 8), 646], [gd(2015, 1, 9), 738], [gd(2015, 1, 10), 899], [gd(2015, 1, 11), 830], [gd(2015, 1, 12), 789]

$markerData = json_encode($mapData);

$this->registerJs(
    "$('document').ready(function(){
		var latitude = $latitude;
		var longitude = $longitude;

		console.log('Goto count: ".$gotoCount."');

		jQuery('#dateto,#datefrom').datepicker( {dateFormat: 'yy-mm-dd',changeMonth:true,changeYear:false} );

		$('#c_range').click(function(e){
			e.preventDefault();
			$('#range-form').show();
		})

		function gd(year, day, month) {
			return new Date(year, month, day).getTime();
		}

		if ($('#graph-bar').length) {
			var data1 = [
			    $data_str
			];

			var data2 = [
			    $data_str2
			];

			var data3 = [
			    $data_str3
			];

			var data4 = [
			    $data_str4
			];

			var series = new Array();

			series.push({
				data: data1,
				color:'#03A9F4',
				bars: {
					//show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000/$divide,
					lineWidth: 1/$num_m,
					fill: 1,
					align: 'center'
				},
				label: 'Chats',
				lines: {
					show : true,
					//fill: 1,
					//show : $show_lines,
					//lineWidth: 3,
				},
				points: {
					//fillColor: '#ffffff',
					//pointWidth: 1,
					show: true,
					//show: $show_points
				}
			});

			if(!g_admin){
				series.push({
					data: data3,
					color: '#9C27B0',
					lines: {
						show : true,
						//lineWidth: 3,
					},
					points: {
						//fillColor: '#e84e40',
						//fillColor: '#ffffff',
						//pointWidth: 1,
						show: true
					},
					label: 'Leads'
				});
			}

			series.push({
				data: data4,
				color: '#E84E40',
				lines: {
					show : true,
					//lineWidth: 3,
				},
				points: {
					fillColor: '#9C27B0',
					//fillColor: '#ffffff',
					//pointWidth: 1,
					show: true
				},
				label: 'Rate Bad'
			});

			series.push({
				data: data2,
				color: '#8BC34A',
				lines: {
					show : true,
					//lineWidth: 3,
				},
				points: {
					//fillColor: '#e84e40',
					//fillColor: '#ffffff',
					//pointWidth: 1,
					show: true
				},
				label: 'Rate Good'
			});


			$.plot('#graph-bar', series, {
				//colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
				ticks : 10,
				grid: {
					//tickColor: '#f2f2f2',
					//borderWidth: 0,
					hoverable: true,
					//clickable: true
				},
				legend: {
					noColumns: 1,
					// labelBoxBorderColor: '#000000',
					// position: 'ne'
				},
				//shadowSize: 0,
				xaxis: {
					mode: 'time',
					//timeformat: '%I %p',
					//timeformat:'%S'
				}
			});

			var previousPoint = null;
			$('#graph-bar').bind('plothover', function (event, pos, item) {
					//console.log(item.series);
				if (item) {

					if (previousPoint != item.dataIndex) {

						previousPoint = item.dataIndex;

						$('#flot-tooltip').remove();
						var x = item.datapoint[0],
						y = item.datapoint[1];

						showTooltip(item.pageX, item.pageY, item.series.label, y , x );
					}
				}
				else {
					$('#flot-tooltip').remove();
					previousPoint = [0,0,0];
				}
			});

			function showTooltip(x, y, label, data , x2) {
				$('<div id=\"flot-tooltip\">' + '<b>' + label + ': </b><i>' + data + '</i><br><!--b>Time : </b-->' + '' + '</div>').css({
					top: y + 5,
					left: x + 20
				}).appendTo('body').fadeIn(200);
			}
		}

		var data = [{
				lat: latitude,
				lon: longitude,
				title: 'ChatMap',
				zoom: 5
			}
		];

		var maplace = new Maplace({
            map_div: '#ks-payment-widget-table-and-map-map',
						locations: $markerData,
            controls_type: 'list',
						controls_on_map: false,
						//map_options: { center: new google.maps.LatLng(-25.378619, 134.464168), zoom: 4}
						//map_options: { set_center: [-25.378619, 134.464168], zoom: 4}
    });
    maplace.Load();
	});"
, \yii\web\View::POS_END);
