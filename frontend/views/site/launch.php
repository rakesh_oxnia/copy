<?php use yii\helpers\Html;

use common\models\User;

$this->title = "Launch";
?>
<div class="ks-page-header">
	<section class="ks-title">
		<h3>Welcome</h3>
		        <?php if(Yii::$app->session->has('wp_signup')) { ?>
    	   <a href="<?= Yii::$app->session->get('wp_signup') ?>" class="btn btn-success" target="_blank">Go Back to Your WordPress Site</a>
        <?php } ?>
	</section>
</div>
<div class="ks-page-content d-flex justify-content-center align-items-center">
	<div class="ks-page-content-body d-flex">
    <iframe width="750" height="500"
    		src="https://www.youtube.com/embed/U6o6AeSAgow">
    </iframe>
	</div>
</div>
