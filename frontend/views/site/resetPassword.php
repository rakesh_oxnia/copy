<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;


$host = explode('.', $_SERVER['HTTP_HOST']);
$username = $host[0];

//$theme_color = 'theme-whbl';
$agencylogo = Yii::$app->homeUrl.'cube/img/logo.png';
if($username != '' && $username != 'fubaco')
{
	$row = (new \yii\db\Query())
    ->from('user')
    ->where(['role' => 30, 'username' => $username])
    ->one();
    
    $agencylogoimg = $row['agency_logo']; 
    if( $agencylogoimg)
	{
		//$theme_color = Yii::$app->user->identity->theme_color;
		$agencylogo = Yii::$app->homeUrl.'logo_images/'.$agencylogoimg;	
	}	
}
?>



 <?php $form = ActiveForm::begin(['options' => ['class' => 'form-container']]); ?>
	 
					 
								
								
                    <h4 class="ks-header">Reset Password</h4>
                       
                            <?= $form->field($model, 'password', [
							'template' => '<div class="input-icon icon-left icon-lg icon-color-primary">{input}<span class="icon-addon"><span class="la la-key"></span></span></div>{error}{hint}'])->passwordInput(["placeholder"=>"Password"]) ?>
                        
						
                    
					<?= Html::submitButton('Reset', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                        
                    
                 
                    
   <?php ActiveForm::end(); ?>
   