<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
?>	
	 
	 <?php $form = ActiveForm::begin(['options' => ['class' => 'form-container']]); ?>
	 
					 
								
								
                    <h4 class="ks-header">Reset Password</h4>
                       
                            <?= $form->field($model, 'email', [
							'template' => '<div class="input-icon icon-left icon-lg icon-color-primary">{input}<span class="icon-addon"><span class="la la-at"></span></span></div>{error}{hint}'])->textInput(["placeholder"=>"Email"]) ?>
                        
						
                    
					<?= Html::submitButton('Reset', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                        
                    
                 
                    
   <?php ActiveForm::end(); ?>