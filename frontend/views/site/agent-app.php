<?php
use yii\helpers\Html;
?>

<div class="chat-plugin" id="chatPluginContainer">
    <ul class="chat-plugin__menu">
        <li class="chat-plugin__menu__item chat-plugin__menu__item--active"><a href=""><span class="app-icon app-icon-dashboard" title="Contact"></span></a></li>
    </ul>
    <section class="chat-plugin__content">
        <div class="chat-plugin__content--centered">
            <p class="chat-plugin__p">
               Chatmetrics Well hello there!<br/>Pick a visitor or chat to view details.
            </p>
        </div>
        <dl class="chat-plugin__dl">
            <dt>Visitor ID</dt>
            <dd id="visitorID"></dd>
            <dt>Visitor Name</dt>
            <dd id="">
              <input type="text" name="visitorName" id="visitorName" />
            </dd>
            <dt>Visitor Email</dt>
            <dd id="">
              <input type="text" name="visitorEmail" id="visitorEmail" />
            </dd>
            <dt>Visitor Phone</dt>
            <dd id="">
              <input type="text" name="visitorPhone" id="visitorPhone" />
            </dd>
            <dt>Chat ID</dt>
            <dd id="chatID"></dd>
            <dt>Group ID</dt>
            <dd id="groupID"></dd>
            <dt>Source</dt>
            <dd id="source"></dd>
            <dt>Category:</dt>
            <dd id="">
              <select id='category_input'>
                <option value='0'>Select Category</option>
                <option value='1'>Category 1</option>
                <option value='2'>Category 2</option>
                <option value='3'>Category 3</option>
                <option value='4'>Category 4</option>
                <option value='5'>Category 5</option>
              </select>
            </dd>
            <dt>Outlet:</dt>
            <dd id="">
              <select id='outlet_input'>
                <option value='0'>Select Outlet</option>
                <option value='1'>Outlet 1</option>
                <option value='2'>Outlet 2</option>
                <option value='3'>Outlet 3</option>
                <option value='4'>Outlet 4</option>
                <option value='5'>Outlet 5</option>
                <option value='5'>Outlet 6</option>
                <option value='5'>Outlet 7</option>
                <option value='5'>Outlet 8</option>
                <option value='5'>Outlet 9</option>
                <option value='5'>Outlet 10</option>
              </select>
            </dd>
            <dt>Tags:</dt>
            <dd id="">
              <select id='tags_input' multiple >
                <option value='1'>Lead</option>
                <option value='2'>Sales</option>
                <option value='3'>Support</option>
                <option value='4'>Brick</option>
                <option value='5'>Positive feedback</option>
                <option value='5'>Complaint</option>
                <option value='5'>My tag</option>
                <option value='5'>Spam</option>
              </select>
            </dd>
        </dl>
        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
        <div style='margin-left: 50px; padding: 50px;'>
          <div style='padding: 20px;'>
            <button style="margin-left: 118px;" class='btn submit-btn btn-success'>Submit</button>
          </div>
        </div>
    </section>
</div>
<div style='margin-left: 50px; padding: 50px;'>
  <div class="livechat-login-button"></div>
</div>
