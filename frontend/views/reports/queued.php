<?php
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Queued Visitors';
$this->params['breadcrumbs'][] = $this->title;  ?>

<div class="col-lg-12">
	<div class="row">
		<div class="col-lg-12">
			<div id="content-header" class="clearfix">
				<div class="pull-left">
					<ol class="breadcrumb">
						<li><a href="<?= Yii::$app->homeUrl;?>">Home</a></li>
						<li class="active"><span><?= Html::encode($this->title) ?></span></li>
					</ol>
					<h1><?= Html::encode($this->title) ?></h1>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="main-box">
				<div class="main-box-body clearfix">
					<div class="row">
						<br>
						Filter By <div class="btn-group">
							<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
								<?php 
								if(isset($_GET["filter"])){
									if($_GET["filter"] == 'crange')
										echo 'Custom Range';
									elseif($_GET["filter"] == 'last7')
										echo 'Last 7days';
									elseif($_GET["filter"] == 'last30')
										echo 'Last 30days';
									else
										echo $_GET["filter"];
								}
								else{
									echo "Last 7days";
								}
								?>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<?php $today = date('Y-m-d');?>
								<li><a href="?filter=today&date_from=<?=$today ;?>&date_to=<?=$today ;?>&group_by=hour">Today</a></li>
								<?php $yesterday = date('Y-m-d');?>
								<?php $last75 = date('Y-m-d',strtotime("-1 days"));?>
								<li><a href="?filter=yesterday&date_from=<?=$last75 ;?>&date_to=<?=$last75 ;?>&group_by=hour">Yesterday</a></li>
								<?php $last7 = date('Y-m-d',strtotime("-7 days"));?>
								<li><a href="?filter=last7&date_from=<?=$last7 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 7days</a></li>
								<?php $last30 = date('Y-m-d',strtotime("-30 days"));?>
								<li><a href="?filter=last30&date_from=<?=$last30 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 30days</a></li>
								<li><a href="?filter=crange" id="c_range">Custom Range</a></li>
							</ul>
						</div>
					
						<div class="daterun"> 
							<form id="range-form" style="display:none;" class="top-formsearch">
								<ul>
									<li><label>Date From</label><input type="text" id="datefrom" name="date_from"></li>
									<li><label>Date To</label><input type="text" id="dateto" name="date_to"></li>
									<input type="hidden" name="filter" value="crange">
									<input type="hidden" name="group_by" value="day">
									<li><input type="submit" value="Filter"></li>
								</ul>							
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
	<?php
	
	$data_str = '';
$data_str2 = '';
$data_str3 = '';
$data_str4 = '';
$data_str5 = '';
$num_m = 0;
$num_m2 = 0;
$divide = 1;
$show_bars = 1;
$show_lines = 0;
$show_points = 0;
$enter_queued_t = 0;
$enter_queued_t = 0;
$el_queued_t = 0;
$enter_queued_c	 = 0;
$enter_queued_c2	 = 0;
$queued = 0;
$left_queue = 0;
$entered_chat = 0;

foreach($data as $key => $ds){
	if(isset($_GET['group_by']) && $_GET['group_by']=='hour'){
		$data_str .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.$ds->queued->count.'],';
		$data_str2 .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.$ds->left_queue->count.'],';
		$data_str3 .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.$ds->entered_chat->count.'],';
		$num_m = 1;
		$queued += $ds->queued->count;
		$left_queue += $ds->left_queue->count;
		$entered_chat += $ds->entered_chat->count;
		$divide = 24;
	}else{
		$data_str .= '['.strtotime($key)*1000 .','.$ds->queued->count.'],';
		$data_str2 .= '['.strtotime($key)*1000 .','.$ds->left_queue->count.'],';
		$data_str3 .= '['.strtotime($key)*1000 .','.$ds->entered_chat->count.'],';
		$num_m++;
		$num_m2++;
		$queued += $ds->queued->count;
		$left_queue += $ds->left_queue->count;
		$entered_chat += $ds->entered_chat->count;
	}
}

foreach($waitings as $key => $ds){
	if(isset($_GET['group_by']) && $_GET['group_by']=='hour'){
		$data_str4 .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.$ds->entered_chat->average->seconds.'],';
		$data_str5 .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.$ds->left_queue->average->seconds.'],';
		
		$enter_queued_t += $ds->entered_chat->average->seconds;
		$el_queued_t += $ds->left_queue->average->seconds;
		$enter_queued_c++;
		$enter_queued_c2++;
		
		$num_m = 1;
		$divide = 24;
	}else{
		//$data_str4 .= '['.strtotime($key)*1000 .','.$ds->entered_chat->average->seconds.'],';
		//$data_str5 .= '['.strtotime($key)*1000 .','.$ds->left_queue->average->seconds.'],';
		
		if($ds->entered_chat->average->seconds){
			$data_str4 .= '['.strtotime($key)*1000 .','.$ds->entered_chat->average->seconds.'],';
			$enter_queued_t += $ds->entered_chat->average->seconds;
			$enter_queued_c++;
		}else{
			$data_str4 .= '['.strtotime($key)*1000 .',0],';
		}
		
		if($ds->left_queue->average->seconds){
			$data_str5 .= '['.strtotime($key)*1000 .',0],';
			$el_queued_t += $ds->left_queue->average->seconds;
			$enter_queued_c2++;
		}else{
			$data_str5 .= '['.strtotime($key)*1000 .',0],';
		}
	}
}

if($enter_queued_t){
	$avgg = floor($enter_queued_t/$enter_queued_c);
}
else{
	$avgg = '0';
}

if($el_queued_t){
	$avgg2 = floor($el_queued_t/$enter_queued_c2);
}
else{
	$avgg2 = '0';
}


if($num_m2 > 10){
	$show_bars = 0;
	$show_lines = 1;
	$show_points = 1;
}

$show_bars = 0;
$show_lines = 1;
$show_points = 1;

?>
	<div class="row">
		<div class="col-md-12">
			<div class="main-box">
				<header class="main-box-header clearfix">
					<h2 class="pull-left">Queue conversion</h2>
				</header>
				<div class="main-box-body clearfix">
					<div class="row">
						<div class="col-md-9">
							<div id="graph-bar" style="height: 240px; padding: 0px; position: relative;"></div>
						</div>
						<div class="col-md-3">
							<p><?= $queued;?> queued</p>
							<p><?= $entered_chat;?> entered the chat</p>
							<p><?= $left_queue;?> left the queue</p>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<div class="main-box">
				<header class="main-box-header clearfix">
					<h2 class="pull-left">Average waiting time</h2>
				</header>
				<div class="main-box-body clearfix">
					<div class="row">
						<div class="col-md-9">
							<div id="graph-bar2" style="height: 240px; padding: 0px; position: relative;"></div>
						</div>
						<div class="col-md-3">
							<p>time to enter the chat ( average: <?=$avgg;?> s )</p>
							<p>time to leave the queue ( average: <?=$avgg2;?> s )</p>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>
<?php 

$this->registerJs(
    "$('document').ready(function(){
		jQuery('#dateto,#datefrom').datepicker( {dateFormat: 'yy-mm-dd',changeMonth:true,changeYear:false} );
		
		$('#c_range').click(function(e){
			e.preventDefault();
			$('#range-form').show();
		})
		function gd(year, day, month) {
			return new Date(year, month, day).getTime();
		}
		if ($('#graph-bar').length) {
			var data1 = [
			    $data_str
			];
			var data2 = [
			    $data_str2
			];
			
			var data3 = [
			    $data_str3
			];
			
			var series = new Array();
			
			series.push({
				data: data1,
				bars: {
					show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000 / parseInt($divide),
					lineWidth: 1 / parseInt($num_m),
					fill: 1,
					align: 'center'
				},
				label: 'Queued',
				lines: {
					show : $show_lines,
					//fill: 1,
				},
				points: { 
					show: $show_points 
				}
			});
			
			series.push({
				data: data2,
				bars: {
					show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000 / parseInt($divide),
					lineWidth: 1 / parseInt($num_m),
					fill: 1,
					align: 'center'
				},
				label: 'Left Queued',
				lines: {
					show : $show_lines,
					//fill: 1,
				},
				points: { 
					show: $show_points 
				}
			});
			
			series.push({
				data: data3,
				bars: {
					show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000 / parseInt($divide),
					lineWidth: 1 / parseInt($num_m),
					fill: 1,
					align: 'center'
				},
				label: 'Entered chat',
				lines: {
					show : $show_lines,
					//fill: 1,
				},
				points: { 
					show: $show_points 
				}
			});
			
			$.plot('#graph-bar', series, {
				colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
				ticks : 10,
				grid: {
					tickColor: '#f2f2f2',
					//borderWidth: 0,
					hoverable: true,
					//clickable: true
				},
				legend: {
					noColumns: 1,
					// labelBoxBorderColor: '#000000',
					// position: 'ne'     
				},
				shadowSize: 0,
				xaxis: {
					mode: 'time',
					//timeformat: '%d %b',
					//timeformat:'%S'
				}
			});

			var previousPoint = null;
			$('#graph-bar').bind('plothover', function (event, pos, item) {
				if (item) {
					if (previousPoint != item.dataIndex) {

						previousPoint = item.dataIndex;

						$('#flot-tooltip').remove();
						var x = item.datapoint[0],
						y = item.datapoint[1];

						showTooltip(item.pageX, item.pageY, item.series.label, y );
					}
				}
				else {
					$('#flot-tooltip').remove();
					previousPoint = [0,0,0];
				}
			});
			function showTooltip(x, y, label, data) {
				$('<div id=\"flot-tooltip\">' + '<b>' + label + ': </b><i>' + data + '</i>' + '</div>').css({
					top: y + 5,
					left: x + 20
				}).appendTo('body').fadeIn(200);
			}
		}
		
		if ($('#graph-bar2').length) {
			var data4 = [
			    $data_str4
			];
			var data5 = [
			    $data_str5
			];
						
			var series = new Array();
			
			series.push({
				data: data4,
				bars: {
					show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000 / parseInt($divide),
					lineWidth: 1 / parseInt($num_m),
					fill: 1,
					align: 'center'
				},
				label: 'time to enter the chat',
				lines: {
					show : $show_lines,
					//fill: 1,
				},
				points: { 
					show: $show_points 
				}
			});
			
			series.push({
				data: data5,
				bars: {
					show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000 / parseInt($divide),
					lineWidth: 1 / parseInt($num_m),
					fill: 1,
					align: 'center'
				},
				label: 'time to leave the queue',
				lines: {
					show : $show_lines,
					//fill: 1,
				},
				points: { 
					show: $show_points 
				}
			});
			
			$.plot('#graph-bar2', series, {
				colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
				ticks : 10,
				grid: {
					tickColor: '#f2f2f2',
					//borderWidth: 0,
					hoverable: true,
					//clickable: true
				},
				legend: {
					noColumns: 1,
					// labelBoxBorderColor: '#000000',
					// position: 'ne'     
				},
				shadowSize: 0,
				xaxis: {
					mode: 'time',
					//timeformat: '%d %b',
					//timeformat:'%S'
				}
			});

			var previousPoint = null;
			$('#graph-bar2').bind('plothover', function (event, pos, item) {
				if (item) {
					if (previousPoint != item.dataIndex) {

						previousPoint = item.dataIndex;

						$('#flot-tooltip').remove();
						var x = item.datapoint[0],
						y = item.datapoint[1];

						showTooltip(item.pageX, item.pageY, item.series.label, y );
					}
				}
				else {
					$('#flot-tooltip').remove();
					previousPoint = [0,0,0];
				}
			});
			function showTooltip(x, y, label, data) {
				$('<div id=\"flot-tooltip\">' + '<b>' + label + ': </b><i>' + data + ' s</i>' + '</div>').css({
					top: y + 5,
					left: x + 20
				}).appendTo('body').fadeIn(200);
			}
		}

	});"
, \yii\web\View::POS_END);