<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;
use yii2assets\printthis\PrintThis;
use yii\widgets\ActiveForm;
use app\models\Leads;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Leads';
$this->params['breadcrumbs'][] = $this->title;

?>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/libs/swiper/css/swiper.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/widgets/tables.min.css">

		<div class="ks-page-header">
            <section class="ks-title">
                <h3>Total Chat</h3>
                <div class="col-md-3">
	          <?php if (!empty($website_users)) { 
	          $form = ActiveForm::begin([
	                  'method' => 'post',
	          ]); ?>          
	          <select id='website_selecter' name="website_filter" class='form-control' onchange="this.form.submit()">              
	              <option value=''>Select Website</option>
	              <?php foreach ($website_users as $website_user) { 
	                if ($website_user->user_group == Yii::$app->session->get('website_filter')) {
	                  echo "<option value='$website_user->user_group' selected>$website_user->website_url</option>";
	                } else {
	                  echo "<option value='$website_user->user_group'>$website_user->website_url</option>";
	                }                
	              } 
	              ?>
	           </select>
	           <?php ActiveForm::end(); ?>
	          <?php } ?>
	         </div> 
            </section>
        </div>

        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="container-fluid ks-rows-section">
                    <div class="row">
                        <div class="col-lg-12 ks-panels-column-section">
                            <div class="card panel ks-information ks-light">
                                <h5 class="card-header">


							<div class="btn-group">
							<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">
								<?php
								if(isset($_GET["filter"])){
									if($_GET["filter"] == 'crange')
										echo 'Custom Range';
									elseif($_GET["filter"] == 'last7')
										echo 'Last 7days';
									elseif($_GET["filter"] == 'last30')
										echo 'Last 30days';
									else
										echo $_GET["filter"];
								}
								else{
									echo "Last 7days";
								}
								?>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-left" role="menu">
								<?php $today = date('Y-m-d');?>
								<li><a href="<?=Url::to(['reports/stats'])?>&filter=today&date_from=<?=$today ;?>&date_to=<?=$today ;?>&group_by=hour">Today</a></li>
								<?php $yesterday = date('Y-m-d');?>
								<?php $last75 = date('Y-m-d',strtotime("-1 days"));?>
								<li><a href="<?=Url::to(['reports/stats'])?>&filter=yesterday&date_from=<?=$last75 ;?>&date_to=<?=$last75 ;?>&group_by=hour">Yesterday</a></li>
								<?php $last7 = date('Y-m-d',strtotime("-6 days"));?>
								<li><a href="<?=Url::to(['reports/stats'])?>&filter=last7&date_from=<?=$last7 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 7days</a></li>
								<?php $last30 = date('Y-m-d',strtotime("-29 days"));?>
								<li><a href="<?=Url::to(['reports/stats'])?>&filter=last30&date_from=<?=$last30 ;?>&date_to=<?=$yesterday ;?>&group_by=day">Last 30days</a></li>
								<li><a href="<?=Url::to(['reports/stats'])?>&filter=crange" id="c_range">Custom Range</a></li>
							</ul>

							<form id="range-form" style="display:none;margin-left : 10px;" class="top-formsearch">
								 <div class="row">
									<div class="col-sm-4">
										<input type="text" placeholder="Date From" class="form-control" id="datefrom" name="date_from">
									</div>
									<div class="col-sm-4">
										<input type="text" placeholder="Date To" class="form-control" id="dateto" name="date_to">
									</div>
									<input type="hidden" name="filter" value="crange">
									<input type="hidden" name="group_by" value="day">
									<div class="col-sm-4">
										<input type="submit" class="btn btn-info" value="Filter">
									</div>
								</div>
							</form>
							<div style="margin-left:10px;">
								 <?php
									echo PrintThis::widget([
										'htmlOptions' => [
											'id' => 'PrintThis',
											'btnClass' => 'btn btn-primary',
											'btnId' => 'btnPrintThis',
											'btnText' => 'Print',
											'btnIcon' => 'fa fa-print'

										],
										'options' => [
											'debug' => false,
											'importCSS' => true,
											'importStyle' => false,
											'loadCSS' => "cube/css/bootstrap/bootstrap.min.css",
											'pageTitle' => "",
											'removeInline' => false,
											'printDelay' => 333,
											'header' => null,
											'formValues' => true,
											'canvas' => true,
										]
									]);
									?>
							</div>
						</div>






                                </h5>
                                <div class="card-block ks-datatable">
      <?php

	$data_str = '';
	$data_str2 = '';
	$data_str3 = '';
	$data_str4 = '';
$num_m = 0;
$num_m2 = 0;
$divide = 1;
$show_bars = 1;
$show_lines = 0;
$show_points = 0;
$total_chats = 0;
$total_greeting = 0;
$started_customer = 0;
$started_agent = 0;
foreach($data as $key => $ds){
	if(isset($_GET['group_by']) && $_GET['group_by']=='hour'){
		$data_str .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.$ds->chats.'],';
		$total_chats += $ds->chats;
		$num_m = 1;
		$divide = 24;
	}else{
		$data_str .= '['.strtotime($key)*1000 .','.$ds->chats.'],';
		$total_chats += $ds->chats;
		$num_m++;
		$num_m2++;
	}
}
foreach($engagement as $key => $ds){
	if(isset($_GET['group_by']) && $_GET['group_by']=='hour'){
		$data_str2 .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.$ds->chats_from_auto_invite.'],';
		$data_str3 .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.$ds->chats_from_manual_invite.'],';
		$data_str4 .= '['.strtotime($_GET['date_from'].' '.$key.':00')*1000 .','.$ds->chats_from_immediate_start.'],';

		$total_greeting += $ds->chats_from_auto_invite;
		$started_customer += $ds->chats_from_manual_invite;
		$started_agent += $ds->chats_from_immediate_start;

		$num_m = 1;
		$divide = 24;
	}else{
		$data_str2 .= '['.strtotime($key)*1000 .','.$ds->chats_from_auto_invite.'],';
		$data_str3 .= '['.strtotime($key)*1000 .','.$ds->chats_from_manual_invite.'],';
		$data_str4 .= '['.strtotime($key)*1000 .','.$ds->chats_from_immediate_start.'],';

		$total_greeting += $ds->chats_from_auto_invite;
		$started_customer += $ds->chats_from_manual_invite;
		$started_agent += $ds->chats_from_immediate_start;

	}
}
if($num_m2 > 10){
	$show_bars = 0;
	$show_lines = 1;
	$show_points = 1;
}
$show_bars = 0;
$show_lines = 1;
$show_points = 1;

?>
<div id="PrintThis">
	<div class="row">
		<div class="col-md-12">
			<div class="main-box">
				<header class="main-box-header clearfix">
					<h2 class="pull-left">Total Chats</h2>
				</header>
				<div class="main-box-body clearfix">
					<div class="row">
						<div class="col-md-9">
							<div id="graph-bar" style="height: 240px; padding: 0px; position: relative;"></div>
						</div>
						<div class="col-md-3">
							<?= $total_chats;?> Chats
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="main-box">
				<header class="main-box-header clearfix">
					<h2 class="pull-left">Chat engagement</h2>
				</header>
				<div class="main-box-body clearfix">
					<div class="row">
						<div class="col-md-9">
							<div id="graph-bar2" style="height: 240px; padding: 0px; position: relative;"></div>
						</div>
						<div class="col-md-3">
							<p><?= $total_greeting;?> from greeting</p>
							<p><?= $started_agent;?>  started by customer</p>
							<p><?= $started_customer;?> started by agent</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>


<?php

$this->registerJs(
    "$('document').ready(function(){
		jQuery('#dateto,#datefrom').datepicker( {dateFormat: 'yy-mm-dd',changeMonth:true,changeYear:false} );
		$('#c_range').click(function(e){
			e.preventDefault();
			$('#range-form').show();
		});
		$('#range-form').on('submit', function(e){
			from = $('#datefrom').val();
				to = $('#dateto').val();
			console.log('From: ' + from);
			console.log('To: ' + to);
			url = '".Url::to(['reports/stats'])."&filter=crange&date_from=' + from + '&date_to=' + to + '&group_by=day';
			window.location = url;
			return false;
		});
		function gd(year, day, month) {
			return new Date(year, month, day).getTime();
		}
		if ($('#graph-bar').length) {
			var data1 = [
			    $data_str
			];
			var data2 = [
			    [gd(2015, 1, 1), 342], [gd(2015, 1, 2), 721], [gd(2015, 1, 3), 493], [gd(2015, 1, 4), 403], [gd(2015, 1, 5), 657], [gd(2015, 1, 6), 782], [gd(2015, 1, 7), 609], [gd(2015, 1, 8), 543], [gd(2015, 1, 9), 599], [gd(2015, 1, 10), 359], [gd(2015, 1, 11), 783], [gd(2015, 1, 12), 680]
			];
			var series = new Array();
			//console.log('$divide');

			series.push({
				data: data1,
				bars: {
					show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000 / parseInt($divide),
					lineWidth: 1 / parseInt($num_m),
					fill: 1,
					align: 'center'
				},
				label: 'Chats',
				lines: {
					show : $show_lines,
					//fill: 1,
					//lineWidth: 3,
				},
				points: {
					//fillColor: '#e84e40',
					//fillColor: '#ffffff',
					//pointWidth: 1,
					show: $show_points
				}

			});

			$.plot('#graph-bar', series, {
				colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
				ticks : 10,
				grid: {
					tickColor: '#f2f2f2',
					//borderWidth: 0,
					hoverable: true,
					//clickable: true
				},
				legend: {
					noColumns: 1,
					// labelBoxBorderColor: '#000000',
					// position: 'ne'
				},
				shadowSize: 0,
				xaxis: {
					mode: 'time',
					//timeformat: '%d %b',
					//timeformat:'%S'
				}
			});

			var previousPoint = null;
			$('#graph-bar').bind('plothover', function (event, pos, item) {
				if (item) {
					if (previousPoint != item.dataIndex) {

						previousPoint = item.dataIndex;

						$('#flot-tooltip').remove();
						var x = item.datapoint[0],
						y = item.datapoint[1];

						showTooltip(item.pageX, item.pageY, item.series.label, y );
					}
				}
				else {
					$('#flot-tooltip').remove();
					previousPoint = [0,0,0];
				}
			});
			function showTooltip(x, y, label, data) {
				$('<div id=\"flot-tooltip\">' + '<b>' + label + ': </b><i>' + data + '</i>' + '</div>').css({
					top: y + 5,
					left: x + 20
				}).appendTo('body').fadeIn(200);
			}
		}

		if ($('#graph-bar2').length) {
			var data1 = [
			    $data_str2
			];
			var data2 = [
			    $data_str3
			];
			var data3 = [
			    $data_str4
			];
			var series = new Array();
			//console.log('$divide');

			series.push({
				data: data1,
				bars: {
					show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000 / parseInt($divide),
					lineWidth: 1 / parseInt($num_m),
					fill: 1,
					align: 'center'
				},
				label: 'from greeting',
				lines: {
					show : $show_lines,
					//fill: 1,
					//lineWidth: 3,
				},
				points: {
					//fillColor: '#e84e40',
					//fillColor: '#ffffff',
					//pointWidth: 1,
					show: $show_points
				}

			});

			series.push({
				data: data2,
				bars: {
					show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000 / parseInt($divide),
					lineWidth: 1 / parseInt($num_m),
					fill: 1,
					align: 'center'
				},
				label: 'started by agent',
				lines: {
					show : $show_lines,
					//fill: 1,
					//lineWidth: 3,
				},
				points: {
					//fillColor: '#e84e40',
					//fillColor: '#ffffff',
					//pointWidth: 1,
					show: $show_points
				}

			});

			series.push({
				data: data3,
				bars: {
					show : $show_bars,
					barWidth: 24 * 60 * 60 * 1000 / parseInt($divide),
					lineWidth: 1 / parseInt($num_m),
					fill: 1,
					align: 'center'
				},
				label: 'started by customer',
				lines: {
					show : $show_lines,
					//fill: 1,
					//lineWidth: 3,
				},
				points: {
					//fillColor: '#e84e40',
					//fillColor: '#ffffff',
					//pointWidth: 1,
					show: $show_points
				}

			});

			$.plot('#graph-bar2', series, {
				colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
				ticks : 10,
				grid: {
					tickColor: '#f2f2f2',
					//borderWidth: 0,
					hoverable: true,
					//clickable: true
				},
				legend: {
					noColumns: 1,
					// labelBoxBorderColor: '#000000',
					// position: 'ne'
				},
				shadowSize: 0,
				xaxis: {
					mode: 'time',
					//timeformat: '%d %b',
					//timeformat:'%S'
				},
				yaxis:{
					tickDecimals:false,
				}
			});

			var previousPoint = null;
			$('#graph-bar2').bind('plothover', function (event, pos, item) {
				if (item) {
					if (previousPoint != item.dataIndex) {

						previousPoint = item.dataIndex;

						$('#flot-tooltip').remove();
						var x = item.datapoint[0],
						y = item.datapoint[1];

						showTooltip(item.pageX, item.pageY, item.series.label, y );
					}
				}
				else {
					$('#flot-tooltip').remove();
					previousPoint = [0,0,0];
				}
			});
			function showTooltip(x, y, label, data) {
				$('<div id=\"flot-tooltip\">' + '<b>' + label + ': </b><i>' + data + '</i>' + '</div>').css({
					top: y + 5,
					left: x + 20
				}).appendTo('body').fadeIn(200);
			}
		}
		var tax_data = [
			{'period': '2011 Q3', 'licensed': 3407, 'sorned': 660},
			{'period': '2011 Q2', 'licensed': 3351, 'sorned': 629}
		];
		/* graphLine = Morris.Line({
			element: 'hero-graph',
			data: tax_data,
			lineColors: ['#8bc34a', '#ffc107', '#e84e40', '#03a9f4', '#9c27b0', '#90a4ae'],
			xkey: 'period',
			ykeys: ['licensed', 'sorned'],
			labels: ['Licensed', 'Off the road'],
			resize: true
		}); */

	});"
, \yii\web\View::POS_END);
