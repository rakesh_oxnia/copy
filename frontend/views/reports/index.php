<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reports';
$this->params['breadcrumbs'][] = $this->title;

?>
<h1><?= Html::encode($this->title) ?></h1>
<div class="user-index col-md-12">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<form class="top-formsearch">
		<ul>
			<li><label>Date From</label><input type="text" id="datefrom" name="date_from"></li>
			<li><label>Date TO</label><input type="text" id="dateto" name="date_to"></li>
			<li><input type="submit" value="submit"></li>
		</ul>		
		
	</form>
<br>

    <?php 
		
		if(count($reports)){ ?>
			<div class="table-responsive">	
				<table class="table">
					<thead>
						<tr>
							<th>Date From</th>
							<th>Date To</th>
							<th>Chats</th>
							<th>Missed chats</th>
						</tr>
					</thead>
					<tbody>
						<?php
						//echo '<pre>';print_r($chats->chats[0]);echo '</pre>';
						foreach($reports as $report){ ?>
							<tr>
								<td><?= $report->begin; ?></td>
								<td><?= $report->end; ?></td>
								<td><?= $report->chats; ?></td>
								<td><?= $report->missed_chats; ?></td>
							</tr>
							<?php 
						}
						}	
						?>
					</tbody>
				</table>
			</div>
			<?php
		
	?>

</div>
<?php 

$this->registerJs(
    '$("document").ready(function(){ jQuery("#dateto,#datefrom").datepicker( {dateFormat: "yy-mm-dd",changeMonth:true,changeYear:true} ); });'
, \yii\web\View::POS_END);