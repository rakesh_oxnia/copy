<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\BusinessCategory;
/**
 * @var yii\web\View $this
 * @var frontend\models\PayasyougoSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="payasyougo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

   <?= $form->field($model, 'business_category_id')
        ->dropDownList(
            ArrayHelper::map(BusinessCategory::find()->all(), 'id', 'name'),      
            ['prompt'=>'Select Business Category']    
        );?>
    

    <?= $form->field($model, 'charges_per_lead') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
