<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use ruskid\stripe\StripeCheckout;
?>	
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl;?>/kosmoassets/styles/pricing/plans.min.css">
   <div class="ks-page-header">
            <section class="ks-title">
                <h3>Pay as you go</h3>

                <div class="ks-controls">
                    <nav class="breadcrumb ks-default">
                        <a class="breadcrumb-item ks-breadcrumb-icon" href="#">
                            <span class="la la-home ks-icon"></span>
                        </a>
                        <a class="breadcrumb-item" href="#">Pricing</a>
                        <span class="breadcrumb-item active" href="#">Coupons</span>
                    </nav>
                </div>
            </section>
        </div>
        <div class="ks-page-content">
            <div class="ks-page-content-body ks-full-height">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="ks-pricing-plans-page">
							<div class="ks-header">
                                <h3 class="ks-name">Coupon</h3>                                
                            </div>
							<?php if( $error ){?>
								<div class="alert alert-danger">
									<?php echo $error;?>
								</div>
							<?php }?>
							<?php if( $success ){?>
								<div class="alert alert-success">
									<?php echo $success;?>
								</div>	
							<?php }?>
						
						<?php if(!isset(Yii::$app->session['discount'])){?>
							<?php $form = ActiveForm::begin(); ?>
								
										
											<div class="form-group">						
												<input placeholder="Apply Coupon" type="text" name="coupon" value="" class="form-control">
											</div>
										
								
											<div class="form-group">	
											   <input type="submit" name="apply" value="Apply" class="btn btn-info btn-block">
											</div>
											
								 <?php ActiveForm::end(); ?>
							<?php }else{?>
									<div class="alert alert-warning">
										Coupon Applied. Discount : <?php echo Yii::$app->session['discount'] . "%";?>
									</div>
							<?php }?>	
                            <div class="ks-header">
                                <h3 class="ks-name">Pricing</h3>
                                <div class="ks-description">Choose your Category</div>
                            </div>
                            <div class="ks-plans">
                             <?php if( !empty( $packages ) ) : ?>
								<?php foreach($packages as $package) : ?>
									<?php
											$amount = $package['charges_per_lead'];
											if(Yii::$app->session['discount']){
												$discount = ($amount * Yii::$app->session['discount'])/100;
												$amount  = $amount - $discount;
											}
									?>										
										<div class="ks-plan ks-info">
											<div class="ks-header">
												<h5 class="ks-name"><?php echo $categories[$package['business_category_id']];?></h5>
											</div>
											<div class="ks-body">
												<ul class="ks-list ks-success">
													<li>Chargers Per Lead <?php echo $package['charges_per_lead'];?></li>
													<?php if(Yii::$app->session['discount']){?>
															<li>Charges Per Lead After Discount: <?php echo $amount;?></li>
													<?php }?>
													 
												</ul>

												<a href="<?php echo Url::to(['/package/paynow', 'id' => $package['id']]);?>" class="btn btn-info btn-block">Pay with Paypal</a>
												<a href="#" class="btn btn-info btn-block">
												<?php
									
									
													echo StripeCheckout::widget([
														'action' => '/',
														'name' => $categories[$package['business_category_id']],
														'description' => "Charges Per Lead : " . $package['charges_per_lead'],
														'amount' => $amount * 100,
														'image' => Yii::$app->request->baseUrl . '/cube/img/logo.png',
														
													]);
													?>
												</a>
											</div>
										</div>
								<?php endforeach;?>
							<?php endif;?>
								
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>