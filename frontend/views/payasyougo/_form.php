<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\BusinessCategory;
/**
 * @var yii\web\View $this
 * @var app\models\Payasyougo $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="payasyougo-form">

    <?php $form = ActiveForm::begin(); ?>
	<?= $form->field($model, 'business_category_id')
        ->dropDownList(
            ArrayHelper::map(BusinessCategory::find()->all(), 'id', 'name'),      
            ['prompt'=>'Select Business Category']    
        );?>
    

    <?= $form->field($model, 'charges_per_lead')->textInput(['maxlength' => 10]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
