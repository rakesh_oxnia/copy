<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var frontend\models\PayasyougoSearch $searchModel
 */

$this->title = 'Payasyougos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payasyougo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payasyougo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
			 'attribute' => 'businesscategory',
			 'value' => 'businesscategory.name'
			 ],
            'charges_per_lead',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
