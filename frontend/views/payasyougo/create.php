<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Payasyougo $model
 */

$this->title = 'Create Payasyougo';
$this->params['breadcrumbs'][] = ['label' => 'Payasyougos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payasyougo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
