<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Payasyougo $model
 */

$this->title = 'Update Payasyougo: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payasyougos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payasyougo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
