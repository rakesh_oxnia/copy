<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\EmailLog;
use yii\widgets\Pjax;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var frontend\models\EmailLogSearch $searchModel
 */

$this->title = 'Email Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ks-page-header">
    <section class="ks-title">
        <h3><?= Html::encode($this->title) ?></h3>
    </section>
</div>
<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">

                              <div class="row">
           											<div class="col-md-4">
           												<input type="text" placeholder="Filter Date" class="form-control" id="datefrom" name="date_from">
           											</div>
                                <div class="col-md-2">
           												<?=Html::a('Go', ['index'], ['class' => 'btn btn-primary search-btn'])?>
           											</div>
                              </div>

                              <br>

                              <?php Pjax::begin(['id' => 'email_log_pjax']); ?>

                              <?= GridView::widget([
                                  'dataProvider' => $dataProvider,
                                  'filterModel' => $searchModel,
                                  'columns' => [
                                      ['class' => 'yii\grid\SerialColumn'],

                                      //'id',
                                      //'leads_id',
                                      'chat_id',
                                      //'sent_to:ntext',
                                      'web_url',
                                      [
                                          'attribute' => 'sent_via',
                                          'value' => function($model){
                                              return $model->sent_via == NULL ? '' : EmailLog::$via[$model->sent_via];
                                          },
                                          //'filter' => ForwardingEmailAddress::$teams,
                                          'filter' => Html::activeDropDownList($searchModel, 'sent_via', EmailLog::$via,['class'=>'form-control','prompt' => 'Select Sent Via']),
                                      ],
                                      'created_at:datetime',
                                      // 'updated_at',

                                      //['class' => 'yii\grid\ActionColumn'],
                                      [
                                      'class' => 'yii\grid\ActionColumn',
                                              'header'=>'Action',
                                              'template'=>'{view}{delete}',
                                              'buttons' => [
                                                  'delete' => function ($url, $model) {
                                                        return Html::a('<span class="ks-icon la la-trash"></span>',
                                                            $url, [
                                                              'data-confirm' => 'Are you sure you want to delete this Email Address ?',
                                                              'data-method' => 'post'
                                                        ]);

                                                  },
                                                  'view' => function ($url, $model) {
                                                      return Html::a('<span class="ks-icon la la-eye"></span>',
                                                          $url);
                                                  },

                                              ],
                                      ],
                                  ],
                              ]); ?>
                              <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
  $this->registerCss("

    .table td{
      padding: 10px !important;
      font-size: 12px !important;
    }

    .ks-icon{
      font-size: 20px !important;
      padding-left: 7px;
    }

    .action-column{
      width: 140px;
    }

    .ks-icon:hover{
      color: #f00;
    }

  ");

    $this->registerJs("
        $('select').select2();

        $.pjax.reload({container:'#email_log_pjax'});

        jQuery('#dateto,#datefrom').datepicker( {dateFormat: 'yy-mm-dd',changeMonth:true,changeYear:true} );

        $('.search-btn').off('click').on('click', function(e){
          e.preventDefault();
          date_from = $('#datefrom').val();
          // if(date_from == ''){
          //   swal('Date cannot be left blank', '', 'error');
          //   return false;
          // }

          chat_id = $(\"input[name='EmailLogSearch[chat_id]']\").val();
          web_url = $(\"input[name='EmailLogSearch[web_url]']\").val();
          sent_via = $(\"input[name='EmailLogSearch[sent_via]']\").val();
          created_at = date_from;
          search_url = '&EmailLogSearch[chat_id]=' + chat_id + '&EmailLogSearch[web_url]=' + web_url + '&EmailLogSearch[sent_via]' + sent_via + '=&EmailLogSearch[created_at]=' + created_at +'&_pjax=%23email_log_pjax';
          url = '".Url::to(['email-log/index'])."' + search_url;
          location = url;
        });
    ");

?>
