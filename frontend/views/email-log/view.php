<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\EmailLog;

/**
 * @var yii\web\View $this
 * @var common\models\EmailLog $model
 */

$this->title = 'Email Log for ' . $model->chat_id;
$this->params['breadcrumbs'][] = ['label' => 'Email Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ks-page-header">
    <section class="ks-title">
        <h3><?= Html::encode($this->title) ?></h3>
    </section>
</div>
<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">
                              <p>
                                  <?= Html::a('Back', ['index'], ['class' => 'btn btn-primary']) ?>

                              </p>

                              <?= DetailView::widget([
                                  'model' => $model,
                                  'attributes' => [
                                      //'id',
                                      //'leads_id',
                                      'chat_id',
                                      'sent_to:ntext',
                                      [
                                        'attribute' => 'sent_via',
                                        'value' => $model->sent_via != NULL ? EmailLog::$via[$model->sent_via] : NULL,
                                      ],
                                      'created_at:datetime',
                                      //'updated_at',
                                  ],
                              ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
