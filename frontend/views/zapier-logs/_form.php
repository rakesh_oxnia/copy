<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var common\models\EmailLog $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="email-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'leads_id')->textInput() ?>

    <?= $form->field($model, 'sent_via')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'sent_to')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'chat_id')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
