<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\ZapierLogs;
use yii\widgets\Pjax;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var frontend\models\EmailLogSearch $searchModel
 */

$this->title = 'Zapier Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ks-page-header">
    <section class="ks-title">
        <h3><?= Html::encode($this->title) ?></h3>
    </section>
</div>
<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">

                              <div class="row">
           											<div class="col-md-4">
           												<input type="text" placeholder="Filter Date" class="form-control" id="datefrom" name="date_from">
           											</div>
                                <div class="col-md-2">
           												<?=Html::a('Go', ['index'], ['class' => 'btn btn-primary search-btn'])?>
           											</div>
                              </div>

                              <br>

                              <?php Pjax::begin(['id' => 'zapier_log_pjax']); ?>

                              <?= GridView::widget([
                                  'dataProvider' => $dataProvider,
                                  'filterModel' => $searchModel,
                                  'columns' => [
                                      ['class' => 'yii\grid\SerialColumn'],
                                      //'chat_id',
                                      'email',
                                      [
                                          'attribute' => 'Chat ID',
                                          'format' => 'raw',
                                          'value' => function($model){
                                            if($model->type == 'lead')
                                            {
                                              $url = Yii::$app->params['baseUrl'].'frontend/web/index.php?r=leads%2Fview&id='.$model->chat_id;
                                            }else {
                                              $url = Yii::$app->params['baseUrl'].'frontend/web/index.php?r=leads%2Fview-chat-service&id='.$model->chat_id;
                                            }
                                            return "<a href='$url'>$model->chat_id</a>";
                                          },
                                          'filter' => Html::textInput('ZapierLogSearch[chat_id]', '', ['class' => 'form-control'])
                                      ],
                                      'lead_id',
                                      [
                                				'attribute' => 'type',
                                				'value' => function ($model) {
                                                    if($model->type == 'lead'){
                                                        return 'Lead';
                                                    }else{
                                                        return 'Service Chat';
                                                    }
                                                },
                                				'filter' => Html::activeDropDownList($searchModel,'type',['lead' => 'Lead','service_chat' => 'Service Chat'],[
                                					'class' => 'form-control',
                                					'prompt' => 'All'
                                				]),
                                			],
                                      'created_at:datetime'
                                  ],
                              ]); ?>
                              <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
  $this->registerCss("

    .table td{
      padding: 10px !important;
      font-size: 12px !important;
    }

    .ks-icon{
      font-size: 20px !important;
      padding-left: 7px;
    }

    .action-column{
      width: 140px;
    }

    .ks-icon:hover{
      color: #f00;
    }

  ");

    $this->registerJs("
        $('select').select2();

        $.pjax.reload({container:'#zapier_log_pjax'});

        jQuery('#dateto,#datefrom').datepicker( {dateFormat: 'yy-mm-dd',changeMonth:true,changeYear:true} );

        $('.search-btn').off('click').on('click', function(e){
          e.preventDefault();
          date_from = $('#datefrom').val();
          // if(date_from == ''){
          //   swal('Date cannot be left blank', '', 'error');
          //   return false;
          // }

          chat_id = $(\"input[name='ZapierLogSearch[chat_id]']\").val();
          lead_id = $(\"input[name='ZapierLogSearch[lead_id]']\").val();
          //web_url = $(\"input[name='EmailLogSearch[web_url]']\").val();
          //sent_via = $(\"input[name='EmailLogSearch[sent_via]']\").val();
          created_at = date_from;
          search_url = '&ZapierLogSearch[chat_id]=' + chat_id + '&ZapierLogSearch[lead_id]=' + lead_id + '&ZapierLogSearch[created_at]=' + created_at +'&_pjax=%23zapier_log_pjax';
          url = '".Url::to(['zapier-logs/index'])."' + search_url;
          location = url;
        });
    ");

?>
