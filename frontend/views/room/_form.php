<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Room */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="room-form">
<?php //echo $model->property_id;die;?>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

	<?php if($model->isNewRecord){ ?>
    <?= $form->field($model, 'room_name')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'property_id')->textInput(['type'=>'hidden','value'=>$id])->label(false) ?>
	<?php }

	else{ ?>
	<?= $form->field($model, 'property_id')->textInput(['type'=>'hidden','value'=>$model->property_id])->label(false) ?>
	<?php }	?>

    <?= $form->field($model, 'image[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?> 
	
    <?= $form->field($model, 'image2[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'formsubmit']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>