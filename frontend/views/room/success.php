<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Property */

$this->title = 'Success Page';
$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['finish']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="layout-header"></div>
<div class="property-finish layout-content">
	<div class="col-md-4 layout-left" style="min-height:243px;">
		<h1>Congratulations</h1>
		<p>You Have Successfilly Uploaded the Images</p>
	</div>
	<div class="col-md-8 layout-right" style="">
		<br>
		<p>Choose Your Option</p>
		<br>
		<a href="<?php echo Url::toRoute(['room/another','id'=>$id]);?>"><button class="btn btn-success">Add Another Room</button></a>
		<a href="<?php echo Url::toRoute(['property/finish','id'=>$id]);?>"><button class="btn btn-success">Finish Adding Rooms</button></a>
	</div>
</div>
