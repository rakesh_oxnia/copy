<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Room */

$this->title = 'Add Room Images';
$this->params['breadcrumbs'][] = ['label' => 'Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="layout-header"></div>
<div class="room-create layout-content">
<div class="col-md-4 layout-left" style="<?php if(Yii::$app->controller->action->id=='another'){ ?>min-height:386px;<?php }else{?>min-height:328px;<?php }?>">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<div class="col-md-8 layout-right">
    <?= $this->render('_form', [
        'model' => $model,
        'model2' => $model2,
        'id' => $id,
        //'propertydata' => $propertydata,
    ]) ?>
</div>
</div>
