<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\ForwardingEmailAddress;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var frontend\models\ForwardingEmailAddress $searchModel
 */

$this->title = 'Forwarding Email Addresses';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ks-page-header">
    <section class="ks-title">
        <h3><?= Html::encode($this->title) ?></h3>
    </section>
</div>
<div class="ks-page-content">
    <div class="ks-page-content-body">
        <div class="ks-nav-body-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ks-panels-column-section">
                        <div class="card">
                            <div class="card-block">
                                 <h5>You can add 5 forwarding email address.</h5>
                                <p>

                                    <?php
                                        if(count($forwardingEmails) <= 4){
                                            echo Html::a('Add new', ['create'], ['class' => 'btn btn-success']);
                                        }
                                        echo '&nbsp; &nbsp;';
                                        echo Html::a('View teams', Url::to(['forwarding-email-team/index']), ['class' => 'btn btn-warning']);
                                    ?>
                                </p>

                                <?= GridView::widget([
                                    'dataProvider' => $dataProvider,
                                    'filterModel' => $searchModel,
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        //'id',
                                        //'user_id',
                                        'name',
                                        'email:email',
                                        //'team',
                                        [
                                            'attribute' => 'team',
                                            'value' => function($model){
                                                return $model->forwardingTeam == NULL ? '' : $model->forwardingTeam->name;
                                            },
                                            //'filter' => ForwardingEmailAddress::$teams,
                                            'filter' => Html::activeDropDownList($searchModel, 'team', ArrayHelper::map($forwardingTeams, 'id', 'name'),['class'=>'form-control','prompt' => 'Select Team']),
                                        ],
                                        // 'created_at',
                                        // 'updated_at',

                                        //['class' => 'yii\grid\ActionColumn'],
                                        [
                                        'class' => 'yii\grid\ActionColumn',
                                                'header'=>'Action',
                                                'template'=>'{view}{update}{delete}',
                                                'buttons' => [
                                                    'delete' => function ($url, $model) {
                                                          return Html::a('<span class="ks-icon la la-trash"></span>',
                                                              $url, [
                                                                'data-confirm' => 'Are you sure you want to delete this Email Address ?',
                                                                'data-method' => 'post'
                                                          ]);

                                                    },
                                                    'view' => function ($url, $model) {
                                                        return Html::a('<span class="ks-icon la la-eye"></span>',
                                                            $url);
                                                    },
                                                    'update' => function ($url, $model) {
                                                        return Html::a('<span class="ks-icon la la-pencil"></span>',
                                                            $url);
                                                    },
                                                ],
                                        ],
                                    ],
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
  $this->registerCss("

    .table td{
      padding: 10px !important;
      font-size: 12px !important;
    }

    .ks-icon{
      font-size: 20px !important;
      padding-left: 7px;
    }

    .action-column{
      width: 140px;
    }

    .ks-icon:hover{
      color: #f00;
    }

  ");

    $this->registerJs("
        $('select').select2();
    ");
