<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var common\models\ForwardingEmailAddress $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="forwarding-email-address-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
      <div class="col-md-6">
        <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <?= $form->field($model, 'team')->dropdownList(ArrayHelper::map($forwardingTeams, 'id', 'name'), ['class' => 'select form-control']) ?>
      </div>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'user_id')->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false); ?>

        <?= Html::submitButton($model->isNewRecord ? 'Add' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php

    $this->registerCss("
        .help-block{
            color: #f00;
        }
    ");

    $this->registerJS("
        $(function(){
            $('.select').select2();
        });
    ");
?>
