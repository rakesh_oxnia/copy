<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\User;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Faqs;
use yii\data\ActiveDataProvider;


/**
 * Site controller
 */
class FaqController extends Controller
{

	public function behaviors()
  {
      return [
          // 'access' => [
          //     'class' => AccessControl::className(),
          //     'rules' => [
          //         [
          //             'actions' => ['login', 'error'],
          //             'allow' => true,
          //         ],
          //     ],
          // ],
      ];
  }

	public function beforeAction($action)
	{
			if(Yii::$app->user->isGuest){
				return $this->redirect(['site/login']);
			}

			return parent::beforeAction($action);
	}

	public function actionIndex(){
		$this->layout = 'kosmoouter';
		$faqs	= Faqs::find()->orderBy(['id' => SORT_DESC])->all();


		return $this->render('index', ['faqs' => $faqs]);
	}

	public function actionAdd(){
		$this->layout = 'kosmoouter';
		$success			= "";
		$model 				= new Faqs;
		$model->user_id		= Yii::$app->user->id;
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$success = "Faq has been added successfully.";
				$model	   = new Faqs;
		}
		return $this->render('add', ['model' => $model, 'success' => $success]);
	}

}
