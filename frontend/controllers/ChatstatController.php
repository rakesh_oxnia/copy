<?php

namespace frontend\controllers;

use Yii;
use common\models\User;
use common\models\LiveChat_API;
use frontend\models\Chatstat;
use frontend\models\ChatstatSearch;
use yii\filters\AccessControl;

class ChatstatController extends \yii\web\Controller
{
	public function behaviors()
	{
	    return [
	        'access' => [
	            'class' => AccessControl::className(),
	            'except' => ['get-report', 'archive-cron'],
	            'rules' => [
	                [
	                    'allow' => true,
	                    'actions' => ['index', 'export'],
	                    'roles' => [User::ADMIN_ROLE, User::SUBADMIN_ROLE],
	                    'matchCallback' => function ($rule, $action) {
							return Yii::$app->user->can('viewReport');                     
                        }	                    
	                ]
	            ],
	        ],
	    ];
	}

    public function actionIndex()
    {
    	$this->layout = 'kosmoouter';
        $searchModel = new ChatstatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionExport() {
	    $dataprovider =  Yii::$app->request->get('dataprovider');
	    $someArray = json_decode($dataprovider, true);

		$pagination = false;

	    $searchModel = new ChatstatSearch;
	    $dataProvider = $searchModel->search($someArray,$pagination);	    
	    $model =  $dataProvider->getModels();

	    if($dataProvider->getTotalCount() > 0) {
	    	$rnd = rand();
		    $filename = "chatstatreport$rnd.csv";	   
		    $file = fopen($filename,"a");

			$columns = [			  
			  'Business Name',
			  'Group',
			  'Greetings', 
			  'Total Chats',
			  'Total Leads',
			  'Chat To Lead %',
			  'Conversion Rate',
			  'Total Greeting Chats',
			  'Total Visitor Chats'   
			];
		    fputcsv($file, $columns);    

		    foreach ($model as $row) {     
		      fputcsv($file, $row);  
		    }

		    header("Cache-Control: public");
		    header("Content-Description: File Transfer");
		    header("Content-Disposition: attachment; filename=$filename");
		    header("Content-Type: application/csv");
		    header("Content-Transfer-Encoding: binary");		   
		    readfile($filename); 
	    } else {
	        Yii::$app->session->setFlash('error', 'You have not bought any lead');
	        return $this->redirect(Yii::$app->request->referrer);
	    }
    }    

    public function actionGetReport() { 
    	$date = isset($_GET['date']) ? $_GET['date'] : date('Y-m-d', strtotime(' -1 day'));
	   	
    	$API = new LiveChat_API(); 	

		$params = array();
    	$params['date_from'] = $date;
    	$params['date_to'] = $date;
    	$params['timezone'] = date_default_timezone_get();
    	$params['group_by'] = 'day';  

    	$groups = $API->groups->testGroup();
    	unset($groups[0]);
    	
    	foreach ($groups as $group) {
	   		$chatstat = new Chatstat();  	
	    	$chatstat->business_name = $group->name;
	    	$chatstat->group = $group->id;
	    	$chatstat->record_date = $date;	    	

  			$params['group'] = $group->id;

	    	$response = $API->reports->get("chats/greetings",$params);
	    	$data = get_object_vars($response);
	    	$chatstat->invites = $data[$date]->displayed;

	    	$response = $API->reports->get("chats/total_chats",$params);
	    	$data = get_object_vars($response);
	    	$chatstat->chats = $data[$date]->chats;

	    	$params['tag[]'] = 'lead'; 
	    	$response = $API->reports->get("chats/total_chats",$params);
	    	$data = get_object_vars($response);
	    	$chatstat->leads = $data[$date]->chats;
	    	unset($params['tag[]']);

	    	$response = $API->reports->get("chats/engagement",$params);
	    	$data = get_object_vars($response);
	    	$chatstat->greeting_chats = $data[$date]->chats_from_auto_invite;
	    	$chatstat->visitor_chats = $data[$date]->chats_from_immediate_start;

	    	$chatstat->save();			
		}
    }

    public function actionArchiveCron() {
    	if (!isset($_GET['date'])) {
    		exit;
    	}

    	$model = new Chatstat();
    	$record_date = '2019-03-10';

    	$query_date = strtotime($record_date .' +1 day');
    	$end_date = strtotime($_GET['date']);    	

    	if ($query_date > $end_date) {
    		exit;
    	}

    	return $this->redirect(['get-report', 'date' => date('Y-m-d', $query_date)]);
    }

}
