<?php
namespace frontend\controllers;

use Yii;
use app\models\User2;
use common\models\User;
use common\models\UserAssociate;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\PasswordResetRequestForm;
use common\models\AgencyUserPasswordForm;
use yii\filters\AccessControl;
use common\models\LiveChat_API;
use frontend\models\ServiceAccount;
use common\models\Packages;
use app\models\CustomLeadFields;
use common\models\LoginForm;
use app\models\Subscription;
use frontend\models\Invoice;
use app\models\Notification;

/**
 * UserController implements the CRUD actions for User2 model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['changepassword','switch-user'],
                        'roles' => ['@'],
                    ],                  
                    [
                        'allow' => true,
                        'actions' => ['indexagency', 'viewagency',  'updateagency', 'createagency', 'deleteagency', 'cancel-plan'],
                        'roles' => [User::ADMIN_ROLE, User::SUBADMIN_ROLE]                    
                    ],                   
                    [
                        'allow' => true,
                        'actions' => ['index', 'view','create', 'delete', 'client-franchise-index', 'website-index'],
                        'roles' => [User::ADMIN_ROLE, User::SUBADMIN_ROLE],
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('manageClients');                     
                        }                     
                    ],  
                    [
                        'allow' => true,
                        'actions' => ['franchise-index', 'franchise-view','franchise-create'],
                        'roles' => [User::ADMIN_ROLE, User::SUBADMIN_ROLE],
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('manageFranchises');                     
                        }                     
                    ],    
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'roles' => [User::ADMIN_ROLE, User::SUBADMIN_ROLE, User::FRANCHISE_ROLE, User::CLIENT_ROLE, User::ASSIST_ROLE],
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('manageClients') || 
                          Yii::$app->user->can('manageFranchises') || 
                          Yii::$app->user->can('manageOutlets');                     
                        }                     
                    ],                    
                    [
                        'allow' => true,
                        'actions' => ['addwebsite', 'deletelogo', 'admin-change-password', 'allocate-plan-view', 'view-custom-fields', 'update-lead-sender', 'edit-lead-sender', 'get-single-plan'],
                        'roles' => [User::ADMIN_ROLE, User::SUBADMIN_ROLE],
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('manageClients') || 
                          Yii::$app->user->can('manageFranchises');                     
                        }                     
                    ], 
                    [
                        'allow' => true,
                        'actions' => ['outlet-index', 'outlet-view', 'outlet-create'],
                        'roles' => [User::ADMIN_ROLE, User::SUBADMIN_ROLE, User::FRANCHISE_ROLE, User::CLIENT_ROLE, User::ASSIST_ROLE],                        
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('manageOutlets');                     
                        }                     
                    ], 
                    [
                        'allow' => true,
                        'actions' => ['service-account', 'create-service-account'],
                        'roles' => [User::ADMIN_ROLE, User::SUBADMIN_ROLE],
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('manageServiceAccounts');                     
                        }                     
                    ],                                                                         
                ],
            ],           

        ];
    }

   public function beforeAction($action)
   {
       if(!in_array($action->id, ['outlet-index', 'outlet-view', 'outlet-create']))
       {
          return parent::beforeAction($action);
       }

        if(!parent::beforeAction($action)) {
            return false;
        }
        
        LoginForm::userAssitLogin();

        return true;
    }

    public function render($view, $params)
    { 

      $params = LoginForm::userAssitLogout($params);

        // before render
        return parent::render($view, $params);
    }    


   /* public function beforeAction ($action)
    {
        $host = explode('.', $_SERVER['HTTP_HOST']);
        $username = $host[0];

        if($username != '' && $username != 'fubaco')
        {
            if(Yii::$app->user->identity->username != '')
            {
                if($username  != Yii::$app->user->identity->username)
                {
                    //echo Yii::$app->homeUrl; exit;
                     //return $this->redirect('http://chat-application.com/site/login');
                }
            }
        }
        return 1;

    }*/

/* 
New user access control replaced 
    public function beforeAction ($action)
    {
        if(Yii::$app->user->isGuest) {
          return $this->redirect(['site/login']);
        }

        $host = explode('.', $_SERVER['HTTP_HOST']);
        $username = $host[0];
        $flag = 0;
        if($role = Yii::$app->user->identity->role == '')
        {
            $flag = 1;
        }

        if($username != '')
        {
            $role = Yii::$app->user->identity->role;
            if(User::ROLE_AGENCY == $role)
            {
                if($username == 'chat-application')
                {
                    $flag = 1;

                }
                else if ($username != Yii::$app->user->identity->username)
                {
                    $flag = 1;
                }
            }
            else if(User::ROLE_USER == $role )
            {
                $parent_id = Yii::$app->user->identity->parent_id;
                if($parent_id != '')
                {
                    $row = (new \yii\db\Query())
                    ->from('user')
                    ->where(['id' => $parent_id])
                    ->one();
                   if($username == 'chat-application')
                    {
                        $flag = 1;

                    }
                    else if ($username != $row['username'])
                    {
                        $flag = 1;
                    }
                }
            }
            if($flag == 1)
            {
                //print_r('site logout'); exit;
                Yii::$app->user->logout();
                //echo Yii::$app->homeUrl; exit;
                return $this->redirect('http://chat-application.com/site/login');
            }
        }
        return 1;

    }
*/
	 public $layout = "dashboard";

    /**
     * Lists all User2 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = 'kosmoouter';
        $id = yii::$app->user->identity->id;
        $userrole = yii::$app->user->identity->role;
        if($userrole == User::ROLE_AGENCY)
        {
            $searchModel = new UserSearch(['role'=>'10', 'parent_id' => $id]);
        }
        else
        {
            $searchModel = new UserSearch(['role'=>'10']);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionClientFranchiseIndex()
    {
        $this->layout = 'kosmoouter';
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('client-franchise-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User2 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		    $this->layout = 'kosmoouter';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

	/**
     * Displays a single User2 model.
     * @param integer $id
     * @return mixed
     */
    public function actionAddwebsite($id)
    {
  		$this->layout = 'kosmoouter';
  		$success = false;
  		$error = false;
  		$model = new User();
  		$userData = $this->findModel($id);
  		if ( isset( $_POST['add'] ) ) {
  			if( !empty( $_POST['User']['user_group'] ) && !empty( $_POST['User']['website_url'] ) ){
  				$user = new User();
          $user->scenario = 'add_website';

  				$user->username = $userData->username;
  				$user->user_group = $_POST['User']['user_group'];
  				$user->email = $userData->email;
  				$user->website_url = $_POST['User']['website_url'];
  				$user->send_transcript = $userData->send_transcript; // Added By Manish Bhuva
          $user->free_access = $userData->free_access; // Added By Ravindra Chavan
          $user->show_past_lead = $userData->show_past_lead;
          $user->show_lead_from = $userData->show_lead_from;
          $user->show_lead_to = $userData->show_lead_to;

          $user->per_lead_cost = $userData->per_lead_cost;
          $user->per_lead_cost_currency = $userData->per_lead_cost_currency;
          $user->average_lead_cost = $userData->average_lead_cost;
          $user->average_lead_value = $userData->average_lead_value;

          $user->disable_payment_status = $userData->disable_payment_status;

  				//$id = yii::$app->user->identity->id;
  				$userrole = yii::$app->user->identity->role;
          $user->setDefaultValues();
  				// if($userrole == User::ROLE_AGENCY)
  				// {
  				// 	$user->parent_id = $id;
  				// }else {
          //   $user->parent_id = $id;
          // }
          $user->parent_id = $id;
  				$user->role = 10;
  				$user->setPassword(time().rand());
  				$user->generateAuthKey();
  				if ($user->save()) {

            $parent_subscribtion = Subscription::findOne(['user_id'=>$id,'is_cancelled'=>0]);
            if($parent_subscribtion) {
              $subscribtion = new Subscription();                 
              $attributes = $parent_subscribtion->attributes;              
              unset($attributes['id']);
              unset($attributes['user_id']);     
              foreach($attributes as  $attribute => $val) {
                  $subscribtion->{$attribute} = $val;
              } 
              $subscribtion->user_id = $user->id;
              $subscribtion->save();
            }

            $auth = Yii::$app->authManager;
            if ($userData->role == User::ROLE_USER) {              
              $user_role = $auth->getRole('client');              
            } else {
              $user_role = $auth->getRole('franchise');   
            }
            $auth->assign($user_role, $user->getId());  


  					$success = "Website has been added successfully.";
  				}else{
            echo '<pre>'; print_r($user->getErrors()); exit;
          }
  			}else{
  				$error = "Please Provide all details";
  			}

  		}

      return $this->render('addwebsite', [
        'userData'  => $userData,
  			'model' 	=> $model,
  			'success'	=> $success,
  			'error' 	=> $error
      ]);
    }

    /**
     * Creates a new User2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		    $this->layout = 'kosmoouter';
        $model = new User2();


        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            //echo '<pre>';print_r(Yii::$app->request->post());die;
            $postUser = Yii::$app->request->post('User2');
            $API = new LiveChat_API();
            $response = $API->groups->addGroup($postUser['business_name'], $postUser['agents']);
            //$response = '{"id":8,"name":"test business name group","language":"en","agents":["cyr.freaxxx@gmail.com","ravindra@webcubictechnologies.com"],"status":"offline"}';
            $response = json_decode($response);
            //print_r($response); exit;

			      $user = new User();
            $user->business_name = $model->business_name;
            $user->username = $model->username;
            $user->user_group = $response->id;
            $user->email = $model->email;
            $user->website_url = $model->website_url;
            $user->send_transcript = $model->send_transcript; // Added By Manish Bhuva
            $user->free_access = $model->free_access; // Added By Ravindra Chavan
            $user->disable_payment_status = $model->disable_payment_status;
            $user->average_lead_value = $model->average_lead_value;
            $user->average_lead_cost = $model->average_lead_cost;
            $user->per_lead_cost = $model->per_lead_cost;
            $user->per_lead_cost_currency = $_POST['User2']['per_lead_cost_currency'];
            $user->tax = $_POST['User2']['tax'];
            $id = yii::$app->user->identity->id;
            $userrole = yii::$app->user->identity->role;
            if($userrole == User::ROLE_AGENCY)
            {
                $user->parent_id = $id;
            }else{
              $user->parent_id = 0;
              $user->agency_name = '';
              $user->agency_logo = '';
              $user->theme_color = '';
              $user->welcome_message = '';
              $user->your_name = '';
              $user->company_name = '';
              $user->company_url = '';
              $user->telephone = '';
              $user->chat_purpose_sales = 0;
              $user->chat_purpose_support = 0;
              $user->industry = 0;
              $user->number_of_employees = 0;
              $user->country = 0;
              $user->b2b = 0;
              $user->b2c = 0;
              $user->internal_use = 0;
              $user->overview = '';
              $user->timezone = 0;
              $user->website_javascript = '';
              $user->email_for_leads = '';
              $user->email_for_chats = '';
              $user->stripeToken = '';
            }
            $user->role = User::ROLE_USER;
            $password = rand();
            $user->setPassword($password);
            $user->generateAuthKey();
			      if ($user->save()) {
                //return $user;
                $auth = Yii::$app->authManager;
                $client = $auth->getRole('client');
                $auth->assign($client, $user->getId());             

                $i=0;
                foreach ($_POST['User2']['email2'] as $value) {

                    if($value != '')
                    {
                        $model2 = new UserAssociate;
                        //Update the Image
                        $model2->user_id = $user->id;
                        $model2->email = $value;
                        $model2->save();
                    }
                    $i++;
                }

				$model_reset = new PasswordResetRequestForm();
				$model_reset->email = $model->email;
				$model_reset->password = $password;
				if($model_reset->validate()){
					if ($model_reset->sendEmail("test")) {
						Yii::$app->getSession()->setFlash('success', 'User Created Mail Send To User\'s Email.');
						//return $this->goHome();
						return $this->redirect(['index']);
					} else {
						Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to Send Email.');
					}
				}
				return $this->redirect(['index']);
            }
			else{
        //echo '<pre>'; print_r($user->getErrors()); exit;
				return $this->render('create', [
					'model' => $model,
				]);
			}

        } else {
            //echo '<pre>'; print_r($model->getErrors()); exit;
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		    $this->layout = 'kosmoouter';
        $model = $this->findModel($id);
        //echo '<pre>';print_r($model);exit;
        if ($model->load(Yii::$app->request->post())) {
            //echo "<pre>"; print_r(Yii::$app->request->post()); exit;
            $postUser = Yii::$app->request->post('User2');
            if($model->role != 20) {
                $API = new LiveChat_API();
                $API->groups->updateGroup($model->user_group,$postUser['business_name'], $postUser['agents']);
            }
            $userModel = User::findOne($model->id);
            $userModel->scenario = 'update_user';
            $userModel->loadModelData($model);
            $userModel->average_lead_value =$model->average_lead_value;
            $userModel->average_lead_cost =$model->average_lead_cost;
            $userModel->tax = $_POST['User2']['tax'];
            
            if($model->role != 20) {
                $userModel->per_lead_cost = $userModel->per_lead_cost;
                $userModel->per_lead_cost_currency = $_POST['User2']['per_lead_cost_currency'];
            }

            if(isset($_POST['User2']['show_past_lead']) && $_POST['User2']['show_past_lead'] == 'on') {
                $userModel->show_past_lead = 1;
                $userModel->show_lead_from = strtotime($_POST['User2']['show_lead_from']);
                $userModel->show_lead_to = strtotime($_POST['User2']['show_lead_to']);
            }else {
                $userModel->show_past_lead = 0;
            }

            $userModel->save();
            
            //Update notification email if exists for this user
            $notification	= Notification::find()->where([
						'user_id'=>$userModel->id
				])->one();
			if(!empty($notification)){
			    if($notification->notify_email != $userModel->email) {
			        $notification->notify_email = $userModel->email;
			        $notification->save();
			    }
			}
			
            //echo '<pre>';print_r($userModel->getErrors());exit;
            // Starting business liencence information.
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand('delete FROM `user_associate`
            WHERE  `user_id` ='.$_GET['id']);
            $command->execute();

            $i=0;
            foreach ($_POST['User2']['email2'] as $value) {

                if($value != '')
                {
                    $model2 = new UserAssociate;
                    //Update the Image
                    $model2->user_id = $model->id;
                    $model2->email = $value;
                    $model2->save();
                }
                $i++;
            }

            if($model->role == User::ROLE_FRANCHISE){
              return $this->redirect(['franchise-view', 'id' => $model->id]);
            }
            if($model->role == User::ROLE_OUTLET){
              return $this->redirect(['outlet-view', 'id' => $model->id]);
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }



    public function actionIndexagency()
    {
        $searchModel = new UserSearch(['role'=>User::ROLE_AGENCY]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('indexagency', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User2 model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewagency($id)
    {
        return $this->render('viewagency', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateagency()
    {
        $model = new User2();


        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            //echo '<pre>';print_r($model->username);die;

            $user = new User();
            $user->setDefaultValues();
            $user->username = $model->username;
            $user->user_group = $model->user_group;
            $user->email = $model->email;
            $user->website_url = $model->website_url;
            //$user->parent_id = $model->parent_id;
            $user->role =User::ROLE_AGENCY;
            $user->agency_name = $model->agency_name;

            $uploadfile = '';
            if(isset($_FILES['User2']['name']['agency_logo']) && $_FILES['User2']['name']['agency_logo'] != '')
            {
                $uploaddir = 'logo_images/';
                $time = time();
                $ext = pathinfo($_FILES['User2']['name']['agency_logo'], PATHINFO_EXTENSION);
                $uploadfile = 'agency_logo_'.$time .'_'.rand(111,999).'.'. $ext;


                if (move_uploaded_file($_FILES['User2']['tmp_name']['agency_logo'], $uploaddir.$uploadfile)) {

                }
                else
                {
                    $uploadfile = '';
                }
            }
            $user->agency_logo = $uploadfile;
            $user->theme_color = $model->theme_color;
            $user->send_transcript = 0; // hardcoded to zero
            $password = rand();
            $user->setPassword($password);
            $user->generateAuthKey();
            if ($user->save()) {

                $connection = Yii::$app->db;
                $connection->createCommand()->update('user', ['role' => User::ROLE_AGENCY], 'id = '.$user->id)->execute();
                //return $user;
                $model_reset = new PasswordResetRequestForm();
                $model_reset->email = $model->email;
                $model_reset->password = $password;
                if($model_reset->validate()){
                    if ($model_reset->sendEmail("test")) {
                        Yii::$app->getSession()->setFlash('success', 'Agency Created Mail Send To User\'s Email.');
                        //return $this->goHome();
                        return $this->redirect(['indexagency']);
                    } else {
                        Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to Send Email.');
                    }
                }
                return $this->redirect(['indexagency']);
            }
            else{
                return $this->render('createagency', [
                    'model' => $model,
                ]);
            }

        } else {
            return $this->render('createagency', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateagency($id)
    {
        $model = $this->findModel($id);
        $agency_logo = $model->agency_logo;
        if ($model->load(Yii::$app->request->post())) {
            $uploadfile = '';
            if(isset($_FILES['User2']['name']['agency_logo']) && $_FILES['User2']['name']['agency_logo'] != '')
            {

                if($agency_logo){
                    @unlink('logo_images/'.$agency_logo);
                }
                $time = time();
                $uploaddir = 'logo_images/';
                $ext = pathinfo($_FILES['User2']['name']['agency_logo'], PATHINFO_EXTENSION);
                $uploadfile = 'agency_logo_'.$time .'_'.rand(111,999).'.'. $ext;


                if (move_uploaded_file($_FILES['User2']['tmp_name']['agency_logo'], $uploaddir.$uploadfile)) {

                }
                else
                {
                    $uploadfile = '';
                }
            }
            if($uploadfile != '')
            {
                $model->agency_logo = $uploadfile;
            }
            else
            {
                $model->agency_logo = $agency_logo;
            }

            $model->save();
            return $this->redirect(['indexagency']);
        } else {
            return $this->render('updateagency', [
                'model' => $model,
            ]);
        }
    }

    public function actionDeletelogo($id)
    {
        $model = $this->findModel($id);

        if($model->agency_logo){
            @unlink('logo_images/'.$model->agency_logo);
        }
        $connection = Yii::$app->db;
        $connection->createCommand()->update('user', ['agency_logo' => ''], 'id = '.$id)->execute();

        /*  $User=User::findOne($id);
        print_r($User); exit;
        $photo->photo=NULL;
        $photo->update();*/
        return $this->redirect(['updateagency','id'=>$_GET['id']]);
    }

    /**
     * Deletes an existing User2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteagency($id)
    {
        $model = $this->findModel($id);

        if($model->agency_logo){
            @unlink('logo_images/'.$model->agency_logo);
        }

        $this->findModel($id)->delete();
        return $this->redirect(['indexagency']);
    }

	public function actionChangepassword($id = false){
        // $layout_ = $this->actionGetlayout();
        // $this->layout = $layout_;
		$id = yii::$app->user->identity->id;
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post()) && $model->validate()){
			//die("sasa");
            $model->password_hash = Yii::$app->security->generatePasswordHash(Yii::$app->request->post()['new_password']);
            if($model->save()) {
                //return $this->redirect(['view', 'id' => $model->id]);
                return $this->goHome();
            }else{

				echo '<pre>';print_r($model);die;
				 return $this->render('changepassword', [
					'model' => $model,
				]);
			}
        } else {
            return $this->render('changepassword', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Deletes an existing User2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $userModel = $this->findModel($id);
        $role = $userModel->role;
        $parent_id = $userModel->parent_id;
        $userModel->delete();
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('delete FROM `user_associate`
        WHERE  `user_id` ='.$id);
        $command->execute();

        if($role == User::ROLE_FRANCHISE){
          return $this->redirect(['franchise-index']);
        }
        if($role == User::ROLE_OUTLET){
          return $this->redirect(['outlet-index', 'id' => $parent_id]);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the User2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User2::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAdminChangePassword($id)
    {
      $this->layout = 'kosmoouter';
      $model = $this->findModel($id);
      $changeAgencyUserPassModel = new AgencyUserPasswordForm();
      return $this->render('change-password', ['model' => $model, 'changeAgencyUserPassModel' => $changeAgencyUserPassModel]);
    }

    public function actionSwitchUser($id)
    {
      Yii::$app->session->remove('website_filter');
      Yii::$app->session->remove('outlet_filter');
      $session = Yii::$app->session;
      $session['admin_id'] = Yii::$app->user->identity->id;
      $session['admin_email'] = Yii::$app->user->identity->email;
      if(User::LoginAsAgent($id)){
        $user = User::findOne($id);
        if($user->role == USER::ROLE_USER){
          return $this->redirect(['site/index']);
        }
        if($user->role == USER::ROLE_AGENCY){
          return $this->redirect(['chat/index']);
        }
        return $this->redirect(['site/index']);

      }else{
        return $this->goBack();
      }
    }

    // public function actionSwitchToAdmin()
    // {
    //   print_r('switching to admin'); exit;
    //   $session = Yii::$app->session;
    //   $session['admin_id'] = Yii::$app->user->identity->id;
    //   if(User::LoginAsAdmin($id)){
    //     unset($session['admin_id']);
    //     unset($session['admin_email']);
    //     return $this->redirect(['site/index']);
    //   }
    // }

    public function actionFranchiseIndex()
    { 

      $this->layout = 'kosmoouter';

      $searchModel = new UserSearch(['role' => USER::ROLE_FRANCHISE]);

      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      return $this->render('franchise-index', [
         'searchModel' => $searchModel,
         'dataProvider' => $dataProvider,
      ]);
    }

    public function actionFranchiseCreate()
    {
      $model = new User2();
      //$model->scenario = 'franchise';
      $this->layout = 'kosmoouter';
      if($model->load(Yii::$app->request->post()) && $model->validate())
      {
        $postUser = Yii::$app->request->post('User2');
        $API = new LiveChat_API();
        $response = $API->groups->addGroup($postUser['name'], $postUser['agents']);
        $response = json_decode($response);
        $userModel = new User();
        $userModel->role = User::ROLE_FRANCHISE;
        $userModel->user_group = $response->id;
        $userModel->loadModelData($model);
        $userModel->per_lead_cost_currency = $postUser['per_lead_cost_currency'];
        // $userModel->average_lead_value = $model->average_lead_value;
        // $userModel->average_lead_cost = $model->average_lead_cost;
        $userModel->setDefaultValues();
        $password = rand();
        $userModel->setPassword($password);
		    $userModel->generateAuthKey();
        if($userModel->save()) {

          $auth = Yii::$app->authManager;
          $franchise = $auth->getRole('franchise');
          $auth->assign($franchise, $userModel->getId());             

          $i=0;
          foreach ($_POST['User2']['email2'] as $value) {
              if($value != '')
              {
                  $model2 = new UserAssociate;
                  //Update the Image
                  $model2->user_id = $userModel->id;
                  $model2->email = $value;
                  $model2->save();
              }
              $i++;
          }

          $model_reset = new PasswordResetRequestForm();
          $model_reset->email = $model->email;
          $model_reset->password = $password;
          if($model_reset->validate()){
            if ($model_reset->sendEmail("test")) {
              Yii::$app->getSession()->setFlash('success', 'User Created Mail Send To User\'s Email.');
              //return $this->goHome();
              return $this->redirect(['franchise-index']);
            } else {
              Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to Send Email.');
            }
          }
          return $this->redirect(['franchise-index']);
        }
        //echo "<pre>"; print_r($model); exit;

      }else{
        return $this->render('franchise-create', ['model' => $model]);
      }
    }

    public function actionFranchiseView($id)
    {
		    $this->layout = 'kosmoouter';
        return $this->render('franchise-view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionWebsiteIndex($id)
    {
        $this->layout = 'kosmoouter';
        $searchModel = new UserSearch(['role' => USER::ROLE_USER, 'parent_id' => $id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $clientModel = User::findOne($id);

        return $this->render('website-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'id' => $id,
            'clientModel' => $clientModel,
        ]);
    }

    public function actionOutletIndex($id)
    {
      $this->layout = 'kosmoouter';

      $searchModel = new UserSearch(['role' => USER::ROLE_OUTLET, 'parent_id' => $id]);
      $franchiseModel = User::findOne($id);

      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      return $this->render('outlet-index', [
         'searchModel' => $searchModel,
         'dataProvider' => $dataProvider,
         'id' => $id,
         'franchiseModel' => $franchiseModel,
      ]);
    }

    public function actionOutletCreate($id)
    {
      $model = new User2();
      //$model->scenario = 'outlet';
      $this->layout = 'kosmoouter';
      if($model->load(Yii::$app->request->post()) && $model->validate())
      {
        $userModel = new User();
        $userModel->role = User::ROLE_OUTLET;
        $userModel->setDefaultValues();
        $userModel->loadModelData($model);
        $userModel->parent_id = $id;
        $userModel->user_group = $model->user_group;
        $userModel->business_name = $model->business_name;
        $password = rand();
        $userModel->setPassword($password);
				$userModel->generateAuthKey();
        if($userModel->save()){
          $auth = Yii::$app->authManager;
          $outlet = $auth->getRole('outlet');
          $auth->assign($outlet, $userModel->getId());   

          $i=0;
          foreach ($_POST['User2']['email2'] as $value) {
              if($value != '')
              {
                  $model2 = new UserAssociate;
                  //Update the Image
                  $model2->user_id = $userModel->id;
                  $model2->email = $value;
                  $model2->save();
              }
              $i++;
          }

          $model_reset = new PasswordResetRequestForm();
          $model_reset->email = $model->email;
          $model_reset->password = $password;
          if($model_reset->validate()){
            if ($model_reset->sendEmail("test")) {
              Yii::$app->getSession()->setFlash('success', 'User Created Mail Send To User\'s Email.');
              //return $this->goHome();
              return $this->redirect(['outlet-index', 'id' => $id]);
            } else {
              Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to Send Email.');
            }
          }
          return $this->redirect(['outlet-index']);
        }
        //echo "<pre>"; print_r($model); exit;

      }else{
        return $this->render('outlet-create', ['model' => $model]);
      }
    }

    public function actionOutletView($id)
    {
		    $this->layout = 'kosmoouter';
        return $this->render('outlet-view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionLoadJs()
    {
      $tracker_id =  Yii::$app->request->get('tracker_id');
      $userModel = User::findOne(['tracker_id' => $tracker_id]);
      if(!$userModel){
        return "alert('Invalid tracker id. Please contact system administrator');";
      }
      return "
     window.__lc = window.__lc || {};
     window.__lc.license = 6354551;
     window.__lc.ga_version;
     window.__lc.chat_between_groups = false;
     window.__lc.params = [
     { name: 'Business Name', value: '".$userModel->business_name."' },
     { name: 'Group Id', value: '".$userModel->user_group."' },
     { name: 'Client Id', value: '".$userModel->id."' }
     ];
     (function() {
       var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
       lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
     })();
    ";
    }

    public function actionServiceAccount($id)
    {
      if(!Yii::$app->user->identity->role == USER::ROLE_ADMIN){
        return $this->goHome();
      }
      $this->layout = 'kosmoouter';

      $searchModel = new UserSearch(['role' => USER::ROLE_SERVICE_ACCOUNT,'parent_id' => $id]);

      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      return $this->render('service-account-index', [
         'searchModel' => $searchModel,
         'dataProvider' => $dataProvider,
         'parent_id' => $id,
      ]);
    }
    
    public function actionAllocatePlanView($id) {
      if(!Yii::$app->user->identity->role == USER::ROLE_ADMIN){
        return $this->goHome();
      }

      $this->layout = 'kosmoouter';
      $packages = Packages::find()->where(['custom_status'=>'0'])->all();
    //  $packages->scenario = 'packages_name';
    //  $packages->scenario = 'packages_name';

      return $this->render('allocate-plan',[
        'packages' => $packages,
        'user_id' => $id,
      ]);
    }
    
    public function actionViewCustomFields($id){
        $customLeadFields = CustomLeadFields::find()->where(['user_id'=>$id])->all();
        $this->layout = 'kosmleadsandchats';
        return  $this->render('view-custom-fields',['customLeadFields' => $customLeadFields]);
    }

    public function actionUpdateLeadSender($id){
               $this->layout = 'kosmleadsandchats';
               $customLeadFields = CustomLeadFields::findOne($id);
               return $this->render('update-lead-sender',[
                 'customLeadFields' => $customLeadFields
               ]);
    }

    public function actionEditLeadSender($id){
      if(!Yii::$app->user->identity->role == USER::ROLE_ADMIN){
        return $this->goHome();
      }
      $this->layout = 'kosmleadsandchats';
      $customLeadFields = CustomLeadFields::find()->where([
                                                            'user_id'=>$id,
                                                            'soft_delete'=>0
                                                          ])->all();
      return $this->render('edit-lead-sender',[
        'user_id' => $id,
        'customLeadFields' => $customLeadFields
      ]);
    }
    
    public function actionGetSinglePlan(){
    $package_id = Yii::$app->request->post('package_id');
    $package = Packages::findOne($package_id);
    //$packages = new Packages(['scenario' => 'packages_name']);
  //  $model = new User(['scenario' => 'packages_name']);
  //$packages = new Packages(['scenario' => 'packages_name']);

    return json_encode(['success' => true,
    'number_of_leads'=>$package->number_of_leads,
    'amount'=>$package->amount,
    'currency'=>$package->currency,
    'interval'=>$package->interval,
    'custom_status'=>$package->custom_status]);
    }

    public function actionCancelPlan()
    {
        $user_id = Yii::$app->request->post('user_id');
        if (is_numeric($user_id)) {
            $subscription = Subscription::findOne(['user_id' => $user_id, 'is_cancelled' => 0]);
            if($subscription) {
                $subscription->is_cancelled = 1;
                $subscription->cancel_date = time();
                $subscription->cancel_reason = '';
                $subscription->save(false);

                //$invoice = Invoice::find(['user_id' => $user_id, 'subscription_id' => $subscription->id]);
                $connection = Yii::$app->db;
                $connection->createCommand('UPDATE invoice SET is_cancelled = 1 WHERE subscription_id = ' . $subscription->id)->execute();

                $connection = Yii::$app->db;
                $connection->createCommand('UPDATE packages SET is_cancelled = 1 WHERE custom_status = 1 AND user_id = ' . $user_id)->execute();
            }
        }

        return json_encode(['success' => true]);
    }


    public function actionCreateServiceAccount($id)
    {
      $model = new ServiceAccount();
      $this->layout = 'kosmoouter';
      if($model->load(Yii::$app->request->post()) && $model->validate())
      {
        //print_r('model validated'); exit;
        $parentModel = User::findOne($id);
        $userModel = new User();
        $userModel->setDefaultValues();
        $userModel->parent_id = $id;
        $userModel->email = $model->email;
        $userModel->website_url = $parentModel->website_url;
        $userModel->user_group = $parentModel->user_group;
        $userModel->role = User::ROLE_SERVICE_ACCOUNT;
        $password = rand();
        $userModel->setPassword($password);
				$userModel->generateAuthKey();
        if($userModel->save())
        {
          $auth = Yii::$app->authManager;
          $service_account = $auth->getRole('service_account');
          $auth->assign($service_account, $userModel->getId());   

          $model_reset = new PasswordResetRequestForm();
          $model_reset->email = $model->email;
          $model_reset->password = $password;
          if($model_reset->validate()){
            if ($model_reset->sendEmail("test")) {
              Yii::$app->getSession()->setFlash('success', 'Service account created successfully. Check email for further instructions.');
              return $this->redirect(['service-account', 'id' => $id]);
            } else {
              Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to Send Email.');
            }
          }
          return $this->redirect(['service-account', 'id' => $id]);
        }else{
          //echo '<pre>'; print_r($model->getErrors()); exit;

        }
        //echo "<pre>"; print_r($model); exit;

      }else{
        return $this->render('create-service-account', ['model' => $model]);
      }
    }

    
}
