<?php

namespace frontend\controllers;

use Yii;
use app\models\User2;
use common\models\User;
use app\models\Leads;
use app\models\LeadsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\LiveChat_API;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * LeadsController implements the CRUD actions for Leads model.
 */
class LeadsController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
				'class'=> AccessControl::className(),
				'only' => ['index','total','view'],
				'rules'=>[
					[
						'actions' => ['index','total','view'],
						'allow' => true,
						'roles'=>['@'],
					]
				]
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
	
	public $layout = 'dashboard';
	
	/* 
	
	2452-Pal Sidhu(Freelancer) Atul: 63736fcd97a00449d5d8e7a5d6749a01
[6:22:52 AM] 2452-Pal Sidhu(Freelancer) Atul: pal@fuba.com.au

	*/

    /**
     * Lists all Leads models.
     * @return mixed
     */
    public function actionIndex()
    {
		//\Yii::$app->getSession()->setFlash('success', 'Lead Updated');
        try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			if(!isset($_GET["date_from"])){
				$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
				$_GET['date_to'] = date('Y-m-d');
			}
			$_GET['tag%5B%5D'] = 'lead';
			$_GET["group"] = Yii::$app->user->identity->user_group;
			$API = new LiveChat_API();
			$chats = $API->chats->get($page,$_GET);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}
		//echo '<pre>';print_r($transcripts);echo '</pre>';die('ghj');
	
		$transcripts = $chats->chats;
		$total = $chats->total;
		$pages = $chats->pages;
		
		
		return $this->render('index2', [
            'transcripts' => $transcripts,
            'total' => $total,
            'pages' => $pages,
			'userdata'=>Yii::$app->user->identity
        ]);

        /* return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]); */
    }
	
	public function actionTotal()
    {
		$user_group = Yii::$app->user->identity->user_group;
		$user_id = Yii::$app->user->identity->id;
		
		$yesterday = date('Y-m-d');
		$last7 = date('Y-m-d',strtotime("-6 days"));
		
		$group_by = isset($_GET['group_by']) ? $_GET['group_by'] : 'day';
		$date_from = isset($_GET['date_from']) ? $_GET['date_from'] : $last7;
		$date_to = isset($_GET['date_to']) ? $_GET['date_to'] : $yesterday;
		
		$transcripts = Leads::getData($user_group,$user_id,false,$date_from,$date_to,false,$group_by);
		$transcripts2 = Leads::getData($user_group,$user_id,false,$date_from,$date_to,'pending',$group_by);
		$transcripts3 = Leads::getData($user_group,$user_id,false,$date_from,$date_to,'rejected',$group_by);
		$transcripts4 = Leads::getData($user_group,$user_id,false,$date_from,$date_to,'approved',$group_by);
		
		//var_dump( array_search("2016-05-21",$transcripts,false));
		//$leats_total = Leads::convert($transcripts,'7','day',$date_from);

		if($group_by=="day"){
			$data_str = Leads::convert($transcripts,'7','day',$date_from,$date_to);
			$data_str2 = Leads::convert($transcripts2,'7','day',$date_from,$date_to);
			$data_str3 = Leads::convert($transcripts3,'7','day',$date_from,$date_to);
			$data_str4 = Leads::convert($transcripts4,'7','day',$date_from,$date_to);
		}else{
			$data_str = Leads::convertHour($transcripts,$date_from);
			$data_str2 = Leads::convertHour($transcripts2,$date_from);
			$data_str3 = Leads::convertHour($transcripts3,$date_from);
			$data_str4 = Leads::convertHour($transcripts4,$date_from);
		}
		
		$total = Leads::getTotal($transcripts);
		$total2 = Leads::getTotal($transcripts2);
		$total3 = Leads::getTotal($transcripts3);
		$total4 = Leads::getTotal($transcripts4);
		
		/* echo '<pre>';
		print_r($data_str);
		echo '</pre>';die; */
		
        /* try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			$API = new LiveChat_API();
			$_GET['tag%5B%5D'] = 'lead';
			$_GET["group"] = Yii::$app->user->identity->user_group;
			$chats = $API->chats->get($page,$_GET);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		} */
	
		/* $transcripts = $chats->chats; */
		//$total = "";
		$pages = "";
		
		//echo '<pre>';print_r($chats);echo '</pre>';die('ghj');
		
		return $this->render('total', [
            'data_str' => $data_str,
            'data_str2' => $data_str2,
            'data_str3' => $data_str3,
            'data_str4' => $data_str4,
            'total' => $total,
            'total2' => $total2,
            'total3' => $total3,
            'total4' => $total4,
            'pages' => $pages,
			'userdata'=>Yii::$app->user->identity
        ]);
    }

    /**
     * Displays a single Leads model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        try {
			$API = new LiveChat_API();
			$chat = $API->chats->getSingleChat($id);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}
		
        return $this->render('view', [
            'chat' => $chat,
        ]);
    }
	
	
	public function actionSurvey($id)
    {
        try {
			$API = new LiveChat_API();
			$chat = $API->chats->getSingleChat($id);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}
		//echo '<pre>';print_r($chat);echo '</pre>';
        return $this->render('survey', [
            'chat' => $chat,
        ]);
    }

    /**
     * Creates a new Leads model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /* public function actionCreate()
    {
        $model = new Leads();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    } */

    /**
     * Updates an existing Leads model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
	 
    public function actionUpdate($id)
    {
		//\Yii::$app->getSession()->setFlash('success', 'Lead Updated');
        if(!$model = Leads::find()->where(['chat_id'=>$id])->one()){
			$model = new Leads();
			$model->user_id = Yii::$app->user->identity->id;
			$model->chat_id = $id;
		}
		
		$chat = false;
		
		try {
			$API = new LiveChat_API();
			$chat = $API->chats->getSingleChat($id);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}
		

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
			\Yii::$app->getSession()->setFlash('success', 'Lead Updated');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'chat' => $chat,
            ]);
        }
    }
	
	public function actionApproved($id)
    {
		if(!$model = Leads::find()->where(['chat_id'=>$id])->one()){
			$model = new Leads();
			$model->user_id = Yii::$app->user->identity->id;
			$model->chat_id = $id;
		}
		
		$model->status = 'approved';

        $model->save();
		
		\Yii::$app->getSession()->setFlash('success', 'Lead Updated');
		return $this->redirect(['index']);
	}
	
	public function actionPending($id)
    {
		if(!$model = Leads::find()->where(['chat_id'=>$id])->one()){
			$model = new Leads();
			$model->user_id = Yii::$app->user->identity->id;
			$model->chat_id = $id;
		}
		
		$model->status = 'pending';

        $model->save();
		
		\Yii::$app->getSession()->setFlash('success', 'Lead Updated');
		return $this->redirect(['index']);
	}
	
	public function actionEmailonlead(){
		//$message = "Line 1\r\nLine 2\r\nLine 3";
               // In case any of our lines are larger than 70 characters, we should use wordwrap()
               //$message = wordwrap($message, 70, "\r\n");
               // Send
              //mail('jatin.sehgal@triusmail.com', 'My Subject', $message);	 	
	    $API = new LiveChat_API();
              $sendmail = new Leads();
		$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
		$_GET['tag%5B%5D'] = 'lead';
		//$_GET["group"] = Yii::$app->user->identity->user_group;
		$chats = $API->chats->get($page,$_GET);
		
		    //echo '<pre>';print_r($chats);echo '</pre>';die("hghg");
		foreach($chats->chats as $chat){
			$users = User2::find()->where(['user_group'=>$chat->group[0]])->all();
			
			foreach($users as $user){
				$leads = Leads::find()->where(['user_group'=>$user['user_group'],'chat_id'=>$chat->id])->one();
			
				if($leads){
					$leads23 = Leads::find()->where(['user_group'=>$user['user_group'],'chat_id'=>$chat->id,'email' => 1])->one();
					if(!$leads23){
                        //echo "ok";
						$sent = $sendmail->sendEmail($user,$chat);
						/* if($sent){
							$model = Leads::find()->where(['user_group'=>$user['user_group'],'chat_id'=>$chat->id])->one();
							$model->email = 1;
							$model->save(); 
						} */
					}
				}else{
					$leads = new Leads();
					$leads->user_id = "0";
					$leads->user_group = $user['user_group'];
					$leads->chat_id = $chat->id;
					$leads->c_time = $chat->started_timestamp;
					$leads->status = 'pending';
					$leads->email = 0;
					if($leads->save()){
						//echo "ok";
						$sent = $sendmail->sendEmail($user,$chat);
						/* if($sent){
							$model = Leads::find()->where(['user_group'=>$user['user_group'],'chat_id'=>$chat->id])->one();
							//$model->email = 1;
							$model->save();
						} */
					}
				}
			}
			
			$leads->email = 1;
			$leads->save();
			//echo '<pre>';print_r($leads);echo '</pre>';die;
		}
		
		return true;
		/* echo '<pre>';print_r($chats->chats);echo '</pre>';
		die;
		$leads = Leads::find()->all();
		foreach($leads as $res){
		   $users = User2::findOne($res->user_id);
		    if(empty($res->email) || $res->email == '0'|| $res->email == 0){
				$sent = $sendmail->sendEmail($users,$res);
				if($sent){
					$model = Leads::find()->where(['user_id'=>$res->user_id])->one();
					$model->email = 1;
					$model->save();
				}
			}
			echo '<pre>';print_r(array($res->user_id,$res->email,$users->username));echo '</pre>';
			
		}
		die('ghj'); */
	}
	
	public function actionSendmail($id){
		
		//echo Url::base(true);die;
		$sendmail = new Leads();
	    $API = new LiveChat_API();
		$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
		$chat = $API->chats->getSingleChat($id);
		//$_GET['tag%5B%5D'] = 'lead';
		//$_GET["group"] = Yii::$app->user->identity->user_group;
		//$chats = $API->chats->get($page,$_GET);
		
		    //echo '<pre>';print_r($chats);echo '</pre>';die("hghg");
		//foreach($chats->chats as $chat){
			$users = User2::find()->where(['user_group'=>$chat->group[0]])->all();
			//echo '<pre>';print_r($user);echo '</pre>';die;
			
			foreach($users as $user){
			
				$leads = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
				
				if($leads){
					//echo '<pre>';print_r($leads);die;
					$leads23 = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
					if($leads23){
						$sent = $sendmail->sendEmail($user,$chat);
						if($sent){
							$model = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
							$model->email = 1;
							$model->save(); 
						}
					}
				}else{
					$lead = new Leads();
					$lead->user_id = $user['id'];
					$lead->chat_id = $chat->id;
					$lead->c_time = $chat->started_timestamp;
					$lead->status = 'pending';
					$lead->email = 0;
					if($lead->save()){
						$sent = $sendmail->sendEmail($user,$chat);
						if($sent){
							$model = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
							$model->email = 1;
							$model->save();
						}
					}
				}
			
			}
			
		//}
		
		\Yii::$app->getSession()->setFlash('success', 'Mail Sent Successfully');
		
		return $this->redirect('index');
		/* echo '<pre>';print_r($chats->chats);echo '</pre>';
		die;
		$leads = Leads::find()->all();
		foreach($leads as $res){
		   $users = User2::findOne($res->user_id);
		    if(empty($res->email) || $res->email == '0'|| $res->email == 0){
				$sent = Leads::sendEmail($users,$res);
				if($sent){
					$model = Leads::find()->where(['user_id'=>$res->user_id])->one();
					$model->email = 1;
					$model->save();
				}
			}
			echo '<pre>';print_r(array($res->user_id,$res->email,$users->username));echo '</pre>';
			
		}
		die('ghj'); */
	}
	
	public function actionRejected($id)
    {
		if(!$model = Leads::find()->where(['chat_id'=>$id])->one()){
			$model = new Leads();
			$model->user_id = Yii::$app->user->identity->id;
			$model->chat_id = $id;
		}
		
		$model->status = 'rejected';

        $model->save();
		
		\Yii::$app->getSession()->setFlash('success', 'Lead Updated');
		return $this->redirect(['index']);
        
	}

    /**
     * Deletes an existing Leads model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /* public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    } */

    /**
     * Finds the Leads model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Leads the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Leads::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
