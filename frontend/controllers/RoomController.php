<?php

namespace frontend\controllers;

use Yii;
use app\models\Property;
use app\models\Room;
use app\models\PropertySearch;
//use app\models\RoomSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

use dosamigos\qrcode\QrCode;
use dosamigos\qrcode\formats\MailTo;


class RoomController extends \yii\web\Controller
{
	public function behaviors()
    {
        return [
			'access' => [
				'class'=> AccessControl::className(),
				'only' => ['index','create', 'update','delete','another'],
				'rules'=>[
					[
						'actions' => ['create','another'],
						'allow' => true,
						'roles'=>['@'],
					]
				]
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
	
	public function actionCreate($id=false)
    { 
		if($id){
			
			$path = Yii::getAlias('@webroot');
			$userpath = $path.'/allusers/'.yii::$app->user->identity->username;
			$qrpath = $userpath.'/qrcode/';
			//QrCode::png('Hello, Mam Good Morning. How are you ?',$qrpath.'code.png');
			
			//$model = $this->findModel($id);
			//echo '<pre>';print_r($model->attributes);die;
			
			$model2 = new Property();
			$model = Room::findOne(['property_id' => $id]);
		
			//echo '<pre>';print_r($_POST);print_r($_FILES);echo '</pre>';die('here');

			if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
			$model->image = 454;
			$model->image = 465455;
			
			//echo '<pre>';print_r($model);die;
            //return $this->redirect(['view', 'id' => $model->id]);

			$total_images = count($_FILES['Room']['name']['image']);
			$total_images2 = count($_FILES['Room']['name']['image2']);
			
			if($total_images == 12 && $total_images2 == 12){
				
				$model->image = UploadedFile::getInstances($model, 'image');
				$model->image2 = UploadedFile::getInstances($model, 'image2');
				
				if($model->upload()){
				
					/***********
					*
					* HERE YOU CAN CALL THE BATCH FILES BECAUSE
					* FILES ARE UPLOADED TO THE temp1 AND temp2 FOLDERS
					*
					*************/
					
					$path = Yii::getAlias('@webroot');
					$userpath = $path.'/allusers/'.yii::$app->user->identity->username;
					
					$result1_path = $userpath.'/result1/temp1.jpg';
					$result2_path = $userpath.'/result2/temp2.jpg';
					
					$p_id = $model->property_id;
					$p_id2 = sprintf("%05d",$model->property_id);
					$r_id = $model->id;
					
					if(file_exists($result1_path)){
						// Below code moves the temp1.jpg from result1 to its property folder
						rename($result1_path,$userpath.'/'.$p_id2.'/'.$p_id.'_'.$r_id.'_result1.jpg');
						// Below code copy the temp1.jpg from result1 to its property folder
						//copy($result1_path,$userpath.'/'.$p_id2.'/'.$p_id.'_'.$r_id.'_result1.jpg');
					}
					
					if(file_exists($result2_path)){
						// Below code moves the temp2.jpg from result2 to its property folder
						rename($result2_path,$userpath.'/'.$p_id2.'/'.$p_id.'_'.$r_id.'_result2.jpg');
						// Below code copy the temp2.jpg from result2 to its property folder
						//copy($result2_path,$userpath.'/'.$p_id2.'/'.$p_id.'_'.$r_id.'_result2.jpg');
					}
					
					$webpath = Yii::$app->urlManager->createAbsoluteUrl(['/']);
					
					
					
					$model->image =  $p_id.'_'.$r_id.'_result1.jpg';
					$model->image2 = $p_id.'_'.$r_id.'_result2.jpg';
					$model->save();
					
					$p_res1_path = $webpath.'allusers/'.yii::$app->user->identity->username.'/'.$p_id2.'/';
					//.$p_id.'_'.$r_id.'_result1.jpg' and .$p_id.'_'.$r_id.'_result2.jpg'
					//$p_res2_path = $webpath.'allusers/'.yii::$app->user->identity->username.'/'.$p_id2.'/';
					
					
					$qrpath = $userpath.'/qrcode/';
					if (!is_dir($qrpath)) {
						mkdir($qrpath);
					}
					
					$mailTo = new MailTo(['email' => 'gagan.saini@triusmail.com']);
					QrCode::png($p_res1_path,$qrpath.'code.png');
					
					return $this->redirect(['room/success','id'=>$model->id]);

				}
				
				echo 'not uploaded';die;
			}
			else{
				\Yii::$app->getSession()->setFlash('error', 'You have to select 12 images');
				return $this->render('create', [
					'model' => $model,
					'model2' => $model2,
					'id' => $id,
				]);
			}

        } else {
            return $this->render('create', [
                'model' => $model,
                'model2' => $model2,
                'id' => $id,
            ]);
        }
		
		}
		else{
			//$this->redirect('@web');
			return $this->goHome();
		}
    }
	
	public function actionAnother($id=false)
    { 
		if($id){

        $model2 = new Property();
        $model = new Room();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
			$total_images = count($_FILES['Room']['name']['image']);
			$total_images2 = count($_FILES['Room']['name']['image2']);
			
			if($total_images == 12 && $total_images2 == 12){
				
				$model->image = UploadedFile::getInstances($model, 'image');
				$model->image2 = UploadedFile::getInstances($model, 'image2');
				
				if($model->upload()){
					$path = Yii::getAlias('@webroot');
					$userpath = $path.'/allusers/'.yii::$app->user->identity->username;
					
					$result1_path = $userpath.'/result1/temp1.jpg';
					$result2_path = $userpath.'/result2/temp2.jpg';
					
					$model->image =  'bcb';
					$model->image2 = 'fdg';
					$model->save();
					$p_id =$model->property_id;
					$p_id2 = sprintf("%05d",$model->property_id);
					$r_id = $model->id;
					
					if(file_exists($result1_path)){
						//rename($result1_path,$userpath.'/property'.$p_id.'/'.$p_id.'_'.$r_id.'_result1.jpg');
						copy($result1_path,$userpath.'/'.$p_id2.'/'.$p_id.'_'.$r_id.'_result1.jpg');
					}
					
					if(file_exists($result2_path)){
						//rename($result2_path,$userpath.'/property'.$p_id.'/'.$p_id.'_'.$r_id.'_result2.jpg');
						copy($result2_path,$userpath.'/'.$p_id2.'/'.$p_id.'_'.$r_id.'_result2.jpg');
					}
					
					$webpath = Yii::$app->urlManager->createAbsoluteUrl(['/']);
					
					$model->image =  $p_id.'_'.$r_id.'_result1.jpg';
					$model->image2 = $p_id.'_'.$r_id.'_result2.jpg';
					$model->save();
					
					$p_res1_path = $webpath.'allusers/'.yii::$app->user->identity->username.'/'.$p_id2.'/';
					//$p_res2_path = $webpath.'allusers/'.yii::$app->user->identity->username.'/'.$p_id2.'/';
					
					
					$qrpath = $userpath.'/qrcode/';
					if (!is_dir($qrpath)) {
						mkdir($qrpath);
					}
					
					$mailTo = new MailTo(['email' => 'gagan.saini@triusmail.com']);
					QrCode::png($p_res1_path,$qrpath.'code.png');
					
					return $this->redirect(['room/success','id'=>$model->id]);
				}
				
				echo 'not uploaded';die;
			}
			else{
				\Yii::$app->getSession()->setFlash('error', 'You have to select 12 images');
				return $this->render('create', [
					'model' => $model,
					'model2' => $model2,
					'id' => $id,
				]);
			}

        } else {
            return $this->render('create', [
                'model' => $model,
                'model2' => $model2,
                'id' => $id,
            ]);
        }
		
		}
		else{
			//$this->redirect('@web');
			return $this->goHome();
		}
    }
	
	 /**
     * Creates a Another Room.
     * If creation is successful, the browser will be redirected to the 'finish' page.
     * @return mixed
     */
    public function actionSuccess($id = false)
    {
        //$model = new Room();

        if ($id) {
			return $this->render('success', ['id' => $id]);
        } else {
            return $this->goHome();
        }
		
    }
	
	protected function findModel($id)
    {
        if (($model = Room::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
}
