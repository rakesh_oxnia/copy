<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\User;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use app\models\AppsCountries;
use common\models\PasswordForm;
use common\models\CustomPackageRange;
use common\models\AgencyUserPasswordForm;
use common\models\AdminSetting;
use app\models\BusinessCategory;
use yii\helpers\ArrayHelper;
use app\models\Notification;
use yii\filters\AccessControl;

/**
 * Settings controller
 */
class SettingsController extends Controller
{

	public $enableCsrfValidation = false;

	public function behaviors()
  {
      return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],  
                        'roles' => ['@']                        
                    ],
                    [
                        'allow' => true,
                        'actions' => ['change-agency-user-password', 'custom-package-range', 'save-iframe', 'save-color-code'],
                        'roles' => [User::ADMIN_ROLE, User::SUBADMIN_ROLE],                        
                    ]                    
                ],
            ], 
      ];
  }

	public function beforeAction($action)
	{
			if(Yii::$app->user->isGuest){
				return $this->redirect(['site/login']);
			}

			return parent::beforeAction($action);
	}

	public function actionIndex(){

		$success = "";
		$this->layout = 'kosmoouter';
		$user_id = Yii::$app->user->identity->id;
		$userData = User::findOne( $user_id );
		$model = new PasswordForm;
        $modeluser = User::find()->where([
            'username'=>Yii::$app->user->identity->username
        ])->one();

		if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                try{ 
                    $modeluser->password_hash = Yii::$app->security->generatePasswordHash($_POST['PasswordForm']['newpass']);
                    $modeluser->scenario = 'update_password';
                    if($modeluser->save()){
                        Yii::$app->getSession()->setFlash(
                            'success','Password has been successfully updated.'
                        );
                        $success = "Password has been successfully updated.";
                    }else{
                        Yii::$app->getSession()->setFlash(
                            'error','Password not changed'
                        );

                    }
                }catch(Exception $e){
                    Yii::$app->getSession()->setFlash(
                        'error',"{$e->getMessage()}"
                    );
                }
            }
        }
        $adminSetting	= AdminSetting::findOne(1);
				//Manager Email//
	      if(isset($_POST['manager_email'])){
           $adminSetting->manager_email = $_POST['email'];
					 if($adminSetting->save()){
						 $success = "manager Email has been saved successfully.";
					 }
				}

				//Manager Email//
  		if(isset($_POST['add_overview'])){
  			$userData->overview = $_POST['overview'];
  			$userData->country  = $_POST['country'];
  			$userData->industry  = $_POST['industry'];
  			$userData->number_of_employees  = $_POST['number_of_employees'];
  			$userData->business_name  = $_POST['business_name'];
  			$userData->website_url  = $_POST['website_url'];
				if( isset( $_POST['chat_purpose_sales'] ) ){
					$userData->chat_purpose_sales	 = 1;
				}else{
          $userData->chat_purpose_sales	 = 0;
        }
				if( isset( $_POST['chat_purpose_support'] ) ){
					$userData->chat_purpose_support	 = 1;
				}else{
          $userData->chat_purpose_support	 = 0;
        }
				if( isset( $_POST['chat_purpose_e_commerce'] ) ){
					$userData->chat_purpose_e_commerce	 = 1;
				}else{
          $userData->chat_purpose_e_commerce	 = 0;
        }
				if( isset( $_POST['b2b'] ) ){
					$userData->b2b	 = 1;
				}else{
          $userData->b2b	 = 0;
        }
				if( isset( $_POST['b2c'] ) ){
					$userData->b2c	 = 1;
				}else{
          $userData->b2c	 = 0;
        }
				if( isset( $_POST['internal_use'] ) ){
					$userData->internal_use	 = 1;
				}else{
          $userData->internal_use	 = 0;
        }
  			if($userData->save(false)){
  				$success = "Business Overview has been saved successfully.";
  			}
  		}else if(isset( $_POST['add_chat_settings'] )){
  			$userData->website_javascript = $_POST['website_javascript'];
  			if($userData->save(false)){
  				$success = "Chat Settings has been saved successfully.";
  			}
  		}else if(isset( $_POST['notification_settings'] )){
  			$userData->email_for_leads = $_POST['email_for_leads'];
  			$userData->email_for_chats = $_POST['email_for_chats'];
  			if($userData->save(false)){
  				$success = "Notification Settings has been saved successfully.";
  			}
  		}else if(isset($_POST['notify_email'])){
				if(isset($_POST['lead_email'])) {
					$_POST['lead_email'] = array_filter($_POST['lead_email']);
				}
				//remove any empty email
				if(isset($_POST['service_email'])) {
					$_POST['service_email'] = array_filter($_POST['service_email']);
				}
				if(isset($_POST['lead_service_email'])) {
					$_POST['lead_service_email'] = array_filter($_POST['lead_service_email']);
				}

				$notification	= Notification::find()->where([
						'user_id'=>Yii::$app->user->identity->id
				])->one();
				if(!empty($notification)){
				  if(isset($_POST['different_email'])){
						$notification->user_id = Yii::$app->user->identity->id;
						$notification->different_email = $_POST['different_email'];
						$notification->notify_lead = (isset($_POST['notify_lead'])) ? $_POST['notify_lead'] :0;
					  $notification->notify_chat = (isset($_POST['notify_chat'])) ? $_POST['notify_chat'] :0;
            $notification->notify_lead_email =(isset($_POST['lead_email'])) ? implode(", ",$_POST['lead_email']) :"";
            $notification->notify_chat_email =(isset($_POST['service_email'])) ? implode(", ",$_POST['service_email']) :"";
            $notification->notify_email = "";
					}else{
							$notification->user_id = Yii::$app->user->identity->id;
							$notification->different_email = 0;
							$notification->notify_lead = (isset($_POST['notify_lead'])) ? $_POST['notify_lead'] :0;
							$notification->notify_chat = (isset($_POST['notify_chat'])) ? $_POST['notify_chat'] :0;
							$notification->notify_lead_email = "";
							$notification->notify_chat_email = "";
							$notification->notify_email = (isset($_POST['lead_service_email'])) ? implode(", ",$_POST['lead_service_email']) :"";
					}
				}else{
					if(isset($_POST['different_email'])){
						$notification = new Notification;
						$notification->user_id = Yii::$app->user->identity->id;
						$notification->different_email = $_POST['different_email'];
						$notification->notify_lead = (isset($_POST['notify_lead'])) ? $_POST['notify_lead'] :0;
						$notification->notify_chat = (isset($_POST['notify_chat'])) ? $_POST['notify_chat'] :0;
						$notification->notify_lead_email =(isset($_POST['lead_email'])) ? implode(", ",$_POST['lead_email']) :"";
            $notification->notify_chat_email =(isset($_POST['service_email'])) ? implode(", ",$_POST['service_email']) :"";
						$notification->notify_email = "";
					}else{
						  $notification = new Notification;
							$notification->user_id = Yii::$app->user->identity->id;
							$notification->different_email = 0;
							$notification->notify_lead = (isset($_POST['notify_lead'])) ? $_POST['notify_lead'] :0;
							$notification->notify_chat = (isset($_POST['notify_chat'])) ? $_POST['notify_chat'] :0;
							$notification->notify_lead_email = "";
							$notification->notify_chat_email = "";
							$notification->notify_email = (isset($_POST['lead_service_email'])) ? implode(", ",$_POST['lead_service_email']) :"";
					}
				}
				$notification->save(false);
			}else if(isset($_POST['add_pay_as_go']))
			{
        $adminSetting = AdminSetting::findOne(1);
				$adminSetting->global_per_lead_cost = $_POST['global_per_lead_cost'];
				$adminSetting->global_per_lead_cost_currency = $_POST['global_per_lead_cost_currency'];
				//$adminSetting->scenario = 'global';
				if($adminSetting->save()){
				  $success = "Global Pay as you Go Data Data has been save succesfully.";
				}
			}

  		$userData = User::findOne( $user_id );
  		if (Yii::$app->session->hasFlash('success')){
  			$success = Yii::$app->session->getFlash('success');
  		}

  		$customer_id = trim(Yii::$app->user->identity->stripeToken);
			//var_dump($customer_id);exit;
			if($customer_id != ''){
  			//require_once("../../vendor/stripe/stripe-php/init.php");
  			\Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);
  			$cu = \Stripe\Customer::retrieve(array("id" => $customer_id, "expand" => array("default_source")));
  		}else{
  			$cu = [];
  		}

  		//echo $cu->default_source->last4;
  		//echo $cu->source->last4;exit;
  		$countries			= AppsCountries::find()->all();
  		$categories				= BusinessCategory::find()->all();

  		$changeAgencyUserPassModel = new AgencyUserPasswordForm();

  		$outletUsersArray = [];
  		$allUsersArray = [];

  		if(Yii::$app->user->identity->role == User::ROLE_ADMIN)
  		{
  			$allUsers = User::findAll(['status' => User::STATUS_ACTIVE]);
  			$allUsersArray = ArrayHelper::map($allUsers,'id', 'username');
  		}

  		if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE)
  		{
  			$outletUsers = User::findAll(['role' => User::ROLE_OUTLET, 'parent_id' => Yii::$app->user->identity->id]);
  			$outletUsersArray = ArrayHelper::map($outletUsers,'id', 'username');
  		}

		  $customPackageRanges = CustomPackageRange::find()->all();

      $iframeText = '';
	    $iframeText = '';
	    $adminUser = User::findOne(['role' => User::ROLE_ADMIN]);
	    $iframeText = $adminUser->iframe_tag;
      $loginColorCode = $adminUser->login_bg_color;

		return $this->render('index', ['userData' => $userData, 'success' => $success,
			'customer_id' => $customer_id, 'cu' => $cu,
			'countries' => $countries,
			'categories' => $categories,
			'model' => $model,
			'changeAgencyUserPassModel' => $changeAgencyUserPassModel,
			'allUsersArray' => $allUsersArray,
			'outletUsersArray' => $outletUsersArray,
			'customPackageRanges' => $customPackageRanges,
      'iframeText' => $iframeText,
			'loginColorCode' => $loginColorCode,
			'adminSetting' => $adminSetting,

		]);
	}

	public function actionChangeAgencyUserPassword()
	{
		$model = new AgencyUserPasswordForm();
		//echo "<pre>"; print_r(Yii::$app->request->post()); exit;
		if($model->load(Yii::$app->request->post()) && $model->validate()) {
		    $user_id = Yii::$app->request->post('id');
		    
			$userModel = empty($user_id) ? User::findOne(['email' => $model->user]) : User::findOne(['id' => $user_id]);
    
			$userModel->password_hash = Yii::$app->security->generatePasswordHash($model->newpass); 
			$userModel->scenario = 'update_password';
			if($userModel->save()) 
			{ 
				  if($model->emailNotification){
						$emailSent = \Yii::$app->mailer->compose(['html' => 'changeAgencyPassword-html', 'text' => 'changeAgencyPassword-text'], ['userModel' => $userModel,'passModel' => $model])
		                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
		                    ->setTo($userModel->email)
		                    ->setSubject('Chatmetrics: Password Changed') // mail subject
		                    ->send();
						Yii::$app->getSession()->setFlash(
								'success',"Password for user($userModel->email) has been changed and email notification to user is sent successfully."
						);
						return $this->redirect(['settings/index']);
					}

					Yii::$app->getSession()->setFlash(
							'success',"Password for user($userModel->email) has been changed successfully."
					);
					return $this->redirect(['settings/index']);

			}

			Yii::$app->getSession()->setFlash(
					'error','Password not changed'
			);
			//return $this->redirect(['site/index']);
			return $this->redirect(['settings/index']);
		}
	}

	public function actionCustomPackageRange()
	{
		if($post = Yii::$app->request->post()){
			CustomPackageRange::deleteAll();
			foreach ($post['rangeStart'] as $key => $value) {
				$customPackageRange = new CustomPackageRange();
				$customPackageRange->range_start = $post['rangeStart'][$key];
				$customPackageRange->range_end = $post['rangeEnd'][$key];
				$customPackageRange->amount = $post['amount'][$key];
				$customPackageRange->save();
			}
			Yii::$app->getSession()->setFlash(
					'success',"Custom range package saved successfully."
			);
			return $this->redirect(['settings/index']);
		}
	}

	public function actionSaveIframe()
	{
		$iframe_text = Yii::$app->request->post('iframe_text');
		$adminUsers = User::findAll(['role' => User::ROLE_ADMIN]);

		foreach ($adminUsers as $admin) {
			$admin->iframe_tag = $iframe_text;
			$admin->save();
		}

		return json_encode(['success' => true]);
	}

  public function actionSaveColorCode()
	{
		$color_code = Yii::$app->request->post('color_code');
		$adminUsers = User::findAll(['role' => User::ROLE_ADMIN]);

		foreach ($adminUsers as $admin) {
			$admin->login_bg_color = $color_code;
			$admin->save();
		}

		return json_encode(['success' => true]);
	}
}
