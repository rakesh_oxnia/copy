<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\User;
use app\models\UserSearch;
use frontend\models\PasswordResetRequestForm;


class UserAccessController extends \yii\web\Controller
{
	public function behaviors()
	{
	    return [
	        'access' => [
	            'class' => AccessControl::className(),
	            'rules' => [
	                [
	                    'allow' => true,	                    
	                    'actions' => ['index', 'create'],
	                    'roles' => [User::ADMIN_ROLE, User::CLIENT_ROLE, User::FRANCHISE_ROLE],
	                ],
	               	[
	                    'allow' => true,
	                    'actions' => ['view', 'update', 'delete'],
	                    'roles' => [User::ADMIN_ROLE, User::CLIENT_ROLE, User::FRANCHISE_ROLE],
	                    'matchCallback' => function ($rule, $action) {
                            $user = User::findOne(Yii::$app->request->get('id'));
							return Yii::$app->user->identity->id == $user->parent_id;                       
                        }
	                ]	 	                               
	            ],
	        ],
	    ];
	}

	public function beforeAction($action)
    {    	

        if(in_array(Yii::$app->user->identity->role, [User::ROLE_USER, User::ROLE_FRANCHISE])
         && !empty(Yii::$app->user->identity->parent_id)) {     
            $user_id = Yii::$app->user->identity->id;
            $parent = User::findOne(Yii::$app->user->identity->parent_id);   
            yii::$app->user->login($parent);
            Yii::$app->session->set('user_client_franchise', $user_id);       
        }        
        
		if(Yii::$app->user->isGuest) {
			return $this->redirect(['site/login']);
		}

        return parent::beforeAction($action);
    }

    public function render($view, $params)
    { 

        if(Yii::$app->session->has('user_client_franchise')) { 
          $user = User::findOne(Yii::$app->session->get('user_client_franchise'));  
          Yii::$app->session->remove('user_client_franchise');             
          yii::$app->user->login($user);             
        }

        // before render
        return parent::render($view, $params);
    }    


    public function actionIndex()
    {   
    	if (Yii::$app->user->can(User::ADMIN_ROLE)) {
    		$searchModel = new UserSearch(['role' =>  User::ROLE_SUBADMIN]); // get all sub admins
    		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);
    	} else {
    		$searchModel = new UserSearch(['parent_id' => Yii::$app->user->identity->id]);
    		$dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);
    	}        

        $this->layout = 'kosmoouter';   
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionCreate() 
    {

    	$user = new User;

    	if ($user->load(Yii::$app->request->post()) && $user->validate()) {
			if (Yii::$app->user->can(User::ADMIN_ROLE)) {
				$user->role = User::ROLE_SUBADMIN;				
			} else {
				$user->role = User::ROLE_ASSIST;
				$user->multisiteaccess = true;
			}
    		
    		$user->parent_id = yii::$app->user->identity->id;
    		$user->user_group = yii::$app->user->identity->user_group;	
    		$user->website_url = yii::$app->user->identity->website_url;
    		$user->business_name = yii::$app->user->identity->business_name;

    		if(!empty(Yii::$app->request->post('website_user')) && empty(Yii::$app->request->post('outlet_user'))) {
    			$website_user = User::findOne(['user_group' => Yii::$app->request->post('website_user')]);
	    	 	$user->user_group = Yii::$app->request->post('website_user');	    	 	
	    		$user->website_url = $website_user->website_url;
	    		$user->business_name = $website_user->business_name;
	    		$user->multisiteaccess = false;
    		}

    		if(empty(Yii::$app->request->post('website_user')) && !empty(Yii::$app->request->post('outlet_user'))) {    			
    			$outlet_user = User::findOne(['id' => Yii::$app->request->post('outlet_user')]);
    			$user->outlet = $outlet_user->id;	  
	    	 	$user->user_group = $outlet_user->user_group;	    	 	
	    		$user->website_url = $outlet_user->website_url;
	    		$user->business_name = $outlet_user->business_name;
	    		$user->multisiteaccess = false;
    		}

    		/* set user properties defaults */
			$user->agency_name = '';
			$user->agency_logo = '';
			$user->theme_color = '';
			$user->welcome_message = '';
			$user->your_name = '';
			$user->company_name = '';
			$user->company_url = '';
			$user->telephone = '';
			$user->chat_purpose_sales = 0;
			$user->chat_purpose_support = 0;
			$user->industry = 0;
			$user->number_of_employees = 0;
			$user->country = 0;
			$user->b2b = 0;
			$user->b2c = 0;
			$user->internal_use = 0;
			$user->overview = '';
			$user->timezone = 0;
			$user->website_javascript = '';
			$user->email_for_leads = '';
			$user->email_for_chats = '';
			$user->stripeToken = '';
			$user->send_transcript = 0; 

			/* Send welcome email with password reset link */
    		$password = rand();
            $user->setPassword($password);
            $user->generateAuthKey();
			if ($user->save()) {
				if (Yii::$app->user->can(User::ADMIN_ROLE)) {
					$role = Yii::$app->authManager->getRole(User::SUBADMIN_ROLE);
					Yii::$app->authManager->assign($role, $user->id);
				} else {
					$role = Yii::$app->authManager->getRole(User::ASSIST_ROLE);
					Yii::$app->authManager->assign($role, $user->id);
				}

				$role_name = Yii::$app->request->post('role');
				$role = Yii::$app->authManager->getRole($role_name);
				Yii::$app->authManager->assign($role, $user->id);
				
				$model_reset = new PasswordResetRequestForm();
				$model_reset->email = $user->email;
				$model_reset->password = $password;
				if($model_reset->validate()){
					if ($model_reset->sendEmail("test")) { 
						Yii::$app->getSession()->setFlash('success', 'User Created Mail Send To User\'s Email.');
						return $this->redirect(['index']);
					} else {
						Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to Send Email.');
					}
				} 

				return $this->redirect(['index']);
			}    		
    	} 

    	$searchModel = new UserSearch(['id' => Yii::$app->user->identity->id]);
   		$roles = $searchModel->getRoles();
        
    	$website_users = User::find()->where(['email' => Yii::$app->user->identity->email])->all();

        $sql = "Select id, name, website_url from `user` where `role`=". User::ROLE_OUTLET." and parent_id in (select id from `user` where id='".Yii::$app->user->identity->id."' or parent_id='".Yii::$app->user->identity->id."')";                      
        $outlets = Yii::$app->db->createCommand($sql)->queryAll();

        $this->layout = 'kosmoouter';   
        return $this->render('create', ['model' => $user, 
        	'roles' => $roles, 'error' => $user->errors,
        	'website_users' => $website_users, 'outlets' => $outlets
        ]);
    }

    public function actionView($id)
    {       
    	$model = User::findOne($id);

        $roles = Yii::$app->authManager->getRolesByUser($id);

        $default_roles = [User::SUBADMIN_ROLE, User::ASSIST_ROLE, User::OUTLET_ROLE, User::SERVICE_ACCOUNT_ROLE];
        foreach ($default_roles as $default_role) {
        	if(array_key_exists($default_role, $roles)) {
        		unset($roles[$default_role]);
        	}
        }

        $this->layout = 'kosmoouter';   
        return $this->render('view', ['model' => $model, 'roles' => array_keys($roles)]);
    }

   	public function actionUpdate($id) 
	{ 
		$user = User::findOne($id);

		if ($user->load(Yii::$app->request->post()) && $user->validate()) {

			if(!empty(Yii::$app->request->post('website_user')) && empty(Yii::$app->request->post('outlet_user'))) {

    			$website_user = User::findOne(['user_group' => Yii::$app->request->post('website_user')]);
	    	 	$user->user_group = Yii::$app->request->post('website_user');	    	 	
	    		$user->website_url = $website_user->website_url;
	    		$user->business_name = $website_user->business_name;
	    		$user->multisiteaccess = false;
    		}

    		if(empty(Yii::$app->request->post('website_user')) && !empty(Yii::$app->request->post('outlet_user'))) {
    			$outlet_user = User::findOne(['id' => Yii::$app->request->post('outlet_user')]);
    			$user->outlet = $outlet_user->id;
	    	 	$user->user_group = $outlet_user->user_group;	    	 	
	    		$user->website_url = $outlet_user->website_url;
	    		$user->business_name = $outlet_user->business_name;
	    		$user->multisiteaccess = false;
    		}

			$user->save();

			/* Revoke all permissions before we new update permissions */
			Yii::$app->authManager->revokeAll($id);

			if (Yii::$app->user->can(User::ADMIN_ROLE)) {
				$role = Yii::$app->authManager->getRole(User::SUBADMIN_ROLE);
				Yii::$app->authManager->assign($role, $user->id);
			} else {
				$role = Yii::$app->authManager->getRole(User::ASSIST_ROLE);
				Yii::$app->authManager->assign($role, $user->id);
			}


			$role_name = Yii::$app->request->post('role');
			$role = Yii::$app->authManager->getRole($role_name);
			Yii::$app->authManager->assign($role, $user->id);

			Yii::$app->getSession()->setFlash('success', 'Successfully updated!');
		} 			

    	$searchModel = new UserSearch(['id' => Yii::$app->user->identity->id]);
   		$roles = $searchModel->getRoles();

		$user_roles = Yii::$app->authManager->getRolesByUser($id);

        $default_roles = [User::SUBADMIN_ROLE, User::ASSIST_ROLE, User::OUTLET_ROLE, User::SERVICE_ACCOUNT_ROLE];
        foreach ($default_roles as $default_role) {
        	if(array_key_exists($default_role, $user_roles)) {
        		unset($user_roles[$default_role]);
        	}
        };

        $website_users = User::find()->where(['email' => Yii::$app->user->identity->email])->all();

        $sql = "Select id, name, website_url from `user` where `role`=". User::ROLE_OUTLET." and parent_id in (select id from `user` where id='".Yii::$app->user->identity->id."' or parent_id='".Yii::$app->user->identity->id."')";                      
        $outlets = Yii::$app->db->createCommand($sql)->queryAll();

	    $this->layout = 'kosmoouter';   
	    return $this->render('update', ['model' => $user, 'user_roles' => array_keys($user_roles),
	    	'roles' => $roles, 'error' => $user->errors,
	    	'website_users' => $website_users, 'outlets' => $outlets
	    ]);
	}

   	public function actionDelete($id) 
	{
		$user = User::findOne($id);

		/* Revoke all permissions before delete the user */
		Yii::$app->authManager->revokeAll($id);		

		if ($user->delete()) {
			Yii::$app->getSession()->setFlash('success', 'Successfully deleted!');
		} 			

		return $this->redirect(['index']);
	}

}