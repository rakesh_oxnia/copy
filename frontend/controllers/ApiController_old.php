<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\User;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LiveChat_API;


/**
 * Site controller
 */
class ApiController extends Controller
{

	public function actionSignup(){
		print_r("API working");
    exit;
	}

	// API for Zapier Integration
	public function actionSuccess(){
		$this->layout = 'kosmologin';
		$msg		  = "";
		$userData = User::find()->where(['username' => $_GET['name']])->one();
		if( empty( $userData ) ){
			$user = new User();
			$password = rand();
            $user->username = $_GET['name'];
			$user->your_name = $_GET['name'];
            $user->email = $_GET['email'];
            $user->role = 10;
            $user->setPassword( $password );
            $user->generateAuthKey();
            if ($user->save()) {

                \Yii::$app->mailer->compose(['html' => 'usernamePasswordsend-html', 'text' => 'usernamePasswordsend-text'], ['user' => $user, "password" => $password])
                                ->setFrom([\Yii::$app->params['supportEmail'] => 'Chat Metrics'])
                                ->setTo($_GET['email'])
                                ->setSubject('Welcome to your Dashboard ') // user create mail subject
                                ->send();

				$model 			 = new LoginForm();
				$model->email = $_GET['email'];
				$model->password = "$password";
				if($model->login()){
					return $this->redirect(['/install/step']);
				}else{
					$msg = "Some Problem has occured. Please try Later!!";
				}
            }
		}else{
			$msg = "Username already exist in database";
		}
		return $this->render('success', ['msg' => $msg]);
	}

	/*
	*API for mobile application starts from here
	*/

	public function actionLogin(){
		$model = new LoginForm();
		$model->username = $_REQUEST['username'];
		$model->password = $_REQUEST['password'];
    //print_r($_REQUEST); exit;
        if ($model->login()) {
            echo "login(" . json_encode(['response' => 'success', 'user_id' => Yii::$app->user->id]) . ");";
        }else{
			echo "login(" . json_encode(['response' => 'error']) . ");";
		}
	}


	public function actionGetleads(){
		//echo 'leads({"response":"success","data":[{"name":"Alexandra","email":"Alexandramorrell@hotmail.com","region":"Victoria","country":"Australia"},{"name":"Ed","email":"Ed.ray@gmail.com","region":"Western Australia","country":"Australia"},{"name":"Dom","email":"dominic.t.prior@gmail.com","region":"Victoria","country":"Australia"},{"name":"Amber","email":"Mu4892@gmail.com","region":"Victoria","country":"Australia"},{"name":"Alexandra","email":"alexandramorrell@hotmail.com","region":"Victoria","country":"Australia"},{"name":"Belinda","email":"Belinda.catherine.allen@gmail.com","region":"Victoria","country":"Australia"},{"name":"jacqui nelson","email":"nelson.group@ozemail.com.au","region":"Victoria","country":"Australia"},{"name":"Daniel","email":"daniel_fieldzy@hotmail.com","region":"Victoria","country":"Australia"},{"name":"richa","email":"bladibum@gmail.com","region":"Victoria","country":"Australia"},{"name":"Emily Brown","email":"emily.jb@live.com","region":"Victoria","country":"Australia"},{"name":"Odol","email":"odol_ojolo7@hotmail.com","region":"Victoria","country":"Australia"},{"name":"Audra","email":"audrarhodes@gmail.com","region":"Victoria","country":"Australia"},{"name":"Melissa Caprioli","email":"mj_4784@hotmail.com","region":"Victoria","country":"Australia"},{"name":"Linda Curcio","email":"linda.curcio@transport.vic.gov.au","region":"Victoria","country":"Australia"},{"name":"Kristyn","email":"pixie_lips@live.com.au","region":"Victoria","country":"Australia"},{"name":"SIddharth Mehra","email":"sidmehra.wwe17@gmail.com","region":"Victoria","country":"Australia"}]});';exit;
		//echo "leads(" . json_encode(['response' => 'success', 'data' => ['aa' => 'aa', 'bb' => 'bb']]) . ");";exit;
		$group = 0;
	    //mail("iamparam.surya@gmail.com","API subject", json_encode($_REQUEST));
	    $userData = User::findOne($_REQUEST['user_id']);
	    if( !empty( $userData ) ){
	        $group = $userData['user_group'];
	    }else{
	        echo "leads(" . json_encode(['response' => 'error']) . ");";exit;
	    }

	    if($group > 0){
			try {

				$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
				if(!isset($_GET["date_from"])){
					$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
					$_GET['date_to'] = date('Y-m-d');
				}

				$_GET['tag%5B%5D'] = 'lead';
				$_GET["group"] = $group;

				$API = new LiveChat_API();

				$chats = $API->chats->get($page,$_GET);


			}
			catch (Exception $e) {
				//throw new NotFoundHttpException($e->getMessage());
				echo "leads(" . json_encode(['response' => 'error']) . ");";exit;
			}
		}
		//echo 'leads({"response":"success","data":[{"name":"Alexandra","email":"Alexandramorrell@hotmail.com","region":"Victoria","country":"Australia"},{"name":"Ed","email":"Ed.ray@gmail.com","region":"Western Australia","country":"Australia"},{"name":"Dom","email":"dominic.t.prior@gmail.com","region":"Victoria","country":"Australia"},{"name":"Amber","email":"Mu4892@gmail.com","region":"Victoria","country":"Australia"},{"name":"Alexandra","email":"alexandramorrell@hotmail.com","region":"Victoria","country":"Australia"},{"name":"Belinda","email":"Belinda.catherine.allen@gmail.com","region":"Victoria","country":"Australia"},{"name":"jacqui nelson","email":"nelson.group@ozemail.com.au","region":"Victoria","country":"Australia"},{"name":"Daniel","email":"daniel_fieldzy@hotmail.com","region":"Victoria","country":"Australia"},{"name":"richa","email":"bladibum@gmail.com","region":"Victoria","country":"Australia"},{"name":"Emily Brown","email":"emily.jb@live.com","region":"Victoria","country":"Australia"},{"name":"Odol","email":"odol_ojolo7@hotmail.com","region":"Victoria","country":"Australia"},{"name":"Audra","email":"audrarhodes@gmail.com","region":"Victoria","country":"Australia"},{"name":"Melissa Caprioli","email":"mj_4784@hotmail.com","region":"Victoria","country":"Australia"},{"name":"Linda Curcio","email":"linda.curcio@transport.vic.gov.au","region":"Victoria","country":"Australia"},{"name":"Kristyn","email":"pixie_lips@live.com.au","region":"Victoria","country":"Australia"},{"name":"SIddharth Mehra","email":"sidmehra.wwe17@gmail.com","region":"Victoria","country":"Australia"}]});';exit;
		$transcripts = $chats->chats;

		$data = [];

    		foreach($transcripts as $row => $transcript){
    		    foreach($transcript as $key => $value){
    		        if($key == 'visitor'){
    		            $final_value = [];
    		            if($key == 'visitor'){
    		                //echo "aa";exit;
    		                foreach($value as $msg_key => $message){
    		                        if($msg_key != 'id' && $msg_key != 'ip' && $msg_key != 'city' && $msg_key != 'country_code' && $msg_key != 'timezone'){
    		                            $final_value[$msg_key] = $message;
    		                        }
    		                    }

    		            }
    		            $data[$row] = $final_value;
    		        }
    		    }
    		}
		echo "leads(" . json_encode(['response' => 'success', 'data' => $data]) . ");";exit;
	}

	// Code for mobile application
	public function actionLeadlist(){

		$group = 0;
	    $userData = User::findOne($_REQUEST['user_id']);
	    if( !empty( $userData ) ){
	        $group = $userData['user_group'];
	    }else{
	        echo "leads(" . json_encode(['response' => 'error']) . ");";exit;
	    }

	    if($group > 0){
			try {

				$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;

				if(!isset($_GET["date_from"])){
					$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
					$_GET['date_to'] = date('Y-m-d');
				}

				$_GET['tag%5B%5D'] = 'lead';
				$_GET["group"] = $group;

				$API = new LiveChat_API();

				$chats = $API->chats->get($page,$_GET);
				echo "hello";exit;
				echo "leads(" . json_encode($chats) . ");";exit;

			}
			catch (Exception $e) {
				//throw new NotFoundHttpException($e->getMessage());
				echo "leads(" . json_encode(['response' => 'error']) . ");";exit;
			}
		}



	}

}
