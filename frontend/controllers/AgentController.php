<?php

namespace frontend\controllers;

use Yii;
use app\models\User2;
use common\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\PasswordResetRequestForm;
use app\models\CustomLeadFieldsOption;
use app\models\CustomLeadFields;

/**
 * UserController implements the CRUD actions for User2 model.
 */
class AgentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
	
	public $layout = "dashboard";

    /**
     * Lists all User2 models.
     * @return mixed
     */
    public function actionIndex()
    {
		die("dsds");
        $searchModel = new UserSearch(['role'=>'10']);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionDeleteFieldsFromAgentApp(){
              $delete_checkbox_arrays = Yii::$app->request->post('delete_checkbox_array');
              if($delete_checkbox_arrays){
                      foreach($delete_checkbox_arrays as $delete_checkbox_array){
                                 $customLeadFields = CustomLeadFields::findOne($delete_checkbox_array);
                                 $customLeadFields->soft_delete = 1;
                                 $customLeadFields->save();
                      }
              }
              return json_encode(['success'=>true,'data'=>'Fields Deleted Successfully']);
    }

    public function actionUpdateFieldsToAgentApp()
    {
      $field_label = htmlspecialchars(Yii::$app->request->post('field_label'));
      $field_type = Yii::$app->request->post('field_type');
      $required = Yii::$app->request->post('required');
      $field_status = Yii::$app->request->post('field_status');
      $field_options = Yii::$app->request->post('field_option');
      $field_option_ids = Yii::$app->request->post('field_option_id');
      $user_id = Yii::$app->request->post('user_id');
      $field_id = Yii::$app->request->post('field_id');
      //return json_encode(['success'=>true,'data'=>$field_option_ids]);
      //echo '<pre>';print_r($field_option_ids);exit;
      $customLeadFields = CustomLeadFields::findOne($field_id);
      $customLeadFields->type = $field_type;
      $customLeadFields->label = $field_label;
      $customLeadFields->required = $required;
      $customLeadFields->status = $field_status;
      $customLeadFields->created_by = Yii::$app->user->identity->id;
      $customLeadFields->updated_at = strtotime("now");
      $customLeadFields->save();
      if($field_options){
          if($field_option_ids){
            $customLeadFieldsOptionidDeactivates = CustomLeadFieldsOption::find()->where(['custom_leads_field_id'=>$field_id])
                                                  ->all();
            foreach($customLeadFieldsOptionidDeactivates as $customLeadFieldsOptionidDeactivate){
              $customLeadFieldsOptionidDeactivate->soft_delete = 1;
              $customLeadFieldsOptionidDeactivate->save();
            }
            foreach($field_option_ids as $field_option_id){
              if(isset($field_option_id['id'])){
                //return json_encode(['success'=>true,'data'=>'Fields Added Successfully6']);
                $customLeadFieldsOptionid = CustomLeadFieldsOption::findOne($field_option_id['id']);
                $customLeadFieldsOptionid->option = htmlspecialchars($field_option_id['name']);
                $customLeadFieldsOptionid->updated_at = strtotime("now");
                $customLeadFieldsOptionid->soft_delete = 0;
                $customLeadFieldsOptionid->save();
              }else{
                $customLeadFieldsOption = new CustomLeadFieldsOption();
                $customLeadFieldsOption->custom_leads_field_id = $customLeadFields->id;
                $customLeadFieldsOption->option = htmlspecialchars($field_option_id['name']);
                $customLeadFields->status = 1;
                $customLeadFieldsOption->created_at = strtotime("now");
                $customLeadFieldsOption->save();
              }
            }
            //return json_encode(['success'=>true,'data'=>$field_option_ids]);
          }else{
            //return json_encode(['success'=>true,'data'=>'Fields Added Successfully2']);
          }
      }else{
        $customLeadFieldsOptionidDeactivates = CustomLeadFieldsOption::find()->where(['custom_leads_field_id'=>$field_id])
                                              ->all();
        foreach($customLeadFieldsOptionidDeactivates as $customLeadFieldsOptionidDeactivate){
          $customLeadFieldsOptionidDeactivate->soft_delete = 1;
          $customLeadFieldsOptionidDeactivate->save();
        }
        //return json_encode(['success'=>true,'data'=>'Fields Added Successfully4']);
      }
        return json_encode(['success'=>true,'data'=>'Fields Added Successfully']);
    }

    public function actionAddFieldsToAgentApp()
    {
           $field_label = htmlspecialchars(Yii::$app->request->post('field_label'));
           $field_type = Yii::$app->request->post('field_type');
           $required = Yii::$app->request->post('required');
           $field_status = Yii::$app->request->post('field_status');
           $field_options = Yii::$app->request->post('field_option');
           $user_id = Yii::$app->request->post('user_id');
           $customLeadFields = new CustomLeadFields();
           $customLeadFields->user_id = $user_id;
           $customLeadFields->type = $field_type;
           $customLeadFields->label = $field_label;
           $customLeadFields->required = $required;
           $customLeadFields->status = $field_status;
           $customLeadFields->created_by = Yii::$app->user->identity->id;
           $customLeadFields->created_at = strtotime("now");
           $customLeadFields->save();
           if($field_options){
             foreach($field_options as $field_option)
             {
               if(!empty($field_option)){
                 $customLeadFieldsOption = new CustomLeadFieldsOption();
                 $customLeadFieldsOption->custom_leads_field_id = $customLeadFields->id;
                 $customLeadFieldsOption->option = htmlspecialchars($field_option);
                 $customLeadFields->status = 1;
                 $customLeadFieldsOption->created_at = strtotime("now");
                 $customLeadFieldsOption->save();
               }
             }
           }
          return json_encode(['success'=>true,'data'=>'Fields Added Successfully']);
    }

    /**
     * Displays a single User2 model.
     * @param integer $id
     * @return mixed
     */
    /* public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    } */

    /**
     * Creates a new User2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /* public function actionCreate()
    {
        $model = new User2();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
			//echo '<pre>';print_r($model->username);die;
			
			$user = new User();
            $user->username = $model->username;
            $user->email = $model->email;
            $user->website_url = $model->website_url;
            $user->setPassword(time().rand());
            $user->generateAuthKey();
			if ($user->save()) {
                //return $user;
				$model_reset = new PasswordResetRequestForm();
				$model_reset->email = $model->email;
				if($model_reset->validate()){
					if ($model_reset->sendEmail()) {
						Yii::$app->getSession()->setFlash('success', 'User Created Mail Send To User\'s Email.');
						//return $this->goHome();
						return $this->redirect(['index']);
					} else {
						Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to Send Email.');
					}
				}
				return $this->redirect(['index']);
            }
			else{
				return $this->render('create', [
					'model' => $model,
				]);
			}
			
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    } */

    /**
     * Updates an existing User2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    /* public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    } */

    /**
     * Deletes an existing User2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /* public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    } */

    /**
     * Finds the User2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    /* protected function findModel($id)
    {
        if (($model = User2::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    } */
	
	/* public function actionChangepassword(){
        $layout_ = $this->actionGetlayout();
        $this->layout = $layout_;
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->password_hash = Yii::$app->security->generatePasswordHash(Yii::$app->request->post()['new_password']);
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('changepassword', [
                'model' => $model,
            ]);
        }        
    } */
	
}
