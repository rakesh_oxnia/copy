<?php

namespace frontend\controllers;

use Yii;
use app\models\User2;
use common\models\User;
use app\models\Leads;
use frontend\models\Invoices;
use common\models\Packages;
use app\models\LeadsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\LiveChat_API;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii2tech\csvgrid\CsvGrid;
use yii\data\ArrayDataProvider;
use common\models\UserCategory;
use yii\helpers\ArrayHelper;
use common\models\LeadSeen;
use common\models\PackageUser;
use common\models\ForwardingEmailAddress;
use common\models\ForwardingEmailTeam;
use common\models\UserAssociate;
use common\models\EmailLog;
use common\models\Chat;
use common\models\ChatMessage;
use common\models\ServiceChatEmail;
use common\models\ZapierLogs;
use common\models\ZapierRequestLogs;
use app\models\Notification;
use app\models\CustomLeadFields;
use app\models\CustomLeadFieldsOption;
use common\models\LoginForm;

/**
 * LeadsController implements the CRUD actions for Leads model..
 */
class LeadsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
              'class'=> AccessControl::className(),
              'only' => ['index','total','view'],
              'rules'=>[
                [
                  'actions' => ['index','total','view'],
                  'allow' => true,
                  'matchCallback' => function ($rule, $action) {
                    return Yii::$app->user->can('viewReport');                     
                  }  
                ]
              ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'corsFilter' => [
              'class' => \yii\filters\Cors::className(),
              'cors' => [
                      // restrict access to
                      'Origin' => [Yii::$app->params['baseUrl']],
                      'Access-Control-Request-Method' => ['POST', 'PUT'],
                      // Allow only POST and PUT methods
                      'Access-Control-Request-Headers' => ['X-Wsse'],
                      // Allow only headers 'X-Wsse'
                      'Access-Control-Allow-Credentials' => true,
                      // Allow OPTIONS caching
                      'Access-Control-Max-Age' => 3600,
                      // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                      'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                  ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'push-new-lead' || $action->id == 'update-agent-app' || $action->id == 'get-sub-categories' || $action->id == 'save-chat') {         
            $this->enableCsrfValidation = false;
        }
        
        if(!parent::beforeAction($action)) {
            return false;
        }

        LoginForm::userAssitLogin();

        // set website filter
        if(Yii::$app->request->post('website_filter')) {
          if(Yii::$app->request->post('website_filter') == 'all') {
            Yii::$app->session->remove('website_filter'); 
            Yii::$app->session->remove('outlet_filter');               
          } else {
            Yii::$app->session->set('website_filter', Yii::$app->request->post('website_filter')); 
            Yii::$app->session->remove('outlet_filter');
          }         
        }

        // set outlet filter
        if(Yii::$app->request->post('outlet_filter')) {
          if(Yii::$app->request->post('outlet_filter') == 'all') {
            Yii::$app->session->remove('outlet_filter');                
          } else {
            Yii::$app->session->set('outlet_filter', Yii::$app->request->post('outlet_filter')); 
            $outletuser = User::findOne(['id' => Yii::$app->request->post('outlet_filter')]);         
            Yii::$app->session->set('website_filter', $outletuser->user_group);                
          }
        }        

        return true;
    }


    public function render($view, $params)
    {
        $params = LoginForm::userAssitLogout($params);

        if(Yii::$app->user->identity->role == User::ROLE_OUTLET) {  
          unset($params['website_users']);                        
        }

        // before render
        return parent::render($view, $params);
    }     

  public $layout = 'dashboard';

  /*

  2452-Pal Sidhu(Freelancer) Atul: 63736fcd97a00449d5d8e7a5d6749a01
[6:22:52 AM] 2452-Pal Sidhu(Freelancer) Atul: pal@fuba.com.au

  */

  public function actionExport($filter = '', $date_from = '', $date_to = '', $group_by = '', $page = '', $category = ''){
    $page       = 1;
    $totalpages   = 0;
    $freeLeads    = 5;
    $filter     = "";
    $date_from    = "";
    $date_to    = "";
    $group_by     = "";
    $category = '';
    $packageInfo  = [];
    $leads      = [];
    $this->layout   = "kosmleadsandchats";
    $user_id    = Yii::$app->user->identity->id;

    $invoiceData = Invoices::find()->where(['user_id' => $user_id])->all();
    if( !empty( $invoiceData ) ){
      foreach( $invoiceData as $key => $invoice){
        if($invoice['package_id'] > 0){

          $package_id  = $invoice['package_id'];
          $packageData = Packages::findOne( $package_id );

          if( !empty( $packageData ) ){
            $packageInfo[$invoice['payment_date']] = $packageData['number_of_leads'];
            Yii::$app->session[$invoice['payment_date']] = 0;
          }
        }
      }
    }

        try {
      $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
      if(!isset($_GET["date_from"])){
        $_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
        $_GET['date_to'] = date('Y-m-d');
      }else{
        $filter = $_GET['filter'];
        $date_from = $_GET['date_from'];
        $date_to = $_GET['date_to'];
        $group_by = $_GET['group_by'];
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $category = isset($_GET['category']) ? $_GET['category'] : '';
      }
      $_GET['tag%5B%5D'] = 'lead';
      $_GET["group"] = Yii::$app->user->identity->user_group;
      $API = new LiveChat_API();
      $chats = $API->chats->get($page,$_GET);
    }
    catch (Exception $e) {
      throw new NotFoundHttpException($e->getMessage());
    }

    $transcripts = $chats->chats;
    $total = $chats->total;
    $pages = $chats->pages;

    $leads = Leads::filterLeadPageData( $transcripts, $leads );

    for($page = 2; $page <= $pages; $page++){
      $API      = new LiveChat_API();
      $_GET['page'] = $page;
      $chats      = $API->chats->get($page,$_GET);
      $transcripts  = $chats->chats;
      $leads      = Leads::filterLeadPageData( $transcripts, $leads );
    }

    $leads = array_reverse( $leads );

    //$leads = Leads::getAllLeads($leads, $packageInfo);
    $leads = Leads::getAllLeadsData($leads, $packageInfo);

    $leads = array_reverse( $leads );

    // added by Ravindra
    foreach ($leads as $key => $chat) {
      //echo "<pre>"; print_r($chat); exit;
      //category part
      if(isset($_GET['category']) && $_GET['category'] != ''){
        $leadModel = Leads::findOne(['chat_id' => $chat['id']]);
        if($leadModel && $leadModel->category_id != NULL){
          if($_GET['category'] == $leadModel->category_id){
            $catModel = UserCategory::findOne($leadModel->category_id);
            if($catModel){
              $leads[$key]['category'] = ['id' => $catModel->id, 'name' => $catModel->category_name];
            }
          }else{
            unset($leads[$key]);
            continue;
          }
        }else{
          unset($leads[$key]);
          continue;
        }
      }else{
        $leadModel = Leads::findOne(['chat_id' => $chat['id']]);
        if($leadModel && $leadModel->category_id != NULL){
          $catModel = UserCategory::findOne($leadModel->category_id);
          if($catModel){
            $leads[$key]['category'] = ['id' => $catModel->id, 'name' => $catModel->category_name];
          }
        }
      }
      // category part

      // filter leads for particular outlet //
      if(Yii::$app->user->identity->role == User::ROLE_OUTLET){
        if(!$leadModel || $leadModel->outlet_id != Yii::$app->user->identity->id || $leadModel->outlet_id == 0){
          unset($leads[$key]);
          continue;
        }
      }
      // filter leads for particular outlet //

      //filter outlet from url get param
        if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE){
          if(isset($_GET['outlet']) && $_GET['outlet'] != ''){
            //$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
            if($leadModel && $leadModel->outlet_id != NULL){
              if($_GET['outlet'] != $leadModel->outlet_id){
                unset($leads[$key]);
                continue;
              }
            }else{
              unset($leads[$key]);
              continue;
            }
          }
        }
        //filter outlet from url get param

        //filter status from url get param
        if(isset($_GET['status']) && $_GET['status'] != ''){
          //$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
          if($leadModel && $leadModel->status != NULL){
            if($_GET['status'] != $leadModel->status){
              unset($leads[$key]);
              continue;
            }
          }else{
            unset($leads[$key]);
            continue;
          }
        }
        //filter status from url get param


      // name and email part
        if($leadModel){
            $leads[$key]['name'] = $leadModel->visitor_name == NULL ? '' : $leadModel->visitor_name;
            $leads[$key]['email'] = $leadModel->visitor_email == NULL ? '' : $leadModel->visitor_email;

            if(!$chat['showLead']){
              $leads[$key]['name'] = Leads::obfuscate_name($leads[$key]['name']);
              $leads[$key]['email'] = Leads::obfuscate_email($leads[$key]['email']);
            }
        }else{
          if(!$chat['showLead']){
            $leads[$key]['name'] = Leads::obfuscate_name($leads[$key]['name']);
            $leads[$key]['email'] = Leads::obfuscate_email($leads[$key]['email']);
          }
        }
        // name and email part

    }
    // added by Ravindra
    //echo "<pre>"; print_r($leads); exit;

    $rnd = rand();
    $filename = "lead$rnd.csv";
    foreach($leads as  $key => $chat){
      $line = [];
      $allModels[$key]['Name'] = $chat['name'];
      $allModels[$key]['Email'] = $chat['email'];
      $allModels[$key]['City'] = $chat['city'];
      $allModels[$key]['State'] = $chat['region'];
      $allModels[$key]['Country'] = $chat['country'];
      $allModels[$key]['Date'] = date('d-m-Y', $chat['started_timestamp']);

      $satus = Leads::getStatus($chat['id']);
      if($satus=='pending'){
        $allModels[$key]['Action Status'] = 'Pending';
      }elseif($satus=='approved'){
        $allModels[$key]['Action Status'] = 'Approved';
      }else{
        $allModels[$key]['Action Status'] = 'Rejected';
      }

      $line['Name'] = $chat['name'];
      $line['Email'] = $chat['email'];
      $line['City'] = $chat['city'];
      $line['Category'] = isset($chat['category']) ? $chat['category']['name'] : 'Not Set';
      $line['State'] = $chat['region'];
      $line['Country'] = $chat['country'];
      $line['Date'] = date('d-m-Y', $chat['started_timestamp']);

      $satus = Leads::getStatus($chat['id']);
      if($satus=='pending'){
        $line['Action Status'] = 'Pending';
      }elseif($satus=='approved'){
        $line['Action Status'] = 'Approved';
      }else{
        $line['Action Status'] = 'Rejected';
      }

      $file = fopen($filename,"a");
      fputcsv($file, $line);
    }


    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=$filename");
    header("Content-Type: application/csv");
    header("Content-Transfer-Encoding: binary");

    // read the file from disk
    readfile($filename);
     //echo "ssdf";exit;
  }

  public function actionExportServiceChat($filter = '', $date_from = '', $date_to = '', $group_by = '', $page = '', $category = '') {
    $page       = 1;
    $totalpages   = 0;
    $freeLeads    = 5;
    $filter     = "";
    $date_from    = "";
    $date_to    = "";
    $group_by     = "";
    $category = '';
    $packageInfo  = [];
    $leads      = [];
    $this->layout   = "kosmleadsandchats";
    $user_id    = Yii::$app->user->identity->id;

    $invoiceData = Invoices::find()->where(['user_id' => $user_id])->all();
    if( !empty( $invoiceData ) ){
      foreach( $invoiceData as $key => $invoice){
        if($invoice['package_id'] > 0){

          $package_id  = $invoice['package_id'];
          $packageData = Packages::findOne( $package_id );

          if( !empty( $packageData ) ){
            $packageInfo[$invoice['payment_date']] = $packageData['number_of_leads'];
            Yii::$app->session[$invoice['payment_date']] = 0;
          }
        }
      }
    }

        try {
      $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
      if(!isset($_GET["date_from"])){
        $_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
        $_GET['date_to'] = date('Y-m-d');
      }else{
        $filter = $_GET['filter'];
        $date_from = $_GET['date_from'];
        $date_to = $_GET['date_to'];
        $group_by = $_GET['group_by'];
        $page = isset($_GET['page']) ? $_GET['page'] : 1;
        $category = isset($_GET['category']) ? $_GET['category'] : '';
      }
      //$_GET['tag%5B%5D'] = 'lead';
      $_GET["group"] = Yii::$app->user->identity->user_group;
      $API = new LiveChat_API();
      $chats = $API->chats->get($page,$_GET);
    }
    catch (Exception $e) {
      throw new NotFoundHttpException($e->getMessage());
    }

    $transcripts = $chats->chats;
    $total = $chats->total;
    $pages = $chats->pages;

    $leads = Leads::filterLeadPageData( $transcripts, $leads );

    for($page = 2; $page <= $pages; $page++){
      $API      = new LiveChat_API();
      $_GET['page'] = $page;
      $chats      = $API->chats->get($page,$_GET);
      $transcripts  = $chats->chats;
      $leads      = Leads::filterLeadPageData( $transcripts, $leads );
    }

    $leads = array_reverse( $leads );

    //$leads = Leads::getAllLeads($leads, $packageInfo);
    $leads = Leads::getAllLeadsData($leads, $packageInfo);

    $leads = array_reverse( $leads );

    // added by Ravindra
    foreach ($leads as $key => $chat) {
      //echo "<pre>"; print_r($chat); exit;
      //category part
      if(isset($_GET['category']) && $_GET['category'] != ''){
        $leadModel = Leads::findOne(['chat_id' => $chat['id']]);

        if($leadModel && $leadModel->category_id != NULL){
          if($_GET['category'] == $leadModel->category_id){
            $catModel = UserCategory::findOne($leadModel->category_id);
            if($catModel){
              $leads[$key]['category'] = ['id' => $catModel->id, 'name' => $catModel->category_name];
            }
          }else{
            unset($leads[$key]);
            continue;
          }
        }else{
          unset($leads[$key]);
          continue;
        }
      }else{
        $leadModel = Leads::findOne(['chat_id' => $chat['id']]);
        if($leadModel && $leadModel->category_id != NULL){
          $catModel = UserCategory::findOne($leadModel->category_id);
          if($catModel){
            $leads[$key]['category'] = ['id' => $catModel->id, 'name' => $catModel->category_name];
          }
        }
      }
      if($leadModel){
        if($leadModel->is_service_chat==0){
          unset($leads[$key]);
          continue;
        }
      }else{
        unset($leads[$key]);
        continue;
      }
      // category part

      // filter leads for particular outlet //
      if(Yii::$app->user->identity->role == User::ROLE_OUTLET){
        if(!$leadModel || $leadModel->outlet_id != Yii::$app->user->identity->id || $leadModel->outlet_id == 0){
          unset($leads[$key]);
          continue;
        }
      }
      // filter leads for particular outlet //

      //filter outlet from url get param
        if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE){
          if(isset($_GET['outlet']) && $_GET['outlet'] != ''){
            //$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
            if($leadModel && $leadModel->outlet_id != NULL){
              if($_GET['outlet'] != $leadModel->outlet_id){
                unset($leads[$key]);
                continue;
              }
            }else{
              unset($leads[$key]);
              continue;
            }
          }
        }
        //filter outlet from url get param

        //filter status from url get param
        if(isset($_GET['status']) && $_GET['status'] != ''){
          //$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
          if($leadModel && $leadModel->status != NULL){
            if($_GET['status'] != $leadModel->status){
              unset($leads[$key]);
              continue;
            }
          }else{
            unset($leads[$key]);
            continue;
          }
        }
        //filter status from url get param


      // name and email part
        if($leadModel){
            $leads[$key]['name'] = $leadModel->visitor_name == NULL ? '' : $leadModel->visitor_name;
            $leads[$key]['email'] = $leadModel->visitor_email == NULL ? '' : $leadModel->visitor_email;

            if(!$chat['showLead']){
              $leads[$key]['name'] = Leads::obfuscate_name($leads[$key]['name']);
              $leads[$key]['email'] = Leads::obfuscate_email($leads[$key]['email']);
            }
        }else{
          if(!$chat['showLead']){
            $leads[$key]['name'] = Leads::obfuscate_name($leads[$key]['name']);
            $leads[$key]['email'] = Leads::obfuscate_email($leads[$key]['email']);
          }
        }
        // name and email part

    }
    //echo '<pre>'; print_r($leadModel);exit;
    // added by Ravindra
    //echo "<pre>"; print_r($leads); exit;

    $rnd = rand();
    $filename = "servicechat$rnd.csv";
    foreach($leads as  $key => $chat){
      $line = [];
      $allModels[$key]['Name'] = $chat['name'];
      $allModels[$key]['Email'] = $chat['email'];
      $allModels[$key]['City'] = $chat['city'];
      $allModels[$key]['State'] = $chat['region'];
      $allModels[$key]['Country'] = $chat['country'];
      $allModels[$key]['Date'] = date('d-m-Y', $chat['started_timestamp']);

      $satus = Leads::getStatus($chat['id']);
      if($satus=='pending'){
        $allModels[$key]['Action Status'] = 'Pending';
      }elseif($satus=='approved'){
        $allModels[$key]['Action Status'] = 'Approved';
      }else{
        $allModels[$key]['Action Status'] = 'Rejected';
      }

      $line['Name'] = $chat['name'];
      $line['Email'] = $chat['email'];
      $line['City'] = $chat['city'];
      $line['Category'] = isset($chat['category']) ? $chat['category']['name'] : 'Not Set';
      $line['State'] = $chat['region'];
      $line['Country'] = $chat['country'];
      $line['Date'] = date('d-m-Y', $chat['started_timestamp']);

      $satus = Leads::getStatus($chat['id']);
      if($satus=='pending'){
        $line['Action Status'] = 'Pending';
      }elseif($satus=='approved'){
        $line['Action Status'] = 'Approved';
      }else{
        $line['Action Status'] = 'Rejected';
      }

      $file = fopen($filename,"a");
      fputcsv($file, $line);
    }


    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=$filename");
    header("Content-Type: application/csv");
    header("Content-Transfer-Encoding: binary");

    // read the file from disk
    readfile($filename);
     //echo "ssdf";exit;
  }


  function obfuscate_email($email)
  {
    $em   = explode("@",$email);
    $name = implode(array_slice($em, 0, count($em)-1), '@');
    $len  = floor(strlen($name)/2);

    return substr($name,0, $len) . str_repeat('*', $len) . "@" . end($em);
  }

  function obfuscate_name($name)
  {

    $len  = floor(strlen($name)/2);

    return substr($name,0, $len) . str_repeat('*', $len);
  }

  public function actionTest(){
    //return $this->render('test');
    $API = new LiveChat_API();
    $chat2 = $API->chats->getSingleChat("PECLGSO94T");
    echo "<pre>";print_r($chat2);exit;
  }

  public function actionOldapi()
  {
    $group = 0;
    $name = '';
      $email = '';
      //mail("iamparam.surya@gmail.com","API subject", json_encode($_REQUEST));
      $userData = User::findByEmail($_REQUEST['username']);

      if( !empty( $userData ) ){
          if($userData->validatePassword($_REQUEST['password'])){
              $group = $userData['user_group'];
          }else{
              echo "Invalid User";exit;
          }
      }else{
          echo "Invalid User";exit;
      }
      if($group > 0){
          try {
          $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
          if(!isset($_GET["date_from"])){
            $_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
            $_GET['date_to'] = date('Y-m-d');
          }
          $_GET['tag%5B%5D'] = 'lead';
          $_GET["group"] = $group;
          $API = new LiveChat_API();
          $chats = $API->chats->get($page,$_GET);
        }
        catch (Exception $e) {
          throw new NotFoundHttpException($e->getMessage());
        }


        $transcripts = $chats->chats;

        $data = [];
        $extra_info = NULL;

        foreach($transcripts as $row => $transcript){
            $leadModel = Leads::find()->where(['chat_id' => $transcript->id ])->one();
            foreach($transcript as $key => $value){
                if($key == 'visitor' || $key == 'ended' || $key == 'prechat_survey' || $key == 'chat_start_url' || $key == 'messages' || $key == 'referrer' || $key == 'duration' || $key == 'started' || $key == 'timezone'){
                    $final_value = [];
                    if($key == 'messages'){
                        foreach($value as $k1 => $messages){
                            foreach($messages as $msg_key => $message){
                              $messageData = $message;
                              $namePos = stripos($messageData,"name:");
                              if( $namePos > 0 ){
                                $nameStartPos = $namePos + 5;
                                $emailPos = stripos($messageData,"email:");
                                $nameLen = $emailPos - $nameStartPos;
                                $emailStartPos = $emailPos + 6;
                                $phonePos = stripos($messageData,"phone:");
                                $emailLen = $phonePos - $emailStartPos;
                                $name = substr($messageData, $nameStartPos, $nameLen);
                                $email = substr($messageData, $emailStartPos, $emailLen);
                                if(!isset($transcript->visitor->email)){
                                  $data[0]['visitor']['name'] = trim($name);
                                  $data[0]['visitor']['email'] = trim($email);
                                }
                              }
                                if($msg_key == 'text' || $msg_key == 'date'){
                                    $final_value[$k1][$msg_key] = $message;
                                }
                            }
                        }
                    }else if($key == 'prechat_survey'){
                        foreach($value as $k1 => $prechat_survey){
                            foreach($prechat_survey as $msg_key => $message){
                                if($msg_key == 'value'){
                                    $final_value[$k1][$msg_key] = $message;
                                }
                            }
                        }
                    }else if($key == 'visitor'){

                        foreach($value as $msg_key => $message){
                          if($msg_key != 'id' && $msg_key != 'ip'){
                              $final_value[$msg_key] = $message;
                          }
                        }
                        if(isset($leadModel)){
                          $extra_info['visitor_first_name'] = $leadModel->visitor_name;
                          $extra_info['visitor_last_name'] = $leadModel->visitor_last_name;
                          $extra_info['visitor_email'] = $leadModel->visitor_email;
                          $extra_info['visitor_phone'] = $leadModel->visitor_phone;
                          //$extra_info['category'] = $leadModel->category_id;
                          $userCat = UserCategory::findOne($leadModel->category_id);
                          $extra_info['category'] = isset($userCat->category_name) ? $userCat->category_name : 'Not Set';
                          //$extra_info['outlet'] = $leadModel->outlet_id;
                          //$extra_info['tags'] = $leadModel->tags;
                          $extra_info['chat_summary'] = $leadModel->chat_summary;
                          $final_value['extra_info'] = $extra_info;
                        }else{
                          $extra_info['visitor_first_name'] = 'Not Set';
                          $extra_info['visitor_last_name'] = 'Not Set';
                          $extra_info['visitor_email'] = 'Not Set';
                          $extra_info['visitor_phone'] = 'Not Set';
                          $extra_info['category'] = 'Not Set';
                          //$extra_info['outlet'] = 'Not Set';
                          //$extra_info['tags'] = 'Not Set';
                          $extra_info['chat_summary'] = 'Not Set';
                          $final_value['extra_info'] = $extra_info;
                        }

                    }else{
                        $final_value = $value;
                    }
                    $data[$row][$key] = $final_value;
                }
            }
        break;
        }
        echo json_encode($data);

      }
  }

  public function actionApi()
  {
    $group = 0;
    $name = '';
    $email = '';
    //mail("iamparam.surya@gmail.com","API subject", json_encode($_REQUEST));
    $userData = User::findByEmail($_REQUEST['username']);

    if( !empty( $userData ) ){
      if($userData->validatePassword($_REQUEST['password'])){
        $group = $userData['user_group'];
      }else{
        echo "Invalid User";exit;
      }
    }else{
      echo "Invalid User";exit;
    }

    if($group > 0){
      try {
        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        if(!isset($_GET["date_from"])){
          $_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
          $_GET['date_to'] = date('Y-m-d');
        }
        $_GET['tag%5B%5D'] = 'lead';
        $_GET["group"] = $group;
        $API = new LiveChat_API();
        $chats = $API->chats->get($page,$_GET);
      }
      catch (Exception $e) {
        throw new NotFoundHttpException($e->getMessage());
      }


      $transcripts = $chats->chats;

      $data = [];
      $extra_info = NULL;

      foreach($transcripts as $row => $transcript){
        $leadModel = Leads::find()->where(['chat_id' => $transcript->id ])->one();

        if($leadModel && $leadModel->from_agent_app == 0){
          continue;
        }

        foreach($transcript as $key => $value){
          if($key == 'visitor' || $key == 'ended' || $key == 'prechat_survey' || $key == 'chat_start_url' || $key == 'messages' || $key == 'referrer' || $key == 'duration' || $key == 'started' || $key == 'timezone'){
              $final_value = [];
              if($key == 'messages'){
                foreach($value as $k1 => $messages){
                  foreach($messages as $msg_key => $message){
                    $messageData = $message;
                    $namePos = stripos($messageData,"name:");
                    if( $namePos > 0 ){
                      $nameStartPos = $namePos + 5;
                      $emailPos = stripos($messageData,"email:");
                      $nameLen = $emailPos - $nameStartPos;
                      $emailStartPos = $emailPos + 6;
                      $phonePos = stripos($messageData,"phone:");
                      $emailLen = $phonePos - $emailStartPos;
                      $name = substr($messageData, $nameStartPos, $nameLen);
                      $email = substr($messageData, $emailStartPos, $emailLen);
                      if(!isset($transcript->visitor->email)){
                        $data[0]['visitor']['name'] = trim($name);
                        $data[0]['visitor']['email'] = trim($email);
                      }
                    }
                    if($msg_key == 'text' || $msg_key == 'date'){
                      $final_value[$k1][$msg_key] = $message;
                    }
                  }
                }
              }else if($key == 'prechat_survey'){
                foreach($value as $k1 => $prechat_survey){
                  foreach($prechat_survey as $msg_key => $message){
                    if($msg_key == 'value'){
                        $final_value[$k1][$msg_key] = $message;
                    }
                  }
                }
              }else if($key == 'visitor'){

                foreach($value as $msg_key => $message){
                  if($msg_key != 'id' && $msg_key != 'ip'){
                      $final_value[$msg_key] = $message;
                  }
                }

                if(isset($leadModel)){
                  $extra_info['visitor_first_name'] = $leadModel->visitor_name;
                  $extra_info['visitor_last_name'] = $leadModel->visitor_last_name;
                  $extra_info['visitor_email'] = $leadModel->visitor_email;
                  $extra_info['visitor_phone'] = $leadModel->visitor_phone;
                  //$extra_info['category'] = $leadModel->category_id;
                  $userCat = UserCategory::findOne($leadModel->category_id);
                  $extra_info['category'] = isset($userCat->category_name) ? $userCat->category_name : 'Not Set';
                  //$extra_info['outlet'] = $leadModel->outlet_id;
                  //$extra_info['tags'] = $leadModel->tags;
                  $extra_info['chat_summary'] = $leadModel->chat_summary;
                  $final_value['extra_info'] = $extra_info;
                }else{
                  $extra_info['visitor_first_name'] = 'Not Set';
                  $extra_info['visitor_last_name'] = 'Not Set';
                  $extra_info['visitor_email'] = 'Not Set';
                  $extra_info['visitor_phone'] = 'Not Set';
                  $extra_info['category'] = 'Not Set';
                  //$extra_info['outlet'] = 'Not Set';
                  //$extra_info['tags'] = 'Not Set';
                  $extra_info['chat_summary'] = 'Not Set';
                  $final_value['extra_info'] = $extra_info;
                }

              }else{
                $final_value = $value;
              }
              $data[$row][$key] = $final_value;
            }
          }
        break;
        }
        echo json_encode($data);
      }
  }

  public function actionChangeMultipleLeadsStatus(){
      $chat_ids = Yii::$app->request->post('chat_ids');
      $bulk_status = Yii::$app->request->post('bulk_status');
      foreach($chat_ids as $key => $chat_id){
      $leads =  Leads::findOne(['chat_id' => $chat_id]);
      if($leads){
        $leads->status = $bulk_status;
        $leads->save();
      }
      }
      return json_encode(['success' => true]);
    }

  public function actionApi1(){
    //mail("iamparam.surya@gmail.com","API REST", json_encode($_REQUEST));
      mail("serversupport@oxnia.com","API REST", json_encode($_REQUEST));
      //echo json_encode(['group_id' => 3]);
  }

  public function actionApigetleads()
  {

    try {
            //mail("iamparam.surya@gmail.com","My subject",json_encode($_REQUEST));
    }
    catch (Exception $e) {
      throw new NotFoundHttpException($e->getMessage());
    }


  }

  public function actionServiceChat($filter = '', $date_from = '', $date_to = '', $group_by = '', $page = '', $category = '')
    {
      //print_r($_GET); exit;
      $page       = 1;
      $totalpages   = 0;
      $freeLeads    = 5;
      $filter     = "";
      $date_from    = "";
      $date_to    = "";
      $group_by     = "";
      $category = '';
      $packageInfo  = [];
      $leads      = [];
      $this->layout   = "kosmleadsandchats";
      $user_id    = Yii::$app->user->identity->id;

      $invoiceData = Invoices::find()->where(['user_id' => $user_id])->all();
      // if( !empty( $invoiceData ) ){
      //  foreach( $invoiceData as $key => $invoice){
      //    if($invoice['package_id'] > 0){
      //
      //      $package_id  = $invoice['package_id'];
      //      $packageData = Packages::findOne( $package_id );
      //
      //      if( !empty( $packageData ) ){
      //        $packageInfo[$invoice['payment_date']] = $packageData['number_of_leads'];
      //        Yii::$app->session[$invoice['payment_date']] = 0;
      //      }
      //    }
      //  }
      // }

      // code modification by Ravindra //
      $payment_date = '';
      $number_of_leads = '';
      $pack_type = '';
      if( !empty( $invoiceData ) ){
        foreach( $invoiceData as $key => $invoice){
          if($invoice['package_id'] > 0){
            $package_id = $invoice['package_id'];
            $packageData = Packages::findOne( $package_id );
            if( !empty( $packageData ) ){
              if($packageData->package_name == 'Custom' && ($pack_type == '' || $pack_type == 'Gold' || $pack_type == 'Startup')){
                $packageUser = PackageUser::findOne(['user_id' => Yii::$app->user->identity->id]);
                if($packageUser){
                    $payment_date = $invoice->payment_date;
                    $number_of_leads = $packageUser->number_of_leads;
                    $pack_type = 'Custom';
                }
              }elseif ($packageData->package_name == 'Gold' && ($pack_type == '' || $pack_type == 'Startup')) {
                $payment_date = $invoice->payment_date;
                $number_of_leads = $packageData->number_of_leads;
                $pack_type = 'Gold';
              }elseif ($packageData->package_name == 'Startup' && $pack_type == '') {
                $payment_date = $invoice->payment_date;
                $number_of_leads = $packageData->number_of_leads;
                $pack_type = 'Startup';
              }
            }
          }
        }
      }

      if($pack_type != ''){
        $packageInfo[$payment_date] = $number_of_leads;
      }
      // code modification by Ravindra

      //echo "<pre>"; print_r($packageInfo); exit;

          try {
        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        if(!isset($_GET["date_from"])){
          $_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
          $_GET['date_to'] = date('Y-m-d');
        }else{
          $filter = $_GET['filter'];
          $date_from = $_GET['date_from'];
          $date_to = $_GET['date_to'];
          $group_by = $_GET['group_by'];
          $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
          $category = isset($_GET['category']) ? $_GET['category'] : '';
        }
        //$_GET['tag%5B%5D'] = 'lead';
        $_GET["group"] = Yii::$app->user->identity->user_group;
        $API = new LiveChat_API();
        //echo "GET data: <pre>"; print_r($_GET); exit;
        $chats = $API->chats->get($page,$_GET);
      }
      catch (Exception $e) {
        throw new NotFoundHttpException($e->getMessage());
      }

      $transcripts = $chats->chats;
      $total = $chats->total;
      $pages = $chats->pages;

      //echo "<pre>"; print_r($transcripts); exit;
      //$chats = $API->chats->get('',[]);
      //echo "<pre>"; print_r($chats); exit;

      $leads = Leads::filterLeadPageData( $transcripts, $leads );

      for($page = 2; $page <= $pages; $page++){
        $API      = new LiveChat_API();
        $_GET['page'] = $page;
        $chats      = $API->chats->get($page,$_GET);
        $transcripts  = $chats->chats;
        $leads      = Leads::filterLeadPageData( $transcripts, $leads );
      }


      $leads = array_reverse( $leads );

      //$leads = Leads::getAllLeads($leads, $packageInfo);
      $leads = Leads::getAllLeadsData($leads, $packageInfo);
      // error here //
      //echo "<pre>"; print_r($leads); exit;

      $leads = array_reverse( $leads );

      // added by Ravindra

      //echo 'Count: ' . count($leads); exit;
    //echo "<pre>"; print_r($leads); exit;
      foreach ($leads as $key => $chat) {
        // mark lead as seen functionality
        $leadModel = Leads::findOne(['chat_id' => $chat['id'], 'new_lead' => 1]);
        if($leadModel){
          if($leadModel->is_service_chat==0){
            unset($leads[$key]);
            continue;
          }
          //echo "<pre>"; print_r($leadModel); exit;
          // $leadSeen = LeadSeen::findOne(['user_id' => Yii::$app->user->identity->id, 'leads_id' => $leadModel->id]);
          // if(!$leadSeen){
          //   $leadSeen = new LeadSeen();
          //   $leadSeen->user_id = Yii::$app->user->identity->id;
          //   $leadSeen->leads_id = $leadModel->id;
          //   $leadSeen->save();
          // }
        }else{
          unset($leads[$key]);
          continue;
        }

        // mark lead as seen functionality

        $leadModel = Leads::findOne(['chat_id' => $chat['id']]);
        //echo "<pre>"; print_r($leadModel); exit;
        // filter leads for particular outlet //
        if(Yii::$app->user->identity->role == User::ROLE_OUTLET){
          if(!$leadModel || $leadModel->outlet_id != Yii::$app->user->identity->id || $leadModel->outlet_id == 0){
            unset($leads[$key]);
            continue;
          }
        }
        // filter leads for particular outlet //

        //category part
        if(isset($_GET['category']) && $_GET['category'] != ''){
          //$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
          if($leadModel && $leadModel->category_id != NULL){
            if($_GET['category'] == $leadModel->category_id){
              $catModel = UserCategory::findOne($leadModel->category_id);
              if($catModel){
                $leads[$key]['category'] = ['id' => $catModel->id, 'name' => $catModel->category_name];
              }
            }else{
              unset($leads[$key]);
              continue;
            }
          }else{
            unset($leads[$key]);
            continue;
          }
        }else{
          $leadModel = Leads::findOne(['chat_id' => $chat['id']]);
          if($leadModel && $leadModel->category_id != NULL){
            $catModel = UserCategory::findOne($leadModel->category_id);
            if($catModel){
              $leads[$key]['category'] = ['id' => $catModel->id, 'name' => $catModel->category_name];
            }
          }
        }
        //category part

        //filter outlet from url get param
        if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE){
          if(isset($_GET['outlet']) && $_GET['outlet'] != ''){
            //$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
            if($leadModel && $leadModel->outlet_id != NULL){
              if($_GET['outlet'] != $leadModel->outlet_id){
                unset($leads[$key]);
                continue;
              }
            }else{
              unset($leads[$key]);
              continue;
            }
          }
        }
        //filter outlet from url get param

        //filter status from url get param
        if(isset($_GET['status']) && $_GET['status'] != ''){
          //$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
          if($leadModel && $leadModel->status != NULL){
            if($_GET['status'] != $leadModel->status){
              unset($leads[$key]);
              continue;
            }
          }else{
            unset($leads[$key]);
            continue;
          }
        }
        //filter status from url get param

        // name and email part
        /*if($chat['name'] == '' || $chat['email'] == ''){
          if($leadModel){
            if($chat['name'] == ''){
              $leads[$key]['name'] = $leadModel->visitor_name == NULL ? '' : $leadModel->visitor_name;
              if($leads[$key]['showLead'] != 1){
                $leads[$key]['name'] = Leads::obfuscate_name($leads[$key]['name']);
              }
            }
            if($chat['email'] == ''){
              $leads[$key]['email'] = $leadModel->visitor_email == NULL ? '' : $leadModel->visitor_email;
              if($leads[$key]['showLead'] != 1){
                $leads[$key]['email'] = Leads::obfuscate_email($leads[$key]['email']);
              }
            }
          }
        }*/
        // name and email part
      }

      //$outlets = [];

      if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE || Yii::$app->user->identity->role == User::ROLE_OUTLET){
        //$outlets = User::findAll(['role' => User::ROLE_FRANCHISE, 'parent_id' => Yii::$app->user->identity->id]);
        foreach ($leads as $key => $chat) {
          $leadModel = Leads::findOne(['chat_id' => $chat['id']]);
          if($leadModel && $leadModel->outlet_id > 0){
            if(isset($leadModel->outlet_id) && isset($leadModel->outlet->name))
              $leads[$key]['outlet'] = ['id' => $leadModel->outlet_id, 'name' => $leadModel->outlet->name];
          }
        }
      }

      //echo '<pre>'; print_r($leads); exit;

      $categories = UserCategory::findAll(['user_id' => Yii::$app->user->id]);
      $franchiseOutlets = [];
      if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE){
        $franchiseOutlets = User::findAll(['role' => User::ROLE_OUTLET, 'parent_id' => Yii::$app->user->identity->id]);
      }

      $forwardingEmailTeams = ForwardingEmailTeam::findAll(['user_id' => Yii::$app->user->identity->id]);

      return $this->render('service-chat', [
        'transcripts' => $leads,
        'total' => $total,
        'pages' => $pages,
        'userdata'=>Yii::$app->user->identity,
        'filter' => $filter,
        'date_from' => $date_from,
        'date_to' => $date_to,
        'group_by' => $group_by,
        'category' => $category,
        'totalpages' => $totalpages,
        'page' => $page,
        'categories' => $categories,
        'franchiseOutlets' => $franchiseOutlets,
        'forwardingEmailTeams' => $forwardingEmailTeams,
      ]);
    }

    /**
     * Lists all Leads models.
     * @return mixed
     */
    public function actionIndex($filter = '', $date_from = '', $date_to = '', $group_by = '', $page = '', $category = '')
    {
        //print_r($_GET); exit;
        $page       = 1;
        $totalpages   = 0;
        $freeLeads    = 5;
        $filter     = "";
        $date_from    = "";
        $date_to    = "";
        $group_by     = "";
        $category = '';
        $packageInfo  = [];
        $leads      = [];
        $this->layout   = "kosmleadsandchats";
        $user_id    = Yii::$app->user->identity->id;

        $invoiceData = Invoices::find()->where(['user_id' => $user_id])->all();
        // if( !empty( $invoiceData ) ){
        //  foreach( $invoiceData as $key => $invoice){
        //    if($invoice['package_id'] > 0){
        //
        //      $package_id  = $invoice['package_id'];
        //      $packageData = Packages::findOne( $package_id );
        //
        //      if( !empty( $packageData ) ){
        //        $packageInfo[$invoice['payment_date']] = $packageData['number_of_leads'];
        //        Yii::$app->session[$invoice['payment_date']] = 0;
        //      }
        //    }
        //  }
        // }

        // code modification by Ravindra //
        $payment_date = '';
        $number_of_leads = '';
        $pack_type = '';
        if( !empty( $invoiceData ) ){
          foreach( $invoiceData as $key => $invoice){
            if($invoice['package_id'] > 0){
              $package_id = $invoice['package_id'];
              $packageData = Packages::findOne( $package_id );
              if( !empty( $packageData ) ){
                if($packageData->package_name == 'Custom' && ($pack_type == '' || $pack_type == 'Gold' || $pack_type == 'Startup')){
                  $packageUser = PackageUser::findOne(['user_id' => Yii::$app->user->identity->id]);
                  if($packageUser){
                      $payment_date = $invoice->payment_date;
                      $number_of_leads = $packageUser->number_of_leads;
                      $pack_type = 'Custom';
                  }
                }elseif ($packageData->package_name == 'Gold' && ($pack_type == '' || $pack_type == 'Startup')) {
                  $payment_date = $invoice->payment_date;
                  $number_of_leads = $packageData->number_of_leads;
                  $pack_type = 'Gold';
                }elseif ($packageData->package_name == 'Startup' && $pack_type == '') {
                  $payment_date = $invoice->payment_date;
                  $number_of_leads = $packageData->number_of_leads;
                  $pack_type = 'Startup';
                }
              }
            }
          }
        }

        if($pack_type != ''){
          $packageInfo[$payment_date] = $number_of_leads;
        }
        // code modification by Ravindra

        //echo "<pre>"; print_r($packageInfo); exit;

            try {
          $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
          if(!isset($_GET["date_from"])){
            $_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
            $_GET['date_to'] = date('Y-m-d');
          }else{
            $filter = $_GET['filter'];
            $date_from = $_GET['date_from'];
            $date_to = $_GET['date_to'];
            $group_by = $_GET['group_by'];
            $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
            $category = isset($_GET['category']) ? $_GET['category'] : '';
          }
          $_GET['tag%5B%5D'] = 'lead';
          $_GET["group"] = Yii::$app->user->identity->user_group;
          $API = new LiveChat_API();
          //echo "GET data: <pre>"; print_r($_GET); exit;
          $chats = $API->chats->get($page,$_GET);
        }
        catch (Exception $e) {
          throw new NotFoundHttpException($e->getMessage());
        }

        $transcripts = $chats->chats;
        $total = $chats->total;
        $pages = $chats->pages;

        //echo "<pre>"; print_r($transcripts); exit;
        //$chats = $API->chats->get('',[]);
        //echo "<pre>"; print_r($chats); exit;

        $leads = Leads::filterLeadPageData( $transcripts, $leads );

        for($page = 2; $page <= $pages; $page++){
          $API      = new LiveChat_API();
          $_GET['page'] = $page;
          $chats      = $API->chats->get($page,$_GET);
          $transcripts  = $chats->chats;
          $leads      = Leads::filterLeadPageData( $transcripts, $leads );
        }


        $leads = array_reverse( $leads );

        //$leads = Leads::getAllLeads($leads, $packageInfo);
        $leads = Leads::getAllLeadsData($leads, $packageInfo);
        // error here //
        //echo "<pre>"; print_r($leads); exit;

        $leads = array_reverse( $leads );

        // added by Ravindra

        //echo 'Count: ' . count($leads); exit;

        foreach ($leads as $key => $chat) {
          // mark lead as seen functionality
          $leadModel = Leads::findOne(['chat_id' => $chat['id'], 'new_lead' => 1]);
          if($leadModel){
            $leadSeen = LeadSeen::findOne(['user_id' => Yii::$app->user->identity->id, 'leads_id' => $leadModel->id]);
            if(!$leadSeen){
              $leadSeen = new LeadSeen();
              $leadSeen->user_id = Yii::$app->user->identity->id;
              $leadSeen->leads_id = $leadModel->id;
              $leadSeen->save();
            }
          }
          // mark lead as seen functionality

          $leadModel = Leads::findOne(['chat_id' => $chat['id']]);

          // filter leads for particular outlet //
          if(Yii::$app->user->identity->role == User::ROLE_OUTLET){
            if(!$leadModel || $leadModel->outlet_id != Yii::$app->user->identity->id || $leadModel->outlet_id == 0){
              unset($leads[$key]);
              continue;
            }
          }
          // filter leads for particular outlet //

          //category part
          if(isset($_GET['category']) && $_GET['category'] != ''){
            //$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
            if($leadModel && $leadModel->category_id != NULL){
              if($_GET['category'] == $leadModel->category_id){
                $catModel = UserCategory::findOne($leadModel->category_id);
                if($catModel){
                  $leads[$key]['category'] = ['id' => $catModel->id, 'name' => $catModel->category_name];
                }
              }else{
                unset($leads[$key]);
                continue;
              }
            }else{
              unset($leads[$key]);
              continue;
            }
          }else{
            $leadModel = Leads::findOne(['chat_id' => $chat['id']]);
            if($leadModel && $leadModel->category_id != NULL){
              $catModel = UserCategory::findOne($leadModel->category_id);
              if($catModel){
                $leads[$key]['category'] = ['id' => $catModel->id, 'name' => $catModel->category_name];
              }
            }
          }
          //category part

          //filter outlet from url get param
          if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE){
            if(isset($_GET['outlet']) && $_GET['outlet'] != ''){
              //$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
              if($leadModel && $leadModel->outlet_id != NULL){
                if($_GET['outlet'] != $leadModel->outlet_id){
                  unset($leads[$key]);
                  continue;
                }
              }else{
                unset($leads[$key]);
                continue;
              }
            }
          }
          //filter outlet from url get param

          //filter status from url get param
          if(isset($_GET['status']) && $_GET['status'] != ''){
            //$leadModel = Leads::findOne(['chat_id' => $chat['id']]);
            if($leadModel && $leadModel->status != NULL){
              if($_GET['status'] != $leadModel->status){
                unset($leads[$key]);
                continue;
              }
            }else{
              unset($leads[$key]);
              continue;
            }
          }
          //filter status from url get param

          // name and email part
          /*if($chat['name'] == '' || $chat['email'] == ''){
            if($leadModel){
              if($chat['name'] == ''){
                $leads[$key]['name'] = $leadModel->visitor_name == NULL ? '' : $leadModel->visitor_name;
                if($leads[$key]['showLead'] != 1){
                  $leads[$key]['name'] = Leads::obfuscate_name($leads[$key]['name']);
                }
              }
              if($chat['email'] == ''){
                $leads[$key]['email'] = $leadModel->visitor_email == NULL ? '' : $leadModel->visitor_email;
                if($leads[$key]['showLead'] != 1){
                  $leads[$key]['email'] = Leads::obfuscate_email($leads[$key]['email']);
                }
              }
            }
          }*/
          // name and email part
        }

        //$outlets = [];

        if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE || Yii::$app->user->identity->role == User::ROLE_OUTLET){
          //$outlets = User::findAll(['role' => User::ROLE_FRANCHISE, 'parent_id' => Yii::$app->user->identity->id]);
          foreach ($leads as $key => $chat) {
            $leadModel = Leads::findOne(['chat_id' => $chat['id']]);
            if($leadModel && $leadModel->outlet_id > 0){
              if(isset($leadModel->outlet_id) && isset($leadModel->outlet->name))
                $leads[$key]['outlet'] = ['id' => $leadModel->outlet_id, 'name' => $leadModel->outlet->name];
            }
          }
        }

        //echo '<pre>'; print_r($leads); exit;

        $categories = UserCategory::findAll(['user_id' => Yii::$app->user->id]);
        $franchiseOutlets = [];
        if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE){
          $franchiseOutlets = User::findAll(['role' => User::ROLE_OUTLET, 'parent_id' => Yii::$app->user->identity->id]);
        }

        $forwardingEmailTeams = ForwardingEmailTeam::findAll(['user_id' => Yii::$app->user->identity->id]);

        return $this->render('index2', [
          'transcripts' => $leads,
          'total' => $total,
          'pages' => $pages,
          'userdata'=>Yii::$app->user->identity,
          'filter' => $filter,
          'date_from' => $date_from,
          'date_to' => $date_to,
          'group_by' => $group_by,
          'category' => $category,
          'totalpages' => $totalpages,
          'page' => $page,
          'categories' => $categories,
          'franchiseOutlets' => $franchiseOutlets,
          'forwardingEmailTeams' => $forwardingEmailTeams,
        ]);
    }

  public function actionTotal()
  {
    $this->layout = "kosmoouter";

    $user_group = Yii::$app->user->identity->user_group;
    $user_id = Yii::$app->user->identity->id;
    if (Yii::$app->session->has('website_filter')) {
      $user_group = Yii::$app->session->get('website_filter');
      $user = User::findOne(['user_group' => $user_group]);
      $user_id = $user->id;
    } 

    $website_users = User::find()->select('user_group')->where(['email' => Yii::$app->user->identity->email])->asArray()->all(); 
    if(count($website_users) > 1 && !Yii::$app->session->has('website_filter'))
    {
      $user_group = array_column($website_users, 'user_group');         
    }

      //$query->andFilterWhere(['user_group' => $user_group]);  


    $yesterday = date('Y-m-d');
    $last7 = date('Y-m-d',strtotime("-6 days"));

    $group_by = isset($_GET['group_by']) ? $_GET['group_by'] : 'day';
    $date_from = isset($_GET['date_from']) ? $_GET['date_from'] : $last7;
    $date_to = isset($_GET['date_to']) ? $_GET['date_to'] : $yesterday;

    //filter outlet from url get param
    $outlet_id = false;
    if (Yii::$app->session->has('outlet_filter')) {
        $outlet_id = Yii::$app->session->get('outlet_filter');       
    }

    if(isset($_GET['outlet']) && $_GET['outlet'] != ''){
      $outlet_id = Yii::$app->request->get('outlet');
      Yii::$app->session->set('outlet_filter', $outlet_id);
    }

    //filter outlet from url get param

    $transcripts = Leads::getData($user_group,$user_id,false,$date_from,$date_to,false,$group_by,$outlet_id);
    //echo "<pre>"; print_r($transcripts); exit;
    $transcripts2 = Leads::getData($user_group,$user_id,false,$date_from,$date_to,'pending',$group_by,$outlet_id);
    $transcripts3 = Leads::getData($user_group,$user_id,false,$date_from,$date_to,'rejected',$group_by,$outlet_id);
    $transcripts4 = Leads::getData($user_group,$user_id,false,$date_from,$date_to,'approved',$group_by,$outlet_id);

    //var_dump( array_search("2016-05-21",$transcripts,false));
    //$leats_total = Leads::convert($transcripts,'7','day',$date_from);

    if($group_by=="day"){
      $data_str = Leads::convert($transcripts,'7','day',$date_from,$date_to);
      $data_str2 = Leads::convert($transcripts2,'7','day',$date_from,$date_to);
      $data_str3 = Leads::convert($transcripts3,'7','day',$date_from,$date_to);
      $data_str4 = Leads::convert($transcripts4,'7','day',$date_from,$date_to);
    }else{
      $data_str = Leads::convertHour($transcripts,$date_from);
      $data_str2 = Leads::convertHour($transcripts2,$date_from);
      $data_str3 = Leads::convertHour($transcripts3,$date_from);
      $data_str4 = Leads::convertHour($transcripts4,$date_from);
    }

    $total = Leads::getTotal($transcripts);
    $total2 = Leads::getTotal($transcripts2);
    $total3 = Leads::getTotal($transcripts3);
    $total4 = Leads::getTotal($transcripts4);

    /* echo '<pre>';
    print_r($data_str);
    echo '</pre>';die; */

        /* try {
      $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
      $API = new LiveChat_API();
      $_GET['tag%5B%5D'] = 'lead';
      $_GET["group"] = Yii::$app->user->identity->user_group;
      $chats = $API->chats->get($page,$_GET);
    }
    catch (Exception $e) {
      throw new NotFoundHttpException($e->getMessage());
    } */

    /* $transcripts = $chats->chats; */
    //$total = "";
    $pages = "";

    //echo '<pre>';print_r($chats);echo '</pre>';die('ghj');
    $siteOutlets = User::find()
                    ->select(['id','name'])
                    ->where(['role' => User::ROLE_OUTLET, 'parent_id' => $user_id])->all(); 
                    
    $website_users = User::find()->where(['email' => Yii::$app->user->identity->email])->all();
    if(count($website_users) > 1)
    {
      $sql = "Select id, name, website_url from `user` where `role`=". User::ROLE_OUTLET." and parent_id in (select id from `user` where id='".$website_users[0]->id."' or parent_id='".$website_users[0]->id."')";                      
      $outlets = Yii::$app->db->createCommand($sql)->queryAll();
      
      $params['website_users'] = $website_users;        
      $params['outlets'] = $outlets;             
    }


    return $this->render('total', [
            'data_str' => $data_str,
            'data_str2' => $data_str2,
            'data_str3' => $data_str3,
            'data_str4' => $data_str4,
            'total' => $total,
            'total2' => $total2,
            'total3' => $total3,
            'total4' => $total4,
            'pages' => $pages,
            'userdata'=>Yii::$app->user->identity,
            'siteOutlets' => $siteOutlets,
            'website_users' => $website_users,
            'outlets' => $outlets,              
        ]);
    }

    /**
     * Displays a single Leads model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
      if(Yii::$app->user->identity->role != USER::ROLE_ADMIN)
      {
        //check whether to show this lead or not
        $showLead = Leads::showleads($id);
        if($showLead == 0)
        {
          Yii::$app->session->setFlash('error', 'You have not bought this lead');
          return $this->redirect(['/lead-list/leads']);
        }
      }

      $this->layout = "kosmoouter";
        try {
        $API = new LiveChat_API();
        $chat = $API->chats->getSingleChat($id);
      }
      catch (Exception $e) {
        throw new NotFoundHttpException($e->getMessage());
      }

      try {
        $API = new LiveChat_API();
        $chat2 = $API->chats->getSingleChat($id);
      }
      catch (Exception $e) {
        throw new NotFoundHttpException($e->getMessage());
      }

      if(!$model = Leads::find()->where(['chat_id'=>$id])->one()){
        $model = new Leads();
        $model->user_id = Yii::$app->user->identity->id;
        $model->chat_id = $id;
        $model->status = 'pending';
        $model->user_group = Yii::$app->user->identity->user_group;
        $model->email = 0;
        $model->c_time = time();
      }

      $allCategories = UserCategory::findAll(['user_id' => Yii::$app->user->identity->id]);
      $categories = [];
      foreach ($allCategories as $cat) {
        $categories[$cat->id] = $cat->category_name . " ($$cat->value)";
      }

      return $this->render('view', [
          'chat' => $chat,
          'chat2' => $chat2,
          'model' => $model,
          'categories' => $categories
      ]);
    }

    public function actionViewChat($id){

         $this->layout = "kosmoouter";
           try {
           $API = new LiveChat_API();
           $chat = $API->chats->getSingleChat($id);
         }
         catch (Exception $e) {
           throw new NotFoundHttpException($e->getMessage());
         }

         try {
           $API = new LiveChat_API();
           $chat2 = $API->chats->getSingleChat($id);
         }
         catch (Exception $e) {
           throw new NotFoundHttpException($e->getMessage());
         }

         if(!$model = Leads::find()->where(['chat_id'=>$id])->one()){
           $model = new Leads();
           $model->user_id = Yii::$app->user->identity->id;
           $model->chat_id = $id;
           $model->status = 'pending';
           $model->user_group = Yii::$app->user->identity->user_group;
           $model->email = 0;
           $model->c_time = time();
         }

         $allCategories = UserCategory::findAll(['user_id' => Yii::$app->user->identity->id]);
         $categories = [];
         foreach ($allCategories as $cat) {
           $categories[$cat->id] = $cat->category_name . " ($$cat->value)";
         }
         return $this->render('view_chat', [
             'chat' => $chat,
             'chat2' => $chat2,
             'model' => $model,
             'categories' => $categories
         ]);
     }

    public function actionViewChatService($id){

        $this->layout = "kosmoouter";
          try {
          $API = new LiveChat_API();
          $chat = $API->chats->getSingleChat($id);
        }
        catch (Exception $e) {
          throw new NotFoundHttpException($e->getMessage());
        }

        try {
          $API = new LiveChat_API();
          $chat2 = $API->chats->getSingleChat($id);
        }
        catch (Exception $e) {
          throw new NotFoundHttpException($e->getMessage());
        }

        if(!$model = Leads::find()->where(['chat_id'=>$id])->one()){
          $model = new Leads();
          $model->user_id = Yii::$app->user->identity->id;
          $model->chat_id = $id;
          $model->status = 'pending';
          $model->user_group = Yii::$app->user->identity->user_group;
          $model->email = 0;
          $model->c_time = time();
        }

        $allCategories = UserCategory::findAll(['user_id' => Yii::$app->user->identity->id]);
        $categories = [];
        foreach ($allCategories as $cat) {
          $categories[$cat->id] = $cat->category_name . " ($$cat->value)";
        }
        return $this->render('view_service_chat', [
            'chat' => $chat,
            'chat2' => $chat2,
            'model' => $model,
            'categories' => $categories
        ]);
    }


  public function actionSurvey($id)
    {
        try {
      $API = new LiveChat_API();
      $chat = $API->chats->getSingleChat($id);
    }
    catch (Exception $e) {
      throw new NotFoundHttpException($e->getMessage());
    }
    //echo '<pre>';print_r($chat);echo '</pre>';
        return $this->render('survey', [
            'chat' => $chat,
        ]);
    }

    /**
     * Creates a new Leads model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /* public function actionCreate()
    {
        $model = new Leads();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    } */

    /**
     * Updates an existing Leads model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionUpdate($id)
    {
        $this->layout = "kosmoouter";
        //\Yii::$app->getSession()->setFlash('success', 'Lead Updated');
            if(!$model = Leads::find()->where(['chat_id'=>$id])->one()){
          $model = new Leads();
          $model->user_id = Yii::$app->user->identity->id;
          $model->chat_id = $id;
          $model->status = 'pending';
          $model->user_group = Yii::$app->user->identity->user_group;
          $model->email = 0;
          $model->c_time = time();
        }

        $chat = false;

        $allCategories = UserCategory::findAll(['user_id' => Yii::$app->user->identity->id]);
        $categories = [];
        foreach ($allCategories as $cat) {
          $categories[$cat->id] = $cat->category_name . " ($$cat->value)";
        }

        try {
          $API = new LiveChat_API();
          $chat = $API->chats->getSingleChat($id);
        }
        catch (Exception $e) {
          throw new NotFoundHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //   if($model->is_service_chat==1){
        //     \Yii::$app->getSession()->setFlash('success', 'Service Chat Updated');
        //     return $this->redirect(['service-chat']);
        //   }else{
        //     \Yii::$app->getSession()->setFlash('success', 'Lead Updated');
        //   return $this->redirect(['/lead-list/leads']);
        //   }
        return $this->redirect(Yii::$app->request->referrer);
        } else {
            //echo "<pre>"; print_r($model->getErrors()); exit;
            return $this->render('update', [
                'model' => $model,
                'chat' => $chat,
                'categories' => $categories
            ]);
        }
    }

    public function actionApproved($id)
    {
      if(!$model = Leads::find()->where(['chat_id'=>$id])->one()){
        $model = new Leads();
        $model->user_id = Yii::$app->user->identity->id;
        $model->chat_id = $id;
      }

      $model->status = 'approved';

          $model->save();

      \Yii::$app->getSession()->setFlash('success', 'Lead Updated');
      return $this->redirect(['/lead-list/leads']);
    }

    public function actionPending($id)
    {
        if(!$model = Leads::find()->where(['chat_id'=>$id])->one()){
          $model = new Leads();
          $model->user_id = Yii::$app->user->identity->id;
          $model->chat_id = $id;
        }

        $model->status = 'pending';

            $model->save();

        \Yii::$app->getSession()->setFlash('success', 'Lead Updated');
        return $this->redirect(['/lead-list/leads']);
    }

  public function actionEmailonlead(){
    //$message = "Line 1\r\nLine 2\r\nLine 3";
               // In case any of our lines are larger than 70 characters, we should use wordwrap()
               //$message = wordwrap($message, 70, "\r\n");
               // Send
              //mail('jatin.sehgal@triusmail.com', 'My Subject', $message);
      $API = new LiveChat_API();
              $sendmail = new Leads();
    $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    $_GET['tag%5B%5D'] = 'lead';
    //$_GET["group"] = Yii::$app->user->identity->user_group;
    $chats = $API->chats->get($page,$_GET);

        //echo '<pre>';print_r($chats);echo '</pre>';die("hghg");
    foreach($chats->chats as $chat){
      $users = User2::find()->where(['user_group'=>$chat->group[0]])->all();

      foreach($users as $user){
        $leads = Leads::find()->where(['user_group'=>$user['user_group'],'chat_id'=>$chat->id])->one();

        try {
          $API = new LiveChat_API();
          $chat2 = $API->chats->getSingleChat($chat->id);
        }
        catch (Exception $e) {
          throw new NotFoundHttpException($e->getMessage());
        }
        if($leads){
          $leads23 = Leads::find()->where(['user_group'=>$user['user_group'],'chat_id'=>$chat->id,'email' => 1])->one();
          if(!$leads23){
                        //echo "ok";
            $sent = $sendmail->sendEmail($user,$chat, $chat2);
            /* if($sent){
              $model = Leads::find()->where(['user_group'=>$user['user_group'],'chat_id'=>$chat->id])->one();
              $model->email = 1;
              $model->save();
            } */
          }
        }else{
          $leads = new Leads();
          $leads->user_id = "0";
          $leads->user_group = $user['user_group'];
          $leads->chat_id = $chat->id;
          $leads->c_time = $chat->started_timestamp;
          $leads->status = 'pending';
          $leads->email = 0;
          if($leads->save()){
            //echo "ok";
            $sent = $sendmail->sendEmail($user,$chat, $chat2);
            /* if($sent){
              $model = Leads::find()->where(['user_group'=>$user['user_group'],'chat_id'=>$chat->id])->one();
              //$model->email = 1;
              $model->save();
            } */
          }
        }
      }

      $leads->email = 1;
      $leads->save();
      //echo '<pre>';print_r($leads);echo '</pre>';die;
    }

    return true;
    /* echo '<pre>';print_r($chats->chats);echo '</pre>';
    die;
    $leads = Leads::find()->all();
    foreach($leads as $res){
       $users = User2::findOne($res->user_id);
        if(empty($res->email) || $res->email == '0'|| $res->email == 0){
        $sent = $sendmail->sendEmail($users,$res);
        if($sent){
          $model = Leads::find()->where(['user_id'=>$res->user_id])->one();
          $model->email = 1;
          $model->save();
        }
      }
      echo '<pre>';print_r(array($res->user_id,$res->email,$users->username));echo '</pre>';

    }
    die('ghj'); */
  }

  public function actionSendmail($id, $showLead, $name, $email){
    //echo $showLead;exit;
    //echo Url::base(true);die;
    $sendmail = new Leads();
      $API = new LiveChat_API();
    $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    $chat = $API->chats->getSingleChat($id);
    //print_r($email); exit;
    //$_GET['tag%5B%5D'] = 'lead';
    //$_GET["group"] = Yii::$app->user->identity->user_group;
    //$chats = $API->chats->get($page,$_GET);

        //echo '<pre>';print_r($chats);echo '</pre>';die("hghg");
    //foreach($chats->chats as $chat){
      $users = User2::find()->where(['user_group'=>$chat->group[0]])->all();
      //echo '<pre>';print_r($users);echo '</pre>';die;

      foreach($users as $user){

        $leads = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();

        try {
          $API = new LiveChat_API();
          $chat2 = $API->chats->getSingleChat($chat->id);
        }
        catch (Exception $e) {
          throw new NotFoundHttpException($e->getMessage());
        }

        if($leads){
          //echo '<pre>';print_r($leads);die;
          $leads23 = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
          if($leads23){
            $sent = $sendmail->sendEmail($user,$chat,$chat2, $showLead, $name, $email);
            if($sent){
              $model = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
              $model->email = 1;
              $model->save();
            }
          }
        }else{
          $lead = new Leads();
          $lead->user_id = $user['id'];
          $lead->chat_id = $chat->id;
          $lead->c_time = $chat->started_timestamp;
          $lead->status = 'pending';
          $lead->email = 0;
          $lead->user_group = $chat->group[0];
          if($lead->save()){
            $sent = $sendmail->sendEmail($user,$chat,$chat2, $showLead, $name, $email);
            if($sent){
              $model = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
              $model->email = 1;
              $model->save();
            }
          }
        }

      }

    //}

    \Yii::$app->getSession()->setFlash('success', 'Mail Sent Successfully');

    return $this->redirect(['/lead-list/leads']);
    /* echo '<pre>';print_r($chats->chats);echo '</pre>';
    die;
    $leads = Leads::find()->all();
    foreach($leads as $res){
       $users = User2::findOne($res->user_id);
        if(empty($res->email) || $res->email == '0'|| $res->email == 0){
        $sent = Leads::sendEmail($users,$res);
        if($sent){
          $model = Leads::find()->where(['user_id'=>$res->user_id])->one();
          $model->email = 1;
          $model->save();
        }
      }
      echo '<pre>';print_r(array($res->user_id,$res->email,$users->username));echo '</pre>';

    }
    die('ghj'); */
  }

  public function actionSendmailUser($id, $showLead, $name, $email){
    $sendmail = new Leads();
      $API = new LiveChat_API();
    $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    $chat = $API->chats->getSingleChat($id);
      $users = User2::find()->where(['id' => Yii::$app->user->identity->id])->all();

      foreach($users as $user){

        //$leads = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
        $leads = Leads::find()->where(['chat_id'=>$chat->id])->one();

        try {
          $API = new LiveChat_API();
          $chat2 = $API->chats->getSingleChat($chat->id);
        }
        catch (Exception $e) {
          throw new NotFoundHttpException($e->getMessage());
        }

        if($leads){
          //$leads23 = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
          $leads23 = Leads::find()->where(['chat_id'=>$chat->id])->one();
          if($leads23){
            $chat_summary = isset($leads23->chat_summary) ? $leads23->chat_summary : 'xxxx';
            $phone = isset($leads23->visitor_phone) ? $leads23->visitor_phone : 'xxxx';
            $preferred_contact_time = isset($leads23->preferred_contact_time) ? $leads23->preferred_contact_time : '';
            $sent = $sendmail->sendEmail($user,$chat,$chat2, $showLead, $name, $email, $phone, $chat_summary, $preferred_contact_time);
            //print_r('email sent'); exit;
            if($sent){
              $model = Leads::find()->where(['chat_id'=>$chat->id])->one();
              $model->email = 1;
              $model->save();
            }
          }
        }else{
          $lead = new Leads();
          $lead->user_id = $user['id'];
          $lead->chat_id = $chat->id;
          $lead->c_time = $chat->started_timestamp;
          $lead->status = 'pending';
          $lead->email = 0;
          $lead->user_group = $chat->group[0];
          if($lead->save()){
            $sent = $sendmail->sendEmail($user,$chat,$chat2, $showLead, $name, $email);
            if($sent){
              $model = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
              $model->email = 1;
              $model->save();
            }
          }
        }
      }
      \Yii::$app->getSession()->setFlash('success', 'Mail Sent Successfully');

      return $this->redirect(['/lead-list/leads']);
  }

  public function actionSendmailTest($id, $showLead, $name, $email){
    $sendmail = new Leads();
    $API = new LiveChat_API();
    $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    $chat = $API->chats->getSingleChat($id);
    $users = User2::find()->where(['id' => Yii::$app->user->identity->id])->all();

    foreach($users as $user){

      //$leads = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
      $leads = Leads::find()->where(['chat_id'=>$chat->id])->one();

      try {
        $API = new LiveChat_API();
        $chat2 = $API->chats->getSingleChat($chat->id);
      }
      catch (Exception $e) {
        throw new NotFoundHttpException($e->getMessage());
      }

      if($leads){
        //$leads23 = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
        $leads23 = Leads::find()->where(['chat_id'=>$chat->id])->one();
        $leadModel = $leads23;
        if($leads23){
          $chat_summary = isset($leads23->chat_summary) ? $leads23->chat_summary : 'xxxx';
          $phone = isset($leads23->visitor_phone) ? $leads23->visitor_phone : 'xxxx';
          $sent = $sendmail->sendEmail($user,$chat,$chat2, $showLead, $name, $email, $phone, $chat_summary);
          //$sent = Leads::sendLeadEmail($user,$chat,$chat2, $showLead, $name, $email, $leadModel);
          //print_r('email sent'); exit;
          if($sent){
            $model = Leads::find()->where(['chat_id'=>$chat->id])->one();
            $model->email = 1;
            $model->save();
          }
        }
      }else{
        $lead = new Leads();
        $lead->user_id = $user['id'];
        $lead->chat_id = $chat->id;
        $lead->c_time = $chat->started_timestamp;
        $lead->status = 'pending';
        $lead->email = 0;
        $lead->user_group = $chat->group[0];
        if($lead->save()){
          $sent = $sendmail->sendEmail($user,$chat,$chat2, $showLead, $name, $email);
          //$sent = Leads::sendLeadEmail($user,$chat,$chat2, $showLead, $name, $email);
          if($sent){
            $model = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
            $model->email = 1;
            $model->save();
          }
        }
      }
    }
    \Yii::$app->getSession()->setFlash('success', 'Mail Sent Successfully');

    return $this->redirect(['leads/index']);
  }

  public function actionRejected($id)
  {
    if(!$model = Leads::find()->where(['chat_id'=>$id])->one()){
      $model = new Leads();
      $model->user_id = Yii::$app->user->identity->id;
      $model->chat_id = $id;
    }

    $model->status = 'rejected';

        $model->save();

    \Yii::$app->getSession()->setFlash('success', 'Lead Updated');
    return $this->redirect(['/lead-list/leads']);

  }

    /**
     * Deletes an existing Leads model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    /* public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    } */

    /**
     * Finds the Leads model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Leads the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Leads::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPushNewLead()
    {
      $post = Yii::$app->request->post();
      $visitor_name = $post['visitor_name'];
      $is_service_chat = $post['is_service_chat'];
      $is_lead = $post['is_lead'];
      $visitor_last_name = $post['visitor_last_name'];
      $visitor_email = $post['visitor_email'];
      $visitor_phone = $post['visitor_phone'];
      $preferred_contact_time = $post['preferred_contact_time'];
      $category_id = $post['category_id'];
      $sub_category_id = $post['sub_category_id'];
      $outlet_id = $post['outlet_id'];
      $outlet_text = $post['outlet_text'];
      $tags = $post['tags'];
      $chat_id = $post['chat_id'];
      $chat_summary = $post['chat_summary'];
      if(isset($post['custom_filled_data'])) {
        $custom_filled_data = $post['custom_filled_data'];
      }
      $group_id = $post['group_id'];
      $created_by = $post['created_by'];
      $action = 'Updated';
      $lead = Leads::findOne(['chat_id' => $chat_id]);
      $email_sent = 1;
      if(!$lead){
        $lead = new Leads();
        $action = 'Created';
        $email_sent = 0;
      }
      //old saved tags
      $old_saved_tags = $lead->tags;

      //is_service_chat field================================//
      $lead->is_service_chat = $is_service_chat;
      $lead->is_lead = $is_lead;

      //$groupUser = User::findOne(['user_group' => $group_id]);
      $lead->user_id = 0; // hardcoded as 0

      //$lead->chat_id = $chat->id;
      $lead->chat_id = $chat_id;
      $lead->user_group = $group_id;

      //$lead->c_time = $chat->started_timestamp;
      $lead->c_time = time();
      $lead->new_lead = 1;
      $lead->from_agent_app = 1;

      //custom fields
      $lead->visitor_name = $visitor_name;
      $lead->visitor_last_name = $visitor_last_name;
      $lead->visitor_email = $visitor_email;
      $lead->visitor_phone = $visitor_phone;
      $lead->preferred_contact_time = $preferred_contact_time;
      $lead->created_by = $created_by;

      if($sub_category_id != 0){
        $lead->category_id = $sub_category_id;
      }else{
        $lead->category_id = $category_id;
      }
        if($outlet_text=='Owner'){
        $lead->outlet_id = 0;
        }else{
        $lead->outlet_id = $outlet_id;
        }
      //$lead->outlet_id = $outlet_id;
      $lead->tags = $tags;
      $lead->chat_summary = $chat_summary;

      $lead->status = 'pending';
      $lead->email = $email_sent;
      $email_count = 0;

      //save custom field data_str
      if(isset($custom_filled_data)) {
        $lead->custom_fields = json_encode($custom_filled_data);
      }

      if($lead->save())
      {
        /* Push New Lead to Customer's API */
        $customer_api_class = "\common\models\CustomerAPI".$group_id;
        if(class_exists($customer_api_class)) {
          $customer_api = new $customer_api_class();

          if($customer_api->authenticate()) {
            $response = $customer_api->pushNewLead($post);          
            //var_dump($response);
          }
        }
        /* End Customer'S API */          
          
        $chatModel = Chat::findOne(['chat_id' => $lead->chat_id]);

        if($chatModel){
          if($chatModel->leads_id == NULL){
            $chatModel->leads_id = $lead->id;
            $chatModel->is_lead = 1;
            $chatModel->save();
          }
        }

        //if($outlet_id!=0 && $outlet_id!=''){
        if($lead->outlet_id !=0 && $lead->outlet_id !=''){
            $userList = User::find()->where(['id' => $outlet_id,'status' => User::STATUS_ACTIVE])
            ->orWhere(['outlet' => $outlet_id,'status' => User::STATUS_ACTIVE])
            ->orWhere(['and', ['role' => [User::ROLE_USER, User::ROLE_FRANCHISE, User::ROLE_ASSIST], 'user_group' => $group_id,'status' => User::STATUS_ACTIVE], ['is', 'outlet', new \yii\db\Expression('null')]])->all();
        }else{
            //client or franchise
           $userList = User::find()
                            ->where(['and', ['role' => [User::ROLE_USER, User::ROLE_FRANCHISE, User::ROLE_ASSIST], 'user_group' => $group_id,'status' => User::STATUS_ACTIVE], ['is', 'outlet', new \yii\db\Expression('null')]])->all();  
        }

        //echo '<pre>';
        //print_r($userList);
        $API = new LiveChat_API();
        $leadChat = $API->chats->getSingleChat($chat_id);

        $sent_email_array = [];
        $sent_email_string = '';
        //Leads Details//
        $name = $visitor_name;
        $email = $visitor_email;
        $phone = $visitor_phone;
        $preferred_contact_time = $preferred_contact_time;
        $chat_summary = $lead->chat_summary;
        $chat2 = $leadChat;
        //Leads Details//
        $sent_email_string1='';

        $showLead = 0;//Initialized for main account email sending
        if($is_service_chat==1){
          //$showLead = Leads::showleads($chat_id);
          //$showLead = Leads::shownewleads($chat_id,$user);
          $showLead = 1;
          if($email_sent==0)
          {
            foreach ($userList as $user) {
                $send_to_account_owner = 0;
              //$showLead = Leads::shownewleads($chat_id,$user);
              $notification = Notification::find()->where(['user_id'=>$user->id])->one();
              if(!empty($notification)){
                if($notification->notify_chat==1){
                  if($notification->different_email==1){
                      if(!empty($notification->notify_chat_email))
                      {
                        $notify_emails = explode(", ",$notification->notify_chat_email);
                        foreach($notify_emails as $notify_email){
                          $leadModel = $lead;
                          $service_chat_emails_send = Leads::sendServicechatAllEmail($user, $chat2, $showLead, $name, $email, $leadModel, $notify_email);
                          $email_count++;
                          $sent_email_array[] = ['type' => 'Service chat-different' , 'email' => $notify_email];
                        }
                      }else {
                          $send_to_account_owner = 1;
                      }
                    $sent_email_string1 = $notification->notify_chat_email;
                  }else{
                      //if no different email
                      if(!empty($notification->notify_email)){
                        $notify_emails = explode(", ",$notification->notify_email);
                        foreach($notify_emails as $notify_email){
                          $leadModel = $lead;
                          $service_chat_emails_send = Leads::sendServicechatAllEmail($user, $chat2, $showLead, $name, $email, $leadModel, $notify_email);
                          $email_count++;
                          $sent_email_array[] = ['type' => 'Service chat-same' , 'email' => $notify_email];
                        }
                      }else {
                          $send_to_account_owner = 1;
                      }
                    $sent_email_string1 = $notification->notify_email;
                  }
                  //For Owner Email//
                  /*
                  if(isset($notify_emails)){
                    $find = $notify_emails;
                  }else{
                    $find = ["NOT-SET"];
                  }
                  if(!in_array($user->email,$find)){
                    $sent = Leads::sendEmailServicechat($user, $leadChat, $chat2, $showLead, $name, $email, $phone, $chat_summary);
                    $email_count++;
                    $sent_email_array[] = ['type' => 'Service chat-Owner-Email' , 'email' => $user->email];
                    $sent_email_string .= $user->email . ', ';
                  }
                  */
                  //For Owner Email//
                  $sent_email_string = $sent_email_string.$sent_email_string1.', ';
                }
              }else {
                  //this user is probably new and does not have notification entry in notification table
                  $send_to_account_owner = 1;
              }

              if($send_to_account_owner == 1) {
                  $sent = Leads::sendEmailServicechat($user, $leadChat, $chat2, $showLead, $name, $email, $phone, $chat_summary);
                  $email_count++;
                  $sent_email_array[] = ['type' => 'Service chat-Owner-Email' , 'email' => $user->email];
                  $sent_email_string .= $user->email . ', ';
              }
            }

            //check whether email has gone to main user (client or franchise), if not then send email to him
//            $main_user_email_sent = 0;
//            if($main_user) {
//                foreach($sent_email_array as $email) {
//                  if($main_user->email == $email['email']) {
//                      $main_user_email_sent++;
//                      break;
//                  }
//                }
//
//                if($main_user_email_sent == 0) {
//                    Leads::sendServicechatAllEmail($main_user, $chat2, $showLead, $name, $visitor_email, $lead, $main_user->email);
//                    $email_count++;
//                    $sent_email_array[] = ['type' => 'Service Chat Main Account' , 'email' => $main_user->email];
//                    $sent_email_string .= $main_user->email . ', ';
//                }
//            }
          }

          if($lead->email == 0){
            $lead->email = 1;
            $lead->save();
          }
        }
        else if($is_lead == 1){
          //$showLead = Leads::shownewleads($chat_id,$user);;
          //$showLead = 1;
          if($email_sent==0)
          {
            foreach ($userList as $user) {
                $send_to_account_owner = 0;
                //echo '=========================';
                //echo '<pre>';print_r($user);
              $showLead = Leads::shownewleads($chat_id,$user);
              //var_dump($showLead);
            $notification = Notification::find()->where(['user_id'=>$user->id])->one();
            if(!empty($notification)){
              if($notification->notify_lead==1){
                if($notification->different_email==1){
                    if(!empty($notification->notify_lead_email))
                    {
                      $notify_emails = explode(", ",$notification->notify_lead_email);
                      foreach($notify_emails as $notify_email){
                        $leadModel = $lead;
                        $service_chat_emails_send = Leads::sendForwardingEmail($user, $chat2, $showLead, $name, $email, $leadModel, $notify_email);
                        $email_count++;
                        $sent_email_array[] = ['type' => 'Lead chat-different' , 'email' => $notify_email];
                      }
                    }else {
                        $send_to_account_owner = 1;
                    }
                  $sent_email_string1 = $notification->notify_lead_email;
                }else{
                    if(!empty($notification->notify_email))
                    {
                      $notify_emails = explode(", ",$notification->notify_email);
                      foreach($notify_emails as $notify_email){
                        $leadModel = $lead;
                        $service_chat_emails_send = Leads::sendForwardingEmail($user, $chat2, $showLead, $name, $email, $leadModel, $notify_email);
                        $email_count++;
                        $sent_email_array[] = ['type' => 'Lead chat-same' , 'email' => $notify_email];
                      }
                    }else {
                        $send_to_account_owner = 1;
                    }
                  $sent_email_string1 = $notification->notify_email;
                }
                //For Owner Email
                /*
                if(isset($notify_emails)){
                  $find = $notify_emails;
                }else{
                  $find = ["NOT-SET"];
                }
                if(!in_array($user->email,$find)){
                  $sent = Leads::sendEmailAgent($user, $leadChat, $chat2, $showLead, $name, $email, $phone, $chat_summary);
                  $email_count++;
                  $sent_email_array[] = ['type' => 'Leads chat-Owner-Email' , 'email' => $user->email];
                  $sent_email_string .= $user->email . ', ';
                //For Owner Email
                }
                */
                $sent_email_string = $sent_email_string.$sent_email_string1.', ';
              }
            }else {
                //if user is new and does not have any notification entry in the notification table
                $send_to_account_owner = 1;
            }

            if($send_to_account_owner == 1)
            {
                $sent = Leads::sendEmailAgent($user, $leadChat, $chat2, $showLead, $name, $email, $phone, $chat_summary);
                $email_count++;
                $sent_email_array[] = ['type' => 'Leads chat-Owner-Email' , 'email' => $user->email];
                $sent_email_string .= $user->email . ', ';
            }
          }

          //check whether email has gone to main user (client or franchise), if not then send email to him
//          $main_user_email_sent = 0;
//          if($main_user) {
//              foreach($sent_email_array as $email) {
//                if($main_user->email == $email['email']) {
//                    $main_user_email_sent++;
//                    break;
//                }
//              }
//
//              if($main_user_email_sent == 0) {
//                  Leads::sendForwardingEmail($main_user, $chat2, $showLead, $name, $visitor_email, $lead, $main_user->email);
//                  $email_count++;
//                  $sent_email_array[] = ['type' => 'Lead Main Account' , 'email' => $main_user->email];
//                  $sent_email_string .= $main_user->email . ', ';
//              }
//          }
        }
          if($lead->email == 0){
            $lead->email = 1;
            $lead->save();
          }
        }

        //currently mails are sent to users only in case of lead or service chat
        if($is_service_chat == 1 || $is_lead == 1) {
            if($sent_email_string != ''){
                $emailLog = new EmailLog();
                $emailLog->leads_id = $lead->id;
                $emailLog->chat_id = $lead->chat_id;
                $emailLog->sent_to = rtrim($sent_email_string,', ');
                $emailLog->sent_via = EmailLog::VIA_LEAD_SENDER;
                $tempUser = User::findOne(['user_group' => $lead->user_group]);
                if($tempUser){
                    $emailLog->web_url = $tempUser->website_url;
                }
                $emailLog->save();
            }
        }


        try {
          $arr_tags_live_chat = array();
          if(count($leadChat->tags) > 0)
          {
            $arr_selected_tags = json_decode($tags);//latest selected tags
            $arr_tags_live_chat = array_unique(array_merge($leadChat->tags, $arr_selected_tags));

            //find those tags which were removed by agent from the agent tag and remove them
            $arr_old_saved_tags = json_decode($old_saved_tags);
            $arr_removed_tags = array_diff($arr_old_saved_tags, $arr_selected_tags);

            if(count($arr_removed_tags) > 0) {
                foreach($arr_removed_tags as $key=>$del_val)
                {
                  //search if deleted value is present in array, if yes then delete it using key
                  if (($del_key = array_search($del_val, $arr_tags_live_chat)) !== false) {
                      unset($arr_tags_live_chat[$del_key]);
                  }
                }

                //reset the keys
                $arr_tags_live_chat = array_values($arr_tags_live_chat);
            }
          }else {
            $arr_tags_live_chat = json_decode($tags);
          }

          //update the tags
          $API->chats->updateTags($chat_id, $arr_tags_live_chat);
        }
        catch(\Exception $e) {
          //some error occured while syncing the tags to livechat
          //print_r($e);
        }

        $created_time =  date('h:i A', $lead->c_time) . ", " . date('j', $lead->c_time) . "<sup>". date('S', $lead->c_time) ."</sup> ". date('M', $lead->c_time). " " . date('Y', $lead->c_time);
        //$API = new LiveChat_API();
        //$agentDetail = $API->agents->getAgent($lead->created_by);
        $created_by = $lead->created_by;
        return json_encode(['success' => true, 'action' => $action, 'email_count' => $email_count, 'sent_email_array' => $sent_email_array, 'created_time' => $created_time, 'created_by' => $created_by]);
      }
      return json_encode(['success' => false, 'error' => $lead->getErrors()]);
    }

    public function actionUpdateAgentApp()
    {
      $chat_id = Yii::$app->request->get('chat_id');
      $group_id = Yii::$app->request->get('group_id');
      $userModel = User::findAll(['user_group' => $group_id]);
      $categories = [];
      $sub_categories = [];
      $outlets = [];
      $custom_input_elements = array();
      $custom_cnt = 0;
      foreach ($userModel as $user) {
        $catModel = UserCategory::findAll(['user_id' => $user->id, 'parent_id' => NULL]);
        foreach ($catModel as $cat) {
          //if($cat->parent_id == NULL){
            $value_text = $cat->value == NULL ? '' : " ($$cat->value)";
            $categories[$cat->id] = ucfirst($cat->category_name) . $value_text;
          //}
        }
        if($user->role == User::ROLE_FRANCHISE || $user->role == User::ROLE_USER)
        {
          $outletModel = User::findAll(['role' => User::ROLE_OUTLET, 'parent_id' => $user->id]);
          foreach ($outletModel as $outlet) {
            $outlets[$outlet->id] = $outlet->name;
          }
          if(!empty($outletModel)){
           $outlets[$user->id] = 'Owner';
          }
        }

        if($custom_cnt == 0){ //to avoid one group multiple user issue
          $custom_cnt++;

            $custom_input_elements = array();
            $custom_fields_options = Yii::$app->db->createCommand("SELECT custom_leads_fields.id as field_id,custom_leads_fields_option.id as option_id, custom_leads_fields.type,custom_leads_fields.label,custom_leads_fields.required,custom_leads_fields_option.option FROM custom_leads_fields LEFT JOIN custom_leads_fields_option ON custom_leads_fields.id = custom_leads_fields_option.custom_leads_field_id WHERE custom_leads_fields.user_id=$user->id AND custom_leads_fields.status = 1 AND custom_leads_fields.soft_delete = 0 AND (custom_leads_fields_option.status = 1 OR custom_leads_fields_option.status IS NULL) AND (custom_leads_fields_option.soft_delete = 0 OR custom_leads_fields_option.soft_delete IS NULL) ORDER BY field_id, option_id ASC")->queryAll();
             //echo '<pre>';print_r($custom_fields_options);


             if(count($custom_fields_options) > 0)
             {
               foreach($custom_fields_options as $option)
               {
                 if(!array_key_exists($option['field_id'],$custom_input_elements))
                 {
                   $custom_input_elements[$option['field_id']] = [
                     'type' => $option['type'],
                     'label'=> $option['label'],
                     'required' => $option['required']
                   ];

                   if($option['type'] == 'textbox')
                   {
                     $custom_input_elements[$option['field_id']]['options'] = [];
                   }else {
                     $custom_input_elements[$option['field_id']]['options'][] = [
                       'option_id' => $option['option_id'],
                       'text' => $option['option']
                     ];
                   }
                 }
                 else {
                   if($option['type'] == 'dropdown' || $option['type'] == 'multiple_choice')
                   {
                     $custom_input_elements[$option['field_id']]['options'][] = [
                       'option_id' => $option['option_id'],
                       'text' => $option['option']
                     ];
                   }
                 }
               }
             }
        }




      }

      $API = new LiveChat_API();
      $live_chat_tags = $API->chats->getAllTags();

      $leadTag = array();
      $serviceTag = array();
      $otherLiveChatTags = array();
      foreach($live_chat_tags as $live_tag)
      {
        if($live_tag->name == 'lead')
        {
          $leadTag[] = $live_tag->name;
        }else if($live_tag->name == 'Service Chat') {
          $serviceTag[] = $live_tag->name;
        }else {
          $otherLiveChatTags[] = $live_tag->name;
        }
      }

      $allLiveChatTags = array_merge($leadTag, $serviceTag, $otherLiveChatTags);

      $lead = Leads::findOne(['chat_id' => $chat_id]);
      if($lead){
        $visitor_name = $lead->visitor_name;
        $visitor_last_name = $lead->visitor_last_name;
        $visitor_email = $lead->visitor_email;
        $visitor_phone = $lead->visitor_phone;
        $preferred_contact_time = $lead->preferred_contact_time;
        //$created_by = $lead->created_by == NULL ? 'NA' : $lead->created_by;
        $custom_fields_response = $lead->custom_fields;
        if($lead->created_by != NULL){
          //$API = new LiveChat_API();
          //$agentDetail = $API->agents->getAgent($lead->created_by);
          $created_by = $lead->created_by;
        }else{
          $API = new LiveChat_API();
          $chatDetail = $API->chats->getSingleChat($lead->chat_id);
          $created_by = isset($chatDetail->agents[0]->email) ? $chatDetail->agents[0]->email : 'NA';
        }
        $subCat = UserCategory::findOne($lead->category_id);
        if(isset($subCat->parent_id)){
          foreach ($subCat->parent->children as $childCat) {
            $value_text = $childCat->value == NULL ? '' : " ($$childCat->value)";
            $sub_categories[$childCat->id] = ucfirst($childCat->category_name) . $value_text;
          }
          $category_id = $subCat->parent->id;
          $sub_category_id = $lead->category_id;
        }else{
          $category_id = $lead->category_id;
          $sub_category_id = 0;
        }

        $outlet_id = $lead->outlet_id;
        $tags = $lead->tags;

        $arr_tags_exists = json_decode($tags);
        $arr_tag_exists_index = array();
        if(count($arr_tags_exists) > 0) {
          foreach($allLiveChatTags as $key=>$val) {
            if(in_array($val, $arr_tags_exists)) {
              $arr_tag_exists_index[] = $key;
            }
          }
        }

        //print_r($arr_tag_exists_index);

        $chat_summary = $lead->chat_summary;
        //$created_time = date('d-m-Y h:i A', $lead->c_time) ;
        $created_time =  date('h:i A', $lead->c_time) . ", " . date('j', $lead->c_time) . "<sup>". date('S', $lead->c_time) ."</sup> ". date('M', $lead->c_time). " " . date('Y', $lead->c_time);
        $response = [
          'lead_exist' => true,
          'visitor_name' => $visitor_name,
          'visitor_last_name' => $visitor_last_name,
          'visitor_email' => $visitor_email,
          'visitor_phone' => $visitor_phone,
          'preferred_contact_time' => $preferred_contact_time,
          'category_id' => $category_id,
          'sub_category_id' => $sub_category_id,
          'outlet_id' => $outlet_id,
          'tags' => $tags,
          'all_live_chat_tags' => $allLiveChatTags,
          'arr_tag_exists_index' => $arr_tag_exists_index,
          'is_service_chat' => $lead->is_service_chat,
          'is_lead' => $lead->is_lead,
          'chat_summary' => $chat_summary,
          'categories' => $categories,
          'sub_categories' => $sub_categories,
          'outlets' => $outlets,
          'custom_input_elements' => $custom_input_elements,
          'custom_fields_response' => $custom_fields_response,
          'created_time' => $created_time,
          'created_by' => $created_by,
        ];
      }else{
        $response = [
          'lead_exist' => false,
          'categories' => $categories,
          'sub_categories' => $sub_categories,
          'outlets' => $outlets,
          'custom_input_elements' => $custom_input_elements,
          'all_live_chat_tags' => $allLiveChatTags
        ];
      }

      return json_encode($response);
    }

    public function actionGetSubCategories(){
      $id = Yii::$app->request->post('id');
      $sub_cats = UserCategory::findAll(['parent_id' => $id]);
      $categories = [];
      $count = 0;
      foreach ($sub_cats as $cat) {
        $value_text = $cat->value == NULL ? '' : " ($$cat->value)";
        $categories[$cat->id] = ucfirst($cat->category_name) . $value_text;
        $count++;
      }
      return json_encode(['success' => true, 'count' => $count, 'sub_categories' => $categories]);
    }

    public function actionMarkLeadSeen()
    {
      if(Yii::$app->user->identity->role == User::ROLE_OUTLET){
        $newLeads = Leads::findAll(['outlet_id' => Yii::$app->user->identity->id, 'user_group' => Yii::$app->user->identity->user_group, 'new_lead' => 1]);
      }else{
        $newLeads = Leads::findAll(['user_group' => Yii::$app->user->identity->user_group, 'new_lead' => 1]);
      }

      foreach ($newLeads as $lead) {
        $seenLead = LeadSeen::findOne(['leads_id' => $lead->id, 'user_id' => Yii::$app->user->identity->id]);
        if(!$seenLead){
          $leadSeen = new LeadSeen();
          $leadSeen->user_id = Yii::$app->user->identity->id;
          $leadSeen->leads_id = $lead->id;
          $leadSeen->save();
        }
      }

      return $this->redirect(['index']);
    }

    public function actionForwardEmail()
    {
      $teamlist = Yii::$app->request->post('teamList');  
      $emailAddress = empty(Yii::$app->request->post('emailArray')) ? array() : Yii::$app->request->post('emailArray');
      $chat_id = Yii::$app->request->post('chat_id');

      $leadModel = Leads::find()->where(['chat_id'=>$chat_id])->one();
      if(!$leadModel){
        return json_encode(['success' => false, 'message' => 'Lead does not exist']);
      }

      try {
        $API = new LiveChat_API();
        $chat2 = $API->chats->getSingleChat($chat_id);
      }
      catch (Exception $e) {
        throw new NotFoundHttpException($e->getMessage());
      }

      $showLead = 1; // hardcode
      $user = User::findOne(Yii::$app->user->identity->id);
      $name = $leadModel->visitor_name;
      $email = $leadModel->visitor_email;
      
      if(!empty($teamlist)) {
          foreach($teamlist as $team) {
              $team_emails = ForwardingEmailAddress::findAll(['team' => $team]);
              foreach($team_emails as $team_email) {
                  $emailAddress[] = $team_email->id;
              }
          }
      }

      $count = count($emailAddress);
      $sent_count = 0;
      $sent_email_string = '';

      foreach ($emailAddress as $value) {
        $forwardEmailAddress = ForwardingEmailAddress::findOne($value);
        $forwardEmail = $forwardEmailAddress->email;

        $sent = Leads::sendForwardingEmail($user, $chat2, $showLead, $name, $email, $leadModel, $forwardEmail);
        $sent_email_string .= $forwardEmail . ', ';
        if($sent){
          $sent_count++;
        }

      }

      if($sent_email_string != ''){
        $emailLog = new EmailLog();
        $emailLog->leads_id = $leadModel->id;
        $emailLog->chat_id = $leadModel->chat_id;
        $emailLog->sent_to = substr($sent_email_string, 0, -2);
        $emailLog->sent_via = EmailLog::VIA_FORWARDING_EMAIL;
        $tempUser = User::findOne(['user_group' => $leadModel->user_group]);
        if($tempUser){
          $emailLog->web_url = $tempUser->website_url;
        }
        $leadModel->email = 1;
        $leadModel->save();
        $emailLog->save();
      }

      return json_encode(['success' => true, 'count' => $count, 'sent_count' => $sent_count]);
    }

    public function actionForwardEmailSeviceChat()
    {
      $teamlist = Yii::$app->request->post('teamList');  
      $emailAddress = empty(Yii::$app->request->post('emailArray')) ? array() : Yii::$app->request->post('emailArray');
      $chat_id = Yii::$app->request->post('chat_id');

      $leadModel = Leads::find()->where(['chat_id'=>$chat_id])->one();
      if(!$leadModel){
        return json_encode(['success' => false, 'message' => 'Lead does not exist']);
      }

      try {
        $API = new LiveChat_API();
        $chat2 = $API->chats->getSingleChat($chat_id);
      }
      catch (Exception $e) {
        throw new NotFoundHttpException($e->getMessage());
      }

      $showLead = 1; // hardcode
      $user = User::findOne(Yii::$app->user->identity->id);
      $name = $leadModel->visitor_name;
      $email = $leadModel->visitor_email;
      
      if(!empty($teamlist)) {
          foreach($teamlist as $team) {
              $team_emails = ForwardingEmailAddress::findAll(['team' => $team]);
              foreach($team_emails as $team_email) {
                  $emailAddress[] = $team_email->id;
              }
          }
      }
      
      $count = count($emailAddress);
      $sent_count = 0;
      $sent_email_string = '';

      foreach ($emailAddress as $value) {
        $forwardEmailAddress = ForwardingEmailAddress::findOne($value);
        $forwardEmail = $forwardEmailAddress->email;

        $sent = Leads::sendServicechatAllEmail($user, $chat2, $showLead, $name, $email, $leadModel, $forwardEmail);

        $sent_email_string .= $forwardEmail . ', ';
        if($sent){
          $sent_count++;
        }
      }

      if($sent_email_string != ''){
        $emailLog = new EmailLog();
        $emailLog->leads_id = $leadModel->id;
        $emailLog->chat_id = $leadModel->chat_id;
        $emailLog->sent_to = substr($sent_email_string, 0, -2);
        $emailLog->sent_via = EmailLog::VIA_FORWARDING_EMAIL;
        $tempUser = User::findOne(['user_group' => $leadModel->user_group]);
        if($tempUser){
          $emailLog->web_url = $tempUser->website_url;
        }
        $leadModel->email = 1;
        $leadModel->save();
        $emailLog->save();
      }
      return json_encode(['success' => true, 'count' => $count, 'sent_count' => $sent_count]);
    }

    public function actionQuickSendServiceChat()
    {
      $chat_id = Yii::$app->request->post('chat_id');
      $quick_email = Yii::$app->request->post('quick_email');

      $leadModel = Leads::find()->where(['chat_id'=>$chat_id])->one();
      if(!$leadModel){
        return json_encode(['success' => false, 'message' => 'Lead does not exist']);
      }
      try {
        $API = new LiveChat_API();
        $chat2 = $API->chats->getSingleChat($chat_id);
      }
      catch (Exception $e) {
        throw new NotFoundHttpException($e->getMessage());
      }

      $showLead = 1; // hardcode
      $user = User::findOne(Yii::$app->user->identity->id);
      $name = $leadModel->visitor_name;
      $email = $leadModel->visitor_email;
      $forwardEmail = $quick_email;

      $sent = Leads::sendServicechatAllEmail($user, $chat2, $showLead, $name, $email, $leadModel, $forwardEmail);

      if($sent){
        $emailLog = new EmailLog();
        $emailLog->leads_id = $leadModel->id;
        $emailLog->chat_id = $leadModel->chat_id;
        $emailLog->sent_to = $forwardEmail;
        $emailLog->sent_via = EmailLog::VIA_QUICK_SEND;
        $tempUser = User::findOne(['user_group' => $leadModel->user_group]);
        if($tempUser){
          $emailLog->web_url = $tempUser->website_url;
        }
        $leadModel->email = 1;
        $leadModel->save();
        $emailLog->save();
        return json_encode(['success' => true]);
      }
    }

    public function actionQuickSend()
    {
      $chat_id = Yii::$app->request->post('chat_id');
      $quick_email = Yii::$app->request->post('quick_email');
      //print_r($chat_id);exit;
      $leadModel = Leads::find()->where(['chat_id'=>$chat_id])->one();
      //print_r($leadModel);exit;
      if(!$leadModel){
        return json_encode(['success' => false, 'message' => 'Lead does not exist']);
      }

      try {
        $API = new LiveChat_API();
        $chat2 = $API->chats->getSingleChat($chat_id);
      }
      catch (Exception $e) {
        throw new NotFoundHttpException($e->getMessage());
      }

      $showLead = 1; // hardcode
      $user = User::findOne(Yii::$app->user->identity->id);
      $name = $leadModel->visitor_name;
      $email = $leadModel->visitor_email;
      $forwardEmail = $quick_email;

      $sent = Leads::sendForwardingEmail($user, $chat2, $showLead, $name, $email, $leadModel, $forwardEmail);

      if($sent){
        $emailLog = new EmailLog();
        $emailLog->leads_id = $leadModel->id;
        $emailLog->chat_id = $leadModel->chat_id;
        $emailLog->sent_to = $forwardEmail;
        $emailLog->sent_via = EmailLog::VIA_QUICK_SEND;
        $tempUser = User::findOne(['user_group' => $leadModel->user_group]);
        if($tempUser){
          $emailLog->web_url = $tempUser->website_url;
        }
        $leadModel->email = 1;
        $leadModel->save();
        $emailLog->save();
        return json_encode(['success' => true]);
      }

    }

    public function actionTestApi()
    {
      $group = 0;
      $name = '';
      $email = '';
      //mail("iamparam.surya@gmail.com","API subject", json_encode($_REQUEST));
      $userData = User::findByEmail($_REQUEST['username']);

      if( !empty( $userData ) ){
        if($userData->validatePassword($_REQUEST['password'])){
          $group = $userData['user_group'];
        }else{
          echo "Invalid User";exit;
        }
      }else{
        echo "Invalid User";exit;
      }

      if($group > 0){
        try {
          $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
          if(!isset($_GET["date_from"])){
            $_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
            $_GET['date_to'] = date('Y-m-d');
          }
          $_GET['tag%5B%5D'] = 'lead';
          $_GET["group"] = $group;
          $API = new LiveChat_API();
          $chats = $API->chats->get($page,$_GET);
        }
        catch (Exception $e) {
          throw new NotFoundHttpException($e->getMessage());
        }


        $transcripts = $chats->chats;

        $data = [];
        $extra_info = NULL;

        foreach($transcripts as $row => $transcript){
          $leadModel = Leads::find()->where(['chat_id' => $transcript->id ])->one();

          if($leadModel && $leadModel->from_agent_app == 0){
            continue;
          }
          //echo "<pre>"; print_r($transcript); exit;
          foreach($transcript as $key => $value){
            if($key == 'visitor' || $key == 'ended' || $key == 'prechat_survey' || $key == 'chat_start_url' || $key == 'messages' || $key == 'referrer' || $key == 'duration' || $key == 'started' || $key == 'timezone'){
                $final_value = [];
                $formattedMessage = '';
                if($key == 'messages'){
                  foreach($value as $k1 => $messages){
                    //print_r($messages); exit;
                    if($messages->user_type == 'agent'){
                      $formattedMessage = $formattedMessage . "Agent($messages->author_name): ".$messages->text. "<br>";
                    }

                    if($messages->user_type == 'visitor'){
                      $formattedMessage = $formattedMessage . "Visitor($messages->author_name): ".$messages->text. "<br>";
                    }

                    foreach($messages as $msg_key => $message){
                      $messageData = $message;
                      $namePos = stripos($messageData,"name:");
                      if( $namePos > 0 ){
                        $nameStartPos = $namePos + 5;
                        $emailPos = stripos($messageData,"email:");
                        $nameLen = $emailPos - $nameStartPos;
                        $emailStartPos = $emailPos + 6;
                        $phonePos = stripos($messageData,"phone:");
                        $emailLen = $phonePos - $emailStartPos;
                        // old name and email //
                        /*$name = substr($messageData, $nameStartPos, $nameLen);
                        $email = substr($messageData, $emailStartPos, $emailLen);
                        if(!isset($transcript->visitor->email)){
                          $data[0]['visitor']['name'] = trim($name);
                          $data[0]['visitor']['email'] = trim($email);
                        }*/
                        if(isset($leadModel)){
                          $data[0]['visitor']['name'] = $leadModel->visitor_first_name . ' ' . $leadModel->visitor_last_name;
                          $data[0]['visitor']['email'] = $leadModel->visitor_email;
                          $data[0]['visitor']['first_name'] =  $leadModel->visitor_first_name;
                          $data[0]['visitor']['last_name'] =  $leadModel->visitor_last_name;
                        }
                      }
                      // if($msg_key == 'text' || $msg_key == 'date'){
                      //   //$final_value[$k1][$msg_key] = $message;
                      //   if($msg_key == 'text'){
                      //     if($k1%2 == 0){
                      //       $formattedMessage = $formattedMessage . 'Agent: ' . $message . "<br>";
                      //     }else{
                      //       $formattedMessage = $formattedMessage . 'Visitor: ' . $message . "<br>";
                      //     }
                      //   }
                      // }
                    }
                  }
                }else if($key == 'prechat_survey'){
                  foreach($value as $k1 => $prechat_survey){
                    foreach($prechat_survey as $msg_key => $message){
                      if($msg_key == 'value'){
                          $final_value[$k1][$msg_key] = $message;
                      }
                    }
                  }
                }else if($key == 'visitor'){

                  foreach($value as $msg_key => $message){
                    if($msg_key != 'id' && $msg_key != 'ip'){
                        $final_value[$msg_key] = $message;
                    }
                  }

                  if(isset($leadModel)){
                    //$extra_info['visitor_first_name'] = $leadModel->visitor_name;
                    //$extra_info['visitor_last_name'] = $leadModel->visitor_last_name;
                    $extra_info['visitor_email'] = $leadModel->visitor_email;
                    $extra_info['visitor_phone'] = $leadModel->visitor_phone;
                    //$extra_info['category'] = $leadModel->category_id;
                    $userCat = UserCategory::findOne($leadModel->category_id);
                    $extra_info['category'] = isset($userCat->category_name) ? $userCat->category_name : 'Not Set';
                    //$extra_info['outlet'] = $leadModel->outlet_id;
                    //$extra_info['tags'] = $leadModel->tags;
                    $extra_info['chat_summary'] = $leadModel->chat_summary;
                    $final_value['extra_info'] = $extra_info;
                  }else{
                    $extra_info['visitor_first_name'] = 'Not Set';
                    $extra_info['visitor_last_name'] = 'Not Set';
                    $extra_info['visitor_email'] = 'Not Set';
                    $extra_info['visitor_phone'] = 'Not Set';
                    $extra_info['category'] = 'Not Set';
                    //$extra_info['outlet'] = 'Not Set';
                    //$extra_info['tags'] = 'Not Set';
                    $extra_info['chat_summary'] = 'Not Set';
                    $final_value['extra_info'] = $extra_info;


                  }

                }else{
                  $final_value = $value;
                }
                if($formattedMessage != ''){
                  $final_value['formatted_messages'] = $formattedMessage;
                }

                $data[$row][$key] = $final_value;
              }
            }
          break;
          }
          echo json_encode($data);
        }
    }

    public function actionNewApi2()
    {
        $headers = Yii::$app->request->headers;
        //print_r($headers);exit;
        if($headers->has('api-key')){
           return $accept = $headers->get('api-key');
        }
    }

    public function actionNewApi()
    {
      $group = 0;
      $zapier_request_id = 0;
      $name = '';
      $email = '';
      //mail("iamparam.surya@gmail.com","API subject", json_encode($_REQUEST));
      //save zapier request log
      try{
        $zapier_request = new ZapierRequestLogs();
        $zapier_request->type = 'newapi';
        $zapier_request->username = $_REQUEST['username'];
        $zapier_request->created_at = time();
        $zapier_request->save(false);
        $zapier_request_id = $zapier_request->id;
      }
      catch(\Exception $e) {

      }

      $userData = User::findByEmail($_REQUEST['username']);

      if( !empty( $userData ) ){
        if($userData->validatePassword($_REQUEST['password'])){
          $group = $userData['user_group'];

          try {
            //update zapier_request_logs
            if($zapier_request_id > 0) {
              $zapier_request->authenticated = 1;
              $zapier_request->user_group = $group;
              $zapier_request->save(false);
            }
          }
          catch(\Exception $e) {

          }
        }else{
          echo "Invalid User";exit;
        }
      }else{
        echo "Invalid User";exit;
      }

      if($group > 0){
        try {
          // $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
          // if(!isset($_GET["date_from"])){
          //   $_GET['date_from'] = date('Y-m-d',strtotime("-1 days"));
          //   $_GET['date_to'] = date('Y-m-d');
          // }
          // $_GET['tag%5B%5D'] = 'lead';
          // $_GET["group"] = $group;
          // $API = new LiveChat_API();
          // $chats = $API->chats->get($page,$_GET);

          $old_timestamp = strtotime('-2 day');

          $queryChats = "SELECT chat.*
                  FROM `leads`
                  JOIN `chat` ON `chat`.`chat_id` = `leads`.`chat_id`
                  WHERE `leads`.`is_lead` = 1
                  AND `leads`.`user_group` = ".$group."
                  AND `chat`.`ended_timestamp` > ".$old_timestamp."
                  ORDER BY `chat`.`id` DESC LIMIT 20";

          $transcripts = Chat::findBySql($queryChats)->all();
        }
        catch (Exception $e) {
          throw new NotFoundHttpException($e->getMessage());
        }

        //$transcripts = $chats->chats;
        try {
          //update zapier_request_logs
          if($zapier_request_id > 0) {
            $arr_fetched_chats = array();
            foreach($transcripts as $row => $transcript)
            {
              $arr_fetched_chats[] = $transcript->chat_id;
            }

            $zapier_request->chat_data = json_encode($arr_fetched_chats);
            $zapier_request->save(false);
          }
        }
        catch(\Exception $e) {

        }

        $data = [];
        $extra_info = NULL;
        $final_data = [];

        $lead_id = 0;
        $chat_id = '';
        $email_for_log = '';
        $count = 0;

        foreach($transcripts as $row => $transcript){
          $response_data = new \stdClass();

          $leadModel = Leads::find()->where(['chat_id' => $transcript->chat_id ])->one();

          if($leadModel && $leadModel->from_agent_app == 0){
            continue;
          }
          //echo "<pre>"; print_r($transcript); exit;

          if($leadModel){
            $lead_id = $leadModel->id;
            $chat_id = $transcript->chat_id;
            $email_for_log = $leadModel->visitor_email;

            $response_data->id = $lead_id;
            $response_data->transcript_id = $transcript->chat_id; //chat_id
            $response_data->visitor_name = ucfirst($leadModel->visitor_name) . ' ' . ucfirst($leadModel->visitor_last_name);
            $response_data->visitor_first_name = ucfirst($leadModel->visitor_name);
            $response_data->visitor_last_name = ucfirst($leadModel->visitor_last_name);
            $response_data->visitor_email = ucfirst($leadModel->visitor_email);
            $response_data->visitor_phone =$leadModel->visitor_phone;
            if(!empty($leadModel->custom_fields)){
              $custom_fields = json_decode($leadModel->custom_fields,true);
              //print_r($custom_fields);exit;
              $multipleChoiceString = '';
              foreach($custom_fields as $key => $custom_field){
                  $custom_table = CustomLeadFields::findOne($custom_field['field_id']);
                if($custom_field['field_type']=='textbox'){
                     $fieldvalue = $custom_field['field_value'];
                     $label1 = str_replace(' ', '_', htmlspecialchars_decode($custom_table->label));
                     $response_data->$label1 = urldecode($fieldvalue);
                }elseif ($custom_field['field_type']=='dropdown'){
                  $dropDownOption = CustomLeadFieldsOption::findOne($custom_field['field_value']);
                  $fieldvalue2 = $dropDownOption['option'];
                  $label2 = str_replace(' ', '_', htmlspecialchars_decode($custom_table->label));
                  $response_data->$label2 = urldecode($fieldvalue2);
                }elseif ($custom_field['field_type']=='multiple_choice'){
                  $custom_field_id_arrays = explode(',', urldecode($custom_field['field_value']));
                  $multipleChoiceOptions = CustomLeadFieldsOption::findAll($custom_field_id_arrays);
                  foreach($multipleChoiceOptions as $multipleChoiceOption){
                    $multipleChoiceString .= $multipleChoiceOption['option'].',';
                  }
                  $fieldvalue3 = rtrim($multipleChoiceString,', ');
                  $label3 = str_replace(' ', '_', htmlspecialchars_decode($custom_table->label));
                  $response_data->$label3 = urldecode($fieldvalue3);
                }
              }
             }
            $userCat = UserCategory::findOne($leadModel->category_id);
            $response_data->category = isset($userCat->category_name) ? $userCat->category_name : 'Not Set';
            $response_data->chat_summary = $leadModel->chat_summary;
          }else{
            echo "No New LEAD";exit;
            // $response_data->visitor_name = $transcript->visitor->name;
            // $response_data->visitor_first_name =  'Not Set';
            // $response_data->visitor_last_name = 'Not Set';
            // $response_data->visitor_email = $transcript->visitor->email;
          }

          $response_data->visitor_city = $transcript->city;
          $response_data->visitor_region = $transcript->region;
          $response_data->visitor_country = $transcript->country;
          $response_data->country_code = $transcript->country_code;
          $response_data->timezone = $transcript->timezone;

          //$chat_messages = $transcript->messages;
          $queryChatMessages = "SELECT chat_message.*
                  FROM `chat_message`
                  WHERE `chat_id` = '".$transcript->id."'
                  ORDER BY `id` ASC";

          $chat_messages = ChatMessage::findBySql($queryChatMessages)->all();
          $formattedMessage = '';

          foreach ($chat_messages as $msg) {
            if($msg->user_type == 'agent'){
              $formattedMessage = $formattedMessage . "Agent: ".$msg->text. "\n";
            }

            if($msg->user_type == 'visitor'){
              $formattedMessage = $formattedMessage . "Visitor: ".$msg->text. "\n";
            }
          }

          $response_data->transcript_html = $formattedMessage;
          $response_data->agent_name = isset($transcript->agent_name) ? $transcript->agent_name : 'Not Set';
          $response_data->referrer = isset($transcript->referrer)?$transcript->referrer:'not-set';
          //echo '<pre>'; print_r($transcript); exit;
          // $chat_prechat_survey = $transcript->prechat_survey;
          // $chat_prechat_survey_data = [];
          // foreach ($chat_prechat_survey as $index => $survey_field) {
          //   $chat_prechat_survey_data[$survey_field->key] = $survey_field->value;
          // }
          //
          // $response_data->prechat_survey = $chat_prechat_survey_data;
          $response_data->chat_start_url = $transcript->chat_start_url;
          $response_data->chat_duration = $transcript->duration;

          //break;
          $final_data[] = $response_data;
          $count++;

          if($count >= 20) {
              break;
          }

          }
          //echo json_encode($data);
          //$resp = json_encode([$response_data]);
          $resp = json_encode($final_data);

          //save zapier log
          try{
            if(count($final_data) > 0) {
              foreach($final_data as $chat_info)
              {
                $zap_log = ZapierLogs::findOne(['chat_id' => $chat_info->transcript_id]);
                if(empty($zap_log)){
                  $zap_log = new ZapierLogs();
                  $zap_log->lead_id = $chat_info->id;
                  $zap_log->chat_id = $chat_info->transcript_id;
                  $zap_log->email = $chat_info->visitor_email;
                  $zap_log->type = 'lead';
                  $zap_log->request_id = $zapier_request_id;
                  $zap_log->user_group = $group;
                  $zap_log->created_at = time();
                  $zap_log->data = json_encode($chat_info);
                  $zap_log->save(false);
                }else{
                  $zap_log->updated_request_id = $zapier_request_id;
                  $zap_log->updated_at = time();
                  $zap_log->data = json_encode($chat_info);
                  $zap_log->save(false);
                }
              }
            }
          }catch(\Exception $e) {
            //zapier log saving failed
          }

          echo $resp;
        }
    }

    public function actionNewApi1()
    {
      $group = 0;
      $zapier_request_id = 0;
      $name = '';
      $email = '';
      $headers = Yii::$app->request->headers;
      //$headers->has('api_key');
      //mail("iamparam.surya@gmail.com","API subject", json_encode($_REQUEST));
      //$userData = User::findByEmail($_REQUEST['username']);
      if($headers->has('api-key')){
        $accept = $headers->get('api-key');

        //save zapier request log
        try{
          $zapier_request = new ZapierRequestLogs();
          $zapier_request->type = 'newapi1';
          $zapier_request->api_key = $accept;
          $zapier_request->created_at = time();
          $zapier_request->save(false);
          $zapier_request_id = $zapier_request->id;
        }
        catch(\Exception $e) {

        }

        $userData = User::findOne(['auth_key'=>$accept]);
      if( !empty( $userData ) ){
        $group = $userData['user_group'];

        try {
          //update zapier_request_logs
          if($zapier_request_id > 0) {
            $zapier_request->authenticated = 1;
            $zapier_request->user_group = $group;
            $zapier_request->save(false);
          }
        }
        catch(\Exception $e) {

        }
      }else{
        return "Invalid User";exit;
      }

      if($group > 0){
        try {
          // $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
          // if(!isset($_GET["date_from"])){
          //   $_GET['date_from'] = date('Y-m-d',strtotime("-1 days"));
          //   $_GET['date_to'] = date('Y-m-d');
          // }
          // $_GET['tag%5B%5D'] = 'lead';
          // $_GET["group"] = $group;
          // $API = new LiveChat_API();
          // $chats = $API->chats->get($page,$_GET);

          $old_timestamp = strtotime('-2 day');

          $queryChats = "SELECT chat.*
                  FROM `leads`
                  JOIN `chat` ON `chat`.`chat_id` = `leads`.`chat_id`
                  WHERE `leads`.`is_lead` = 1
                  AND `leads`.`user_group` = ".$group."
                  AND `chat`.`ended_timestamp` > ".$old_timestamp."
                  ORDER BY `chat`.`id` DESC LIMIT 20";

          $transcripts = Chat::findBySql($queryChats)->all();
        }
        catch (\Exception $e) {
          throw new NotFoundHttpException($e->getMessage());
        }

        //$transcripts = $chats->chats;

        try {
          //update zapier_request_logs
          if($zapier_request_id > 0) {
            $arr_fetched_chats = array();
            foreach($transcripts as $row => $transcript)
            {
              $arr_fetched_chats[] = $transcript->chat_id;
            }

            $zapier_request->chat_data = json_encode($arr_fetched_chats);
            $zapier_request->save(false);
          }
        }
        catch(\Exception $e) {

        }

        $data = [];
        $extra_info = NULL;
        $final_data = [];

        $lead_id = 0;
        $chat_id = '';
        $email_for_log = '';
        $count = 0;

        foreach($transcripts as $row => $transcript){
          $response_data = new \stdClass();

          $leadModel = Leads::find()->where(['chat_id' => $transcript->chat_id ])->one();

          if($leadModel && $leadModel->from_agent_app == 0){
            continue;
          }

          if($leadModel){
            $lead_id = $leadModel->id;
            $chat_id = $transcript->chat_id;
            $email_for_log = $leadModel->visitor_email;

            $response_data->id = $lead_id;
            $response_data->transcript_id = $transcript->chat_id; //chat_id
            $response_data->visitor_name = ucfirst($leadModel->visitor_name) . ' ' . ucfirst($leadModel->visitor_last_name);
            $response_data->visitor_first_name = ucfirst($leadModel->visitor_name);
            $response_data->visitor_last_name = ucfirst($leadModel->visitor_last_name);
            $response_data->visitor_email = ucfirst($leadModel->visitor_email);
            $response_data->visitor_phone =$leadModel->visitor_phone;
            if(!empty($leadModel->custom_fields)){
              $custom_fields = json_decode($leadModel->custom_fields,true);
              //print_r($custom_fields);exit;
              $multipleChoiceString = '';
              foreach($custom_fields as $key => $custom_field){
                  $custom_table = CustomLeadFields::findOne($custom_field['field_id']);
                if($custom_field['field_type']=='textbox'){
                     $fieldvalue = $custom_field['field_value'];
                     $label1 = str_replace(' ', '_', htmlspecialchars_decode($custom_table->label));
                     $response_data->$label1 = urldecode($fieldvalue);
                }elseif ($custom_field['field_type']=='dropdown'){
                  $dropDownOption = CustomLeadFieldsOption::findOne($custom_field['field_value']);
                  $fieldvalue2 = $dropDownOption['option'];
                  $label2 = str_replace(' ', '_', htmlspecialchars_decode($custom_table->label));
                  $response_data->$label2 = urldecode($fieldvalue2);
                }elseif ($custom_field['field_type']=='multiple_choice'){
                  $custom_field_id_arrays = explode(',', urldecode($custom_field['field_value']));
                  $multipleChoiceOptions = CustomLeadFieldsOption::findAll($custom_field_id_arrays);
                  foreach($multipleChoiceOptions as $multipleChoiceOption){
                    $multipleChoiceString .= $multipleChoiceOption['option'].',';
                  }
                  $fieldvalue3 = rtrim($multipleChoiceString,', ');
                  $label3 = str_replace(' ', '_', htmlspecialchars_decode($custom_table->label));
                  $response_data->$label3 = urldecode($fieldvalue3);
                }
              }
              }
            $userCat = UserCategory::findOne($leadModel->category_id);
            $response_data->category = isset($userCat->category_name) ? $userCat->category_name : 'Not Set';
            $response_data->chat_summary = $leadModel->chat_summary;
          }else{
            return "No New LEAD";exit;
            // $response_data->visitor_name = $transcript->visitor->name;
            // $response_data->visitor_first_name =  'Not Set';
            // $response_data->visitor_last_name = 'Not Set';
            // $response_data->visitor_email = $transcript->visitor->email;
          }

          $response_data->visitor_city = $transcript->city;
          $response_data->visitor_region = $transcript->region;
          $response_data->visitor_country = $transcript->country;
          $response_data->country_code = $transcript->country_code;
          $response_data->timezone = $transcript->timezone;

          //$chat_messages = $transcript->messages;
          $queryChatMessages = "SELECT chat_message.*
                  FROM `chat_message`
                  WHERE `chat_id` = '".$transcript->id."'
                  ORDER BY `id` ASC";

          $chat_messages = ChatMessage::findBySql($queryChatMessages)->all();
          $formattedMessage = '';
          //echo '<pre>';
          //print_r($chat_messages);
          foreach ($chat_messages as $msg) {
            if($msg->user_type == 'agent'){
              $formattedMessage = $formattedMessage . "Agent: ".$msg->text. "\n";
            }

            if($msg->user_type == 'visitor'){
              $formattedMessage = $formattedMessage . "Visitor: ".$msg->text. "\n";
            }
          }

          $response_data->transcript_html = $formattedMessage;
          $response_data->agent_name = isset($transcript->agent_name) ? $transcript->agent_name : 'Not Set';
          $response_data->referrer = isset($transcript->referrer)?$transcript->referrer:'not-set';
          //echo '<pre>'; print_r($transcript); exit;
          // $chat_prechat_survey = $transcript->prechat_survey;
          // $chat_prechat_survey_data = [];
          // foreach ($chat_prechat_survey as $index => $survey_field) {
          //   $chat_prechat_survey_data[$survey_field->key] = $survey_field->value;
          // }
          //
          // $response_data->prechat_survey = $chat_prechat_survey_data;
          $response_data->chat_start_url = $transcript->chat_start_url;
          $response_data->chat_duration = $transcript->duration;

          //break;
          $final_data[] = $response_data;
          $count++;

          if($count >= 20) {
              break;
          }

          }
          //echo json_encode($data);
          //$resp = json_encode([$response_data]);
          $resp = json_encode($final_data);

          //save zapier log
          try{
            if(count($final_data) > 0) {
              foreach($final_data as $chat_info)
              {
                $zap_log = ZapierLogs::findOne(['chat_id' => $chat_info->transcript_id]);
                if(empty($zap_log)){
                  $zap_log = new ZapierLogs();
                  $zap_log->lead_id = $chat_info->id;
                  $zap_log->chat_id = $chat_info->transcript_id;
                  $zap_log->email = $chat_info->visitor_email;
                  $zap_log->type = 'lead';
                  $zap_log->request_id = $zapier_request_id;
                  $zap_log->user_group = $group;
                  $zap_log->created_at = time();
                  $zap_log->data = json_encode($chat_info);
                  $zap_log->save(false);
                }else{
                  $zap_log->updated_request_id = $zapier_request_id;
                  $zap_log->updated_at = time();
                  $zap_log->data = json_encode($chat_info);
                  $zap_log->save(false);
                }
              }
            }
          }catch(\Exception $e) {
            //zapier log saving failed
          }

          return $resp;
        }
      }else{
        return "Invalid Header";exit;
      }
    }

    public function actionZapierServiceChat()
    {
      $group = 0;
      $zapier_request_id = 0;
      $name = '';
      $email = '';
      $headers = Yii::$app->request->headers;
      //$headers->has('api_key');
      //mail("iamparam.surya@gmail.com","API subject", json_encode($_REQUEST));
      //$userData = User::findByEmail($_REQUEST['username']);
      if($headers->has('api-key')){
        $accept = $headers->get('api-key');

        //save zapier request log
        try{
          $zapier_request = new ZapierRequestLogs();
          $zapier_request->type = 'zapierservicechat';
          $zapier_request->api_key = $accept;
          $zapier_request->created_at = time();
          $zapier_request->save(false);
          $zapier_request_id = $zapier_request->id;
        }
        catch(\Exception $e) {

        }

        $userData = User::findOne(['auth_key'=>$accept]);
      if( !empty( $userData ) ){
        $group = $userData['user_group'];

        try {
          //update zapier_request_logs
          if($zapier_request_id > 0) {
            $zapier_request->authenticated = 1;
            $zapier_request->user_group = $group;
            $zapier_request->save(false);
          }
        }
        catch(\Exception $e) {

        }

      }else{
        echo "Invalid User";exit;
      }

      if($group > 0){
        try {
          // $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
          // if(!isset($_GET["date_from"])){
          //   $_GET['date_from'] = date('Y-m-d',strtotime("-1 days"));
          //   $_GET['date_to'] = date('Y-m-d');
          // }
          // $_GET['tag%5B%5D'] = 'service_chat';
          // $_GET["group"] = $group;
          // $API = new LiveChat_API();
          // $chats = $API->chats->get($page,$_GET);

          $old_timestamp = strtotime('-2 day');

          $queryChats = "SELECT chat.*
                  FROM `leads`
                  JOIN `chat` ON `chat`.`chat_id` = `leads`.`chat_id`
                  WHERE `leads`.`is_service_chat` = 1
                  AND `leads`.`user_group` = ".$group."
                  AND `chat`.`ended_timestamp` > ".$old_timestamp."
                  ORDER BY `chat`.`id` DESC LIMIT 15";

          $transcripts = Chat::findBySql($queryChats)->all();
        }
        catch (\Exception $e) {
          throw new NotFoundHttpException($e->getMessage());
        }

        //$transcripts = $chats->chats;

        try {
          //update zapier_request_logs
          if($zapier_request_id > 0) {
            $arr_fetched_chats = array();
            foreach($transcripts as $row => $transcript)
            {
              $arr_fetched_chats[] = $transcript->chat_id;
            }

            $zapier_request->chat_data = json_encode($arr_fetched_chats);
            $zapier_request->save(false);
          }
        }
        catch(\Exception $e) {

        }

        $data = [];
        $extra_info = NULL;
        $final_data = [];

        $lead_id = 0;
        $chat_id = '';
        $email_for_log = '';
        $count = 0;

        foreach($transcripts as $row => $transcript){
          $response_data = new \stdClass();

          $leadModel = Leads::find()->where(['chat_id' => $transcript->chat_id,
          'is_service_chat'=> 1])->one();

          if($leadModel && $leadModel->from_agent_app == 0){
            continue;
          }
          //echo "<pre>"; print_r($transcript); exit;

          if($leadModel){
            $lead_id = $leadModel->id;
            $chat_id = $transcript->chat_id;
            $email_for_log = $leadModel->visitor_email;

            $response_data->id = $lead_id;
            $response_data->transcript_id = $transcript->chat_id; //chat_id
            $response_data->visitor_name = ucfirst($leadModel->visitor_name) . ' ' . ucfirst($leadModel->visitor_last_name);
            $response_data->visitor_first_name = ucfirst($leadModel->visitor_name);
            $response_data->visitor_last_name = ucfirst($leadModel->visitor_last_name);
            $response_data->visitor_email = ucfirst($leadModel->visitor_email);
            $response_data->visitor_phone =$leadModel->visitor_phone;
            if(!empty($leadModel->custom_fields)){
              $custom_fields = json_decode($leadModel->custom_fields,true);
              //print_r($custom_fields);exit;
              $multipleChoiceString = '';
              foreach($custom_fields as $key => $custom_field){
                  $custom_table = CustomLeadFields::findOne($custom_field['field_id']);
                if($custom_field['field_type']=='textbox'){
                     $fieldvalue = $custom_field['field_value'];
                     $label1 = str_replace(' ', '_', htmlspecialchars_decode($custom_table->label));
                     $response_data->$label1 = urldecode($fieldvalue);
                }elseif ($custom_field['field_type']=='dropdown'){
                  $dropDownOption = CustomLeadFieldsOption::findOne($custom_field['field_value']);
                  $fieldvalue2 = $dropDownOption['option'];
                  $label2 = str_replace(' ', '_', htmlspecialchars_decode($custom_table->label));
                  $response_data->$label2 = urldecode($fieldvalue2);
                }elseif ($custom_field['field_type']=='multiple_choice'){
                  $custom_field_id_arrays = explode(',', urldecode($custom_field['field_value']));
                  $multipleChoiceOptions = CustomLeadFieldsOption::findAll($custom_field_id_arrays);
                  foreach($multipleChoiceOptions as $multipleChoiceOption){
                    $multipleChoiceString .= $multipleChoiceOption['option'].',';
                  }
                  $fieldvalue3 = rtrim($multipleChoiceString,', ');
                  $label3 = str_replace(' ', '_', htmlspecialchars_decode($custom_table->label));
                  $response_data->$label3 = urldecode($fieldvalue3);
                }
              }
              }
            $userCat = UserCategory::findOne($leadModel->category_id);
            $response_data->category = isset($userCat->category_name) ? $userCat->category_name : 'Not Set';
            $response_data->chat_summary = $leadModel->chat_summary;
          }else{
            echo "No New LEAD";exit;
            // $response_data->visitor_name = $transcript->visitor->name;
            // $response_data->visitor_first_name =  'Not Set';
            // $response_data->visitor_last_name = 'Not Set';
            // $response_data->visitor_email = $transcript->visitor->email;
          }

          $response_data->visitor_city = $transcript->city;
          $response_data->visitor_region = $transcript->region;
          $response_data->visitor_country = $transcript->country;
          $response_data->country_code = $transcript->country_code;
          $response_data->timezone = $transcript->timezone;

          //$chat_messages = $transcript->messages;
          $queryChatMessages = "SELECT chat_message.*
                  FROM `chat_message`
                  WHERE `chat_id` = $transcript->chat_id
                  ORDER BY `id` ASC";

          $chat_messages = ChatMessage::findBySql($queryChatMessages)->all();
          $formattedMessage = '';

          foreach ($chat_messages as $msg) {
            if($msg->user_type == 'agent'){
              $formattedMessage = $formattedMessage . "Agent: ".$msg->text. "\n";
            }

            if($msg->user_type == 'visitor'){
              $formattedMessage = $formattedMessage . "Visitor: ".$msg->text. "\n";
            }
          }

          $response_data->transcript_html = $formattedMessage;
          $response_data->agent_name = isset($transcript->agent_name) ? $transcript->agent_name : 'Not Set';
          $response_data->referrer = isset($transcript->referrer)?$transcript->referrer:'not-set';
          //echo '<pre>'; print_r($transcript); exit;
          // $chat_prechat_survey = $transcript->prechat_survey;
          // $chat_prechat_survey_data = [];
          // foreach ($chat_prechat_survey as $index => $survey_field) {
          //   $chat_prechat_survey_data[$survey_field->key] = $survey_field->value;
          // }
          //
          // $response_data->prechat_survey = $chat_prechat_survey_data;
          $response_data->chat_start_url = $transcript->chat_start_url;
          $response_data->chat_duration = $transcript->duration;

          //break;
          $final_data[] = $response_data;
          $count++;

          if($count >= 15) {
              break;
          }

          }
          //echo json_encode($data);
          //$resp = json_encode([$response_data]);
          $resp = json_encode($final_data);

          //save zapier log
          try{
            if(count($final_data) > 0) {
              foreach($final_data as $chat_info)
              {
                $zap_log = ZapierLogs::findOne(['chat_id' => $chat_info->transcript_id]);
                if(empty($zap_log)){
                  $zap_log = new ZapierLogs();
                  $zap_log->lead_id = $chat_info->id;
                  $zap_log->chat_id = $chat_info->transcript_id;
                  $zap_log->email = $chat_info->visitor_email;
                  $zap_log->type = 'service_chat';
                  $zap_log->request_id = $zapier_request_id;
                  $zap_log->user_group = $group;
                  $zap_log->created_at = time();
                  $zap_log->data = json_encode($chat_info);
                  $zap_log->save(false);
                }else{
                  $zap_log->updated_request_id = $zapier_request_id;
                  $zap_log->updated_at = time();
                  $zap_log->data = json_encode($chat_info);
                  $zap_log->save(false);
                }
              }
            }
          }catch(\Exception $e) {
            //zapier log saving failed
          }

          echo $resp;
        }
      }else{
        echo "Invalid Header";exit;
      }
    }

    public function actionAgentDetail()
    {
      $API = new LiveChat_API();
      //$agentDetail = $API->agents->getAgent($agentEmail);
      $chatDetail = $API->chats->getSingleChat('O7XTCOSXHK');
      echo "<pre>"; print_r($chatDetail); exit;
      //return json_encode($agentDetail);
    }

    public function actionMigrateData()
    {
      $API = new LiveChat_API();
      $chats = $API->chats->get('',[]);

      $chat_count = 0;
      $missed_chat_count = 0;
      $chat_message_count = 0;
      $lead_count = 0;
      $new_lead_count = 0;
      $pages = $chats->pages;
      $inital_chat_count = $chats->total;
      $other_type_count = 0;
      $otherTypeArr = [];
      echo memory_get_usage();
      echo "<br />";
      //echo memory_get_peak_usage();
      echo '<pre>'; print_r($chats); exit;
      //echo '<pre>'; print_r(array_reverse($chats->chats)); exit;

      echo "Migrating data for $chats->total chats and $chats->pages pages <br>";
      $set = 0;

      for($page = 500; $page >= 100 ; $page--)
      {
        $chats = $API->chats->get('',['page' => $page]);
        //echo '<pre>'; print_r($chats); exit;
        //$chats = array_reverse($chats->chats);
        //echo '<pre>'; print_r($chats); exit;
        // migration code //
        foreach ($chats->chats as $chat)
        {
           if($chat->id){
            $chatModel = Chat::findOne(['chat_id' => $chat->id]);
            if(empty($chatModel)){
              $chatModel = new Chat();
            }
          }
          //$chatModel = new Chat();

          if($chat->type == 'missed_chat'){
            $chatModel->type = Chat::TYPE_MISSED_CHAT;
            $chatModel->chat_id = $chat->id;
            $chatModel->name = json_encode($chat->visitor_name);
            $chatModel->email = $chat->email;
            $chatModel->user_group = $chat->group;
            $chatModel->started_timestamp = strtotime($chat->time);
            $chatModel->ended_timestamp = strtotime($chat->time);

            if($chatModel->save()){
              $chat_count++;
              $missed_chat_count++;
              $chatMessageModel = new ChatMessage();
              $chatMessageModel->chat_id = $chatModel->id;
              $chatMessageModel->author_name = $chatModel->name;
              $chatMessageModel->text = $chat->message;
              $chatMessageModel->date = $chat->time;
              $chatMessageModel->timestamp = strtotime($chat->time);
              $chatMessageModel->user_type = 'visitor';
              $chatMessageModel->save();
              $chat_message_count++;
            }

            continue;

          }else if($chat->type == 'chat'){
            $chatModel->type = Chat::TYPE_CHAT;
          }else{
            $other_type_count++;
            $otherTypeArr[] = $chat->type;
            continue;
          }

          $chatModel->chat_id = $chat->id;
          $chatModel->name = isset($chat->visitor->name) ? json_encode($chat->visitor->name) : '';
          $chatModel->email = isset($chat->visitor->email) ? $chat->visitor->email : '';
          $chatModel->city = isset($chat->visitor->city) ? $chat->visitor->city : '';
          $chatModel->region = isset($chat->visitor->region) ? $chat->visitor->region : '';
          $chatModel->country = isset($chat->visitor->country) ? $chat->visitor->country : '';

          $chatModel->agent_name = isset($chat->agents[0]->display_name) ? $chat->agents[0]->display_name : '';
          $chatModel->agent_email = isset($chat->agents[0]->email) ? $chat->agents[0]->email : '';

          $chatModel->rate = $chat->rate;
          $chatModel->duration = $chat->duration;
          $chatModel->chat_start_url = isset($chat->chat_start_url) ? $chat->chat_start_url : '';
          $chatModel->user_group = isset($chat->group[0]) ? $chat->group[0] : 0;
          $chatModel->tags = json_encode($chat->tags);
          $chatModel->is_lead = 0;
          $chatModel->started_timestamp = $chat->started_timestamp;
          $chatModel->ended_timestamp = $chat->ended_timestamp;

          foreach ($chat->tags as $tag) {
            if($tag == 'lead'){
              $chatModel->is_lead = 1;
            }
          }

          if($chatModel->is_lead){
            $leadModel = Leads::findOne(['chat_id' => $chat->id]);
            if($leadModel){
              $chatModel->leads_id = $leadModel->id;
            }else{
              $leadModel = new Leads();
              $leadModel->user_id = 0;
              $leadModel->user_group = $chatModel->user_group;
              $leadModel->chat_id = $chatModel->chat_id;
              $leadModel->visitor_email = $chatModel->email;
              $leadModel->visitor_name = $chatModel->name;
              $leadModel->tags = $chatModel->tags;
              $leadModel->status = 'pending';
              $leadModel->email = 1;
              $leadModel->c_time = $chatModel->ended_timestamp;
              $leadModel->created_by = $chatModel->agent_email;
              $leadModel->new_lead = 0;
              $leadModel->from_agent_app = 0;
              $leadModel->save();
              $chatModel->leads_id = $leadModel->id;
              $new_lead_count++;
            }
            $lead_count++;
          }
          if($chatModel->save()){
            $chat_count++;
            if($chatModel->id){
              $chatMessageModel = ChatMessage::findOne(['chat_id' => $chatModel->id]);
              if(empty($chatMessageModel)){
                foreach ($chat->messages as $message)
                {
                    $chatMessageModel = new ChatMessage();
                    $chatMessageModel->chat_id = $chatModel->id;
                    $chatMessageModel->author_name = $message->author_name;
                    $chatMessageModel->text = $message->text;
                    $chatMessageModel->date = $message->date;
                    $chatMessageModel->timestamp = $message->timestamp;
                    $chatMessageModel->user_type = $message->user_type;
                    $chatMessageModel->save();
                    //unset($chatMessageModel);
                    $chatMessageModel = NULL;
                    $chat_message_count++;
                }
              }
            }
            //unset($chatModel);
            $chatModel= NULL;
          }
        }
        $set++;
        // migration code //
         echo "Page count: $page   Chat count : $chat_count   Chatmessage count : $chat_message_count Lead count: $lead_count NewLead count: $new_lead_count Set $set<br>";
      }
      echo memory_get_usage();
      echo '<br />';
      echo 'Migration successful';
      exit;
    }

    public function actionSaveChat()
    {
      $data = file_get_contents('php://input');
      $data = json_decode($data);

      if($data->event_type == 'chat_ended')
      {

        if($data->chat->id){
          $chatModel = Chat::findOne(['chat_id' => $data->chat->id]);
          if(empty($chatModel)){
            $chatModel = new Chat();
          }
        }
        //$chatModel = new Chat();

        $chatModel->chat_id = $data->chat->id;
        $chatModel->name = isset($data->visitor->name) ? json_encode($data->visitor->name) : '';
        $chatModel->email = isset($data->visitor->email) ? $data->visitor->email : '';
        $chatModel->city = isset($data->visitor->city) ? $data->visitor->city : '';

        // call API to save region//
        $API = new LiveChat_API();
        $chatDetail = $API->chats->getSingleChat($data->chat->id);
        // call API to save region//

        $chatModel->region = $chatDetail->visitor->region;
        $chatModel->country = isset($data->visitor->country) ? $data->visitor->country : '';

        $chatModel->agent_name = isset($data->chat->agents[0]->name) ? $data->chat->agents[0]->name : '';
        $chatModel->agent_email = isset($data->chat->agents[0]->login) ? $data->chat->agents[0]->login : '';

        $chatModel->rate = $chatDetail->rate;
        $chatModel->duration = $chatDetail->duration;
        $chatModel->chat_start_url = $data->chat->url;
        $chatModel->user_group = isset($data->chat->groups[0]) ? $data->chat->groups[0] : 0;
        $chatModel->tags = json_encode($data->chat->tags);
        $chatModel->is_lead = 0;
        $chatModel->started_timestamp = $chatDetail->started_timestamp;
        $chatModel->ended_timestamp = $chatDetail->ended_timestamp;
        $chatModel->country_code = $chatDetail->visitor->country_code;
        $chatModel->timezone = $chatDetail->visitor->timezone;
        //$chatModel->referrer = '';

        foreach ($data->chat->tags as $tag) {
          if($tag == 'lead'){
            $chatModel->is_lead = 1;
          }
        }

        $leadModel = Leads::findOne(['chat_id' => $data->chat->id]);
        if($leadModel){
          $chatModel->leads_id = $leadModel->id;
          $chatModel->is_lead = 1;
        }

        if($chatModel->save()){
          if($chatModel->id){
            $chatMessageModel = ChatMessage::findOne(['chat_id' => $chatModel->id]);
            if(empty($chatMessageModel)){
              foreach ($data->chat->messages as $message)
              {
                $chatMessageModel = new ChatMessage();
                $chatMessageModel->chat_id = $chatModel->id;
                $chatMessageModel->author_name = $message->author_name;
                $chatMessageModel->text = $message->text;
                $chatMessageModel->date = date('D, m/d/y h:i:s a', $message->timestamp);
                $chatMessageModel->timestamp = $message->timestamp;
                $chatMessageModel->user_type = $message->user_type;
                $chatMessageModel->save();
              }
            }
          }
        }
      }
    }

    public function actionSendToMeServiceChat()
    {
      $user = User::findOne(['id' => Yii::$app->user->identity->id]);
      $chat_id =  Yii::$app->request->post('chat_id');
      $showLead = Yii::$app->request->post('show_lead');

      $user_email = $user->email;

      $leadModel = Leads::find()->where(['chat_id' => $chat_id])->one();
      if(!$leadModel){
        return ['success' => false, 'message' => 'Lead does not exist'];
      }
      try {
        $API = new LiveChat_API();
        $chat2 = $API->chats->getSingleChat($chat_id);
      }
      catch (Exception $e) {
        throw new NotFoundHttpException($e->getMessage());
      }
      //$showLead = 1; // hardcode
      $name = $leadModel->visitor_name;
      $email = $leadModel->visitor_email;
      $forwardEmail = $user_email;

      $sent = Leads::sendServicechatAllEmail($user, $chat2, $showLead, $name, $email, $leadModel, $forwardEmail);

      if($sent){
        $emailLog = new EmailLog();
        $emailLog->leads_id = $leadModel->id;
        $emailLog->chat_id = $leadModel->chat_id;
        $emailLog->sent_to = $forwardEmail;
        $emailLog->sent_via = EmailLog::VIA_SEND_TO_ME;
        $tempUser = User::findOne(['user_group' => $leadModel->user_group]);
        $leadModel->email = 1;
        if($tempUser){
          $emailLog->web_url = $tempUser->website_url;
        }
        $emailLog->save();
        $leadModel->save();
        return json_encode(['success' => true, 'message' => 'Email has been sent successfully to ' . $user->email]);
      }
      return json_encode(['success' => false, 'message' => 'Some problem occurred! Please try again later after sometime']);
    }

    public function actionSendToMe()
    {
      $user = User::findOne(['id' => Yii::$app->user->identity->id]);
      $chat_id =  Yii::$app->request->post('chat_id');
      $showLead = Yii::$app->request->post('show_lead');

      $user_email = $user->email;

      $leadModel = Leads::find()->where(['chat_id' => $chat_id])->one();
      if(!$leadModel){
        return ['success' => false, 'message' => 'Lead does not exist'];
      }

      try {
        $API = new LiveChat_API();
        $chat2 = $API->chats->getSingleChat($chat_id);
      }
      catch (Exception $e) {
        throw new NotFoundHttpException($e->getMessage());
      }

      //$showLead = 1; // hardcode
      $name = $leadModel->visitor_name;
      $email = $leadModel->visitor_email;
      $forwardEmail = $user_email;

      $sent = Leads::sendForwardingEmail($user, $chat2, $showLead, $name, $email, $leadModel, $forwardEmail);

      if($sent){
        $emailLog = new EmailLog();
        $emailLog->leads_id = $leadModel->id;
        $emailLog->chat_id = $leadModel->chat_id;
        $emailLog->sent_to = $forwardEmail;
        $emailLog->sent_via = EmailLog::VIA_SEND_TO_ME;
        $tempUser = User::findOne(['user_group' => $leadModel->user_group]);
        if($tempUser){
          $emailLog->web_url = $tempUser->website_url;
        }
            $leadModel->email = 1;
            $leadModel->save();
        $emailLog->save();
        return json_encode(['success' => true, 'message' => 'Email has been sent successfully to ' . $user->email]);
      }
      return json_encode(['success' => false, 'message' => 'Some problem occurred! Please try again later after sometime']);
    }

    public function actionListing()
    {
      $API = new LiveChat_API();
      $chats = $API->chats->get('',[]);
      echo '<pre>'; print_r($chats); exit;
    }

    public function actionTrackerid()
    {
        //if(User::find()->where( [ 'email' => $row['email'] ] )->exists())
        $users = User::find()->where( [ 'tracker_id' => NULL] )->all();
        //echo '<pre>'; print_r($users); exit;
        if( !empty( $users ) ){
          foreach($users as $user){
            $tracker_id = rand(00000000,99999999);
            while(User::find()->where( [ 'tracker_id' => $tracker_id ] )->exists()){
              $tracker_id = rand(00000000,99999999);
            }
            if(!User::find()->where( [ 'tracker_id' => $tracker_id ] )->exists()){
              return  $tracker_id;

            }
          }
          echo 'user tracker updated successfully';
        }
    }
}
