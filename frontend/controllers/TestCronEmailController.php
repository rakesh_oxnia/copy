<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\Subscription;
use common\models\User;
use common\models\LiveChat_API;

class TestCronEmailController extends Controller
{
      public function actionSendEmailFiveMinutes()
      {
        $user = User::find()
                        ->where(["id"=>1])
                        ->all();

        $name = $user[0]->username;
        try {
          $sendMail = Subscription::sendTestCronEmail('deepak@webcubictechnologies.com',$name);
        }
        catch(Exception $e) {
          //do nothing
        }
      }

    public function actionTags()
    {
        try {

            $API = new LiveChat_API();
            $tags = $API->chats->getAllTags();
            print_r($tags);
        }catch(\Exception $e) {
            print_r($e);
        }
    }

    public function actionTest()
    {
      $is_service_chat = 0;
      $API = new LiveChat_API();
      $leadChat = $API->chats->getSingleChat('POP85LG07C');
      $tags = $leadChat->tags;
      print_r($tags);
      $new_tag = array();
      foreach($tags as $tag) {
        if($tag != 'lead' && $tag != 'Service Chat') {
          $new_tag[] = $tag;
        }
      }
      echo '++++++++++++';
      print_r($new_tag);
      if($is_service_chat == 1) {
        $new_tag[] = 'Service Chat';
      }else {
        $new_tag[] = 'lead';
      }
      echo '----------';
      print_r($new_tag);

      $t = $API->chats->updateTags('POP85LG07C', $new_tag);
      echo '<pre>';
      print_r($t);
    }
}
