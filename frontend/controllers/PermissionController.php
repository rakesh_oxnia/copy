<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\User;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Permissions; 

class PermissionController extends Controller
{
	
	public function actionIndex(){
		$this->layout = 'dashboard';
		$success = "";	
		$model = Permissions::findOne(1);
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
				$success = "Permissions has been added successfully.";
		}
		return $this->render('index', ['success' => $success, 'model' => $model]);
	}
	
	
	
}