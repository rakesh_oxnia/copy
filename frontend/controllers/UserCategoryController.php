<?php

namespace frontend\controllers;

use Yii;
use common\models\UserCategory;
use common\models\User;
use common\models\UserCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * UserCategoryController implements the CRUD actions for UserCategory model.
 */
class UserCategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'delete-categories', 'bulk-create'],
                        
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('manageCategories');                     
                        }                     
                    ],
                    [
                        'allow' => true,
                        'actions' => ['switch-to-admin'],
                        'roles' => ['@']                 
                    ]                     
                ],
            ],           
        ];
    }


    public function beforeAction($action)
    {
        if($action->id == 'switch-to-admin')
       {
          return parent::beforeAction($action);
       }

        if(!parent::beforeAction($action)) {
            return false;
        }

        LoginForm::userAssitLogin();

        if(Yii::$app->user->identity->role == User::ROLE_SUBADMIN) {     
            $user_id = Yii::$app->user->identity->id;
            $parent = User::findOne(Yii::$app->user->identity->parent_id);   
            yii::$app->user->login($parent);
            Yii::$app->session->set('user_subadmin', $user_id);       
        }

        if(Yii::$app->user->isGuest){
          return $this->redirect(['site/login']);
        }

        if ($action->id == 'delete') {
            $this->enableCsrfValidation = false;
        }

        return true;
    }

    public function render($view, $params)
    {
      $params = LoginForm::userAssitLogout($params);

        if(Yii::$app->session->has('user_subadmin')) { 
          $user = User::findOne(Yii::$app->session->get('user_subadmin'));  
          Yii::$app->session->remove('user_subadmin');             
          yii::$app->user->login($user);             
        }

        // before render
        return parent::render($view, $params);
    }

    /**
     * Lists all UserCategory models.
     * @return mixed
     */
    public function actionIndex($user_id = 0)
    {
        $this->layout = 'kosmoouter'; 
        $searchModel = new UserCategorySearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $categories = UserCategory::findAll(['user_id' => Yii::$app->user->identity->id, 'parent_id' => NULL]);
        $clientModel = User::find()->andFilterWhere([ 'or',
                           ['=', 'role', User::ROLE_USER],
                           ['=', 'role', User::ROLE_FRANCHISE],
                           ['=', 'role', User::ROLE_OUTLET],
                           ['=', 'role', User::ROLE_AGENCY],
                      ])->all();

        $clientArray = ArrayHelper::map($clientModel, 'id', 'email');

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'categories' => $categories,
            'clientArray' => $clientArray,
            'user_id' => $user_id,
        ]);
    }

    /**
     * Displays a single UserCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->layout = 'kosmoouter';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->layout = 'kosmoouter';
        $model = new UserCategory;
        $categories = UserCategory::findAll(['user_id' => Yii::$app->user->identity->id, 'parent_id' => NULL]);
       
       if(Yii::$app->request->post()) {
            $params = Yii::$app->request->post(); 
            $params['UserCategory']['user_id'] = Yii::$app->user->identity->id;
            $params['UserCategory']['parent_id'] = empty($params['UserCategory']['parent_id']) ? NULL : $params['UserCategory']['parent_id'];

            if ($model->load($params) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }            
       }
        
        return $this->render('create', [
                'model' => $model,
                'categories' => $categories,
        ]);
    }

    /**
     * Updates an existing UserCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->layout = 'kosmoouter';
        $model = $this->findModel($id);
        $categories = UserCategory::findAll(['user_id' => Yii::$app->user->identity->id, 'parent_id' => NULL]);

        $params = Yii::$app->request->post(); 
        unset($params['UserCategory']['user_id']);

        if ($model->load($params) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'categories' => $categories,
            ]);
        }
    }

    /**
     * Deletes an existing UserCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSwitchToAdmin()
    {
      $session = Yii::$app->session;
      //echo '<pre>'; print_r(Yii::$app->user->identity); exit;
      if(User::LoginAsAdmin($session['admin_id'])){
        unset($session['admin_id']);
        unset($session['admin_email']);

        return $this->redirect(['site/index']);
      }
    }



    /**
    * This function deletes all categories that are submitted though ajax
    * @params  categories post array
    * @return json string
    **/

    public function actionDeleteCategories()
    {
      $categories = Yii::$app->request->post('categories');
      $count = 0;
      foreach ($categories as $cat_id) {
        $userCategory = UserCategory::findOne($cat_id);
        if($userCategory->delete()){
            $count++;
        }
      }
      return json_encode(['success' => true, 'count' => $count]);
    }

     /**
    * This function is used to create bulk categories for a particular user via Admin
    * @params  categories post array
    * @return json string
    **/


    public function actionBulkCreate($user_id)
    {
        if(Yii::$app->user->identity->role != User::ROLE_ADMIN){
            //throw new NotFoundHttpException('The requested page does not exist.');
        }

        if(Yii::$app->session->has('user_assist')) {
          $user_id = Yii::$app->user->identity->id;
        }

        $this->layout = 'kosmoouter';
        $categories = UserCategory::findAll(['parent_id' => NULL, 'user_id' => $user_id]);
        $userModel = User::findOne($user_id);
        $catArray = [];
        foreach ($categories as $category) {
            $catArray[$category->id] = $category->category_name;
        }

        $clientModel = User::find()->andFilterWhere([ 'or',
                           ['=', 'role', User::ROLE_USER],
                           ['=', 'role', User::ROLE_FRANCHISE],
                           ['=', 'role', User::ROLE_OUTLET],
                           ['=', 'role', User::ROLE_AGENCY],
                      ])->all();

        $clientArray = ArrayHelper::map($clientModel, 'id', 'email');

        if ($post = Yii::$app->request->post()) {
            //echo "<pre>"; print_r($post); exit;
            $catName = $post['categoryName'];
            $catValue = $post['categoryValue'];
            $parentCat = $post['parentCategory'];
            foreach ($catName as $key => $value) {
                $userCategory = new UserCategory;
                $userCategory->user_id = $user_id;
                $userCategory->parent_id = $parentCat[$key] == 0 ? NULL : $parentCat[$key];
                $userCategory->category_name =  $catName[$key];
                $userCategory->value = $catValue[$key];
                $userCategory->save();
            }

            // return $this->render('index', [
            //     'user_id' => $user_id,
            // ]);

            return $this->redirect(['index', 'user_id' => $user_id]);

        } else {
            return $this->render('bulk-create', [
                'categories' => $categories,
                'catArray' => $catArray,
                'clientArray' => $clientArray,
                'user_id' => $user_id,
                'userModel' => $userModel,
            ]);
        }
    }
}
