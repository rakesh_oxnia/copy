<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\User;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LiveChat_API;
use yii\helpers\Url;

/**
 * Site controller
 */
class ApiController extends Controller
{

	public function beforeAction($action)
    {
    	$this->enableCsrfValidation = false;

    	if(isset($_GET['wp'])) {
    		Yii::$app->session->set('wp_signup', $_GET['wp']); 
    	}
        
        return parent::beforeAction($action);
    }

     /*Client Login With Wordpress*/
     public function actionSuccess(){
		$this->layout = 'kosmologin';
		$msg		  = "";
		if(!(isset($_GET['name'])) || !(isset($_GET['email'])) ){
			print_r("Bad request");
			exit;
		}
		$useremail = User::find()->where(['email' => $_GET['email']])->one();
		if( empty( $useremail ) ){
		    //return 1;
			$user = new User();
			$user->website_url = '';
			$user->user_group = 0;
			$user->setDefaultValues();
			$password = rand();
            $user->username = $_GET['name'];
			$user->your_name = $_GET['name'];
            $user->email = $_GET['email'];
            $user->role = 10;
            $user->setPassword( $password );
            $user->generateAuthKey();
            
            if ($user->save()) {
                //return $user->id;
                \Yii::$app->mailer->compose(['html' => 'usernamePasswordsend-html', 'text' => 'usernamePasswordsend-text'], ['user' => $user, "password" => $password])
                                ->setFrom([\Yii::$app->params['supportEmail'] => 'Chat Metrics'])
                                ->setTo($_GET['email'])
                                ->setSubject('Welcome to your Dashboard ') // user create mail subject
                                ->send();

				$model 		  = new LoginForm();
				$model->email = $_GET['email'];
				$model->password = "$password";
				if($model->login()){
					/* If sign up happens from Wordpress plugin, activate the plugin first. */
					if(Yii::$app->session->has('wp_signup')) { 
					    $user->signup_key = Yii::$app->security->generateRandomString();
					    $user->save();
					    $this->notify_team_new_signup(); 
					    $data =Yii::$app->params['baseUrl']."embed/index.php?tracker_id=".$user->tracker_id."";
				        $response = ['success' => 1, 'message' => $data, 'api_key' => $user->signup_key, 'email' => $user->email];
				        
				        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
						return $response;
					}
					return $this->redirect(['/install/step']);
				}else{
					$msg = "Some Problem has occured. Please try Later!!";
				}
            }else{
					$msg = "Some Problem has occured. Please try Later!!";
				}
		}else{
			$msg = "User Email already exist in database";
		}
		return $this->render('success', ['msg' => $msg]);
	}
     /*Client Login With Wordpress*/


	public function actionLogin()
	{
		$email = Yii::$app->request->post('email');
		$password = Yii::$app->request->post('password');

		if(!$email){
            throw new BadRequestHttpException("Email cannot be left blank");
		}

		if(!$password){
			throw new BadRequestHttpException("Password cannot be left blank");
		}

		$userModel = User::findOne(['email' => $email]);

		if($userModel){

			if($userModel->validatePassword($password)){
				$userArray = User::find()->where(['id' => $userModel->id])->asArray()->one();
				unset($userArray['name']);
				unset($userArray['iframe_tag']);
				unset($userArray['login_bg_color']);
				unset($userArray['password_hash']);
        		unset($userArray['password_reset_token']);
				$response = ['success' => 1, 'message' => 'Login successful', 'data' => $userArray];
			}else{
				$response = ['success' => 0, 'message' => 'Password is wrong'];
			}
		}else{
			$response = ['success' => 0, 'message' => 'Email not registered'];
		}
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $response;
	}

	public function actionLoadJs()
    {
      $tracker_id =  Yii::$app->request->get('tracker_id');
      $userModel = User::findOne(['tracker_id' => $tracker_id]);
      if(!$userModel){
        return "alert('Invalid tracker id. Please contact system administrator');";
      }

      if($userModel->status != User::STATUS_ACTIVE) {
            return false;
      }

      return "
     window.__lc = window.__lc || {};
     window.__lc.license = ".Yii::$app->params['live_chat_license_key'].";
     window.__lc.ga_version;
     window.__lc.chat_between_groups = false;
     window.__lc.params = [
     { name: 'Business Name', value: '".$userModel->business_name."' },
     { name: 'Group Id', value: '".$userModel->user_group."' },
     { name: 'Client Id', value: '".$userModel->id."' }
     ];
     (function() {
       var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
       lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
     })();
    ";
    }

    public function actionGetTrackingCode(){
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		$email = Yii::$app->request->post('email');
		$password = Yii::$app->request->post('password');

		if(!$email){
			$response = ['success' => 0, 'message' => 'Email cannot be left blank'];
			return $response;
			//throw new BadRequestHttpException("Email cannot be left blank");
		}

		if(!$password){
			$response = ['success' => 0, 'message' => 'Password cannot be left blank'];
			return $response;
			//throw new BadRequestHttpException("Password cannot be left blank");
		}

		$userModel = User::findOne(['email' => $email]);
		if(!$userModel){
			$response = ['success' => 0, 'message' => 'User Does not Exist'];
			return $response;
			//throw new BadRequestHttpException("User Does not Exist");
		}

		if($userModel->email_verified != 1){
			$response = ['success' => 0, 'message' => 'Email not verified'];
			//throw new BadRequestHttpException("Email not verified");
			return $response;
		}

		if($userModel){
			if($userModel->validatePassword($password)){
				$data =Yii::$app->params['baseUrl']."embed/index.php?tracker_id=".$userModel->tracker_id."";
				$response = ['success' => 1, 'message' => $data];
			 return $response;
			}else{
				$response = ['success' => 0, 'message' => 'Invalid Password'];
				return $response;
			}
		}else{
			$response = ['success' => 0, 'message' => 'Email not registered'];
			return $response;
		}
	}

	private function notify_team_new_signup() {

		$html 		= "Hi Team,<br><br>";
		$html 		= $html . "Exciting, We have received another new signup to Chat Metrics.<br>";
		$html 		= $html . "Let's get them setup for more leads right away.<br><br>";
		$html 		= $html . "Client details:<br>";
		$html 		= $html . "Email : " . Yii::$app->user->identity->email ." <br>";
		$html 		= $html . "<a href='". Url::to(['user/view', 'id' => Yii::$app->user->id], 'https')  ."'><input type='button' style='color:green;' value='VIEW CLIENT DETAILS'></a>";		
		
	    $model = User::findOne(Yii::$app->user->id);
			Yii::$app->mailer->compose()
			->setTo(Yii::$app->params['supportEmail'])
			->setFrom([\Yii::$app->params['supportEmail'] => 'Chat Metrics']) // mail from
			->setSubject("Hurray! New Chat Metrics Sign Up from WP plugin.")
			->setHtmlBody( $html )
			->send();		
	}

}
