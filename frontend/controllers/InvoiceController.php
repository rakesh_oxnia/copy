<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\User;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Packages;
use frontend\models\Invoices;
use app\models\Leads;
use common\models\LiveChat_API;
use common\models\CustomPackageRange;
use common\models\PackageUser;
use frontend\models\Invoice;
use common\models\StripeLog;
use app\models\Subscription;
//use mPDF;
/**
 * Invoice controller
 */

class InvoiceController extends Controller
{
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'except' => ['test', 'update-invoice-status'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'print-pay-as-you-go-pdf', 'invoice-listing',
                        			'print-pdf', 'failed-listing', 'success', 'paybysavedcard', 'payperlead',
                        			'payperleadbysavedcard', 'pay-custom-package-by-saved-card'],
                        'roles' => ['@'],
                    ],
                     [
                        'allow' => true,
                        'actions' => ['savecard'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

     public function beforeAction($action)
    {
        if(!parent::beforeAction($action)) {
            return false;
        }

		LoginForm::userAssitLogin();


        return true;
    }

    public function render($view, $params)
    {
		$params = LoginForm::userAssitLogout($params);
        // before render
        return parent::render($view, $params);
    }

	public function actionIndex(){
		$this->layout = 'kosmoouter';
		$success	  = "";
		$error		  = "";



		$this->layout = 'kosmleadsandchats';
		if(in_array(Yii::$app->user->identity->role, [User::ROLE_ADMIN, User::ROLE_SUBADMIN])) {
			$invoices	  = Invoices::find()->orderBy('id DESC')->all();

			return $this->render('index', ['success' => $success, 'error' => $error, 'invoices' => $invoices]);
		}else{
			$invoices	  = Invoices::find()->where(['user_id' => Yii::$app->user->id])->orderBy('id DESC')->all();

			return $this->render('index', ['success' => $success, 'error' => $error, 'invoices' => $invoices]);
		}
	}

	public function actionPrintPdf(){
     $invoice_id = Yii::$app->request->get('id');
		 $invoice = Invoice::findOne($invoice_id);
		 $userData 	 = User::findOne($invoice->user_id);
		 if($invoice->gst_percent!=0){
			 $gst = '<tr>
					 <td colspan="3"></td>
					 <td class="mono">GST '.$invoice->gst_percent.'%</td>
					 <td class="mono">'.$invoice->gst_charge.'</td>
			 </tr>';
		 }else{
			 $gst = '';
		 }
      if($invoice->invoice_status==1){
				$status = 'Paid';
			}else{
				$status = '';
			}
		 if($invoice->extra_lead_consumed>0){
			 $extra = '<tr>
					 <td style="text-align:left; padding-left:10px; width:8%;">2</td>
					 <td style="text-align:left; padding-left:10px;">Extra Lead<br />
					 <td class="mono" style="width:15%;">'.$invoice->extra_lead_consumed.'</td><td style="width:15%;" class="mono">'.$invoice->extra_per_lead_amount.'</td>
					 <td style="width:15%;" class="mono">'.$invoice->extra_lead_consumed*$invoice->extra_per_lead_amount.'</td>
			 </tr>';
		 }else{
			 $extra = '';
		 }
		 //echo '<pre>';print_r($invoice->packageDetails);exit;
		 if($invoice->advance_payment_status==1){
			 $table = '<tr>
					 <td style="text-align:left; padding-left: 10px; width:8%;">1</td>
					 <td style="text-align:left; padding-left:10px;">'.$invoice->packageDetails->package_name.'<br /></td>
					 <td class="mono" style="width:15%;">'.$invoice->allocated_lead.'</td><td style="width:15%;" class="mono">'.$invoice->advanced_payment.'</td>
					 <td style="width:15%;" class="mono">'.$invoice->remaining_days_total_amount.'</td>
			 </tr>
			 <tr>
					 <td colspan="3"></td>
					 <td></td>
					 <td></td>
			 </tr>
			 '.$extra.''.$gst;
		 }else{
			 $table = '
			 <tr>
					 <td style="text-align:left; padding-left: 10px; width:8%;">1</td>
					 <td style="text-align:left; padding-left:10px;">'.$invoice->packageDetails->package_name.'<br /></td>
					 <td class="mono" style="width:15%;">'.$invoice->allocated_lead.'</td><td style="width:15%;" class="mono">'.$invoice->advanced_payment.'</td>
					 <td style="width:15%;" class="mono">'.round($invoice->remaining_days_total_amount).'</td>
			 </tr>
			 <tr>
					 <td colspan="3"></td>
					 <td></td>
					 <td></td>
			 </tr>
			 '.$extra.''.$gst;
		 }

     if($invoice->invoice_status==0 && $invoice->automatic_charge_status!=1){
         //$duedate = '<h3>Due Date: '.date('d-F-Y',strtotime('+4 days', $invoice->created_at)).'</h3><p>Electronic Payment Preferred. Please use the Invoice number in the reference to ensure correct allocation of your payment.<br />Thank you for your custom.</p><p>Westpac</p><p>BSB: 033120</p><p>A/C#: 361629</p>';

				 $duedate='';
		 }else{
         $duedate='';
		 }

        //require_once('../mpdf_mpdf_7.1.4.0_require');
       // require_once '../mpdf_mpdf_7.1.4.0_require/vendor/autoload.php';
        $mpdf = new \Mpdf\Mpdf();
		 //$mpdf = new \Mpdf\Mpdf();
		 $mpdf->SetDisplayMode('fullpage');
		 $mpdf->list_indent_first_level = 0;
     $mpdf->WriteHTML('<!DOCTYPE html>
		 <html>
		 <head>
		     <title>Print Invoice</title>
		     <style>
		         *
		         {
		             margin:0;
		             padding:0;
		             font-family:Arial;
		             font-size:10pt;
		             color:#000;
		         }
		         body
		         {
		             width:100%;
		             font-family:Arial;
		             font-size:10pt;
		             margin:0;
		             padding:0;
		         }

		         p
		         {
		             margin:0;
		             padding:0;
		         }

		         #wrapper
		         {
		             width:150mm;
		             margin:0 15mm;
		         }

		         .page
		         {
		             height:297mm;
		             width:210mm;
		             page-break-after:always;
		         }

		         table
		         {
		             border-left: 1px solid #ccc;
		             border-top: 1px solid #ccc;

		             border-spacing:0;
		             border-collapse: collapse;

		         }

		         table td
		         {
		             border-right: 1px solid #ccc;
		             border-bottom: 1px solid #ccc;
		             padding: 2mm;
		         }

		         table.heading
		         {
		             height:50mm;
		         }

		         h1.heading
		         {
		             font-size:14pt;
		             color:#000;
		             font-weight:normal;
		         }

		         h2.heading
		         {
		             font-size:9pt;
		             color:#000;
		             font-weight:normal;
		         }

		         hr
		         {
		             color:#ccc;
		             background:#ccc;
		         }

		         #invoice_body
		         {
		             height: 80mm;
		         }

		         #invoice_body , #invoice_total
		         {
		             width:100%;
		         }
		         #invoice_body table , #invoice_total table
		         {
		             width:100%;
		             border-left: 1px solid #ccc;
		             border-top: 1px solid #ccc;

		             border-spacing:0;
		             border-collapse: collapse;

		             margin-top:5mm;
		         }

		         #invoice_body table td , #invoice_total table td
		         {
		             text-align:center;
		             font-size:9pt;
		             border-right: 1px solid #ccc;
		             border-bottom: 1px solid #ccc;
		             padding:2mm 0;
		         }

		         #invoice_body table td.mono  , #invoice_total table td.mono
		         {
		             text-align:left;
		             padding-right:3mm;
								 padding-left: 2mm;
		             font-size:10pt;
		         }

		         #footer
		         {
		             width:180mm;
		             margin:0 15mm;
		             padding-bottom:3mm;
		         }
		         #footer table
		         {
		             width:100%;
		             border-left: 1px solid #ccc;
		             border-top: 1px solid #ccc;

		             background:#eee;

		             border-spacing:0;
		             border-collapse: collapse;
		         }
		         #footer table td
		         {
		             width:25%;
		             text-align:center;
		             font-size:9pt;
		             border-right: 1px solid #ccc;
		             border-bottom: 1px solid #ccc;
		         }
		     </style>
		 </head>
		 <body>
		 <div id="wrapper">

		     <p style="text-align:center; font-weight:bold; padding-top:5mm;">
				 </p>
				 <p style="text-align:center; font-weight:bold; padding-top:5mm;">
				 <!--<img src="/assets/CM-logo-new.png" width="200" />-->
				 <img src="/assets/CM-logo-new-dark.svg" width="200" />
				 </p>
		     <br />
		     <table class="heading" style="width:100%;">
		         <tr>
		             <td style="width:80mm;">
		                 <h1 class="heading">TAX INVOICE</h1>
		                 <h2 class="heading">
												'.$userData->business_name.'<br />
												'.$userData->email.'<br />
												'.$userData->website_url.'<br />

		                 </h2>
		             </td>
		             <td rowspan="2" valign="top" align="left" style="padding:3mm;">
		                 <table>
		                     <tr><td>Invoice No </td><td>'.$invoice->id.'</td></tr>
		                     <tr><td>Package Activation Date </td><td>'.date('Y-m-d',$invoice->subscriptionDetail->created_at).'</td> </tr>
												 <tr><td>Invoice Date </td><td>'.date('Y-m-d',  $invoice->created_at).'</td></tr>
		                     <tr><td>Currency </td><td>'.$invoice->transaction_currency.'</td></tr>
		                 </table>
		             </td>
		         </tr>
		         <tr>
		             <td>
		                 <b>Chat Metrics</b> :<br />
		                 677 Victoria St<br />
		             ABBOTSFORD VIC 3062
		                 <br />
		                 AUSTRALIA<br />
		             </td>

		         </tr>
		     </table>


		     <div id="content">

		         <div id="invoice_body">
		             <table>
		             <tr style="background:#eee;">
		                 <td style="width:8%;"><b>Sl. No.</b></td>
		                 <td><b>Package</b></td>
		                 <td style="width:15%;"><b>Quantity</b></td>
		                 <td style="width:15%;"><b>Rate</b></td>
		                 <td style="width:15%;"><b>Total</b></td>
		             </tr>
		             </table>

		             <table>
                  '.$table.'
		             <tr>
		                 <td colspan="3"></td>
		                 <td class="mono">Total :</td>
		                 <td class="mono">'.number_format($invoice->total_amount,2).'</td>
		             </tr>
		         </table>
		         </div>
		         <div id="invoice_total">
		             Total Amount :
		             <table>
		                 <tr>
		                     <td style="text-align:left; padding-left:10px;">'.$status.'</td>
		                     <td style="text-align:left; padding-left:10px; width:15%;">'.$invoice->transaction_currency.'</td>
		                     <td style="width:15%;" class="mono">'.number_format($invoice->total_amount,2).'</td>
		                 </tr>
		             </table>
		         </div>
		         <br />
		         <hr />
		         <br />
		     </div>
		     <br />
		     </div>
				 '.$duedate.'
		 </body>
		 </html>');
     $mpdf->Output('invoice-'.date('d-m-Y',  $invoice->created_at).'.pdf','I');
		 exit;
	}

	public function actionPrintPayAsYouGoPdf(){
     $invoice_id = Yii::$app->request->get('id');
		 $invoice = Invoices::findOne($invoice_id);
		 //var_dump($invoice);exit;
		 $userData 	 = User::findOne($invoice->user_id);

      if($invoice->status==1){
				$status = 'Paid';
			}else{
				$status = '';
			}

		     //require_once('../mpdf_mpdf_7.1.4.0_require');
        //require_once '../mpdf_mpdf_7.1.4.0_require/vendor/autoload.php';
        $mpdf = new \Mpdf\Mpdf();
		 //$mpdf = new \Mpdf\Mpdf();
		 $mpdf->SetDisplayMode('fullpage');
		 $mpdf->list_indent_first_level = 0;
     $mpdf->WriteHTML('<!DOCTYPE html>
		 <html>
		 <head>
		     <title>Print Invoice</title>
		     <style>
		         *
		         {
		             margin:0;
		             padding:0;
		             font-family:Arial;
		             font-size:10pt;
		             color:#000;
		         }
		         body
		         {
		             width:100%;
		             font-family:Arial;
		             font-size:10pt;
		             margin:0;
		             padding:0;
		         }

		         p
		         {
		             margin:0;
		             padding:0;
		         }

		         #wrapper
		         {
		             width:150mm;
		             margin:0 15mm;
		         }

		         .page
		         {
		             height:297mm;
		             width:210mm;
		             page-break-after:always;
		         }

		         table
		         {
		             border-left: 1px solid #ccc;
		             border-top: 1px solid #ccc;

		             border-spacing:0;
		             border-collapse: collapse;

		         }

		         table td
		         {
		             border-right: 1px solid #ccc;
		             border-bottom: 1px solid #ccc;
		             padding: 2mm;
		         }

		         table.heading
		         {
		             height:50mm;
		         }

		         h1.heading
		         {
		             font-size:14pt;
		             color:#000;
		             font-weight:normal;
		         }

		         h2.heading
		         {
		             font-size:9pt;
		             color:#000;
		             font-weight:normal;
		         }

		         hr
		         {
		             color:#ccc;
		             background:#ccc;
		         }

		         #invoice_body
		         {
		             height: 40mm;
		         }

		         #invoice_body , #invoice_total
		         {
		             width:100%;
		         }
		         #invoice_body table , #invoice_total table
		         {
		             width:100%;
		             border-left: 1px solid #ccc;
		             border-top: 1px solid #ccc;

		             border-spacing:0;
		             border-collapse: collapse;

		             margin-top:5mm;
		         }

		         #invoice_body table td , #invoice_total table td
		         {
		             text-align:center;
		             font-size:9pt;
		             border-right: 1px solid #ccc;
		             border-bottom: 1px solid #ccc;
		             padding:2mm 0;
		         }

		         #invoice_body table td.mono  , #invoice_total table td.mono
		         {
		             text-align:left;
		             padding-right:3mm;
								 padding-left: 2mm;
		             font-size:10pt;
		         }

		         #footer
		         {
		             width:180mm;
		             margin:0 15mm;
		             padding-bottom:3mm;
		         }
		         #footer table
		         {
		             width:100%;
		             border-left: 1px solid #ccc;
		             border-top: 1px solid #ccc;

		             background:#eee;

		             border-spacing:0;
		             border-collapse: collapse;
		         }
		         #footer table td
		         {
		             width:25%;
		             text-align:center;
		             font-size:9pt;
		             border-right: 1px solid #ccc;
		             border-bottom: 1px solid #ccc;
		         }
		     </style>
		 </head>
		 <body>
		 <div id="wrapper">

		     <p style="text-align:center; font-weight:bold; padding-top:5mm;">
				 </p>
				 <p style="text-align:center; font-weight:bold; padding-top:5mm;">
				 <!--<img src="/assets/CM-logo-new.png" width="200" />-->
				 <img src="/assets/CM-logo-new-dark.svg" width="200" />
				 </p>
		     <br />
		     <table class="heading" style="width:100%;">
		         <tr>
		             <td style="width:80mm;">
		                 <h1 class="heading">PAY AS YOU GO INVOICE</h1>
		                 <h2 class="heading">
												'.$userData->business_name.'<br />
												'.$userData->email.'<br />
												'.$userData->website_url.'<br />

		                 </h2>
		             </td>
		             <td rowspan="2" valign="top" align="left" style="padding:3mm;">
		                 <table>
		                     <tr><td>Invoice No </td><td>'.$invoice->id.'</td></tr>
		                     <tr><td>Date </td><td>'.date('d-m-Y',  strtotime($invoice->payment_date)).'</td></tr>
		                     <tr><td>Currency </td><td>'.$invoice->currency.'</td></tr>
		                 </table>
		             </td>
		         </tr>
		         <tr>
		             <td>
		                 <b>Chat Metrics</b> :<br />
		                 677 Victoria St<br />
		             ABBOTSFORD VIC 3062
		                 <br />
		                 AUSTRALIA<br />
		             </td>

		         </tr>
		     </table>


		     <div id="content">

		         <div id="invoice_body">
		             <table>
		             <tr style="background:#eee;">
								 		 <td style="width:5%;"><b>Sr.</b></td>
		                 <td style="width:5%;"><b>Qty</b></td>
		                 <td style="width:10%;"><b>Rate</b></td>
										 <td style="width:10%;"><b>Currency</b></td>
										 <td style="width:45%;"><b>Chat Id</b></td>
		                 <td style="width:20%;"><b>Total</b></td>
		             </tr>
								 <tr>
								 		 <td style="text-align:left; padding-left: 10px; width:5%;"> 1 </td>
										 <td style="text-align:left; padding-left: 10px; width:5%;">'.count(explode(",",$invoice->chat_id)).'</td>
										 <td style="text-align:left; padding-left:10px;">'.round($invoice->single_amount).'<br /></td>
										 <td class="mono" style="width:10%;">'.$invoice->currency.'</td><td style="width:45%;" class="mono">'.implode(', ', explode(',',$invoice->chat_id)).'</td>
										 <td style="width:20%;" class="mono">'.round($invoice->amount).'</td>
								 </tr>
		             </table>
		         </div>
		         <div id="invoice_total">
		             Total Amount :
		             <table>
		                 <tr>
		                     <td style="text-align:left; padding-left:10px;">'.$status.'</td>
		                     <td style="text-align:left; padding-left:10px; width:15%;">'.$invoice->currency.'</td>
		                     <td style="width:15%;" class="mono">'.number_format($invoice->amount,2).'</td>
		                 </tr>
		             </table>
		         </div>
		         <br />
		         <hr />
		         <br />
		     </div>
		     <br />
		     </div>
		 </body>
		 </html>');
     $mpdf->Output('invoice-'.date('d-m-Y',  strtotime($invoice->payment_date)).'.pdf','I');
		 exit;
	}

   public function actionTest(){
		 $mpdf = new \Mpdf\Mpdf();
		 $mpdf->SetDisplayMode('fullpage');
		 $mpdf->list_indent_first_level = 0;
     $mpdf->WriteHTML('<!DOCTYPE html>
		 <html>
		 <head>
		     <title>Print Invoice</title>
		     <style>
		         *
		         {
		             margin:0;
		             padding:0;
		             font-family:Arial;
		             font-size:10pt;
		             color:#000;
		         }
		         body
		         {
		             width:100%;
		             font-family:Arial;
		             font-size:10pt;
		             margin:0;
		             padding:0;
		         }

		         p
		         {
		             margin:0;
		             padding:0;
		         }

		         #wrapper
		         {
		             width:150mm;
		             margin:0 15mm;
		         }

		         .page
		         {
		             height:297mm;
		             width:210mm;
		             page-break-after:always;
		         }

		         table
		         {
		             border-left: 1px solid #ccc;
		             border-top: 1px solid #ccc;

		             border-spacing:0;
		             border-collapse: collapse;

		         }

		         table td
		         {
		             border-right: 1px solid #ccc;
		             border-bottom: 1px solid #ccc;
		             padding: 2mm;
		         }

		         table.heading
		         {
		             height:50mm;
		         }

		         h1.heading
		         {
		             font-size:14pt;
		             color:#000;
		             font-weight:normal;
		         }

		         h2.heading
		         {
		             font-size:9pt;
		             color:#000;
		             font-weight:normal;
		         }

		         hr
		         {
		             color:#ccc;
		             background:#ccc;
		         }

		         #invoice_body
		         {
		             height: 42mm;
		         }

		         #invoice_body , #invoice_total
		         {
		             width:100%;
		         }
		         #invoice_body table , #invoice_total table
		         {
		             width:100%;
		             border-left: 1px solid #ccc;
		             border-top: 1px solid #ccc;

		             border-spacing:0;
		             border-collapse: collapse;

		             margin-top:5mm;
		         }

		         #invoice_body table td , #invoice_total table td
		         {
		             text-align:center;
		             font-size:9pt;
		             border-right: 1px solid #ccc;
		             border-bottom: 1px solid #ccc;
		             padding:2mm 0;
		         }

		         #invoice_body table td.mono  , #invoice_total table td.mono
		         {
		             font-family:monospace;
		             text-align:right;
		             padding-right:3mm;
		             font-size:10pt;
		         }

		         #footer
		         {
		             width:180mm;
		             margin:0 15mm;
		             padding-bottom:3mm;
		         }
		         #footer table
		         {
		             width:100%;
		             border-left: 1px solid #ccc;
		             border-top: 1px solid #ccc;

		             background:#eee;

		             border-spacing:0;
		             border-collapse: collapse;
		         }
		         #footer table td
		         {
		             width:25%;
		             text-align:center;
		             font-size:9pt;
		             border-right: 1px solid #ccc;
		             border-bottom: 1px solid #ccc;
		         }
		     </style>
		 </head>
		 <body>
		 <div id="wrapper">

		     <p style="text-align:center; font-weight:bold; padding-top:5mm;"><img src="/images/chatmetrics-logo-new.png" width="100" /></p>
		     <br />
		     <table class="heading" style="width:100%;">
		         <tr>
		             <td style="width:80mm;">
		                 <h1 class="heading">TAX INVOICE</h1>
		                 <h2 class="heading">
												The Royals<br />
												Attention: Claire Ambrosio<br />
												3/105-115 Dover St<br />
												CREMORNE VIC 3121<br />
												AUS<br />
												ABN: 90 153 181 241

		                 </h2>
		             </td>
		             <td rowspan="2" valign="top" align="right" style="padding:3mm;">
		                 <table>
		                     <tr><td>Invoice No : </td><td>11-12-17</td></tr>
		                     <tr><td>Dated : </td><td>01-Aug-2011</td></tr>
		                     <tr><td>Currency : </td><td>USD</td></tr>
		                 </table>
		             </td>
		         </tr>
		         <tr>
		             <td>
		                 <b>Chat Metrics</b> :<br />
		                 677 Victoria St<br />
		             ABBOTSFORD VIC 3062
		                 <br />
		                 AUSTRALIA<br />
		             </td>

		         </tr>
		     </table>


		     <div id="content">

		         <div id="invoice_body">
		             <table>
		             <tr style="background:#eee;">
		                 <td style="width:8%;"><b>Sl. No.</b></td>
		                 <td><b>Product</b></td>
		                 <td style="width:15%;"><b>Quantity</b></td>
		                 <td style="width:15%;"><b>Rate</b></td>
		                 <td style="width:15%;"><b>Total</b></td>
		             </tr>
		             </table>

		             <table>
		             <tr>
		                 <td style="width:8%;">1</td>
		                 <td style="text-align:left; padding-left:10px;">Software Development<br />Description : Upgradation of telecrm</td>
		                 <td class="mono" style="width:15%;">1</td><td style="width:15%;" class="mono">157.00</td>
		                 <td style="width:15%;" class="mono">157.00</td>
		             </tr>
		             <tr>
		                 <td colspan="3"></td>
		                 <td></td>
		                 <td></td>
		             </tr>

		             <tr>
		                 <td colspan="3"></td>
		                 <td>Total :</td>
		                 <td class="mono">157.00</td>
		             </tr>
		         </table>
		         </div>
		         <div id="invoice_total">
		             Total Amount :
		             <table>
		                 <tr>
		                     <td style="text-align:left; padding-left:10px;">One  Hundred And Fifty Seven  only</td>
		                     <td style="width:15%;">USD</td>
		                     <td style="width:15%;" class="mono">157.00</td>
		                 </tr>
		             </table>
		         </div>
		         <br />
		         <hr />
		         <br />
		     </div>
		     <br />
		     </div>
				 <h3>Due Date: 4 Apr 2018</h3><p>Electronic Payment Preferred.Please use the Invoice number in the reference to ensure correct allocation of your payment.Thank you for your custom.</p><p>Westpac</p><p>BSB: 033120</p><p>A/C#: 361629</p>
		 </body>
		 </html>');
     $mpdf->Output();
		 exit;
	 }

	public function actionInvoiceListing(){
		$this->layout = 'kosmleadsandchats';
		if(in_array(Yii::$app->user->identity->role, [User::ROLE_ADMIN, User::ROLE_SUBADMIN])) {
//			$invoices = Invoice::find()
//									->where(['failed_invoice' => 0])
//									->orderBy('id DESC')
//									->all();
		//if(Yii::$app->user->identity->role == User::ROLE_ADMIN){
			// $invoices = Invoice::find()
			// 						->where(['failed_invoice' => 0])
			// 						->andWhere(['!=','advancee_payment_status' => 0, 'automatic_charge_status' => 0, 'pending_invoice' => 1])
			// 						->orderBy('id DESC')
			// 						->all();

            //$invoices = Invoice::findBySql('SELECT * FROM invoice WHERE failed_invoice = 0 AND !(advance_payment_status = 0 AND automatic_charge_status = 0 AND pending_invoice = 1) ORDER BY id DESC')->all();


//            $invoices = Invoice::find()
//                ->where('failed_invoice = 0 AND !(advance_payment_status = 0 AND automatic_charge_status = 0 AND pending_invoice = 1)')
//                ->orderBy('id DESC')
//                ->all();

            $invoices = Invoice::findBySql('SELECT * FROM invoice WHERE failed_invoice = 0 AND !(advance_payment_status = 0 AND automatic_charge_status = 0 AND pending_invoice = 1) ORDER BY id DESC')->all();

			//echo '<pre>';print_r($invoices);


			return  $this->render('list-admin',['invoices' => $invoices]);
		}else{
			//$invoices = Invoice::find()->where(['user_id'=>Yii::$app->user->identity->id])->all();
			$invoices = Invoice::findBySql('SELECT * FROM invoice WHERE user_id = "'.Yii::$app->user->identity->id.'" AND failed_invoice = 0 AND !(advance_payment_status = 0 AND automatic_charge_status = 0 AND pending_invoice = 1) ORDER BY id DESC')->all();
			return  $this->render('list-user',['invoices' => $invoices]);
		}
	}

    public function actionFailedListing()
    {
        $this->layout = 'kosmleadsandchats';
        if(in_array(Yii::$app->user->identity->role, [User::ROLE_ADMIN, User::ROLE_SUBADMIN])) {
            $invoices = Invoice::find()
                ->where(['failed_invoice' => 1])
                ->all();
            return $this->render('fail-list', ['invoices' => $invoices]);
        } else {
            return 1;
        }
    }

	public function actionUpdateInvoiceStatus(){
		$pending_invoice = Yii::$app->request->post('pending_invoice');
		$invoice_id = Yii::$app->request->post('invoice_id');
		$paid_on = Yii::$app->request->post('paid_on');
		$paid_status = Yii::$app->request->post('paid_status');
		$invoice = Invoice::find()->where(['id' => $invoice_id])->one();
		$invoice->invoice_status = $paid_status;
		$invoice->pending_invoice = $pending_invoice;
		$invoice->payment_date = strtotime($paid_on);
		$invoice->update_at = strtotime("now");
	  if($invoice->save()){
			return json_encode(['success'=>true,'data'=>'Package Allocated Successfully']);
		}else{
			return json_encode(['success'=>false,'data'=>$invoice->getErrors()]);
		}
	}

	public function actionSuccess( $id = 0 ) {
		$this->layout = 'kosmoouter';
		$success	  = "";
		$error		  = "";

		//require_once("../../vendor/stripe/stripe-php/init.php");
		\Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);

		if( $id > 0 ){
			$token = $_REQUEST['stripeToken'];
			$packageData = Packages::findOne( $id );
			if( !empty( $packageData ) ){

				$customer = \Stripe\Customer::create(array(
				  "email" => $_REQUEST['stripeEmail'],
				  "source" => $token,
				));

				$charge = \Stripe\Charge::create(array(
				  "amount" => $packageData['amount'] * 10,
				  "currency" => "usd",
				  "description" => $packageData['package_name'],
				  //"source" => $token,
				  "customer" => $customer->id
				));


				$invoice = \Stripe\Invoiceitem::create(array(
				  "customer" => $customer->id,
				  "amount" => $packageData['amount'] * 10,
				  "currency" => "usd",
				  "description" => $packageData['package_name'],
				));

				$model = new Invoices;

				$model->user_id = Yii::$app->user->id;
				$model->amount = $charge->amount;
				$model->transaction_id = $charge->balance_transaction;
				$model->invoice_id = $invoice->id;
				$model->package_id = $id;
				$model->status = 1;
				$model->payment_date = date('Y-m-d H:i:s');
				$model->save(false);

			}
		}
		return $this->redirect(['index']);
	}

	public function actionPaybysavedcard( $id ){
		$this->layout = 'kosmoouter';
		$success	  = "";
		$error		  = "";

		//require_once("../../vendor/stripe/stripe-php/init.php");
		\Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);
		 $packageData = Packages::findOne( $id );
		 if( !empty( $packageData ) ){
				$customer_id = Yii::$app->user->identity->stripeToken;



				$charge = \Stripe\Charge::create(array(
				  "amount" => $packageData['amount'] * 10,
				  "currency" => "usd",
				  "description" => $packageData['package_name'],
				  //"source" => $token,
				  "customer" => $customer_id
				));


				$invoice = \Stripe\Invoiceitem::create(array(
				  "customer" => $customer_id,
				  "amount" => $packageData['amount'] * 10,
				  "currency" => "usd",
				  "description" => $packageData['package_name'],
				));


				$model = new Invoices;

				$model->user_id = Yii::$app->user->id;
				$model->amount = $charge->amount;
				$model->transaction_id = $charge->balance_transaction;
				$model->invoice_id = $invoice->id;
				$model->package_id = $id;
				$model->status = 1;
				$model->payment_date = date('Y-m-d H:i:s');
				$model->save(false);

		 }

		return $this->redirect(['index']);
	}


	//Pay Per Lead Functionality

	public function actionPayperlead( $id ){
		$this->layout = 'kosmoouter';
		$success	  = "";
		$error		  = "";

		//require_once("../../vendor/stripe/stripe-php/init.php");
		\Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);


				$token = $_REQUEST['stripeToken'];

				$customer = \Stripe\Customer::create(array(
				  "email" => $_REQUEST['stripeEmail'],
				  "source" => $token,
				));

				$charge = \Stripe\Charge::create(array(
				  "amount" => 5 * 10,
				  "currency" => "usd",
				  "description" => "Pay Per Lead",
				  //"source" => $token,
				  "customer" => $customer->id
				));


				$invoice = \Stripe\Invoiceitem::create(array(
				  "customer" => $customer->id,
				  "amount" => 5 * 10,
				  "currency" => "usd",
				  "description" => "Pay Per Lead",
				));


				$model = new Invoices;

				$model->user_id = Yii::$app->user->id;
				$model->amount = $charge->amount;
				$model->transaction_id = $charge->balance_transaction;
				$model->invoice_id = $invoice->id;
				$model->package_id = 0;
				$model->chat_id = $id;
				$model->status = 1;
				$model->payment_date = date('Y-m-d H:i:s');
				$model->save(false);



		return $this->redirect(['index']);
	}

	public function actionPayperleadbysavedcard( $id ){
		$this->layout = 'kosmoouter';
		$success	  = "";
		$error		  = "";

		//require_once("../../vendor/stripe/stripe-php/init.php");
		\Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);
				$customer_id = Yii::$app->user->identity->stripeToken;
				$charge = \Stripe\Charge::create(array(
				  "amount" => 50,
				  "currency" => "usd",
				  "description" => "Pay Per Lead",
				  //"source" => $token,
				  "customer" => $customer_id
				));
				$invoice = \Stripe\Invoiceitem::create(array(
				  "customer" => $customer_id,
				  "amount" => 50,
				  "currency" => "usd",
				  "description" => "Pay Per Lead",
				));

				$model = new Invoices;
				$model->user_id = Yii::$app->user->id;
				$model->amount = $charge->amount;
				$model->transaction_id = $charge->balance_transaction;
				$model->invoice_id = $invoice->id;
				$model->package_id = 0;
				$model->chat_id = $id;
				$model->status = 1;
				$model->payment_date = date('Y-m-d H:i:s');
				$model->save(false);
		return $this->redirect(['index']);
	}

	protected function SaveStripeLog($user_id=0,$amount=0,$currency='',$status=0,$stripe_token='',$log='')
	{
		try {
			$log = json_encode($log);

			$stripe_log = new StripeLog();
			$stripe_log->user_id = $user_id;
			$stripe_log->amount = $amount;
			$stripe_log->currency = $currency;
			$stripe_log->status = $status;
			$stripe_log->stripe_token = $stripe_token;
			$stripe_log->log = $log;
			$stripe_log->error_type = 'updating_card_from_settings_page';//error occured while updating card details
			$stripe_log->created_at = time();
			$stripe_log->save(false);
		}catch(Exception $e) {

		}
	}

	public function actionSavecard(){
		$stripeToken = $_REQUEST['stripeToken'];
		//require_once("../../vendor/stripe/stripe-php/init.php");
		\Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);

		$card_updated = false;
		if($stripeToken != '') {
			$user_id  	 = Yii::$app->user->id;
			$userData 	 = User::findOne( $user_id );
			$customer_id = trim(Yii::$app->user->identity->stripeToken);

			//Flow:
			// 1) User enters his credit card in the dashboard.
			// 2) We check whether the stripe customer id available in our database and if user found then we will update the card details of the user in stripe.
			// 3) If stripe customer id not found in our database then we check whether user with same email address present in stripe, if yes we update the existing stripe users card details, if multiple entries are found in stripe with same email address (currently we have few uses with multiple entries) then it will update the first entry which stripe returns.
			// 4) If user with email and stripe customer id is not found (as mentioned in step 2 and 3) then we create new customer and save the stripe customer id in our database.

			//Check whether customer already exists, if yes then update the customer card details
			if($customer_id != '') {
				try{
				$cu = \Stripe\Customer::retrieve($customer_id);
				$cu->source = $stripeToken;
				$cu->save();

				$card_updated = true;

			  }catch(\Stripe\Error\Card $e) {
				// Since it's a decline, \Stripe\Error\Card will be caught
				$body = $e->getJsonBody();
				$err  = $body['error'];

				$this->SaveStripeLog($user_id,'','',0,'',$err);

			 Yii::$app->session->setFlash('error',$err['message']);
			 return $this->redirect(['/settings/index']);
			} catch (\Stripe\Error\RateLimit $e) {
				// Too many requests made to the API too quickly
				$this->SaveStripeLog($user_id,'','',0,'',$e);

			 	Yii::$app->session->setFlash('error', "Headers are missing");
				return $this->redirect(['/settings/index']);
			} catch (\Stripe\Error\InvalidRequest $e) {
				// Invalid parameters were supplied to Stripe's API

				$this->SaveStripeLog($user_id,'','',0,'',$e);

				 	Yii::$app->session->setFlash('error', "Invalid parameters were supplied to Stripe\'s API");
					return $this->redirect(['/settings/index']);
			} catch (\Stripe\Error\Authentication $e) {
				// Authentication with Stripe's API failed

				$this->SaveStripeLog($user_id,'','',0,'',$e);

			 	Yii::$app->session->setFlash('error', "maybe you changed API keys recently");
				return $this->redirect(['/settings/index']);
				// (maybe you changed API keys recently)
			} catch (\Stripe\Error\ApiConnection $e) {
				$this->SaveStripeLog($user_id,'','',0,'',$e);

			 	Yii::$app->session->setFlash('error', "Network communication with Stripe failed");
				return $this->redirect(['/settings/index']);
				// Network communication with Stripe failed
			} catch (\Stripe\Error\Base $e) {
				$this->SaveStripeLog($user_id,'','',0,'',$e);

			 	Yii::$app->session->setFlash('error', "Display a very generic error to the user, and maybe send");
				return $this->redirect(['/settings/index']);
				// Display a very generic error to the user, and maybe send
				// yourself an email
			} catch (Exception $e) {
				$this->SaveStripeLog($user_id,'','',0,'',$e);

				Yii::$app->session->setFlash('error', "Something else happened, completely unrelated to Stripe");
				return $this->redirect(['/settings/index']);
				// Something else happened, completely unrelated to Stripe
			}
				Yii::$app->session->setFlash('success', "Your Card details has been updated successfully.");
			}

			//If customer not exists, then check whether customer already exists in stripe (customer might have been manually created in stripe), if yes then retrieve the customer from stripe and update the card detail
			if(!$card_updated){
				try{
					// https://stripe.com/docs/api/customers/list?lang=php
					//Returns a list of your customers. The customers are returned sorted by creation date, with the most recent customers appearing first.
					$customer = \Stripe\Customer::all(["email" => Yii::$app->user->identity->email, "limit" => 1]);
					if(count($customer->data) > 0) {
						$customer_id = $customer->data[0]['id'];

						$cu = \Stripe\Customer::retrieve($customer_id);
						$cu->source = $stripeToken;
						$cu->save();

						$userData->stripeToken = $cu->id;
						if($userData->save(false)){
							Yii::$app->user->identity->stripeToken = $cu->id;
							Yii::$app->session->setFlash('success', "Your card details has been updated successfully for this email address.");

							$card_updated = true;
						}
					}
			  }catch(\Stripe\Error\Card $e) {
				// Since it's a decline, \Stripe\Error\Card will be caught
				$body = $e->getJsonBody();
				$err  = $body['error'];

				$this->SaveStripeLog($user_id,'','',0,'',$err);

				Yii::$app->session->setFlash('error',$err['message']);
			  return $this->redirect(['/settings/index']);
			} catch (\Stripe\Error\RateLimit $e) {
				// Too many requests made to the API too quickly
				$this->SaveStripeLog($user_id,'','',0,'',$e);

			 	Yii::$app->session->setFlash('error', "Headers are missing");
				return $this->redirect(['/settings/index']);
			} catch (\Stripe\Error\InvalidRequest $e) {
				// Invalid parameters were supplied to Stripe's API
				$this->SaveStripeLog($user_id,'','',0,'',$e);

				Yii::$app->session->setFlash('error', "Invalid parameters were supplied to Stripe\'s API");
				return $this->redirect(['/settings/index']);
			} catch (\Stripe\Error\Authentication $e) {
				// Authentication with Stripe's API failed
				$this->SaveStripeLog($user_id,'','',0,'',$e);

			 	Yii::$app->session->setFlash('error', "maybe you changed API keys recently");
				return $this->redirect(['/settings/index']);
				// (maybe you changed API keys recently)
			} catch (\Stripe\Error\ApiConnection $e) {
				$this->SaveStripeLog($user_id,'','',0,'',$e);

			 	Yii::$app->session->setFlash('error', "Network communication with Stripe failed");
				return $this->redirect(['/settings/index']);
				// Network communication with Stripe failed
			} catch (\Stripe\Error\Base $e) {
				$this->SaveStripeLog($user_id,'','',0,'',$e);

			 	Yii::$app->session->setFlash('error', "Display a very generic error to the user, and maybe send");
				return $this->redirect(['/settings/index']);
				// Display a very generic error to the user, and maybe send
				// yourself an email
			} catch (Exception $e) {
				$this->SaveStripeLog($user_id,'','',0,'',$e);

				Yii::$app->session->setFlash('error', "Something else happened, completely unrelated to Stripe");
				return $this->redirect(['/settings/index']);
				// Something else happened, completely unrelated to Stripe
			}

			}

			//if customer is not available in our database and already manually NOT added in stripe then create customer.
			if(!$card_updated) {
          try{
						$customer = \Stripe\Customer::create(array(
						  "email" => Yii::$app->user->identity->email,
						  "source" => $stripeToken,
						));

						$userData->stripeToken = $customer->id;
						if($userData->save(false)){
							Yii::$app->user->identity->stripeToken = $customer->id;
							Yii::$app->session->setFlash('success', "Your Card details has been saved successfully.");
						}

						$card_updated = true;

					}catch(\Stripe\Error\Card $e) {
					// Since it's a decline, \Stripe\Error\Card will be caught
					$body = $e->getJsonBody();
					$err  = $body['error'];

					$this->SaveStripeLog($user_id,'','',0,'',$err);

				 Yii::$app->session->setFlash('error',$err['message']);
				 return $this->redirect(['/settings/index']);
				} catch (\Stripe\Error\RateLimit $e) {
					// Too many requests made to the API too quickly
					$this->SaveStripeLog($user_id,'','',0,'',$e);

				 	Yii::$app->session->setFlash('error', "Headers are missing");
					return $this->redirect(['/settings/index']);
				} catch (\Stripe\Error\InvalidRequest $e) {
					// Invalid parameters were supplied to Stripe's API
					$this->SaveStripeLog($user_id,'','',0,'',$e);

					 	Yii::$app->session->setFlash('error', "Invalid parameters were supplied to Stripe\'s API");
						return $this->redirect(['/settings/index']);
				} catch (\Stripe\Error\Authentication $e) {
					// Authentication with Stripe's API failed
					$this->SaveStripeLog($user_id,'','',0,'',$e);

				 	Yii::$app->session->setFlash('error', "maybe you changed API keys recently");
					return $this->redirect(['/settings/index']);
					// (maybe you changed API keys recently)
				} catch (\Stripe\Error\ApiConnection $e) {
					$this->SaveStripeLog($user_id,'','',0,'',$e);

				 	Yii::$app->session->setFlash('error', "Network communication with Stripe failed");
					return $this->redirect(['/settings/index']);
					// Network communication with Stripe failed
				} catch (\Stripe\Error\Base $e) {
					$this->SaveStripeLog($user_id,'','',0,'',$e);

				 	Yii::$app->session->setFlash('error', "Display a very generic error to the user, and maybe send");
					return $this->redirect(['/settings/index']);
					// Display a very generic error to the user, and maybe send
					// yourself an email
				} catch (Exception $e) {
					$this->SaveStripeLog($user_id,'','',0,'',$e);

					Yii::$app->session->setFlash('error', "Something else happened, completely unrelated to Stripe");
					return $this->redirect(['/settings/index']);
					// Something else happened, completely unrelated to Stripe
				}

			}
		}
		return $this->redirect(['/settings/index']);
	}

	public function actionPayCustomPackageBySavedCard($id){
		$this->layout = 'kosmoouter';
		$success	  = "";
		$error		  = "";

		//require_once("../../vendor/stripe/stripe-php/init.php");
		\Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);
		 $customPackageRange = CustomPackageRange::findOne( $id );
		 if( !empty( $customPackageRange ) ){
				$customer_id = Yii::$app->user->identity->stripeToken;

				$charge = \Stripe\Charge::create(array(
				  "amount" => $customPackageRange->amount,
				  "currency" => "usd",
				  "description" => 'Custom',
				  //"source" => $token,
				  "customer" => $customer_id
				));


				$invoice = \Stripe\Invoiceitem::create(array(
				  "customer" => $customer_id,
				  "amount" => $customPackageRange->amount,
				  "currency" => "usd",
				  "description" => 'Custom',
				));

				$packageUser = new PackageUser();
				$packageUser->user_id = Yii::$app->user->identity->id;
				$packageUser->number_of_leads = $customPackageRange->range_end;
				$packageUser->number_of_chats = $customPackageRange->range_end;
				$packageUser->amount = $customPackageRange->amount;
				$packageUser->validity = 0;
				$packageUser->save();


				$model = new Invoices;

				$model->user_id = Yii::$app->user->id;
				$model->amount = $charge->amount;
				$model->transaction_id = $charge->balance_transaction;
				$model->invoice_id = $invoice->id;
				$model->package_id = $customPackageRange->id;
				$model->status = 1;
				$model->payment_date = date('Y-m-d H:i:s');
				$model->save(false);

		 }

		return $this->redirect(['index']);
	}



}
