<?php

namespace frontend\controllers;

use Yii;
use app\models\User2;
use common\models\User;
use common\models\UserAssociate;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\PasswordResetRequestForm;

/**
 * UserController implements the CRUD actions for User2 model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
	

   /* public function beforeAction ($action)
    {
        $host = explode('.', $_SERVER['HTTP_HOST']);
        $username = $host[0];
        
        if($username != '' && $username != 'fubaco')
        {
            if(Yii::$app->user->identity->username != '')
            {
                if($username  != Yii::$app->user->identity->username)
                {
                    //echo Yii::$app->homeUrl; exit;
                     //return $this->redirect('http://chat-application.com/site/login');
                }
            }
        }
        return 1;
        
    }*/

    public function beforeAction ($action)
    {
        $host = explode('.', $_SERVER['HTTP_HOST']);
        $username = $host[0];
        $flag = 0;
        if($role = Yii::$app->user->identity->role == '')
        {
            $flag = 1;
        }
        if($username != '')
        {   
            $role = Yii::$app->user->identity->role;
            if(User::ROLE_AGENCY == $role)
            {
                if($username == 'chat-application')
                {
                    $flag = 1;
                
                }
                else if ($username != Yii::$app->user->identity->username)
                {
                    $flag = 1;
                }
            }
            else if(User::ROLE_USER == $role )
            {
                $parent_id = Yii::$app->user->identity->parent_id;
                if($parent_id != '')
                {
                    $row = (new \yii\db\Query())
                    ->from('user')
                    ->where(['id' => $parent_id])
                    ->one();
                   if($username == 'chat-application')
                    {
                        $flag = 1;
                    
                    }
                    else if ($username != $row['username'])
                    {
                        $flag = 1;
                    }
                }
            }
            if($flag == 1)
            {
                Yii::$app->user->logout(); 
                //echo Yii::$app->homeUrl; exit;
                return $this->redirect('http://chat-application.com/site/login');
            }
        }
        return 1;
        
    }

	public $layout = "dashboard";

    /**
     * Lists all User2 models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $id = yii::$app->user->identity->id;
        $userrole = yii::$app->user->identity->role;
        if($userrole == User::ROLE_AGENCY)
        {
            $searchModel = new UserSearch(['role'=>'10', 'parent_id' => $id]);
        }
        else
        {
            $searchModel = new UserSearch(['role'=>'10']);
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User2 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User2();		
		

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
			//echo '<pre>';print_r($model->username);die;
			
			$user = new User();
            $user->username = $model->username;
            $user->user_group = $model->user_group;
            $user->email = $model->email;
            $user->website_url = $model->website_url;
            
            $id = yii::$app->user->identity->id;
            $userrole = yii::$app->user->identity->role;
            if($userrole == User::ROLE_AGENCY)
            {
                $user->parent_id = $id;
            }
            $user->role =User::ROLE_USER;
            $user->setPassword(time().rand());
            $user->generateAuthKey();
			if ($user->save()) {
                //return $user;

                $i=0;
                foreach ($_POST['User2']['email2'] as $value) {
                    
                    if($value != '')
                    {   
                        $model2 = new UserAssociate;
                        //Update the Image
                        $model2->user_id = $user->id;
                        $model2->email = $value;
                        $model2->save();
                    }                    
                    $i++;
                }

				$model_reset = new PasswordResetRequestForm();
				$model_reset->email = $model->email;
				if($model_reset->validate()){
					if ($model_reset->sendEmail("test")) {
						Yii::$app->getSession()->setFlash('success', 'User Created Mail Send To User\'s Email.');
						//return $this->goHome();
						return $this->redirect(['index']);
					} else {
						Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to Send Email.');
					}
				}
				return $this->redirect(['index']);
            }
			else{
				return $this->render('create', [
					'model' => $model,
				]);
			}
			
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            // Starting business liencence information.   
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand('delete FROM `user_associate` 
            WHERE  `user_id` ='.$_GET['id']);
            $command->execute();   

            $i=0;
            foreach ($_POST['User2']['email2'] as $value) {

                if($value != '')
                {         
                    $model2 = new UserAssociate;
                    //Update the Image
                    $model2->user_id = $model->id;
                    $model2->email = $value;
                    $model2->save();
                }
                $i++;
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }



    public function actionIndexagency()
    {
        
                
        $searchModel = new UserSearch(['role'=>User::ROLE_AGENCY]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('indexagency', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User2 model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewagency($id)
    {
        return $this->render('viewagency', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateagency()
    {
        $model = new User2();        
       

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            //echo '<pre>';print_r($model->username);die;
            
            $user = new User();
            $user->username = $model->username;
            $user->user_group = $model->user_group;
            $user->email = $model->email;
            $user->website_url = $model->website_url;
            //$user->parent_id = $model->parent_id;
            $user->role =User::ROLE_AGENCY;
            $user->agency_name = $model->agency_name;

            $uploadfile = '';
            if(isset($_FILES['User2']['name']['agency_logo']) && $_FILES['User2']['name']['agency_logo'] != '')
            {
                $uploaddir = 'logo_images/';
                $time = time();
                $ext = pathinfo($_FILES['User2']['name']['agency_logo'], PATHINFO_EXTENSION);
                $uploadfile = 'agency_logo_'.$time .'_'.rand(111,999).'.'. $ext;

                
                if (move_uploaded_file($_FILES['User2']['tmp_name']['agency_logo'], $uploaddir.$uploadfile)) {
                   
                } 
                else
                {
                    $uploadfile = '';
                } 
            }
            $user->agency_logo = $uploadfile;
            $user->theme_color = $model->theme_color;
            $user->setPassword(time().rand());
            $user->generateAuthKey();
            if ($user->save()) {

                $connection = Yii::$app->db;
                $connection->createCommand()->update('user', ['role' => User::ROLE_AGENCY], 'id = '.$user->id)->execute();
                //return $user;
                $model_reset = new PasswordResetRequestForm();
                $model_reset->email = $model->email;
                if($model_reset->validate()){
                    if ($model_reset->sendEmail("test")) {
                        Yii::$app->getSession()->setFlash('success', 'Agency Created Mail Send To User\'s Email.');
                        //return $this->goHome();
                        return $this->redirect(['indexagency']);
                    } else {
                        Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to Send Email.');
                    }
                }
                return $this->redirect(['indexagency']);
            }
            else{
                return $this->render('createagency', [
                    'model' => $model,
                ]);
            }
            
        } else {
            return $this->render('createagency', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateagency($id)
    {
        $model = $this->findModel($id);
        $agency_logo = $model->agency_logo;
        if ($model->load(Yii::$app->request->post())) {
            $uploadfile = '';
            if(isset($_FILES['User2']['name']['agency_logo']) && $_FILES['User2']['name']['agency_logo'] != '')
            {

                if($agency_logo){
                    @unlink('logo_images/'.$agency_logo);
                }
                $time = time();
                $uploaddir = 'logo_images/';
                $ext = pathinfo($_FILES['User2']['name']['agency_logo'], PATHINFO_EXTENSION);
                $uploadfile = 'agency_logo_'.$time .'_'.rand(111,999).'.'. $ext;

                
                if (move_uploaded_file($_FILES['User2']['tmp_name']['agency_logo'], $uploaddir.$uploadfile)) {
                   
                } 
                else
                {
                    $uploadfile = '';
                } 
            }
            if($uploadfile != '')
            {
                $model->agency_logo = $uploadfile;
            }
            else
            {
                $model->agency_logo = $agency_logo;
            }
            
            $model->save();
            return $this->redirect(['indexagency']);
        } else {
            return $this->render('updateagency', [
                'model' => $model,
            ]);
        }
    }

    public function actionDeletelogo($id)
    {
        $model = $this->findModel($id);

        if($model->agency_logo){
            @unlink('logo_images/'.$model->agency_logo);
        }
        $connection = Yii::$app->db;
        $connection->createCommand()->update('user', ['agency_logo' => ''], 'id = '.$id)->execute();

        /*  $User=User::findOne($id);
         print_r($User); exit;
        $photo->photo=NULL;
        $photo->update();*/
        return $this->redirect(['updateagency','id'=>$_GET['id']]);
    }
    /**
     * Deletes an existing User2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteagency($id)
    {
        $model = $this->findModel($id);

        if($model->agency_logo){
            @unlink('logo_images/'.$model->agency_logo);
        }

        $this->findModel($id)->delete();        
        return $this->redirect(['indexagency']);
    }

	public function actionChangepassword($id = false){
        // $layout_ = $this->actionGetlayout();
        // $this->layout = $layout_;
		$id = yii::$app->user->identity->id;
        $model = $this->findModel($id);
		
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()){
			//die("sasa");
            $model->password_hash = Yii::$app->security->generatePasswordHash(Yii::$app->request->post()['new_password']);
            if($model->save()) {
                //return $this->redirect(['view', 'id' => $model->id]);
                return $this->goHome();
            }else{
				
				echo '<pre>';print_r($model);die;
				 return $this->render('changepassword', [
					'model' => $model,
				]);
			}
        } else {
            return $this->render('changepassword', [
                'model' => $model,
            ]);
        }        
    }
    /**
     * Deletes an existing User2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand('delete FROM `user_associate` 
        WHERE  `user_id` ='.$id);
        $command->execute();   
        return $this->redirect(['index']);
    }

    /**
     * Finds the User2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User2::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	
}
