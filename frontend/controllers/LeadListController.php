<?php

namespace frontend\controllers;

use Yii;
use common\models\Chat;
use common\models\LeadListSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use common\models\UserCategory;
use common\models\ForwardingEmailTeam;
use frontend\models\Invoices;
use common\models\Packages;
use common\models\PackageUser;
use app\models\Leads;
use app\models\Subscription;
use common\models\ForwardingEmailAddress;
use app\models\CustomLeadFields;
use app\models\CustomLeadFieldsOption;
use yii\filters\AccessControl;
use common\models\LoginForm;


use app\models\User2;
use common\models\LiveChat_API;
use common\models\EmailLog;
use common\models\ChatMessage;
use common\models\ServiceChatEmail;
use app\models\Notification;



/**
 * LeadListController implements the CRUD actions for Chat model.
 */
class LeadListController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'except' => ['view', 'create', 'update', 'delete', 'load-chat-message', 'load-custom-field', 'load-visitor-details', 'save-custom-fields', 'timestamp'],
                'rules' => [
                    [
                        'allow' => true,    
                        'actions' => ['index', 'leads','export-leads', 
                          'print-leads', 'new-index'],                  
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('manageLeads');                     
                        }                     
                    ],
                    [
                        'allow' => true,    
                        'actions' => ['chats', 'export-chats', 'print-chats'],                  
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('manageChats');                     
                        }                     
                    ],   
                    [
                        'allow' => true,    
                        'actions' => ['service-chats', 'export-service-chats', 
                          'print-service-chats'],                  
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('manageServiceChats');                     
                        }                     
                    ],   
                    [
                        'allow' => true,    
                        'actions' => ['forward-email-list'],                  
                        'roles' => ['@']                     
                    ],                                                                          
                ],
            ],             
        ];
    }


    public function beforeAction($action)
    {    
        if(!parent::beforeAction($action)) {
            return false;
        }

        LoginForm::userAssitLogin();

        // set website filter
        if(Yii::$app->request->post('website_filter')) {
          unset($_GET["LeadListSearch"]['outlet_id']);
          unset($_GET["LeadListSearch"]['category_id']); 

          if(Yii::$app->request->post('website_filter') == 'all') {
            Yii::$app->session->remove('website_filter'); 
            Yii::$app->session->remove('outlet_filter');               
          } else {
            Yii::$app->session->set('website_filter', Yii::$app->request->post('website_filter')); 
            Yii::$app->session->remove('outlet_filter');
          }         
        }

        // set outlet filter
        if(Yii::$app->request->post('outlet_filter')) {
          unset($_GET["LeadListSearch"]['category_id']);
          unset($_GET["LeadListSearch"]['outlet_id']);

          if(Yii::$app->request->post('outlet_filter') == 'all') {
            Yii::$app->session->remove('outlet_filter');                
          } else {
            Yii::$app->session->set('outlet_filter', Yii::$app->request->post('outlet_filter')); 
            $outletuser = User::findOne(['id' => Yii::$app->request->post('outlet_filter')]);         
            Yii::$app->session->set('website_filter', $outletuser->user_group);                
          }
        }
     
        if(Yii::$app->user->isGuest) {
          return $this->redirect(['site/index']);
        }

        $this->layout = 'kosmoouter';
        return true;
    }

    public function render($view, $params)
    { 
        $website_users = User::find()->where(['email' => Yii::$app->user->identity->email])->all();
        if(count($website_users) > 1)
        {
          $sql = "Select id, name, website_url from `user` where `role`=". User::ROLE_OUTLET." and parent_id in (select id from `user` where id='".$website_users[0]->id."' or parent_id='".$website_users[0]->id."')";                      
          $outlets = Yii::$app->db->createCommand($sql)->queryAll();
          
          $params['website_users'] = $website_users;        
          $params['outlets'] = $outlets;

          if(!Yii::$app->session->has('website_filter')) {
            unset($params['categories']);
            unset($params['siteOutlets']);
          }
        }

        $params = LoginForm::userAssitLogout($params);

        // before render
        return parent::render($view, $params);
    }

    /**
     * Lists all Chat models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new LeadListSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        $categories = UserCategory::findAll(['user_id' => Yii::$app->user->id]);

        $franchiseOutlets = [];
        if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE) {
          $franchiseOutlets = User::findAll(['role' => User::ROLE_OUTLET, 'parent_id' => Yii::$app->user->identity->id]);
        }

        $forwardingEmailTeams = ForwardingEmailTeam::findAll(['user_id' => Yii::$app->user->identity->id]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'categories' => $categories,
            'franchiseOutlets' => $franchiseOutlets,
            'forwardingEmailTeams' => $forwardingEmailTeams,
        ]);
    }

    public function actionLeads()
    { 
        $searchModel = new LeadListSearch;
        $pagination = ['pageSize' => 10];
        $params = Yii::$app->request->getQueryParams();

        $dataProvider = $searchModel->searchLeads($params,$pagination);       

        $user_id = Yii::$app->user->identity->id;
        if (Yii::$app->session->has('website_filter')) {
          $filteruser = User::findOne(['user_group' => Yii::$app->session->get('website_filter')]);
          $user_id = $filteruser->id;                                 
        } 

        $categories = UserCategory::find()->select(['id','category_name','value'])
                                ->where(['user_id' => $user_id])->all();
        $siteOutlets = User::find()
                        ->select(['id','name'])
                        ->where(['role' => User::ROLE_OUTLET, 'parent_id' => $user_id])->all();      

        $pay_as_you_go = 1;
        $check_subscribe = Subscription::findOne(['user_id'=>Yii::$app->user->identity->id]);
        if(Yii::$app->user->identity->free_access==1 || $check_subscribe) {
                  $pay_as_you_go = 0;
        }
        
       
        return $this->render('leads', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'categories' => $categories,
            'siteOutlets' => $siteOutlets,
            'showPayAsYouGo' => $pay_as_you_go,
        ]);
    }


    public function actionExportLeads(){
    $dataprovider =  Yii::$app->request->get('dataprovider');
    //echo '<pre>'; print_r($dataprovider);exit;
    $someArray = json_decode($dataprovider, true);
    $searchModel = new LeadListSearch;
    $pagination = false;
    $dataProvider = $searchModel->searchLeads($someArray,$pagination);
    $model =  $dataProvider->getModels();
    $rnd = rand();
    $filename = "lead$rnd.csv";
     //echo '<pre>';  print_r($model);exit;
    $yes = '';
    foreach($model as $key => $chat){
      //echo '<pre>';  print_r($chat['leads']['status']);exit;
        $showlead = Leads::showleads($chat['chat_id']);
      if($showlead==1){
      $line = [];
      $line['Name'] = $chat['leads']['visitor_name'];
      $line['Email'] = $chat['leads']['visitor_email'];
      $line['Phone'] = $chat['leads']['visitor_phone'];
      $line['City'] = $chat['city'];
      //$line['Category'] = isset($chat['category']) ? $chat['category']['name'] : 'Not Set';
      $line['State'] = $chat['region'];
      $line['Country'] = $chat['country'];
      $line['Date'] = date('d-m-Y', $chat['ended_timestamp']);
      $line['Action Status'] = ucfirst($chat['leads']['status']);
      $file = fopen($filename,"a");
      fputcsv($file, $line);
        $yes = 1;
      }
    }
    if($yes == 1){
       header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=$filename");
    header("Content-Type: application/csv");
    header("Content-Transfer-Encoding: binary");

    // read the file from disk
    readfile($filename);
    //return json_encode(['success' => true, 'data' => $filename]);
     //echo "ssdf";exit;    
    }else{
        Yii::$app->session->setFlash('error', 'You have not bought any lead');
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    }
    
    public function actionPrintLeads(){
      $dataprovider =  Yii::$app->request->post('dataprovider');
      $page =  Yii::$app->request->post('page');
        //echo '<pre>'; print_r($dataprovider);exit;
      $someArray = json_decode($dataprovider, true);
      $searchModel = new LeadListSearch;
      //$pagination = ['pageSize' => 10,'page' => $page-1];
      $pagination = false;
      $dataProvider = $searchModel->searchLeads($someArray,$pagination);
      $model =  $dataProvider->getModels();
      foreach($model as $key => $chat){
            $showlead = Leads::showleads($chat['chat_id']);
        if($showlead==1){
        //echo '<pre>';  print_r($chat['leads']['status']);exit;
        $line = [];
        $line['name'] = $chat['leads']['visitor_name'];
        $line['email'] = $chat['leads']['visitor_email'];
        $line['phone'] = $chat['leads']['visitor_phone'];
        $line['city'] = $chat['city'];
        //$line['Category'] = isset($chat['category']) ? $chat['category']['name'] : 'Not Set';
        $line['state'] = $chat['region'];
        $line['country'] = $chat['country'];
        $line['date'] = date('d-m-Y', $chat['ended_timestamp']);
        $line['action_status'] = ucfirst($chat['leads']['status']);
        // $html[] = '<tr><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Name'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Email'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['City'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['State'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Country'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Date'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Action Status'].'</td></tr><tr>';
        $html[] = $line;
        }
      }
      if(isset($html)){
      return json_encode(['success' => true, 'data' => $html]);
    }else{
      return json_encode(['success' => false, 'data' => 'You have not bought Any lead']);
    }
    }

    protected function showleads($dataProvider)
    {
         $packageInfo = $this->findPackage();
         $showLead = $this->findPackageInfo1($packageInfo,$dataProvider);
         return $showLead;
    }

    protected function findPackageInfo1($packageInfo,$dataProvider){
      $leads = $dataProvider->getModels();
      $leadData = [];

      foreach($leads as $key => $chat){
        $showLead = 0;
        //$chatModel = $this->findModel($chat['id']);
        if( !empty( $packageInfo ) ){
  				foreach($packageInfo as $packageDate => $leadCount){
            // Package filter code
  					if((strtotime( $packageDate) <= $chat['started_timestamp']) && (strtotime( "$packageDate + 30 days") >= $chat['started_timestamp']) && $leadCount > $packageShowCount){
  					return	$showLead = 1;
  					}
            // Package filter code
  				}
  			}
        //echo '<pre>'; print_r($chat['chat_id']);exit;
        if( !$showLead ){
  				$isPaid = Invoices::find()->where(['chat_id' => $chat['chat_id'], 'user_id' => Yii::$app->user->id])->one();
  				if($isPaid){
  					$showLead = 1;
  				}
  				else if( $key < 5 ){
  					$showLead = 1;
  				}
  			}
        $user = Yii::$app->user->identity;
        if($user){
          if($user->free_access){
              $showLead = 1;
          }elseif ($user->role == User::ROLE_OUTLET && $user->parent->free_access) {
              $showLead = 1;
          }
        }else{
          if(Yii::$app->user->identity->free_access){
              $showLead = 1;
          }elseif (Yii::$app->user->identity->role == User::ROLE_OUTLET && Yii::$app->user->identity->parent->free_access) {
              $showLead = 1;
          }
        }
        $column['showLead'] 		 = $showLead;
        $leadData[] = $column;
      }

      //echo '<pre>'; print_r($leadData);exit;
      return $leadData[] = $column;

    }

    public function actionChats()
    {
        $searchModel = new LeadListSearch;
        $pagination = ['pageSize' => 10];
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(),$pagination);

        return $this->render('chats', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionServiceChats()
    {
      $searchModel = new LeadListSearch;
      $pagination = ['pageSize' => 10];
      $params = Yii::$app->request->getQueryParams();

      $dataProvider = $searchModel->searchServiceChats($params, $pagination);
     
      $user_id = Yii::$app->user->identity->id;
      if (Yii::$app->session->has('website_filter')) {
        $filteruser = User::findOne(['user_group' => Yii::$app->session->get('website_filter')]);
        $user_id = $filteruser->id;                                 
      } 

      $categories = UserCategory::find()->select(['id','category_name','value'])
                              ->where(['user_id' => $user_id])->all();
      $siteOutlets = User::find()
                      ->select(['id','name'])
                      ->where(['role' => User::ROLE_OUTLET, 'parent_id' => $user_id])->all();        

      return $this->render('service-chats', [
          'dataProvider' => $dataProvider,
          'searchModel' => $searchModel,
          'categories' => $categories,
          'siteOutlets' => $siteOutlets,        
      ]);
    }
    
    public function actionExportServiceChats() {
    $dataprovider =  Yii::$app->request->get('dataprovider');
   // echo '<pre>'; print_r($dataprovider);exit;
    $someArray = json_decode($dataprovider, true);
    $searchModel = new LeadListSearch;
    $pagination = false;
    $dataProvider = $searchModel->searchServiceChats($someArray,$pagination);
    $model =  $dataProvider->getModels();
    $rnd = rand();
    $filename = "ServiceChat$rnd.csv";
     //echo '<pre>';  print_r($model);exit;
    foreach($model as $key => $chat){
     // echo '<pre>';  print_r($chat['leads']['status']);exit;
      $line = [];
      $line['Name'] = $chat['leads']['visitor_name'];
      $line['Email'] = $chat['leads']['visitor_email'];
      $line['Phone'] = $chat['leads']['visitor_phone'];
      $line['City'] = $chat['city'];
      //$line['Category'] = isset($chat['category']) ? $chat['category']['name'] : 'Not Set';
      $line['State'] = $chat['region'];
      $line['Country'] = $chat['country'];
      $line['Date'] = date('d-m-Y', $chat['ended_timestamp']);
      $line['Action Status'] = ucfirst($chat['leads']['status']);

      $file = fopen($filename,"a");
      fputcsv($file, $line);
    }
    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=$filename");
    header("Content-Type: application/csv");
    header("Content-Transfer-Encoding: binary");

    // read the file from disk
     readfile($filename);
    //return json_encode(['success' => true, 'data' => $filename]);
     //echo "ssdf";exit;
    }
    
    public function actionExportChats(){
    $dataprovider =  Yii::$app->request->get('dataprovider');
    //echo '<pre>'; print_r($dataprovider);exit;
    $someArray = json_decode($dataprovider, true);
    $searchModel = new LeadListSearch;
    $pagination = false;
    $dataProvider = $searchModel->search($someArray,$pagination);
    $model =  $dataProvider->getModels();
    $rnd = rand();
    $filename = "chats$rnd.csv";
    $yes = '';
     //echo '<pre>';  print_r($model);exit;
    foreach($model as $key => $chat){
      //echo '<pre>';  print_r($chat['leads']['status']);exit;
      $showlead = Leads::showleads($chat['chat_id']);
      if($showlead==1){
      $line = [];
      $line['Name'] = $chat['leads']['visitor_name'];
      $line['Email'] = $chat['leads']['visitor_email'];
      $line['Phone'] = $chat['leads']['visitor_phone'];
      $line['City'] = $chat['city'];
      //$line['Category'] = isset($chat['category']) ? $chat['category']['name'] : 'Not Set';
      $line['State'] = $chat['region'];
      $line['Country'] = $chat['country'];
      $line['Date'] = date('d-m-Y', $chat['ended_timestamp']);
      $line['Action Status'] = ucfirst($chat['leads']['status']);
      $file = fopen($filename,"a");
      fputcsv($file, $line);
      $yes = 1;
      }
    }
    
    if($yes == 1){
        header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=$filename");
    header("Content-Type: application/csv");
    header("Content-Transfer-Encoding: binary");

    // read the file from disk
     readfile($filename);
    //return json_encode(['success' => true, 'data' => $filename]);
     //echo "ssdf";exit;
    }else{
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    }
    
    public function actionPrintServiceChats(){
      $dataprovider =  Yii::$app->request->post('dataprovider');
      $page =  Yii::$app->request->post('page');
      $someArray = json_decode($dataprovider, true);
      //echo '<pre>'; print_r($someArray);exit;
      $searchModel = new LeadListSearch;
      //$pagination = ['pageSize' => 10,'page' => $page-1];
      $pagination = false;
      $dataProvider = $searchModel->searchServiceChats($someArray,$pagination);
      $model =  $dataProvider->getModels();
      foreach($model as $key => $chat){
        //echo '<pre>';  print_r($chat['leads']['status']);exit;
        //$line = [];
        $line['name'] = $chat['leads']['visitor_name'];
        $line['email'] = $chat['leads']['visitor_email'];
        $line['phone'] = $chat['leads']['visitor_phone'];
        $line['city'] = $chat['city'];
        //$line['Category'] = isset($chat['category']) ? $chat['category']['name'] : 'Not Set';
        $line['state'] = $chat['region'];
        $line['country'] = $chat['country'];
        $line['date'] = date('d-m-Y', $chat['ended_timestamp']);
        $line['action_status'] = ucfirst($chat['leads']['status']);
        // $html[] = '<tr><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Name'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Email'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['City'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['State'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Country'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Date'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Action Status'].'</td></tr><tr>';
        $html[] = $line;
      }
      return json_encode(['success' => true, 'data' => $html]);
    }
    
    public function actionPrintChats(){
      $dataprovider =  Yii::$app->request->post('dataprovider');
      $page =  Yii::$app->request->post('page');
      $someArray = json_decode($dataprovider, true);
      //echo '<pre>'; print_r($someArray);exit;
      $searchModel = new LeadListSearch;
      //$pagination = ['pageSize' => 10,'page' => $page-1];
      $pagination = false;
      $dataProvider = $searchModel->search($someArray,$pagination);
      $model =  $dataProvider->getModels();
      foreach($model as $key => $chat){
        //echo '<pre>';  print_r($chat['leads']['status']);exit;
        //$line = [];
        $showlead = Leads::showleads($chat['chat_id']);
      if($showlead==1){
        $line['name'] = ($chat['leads']['visitor_name']==null)?"-":$chat['leads']['visitor_name'];
        $line['email'] = ($chat['leads']['visitor_email']==null)?"-":$chat['leads']['visitor_email'];
         $line['phone'] = ($chat['leads']['visitor_phone']==null)?"-":$chat['leads']['visitor_phone'];
        $line['city'] = $chat['city'];
        //$line['Category'] = isset($chat['category']) ? $chat['category']['name'] : 'Not Set';
        $line['state'] = $chat['region'];
        $line['country'] = $chat['country'];
        $line['date'] = date('d-m-Y', $chat['ended_timestamp']);
        $line['action_status'] = ucfirst($chat['leads']['status']);
        // $html[] = '<tr><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Name'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Email'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['City'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['State'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Country'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Date'].'</td><td style="border-right:1px solid #999" cellspacing="0" cellpadding="0">'.$line['Action Status'].'</td></tr><tr>';
        $html[] = $line;
      }
      }
      return json_encode(['success' => true, 'data' => $html]);
    }
    
    

    public function actionNewIndex()
    {
        $searchModel = new LeadListSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index_old', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Chat model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Chat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Chat;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Chat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Chat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Chat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Chat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Chat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findPackage(){
         $user_id    = Yii::$app->user->identity->id;
         $invoiceData = Invoices::find()->where(['user_id' => $user_id])->all();
         $payment_date = '';
         $number_of_leads = '';
         $pack_type = '';
         if( !empty( $invoiceData ) ){
           foreach( $invoiceData as $key => $invoice){
             if($invoice['package_id'] > 0){
               $package_id = $invoice['package_id'];
               $packageData = Packages::findOne( $package_id );
               if( !empty( $packageData ) ){
                 if($packageData->package_name == 'Custom' && ($pack_type == '' || $pack_type == 'Gold' || $pack_type == 'Startup')){
                   $packageUser = PackageUser::findOne(['user_id' => Yii::$app->user->identity->id]);
                   if($packageUser){
                       $payment_date = $invoice->payment_date;
                       $number_of_leads = $packageUser->number_of_leads;
                       $pack_type = 'Custom';
                   }
                 }elseif ($packageData->package_name == 'Gold' && ($pack_type == '' || $pack_type == 'Startup')) {
                   $payment_date = $invoice->payment_date;
                   $number_of_leads = $packageData->number_of_leads;
                   $pack_type = 'Gold';
                 }elseif ($packageData->package_name == 'Startup' && $pack_type == '') {
                   $payment_date = $invoice->payment_date;
                   $number_of_leads = $packageData->number_of_leads;
                   $pack_type = 'Startup';
                 }
               }
             }
           }
         }
         if($pack_type != ''){
        return  [$payment_date => $number_of_leads];
         }
    }

    protected function findPackageInfo($packageInfo,$chatModel){
      $showLead = 0;
      $packageDataCount = [];
      $packageShowCount = 0;
      if( !empty( $packageInfo ) ){
				foreach($packageInfo as $packageDate => $leadCount){
          // Package filter code
					if((strtotime( $packageDate) <= $chatModel['started_timestamp']) && (strtotime( "$packageDate + 30 days") >= $chatModel['started_timestamp']) && $leadCount > $packageShowCount){
                        $showLead = 1;
                        $packageShowCount++;
                        break;
					}
          // Package filter code
				}
			}
      //print_r($chatModel->chat_id);exit;
      if( !$showLead ){
				$isPaid = Invoices::find()->where(['chat_id' => $chatModel->chat_id, 'user_id' => Yii::$app->user->id])->one();
				if($isPaid){
					$showLead = 1;
				}
				// else if( $key < 5 ){
				// 	$showLead = 1;
				// }
			}

     $user = Yii::$app->user->identity;

      if($user){
        if($user->free_access){
            $showLead = 1;
        }elseif ($user->role == User::ROLE_OUTLET && $user->parent->free_access) {
            $showLead = 1;
        }
      }else{
        if(Yii::$app->user->identity->free_access){
            $showLead = 1;
        }elseif (Yii::$app->user->identity->role == User::ROLE_OUTLET && Yii::$app->user->identity->parent->free_access) {
            $showLead = 1;
        }
      }
      return $showLead;
    }


    public function actionLoadVisitorDetails()
    {
      $response = array();

      $chatid = Yii::$app->request->post('chatid');
      $chatModel = Chat::findOne(['chat_id'=>$chatid]);      

      $showlead = Leads::showleads($chatid);
      if($showlead==1) {
        $response['Name'] = $chatModel->leads->fullName;
        $response['Email'] = $chatModel->leads->visitor_email;
        $response['Phone'] = $chatModel->leads->visitor_phone;
      
      }else{
        $response['Name'] = Leads::obfuscate_name($chatModel->leads->fullName);
        $response['Email'] = Leads::obfuscate_email($chatModel->leads->visitor_email);   
        $response['Phone'] = Leads::obfuscate_name($chatModel->leads->visitor_phone);      
      }
      $response['City'] = $chatModel->city;
      $response['Region'] = $chatModel->region;
      $response['Country'] = $chatModel->country;  

      // Custom Fields
      $leadModel = Leads::findOne(['chat_id'=>$chatid]);
      if($showlead==1){
        if(!empty($leadModel->custom_fields)){
          $custom_fields = json_decode($leadModel->custom_fields,true);
          $customdata = [];
          $multipleChoiceString = '';
          foreach($custom_fields as $key => $custom_field){
            $custom_table = CustomLeadFields::findOne($custom_field['field_id']);
            if($custom_field['field_type']=='textbox'){
              $fieldvalue = $custom_field['field_value'];
              $customdata[$key]['label'] = htmlspecialchars_decode($custom_table['label']);
              $customdata[$key]['field_value'] = urldecode($fieldvalue);
            }elseif ($custom_field['field_type']=='dropdown'){
              $dropDownOption = CustomLeadFieldsOption::findOne($custom_field['field_value']);
              $fieldvalue2 = $dropDownOption['option'];
              $customdata[$key]['label'] = htmlspecialchars_decode($custom_table['label']);
              $customdata[$key]['field_value'] = $fieldvalue2;
            }elseif ($custom_field['field_type']=='multiple_choice'){
              $custom_field_id_arrays = explode(',', urldecode($custom_field['field_value']));
              $multipleChoiceOptions = CustomLeadFieldsOption::findAll($custom_field_id_arrays);
              foreach($multipleChoiceOptions as $multipleChoiceOption){
                $multipleChoiceString .= $multipleChoiceOption['option'].',';
              }
              //print_r($multipleChoiceOption);exit;
               $fieldvalue3 = rtrim($multipleChoiceString,', ');
               $customdata[$key]['label'] = htmlspecialchars_decode($custom_table['label']);
               $customdata[$key]['field_value'] = $fieldvalue3;

            }
          }
          return json_encode(['success' => true, 'data' => $response, 'customdata'=>$customdata]);
      }else{
        return json_encode(['success' => 'Partial', 'data' => $response, 'message' => 'There are no custom fields avaliable for this chat']);
      }
      }else{
        return json_encode(['success' => 'Expired', 'data' => $response, 'customdata' => 'Expired']);
      }
      //Custom Fields end
    }


    public function actionLoadChatMessage()
    {
    $chatid = Yii::$app->request->post('chatid');
    $chatModel = Chat::findOne(['chat_id'=>$chatid]);
    $showlead = Leads::showleads($chatid);
      if($showlead==1){
        return json_encode(['success' => true, 'data' => $chatModel->chatMessageArray]);
      }else{
        return json_encode(['success' => true, 'data' => 'Expired']);
      }
      return json_encode(['success' => false, 'message' => 'There are no messages avaliable for this chat']);
    }
    
    public function actionLoadCustomField(){
      $chatid = Yii::$app->request->post('chat_id');
      $leadModel = Leads::findOne(['chat_id'=>$chatid]);
      $showlead = Leads::showleads($chatid);
      if($showlead==1){
        if(!empty($leadModel->custom_fields)){
          $custom_fields = json_decode($leadModel->custom_fields,true);
          $customdata = [];
          $multipleChoiceString = '';
          foreach($custom_fields as $key => $custom_field){
            $custom_table = CustomLeadFields::findOne($custom_field['field_id']);
            if($custom_field['field_type']=='textbox'){
              $fieldvalue = $custom_field['field_value'];
              $customdata[$key]['label'] = htmlspecialchars_decode($custom_table['label']);
              $customdata[$key]['field_value'] = urldecode($fieldvalue);
            }elseif ($custom_field['field_type']=='dropdown'){
              $dropDownOption = CustomLeadFieldsOption::findOne($custom_field['field_value']);
              $fieldvalue2 = $dropDownOption['option'];
              $customdata[$key]['label'] = htmlspecialchars_decode($custom_table['label']);
              $customdata[$key]['field_value'] = $fieldvalue2;
            }elseif ($custom_field['field_type']=='multiple_choice'){
              $custom_field_id_arrays = explode(',', urldecode($custom_field['field_value']));
              $multipleChoiceOptions = CustomLeadFieldsOption::findAll($custom_field_id_arrays);
              foreach($multipleChoiceOptions as $multipleChoiceOption){
                $multipleChoiceString .= $multipleChoiceOption['option'].',';
              }
              //print_r($multipleChoiceOption);exit;
               $fieldvalue3 = rtrim($multipleChoiceString,', ');
               $customdata[$key]['label'] = htmlspecialchars_decode($custom_table['label']);
               $customdata[$key]['field_value'] = $fieldvalue3;

            }
          }
          return json_encode(['success' => true, 'data'=>$customdata]);
      }else{
        return json_encode(['success' => false, 'message' => 'There are no custom fields avaliable for this chat']);
      }
      }else{
        return json_encode(['success' => 'Expired', 'data' => 'Expired']);
      }
    }
    
    public function actionSaveCustomFields(){
       $custom_filled_data = Yii::$app->request->post('custom_filled_data');
       $chat_id = Yii::$app->request->post('chat_id');
       if(!isset($chat_id)){
         return json_encode(['success' => false, 'message' => 'Chat-id Invalid']);
       }
       if(isset($custom_filled_data)){
         $lead = Leads::findOne(['chat_id'=>$chat_id]);
         $lead->custom_fields = json_encode($custom_filled_data);
         if($lead->save()){
           return json_encode(['success' => true, 'message' => 'Custom Data Saved Successfully']);
         }else{
           return json_encode(['success' => false, 'message' => 'Save Error']);
         }
       }else{
         return json_encode(['success' => false, 'message' => 'Invalid Custom Fields']);
       }
    }

    // public function actiontest(){
    //   $chat_id = 'P8A8HUWCJG'
    //   try {
    //     $API = new LiveChat_API();
    //     $chat2 = $API->chats->getSingleChat($chat_id);
    //   }
    //   catch (Exception $e) {
    //     throw new NotFoundHttpException($e->getMessage());
    //   }
    // }

    public function actionForwardEmailList(){
      $forwardingEmailAddress = ForwardingEmailAddress::findAll(['user_id' => Yii::$app->user->identity->id]);
      $forwardingEmailTeams = ForwardingEmailTeam::findAll(['user_id' => Yii::$app->user->identity->id]);
      //
      $tr = '';
      $teams ='';
      if($forwardingEmailAddress){
        foreach ($forwardingEmailAddress as $value) {
          $tr .= '<tr><td><label class="custom-control custom-checkbox"><input data-team="'.$value->team.'" type="checkbox" value="'.$value->id.'" class="custom-control-input forward-email-check"><span class="custom-control-indicator"></span><span class="custom-control-description"></span></label></td><td>'.ucfirst($value->name).'</td><td>'.$value->email.'</td><td>'.$value->forwardingTeam->name.'</td></tr>';
        }
        
        foreach ($forwardingEmailTeams as $team) {
            $teams .= '<li><label class="custom-control custom-checkbox"><input data-id="'.$team->id.'" type="checkbox" value="'.$team->id.'" class="custom-control-input forward-email-team"><span class="custom-control-indicator"></span><span class="custom-control-description">'.ucwords($team->name).'</span></label></li>';
        } 
        
        if(!empty($teams)) {
             $teams = '<h4>Send to entire teams</h4><ul style="padding: 0;list-style:none;">'. $teams . '</ul>';           
        }
        
        return json_encode(['success' => true, 'data' => $tr, 'team' => $teams]);

      }else{
        return json_encode(['success' => false, 'data' => 'There are no Forwarding Email']);
      }

    }

    public function actionTimestamp()
    {
      $id = 51710;
      $chatModel = $this->findModel($id);
      $packageInfo = $this->findPackage();
      $showLead = $this->findPackageInfo($packageInfo,$chatModel);
      //echo '<pre>';   print_r($chatModel['chatMessageArray'][0]['timestamp']);exit;
      //echo '<pre>';   print_r($showLead);exit;
      //print_r(strtotime("-6 days")); exit;
    }
    
    public function actionSendPastLeadEmails()
    {
      $connection = Yii::$app->getDb();
      $command = $connection->createCommand('
            select * from leads where id > 48463 and id < 48683 and is_lead=1 and is_service_chat=0 order by id DESC');

      $records = $command->queryAll();

      foreach ($records as $post) {

        $visitor_name = $post['visitor_name'];
        $is_service_chat = $post['is_service_chat'];
        $is_lead = $post['is_lead'];
        $visitor_last_name = $post['visitor_last_name'];
        $visitor_email = $post['visitor_email'];
        $visitor_phone = $post['visitor_phone'];
        $preferred_contact_time = $post['preferred_contact_time'];
        $category_id = $post['category_id'];
       // $sub_category_id = $post['sub_category_id'];
        $outlet_id = $post['outlet_id'];
       // $outlet_text = $post['outlet_text'];
        $tags = $post['tags'];
        $chat_id = $post['chat_id'];
        $chat_summary = $post['chat_summary'];
       /* if(isset($post['custom_filled_data'])) {
          $custom_filled_data = $post['custom_filled_data'];
        } */
        $group_id = $post['user_group'];
        $created_by = $post['created_by'];
        $action = 'Updated';


        $lead = Leads::findOne(['chat_id' => $chat_id]);
        $email_sent = 0;

        //old saved tags
        $old_saved_tags = $lead->tags;

        $email_count = 0;

        if($lead)
        { 
          $userList = User::find()
                  ->where(['and', ['role' => [User::ROLE_USER, User::ROLE_FRANCHISE, User::ROLE_ASSIST], 'user_group' => $group_id,'status' => User::STATUS_ACTIVE], ['is', 'outlet', new \yii\db\Expression('null')]])->all();  

          //echo '<pre>';
          //print_r($userList);
          $API = new LiveChat_API();
          $leadChat = $API->chats->getSingleChat($chat_id);

          $sent_email_array = [];
          $sent_email_string = '';
          //Leads Details//
          $name = $visitor_name;
          $email = $visitor_email;
          $phone = $visitor_phone;
          $preferred_contact_time = $preferred_contact_time;
          $chat_summary = $lead->chat_summary;
          $chat2 = $leadChat;
          //Leads Details//
          $sent_email_string1='';

          $showLead = 0;//Initialized for main account email sending
          if($is_service_chat==1){
            //$showLead = Leads::showleads($chat_id);
            //$showLead = Leads::shownewleads($chat_id,$user);
            $showLead = 1;
            if($email_sent==0)
            {
              foreach ($userList as $user) {
                  $send_to_account_owner = 0;
                //$showLead = Leads::shownewleads($chat_id,$user);
                $notification = Notification::find()->where(['user_id'=>$user->id])->one();
                if(!empty($notification)){
                  if($notification->notify_chat==1){
                    if($notification->different_email==1){
                        if(!empty($notification->notify_chat_email))
                        {
                          $notify_emails = explode(", ",$notification->notify_chat_email);
                          foreach($notify_emails as $notify_email){
                            $leadModel = $lead;
                            $service_chat_emails_send = Leads::sendServicechatAllEmail($user, $chat2, $showLead, $name, $email, $leadModel, $notify_email);
                            $email_count++;
                            $sent_email_array[] = ['type' => 'Service chat-different' , 'email' => $notify_email];
                          }
                        }else {
                            $send_to_account_owner = 1;
                        }
                      $sent_email_string1 = $notification->notify_chat_email;
                    }else{
                        //if no different email
                        if(!empty($notification->notify_email)){
                          $notify_emails = explode(", ",$notification->notify_email);
                          foreach($notify_emails as $notify_email){
                            $leadModel = $lead;
                            $service_chat_emails_send = Leads::sendServicechatAllEmail($user, $chat2, $showLead, $name, $email, $leadModel, $notify_email);
                            $email_count++;
                            $sent_email_array[] = ['type' => 'Service chat-same' , 'email' => $notify_email];
                          }
                        }else {
                            $send_to_account_owner = 1;
                        }
                      $sent_email_string1 = $notification->notify_email;
                    }
                    $sent_email_string = $sent_email_string.$sent_email_string1.', ';
                  }
                }else {
                    //this user is probably new and does not have notification entry in notification table
                    $send_to_account_owner = 1;
                }

                if($send_to_account_owner == 1) {
                    $sent = Leads::sendEmailServicechat($user, $leadChat, $chat2, $showLead, $name, $email, $phone, $chat_summary);
                    $email_count++;
                    $sent_email_array[] = ['type' => 'Service chat-Owner-Email' , 'email' => $user->email];
                    $sent_email_string .= $user->email . ', ';
                }
              }
            }

            if($lead->email == 0){
              $lead->email = 1;
              $lead->save();
            }
          }
          else if($is_lead == 1){
            //$showLead = Leads::shownewleads($chat_id,$user);;
            //$showLead = 1;
            if($email_sent==0)
            {
              foreach ($userList as $user) {
                  $send_to_account_owner = 0;
                  //echo '=========================';
                  //echo '<pre>';print_r($user);
                $showLead = Leads::shownewleads($chat_id,$user);
                //var_dump($showLead);
              $notification = Notification::find()->where(['user_id'=>$user->id])->one();
              if(!empty($notification)){
                if($notification->notify_lead==1){
                  if($notification->different_email==1){
                      if(!empty($notification->notify_lead_email))
                      {
                        $notify_emails = explode(", ",$notification->notify_lead_email);
                        foreach($notify_emails as $notify_email){
                          $leadModel = $lead;
                          $service_chat_emails_send = Leads::sendForwardingEmail($user, $chat2, $showLead, $name, $email, $leadModel, $notify_email);
                          $email_count++;
                          $sent_email_array[] = ['type' => 'Lead chat-different' , 'email' => $notify_email];
                        }
                      }else {
                          $send_to_account_owner = 1;
                      }
                    $sent_email_string1 = $notification->notify_lead_email;
                  }else{
                      if(!empty($notification->notify_email))
                      {
                        $notify_emails = explode(", ",$notification->notify_email);
                        foreach($notify_emails as $notify_email){
                          $leadModel = $lead;
                          $service_chat_emails_send = Leads::sendForwardingEmail($user, $chat2, $showLead, $name, $email, $leadModel, $notify_email);
                          $email_count++;
                          $sent_email_array[] = ['type' => 'Lead chat-same' , 'email' => $notify_email];
                        }
                      }else {
                          $send_to_account_owner = 1;
                      }
                    $sent_email_string1 = $notification->notify_email;
                  }

                  $sent_email_string = $sent_email_string.$sent_email_string1.', ';
                }
              }else {
                  //if user is new and does not have any notification entry in the notification table
                  $send_to_account_owner = 1;
              }

              if($send_to_account_owner == 1)
              {
                  $sent = Leads::sendEmailAgent($user, $leadChat, $chat2, $showLead, $name, $email, $phone, $chat_summary);
                  $email_count++;
                  $sent_email_array[] = ['type' => 'Leads chat-Owner-Email' , 'email' => $user->email];
                  $sent_email_string .= $user->email . ', ';
              }
            }
          }
            if($lead->email == 0){
              $lead->email = 1;
              $lead->save();
            }
          }

          //currently mails are sent to users only in case of lead or service chat
          if($is_service_chat == 1 || $is_lead == 1) {
              if($sent_email_string != ''){
                  $emailLog = new EmailLog();
                  $emailLog->leads_id = $lead->id;
                  $emailLog->chat_id = $lead->chat_id;
                  $emailLog->sent_to = rtrim($sent_email_string,', ');
                  $emailLog->sent_via = EmailLog::VIA_LEAD_SENDER;
                  $tempUser = User::findOne(['user_group' => $lead->user_group]);
                  if($tempUser){
                      $emailLog->web_url = $tempUser->website_url;
                  }
                  $emailLog->save();
              }
          }

          echo 'success';
        }
        echo 'failed';

      }
    }
}
