<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\User;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LiveChat_API;
use app\models\AppsCountries;
use app\models\BusinessCategory;

/**
 * Load controller
 */
class InstallController extends Controller
{

	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [ 'step', 'step1', 'step2', 'step4', 'step5'],
                'rules' => [

                    [
                        'actions' => [ 'step', 'step1', 'step2', 'step4', 'step5'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }
    
    public function beforeAction($action)
    {
        if(isset($_GET['wp']) && isset($_GET['api_key']) && isset($_GET['email'])) { 
    		$user = User::findOne(['signup_key' => Yii::$app->request->get('api_key'), 'email' => Yii::$app->request->get('email')]);
            if(Yii::$app->user->login($user, 0)) {
                $user->signup_key = '';
                $user->save();
                Yii::$app->session->set('wp_signup', Yii::$app->request->get('wp'));
            }
    	}
        return parent::beforeAction($action);
    }

	public function actionStep(){
		$this->layout = 'kosmosetup';
		return $this->render('step');
	}

	public function actionStep1(){
		$this->layout = 'kosmosetup';
		$success	  = "";
		$error		  = "";

		$model 					= User::findOne(Yii::$app->user->id);
		if( isset( $_POST['save'] ) ){
			if($_POST['User']['email'] != '' && $_POST['User']['your_name'] != ''){
				$model->email 		= $_POST['User']['email'];
				$model->your_name 	= $_POST['User']['your_name'];
				//$model->password_hash = Yii::$app->security->generatePasswordHash($_POST['password']);
				if($model->save()){
					$success = "Your data has been saved successfully.";
					return $this->redirect(['/install/step2']);
				}
			}else{
					$error = "Please enter all data.";
			}
		}

		return $this->render('step1', ['success' => $success, 'error' => $error, 'model' => $model]);
	}

	public function actionStep2(){
		$this->layout = 'kosmosetup';
		$success	  = "";
		$error		  = "";

		$model 					= User::findOne(Yii::$app->user->id);
    //echo '<pre>'; print_r($model); exit;
		if( isset( $_POST['save'] ) ){
			if($_POST['User']['website_url'] != ''){
				$model->business_name 		 = $_POST['User']['business_name'];
				$model->website_url 		 = $_POST['User']['website_url'];

        if($model->user_group == 0 && $model->business_name != '')
        {
          //echo '<pre>'; print_r($model); exit;
          $API = new LiveChat_API();

          $allAgents = $API->agents->getAllAgents();
          $agentArray = [];
          foreach ($allAgents as $agent) {
            $agentArray[] = $agent->login;
          }
          //echo '<pre>';  print_r($agentArray); exit;
          $response = $API->groups->addGroup($model->business_name, $agentArray);
          $response = json_decode($response);
          $model->user_group = $response->id;
        }

				if( isset( $_POST['chat_purpose_sales'] ) ){
					$model->chat_purpose_sales	 = 1;
				}
				if( isset( $_POST['chat_purpose_support'] ) ){
					$model->chat_purpose_support = 1;
				}
				if( isset( $_POST['chat_purpose_e_commerce'] ) ){
					//print_r()
					$model->chat_purpose_e_commerce = 1;
				}
				if($model->save()){
          //echo '<pre>'; print_r($model); exit;
					$success = "Your data has been saved successfully.";
					return $this->redirect(['/install/step3']);
				}
        print_r($model->getErrors()); exit;
			}else{
					$error = "Please enter data.";
			}
		}
		$model 					= User::findOne(Yii::$app->user->id);
		return $this->render('step2', ['success' => $success, 'error' => $error, 'model' => $model]);
	}

	public function actionStep3(){
		$this->layout = 'kosmosetup';
		$success	  = "";
		$error		  = [];

		$model 					= User::findOne(Yii::$app->user->id);
		if( isset( $_POST['save'] ) ){
			if($_POST['industry'] == ""){
				$error[] = "Please Enter Your Industry.<br>";
			}
			if($_POST['number_of_employees'] == ""){
				$error[] = "Please Enter Number of Employees.<br>";
			}
			if($_POST['country'] == ""){
				$error[] = "Please Enter Your Country.<br>";
			}

			if( empty( $error ) ){
				$model->industry 				= $_POST['industry'];
				$model->number_of_employees 	= $_POST['number_of_employees'];
				$model->country 				= $_POST['country'];
				$model->telephone 				= $_POST['telephone'];

				if( isset( $_POST['b2b'] ) ){
					$model->b2b	 = 1;
				}else{
          $model->b2b	 = 0;
        }

				if( isset( $_POST['b2c'] ) ){
					$model->b2c	 = 1;
				}else{
          $model->b2c	 = 0;
        }

				if( isset( $_POST['internal_use'] ) ){
					$model->internal_use	 = 1;
				}else{
          $model->internal_use	 = 0;
        }

				if($model->save()){
          			$model->notifySignupEmail();
					$success = "Your data has been saved successfully.";
					if(Yii::$app->session->has('wp_signup')) {
						return $this->redirect(['site/launch']);
					} else {
						return $this->redirect(['/install/step4']);
					}					
				}
			}else{
				$model->industry 				= $_POST['industry'];
				$model->number_of_employees 	= $_POST['number_of_employees'];
				$model->country 				= $_POST['country'];
				$model->telephone 				= $_POST['telephone'];
			}
		}
		$model 					= User::findOne(Yii::$app->user->id);
		$countries				= AppsCountries::find()->all();
		$categories				= BusinessCategory::find()->all();

		return $this->render('step3', ['success' => $success, 'error' => $error, 'model' => $model, 'countries' => $countries, 'categories' => $categories]);
	}

	public function actionStep4(){
		$this->layout = 'kosmooutersetup';

		$success	  = "";
		$error		  = [];

		$model 		  = User::findOne(Yii::$app->user->id);
		$request = Yii::$app->request;
		$get = $request->get();

		  $html     = "";
			$html 		= $html . "Hi Team,<br><br>";
			$html 		= $html . "Exciting, We have received another new signup to Chat Metrics.<br>";
			$html 		= $html . "Let's get them setup for more leads right away.<br><br>";
			$html 		= $html . "Client details:<br>";
			$html 		= $html . "Email : " . Yii::$app->user->identity->email ." <br>";
			$html 		= $html . "<a href='https://chat-application.com/frontend/web/index.php?r=user%2Fview&id=" .  Yii::$app->user->id . "'><input type='button' style='color:green;' value='VIEW CLIENT DETAILS'></a>";

		if( isset( $_REQUEST['email'] ) ){
			//echo "ddd";exit;
			 if($_POST['email'] == ""){
				$error[] = "Please Enter Your Email.<br>";
			}
			if($_POST['message'] == ""){
				$error[] = "Please Enter Message.<br>";
			}
			if( empty( $error ) ){
            $message = nl2br($_POST['message']);
  		    $html2 		= $message;
            $html2     = $html2 . '&lt;script src="http://chat-application.com/embed/index.php?tracker_id='.Yii::$app->user->identity->tracker_id.'&gt;&lt;/script&gt;';
            
				/*
				\Yii::$app->mailer->compose(['html' => $_POST['message'], 'text' => 'leadnew-text'])
	                    ->setFrom([\Yii::$app->params['supportEmail'] => 'Chat Metrics']) // mail from
	                    ->setTo($_POST['email'])
	                    ->setSubject("Install Chat Metrics.")
	                    ->send();
				*/
				Yii::$app->mailer->compose()
					->setTo($_POST['email'])
					->setFrom([\Yii::$app->params['supportEmail'] => 'Chat Metrics']) // mail from
					->setSubject("Install Chat Metrics.")
					->setHtmlBody( $html2 )
					->send();
			}
		}else if(isset($_REQUEST['let_chat_metrics_do_it'])){
			Yii::$app->mailer->compose()
					->setTo(Yii::$app->params['supportEmail'])
					->setFrom([\Yii::$app->params['supportEmail'] => 'Chat Metrics']) // mail from
					->setSubject("Install Chat Metrics.")
					->setHtmlBody( $html )
					->send();
			return $this->redirect(['site/index']);
		}else{
          //print_r($html); exit;

			    $model = User::findOne(Yii::$app->user->id);
					Yii::$app->mailer->compose()
					->setTo(Yii::$app->params['supportEmail'])
					->setFrom([\Yii::$app->params['supportEmail'] => 'Chat Metrics']) // mail from
					->setSubject("Hurray! New Chat Metrics Sign Up.")
					->setHtmlBody( $html )
					->send();

		}

		return $this->render('step4', ['success' => $success, 'error' => $error, 'model' => $model]);
	}

	public function actionStep5(){
		$this->layout = 'kosmooutersetup';
		return $this->render('step5');
	}

	public function actionStep6(){
		$this->layout = 'kosmooutersetup';
		return $this->render('step6');
	}

	public function actionStep7(){
		$this->layout = 'kosmooutersetup';
		if( isset( $_POST['save'] ) ){
			return $this->redirect(['step8']);
		}
		return $this->render('step7');
	}

	public function actionStep8(){
		$this->layout = 'kosmooutersetup';
		$success	  = "";
		$error		  = "";

		$model 					= User::findOne(Yii::$app->user->id);
		if( isset( $_POST['save'] ) ){
			if($_POST['website_url'] != ''){
				$model->website_url 	= $_POST['website_url'];
				if($model->save()){
					$success = "Your data has been saved successfully.";
					return $this->redirect(['/site/index']);
				}
			}else{
					$error = "Please enter Website Url.";
			}
		}
		return $this->render('step8', ['success' => $success, 'error' => $error, 'model' => $model]);
	}

}
