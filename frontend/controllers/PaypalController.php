<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\User;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
 

/**
 * Site controller
 */
class PaypalController extends Controller
{
	
	public function actionSuccess(){
		$this->layout = 'outer';
		return $this->render('success');
	}
	
	public function actionError(){
		$this->layout = 'outer';
		return $this->render('error');		
	}
	
	
	
}