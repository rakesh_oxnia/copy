<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\ZapierRequestLogs;

class CronRemoveOldLogController extends Controller
{
    public function actionRemoveOldZapierRequestLog()
    {
        //fetch timestamp of one month prior
        $timestamp = strtotime(date('Y-m-d 00:00:00', strtotime('-30 days')));
        ZapierRequestLogs::deleteAll('created_at < '.$timestamp);
    }
}
