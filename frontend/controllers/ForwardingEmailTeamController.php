<?php

namespace frontend\controllers;

use Yii;
use common\models\ForwardingEmailTeam;
use frontend\models\ForwardingEmailTeamSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;
use common\models\User;

/**
 * ForwardingEmailTeamController implements the CRUD actions for ForwardingEmailTeam model.
 */
class ForwardingEmailTeamController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],                      
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('manageForwardingEmails');                     
                        }                     
                    ]                   
                ],
            ],            
        ];
    }

    public function beforeAction($action)
    {
        if(!parent::beforeAction($action)) {
            return false;
        }

        if(Yii::$app->user->isGuest){
          return $this->redirect(['site/index']);
        }

        if(Yii::$app->user->identity->role == User::ROLE_ASSIST) {     
            $user_id = Yii::$app->user->identity->id;
            $parent = User::findOne(Yii::$app->user->identity->parent_id);   
            yii::$app->user->login($parent);
            Yii::$app->session->set('user_assist', $user_id);       
        }

        $this->layout = 'kosmoouter';
        return true;
    }

    public function render($view, $params)
    {
        if(Yii::$app->session->has('user_assist')) { 
          $user = User::findOne(Yii::$app->session->get('user_assist'));  
          Yii::$app->session->remove('user_assist');             
          yii::$app->user->login($user);             
        }

        // before render
        return parent::render($view, $params);
    }


    public function actionIndex()
    {
        $searchModel = new ForwardingEmailTeamSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $teams = ForwardingEmailTeam::findAll(['user_id' => Yii::$app->user->identity->id]);
        $teamCount = count($teams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'teamCount' => $teamCount,
        ]);
    }

    /**
     * Displays a single ForwardingEmailTeam model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ForwardingEmailTeam model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ForwardingEmailTeam;

        $forwardingTeams = ForwardingEmailTeam::findAll(['user_id' => Yii::$app->user->identity->id]);

        if(count($forwardingTeams) > 1 ){
            throw new ForbiddenHttpException("You cannot add more than 2 teams");
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->identity->id;
            $model->user_group = Yii::$app->user->identity->user_group;
            $model->save();
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ForwardingEmailTeam model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $params = Yii::$app->request->post(); 
        $params['ForwardingEmailTeam']['user_id'] = Yii::$app->user->identity->id;

        if ($model->load($params) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ForwardingEmailTeam model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ForwardingEmailTeam model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ForwardingEmailTeam the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ForwardingEmailTeam::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
