<?php

namespace frontend\controllers;

use Yii;
use app\models\User2;
use common\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use frontend\models\Invoices;
use app\models\Leads;
use common\models\Packages;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\PasswordResetRequestForm;
use common\models\LiveChat_API;
use yii\filters\AccessControl;
use yii2tech\csvgrid\CsvGrid;
use yii\data\ArrayDataProvider;
use common\models\ForwardingEmailTeam;
use common\models\Chat;

//require(\Yii::getAlias('@app') . '/../g_php/LiveChat_API.php');

/**
 * ChatController implements the CRUD actions for User2 model.
 */
class ChatController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
				'class'=> AccessControl::className(),
				'only' => ['index','view','survey'],
				'rules'=>[
					[
						'actions' => ['index','view','survey'],
						'allow' => true,
						'roles'=>['@'],
					]
				]
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    //public $layout = "dashboard";
	   public $layout = "kosmoouter";

    /**
     * Lists all .
     * @return mixed
     */
    public function actionIndex2()
    {
      if(isset($_GET['filter'])){
        $fromdate = strtotime("00:00:00", strtotime(Yii::$app->request->get('date_from')));
        $todate = strtotime("23:59:59", strtotime(Yii::$app->request->get('date_to')));
      }else{
        $fromdate = strtotime('-6 days');
        $todate = time();
      }

      $this->layout = 'kosmleadsandchats';
      $chats = Chat::find()->where(['user_group'=>Yii::$app->user->identity->user_group])->andWhere(['>=','ended_timestamp',$fromdate])
                        ->andWhere(['<=','ended_timestamp',$todate])
                        ->orderBy(['ended_timestamp'=>SORT_DESC])->all();
      //echo "<pre>"; print_r($chats); exit;
      return  $this->render('index2',['chats' => $chats]);
    }

    public function actionExportChat(){
    if(isset($_GET['filter'])){
      $fromdate = strtotime("00:00:00", strtotime(Yii::$app->request->get('date_from')));
      $todate = strtotime("23:59:59", strtotime(Yii::$app->request->get('date_to')));
    }else{
      $fromdate = strtotime('-6 days');
      $todate = time();
    }
    $chats = Chat::find()->where(['user_group'=>Yii::$app->user->identity->user_group])
                         ->andWhere(['>=','ended_timestamp',$fromdate])
                         ->andWhere(['<=','ended_timestamp',$todate])
                         ->orderBy(['ended_timestamp'=>SORT_DESC])->all();
   $rnd = rand();
   $filename = "chat$rnd.csv";
   foreach($chats as $key => $chat){
     //echo '<pre>';  print_r($chat['leads']['status']);exit;
     $line = [];
     $line['Name'] = isset($chat['leads']['visitor_name'])?$chat['leads']['visitor_name']:'-';
     $line['Email'] = isset($chat['leads']['visitor_name'])?$chat['leads']['visitor_email']:'-';
     $line['City'] = $chat['city'];
     //$line['Category'] = isset($chat['category']) ? $chat['category']['name'] : 'Not Set';
     $line['State'] = $chat['region'];
     $line['Country'] = $chat['country'];
     $line['Date'] = date('d-m-Y', $chat['ended_timestamp']);
     $line['Action Status'] = ucfirst($chat['leads']['status']);

     $file = fopen($filename,"a");
     fputcsv($file, $line);
   }
   header("Cache-Control: public");
   header("Content-Description: File Transfer");
   header("Content-Disposition: attachment; filename=$filename");
   header("Content-Type: application/csv");
   header("Content-Transfer-Encoding: binary");

   // read the file from disk
    readfile($filename);
   //return json_encode(['success' => true, 'data' => $filename]);
    //echo "ssdf";exit;
  }


    public function actionIndex($filter = '', $date_from = '', $date_to = '', $group_by = '', $page = '')
    {
      $this->layout = 'kosmoouter';
      //echo "<pre>"; print_r(Yii::$app->user); print_r($_SESSION); exit;
  		$page 			= 1;
  		$totalpages 	= 0;
  		$freeLeads 		= 5;
  		$filter 		= "";
  		$date_from 		= "";
  		$date_to 		= "";
  		$group_by 		= "";
  		$packageInfo 	= [];
  		$chatsData		= [];
  		$this->layout 	= "kosmleadsandchats";
      //$this->layout = 'kosmoouter';
  		$user_id 		= Yii::$app->user->identity->id;

  		$invoiceData = Invoices::find()->where(['user_id' => $user_id])->all();
  		if( !empty( $invoiceData ) ){
  			foreach( $invoiceData as $key => $invoice){
  				if($invoice['package_id'] > 0){

  					$package_id  = $invoice['package_id'];
  					$packageData = Packages::findOne( $package_id );

  					if( !empty( $packageData ) ){
  						$packageInfo[$invoice['payment_date']] = $packageData['number_of_leads'];
  						Yii::$app->session[$invoice['payment_date']] = 0;
  					}
  				}
  			}
  		}

      try {
    		$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    		if(!isset($_GET["date_from"])){
    			$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
    			$_GET['date_to'] = date('Y-m-d');
    		}else{
    			$filter = $_GET['filter'];
    			$date_from = $_GET['date_from'];
    			$date_to = $_GET['date_to'];
    			$group_by = $_GET['group_by'];
    			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    		}

    		$_GET["group"] = Yii::$app->user->identity->user_group;
    		$API = new LiveChat_API();
    		$chats = $API->chats->get($page,$_GET);
    	}
    	catch (Exception $e) {
    		throw new NotFoundHttpException($e->getMessage());
    	}

    	//echo "<pre>"; print_r($chats->chats); exit;

  		$transcripts = $chats->chats;
  		$total = $chats->total;
  		$pages = $chats->pages;


  		$chatsData = Leads::filterLeadPageData( $transcripts, $chatsData );

  		for($page = 2; $page <= $pages; $page++){
  			$API 			= new LiveChat_API();
  			$_GET['page']	= $page;
  			$chats 			= $API->chats->get($page,$_GET);
  			$transcripts 	= $chats->chats;
  			$chatsData 			= Leads::filterLeadPageData( $transcripts, $chatsData );
  		}

  		$chatsData = array_reverse( $chatsData );

  		$chatsData = Leads::getAllChats($chatsData, $packageInfo);

  		$chatsData = array_reverse( $chatsData );

  		// added by Ravindra
      //echo "<pre>"; print_r($chatsData); exit;

  		foreach ($chatsData as $key => $chat) {

          $leadModel = Leads::findOne(['chat_id' => $chat['id']]);

          // name and email part
            if($leadModel){
	            $chatsData[$key]['name'] = $leadModel->visitor_name == NULL ? '' : $leadModel->visitor_name;
	            if($chatsData[$key]['showLead'] != 1){
	              $chatsData[$key]['name'] = Leads::obfuscate_name($chatsData[$key]['name']);
	            }

	            $chatsData[$key]['email'] = $leadModel->visitor_email == NULL ? '' : $leadModel->visitor_email;
	            if($chatsData[$key]['showLead'] != 1){
	              $chatsData[$key]['email'] = Leads::obfuscate_email($chatsData[$key]['email']);
	            }
            }
          // name and email part
        }

      $forwardingEmailTeams = [];
      $forwardingEmailTeams = ForwardingEmailTeam::findAll(['user_id' => Yii::$app->user->identity->id]);
      // added by Ravindra

  		return $this->render('index', [
        'chats' => $chatsData,
        'total' => $total,
        'pages' => $pages,
  			'userdata'=>Yii::$app->user->identity,
  			'filter' => $filter,
  			'date_from' => $date_from,
  			'date_to' => $date_to,
  			'group_by' => $group_by,
  			'totalpages' => $totalpages,
  			'page' => $page,
        'forwardingEmailTeams' => $forwardingEmailTeams,

          ]);
    }

	public function actionExport($filter = '', $date_from = '', $date_to = '', $group_by = '', $page = ''){
		$page 			= 1;
		$totalpages 	= 0;
		$freeLeads 		= 5;
		$filter 		= "";
		$date_from 		= "";
		$date_to 		= "";
		$group_by 		= "";
		$packageInfo 	= [];
		$chatsData		= [];
		$this->layout 	= "kosmleadsandchats";
		$user_id 		= Yii::$app->user->identity->id;

		$invoiceData = Invoices::find()->where(['user_id' => $user_id])->all();
		if( !empty( $invoiceData ) ){
			foreach( $invoiceData as $key => $invoice){
				if($invoice['package_id'] > 0){

					$package_id  = $invoice['package_id'];
					$packageData = Packages::findOne( $package_id );

					if( !empty( $packageData ) ){
						$packageInfo[$invoice['payment_date']] = $packageData['number_of_leads'];
						Yii::$app->session[$invoice['payment_date']] = 0;
					}
				}
			}
		}

        try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			if(!isset($_GET["date_from"])){
				$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
				$_GET['date_to'] = date('Y-m-d');
			}else{
				$filter = $_GET['filter'];
				$date_from = $_GET['date_from'];
				$date_to = $_GET['date_to'];
				$group_by = $_GET['group_by'];
				$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			}

			$_GET["group"] = Yii::$app->user->identity->user_group;
			$API = new LiveChat_API();
			$chats = $API->chats->get($page,$_GET);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}

		$transcripts = $chats->chats;
		$total = $chats->total;
		$pages = $chats->pages;


		$chatsData = Leads::filterLeadPageData( $transcripts, $chatsData );

		for($page = 2; $page <= $pages; $page++){
			$API 			= new LiveChat_API();
			$_GET['page']	= $page;
			$chats 			= $API->chats->get($page,$_GET);
			$transcripts 	= $chats->chats;
			$chatsData 			= Leads::filterLeadPageData( $transcripts, $chatsData );
		}

		$chatsData = array_reverse( $chatsData );

		$chatsData = Leads::getAllChats($chatsData, $packageInfo);

		$chatsData = array_reverse( $chatsData );

      //echo "<pre>"; print_r($chatsData); exit;

			foreach($chatsData as $key => $chat){

        $leadModel = Leads::findOne(['chat_id' => $chat['id']]);

        // name and email part
          if($leadModel){
            $chatsData[$key]['name'] = $leadModel->visitor_name == NULL ? '' : $leadModel->visitor_name;
            if($chatsData[$key]['showLead'] != 1){
              $chatsData[$key]['name'] = Leads::obfuscate_name($chatsData[$key]['name']);
            }

            $chatsData[$key]['email'] = $leadModel->visitor_email == NULL ? '' : $leadModel->visitor_email;
            if($chatsData[$key]['showLead'] != 1){
              $chatsData[$key]['email'] = Leads::obfuscate_email($chatsData[$key]['email']);
            }
          }else{
            if($chatsData[$key]['showLead'] != 1){
              $chatsData[$key]['name'] = Leads::obfuscate_name($chatsData[$key]['name']);
              $chatsData[$key]['email'] = Leads::obfuscate_email($chatsData[$key]['email']);
            }
          }
        // name and email part

        // assign name and email to current var
        $chat['name'] = $chatsData[$key]['name'];
        $chat['email'] = $chatsData[$key]['email'];
        // assign name and email to current var

				$allModels[$key]['Name'] = $chat['name'];
				$allModels[$key]['Email'] = $chat['email'];
				$allModels[$key]['City'] = $chat['city'];
				$allModels[$key]['State'] = $chat['region'];
				$allModels[$key]['Country'] = $chat['country'];
				$allModels[$key]['Date'] = $chat['started_timestamp'];

			}

      //echo "<pre>"; print_r($allModels); exit;

		$exporter = new CsvGrid([
			'dataProvider' => new ArrayDataProvider([
				'allModels' => $allModels,
        'pagination' => [
            'pageSize' => 0, // export batch size
        ],
			]),
			'columns' => [
				[
					'attribute' => 'Name',
				],
				[
					'attribute' => 'Email',
				],

				[
					'attribute' => 'City',
				],
				[
					'attribute' => 'State',
				],
				[
					'attribute' => 'Country',
				],
				[
					'attribute' => 'Date',
				],


			],
		]);
		$exporter->export()->saveAs('chat.csv');
		$file = "chat.csv";
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=$file");
		header("Content-Type: application/csv");
		header("Content-Transfer-Encoding: binary");

		// read the file from disk
		readfile($file);
		 //echo "ssdf";exit;
	}


	function obfuscate_email($email)
	{
		$em   = explode("@",$email);
		$name = implode(array_slice($em, 0, count($em)-1), '@');
		$len  = floor(strlen($name)/2);

		return substr($name,0, $len) . str_repeat('*', $len) . "@" . end($em);
	}

	function obfuscate_name($name)
	{

		$len  = floor(strlen($name)/2);

		return substr($name,0, $len) . str_repeat('*', $len);
	}

    /**
     * Displays a single User2 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$this->layout = "kosmoouter";
		try {
			$API = new LiveChat_API();
			$chat = $API->chats->getSingleChat($id);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}

        return $this->render('view', [
            'chat' => $chat,
        ]);
    }

    /**
     * Creates a new User2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSurvey($id)
    {
		$this->layout = "kosmoouter";
        try {
			$API = new LiveChat_API();
			$chat = $API->chats->getSingleChat($id);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}
		//echo '<pre>';print_r($chat);echo '</pre>';
        return $this->render('survey', [
            'chat' => $chat,
        ]);
    }

    /**
     * Updates an existing User2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionAvailability()
    {
        try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			$_GET["group"] = Yii::$app->user->identity->user_group;
			if(!isset($_GET["date_from"])){
				$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
				$_GET['date_to'] = date('Y-m-d');
				$_GET['group_by'] = 'day';
			}

			$_GET['group'] = Yii::$app->user->identity->user_group;
			$API = new LiveChat_API();
			$availability = $API->reports->get("availability",$_GET);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}

		/* echo '<pre>';
print_r($availability);
echo '</pre>';die; */

		return $this->render('availability', [
			'availability' => $availability,
		]);
    }

	public function actionAvailability22()
    {
        try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			$_GET["group"] = Yii::$app->user->identity->user_group;
			if(!isset($_GET["date_from"])){
				$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
				$_GET['date_to'] = date('Y-m-d');
				$_GET['group_by'] = 'day';
			}

			$_GET['group'] = Yii::$app->user->identity->user_group;
			$API = new LiveChat_API();
			$availability = $API->reports->get("availability",$_GET);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}

		/* echo '<pre>';
print_r($availability);
echo '</pre>';die; */

		return $this->render('availability22', [
			'availability' => $availability,
		]);
    }

    /**
     * Deletes an existing User2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionTotal()
    {
        try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			$_GET["group"] = Yii::$app->user->identity->user_group;
			if(!isset($_GET['date_from'])){
				$_GET['date_from'] = '2016-04-01';
			}
			$_GET['group_by'] = 'month';
			$API = new LiveChat_API();
			$reports = $API->reports->get("chats",$_GET);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
			//die($e->getCode().' '.$e->getMessage());
		}

		/* $transcripts = $chats->chats;
		$total = $chats->total;
		$pages = $chats->pages */;

		return $this->render('total', [
            'reports' => $reports,
        ]);
    }

    /**
     * Finds the User2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    /* protected function findModel($id)
    {
        if (($model = User2::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    } */

	/* public function actionChangepassword(){
        $layout_ = $this->actionGetlayout();
        $this->layout = $layout_;
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->password_hash = Yii::$app->security->generatePasswordHash(Yii::$app->request->post()['new_password']);
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('changepassword', [
                'model' => $model,
            ]);
        }
    } */

    public function actionUnmarkLead()
    {
      if(Yii::$app->user->identity->role == User::ROLE_ADMIN){
        $request = Yii::$app->request;
        if($request->post('chat_id')) {
            $chat_id = $request->post('chat_id');

            if(isset($chat_id) && !empty($chat_id)) {
                $invoices = Invoices::find()->where(['chat_id' => $chat_id])->one();
                if(!$invoices) {
                    //check whether lead generated in the current month, if yes only then delete it
                    $lead = Leads::find()->where(['chat_id' => $chat_id])->one();
                    if($lead) {
                        $first_second = strtotime(date('Y-m-d 00:00:00', strtotime('first day of this month')));
                        $last_second = strtotime(date('Y-m-d 23:59:59', strtotime('last day of this month')));
                        //this is to make sure that this lead has not already been accounted for while package renewing
                        if($lead->c_time >= $first_second && $lead->c_time <= $last_second) {
                            $lead->delete();
                            Yii::$app->session->setFlash('success', "Chat was successfully unmarked as lead");
                            return $this->redirect(['/chat/unmark-lead']);
                        }else {
                            //lead not generated in current month, so can't delete it
                            Yii::$app->session->setFlash('error', "You can delete only those leads which are generated in the current month");
                            return $this->redirect(['/chat/unmark-lead']);
                        }
                    }else {
                        Yii::$app->session->setFlash('error', "Lead not found");
                        return $this->redirect(['/chat/unmark-lead']);
                    }
                }else {
                    //already paid, can't delete
                    Yii::$app->session->setFlash('error', "Payment for this lead has already been deducted using pay as you go, can't delete this lead");
                    return $this->redirect(['/chat/unmark-lead']);
                }
            }
        }

        return $this->render('unmark-lead');
      }
    }

}
