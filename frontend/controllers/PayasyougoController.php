<?php

namespace frontend\controllers;

use Yii;
use app\models\Payasyougo;
use frontend\models\PayasyougoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\BusinessCategory;
use app\models\Coupons;

/**
 * PayasyougoController implements the CRUD actions for Payasyougo model.
 */
class PayasyougoController extends Controller
{
	public $enableCsrfValidation = false;
	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
	
	public function actionList(){
		$error = "";	
		$success = "";
		$this->layout = 'kosmoouter';
		
		if( isset( $_POST['apply'] ) ){
			$coupon = $_POST['coupon'];
			$couponData	= Coupons::find()->where(['code' => $coupon])->one();
			if( !empty( $couponData ) ){
				if($couponData['status'] == '1'){
					$discount = $couponData['discount'];
					Yii::$app->session['discount'] = $discount;
					$success = "Coupon has been applied successfully.";
				}else{
					$error = "Sorry!! This Coupon has been expired now.";
				}
				
			}else{
					$error = "Sorry!! There is no such coupon.";
			}
		}
		$packages	= Payasyougo::find()->orderBy(['id' => SORT_DESC])->all();
		$categories = ArrayHelper::map(BusinessCategory::find()->all(), 'id', 'name');
		return $this->render('list', ['packages' => $packages, 'categories' => $categories, 'error' => $error, 'success' => $success]);
	}
	

    /**
     * Lists all Payasyougo models.
     * @return mixed
     */
    public function actionIndex()
    {
		$this->layout = 'dashboard';
        $searchModel = new PayasyougoSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Payasyougo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$this->layout = 'dashboard';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Payasyougo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$this->layout = 'dashboard';
        $model = new Payasyougo;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Payasyougo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		$this->layout = 'dashboard';
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Payasyougo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Payasyougo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payasyougo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payasyougo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
