<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Packages;
use common\models\User;
use app\models\Subscription;
use frontend\models\Invoice;
use common\models\StripeLog;

class AllocatePackageController extends Controller
{
      public function beforeAction($action)
      {
          if(Yii::$app->user->isGuest){
            return $this->redirect(['site/login']);
          }
          return parent::beforeAction($action);
      }

      public function actionAllocatePlan(){
        $user_id = Yii::$app->request->post('user_id');
        $user = User::findOne($user_id);
        if($user){
        $subscription_pack = Subscription::find()->where(['user_id'=>$user_id,'is_cancelled'=>0])->exists();
        if($subscription_pack) {
            return json_encode(['success'=>false,'data'=>'User already has package allocated to him']);
            exit;
        }

        $automatic_charge = Yii::$app->request->post('automatic_charge');
        if($automatic_charge == 1 && ($user->stripeToken == "" || is_null($user->stripeToken)))
        {
            return json_encode(['success'=>false,'data'=>'User Stripe Card has not been registered']);
            exit;
        }

        $new_invoice_id = 0;
        //if($user->stripeToken!=""){
          $packages_select = Yii::$app->request->post('packages_select');
          $custom_package_id = Yii::$app->request->post('custom_package_id');
          $gst_status = Yii::$app->request->post('gst_status');
          $gst_percent = Yii::$app->request->post('gst_percent');
          $advance_payment = Yii::$app->request->post('advance_payment');
          $show_previous_lead = Yii::$app->request->post('show_previous_lead');
          $change_plan_activation = Yii::$app->request->post('change_plan_activation');
          $plan_activation_date = Yii::$app->request->post('plan_activation_date');
          //+++++++++++++++++CUSTOM PACKAGE+++++++++++++++++++++++++++//
          if(isset($custom_package_id)){
            $custom_lead   = Yii::$app->request->post('custom_lead');
            $custom_amount = Yii::$app->request->post('custom_amount');
            $custom_currency = Yii::$app->request->post('custom_currency');
            $custom_package_insert = Packages::find()->where(['id'=>$custom_package_id,'user_id'=>$user_id,'is_cancelled'=>0])->one();
            if(empty($custom_package_insert)){
                  $custom_package_insert = new Packages();
                  $custom_package_insert->package_name = 'Custom Package';
                  $custom_package_insert->created_at = strtotime("now");
                  $custom_package_insert->interval = "month";
                  $custom_package_insert->user_id = $user_id;
                  $custom_package_insert->custom_status = 1;
            }else{
              $custom_package_insert->updated_at = strtotime("now");
            }
            $custom_package_insert->number_of_leads = $custom_lead;
            $custom_package_insert->amount = $custom_amount;
            $custom_package_insert->currency = $custom_currency;
            $custom_package_insert->save();
            $packages_select = $custom_package_insert->id;
          }
          $package = Packages::findOne($packages_select);
          //+++++++++++++++++CUSTOM PACKAGE+++++++++++++++++++++++++++//
          $subscription_pack = Subscription::find()->where(['user_id'=>$user_id,'is_cancelled'=>0])->exists();
          if(!$subscription_pack){
            //+++++Insert++++++++//
            $maxDays=date('t'); //Total Day of This Month;
            if($change_plan_activation == 1) {
                $activationDay = strtotime($plan_activation_date);
                $currentDayOfMonth = date('j', $activationDay);
            }else {
                $currentDayOfMonth=date('j'); //Current date of Month;
            }

            $totalDaysLeft = ($maxDays-$currentDayOfMonth)+1;
            $singleDayCharge = $package->amount/$maxDays;
            $currentChargeAmount = $singleDayCharge*$totalDaysLeft;
            //Smallest Unit Conversion//
            if($gst_status==1){
              //$gst_charge = round($currentChargeAmount)*($gst_percent/100);
              $gst_charge = ($currentChargeAmount)*($gst_percent/100);
              //$finalCharge = round($currentChargeAmount)+$gst_charge;
              //$finalChargeAmount = $finalCharge*100;
              $finalCharge = ($currentChargeAmount)+$gst_charge;
              $finalChargeAmount = round($finalCharge)*100;
            }else{
              $gst_charge = 0;
              $finalChargeAmount = round($currentChargeAmount)*100;
            }

            //check stripe minimum allowed charge
            //https://stripe.com/docs/currencies#minimum-and-maximum-charge-amounts
            if($automatic_charge == 1)
            {
                $allowed_minimum_charge = User::validMinimumStripeCharge(($finalChargeAmount/100), $package->currency);
                if(!$allowed_minimum_charge) {
                  return json_encode(['success'=>false,'data'=>'Stripe does not allow such a small transaction']);
                  exit;
                }
            }

           if($automatic_charge==1){
             if($advance_payment==1){
               ////////////////////////////
               $charge = $this->MakeAStripeCharge($package,$user,$finalChargeAmount,$totalDaysLeft,$singleDayCharge);
               if(isset($charge->code)){
                  //***********Save in invoice Table***********//
                  //Update A Subscription//
                  $invoice = new Invoice();
                  $invoice->user_id = $user->id;
                  $invoice->total_amount = $charge->json['amount']/100;
                  $invoice->gst_charge = $gst_charge;
                  $invoice->gst_percent = $gst_percent;
                  $invoice->advanced_payment = $package->amount;
                  $invoice->package_id = $package->id;
                  $invoice->stripe_payment_id = $charge->json['id'];
                  $invoice->allocated_lead = $package->number_of_leads;
                  $invoice->extra_per_lead_amount = Yii::$app->request->post('extra_per_lead_amount');
                  $invoice->extra_lead_consumed = 0;
                  $invoice->advance_payment_status = $advance_payment;
                  $invoice->transaction_currency = $package->currency;
                  $invoice->invoice_status = 1;
                  $invoice->automatic_charge_status = 1;
                  $invoice->created_at = strtotime("now");
                  $invoice->billing_date = strtotime("now");
                  $invoice->payment_date = $charge->json['created'];
                  $invoice->subscription_id = 0;
                  $invoice->cron_added = 0;
                  $invoice->cron_updated = 0;
                  $invoice->remaining_days = $totalDaysLeft;//total days remaining as per pro rata
                  $invoice->remaining_days_total_amount = $currentChargeAmount;//actual charge based on pro rata without gst
                  $invoice->single_day_charge = $singleDayCharge;//single day charege based on pro rata
                  $invoice->save();
                  $new_invoice_id = $invoice->id;
                 /////////////////////////////////////////////
                 try {
                    //$sendMail = Subscription::sendPackageAllocationSuccessMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency);
                    $sendMail = Subscription::sendPackageAllocationPlanASuccessMail($new_invoice_id,$invoice->stripe_payment_id,$invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency);
                 }
                 catch(\Exception $e) {
                   //do nothing
                 }
                 //Send Mail//
               }else{
                 return $charge;
               }
             }else if($advance_payment==0){
               //***********Save in invoice Table For Billing***********//
               $invoice = new Invoice();
               $invoice->user_id = $user->id;
               $invoice->total_amount = $finalChargeAmount/100;
               $invoice->advanced_payment = $package->amount;
               $invoice->gst_charge = $gst_charge;
               $invoice->gst_percent = $gst_percent;
               $invoice->package_id = $package->id;
               $invoice->stripe_payment_id = "Null";
               $invoice->allocated_lead = $package->number_of_leads;
               $invoice->extra_per_lead_amount = Yii::$app->request->post('extra_per_lead_amount');
               $invoice->extra_lead_consumed = 0;
               $invoice->advance_payment_status = $advance_payment;
               $invoice->transaction_currency = $package->currency;
               $invoice->invoice_status = 0;
               $invoice->automatic_charge_status = 1;
               $invoice->created_at = strtotime("now");
               $invoice->billing_date = strtotime("now");
               $invoice->payment_date = 0;
               $invoice->pending_invoice = 1;
               $invoice->subscription_id = 0;
               $invoice->cron_added = 0;
               $invoice->cron_updated = 0;
               $invoice->remaining_days = $totalDaysLeft;//total days remaining as per pro rata
               $invoice->remaining_days_total_amount = $currentChargeAmount;//actual charge based on pro rata
               $invoice->single_day_charge = $singleDayCharge;//single day charege based on pro rata
               $invoice->save();
               $new_invoice_id = $invoice->id;
              /////////////////////////////////////////////

              try {
                 $sendMail = Subscription::sendPackageAllocationSuccessMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency);
              }
              catch(\Exception $e) {
                //do nothing
              }
             }
           }else if($automatic_charge==0){
            if($advance_payment==1){
              //***********Save in invoice Table For Billing***********//
              $invoice = new Invoice();
              $invoice->user_id = $user->id;
              $invoice->total_amount = $finalChargeAmount/100;
              $invoice->advanced_payment = $package->amount;
              $invoice->gst_charge = $gst_charge;
              $invoice->gst_percent = $gst_percent;
              $invoice->package_id = $package->id;
              $invoice->stripe_payment_id = "Null";
              $invoice->allocated_lead = $package->number_of_leads;
              $invoice->extra_per_lead_amount = Yii::$app->request->post('extra_per_lead_amount');
              $invoice->extra_lead_consumed = 0;
              $invoice->advance_payment_status = $advance_payment;
              $invoice->transaction_currency = $package->currency;
              $invoice->invoice_status = 0;
              $invoice->automatic_charge_status = 0;
              $invoice->created_at = strtotime("now");
              $invoice->billing_date = strtotime("now");
              $invoice->payment_date = 0;
              $invoice->subscription_id = 0;
              $invoice->cron_added = 0;
              $invoice->cron_updated = 0;
              $invoice->remaining_days = $totalDaysLeft;//total days remaining as per pro rata
              $invoice->remaining_days_total_amount = $currentChargeAmount;//actual charge based on pro rata
              $invoice->single_day_charge = $singleDayCharge;//single day charege based on pro rata
              $invoice->save();
              $new_invoice_id = $invoice->id;
             /////////////////////////////////////////////

             try {
                $sendMail = Subscription::sendPackageAllocationSuccessMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency);
             }
             catch(\Exception $e) {
               //do nothing
             }
            }
            else if($advance_payment==0){
              //Save in invoice Table For Billing
              $invoice = new Invoice();
              $invoice->user_id = $user->id;
              $invoice->total_amount = $finalChargeAmount/100;
              $invoice->advanced_payment = $package->amount;
              $invoice->gst_charge = $gst_charge;
              $invoice->gst_percent = $gst_percent;
              $invoice->package_id = $package->id;
              $invoice->stripe_payment_id = "Null";
              $invoice->allocated_lead = $package->number_of_leads;
              $invoice->extra_per_lead_amount = Yii::$app->request->post('extra_per_lead_amount');
              $invoice->extra_lead_consumed = 0;
              $invoice->advance_payment_status = $advance_payment;
              $invoice->transaction_currency = $package->currency;
              $invoice->invoice_status = 0;
              $invoice->automatic_charge_status = 0;
              $invoice->created_at = strtotime("now");
              //$invoice->billing_date = strtotime("now");//CHECK THIS
              $invoice->payment_date = 0;
              $invoice->pending_invoice = 1;
              $invoice->subscription_id = 0;
              $invoice->cron_added = 0;
              $invoice->cron_updated = 0;
              $invoice->remaining_days = $totalDaysLeft;//total days remaining as per pro rata
              $invoice->remaining_days_total_amount = $currentChargeAmount;//actual charge based on pro rata
              $invoice->single_day_charge = $singleDayCharge;//single day charege based on pro rata
              $invoice->save();
              $new_invoice_id = $invoice->id;

              try {
                 $sendMail = Subscription::sendPackageAllocationSuccessMail('',$invoice->created_at,$user->email,$invoice->transaction_currency);
              }
              catch(Exception $e) {
                //do nothing
              }
           }
           }
           //+++++++INSERT++++++++//
           $subscription = new Subscription();
           if($change_plan_activation == 1) {
              $subscription->created_at = strtotime($plan_activation_date);//if custom package activation date is choosen then package will get activated on that day
           }else {
              $subscription->created_at = strtotime("now");//if custom package activation date is not choosen then package will get activated today itself
           }
           $subscription->user_id = $user_id;
           $subscription->package_name = $package->package_name;
           $subscription->package_id = $package->id;
           $subscription->number_of_leads = $package->number_of_leads;
           $subscription->amount = $package->amount;
           $subscription->currency = $package->currency;
           $subscription->extra_per_lead_amount = Yii::$app->request->post('extra_per_lead_amount');
           $subscription->advance_payment = Yii::$app->request->post('advance_payment');
           $subscription->automatic_charge = Yii::$app->request->post('automatic_charge');
           $subscription->gst_status = $gst_status;
           $subscription->gst_percent = $gst_percent;
           $subscription->show_previous_lead = $show_previous_lead;
           $subscription->custom_activation = $change_plan_activation;
           $subscription->package_creation_date = strtotime("now");//date on which package was allocated
           $subscription->save();

           //update subscription_id column in invoice table
           $newly_created_invoice = Invoice::find()->where(['id'=>$new_invoice_id])->one();
           if(!empty($newly_created_invoice)){
             $newly_created_invoice->subscription_id = $subscription->id;
             $newly_created_invoice->save();
           }

           return json_encode(['success'=>true,'data'=>'Package Allocated Successfully']);
         }else{
           //++++++++++++Update++++++++++++++++++//
           //$subscription = Subscription::findOne(['user_id'=>$user_id]);
           //$subscription->updated_at = strtotime("now");
           //++++++++++++Update++++++++++++++++++//
           return json_encode(['success'=>false,'data'=>'User Already Subscribed to this plan']);
         }
        //}
      }
    }

    protected function SaveStripeLog($user_id,$amount,$currency,$status,$stripe_token,$log)
    {
      try {
        $log = json_encode($log);

        $stripe_log = new StripeLog();
        $stripe_log->user_id = $user_id;
        $stripe_log->amount = $amount/100;//amount is getting passed in cents, paise etc
        $stripe_log->currency = $currency;
        $stripe_log->status = $status;
        $stripe_log->stripe_token = $stripe_token;
        $stripe_log->log = $log;
        $stripe_log->error_type = 'allocate_package';//error occured while allocating package
        $stripe_log->created_at = time();
        $stripe_log->save(false);
      }catch(\Exception $e) {

      }
    }

  protected function MakeAStripeCharge($package,$user,$finalChargeAmount,$totalDaysLeft,$singleDayCharge){
    //echo $finalChargeAmount.' == '.$totalDaysLeft.' == '.$singleDayCharge;
    //$finalChargeAmount = 100;
                 try{
                   \Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);
                   $charge = \Stripe\Charge::create(array(
                          "amount" => $finalChargeAmount,
                          "currency" => $package->currency,
                          "description" => "Charge for Subscription Advanced Payment ".$user->email,
                          "customer"    => $user->stripeToken,
                          "metadata" => [
                          "Advanced Payment" => $finalChargeAmount/100,
                          "Payment-type" => "Subscription",
                          "Total-day" => $totalDaysLeft,
                          "Per Day Charge" => $singleDayCharge,
                          "Extra Lead Charge" => "Null"
                       ]
                     ));
                 } catch(\Stripe\Error\Card $e) {
                 // Since it's a decline, \Stripe\Error\Card will be caught
                 $body = $e->getJsonBody();
                 $err  = $body['error'];

                 $this->SaveStripeLog($user->id,$finalChargeAmount,$package->currency,0,$user->stripeToken,$err);
                return json_encode(['success'=>false,'data'=>$err['message']]);
               } catch (\Stripe\Error\RateLimit $e) {
                 // Too many requests made to the API too quickly
                 $this->SaveStripeLog($user->id,$finalChargeAmount,$package->currency,0,$user->stripeToken,$e);
                return json_encode(['success'=>false,'data'=>'Headers are missing']);
               } catch (\Stripe\Error\InvalidRequest $e) {
                 //print_r($e);
                 // Invalid parameters were supplied to Stripe's API
                 $this->SaveStripeLog($user->id,$finalChargeAmount,$package->currency,0,$user->stripeToken,$e);
                  return json_encode(['success'=>false,'data'=>'Invalid parameters were supplied to Stripe\'s API']);
               } catch (\Stripe\Error\Authentication $e) {
                 // Authentication with Stripe's API failed
                 $this->SaveStripeLog($user->id,$finalChargeAmount,$package->currency,0,$user->stripeToken,$e);
                return json_encode(['success'=>false,'data'=>'maybe you changed API keys recently']);
                 // (maybe you changed API keys recently)
               } catch (\Stripe\Error\ApiConnection $e) {
                 $this->SaveStripeLog($user->id,$finalChargeAmount,$package->currency,0,$user->stripeToken,$e);
                return json_encode(['success'=>false,'data'=>'Network communication with Stripe faile']);
                 // Network communication with Stripe failed
               } catch (\Stripe\Error\Base $e) {
                 $this->SaveStripeLog($user->id,$finalChargeAmount,$package->currency,0,$user->stripeToken,$e);
                return json_encode(['success'=>false,'data'=>'Display a very generic error to the user, and maybe send']);
                 // Display a very generic error to the user, and maybe send
                 // yourself an email
               } catch (\Exception $e) {
                 $this->SaveStripeLog($user->id,$finalChargeAmount,$package->currency,0,$user->stripeToken,$e);
                 return json_encode(['success'=>false,'data'=>'Something else happened, completely unrelated to Stripe']);
                 // Something else happened, completely unrelated to Stripe
               }
               return $charge->getLastResponse();
      }

      public function actionAllocatePlanData(){
        $user_id = Yii::$app->request->get('user_id');
        $subscription = Subscription::findOne(['user_id'=>$user_id,'is_cancelled'=>0]);
        if(!empty($subscription)){
          return json_encode(['success'=>true,'data'=>[
            'package_id'=>$subscription->package_id,
            'extra_per_lead_amount'=>$subscription->extra_per_lead_amount,
            'advance_payment'=>$subscription->advance_payment,
            'automatic_charge'=>$subscription->automatic_charge,
            'gst_status'=>$subscription->gst_status,
            'gst_percent'=>$subscription->gst_percent,
            'custom_status'=>$subscription->packageModel->custom_status,
            'show_previous_lead'=>$subscription->show_previous_lead,
            'custom_activation'=>$subscription->custom_activation,
            'created_at'=>($subscription->custom_activation == 1)? date('Y-m-d',$subscription->created_at):''
            ]]);
        }else{
          return json_encode(['success'=>false]);
        }
      }
}
