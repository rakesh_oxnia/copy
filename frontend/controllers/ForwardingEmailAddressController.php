<?php

namespace frontend\controllers;

use Yii;
use common\models\ForwardingEmailAddress;
use common\models\ForwardingEmailTeam;
use frontend\models\ForwardingEmailAddress as ForwardingEmailAddressSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use common\models\User;
use yii\filters\AccessControl;

/**
 * ForwardingEmailAddressController implements the CRUD actions for ForwardingEmailAddress model.
 */
class ForwardingEmailAddressController extends Controller
{
    public function behaviors()
    {
        return [
            // 'access' => [
            //     'class' => \yii\filters\AccessControl::className(),
            //     'only' => ['create', 'update'],
            //     'rules' => [
            //         // allow authenticated users
            //         [
            //             'allow' => true,
            //             'roles' => ['@'],
            //         ],
            //         // everything else is denied
            //     ],
            // ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],                      
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('manageForwardingEmails');                     
                        }                     
                    ]                   
                ],
            ], 

        ];
    }

    public function beforeAction($action)
    { 
        if(!parent::beforeAction($action)) {
            return false;
        }

        if(Yii::$app->user->isGuest){
          return $this->redirect(['site/index']);
        }

        if(Yii::$app->user->identity->role == User::ROLE_ASSIST) {     
            $user_id = Yii::$app->user->identity->id;
            $parent = User::findOne(Yii::$app->user->identity->parent_id);   
            yii::$app->user->login($parent);
            Yii::$app->session->set('user_assist', $user_id);       
        }

        $this->layout = 'kosmoouter';
        return true;
    }

    public function render($view, $params)
    {
        if(Yii::$app->session->has('user_assist')) { 
          $user = User::findOne(Yii::$app->session->get('user_assist'));  
          Yii::$app->session->remove('user_assist');             
          yii::$app->user->login($user);             
        }

        // before render
        return parent::render($view, $params);
    }

    public function actionIndex()
    {
        if(Yii::$app->user->identity->role != User::ROLE_USER){
            throw new ForbiddenHttpException("This feature is only avaliable for clients");
        }

        $searchModel = new ForwardingEmailAddressSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());
        $forwardingEmails = ForwardingEmailAddress::findAll(['user_id' => Yii::$app->user->identity->id]);
        $forwardingTeams = ForwardingEmailTeam::findAll(['user_id' => Yii::$app->user->identity->id]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'forwardingEmails' => $forwardingEmails,
            'forwardingTeams' => $forwardingTeams,
        ]);
    }

    /**
     * Displays a single ForwardingEmailAddress model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(Yii::$app->user->identity->role != User::ROLE_USER){
            throw new ForbiddenHttpException("This feature is only avaliable for clients");
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ForwardingEmailAddress model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(Yii::$app->user->identity->role != User::ROLE_USER){
            throw new ForbiddenHttpException("This feature is only avaliable for clients");
        }

        $forwardingEmails = ForwardingEmailAddress::findAll(['user_id' => Yii::$app->user->identity->id]);
        if(count($forwardingEmails) > 4){
            throw new ForbiddenHttpException("You cannot add more than 5 forwarding email addresses");
        }

        $model = new ForwardingEmailAddress;
        $forwardingTeams = ForwardingEmailTeam::findAll(['user_id' => Yii::$app->user->identity->id]);

        $params = Yii::$app->request->post(); 
        $params['ForwardingEmailAddress']['user_id'] = Yii::$app->user->identity->id;

        if ($model->load($params) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'forwardingTeams' => $forwardingTeams,
            ]);
        }
    }

    /**
     * Updates an existing ForwardingEmailAddress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->identity->role != User::ROLE_USER){
            throw new ForbiddenHttpException("This feature is only avaliable for clients");
        }

        $model = $this->findModel($id);
        $forwardingTeams = ForwardingEmailTeam::findAll(['user_id' => Yii::$app->user->identity->id]);

        $params = Yii::$app->request->post(); 
        unset($params['ForwardingEmailAddress']['user_id']);

        if ($model->load($params) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'forwardingTeams' => $forwardingTeams,
            ]);
        }
    }

    /**
     * Deletes an existing ForwardingEmailAddress model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->identity->role != User::ROLE_USER){
            throw new ForbiddenHttpException("This feature is only avaliable for clients");
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ForwardingEmailAddress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ForwardingEmailAddress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ForwardingEmailAddress::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
