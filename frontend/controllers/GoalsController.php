<?php

namespace frontend\controllers;

use Yii;
use app\models\User2;
use common\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\PasswordResetRequestForm;
use common\models\LiveChat_API;
use yii\filters\AccessControl;
use common\models\LoginForm;

//require(\Yii::getAlias('@app') . '/../g_php/LiveChat_API.php');

/**
 * UserController implements the CRUD actions for User2 model.
 */
class GoalsController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
				'class'=> AccessControl::className(),
				'only' => ['index'],
				'rules'=>[
					[
						'actions' => ['index'],
						'allow' => true,
						'matchCallback' => function ($rule, $action) {
							return Yii::$app->user->can('viewReport');                     
						}
					]
				]
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

	public $layout = "dashboard";

    public function beforeAction($action)
    { 
        if(!parent::beforeAction($action)) {
            return false;
        }
        LoginForm::userAssitLogin();
        return true;
    }

    public function render($view, $params)
    {
      $params = LoginForm::userAssitLogout($params);
        
      if(Yii::$app->user->identity->role == User::ROLE_OUTLET) {  
        unset($params['website_users']);                        
      }
        // before render
        return parent::render($view, $params);
    }	

    /**
     * Lists all User2 models.
     * @return mixed
     */
    public function actionIndex()
    {
		$this->layout = "kosmoouter";

    	// set website filter
        if(!empty(Yii::$app->request->post('website_filter'))) {
          Yii::$app->session->set('website_filter', Yii::$app->request->post('website_filter')); 
          //Yii::$app->session->remove('outlet_filter');    
          $_GET["group"] = Yii::$app->session->get('website_filter');     
        } else {
        	$_GET["group"] = Yii::$app->user->identity->user_group;
        }

        try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;			

			if(!isset($_GET["date_from"])){
				$_GET['date_from'] = date('Y-m-d',strtotime("-7 days"));
				$_GET['date_to'] = date('Y-m-d');
				$_GET['group_by'] = 'day';
			}

			$API = new LiveChat_API();
			$goals = $API->reports->get("chats/goals",$_GET);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}

		/* $transcripts = $chats->chats;
		$total = $chats->total;
		$pages = $chats->pages */;

		//echo '<pre>';print_r($goals);echo '</pre>';die;
		$website_users = User::find()->where(['email' => Yii::$app->user->identity->email])->all();

		return $this->render('index', [
            'goals' => $goals,
            'website_users' => $website_users,
        ]);
    }


	public function actionTest(){

		$time    = microtime(true);
		$dFormat = "Y-m-d H:i:s";
		$mSecs   =  $time - floor($time);
		$mSecs   =  substr($mSecs,1);

		echo date($dFormat);

		/* echo '<br />$time ==' .$time;
		echo '<br />';
		echo '<br />' .sprintf('%s%s', date($dFormat), $mSecs );  */


		$dt = new \DateTime('2016-11-06 10:12:00');
		echo '<pre>';print_r($dt);echo '</pre>';
		echo strtotime($dt->getTimestamp());echo '<br>';
		echo microtime(true);
	}

	//1463070607435
	//1481019120


}
