<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\User;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Feedback;
use yii\data\ActiveDataProvider;

class FeedbackController extends Controller
{

	public function behaviors()
  {
      return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [User::ADMIN_ROLE, User::SUBADMIN_ROLE],
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('manageFeedbacks');                     
                        }                     
                    ],
                    [
                        'allow' => true,
                        'actions' => ['add'],
                        'roles' => ['@']                 
                    ]                    
                ],
            ], 
      ];
  }

	public function beforeAction($action)
	{
			if(Yii::$app->user->isGuest){
				return $this->redirect(['site/login']);
			}

			return parent::beforeAction($action);
	}

	public function actionAdd(){
		$this->layout = 'kosmoouter';
		$success = "";
		$model	 = new Feedback;
		if ($model->load(Yii::$app->request->post())) {
			$model->user_id  = Yii::$app->user->id;
			$model->given_by = Yii::$app->user->id;
			if($model->save()){
				$success = "Feedback has been added successfully.";
				 Yii::$app->mailer->compose()
					->setTo(Yii::$app->params['supportEmail'])
					->setFrom(Yii::$app->params['supportEmail'])
					->setSubject("New Feedback")
					->setTextBody($model->feedback)
					->send();

					 Yii::$app->mailer->compose()
					->setTo(Yii::$app->params['adminEmail'])
					->setFrom(Yii::$app->params['supportEmail'])
					->setSubject("New Feedback")
					->setTextBody($model->feedback)
					->send();

					 Yii::$app->mailer->compose()
					->setTo("param@team.oxnia.com")
					->setFrom(Yii::$app->params['supportEmail'])
					->setSubject("New Feedback")
					->setTextBody($model->feedback)
					->send();
			}
		}
		return $this->render('add', ['success' => $success, 'model' => $model]);
	}


	public function actionIndex(){
		$this->layout = 'kosmoouter';
		$users  = User::getAllUsers();
		$feedbacks	= Feedback::find()->orderBy(['id' => SORT_DESC])->all();


		return $this->render('index', ['users' => $users, 'feedbacks' => $feedbacks]);
	}



}
