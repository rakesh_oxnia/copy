<?php

namespace frontend\controllers;

use Yii;
use app\models\User2;
use common\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\PasswordResetRequestForm;

/**
 * UserController implements the CRUD actions for User2 model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
	
	public $layout = "dashboard";

    /**
     * Lists all User2 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch(['role'=>'10']);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User2 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User2();
		
		/* Yii::$app->mailer->compose()
		->setFrom('gagan.saini@triusmail.com')
		->setTo('gagan.saini@triusmail.com')
		->setSubject('Message subject')
		->setTextBody('Plain text content')
		->setHtmlBody('<b>HTML content</b>')
		->send(); */

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			
			//echo '<pre>';print_r($model->username);die;
			
			$user = new User();
            $user->username = $model->username;
            $user->user_group = $model->user_group;
            $user->email = $model->email;
            $user->website_url = $model->website_url;
            $user->setPassword(time().rand());
            $user->generateAuthKey();
			if ($user->save()) {
                //return $user;
				$model_reset = new PasswordResetRequestForm();
				$model_reset->email = $model->email;
				if($model_reset->validate()){
					if ($model_reset->sendEmail("test")) {
						Yii::$app->getSession()->setFlash('success', 'User Created Mail Send To User\'s Email.');
						//return $this->goHome();
						return $this->redirect(['index']);
					} else {
						Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to Send Email.');
					}
				}
				return $this->redirect(['index']);
            }
			else{
				return $this->render('create', [
					'model' => $model,
				]);
			}
			
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
	public function actionChangepassword($id = false){
        // $layout_ = $this->actionGetlayout();
        // $this->layout = $layout_;
		$id = yii::$app->user->identity->id;
        $model = $this->findModel($id);
		
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()){
			die("sasa");
            $model->password_hash = Yii::$app->security->generatePasswordHash(Yii::$app->request->post()['new_password']);
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
				
				echo '<pre>';print_r($model);die;
				 return $this->render('changepassword', [
					'model' => $model,
				]);
			}
        } else {
            return $this->render('changepassword', [
                'model' => $model,
            ]);
        }        
    }
    /**
     * Deletes an existing User2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User2::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	
	
}
