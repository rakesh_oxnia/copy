<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\User;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LiveChat_API;
use app\models\Leads;
use frontend\models\Invoices;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','logout', 'newuser'],
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['newuser'],
                        'allow' => true,
                        'roles' => ['@'],
            						'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->username);
                        }
                    ],
                    [
                        'actions' => ['logout','index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {

        if ($action->id == 'render-agent-app') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


   /* @y2j public function beforeAction ($action)
    {
        $host = explode('.', $_SERVER['HTTP_HOST']);
        $username = $host[0];
        if($username != '' && $username != 'chat-application')
        {
            if(Yii::$app->user->identity->username != '' && $username  != Yii::$app->user->identity->username)
            {
                //echo Yii::$app->homeUrl; exit;
               //  return $this->redirect('http://chat-application.com/site/login');
            }
        }
        return 1;

    }*/
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
      if(Yii::$app->user->identity->role == USER::ROLE_SERVICE_ACCOUNT){
        return $this->redirect(['leads/service-chat']);
      }

      LoginForm::userAssitLogin();

        //echo "<pre>"; print_r($_SESSION); exit;
        $host = explode('.', $_SERVER['HTTP_HOST']);
        $username = $host[0];
        $flag = 0;
        if($username != '')
        {
            $role = Yii::$app->user->identity->role;

            if(User::ROLE_AGENCY == $role)
            {
                if($username == 'chat-application')
                {
                    $flag = 1;

                }
                else if ($username != Yii::$app->user->identity->username)
                {
                    $flag = 1;
                }
            }
            else if(User::ROLE_USER == $role )
            {
                $parent_id = Yii::$app->user->identity->parent_id;
                if($parent_id != '')
                {
                    $row = (new \yii\db\Query())
                    ->from('user')
                    ->where(['id' => $parent_id])
                    ->one();
                   if($username == 'chat-application')
                    {
                        $flag = 1;

                    }
                    else if ($username != $row['username'])
                    {
                        $flag = 1;
                    }
                }
            }
            if($flag == 1)
            {
                //Yii::$app->user->logout();
                //return $this->redirect('http://chat-application.com/site/login');

            }
        }

    		try {

      			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
      			if(!User::isUserAdmin(Yii::$app->user->identity->username))
      			{
      		    $_GET["group"] = Yii::$app->user->identity->user_group;
      			}
            if(!isset($_GET["date_from"])){

                //$_GET['date_from'] = date('Y-m-d');
                $_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
                $_GET['date_to'] = date('Y-m-d');
                $_GET['group_by'] = 'day';
            }

            $API = new LiveChat_API();
            $flag = 0;
            //return Yii::$app->user->identity->username;
            if(User::isUserAgency(Yii::$app->user->identity->username))
            {
                $connection = Yii::$app->getDb();
                $command = $connection->createCommand('SELECT user_group
                FROM  `user`
                WHERE  `parent_id` ='.Yii::$app->user->identity->id);
                $bl = $command->queryAll();
                $usercnt = count($bl);
                if($usercnt > 0)
                {
                    $flag = 1;
                    $_GET["group"] = Yii::$app->user->identity->user_group;
                    $dataArr[] = $API->reports->get("chats/ratings",$_GET);
                    $data2Arr[] = $API->reports->get("chats/total_chats",$_GET);
                    //echo "<pre>"; print_r($dataArr); exit;
                    $_GET['tag[]'] = 'lead';
                    //$leads = $API->chats->get($page,$_GET);
                    $leadsArr[] = $API->reports->get("chats/total_chats",$_GET);

                    $i = 0;
                    foreach ($bl as $value) {
                        if($value['user_group'] != '')
                        {
                            $_GET['tag[]'] = '';

                            $_GET["group"] = $value['user_group'];
                            $dataArr[] = $API->reports->get("chats/ratings",$_GET);
                            $data2Arr[] = $API->reports->get("chats/total_chats",$_GET);

                            $_GET['tag[]'] = 'lead';
                            //$leads = $API->chats->get($page,$_GET);
                            $leadsArr[] = $API->reports->get("chats/total_chats",$_GET);

                        }
                    }
					/*
                   echo "<pre>";
                    echo "Data ";
                    print_r($dataArr);
                    echo "Data2 ";
                    print_r($data2Arr);
                    echo "Data 3 ";
                    print_r($leadsArr);
					exit;
                    */

                    $data = $dataArr;
                    $data2 = $data2Arr;
                    $leads = $leadsArr;
                }
                else
                {
                    $_GET["group"] = Yii::$app->user->identity->user_group;
                    $data = $API->reports->get("chats/ratings",$_GET);
                    $data2 = $API->reports->get("chats/total_chats",$_GET);

                    $_GET['tag[]'] = 'lead';
                    //$leads = $API->chats->get($page,$_GET);
                    $leads = $API->reports->get("chats/total_chats",$_GET);

                }

                $users = $usercnt+1;;

            }
            else
            {
                $data = $API->reports->get("chats/ratings",$_GET);
                $data2 = $API->reports->get("chats/total_chats",$_GET);

                //$_GET['tag%5B%5D'] = 'positive%20feedback';
                //$_GET['tag[]'] = 'positive feedback';
                $_GET['tag[]'] = 'lead';
                //$leads = $API->chats->get($page,$_GET);
                $leads = $API->reports->get("chats/total_chats",$_GET);

                $users = User::getCount();
            }

			/*echo "<pre>";
                    echo "Data ";
                    print_r($data);
                    echo "Data2 ";
                    print_r($data2);
                    echo "Data 3 ";
                    print_r($leads);*/
                    //exit;
			//$_GET['tag%5B%5D'] = 'positive%20feedback';
			//$_GET['tag[]'] = 'positive feedback';


    		}
    		catch (Exception $e) {
    			throw new NotFoundHttpException($e->getMessage());
    		}

		    if($flag == 1)
        {
            if(count($dataArr) > 0)
            {
              //  $data = new obj;
                $tempArr= array();
                $i = 0;
                foreach($dataArr as $key => $ds)
                {

                    foreach ($ds as $key1 => $value1) {
                        # code...
                        if($i == 0)
                        {
                            $tempArr[$key1]['bad'] = $value1->bad;
                            $tempArr[$key1]['good'] = $value1->good;
                            $tempArr[$key1]['chats'] = $value1->chats;
                        }
                        else
                        {
                            $tempArr[$key1]['bad'] = $tempArr[$key1]['bad'] + $value1->bad;
                            $tempArr[$key1]['good'] = $tempArr[$key1]['good'] + $value1->good;
                            $tempArr[$key1]['chats'] = $tempArr[$key1]['chats'] + $value1->chats;
                        }

                    }
                    $i++;
                }
                $data = json_decode(json_encode($tempArr), FALSE);
            }


            if(count($data2Arr) > 0)
            {
              //  $data = new obj;
                $tempArr2= array();
                $i2 = 0;
                foreach($data2Arr as $key => $ds)
                {

                    foreach ($ds as $key1 => $value1) {
                        # code...
                        if($i == 0)
                        {
                            $tempArr2[$key1]['chats'] = $value1->chats;
                            $tempArr2[$key1]['missed_chats'] = $value1->missed_chats;

                        }
                        else
                        {
                            $tempArr2[$key1]['chats'] = $tempArr2[$key1]['chats'] + $value1->chats;
                            $tempArr2[$key1]['missed_chats'] = $tempArr2[$key1]['missed_chats'] + $value1->missed_chats;
                        }

                    }
                    $i++;
                }
                $data2 = json_decode(json_encode($tempArr2), FALSE);
            }


            if(count($leadsArr) > 0)
            {
              //  $data = new obj;
                $tempArr3= array();
                $i3 = 0;
                foreach($leadsArr as $key => $ds)
                {

                    foreach ($ds as $key1 => $value1) {
                        # code...
                        if($i == 0)
                        {
                            $tempArr3[$key1]['chats'] = $value1->chats;
                            $tempArr3[$key1]['missed_chats'] = $value1->missed_chats;

                        }
                        else
                        {
                            $tempArr3[$key1]['chats'] = $tempArr3[$key1]['chats'] + $value1->chats;
                            $tempArr3[$key1]['missed_chats'] = $tempArr3[$key1]['missed_chats'] + $value1->missed_chats;
                        }

                    }
                    $i++;
                }
                $leads = json_decode(json_encode($tempArr3), FALSE);

            }
        }
        //@y2j
    		//$filter = "";
    		$date_from = "";
    		$date_to = "";
    		$group_by = "";
    		$page = 1;
            try {
    			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    			$API = new LiveChat_API();

    			if(!isset($_GET["date_from"])){
    				$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
    				$_GET['date_to'] = date('Y-m-d');
    			}else{
    				$filter = 'crange';
    				$date_from = $_GET['date_from'];
    				$date_to = $_GET['date_to'];
    				$group_by = $_GET['group_by'];
    				$page = 1;
    			}
    			//$_GET['tag%5B%5D'] = 'lead';

          // map bug //
          // if(Yii::$app->user->identity->role == User::ROLE_ADMIN){
          //   unset($_GET["group"]);
          // }else{
          //   $_GET["group"] = Yii::$app->user->identity->user_group;
          // }
          //map bug //

    			$_GET["group"] = Yii::$app->user->identity->user_group;
          if(Yii::$app->user->identity->role == User::ROLE_ADMIN){
            unset($_GET["group"]);
          }
    			$chats = $API->chats->get($page,$_GET);
          //echo '<pre>'; print_r($chats); exit;
    		}
    		catch (Exception $e) {
    			throw new NotFoundHttpException($e->getMessage());
    		}

    		if(!$chats){
    			throw new NotFoundHttpException("Server maintenance");
    		}


    		$survayData 	= [];
    		$transcripts 	= $chats->chats;
    		$total 			= $chats->total;
    		$pages 			= $chats->pages;
    		$row			= 0;

        //echo "<pre>"; print_r($chats); exit;
    		if( $pages > 1){
    			/*for($i = 1; $i <= $pages; $i++){
    				try {
    					$page = $i;
    					$API = new LiveChat_API();
    					if(!isset($_GET["date_from"])){
    						$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
    						$_GET['date_to'] = date('Y-m-d');

    					}else{
    						$filter = 'crange';
    						$date_from = $_GET['date_from'];
    						$date_to = $_GET['date_to'];
    						$group_by = $_GET['group_by'];
    						$page = $i;
    					}

    					$_GET["group"] = Yii::$app->user->identity->user_group;
              if(Yii::$app->user->identity->role == User::ROLE_ADMIN){
                unset($_GET["group"]);
              }
    					$chats = $API->chats->get($page,$_GET);

    				}
    				catch (Exception $e) {
    					throw new NotFoundHttpException($e->getMessage());
    				}

    				if(!$chats){
    					throw new NotFoundHttpException("Server maintenance");
    				}

    				$transcripts = $chats->chats;
    				foreach($transcripts as $key => $transcript){
    					$survayData[$row] 		=  $transcript->visitor->city . ", " . $transcript->visitor->region . ", " . $transcript->visitor->country;
    					$row++;
    				}

    			}*/
    		}else{
    			foreach($transcripts as $key => $transcript){

    				$survayData[$key] 		=  $transcript->visitor->city . ", " . $transcript->visitor->region . ", " . $transcript->visitor->country;

    			}
    		}


  		$finalSurveyData = array_count_values($survayData);
  		arsort( $finalSurveyData );
  		$srvay = array_slice($finalSurveyData, 0, 10);
      //echo "<pre>"; print_r($finalSurveyData); exit;

      // filter params //
      $filter = false;
      if($filter = Yii::$app->request->get('filter')){
        $from_date = Yii::$app->request->get('date_from');
        $to_date = Yii::$app->request->get('date_to');
      }
      // filter params //

      // client lead value //
      $clientList = User::findAll(['role' => User::ROLE_USER]);
      $lead_value = 0;
      $count = 0;
      foreach ($clientList as $client) {
        if($client->average_lead_value != NULL){
          $count++;
          $lead_value = $lead_value + $client->average_lead_value;
        }
      }
      $average_lead_value = round($lead_value/$count);
      // client lead value //

      // Chatmetrics revenue //
      if(!isset($_GET["date_from"])){
        $params['date_from'] = date('Y-m-d',strtotime("-6 days"));
        $params['date_to'] = date('Y-m-d');
      }else{
        $params['date_from'] = $_GET['date_from'];
        $params['date_to'] = $_GET['date_to'];
      }
      $from_date = strtotime($params['date_from']);
      $to_date = strtotime($params['date_to']);
      $params['tag[]'] = 'lead';
      $API = new LiveChat_API();
      //$allLeads = $API->chats->get($page,$params);
      $allLeads = $API->reports->get("chats/total_chats",$params);
      $leadCount = 0;
      //echo "<pre>"; print_r($params); exit;
      foreach ($allLeads as $key => $value) {
        $leadCount = $leadCount + $value->chats;
      }

      // if($filter){
      //   $allLeads = Leads::find()->andFilterWhere(['>=', 'c_time', strtotime($from_date)])->andFilterWhere(['<=', 'c_time', strtotime($to_date)])->all();
      // }else{
      //   $allLeads = Leads::find()->all();
      // }
      //$leadCount = count($allLeads);
      $outletLeadCount = 0;
      if(Yii::$app->user->identity->role == User::ROLE_OUTLET){
        if(isset($_GET["filter"]) && $_GET["filter"] == 'yesterday'){
          $from_date = $_GET['date_from'];
          $to_date = date('Y-m-d');

          //echo 'From date : ' . $from_date . "(".strtotime($from_date).")";
          //echo '<br>To date : ' . $to_date . "(".strtotime($to_date).")"; exit;
          $outletLeads = Leads::find()->where(['outlet_id' => Yii::$app->user->identity->id])->andFilterWhere(['>=', 'c_time', $from_date])->andFilterWhere(['<=', 'c_time', strtotime($to_date)])->andFilterWhere(['!=', 'is_service_chat', 1])->all();
          //print_r($to_date); exit;
        }else{
          $outletLeads = Leads::find()->where(['outlet_id' => Yii::$app->user->identity->id])->andFilterWhere(['>=', 'c_time', $from_date])->andFilterWhere(['!=', 'is_service_chat', 1])->all();
        }

        $outletLeadCount = count($outletLeads);

        // $allOutletArchives = $API->chats->get($page,$_GET);
        // $outletLeadCount = 0;
        // $leadFound = [];
        // $notFound = [];
        // foreach ($allOutletArchives->chats as $key => $chat) {
        //   $outletLead = Leads::findOne(['chat_id' => $chat->id]);
        //   if($outletLead){
        //     $leadFound[] = $outletLead->outlet_id;
        //     if($outletLead->outlet_id == Yii::$app->user->identity->id){
        //       $outletLeadCount++;
        //     }
        //   }else{
        //     $notFound[] = $chat->id;
        //   }
        // }

      }

      $chatmetricsRevenue = 10 * $leadCount;
      // Chatmetrics revenue //

      // last month billing and Revenue
      $month_start = strtotime('first day of last month');
      $month_end = strtotime('last day of last month');
      //if(Yii::$app->user->identity->role == User::ROLE_USER){
        //$leads = Leads::find()->where(['user_id' => Yii::$app->user->identity->id])->andFilterWhere(['>=', 'c_time', strtotime($month_start)])->andFilterWhere(['<=', 'c_time', strtotime($month_end)])->all();
      //}

      $clientRevenue = 0;
      $clientLeadValue = 200;
      if(Yii::$app->user->identity->role == User::ROLE_ADMIN){
        $clientRevenue = $leadCount * 200;
      }else{
        $userModel = User::findOne(Yii::$app->user->identity->id);
        if($userModel->average_lead_value != NULL){
          $clientLeadValue = $userModel->average_lead_value;
        }
      }

      $lastMonthBill = 0;

      //$invoices = Invoices::find()->where(['user_id' => Yii::$app->user->identity->id])->andFilterWhere(['between', 'payment_date', strtotime($sd), strtotime($ed)])->all();
      $invoices = Invoices::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
      foreach ($invoices as $invoice) {
        if( strtotime($invoice->payment_date) >=  $month_start && strtotime($invoice->payment_date) <= $month_end){
          $lastMonthBill = $lastMonthBill + $invoice->amount;
        }
      }
      $lastMonthBill = round($lastMonthBill);

      // last month billing and Revenue


      LoginForm::userAssitLogout();

  		if (!\Yii::$app->user->isGuest) {
  			$this->layout = 'kosmoouter';
  			return $this->render('nddashboard',[
  				"data" => $data,
  				"data2" => $data2,
  				"users" => $users,
  				"leads" => $leads,
          'finalSurveyData' => $srvay,
  				'average_lead_value' => $average_lead_value,
          'chatmetricsRevenue' => $chatmetricsRevenue,
          'leadCount' => $leadCount,
          'clientRevenue' => $clientRevenue,
          'lastMonthBill' => $lastMonthBill,
          'clientLeadValue' => $clientLeadValue,
          'outletLeadCount' => $outletLeadCount,
  			]);
  		}

  		return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    { 
      //echo "<pre>";  print_r(Yii::$app->user); exit;
       $this->layout = 'kosmologin';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if($model->login()){
                if(Yii::$app->user->identity->role == User::ROLE_AGENCY){
                  return $this->redirect(['chat/index']);
                }
                return $this->goBack();
            }else{
                //print_r($model->getErrors()); exit;
                return $this->render('login', [
                    'model' => $model,
                ]);
            }

            //print_r('Redirecting to go back since user in not agency');
            //exit;

        } else {

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionAjaxLogin()
    {
        $email = Yii::$app->request->post('email');
        $password = Yii::$app->request->post('password');
        $rememberMe = Yii::$app->request->post('remember_me');

        $userModel = User::findOne(['email' => $email]);


        if($userModel){
          if($userModel->status != User::STATUS_ACTIVE) {
                return json_encode(['error' => true, 'code' => '501', 'message' => 'This account is either inactive or deleted']);
          }

          if($userModel->free_access!=1){
            if($userModel->disable_payment_status==1 && $userModel->payment_status==1){
              return json_encode(['error' => true, 'code' => '501', 'message' => 'Please Make Payment for Uninterrupted Service']);
            }
          }
            //return json_encode(['error' => true, 'code' => '200', 'message' => 'test']);
            if($userModel->validatePassword($password)){
                if(Yii::$app->user->login($userModel, $rememberMe ? 3600 * 24 * 30 : 0)){
                    return $this->redirect(['site/login']);
                }else{
                    return json_encode(['error' => true, 'code' => '503', 'message' => 'Incorrect login']);
                }
            }else{
                return json_encode(['error' => true, 'code' => '502', 'message' => 'Incorrect password']);
            }
        }else{
            return json_encode(['error' => true, 'code' => '501', 'message' => 'Incorrect email']);
        }
    }

	public function actionSwitch( $id ) {  
        Yii::$app->session->remove('website_filter');
        Yii::$app->session->remove('outlet_filter');
		$model = new LoginForm();
        if ($model->switchuser( $id )) {             
            return $this->redirect(['/site/index']);
        }
	}

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        //print_r('You cannot logout becoz of developer\'s restriction, check site/logout'); exit;
		    unset(Yii::$app->session['discount']);
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionNewuser()
    {

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {

			$path = Yii::getAlias('@webroot');

            if ($user = $model->signup()) {
				return $this->goHome();
            }

        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
		$this->layout = 'kosmologin';
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

	/**
     * Creating a diectory
     *
	*/
	/* public static function createDirectory($path, $mode = 0775, $recursive = true)
    {
        if (is_dir($path)) {
            return true;
        }

        $parentDir = dirname($path);

        if ($recursive && !is_dir($parentDir)) {
            static::createDirectory($parentDir, $mode, true);
        }

        try {
            $result = mkdir($path, $mode);
            chmod($path, $mode);
        } catch (\Exception $e) {
            throw new \yii\base\Exception("Failed to create directory '$path': " . $e->getMessage(), $e->getCode(), $e);
        }

        return $result;
    } */

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
		$this->layout = 'kosmologin';

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $id = $model->resetPassword();

            $user = User::findOne($id);
            $username = $user->username;
            $role = $user->role;
            $parent_id = $user->parent_id;
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand('SELECT *
            FROM  `user_associate`
            WHERE  `user_id` ='.$user->id);
            $bl = $command->queryAll();

            if(count($bl) > 0)
            {
                $i = 0;
                foreach ($bl as $value) {
                    \Yii::$app->mailer->compose(['html' => 'usernamePasswordsend-html', 'text' => 'usernamePasswordsend-text'], ['user' => $user, "password" => $_POST['ResetPasswordForm']['password']])
                                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' '])
                                ->setTo($value['email'])
                                ->setSubject('Welcome to our Dashboard ') // user create mail subject
                                ->send();
                }
            }

            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            $flag = 0;

            if(User::ROLE_AGENCY == $role)
            {
                    $flag = 1;
            }
            else if(User::ROLE_USER == $role )
            {
                if($parent_id != '')
                {
                     $row = (new \yii\db\Query())
                    ->from('user')
                    ->where(['id' => $parent_id])
                    ->one();
                    $username = $row['username'];
                    $flag = 1;
                }
            }
            if($flag == 1)
            {

                Yii::$app->user->logout();
                //echo Yii::$app->homeUrl; exit;
                $url = $this->goHome();
                //return $this->redirect('http://'.$username.'.chatapptest.oxnia.com/site/login');
            }
            else
            {
                return $this->goHome();
            }

        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
     public function actionTestmailaaa()
    {
    // 	 \Yii::$app->mailer->compose(['html' => 'leadnew-html', 'text' => 'leadnew-text'], ['user' => $user,'lead'=>$lead])
    //                 ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
    //                 ->setTo('jatin.sehgal@triusmail.com')
    //                 ->setSubject('You have a new lead ') // mail subject
    //                 ->send();
    }


	public function actionLaunch(){
    if(Yii::$app->user->isGuest){
      return $this->redirect(['site/login']);
    }
		$this->layout = 'kosmoouter';
		return $this->render('launch');
	}

  public function actionRenderAgentApp()
  {
    $this->layout = 'agentapp';
    return $this->render('agent-app');
  }
}
