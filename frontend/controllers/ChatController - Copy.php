<?php

namespace frontend\controllers;

use Yii;
use app\models\User2;
use common\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\PasswordResetRequestForm;
use common\models\LiveChat_API;
use yii\filters\AccessControl;
use yii2tech\csvgrid\CsvGrid;
use yii\data\ArrayDataProvider;


//require(\Yii::getAlias('@app') . '/../g_php/LiveChat_API.php');

/**
 * ChatController implements the CRUD actions for User2 model.
 */
class ChatController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
				'class'=> AccessControl::className(),
				'only' => ['index','view','survey'],
				'rules'=>[
					[
						'actions' => ['index','view','survey'],
						'allow' => true,
						'roles'=>['@'],
					]
				]
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
	
	public $layout = "dashboard";

    /**
     * Lists all .
     * @return mixed
     */
    public function actionIndex()
    {
		$filter = "";
		$date_from = "";
		$date_to = "";
		$group_by = "";
		$page = 1;
        try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			$API = new LiveChat_API();
			if(!isset($_GET["date_from"])){
				$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
				$_GET['date_to'] = date('Y-m-d');
				
			}else{
				$filter = $_GET['filter'];
				$date_from = $_GET['date_from'];
				$date_to = $_GET['date_to'];
				$group_by = $_GET['group_by'];
				$page = $_GET['page'];
			}
			$_GET["group"] = Yii::$app->user->identity->user_group;
			$chats = $API->chats->get($page,$_GET);
			
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}
		
		if(!$chats){
			throw new NotFoundHttpException("Server maintenance");
		}
		
//echo '<pre>';print_r($chats);echo '</pre>';die('ghj');
		$transcripts = $chats->chats;
		$total = $chats->total;
		$pages = $chats->pages;
		
		return $this->render('index', [
            'chats' => $chats,
            'pages' => $pages,
            'total' => $total,
			'filter' => $filter,
			'date_from' => $date_from,
			'date_to' => $date_to,
			'group_by' => $group_by,
			'page' => $page
        ]);
    }
	
	public function actionExport(){
		try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			$API = new LiveChat_API();
			if(!isset($_GET["date_from"])){
				$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
				$_GET['date_to'] = date('Y-m-d');
			}
			$_GET["group"] = Yii::$app->user->identity->user_group;
			$chats = $API->chats->get($page,$_GET);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}
		
		if(!$chats){
			throw new NotFoundHttpException("Server maintenance");
		}
		$transcripts = $chats->chats;
		 
		//echo '<pre>';print_r($chats);echo '</pre>';die('ghj');
		$total = $chats->total;
		$pages = $chats->pages;
		$allModels  = [];
		foreach($chats->chats as $key => $chat){
			$allModels[$key]['Name'] = $chat->visitor->name;			
			$allModels[$key]['Email'] = $chat->visitor->email;			
			foreach($chat->prechat_survey as $pkey => $pvalue){
				foreach($pvalue as $ppkey => $ppvalue){
					if($ppvalue == 'Phone:'){
						$allModels[$key]['Phone'] = $pvalue->value;
					}
				}				
			}
			
			$allModels[$key]['City'] = $chat->visitor->city;
			$allModels[$key]['State'] = $chat->visitor->region;
			$allModels[$key]['Country'] = $chat->visitor->country;
			
			if($chat->rate == 'rated_bad'){
				$allModels[$key]['Rate'] = 'Bad';
			}else if($chat->rate == 'rated_good'){
				$allModels[$key]['Rate'] = 'Good';
			}else{
				$allModels[$key]['Rate'] = '--';
			}
			 
		}	
		
		//echo "<pre>";
		//print_r($allModels);exit;
		 
		$exporter = new CsvGrid([
			'dataProvider' => new ArrayDataProvider([
				'allModels' => $allModels
			]),
			'columns' => [
				[
					'attribute' => 'Name',
				],
				[
					'attribute' => 'Email',
				],
				 [
					'attribute' => 'Phone',
				],
				
				[
					'attribute' => 'City',
				],
				[
					'attribute' => 'State',
				],
				[
					'attribute' => 'Country',
				],
				[
					'attribute' => 'Rate',
				],
				 
				
			],
		]);
		$exporter->export()->saveAs('chats1.csv');		
		$file = "chats1.csv";
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=$file");
		header("Content-Type: application/csv");
		header("Content-Transfer-Encoding: binary");

		// read the file from disk
		readfile($file);
		 //echo "ssdf";exit;
	}

    /**
     * Displays a single User2 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		
		try {
			$API = new LiveChat_API();
			$chat = $API->chats->getSingleChat($id);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}
		
        return $this->render('view', [
            'chat' => $chat,
        ]);
    }

    /**
     * Creates a new User2 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSurvey($id)
    {
        try {
			$API = new LiveChat_API();
			$chat = $API->chats->getSingleChat($id);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}
		//echo '<pre>';print_r($chat);echo '</pre>';
        return $this->render('survey', [
            'chat' => $chat,
        ]);
    }

    /**
     * Updates an existing User2 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionAvailability()
    {
        try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			$_GET["group"] = Yii::$app->user->identity->user_group;
			if(!isset($_GET["date_from"])){
				$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
				$_GET['date_to'] = date('Y-m-d');
				$_GET['group_by'] = 'day';
			}
			
			$_GET['group'] = Yii::$app->user->identity->user_group;
			$API = new LiveChat_API();
			$availability = $API->reports->get("availability",$_GET);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}
		
		/* echo '<pre>';
print_r($availability);
echo '</pre>';die; */
		
		return $this->render('availability', [
			'availability' => $availability,
		]);
    }
	
	public function actionAvailability22()
    {
        try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			$_GET["group"] = Yii::$app->user->identity->user_group;
			if(!isset($_GET["date_from"])){
				$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
				$_GET['date_to'] = date('Y-m-d');
				$_GET['group_by'] = 'day';
			}
			
			$_GET['group'] = Yii::$app->user->identity->user_group;
			$API = new LiveChat_API();
			$availability = $API->reports->get("availability",$_GET);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}
		
		/* echo '<pre>';
print_r($availability);
echo '</pre>';die; */
		
		return $this->render('availability22', [
			'availability' => $availability,
		]);
    }

    /**
     * Deletes an existing User2 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionTotal()
    {
        try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			$_GET["group"] = Yii::$app->user->identity->user_group;
			if(!isset($_GET['date_from'])){
				$_GET['date_from'] = '2016-04-01';
			}
			$_GET['group_by'] = 'month';
			$API = new LiveChat_API();
			$reports = $API->reports->get("chats",$_GET);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
			//die($e->getCode().' '.$e->getMessage());
		}

		/* $transcripts = $chats->chats;
		$total = $chats->total;
		$pages = $chats->pages */;
		
		return $this->render('total', [
            'reports' => $reports,
        ]);
    }

    /**
     * Finds the User2 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User2 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    /* protected function findModel($id)
    {
        if (($model = User2::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    } */
	
	/* public function actionChangepassword(){
        $layout_ = $this->actionGetlayout();
        $this->layout = $layout_;
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()){
            $model->password_hash = Yii::$app->security->generatePasswordHash(Yii::$app->request->post()['new_password']);
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('changepassword', [
                'model' => $model,
            ]);
        }        
    } */
	
}
