<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\models\User;
use app\models\UserSearch;


class RoleController extends \yii\web\Controller
{
	public function behaviors()
	{
	    return [
	        'access' => [
	            'class' => AccessControl::className(),
	            'rules' => [ 
	               	[
	                    'allow' => true,
	                    'actions' => ['index', 'create'],
	                    'roles' => [User::ADMIN_ROLE, User::CLIENT_ROLE, User::FRANCHISE_ROLE]
	                ],
	               	[
	                    'allow' => true,
	                    'actions' => ['view', 'update', 'delete'],
	                    'roles' => [User::ADMIN_ROLE, User::CLIENT_ROLE, User::FRANCHISE_ROLE],
	                    'matchCallback' => function ($rule, $action) {
                            $name = explode('_', Yii::$app->request->get('name'));
							return Yii::$app->user->identity->id == $name[0];                       
                        }
	                ]	                	                               
	            ],
	        ],
	    ];
	}

	public function beforeAction($action)
    {    	

        if(in_array(Yii::$app->user->identity->role, [User::ROLE_USER, User::ROLE_FRANCHISE])
         && !empty(Yii::$app->user->identity->parent_id)) {     
            $user_id = Yii::$app->user->identity->id;
            $parent = User::findOne(Yii::$app->user->identity->parent_id);   
            yii::$app->user->login($parent);
            Yii::$app->session->set('user_client_franchise', $user_id);       
        }        
        
		if(Yii::$app->user->isGuest) {
			return $this->redirect(['site/login']);
		}

        return parent::beforeAction($action);
    }

    public function render($view, $params)
    { 

        if(Yii::$app->session->has('user_client_franchise')) { 
          $user = User::findOne(Yii::$app->session->get('user_client_franchise'));  
          Yii::$app->session->remove('user_client_franchise');             
          yii::$app->user->login($user);             
        }

        // before render
        return parent::render($view, $params);
    }    

    public function actionIndex()
    {   
    	$searchModel = new UserSearch(['id' => Yii::$app->user->identity->id]);

		// Get all custom roles created by the user
        $dataProvider = $searchModel->getRolesData();

        $this->layout = 'kosmoouter';   
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {    

    	if (!empty(Yii::$app->request->post('name'))) {
    		$auth = Yii::$app->authManager;

    		/* Create Role and assign permissions */
    		$name = Yii::$app->user->identity->id . '_'.Yii::$app->request->post('name');
    		if(!$auth->getRole($name)) {
		        $role = $auth->createRole($name);
		        $auth->add($role);        

				$permissions = Yii::$app->request->post('permissions');
				foreach ($permissions as $permission_name) {
					$permission = $auth->getPermission($permission_name);
					$auth->addChild($role, $permission);
				}
		 	}

			return $this->redirect(['index']);			   		
    	} 

   		$permission_lists = array();
   		$permissions = Yii::$app->authManager->getPermissionsByUser(yii::$app->user->identity->id);

   		foreach ($permissions as $permission) {
   			$permission_lists[$permission->name] = $permission->description;
   		}

        $this->layout = 'kosmoouter';         
        return $this->render('create', ['permissions' => $permission_lists]);   	
    }

    public function actionView($name)
    {       
    	$auth = Yii::$app->authManager;

    	$permission_lists = array();
   		$permissions = $auth->getPermissionsByRole($name);

   		foreach ($permissions as $permission) {
   			$permission_lists[$permission->name] = $permission->description;
   		}

        $this->layout = 'kosmoouter';   
        return $this->render('view', ['name' => $name, 'permissions' => $permission_lists]);
    }

    public function actionUpdate($name) {    

    	if (!empty(Yii::$app->request->post('newname'))) {
    		$auth = Yii::$app->authManager;

    		$role = $auth->getRole($name);
    		$auth->removeChildren($role);

    		$newname = Yii::$app->user->identity->id . '_'.Yii::$app->request->post('newname');    		
    		$newrole = $auth->createRole($newname);

    		if($auth->update($name, $newrole)) {
				$permissions = Yii::$app->request->post('permissions');
				foreach ($permissions as $permission_name) {
					$permission = $auth->getPermission($permission_name);
					$auth->addChild($newrole, $permission);
				}
		 	}

			return $this->redirect(['index']);			   		
    	}

 		$permissions = Yii::$app->authManager->getPermissionsByRole($name);
 		$role_permissions = array_keys($permissions);

   		$permission_lists = array();
   		$permissions = Yii::$app->authManager->getPermissionsByUser(yii::$app->user->identity->id);

   		foreach ($permissions as $permission) {
   			$permission_lists[$permission->name] = $permission->description;
   		}

        $this->layout = 'kosmoouter';         
        return $this->render('update', ['name' => $name, 'permissions' => $permission_lists, 
        	'role_permissions' => $role_permissions]);   	
    }

   	public function actionDelete($name) 
	{
		$auth = Yii::$app->authManager;

    	$role = $auth->getRole($name);

		if ($auth->remove($role)) {
			Yii::$app->getSession()->setFlash('success', 'Successfully deleted!');
		} 			

		return $this->redirect(['index']);
	}
}