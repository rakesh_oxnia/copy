<?php

namespace frontend\controllers;

use Yii;
use common\models\Packages;
use frontend\models\PackagesSearch;
use yii\web\Controller;
use yii\helpers\json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\widgets\ActiveForm;
use yii\web\Response;
use app\models\Subscription;
use common\models\AdminSetting;
use common\models\User;
use common\models\LoginForm;

/**
 * PackageController implements the CRUD actions for Packages model.
 */
class PackagesController extends Controller
{

	public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update1', 'update', 'delete'],
                        'roles' => [User::ADMIN_ROLE, User::SUBADMIN_ROLE],
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('managePackages');                     
                        }                     
                    ],  
                    [
                        'allow' => true,
                        'actions' => ['subscription'],
                        'roles' => ['@']                 
                    ]                                      
                ],
            ], 
        ];
    }

	public function beforeAction($action)
    {
        if(!parent::beforeAction($action)) {
            return false;
        }

        LoginForm::userAssitLogin();


    	  if(Yii::$app->user->isGuest){
    			return $this->redirect(['site/login']);
    		}

        return true;
    }

    public function render($view, $params)
    {
        $params = LoginForm::userAssitLogout($params);

        // before render
        return parent::render($view, $params);
    }


    /**
     * Lists all Packages models.
     * @return mixed
     */
    public function actionIndex()
    {
				//$this->layout = 'dashboard';
				$this->layout = 'kosmoouter';
        $searchModel = new PackagesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Packages model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
		$this->layout = 'kosmoouter';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Packages model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
				$this->layout = 'kosmoouter';
			  $model = new Packages();


			//	$model->scenario = 'packages_name';
				if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()))
			 {
			  Yii::$app->response->format = 'json';
				return ActiveForm::validate($model); \Yii::$app->end();
				//$model->scenario = 'packages_name';

				//$model->scenario = 'packages_name';
			 }

				// Yii::$app->getSession()->setFlash('success', []);
        if ($model->load(Yii::$app->request->post())) {
				//	$model = new Packages();

							$model->interval = "month";
							$model->currency = Yii::$app->request->post('currency');

							$model->created_at = strtotime("now");


						  $model->save();
					// 		if(!$model->save()){
         //     print_r($model->getErrors());
         //     die("not saved!");
         // }
              ///echo'<pre>';	print_r($stripe_response);exit;
	            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionSubscription() {
			       $this->layout = 'kosmoouter';
						 if(Yii::$app->user->identity->free_access==1){
							 $subscription_type = 'free_access';
						 }else{
							 $check_subscribe = Subscription::findOne(['user_id'=>Yii::$app->user->identity->id]);
							 if($check_subscribe)
							 {
								 $subscription_type = $check_subscribe->package_name;
							 }else{
								 $per_lead_cost = Yii::$app->user->identity->per_lead_cost;
								 if(empty($per_lead_cost)){
					 				$adminSetting	= AdminSetting::findOne(1);
					 				$single_charge = $adminSetting->global_per_lead_cost;
					 				$transactionCurrency = $adminSetting->global_per_lead_cost_currency;
					 			}else{
					 				$single_charge = $per_lead_cost;
					 				$transactionCurrency = Yii::$app->user->identity->per_lead_cost_currency;
					 			}
			           $subscription_type = 'pay_as_you_go';
			         }
						 }
						 if($subscription_type=='pay_as_you_go'){
						   return $this->render('subscription', ['subscription_type'
							                                       =>$subscription_type,
																										 'single_charge'=>$single_charge,
																									   'transactionCurrency'=>$transactionCurrency]);
						 }else{
							 return $this->render('subscription', ['subscription_type'
							                                       =>$subscription_type,
																									  ]);
						 }

	}

		// public function actionValidate()
	  //    {
	  //         $model = new Packages();
	  //         if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()))
	  //        {
	  //        Yii::$app->response->format = Response::FORMAT_JSON;
    //       return ActiveForm::validate($model); \Yii::$app->end();	         }
		//
	  //    }
    /**
     * Updates an existing Packages model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate1()
		{
			  \Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);
        $product =   \Stripe\Product::all(array("limit" => 3));
				echo'<pre>';	print_r($product);exit;
        $product = \Stripe\Product::retrieve("prod_D8FTKX63wC8yey");
				$product->delete();
		}
    public function actionUpdate($id)
    {
				echo 'Packages cannot be updated';exit;
		    $this->layout = 'kosmoouter';
        $model = $this->findModel($id);
				if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()))
			 {
				Yii::$app->response->format = 'json';
				return ActiveForm::validate($model); \Yii::$app->end();
			 }
        if ($model->load(Yii::$app->request->post())) {
					//print_r($model->stripe_plan_id);exit;
						$model->interval = "month";
						$model->currency = Yii::$app->request->post('currency');
						$model->updated_at = strtotime("now");
						$model->save();
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Packages model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Packages model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Packages the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Packages::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
