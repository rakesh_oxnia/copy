<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use app\models\Subscription;
use app\models\Leads;
use common\models\User;
use frontend\models\Invoice;
use common\models\StripeLog;

class PaymentController extends Controller
{
          //COMMENTED THIS SO THAT CRON FUNCTIONS CAN BE ACCESSED FROM COMMAND WITHOUT NEED TO LOGIN
          // public function beforeAction($action)
          // {
          //     if(Yii::$app->user->isGuest){
          //       return $this->redirect(['site/login']);
          //     }
          //     return parent::beforeAction($action);
          // }

          public function actionFailPayment(){
            exit;
            $invoice_id = Yii::$app->request->post('invoice_id');
            if($invoice_id){
              $invoice = Invoice::findOne($invoice_id);
               //echo '<pre>';print_r($invoice->userModel);exit;
                if($invoice->automatic_charge_status==1){
                       $charge = $this->createFailStripeCharge($invoice);
                       if(isset($charge->code)){
                         $invoice->failed_invoice = 0;
                         $invoice->stripe_payment_id = $charge->json['id'];
                         $invoice->invoice_status = 1;
                         $invoice->update_at = $charge->json['created'];
                         $invoice->payment_date = $charge->json['created'];
                         $invoice->pending_invoice = 0;
                         //Send Mail//
                         if($invoice->save()){
                           //Update in the user//
                           $userdata = User::findOne(['id' => $invoice_id->user_id]);
                           $userdata->payment_status = 0;
                           $userdata->save();
                           //Update in User Table User//

                           try {
                              $sendMail = Subscription::sendSuccessMail($invoice->total_amount,$invoice->created_at,$userdata->email,$invoice->transaction_currency);
                           }catch(\Exception $e) {
                             //do nothing
                           }

                         }
                         //Send Mail//

                         return json_encode(['success'=>true,'data'=>'Payment Paid Successfully']);
                       }else{
                         // $invoice->message = $charge["message"];
                         // if(isset($charge["charge"])){
                         //   $invoice->charge = $charge["charge"];
                         //   $invoice->decline_code = $charge["decline_code"];
                         //   $invoice->doc_url = $charge["doc_url"];
                         //   $invoice->code = $charge["code"];
                         //   $invoice->type = $charge["type"];
                         // }

                         if(isset($charge["message"])){
                           $invoice->message = $charge["message"];
                         }
                         if(isset($charge["charge"])){
                           $invoice->charge = $charge["charge"];
                         }
                         if(isset($charge["decline_code"])){
                           $invoice->decline_code = $charge["decline_code"];
                         }
                         if(isset($charge["doc_url"])){
                           $invoice->doc_url = $charge["doc_url"];
                         }if(isset($charge["code"])){
                           $invoice->code = $charge["code"];
                         }if(isset($charge["type"])){
                           $invoice->type = $charge["type"];
                         }

                          //Send Mail//
                         if($invoice->save()){
                           try {
                              $sendMail = Subscription::sendFailMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->message);//NEED TO CHANGE THIS FUNCTION SENDFAILMAIL
                            }
                            catch(\Exception $e) {
                            	//do nothing
                            }

                         }
                         //Send Mail//
                         return json_encode(['success'=>true,'data'=>$charge["message"]]);
                       }
                }
            }
          }

          public function actionStripeBillingPayment()
          {
            //$today_timestamp = strtotime(date('2018-11-30 00:00:00'));
            $today_timestamp = strtotime(date('Y-m-d 00:00:00'));

            //we can't renew subscription which was created today itself;
            $subscriptions = Subscription::find()
                            ->andWhere(["automatic_charge"=>0])
                            ->andWhere(["is_deactivated"=>0])
                            ->andWhere(["is_cancelled"=>0])
                            ->andWhere(["<", "created_at", $today_timestamp])
                            ->all();

            if($subscriptions){
              //echo '<pre>';
              foreach($subscriptions as $subscription){
                 //print_r($subscription);

                 $user = $subscription->subscribedUserModel;
                 $user_group_id = $user->user_group;

                 if($subscription->subscribe_status==1){
                   //$first_minute = mktime(0, 0, 0, date("n")-1, 1);
                   $first_minute = strtotime(date('Y-m-d 00:00:00', strtotime('first day of previous month')));
                 }elseif($subscription->subscribe_status==0){
                   //$first_minute = $subscription->created_at;
                   $first_minute = strtotime(date('Y-m-d 00:00:00', $subscription->created_at));
                   $subscription->subscribe_status = 1;
                   $subscription->save();
                 }
                 //$last_minute = mktime(0, 0, 0, date("n"), 1);
                 //$last_minute = $last_minute - 1;
                 $last_minute = strtotime(date('Y-m-d 23:59:59', strtotime('last day of previous month')));

                 //$first_minute = 1540339200;// strtotime('2018-10-24 00:00:00');
                 //$last_minute = 1540425599;// strtotime('2018-10-24 23:59:59');

                 $lead_count = Leads::find()
                         ->andWhere(['user_group'=>$user_group_id])
                         ->andWhere(['is_lead'=>1])
                         ->andWhere(['between','c_time',$first_minute,$last_minute])
                         ->count();

                 //echo '<br>====LEAD COUNT===';
                 //echo '<br>'.$lead_count;

                 $maxDays=date('t'); //Total Day of This Month;
                 $currentDayOfMonth=date('j'); //Current date of Month;
                 $totalDaysLeft = ($maxDays-$currentDayOfMonth)+1;
                 $singleDayCharge = ($subscription->packageModel->amount)/$maxDays;
                 $currentChargeAmount = $singleDayCharge*$totalDaysLeft;

                  //if($lead_count!="0" && $lead_count!=""){
                    if($subscription->advance_payment==1){
                      if($lead_count<=$subscription->packageModel->number_of_leads){

                        if($subscription->gst_status==1){
                          $gst_percent = $subscription->gst_percent;
                          // $gst_charge = ($subscription->packageModel->amount)*($subscription->gst_percent/100);
                          // $finalCharge = $subscription->packageModel->amount+$gst_charge;
                          // $amount = round($finalCharge);
                          $gst_charge = ($currentChargeAmount)*($subscription->gst_percent/100);
                          $finalCharge = ($currentChargeAmount)+$gst_charge;
                          $amount = round($finalCharge);
                        }else{
                          $gst_percent = 0;
                          $gst_charge = 0;
                          //$amount = round($subscription->packageModel->amount);
                          $amount = round($currentChargeAmount);
                        }
                        //GST Calculation//
                        //Update A Invoice//
                        $invoice = new Invoice();
                        $invoice->user_id = $user->id;
                        $invoice->total_amount = $amount;
                        $invoice->gst_charge = $gst_charge;
                        $invoice->gst_percent = $gst_percent;
                        $invoice->advanced_payment = $subscription->packageModel->amount;
                        $invoice->package_id = $subscription->packageModel->id;
                        $invoice->stripe_payment_id = "Null";
                        $invoice->allocated_lead = $subscription->packageModel->number_of_leads;
                        $invoice->extra_per_lead_amount = $subscription->extra_per_lead_amount;
                        $invoice->extra_lead_consumed = 0;
                        $invoice->advance_payment_status = 1;
                        $invoice->transaction_currency = $subscription->packageModel->currency;
                        $invoice->invoice_status = 0;// I.S is 1 only when actual payment is done, in this case it will be 0 because right now no money was deducted, once admin manually receives the money he will go into the invoice listing page and mark the invoice status as "Paid"
                        $invoice->automatic_charge_status = 0;
                        $invoice->created_at = strtotime("now");
                        $invoice->billing_date = strtotime("now");
                        $invoice->payment_date = 0;
                        $invoice->subscription_id = $subscription->id;
                        $invoice->cron_added = 1;
                        $invoice->cron_updated = 0;
                        $invoice->remaining_days = $totalDaysLeft;//total days remaining as per pro rata
                        $invoice->remaining_days_total_amount = $currentChargeAmount;//actual charge based on pro rata without gst
                        $invoice->single_day_charge = $singleDayCharge;//single day charege based on pro rata
                        if($invoice->save()){
                          try {
                            $sendMail = Subscription::sendPackagePaymentSuccessMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->id);
                          }
                          catch(\Exception $e) {
                            //do nothing
                          }

                        }
                        //Update A Invoice//
                      }else{
                        $extralead = $lead_count-$subscription->packageModel->number_of_leads;
                        $extra_amount = $extralead*$subscription->extra_per_lead_amount;

                        //GST Calculation//
                        if($subscription->gst_status==1){
                          $gst_percent= $subscription->gst_percent;
                          // $gst_charge =  ($subscription->packageModel->amount+$extra_amount)*($subscription->gst_percent/100);
                          // $total_amount = round($subscription->packageModel->amount+$gst_charge+$extra_amount);
                          $gst_charge =  ($currentChargeAmount+$extra_amount)*($subscription->gst_percent/100);
                          $total_amount = round($currentChargeAmount+$gst_charge+$extra_amount);
                        }else{
                          $gst_percent =0;
                          $gst_charge = 0;
                          //$total_amount = round($extra_amount+$subscription->packageModel->amount);
                          $total_amount = round($extra_amount+$currentChargeAmount);
                        }
                        //GST Calculation//
                        //Update A Invoice//
                        $invoice = new Invoice();
                        $invoice->user_id = $user->id;
                        $invoice->total_amount = $total_amount;
                        $invoice->gst_charge = $gst_charge;
                        $invoice->gst_percent = $gst_percent;
                        $invoice->advanced_payment = $subscription->packageModel->amount;
                        $invoice->package_id = $subscription->packageModel->id;
                        $invoice->stripe_payment_id = "Null";
                        $invoice->allocated_lead = $subscription->packageModel->number_of_leads;
                        $invoice->extra_per_lead_amount = $subscription->extra_per_lead_amount;
                        $invoice->extra_lead_consumed = $extralead;
                        $invoice->advance_payment_status = 1;
                        $invoice->transaction_currency = $subscription->packageModel->currency;
                        $invoice->invoice_status = 0;
                        $invoice->automatic_charge_status = 0;
                        $invoice->created_at = strtotime("now");
                        $invoice->billing_date = strtotime("now");
                        $invoice->payment_date = 0;
                        $invoice->subscription_id = $subscription->id;
                        $invoice->cron_added = 1;
                        $invoice->cron_updated = 0;
                        $invoice->remaining_days = $totalDaysLeft;//total days remaining as per pro rata
                        $invoice->remaining_days_total_amount = $currentChargeAmount;//actual charge based on pro rata without gst
                        $invoice->single_day_charge = $singleDayCharge;//single day charege based on pro rata
                        if($invoice->save()){
                          try {
                            $sendMail = Subscription::sendPackagePaymentSuccessMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->id);
                          }
                          catch(\Exception $e) {
                            //do nothing
                          }

                        }
                      }
                    } elseif ($subscription->advance_payment==0){
                      if($lead_count<=$subscription->packageModel->number_of_leads)
                      {
                         //Pay for pending Invoice //
                         $invoice = Invoice::findOne(['user_id'=>$user->id,
                                                      'pending_invoice'=>1,
                                                      'is_cancelled'=>0,
                                                      'failed_invoice'=>0]);
                          //Old Invoice//
                          if($invoice){
                            //update pending Invoice //
                            $invoice->stripe_payment_id = "Null";
                            $invoice->update_at = strtotime("now");
                            $invoice->billing_date = strtotime("now");
                            $invoice->payment_date = 0;
                            $invoice->pending_invoice = 0;
                            $invoice->subscription_id = $subscription->id;
                            $invoice->cron_updated = 1;
                            //Send Mail//
                            if($invoice->save()){
                              try {
                                $sendMail = Subscription::sendPackagePaymentSuccessMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->id);
                              }
                              catch(\Exception $e) {
                              	//do nothing
                              }
                            }
                          }

                        //Pay for pending Invoice //
                        $maxDays=date('t'); //Total Day of This Month;
                        $currentDayOfMonth=date('j'); //Current date of Month;
                        $totalDaysLeft = ($maxDays-$currentDayOfMonth)+1;
                        $singleDayCharge = $subscription->packageModel->amount/$maxDays;
                        $currentChargeAmount = $singleDayCharge*$totalDaysLeft;
                        //Create new pending Invoice//
                        //Calculate GST//
                        if($subscription->gst_status==1){
                          $gst_percent = $subscription->gst_percent;
                          $gst_charge = ($currentChargeAmount)*($subscription->gst_percent/100);
                          $finalCharge = ($currentChargeAmount)+$gst_charge;
                          $amount = round($finalCharge);
                        }else{
                          $gst_percent = 0;
                          $gst_charge = 0;
                          //$amount = round($subscription->packageModel->amount);
                          $amount = round($currentChargeAmount);
                        }

                        $invoice = new Invoice();
                        $invoice->user_id = $user->id;
                        $invoice->total_amount = $amount;
                        $invoice->gst_charge = $gst_charge;
                        $invoice->gst_percent = $subscription->gst_percent;
                        $invoice->package_id = $subscription->packageModel->id;
                        $invoice->stripe_payment_id = "Null";
                        $invoice->advanced_payment = $subscription->packageModel->amount;
                        $invoice->allocated_lead = $subscription->packageModel->number_of_leads;
                        $invoice->extra_per_lead_amount = $subscription->extra_per_lead_amount;
                        $invoice->extra_lead_consumed = 0;
                        $invoice->advance_payment_status = $subscription->advance_payment;
                        $invoice->transaction_currency = $subscription->packageModel->currency;
                        $invoice->invoice_status = 0;
                        $invoice->automatic_charge_status = 0;
                        $invoice->created_at = strtotime("now");
                        $invoice->payment_date = 0;
                        $invoice->pending_invoice = 1;
                        $invoice->subscription_id = $subscription->id;
                        $invoice->cron_added = 1;
                        $invoice->cron_updated = 0;
                        $invoice->remaining_days = $totalDaysLeft;//total days remaining as per pro rata
                        $invoice->remaining_days_total_amount = $currentChargeAmount;//actual charge based on pro rata without gst
                        $invoice->single_day_charge = $singleDayCharge;//single day charege based on pro rata
                        $invoice->save();
                        //Create new pending Invoice//
                    }else{
                        $extralead = $lead_count-$subscription->packageModel->number_of_leads;
                        $extra_amount = $extralead*$subscription->extra_per_lead_amount;

                        $invoice = Invoice::findOne(['user_id'=>$user->id,
                                                     'pending_invoice'=>1,
                                                     'is_cancelled'=>0,
                                                     'failed_invoice'=>0]);
                         //Pay for Extra Leads+Pending invoice//

                         if($invoice){
                           //GST Calculation//
                           $invoice_amount = $invoice->total_amount;
                           $original_invoice_amount_without_gst = $invoice->remaining_days_total_amount;
                           if($subscription->gst_status==1){
                             $gst_percent= $invoice->gst_percent;
                             $gst_charge =  ($original_invoice_amount_without_gst+$extra_amount)*($gst_percent/100);

                            $total_amount = round($original_invoice_amount_without_gst+$gst_charge+$extra_amount);
                           }else{
                             $gst_percent =0;
                             $gst_charge = 0;
                             $total_amount = round($extra_amount+$invoice_amount);
                           }

                          //update pending Invoice //
                          $invoice->stripe_payment_id = "Null";
                          $invoice->total_amount = $total_amount;
                          $invoice->gst_charge = $gst_charge;
                          $invoice->gst_percent = $gst_percent;
                          $invoice->allocated_lead = $subscription->packageModel->number_of_leads;
                          $invoice->extra_lead_consumed = $extralead;
                          $invoice->advance_payment_status = $subscription->advance_payment;
                          $invoice->transaction_currency = $invoice->transaction_currency;
                          $invoice->invoice_status = 0;
                          $invoice->update_at = strtotime("now");
                          $invoice->payment_date = 0;
                          $invoice->pending_invoice = 0;
                          $invoice->subscription_id = $subscription->id;
                          $invoice->cron_updated = 1;
                          //Send Mail//
                          if($invoice->save()){
                            try {
                              $sendMail = Subscription::sendPackagePaymentSuccessMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->id);
                            }
                            catch(\Exception $e) {
                            	//do nothing
                            }
                          }
                        }

                       //Pay for Extra Leads+Pending invoice//
                       //Pay for pending Invoice //
                       $maxDays=date('t'); //Total Day of This Month;
                       $currentDayOfMonth=date('j'); //Current date of Month;
                       $totalDaysLeft = ($maxDays-$currentDayOfMonth)+1;
                       $singleDayCharge = $subscription->packageModel->amount/$maxDays;
                       $currentChargeAmount = $singleDayCharge*$totalDaysLeft;
                       //Create new Pending Invoice//
                       //Calculate GST//
                       if($subscription->gst_status==1){
                         $gst_percent = $subscription->gst_percent;
                         $gst_charge = $currentChargeAmount*($subscription->gst_percent/100);
                         $finalCharge = $currentChargeAmount+$gst_charge;
                         $amount = round($finalCharge);
                       }else{
                         $gst_percent = 0;
                         $gst_charge = 0;
                         $amount = round($currentChargeAmount);
                       }

                       $invoice = new Invoice();
                       $invoice->user_id = $user->id;
                       $invoice->total_amount = $amount;
                       $invoice->gst_charge = $gst_charge;
                       $invoice->gst_percent = $subscription->gst_percent;
                       $invoice->package_id = $subscription->packageModel->id;
                       $invoice->stripe_payment_id = "Null";
                       $invoice->advanced_payment = $subscription->packageModel->amount;
                       $invoice->allocated_lead = $subscription->packageModel->number_of_leads;
                       $invoice->extra_per_lead_amount = $subscription->extra_per_lead_amount;
                       $invoice->extra_lead_consumed = 0;
                       $invoice->advance_payment_status = $subscription->advance_payment;
                       $invoice->transaction_currency = $subscription->packageModel->currency;
                       $invoice->invoice_status = 0;
                       $invoice->automatic_charge_status = 0;
                       $invoice->created_at = strtotime("now");
                       //$invoice->billing_date = strtotime("now");
                       $invoice->payment_date = 0;
                       $invoice->pending_invoice = 1;
                       $invoice->subscription_id = $subscription->id;
                       $invoice->cron_added = 1;
                       $invoice->cron_updated = 0;
                       $invoice->remaining_days = $totalDaysLeft;//total days remaining as per pro rata
                       $invoice->remaining_days_total_amount = $currentChargeAmount;//actual charge based on pro rata without gst
                       $invoice->single_day_charge = $singleDayCharge;//single day charege based on pro rata
                       $invoice->save();
                      }
                    }
              }
            }else {
              echo 'None found';
            }
          }

          public function actionStripeAutoPayment()
          {
            //$today_timestamp = strtotime(date('2018-11-30 00:00:00'));
            $today_timestamp = strtotime(date('Y-m-d 00:00:00'));

            //we can't renew those subscriptions which were created today itself
            $subscriptions = Subscription::find()
                            ->andWhere(["automatic_charge"=>1])
                            ->andWhere(["is_deactivated"=>0])
                            ->andWhere(["is_cancelled"=>0])
                            ->andWhere(["<", "created_at", $today_timestamp])
                            ->all();

            if($subscriptions){
              //echo '<pre>';
              foreach ($subscriptions as $subscription)
              {
                //print_r($subscription);
                $user = $subscription->subscribedUserModel;
                $user_group_id = $user->user_group;

                if($subscription->subscribe_status==1){
                  //$first_minute = mktime(0, 0, 0, date("n")-1, 1);
                  $first_minute = strtotime(date('Y-m-d 00:00:00', strtotime('first day of previous month')));
                }elseif($subscription->subscribe_status==0){
                  //$first_minute = $subscription->created_at;
                  $first_minute = strtotime(date('Y-m-d 00:00:00', $subscription->created_at));
                  $subscription->subscribe_status = 1;
                  $subscription->save();
                }

                //$last_minute = mktime(0, 0, 0, date("n"), 1);
                //$last_minute = $last_minute - 1;
                $last_minute = strtotime(date('Y-m-d 23:59:59', strtotime('last day of previous month')));

                // echo date('Y-m-d H:i:s',$first_minute);
                // echo '<br>';
                // echo date('Y-m-d H:i:s',$last_minute);
                // echo '<br>';exit;

                //$first_minute = 1540339200;// strtotime('2018-10-24 00:00:00');
                //$last_minute = 1540425599;// strtotime('2018-10-24 23:59:59');

                $lead_count = Leads::find()
                              ->andWhere(['user_group'=>$user_group_id])
                              ->andWhere(['is_lead'=>1])
                              ->andWhere(['between','c_time',$first_minute,$last_minute])
                              ->count();
                              //print_r($lead_count);echo '<br>';

                // echo '<br>====LEAD COUNT===';
                // echo '<br>'.$lead_count;

                $maxDays=date('t'); //Total Day of This Month;
                $currentDayOfMonth=date('j'); //Current date of Month;
                $totalDaysLeft = ($maxDays-$currentDayOfMonth)+1;
                $singleDayCharge = ($subscription->packageModel->amount)/$maxDays;
                $currentChargeAmount = $singleDayCharge*$totalDaysLeft;

                //if($lead_count!="0" && $lead_count!=""){
                  if($subscription->advance_payment==1){
                    if($lead_count<=$subscription->packageModel->number_of_leads)
                    {
                      //Only Advanced Payment needed No Extra Lead//
                                    //GST Calculation//
                        if($subscription->gst_status==1){
                          $gst_percent=$subscription->gst_percent;
                          // $gst_charge = ($subscription->packageModel->amount)*($subscription->gst_percent/100);
                          // $finalCharge = $subscription->packageModel->amount+$gst_charge;
                          // $amount = round($finalCharge)*100;

                          $gst_charge = ($currentChargeAmount)*($subscription->gst_percent/100);
                          $finalCharge = ($currentChargeAmount)+$gst_charge;
                          $amount = round($finalCharge)*100;
                        }else{
                          $gst_percent =0;
                          $gst_charge = 0;
                          //$amount = round($subscription->packageModel->amount)*100;
                          $amount = round($currentChargeAmount)*100;
                        }
                                 //GST Calculation//
                        $charge = $this->createAdvancedPaymentStripeCharge($subscription,$user,$amount);
                        if(isset($charge->code)){
                          //Update A Subscription//
                          //echo '<br>1->1 insert no extra';
                          $invoice = new Invoice();
                          $invoice->user_id = $user->id;
                          $invoice->total_amount = $charge->json['amount']/100;
                          $invoice->gst_charge = $gst_charge;
                          $invoice->gst_percent = $gst_percent;
                          $invoice->advanced_payment = $subscription->packageModel->amount;
                          $invoice->package_id = $subscription->packageModel->id;
                          $invoice->stripe_payment_id = $charge->json['id'];
                          $invoice->allocated_lead = $subscription->packageModel->number_of_leads;
                          $invoice->extra_per_lead_amount = $subscription->extra_per_lead_amount;
                          $invoice->extra_lead_consumed = 0;
                          $invoice->advance_payment_status = 1;
                          $invoice->transaction_currency = $subscription->packageModel->currency;
                          $invoice->invoice_status = 1;
                          $invoice->automatic_charge_status = 1;
                          $invoice->created_at = strtotime("now");
                          $invoice->billing_date = strtotime("now");
                          $invoice->payment_date = $charge->json['created'];
                          $invoice->subscription_id = $subscription->id;
                          $invoice->cron_added = 1;
                          $invoice->cron_updated = 0;
                          $invoice->remaining_days = $totalDaysLeft;//total days remaining as per pro rata
                          $invoice->remaining_days_total_amount = $currentChargeAmount;//actual charge based on pro rata without gst
                          $invoice->single_day_charge = $singleDayCharge;//single day charege based on pro rata
                          //Send Mail//
                          if($invoice->save()){
                            try {
                              $sendMail = Subscription::sendPackagePaymentSuccessMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->id);
                            }
                            catch(\Exception $e) {
                            	//do nothing
                            }

                          }
                          //Send Mail//
                          //Update A Subscription//
                        }else{
                          //+++++++Error Transaction Failed++++++++++++//
                          $invoice = new Invoice();
                          $invoice->user_id = $user->id;
                          $invoice->total_amount = $amount/100;
                          $invoice->gst_charge = $gst_charge;
                          $invoice->gst_percent = $gst_percent;
                          $invoice->advanced_payment = $subscription->packageModel->amount;
                          $invoice->package_id = $subscription->packageModel->id;
                          $invoice->stripe_payment_id = 'Null';
                          $invoice->allocated_lead = $subscription->packageModel->number_of_leads;
                          $invoice->extra_per_lead_amount = $subscription->extra_per_lead_amount;
                          $invoice->extra_lead_consumed = 0;
                          $invoice->advance_payment_status = 1;
                          $invoice->transaction_currency = $subscription->packageModel->currency;
                          $invoice->invoice_status = 0;
                          $invoice->automatic_charge_status = 1;
                          $invoice->created_at = strtotime("now");
                          $invoice->billing_date = strtotime("now");
                          $invoice->payment_date = 0;
                          $invoice->pending_invoice = 1;
                          $invoice->failed_invoice = 1;
                          $invoice->subscription_id = $subscription->id;
                          $invoice->cron_added = 1;
                          $invoice->cron_updated = 0;
                          $invoice->remaining_days = $totalDaysLeft;//total days remaining as per pro rata
                          $invoice->remaining_days_total_amount = $currentChargeAmount;//actual charge based on pro rata without gst
                          $invoice->single_day_charge = $singleDayCharge;//single day charege based on pro rata
                          if(isset($charge["message"])){
                            $invoice->message = $charge["message"];
                          }
                          if(isset($charge["charge"])){
                            $invoice->charge = $charge["charge"];
                          }
                          if(isset($charge["decline_code"])){
                            $invoice->decline_code = $charge["decline_code"];
                          }
                          if(isset($charge["doc_url"])){
                            $invoice->doc_url = $charge["doc_url"];
                          }if(isset($charge["code"])){
                            $invoice->code = $charge["code"];
                          }if(isset($charge["type"])){
                            $invoice->type = $charge["type"];
                          }

                          //Send Mail//
                          if($invoice->save()){
                            //Update in the user//
                            $user->payment_status = 1;
                            $user->save();
                            //Update in User Table User//
                            try {
                              //$sendMail = Subscription::sendFailMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->message);
                              $sendMail = Subscription::sendPackagePaymentFailMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->message);
                            }
                            catch(\Exception $e) {
                            	//do nothing
                            }

                          }
                          //Send Mail//
                          //+++++++Error Transaction Failed++++++++++++//
                        }
                    }else{
                      $extralead = $lead_count-$subscription->packageModel->number_of_leads;
                      $extra_amount = $extralead*$subscription->extra_per_lead_amount;
                                   //Advanced Plus Extra payment//
                                        //GST Calculation//
                        if($subscription->gst_status==1){
                        $gst_percent= $subscription->gst_percent;
                        // $gst_charge =  ($subscription->packageModel->amount+$extra_amount)*($subscription->gst_percent/100);
                        // $total_amount = round($subscription->packageModel->amount+$gst_charge+$extra_amount);
                        $gst_charge =  ($currentChargeAmount+$extra_amount)*($subscription->gst_percent/100);
                        $total_amount = round($currentChargeAmount+$gst_charge+$extra_amount);
                        }else{
                        $gst_percent = 0;
                        $gst_charge = 0;
                        //$total_amount = round($extra_amount+$subscription->packageModel->amount);
                        $total_amount = round($extra_amount+$currentChargeAmount);
                        }
                                        //GST Calculation//
                      $charge = $this->createAdvancedExtraPaymentStripeCharge($subscription,$user,$total_amount,$extra_amount,$subscription->packageModel->amount);
                      if(isset($charge->code)){
                        //Update A Invoice//
                        //echo '<br>1->1 insert extra';
                        $invoice = new Invoice();
                        $invoice->user_id = $user->id;
                        $invoice->total_amount = $charge->json['amount']/100;
                        $invoice->gst_charge = $gst_charge;
                        $invoice->gst_percent = $gst_percent;
                        $invoice->advanced_payment = $subscription->packageModel->amount;
                        $invoice->package_id = $subscription->packageModel->id;
                        $invoice->stripe_payment_id = $charge->json['id'];
                        $invoice->allocated_lead = $subscription->packageModel->number_of_leads;
                        $invoice->extra_per_lead_amount = $subscription->extra_per_lead_amount;
                        $invoice->extra_lead_consumed = $extralead;
                        $invoice->advance_payment_status = 1;
                        $invoice->transaction_currency = $subscription->packageModel->currency;
                        $invoice->invoice_status = 1;
                        $invoice->automatic_charge_status = 1;
                        $invoice->created_at = strtotime("now");
                        $invoice->billing_date = strtotime("now");
                        $invoice->payment_date = $charge->json['created'];
                        $invoice->subscription_id = $subscription->id;
                        $invoice->cron_added = 1;
                        $invoice->cron_updated = 0;
                        $invoice->remaining_days = $totalDaysLeft;//total days remaining as per pro rata
                        $invoice->remaining_days_total_amount = $currentChargeAmount;//actual charge based on pro rata without gst
                        $invoice->single_day_charge = $singleDayCharge;//single day charege based on pro rata
                        //Send Mail//
                        if($invoice->save()){
                          try {
                            $sendMail = Subscription::sendPackagePaymentSuccessMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->id);
                          }
                          catch(\Exception $e) {
                          	//do nothing
                          }

                        }
                        //Send Mail//
                        //echo'<pre>';print_r($charge);
                        }else{
                        //++++++++++++Error++++++++++++//
                        //+++++++Error Transaction Failed++++++++++++//
                        $invoice = new Invoice();
                        $invoice->user_id = $user->id;
                        //$invoice->total_amount = $total_amount;
                        $invoice->total_amount = $total_amount;
                        $invoice->gst_charge = $gst_charge;
                        $invoice->gst_percent = $gst_percent;
                        $invoice->advanced_payment = $subscription->packageModel->amount;
                        $invoice->package_id = $subscription->packageModel->id;
                        $invoice->stripe_payment_id = 'Null';
                        $invoice->allocated_lead = $subscription->packageModel->number_of_leads;
                        $invoice->extra_per_lead_amount = $subscription->extra_per_lead_amount;
                        $invoice->extra_lead_consumed = $extralead;
                        $invoice->advance_payment_status = 1;
                        $invoice->transaction_currency = $subscription->packageModel->currency;
                        $invoice->invoice_status = 0;
                        $invoice->automatic_charge_status = 1;
                        $invoice->created_at = strtotime("now");
                        $invoice->billing_date = strtotime("now");
                        $invoice->payment_date = 0;
                        $invoice->pending_invoice = 1;
                        $invoice->failed_invoice = 1;
                        $invoice->subscription_id = $subscription->id;
                        $invoice->cron_added = 1;
                        $invoice->cron_updated = 0;
                        $invoice->remaining_days = $totalDaysLeft;//total days remaining as per pro rata
                        $invoice->remaining_days_total_amount = $currentChargeAmount;//actual charge based on pro rata without gst
                        $invoice->single_day_charge = $singleDayCharge;//single day charege based on pro rata

                        // $invoice->message = $charge["message"];
                        // if(isset($charge["charge"])){
                        //   $invoice->charge = $charge["charge"];
                        //   $invoice->decline_code = $charge["decline_code"];
                        //   $invoice->doc_url = $charge["doc_url"];
                        //   $invoice->code = $charge["code"];
                        //   $invoice->type = $charge["type"];
                        // }

                        if(isset($charge["message"])){
                          $invoice->message = $charge["message"];
                        }
                        if(isset($charge["charge"])){
                          $invoice->charge = $charge["charge"];
                        }
                        if(isset($charge["decline_code"])){
                          $invoice->decline_code = $charge["decline_code"];
                        }
                        if(isset($charge["doc_url"])){
                          $invoice->doc_url = $charge["doc_url"];
                        }if(isset($charge["code"])){
                          $invoice->code = $charge["code"];
                        }if(isset($charge["type"])){
                          $invoice->type = $charge["type"];
                        }

                        //Send Mail//
                        if($invoice->save()){
                          //Update in the user//
                          $user->payment_status = 1;
                          $user->save();
                          //Update in the user//
                          try {
                            $sendMail = Subscription::sendPackagePaymentFailMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->message);
                          }
                          catch(\Exception $e) {
                          	//do nothing
                          }

                        }
                        //Send Mail//
                        //+++++++Error Transaction Failed++++++++++++//
                        //++++++++++++Error++++++++++++//
                       }
                    }
                  }elseif ($subscription->advance_payment==0){
                    if($lead_count<=$subscription->packageModel->number_of_leads)
                    {
                      //Pay for pending Invoice //
                              //Old Invoice//
                       $invoice = Invoice::findOne(['user_id'=>$user->id,
                                                    'pending_invoice'=>1,
                                                    'is_cancelled'=>0,
                                                    'failed_invoice'=>0]);
                        //Old Invoice//
                        if($invoice){
                          $amount = $invoice->total_amount*100;
                          $charge = $this->createAdvancedPaymentStripeChargePendingInvoice($invoice,$user,$amount);
                          //update pending Invoice //
                          if(isset($charge->code)){
                            $invoice->stripe_payment_id = $charge->json['id'];
                            $invoice->invoice_status = 1;
                            $invoice->update_at = $charge->json['created'];
                            $invoice->payment_date = $charge->json['created'];
                            $invoice->pending_invoice = 0;
                            $invoice->subscription_id = $subscription->id;
                            $invoice->cron_updated = 1;
                            //Send Mail//
                            if($invoice->save()){
                              try {
                                $sendMail = Subscription::sendPackagePaymentSuccessMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->id);
                              }
                              catch(\Exception $e) {
                              	//do nothing
                              }

                            }
                            //Send Mail//
                          }else{
                            //+++++++++++++++Error++++++++++++//
                            $invoice->stripe_payment_id = "Null";
                            $invoice->invoice_status = 0;
                            $invoice->update_at = strtotime("now");
                            $invoice->payment_date = 0;
                            $invoice->pending_invoice = 1;
                            $invoice->failed_invoice = 1;
                            $invoice->subscription_id = $subscription->id;
                            $invoice->cron_updated = 1;

                            // $invoice->message = $charge["message"];
                            // if(isset($charge["charge"])){
                            //   $invoice->charge = $charge["charge"];
                            //   $invoice->decline_code = $charge["decline_code"];
                            //   $invoice->doc_url = $charge["doc_url"];
                            //   $invoice->code = $charge["code"];
                            //   $invoice->type = $charge["type"];
                            // }

                            if(isset($charge["message"])){
                              $invoice->message = $charge["message"];
                            }
                            if(isset($charge["charge"])){
                              $invoice->charge = $charge["charge"];
                            }
                            if(isset($charge["decline_code"])){
                              $invoice->decline_code = $charge["decline_code"];
                            }
                            if(isset($charge["doc_url"])){
                              $invoice->doc_url = $charge["doc_url"];
                            }if(isset($charge["code"])){
                              $invoice->code = $charge["code"];
                            }if(isset($charge["type"])){
                              $invoice->type = $charge["type"];
                            }

                            //Send Mail//
                            if($invoice->save()){
                              //Update in the user//
                              $user->payment_status = 1;
                              $user->save();
                              //Update in User Table User//
                              try {
                                //$sendMail = Subscription::sendFailMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->message);
                                $sendMail = Subscription::sendPackagePaymentFailMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->message);
                              }
                              catch(\Exception $e) {
                              	//do nothing
                              }

                            }
                            //Send Mail//
                            //+++++++++++++++Error++++++++++++//
                          }
                          //update pending Invoice //
                        }
                      //Pay for pending Invoice //
                      $maxDays=date('t'); //Total Day of This Month;
                      $currentDayOfMonth=date('j'); //Current date of Month;
                      $totalDaysLeft = ($maxDays-$currentDayOfMonth)+1;
                      $singleDayCharge = $subscription->packageModel->amount/$maxDays;
                      $currentChargeAmount = $singleDayCharge*$totalDaysLeft;
                      //Create new pending Invoice//
                                       //Calculate GST//
                      if($subscription->gst_status==1){
                        $gst_charge = ($currentChargeAmount)*($subscription->gst_percent/100);
                        $finalCharge = $currentChargeAmount+$gst_charge;
                        $finalChargeAmount = round($finalCharge)*100;
                      }else{
                        $gst_charge = 0;
                        $finalChargeAmount = round($currentChargeAmount)*100;
                      }
                                     //Calculate GST//
                                      //echo '<br>0->1 insert no extra';
                      $invoice = new Invoice();
                      $invoice->user_id = $user->id;
                      $invoice->total_amount = $finalChargeAmount/100;
                      $invoice->gst_charge = $gst_charge;
                      $invoice->gst_percent = $subscription->gst_percent;
                      $invoice->package_id = $subscription->packageModel->id;
                      $invoice->stripe_payment_id = "Null";
                      $invoice->advanced_payment = $subscription->packageModel->amount;
                      $invoice->allocated_lead = $subscription->packageModel->number_of_leads;
                      $invoice->extra_per_lead_amount = $subscription->extra_per_lead_amount;
                      $invoice->extra_lead_consumed = 0;
                      $invoice->advance_payment_status = $subscription->advance_payment;
                      $invoice->transaction_currency = $subscription->packageModel->currency;
                      $invoice->invoice_status = 0;
                      $invoice->automatic_charge_status = 1;
                      $invoice->created_at = strtotime("now");
                      $invoice->billing_date = strtotime("now");
                      $invoice->payment_date = 0;
                      $invoice->pending_invoice = 1;
                      $invoice->subscription_id = $subscription->id;
                      $invoice->cron_added = 1;
                      $invoice->cron_updated = 0;
                      $invoice->remaining_days = $totalDaysLeft;//total days remaining as per pro rata
                      $invoice->remaining_days_total_amount = $currentChargeAmount;//actual charge based on pro rata without gst
                      $invoice->single_day_charge = $singleDayCharge;//single day charege based on pro rata
                      $invoice->save();
                      //Create new pending Invoice//
                  }else{
                      $extralead = $lead_count-$subscription->packageModel->number_of_leads;
                      $extra_amount = $extralead*$subscription->extra_per_lead_amount;

                      $invoice = Invoice::findOne(['user_id'=>$user->id,
                                                   'pending_invoice'=>1,
                                                   'is_cancelled'=>0,
                                                   'failed_invoice'=>0]);
                       //Pay for Extra Leads+Pending invoice//

                       if($invoice){
                         //GST Calculation//
                         //$invoice_amount = $invoice->total_amount-$invoice->gst_charge;
                         $invoice_amount = $invoice->total_amount;
                         //$original_invoice_amount_without_gst = $invoice->advanced_payment;
                         $original_invoice_amount_without_gst = $invoice->remaining_days_total_amount;
                         //if($invoice->gst_charge>=1){
                         if($subscription->gst_status==1){
                           $gst_percent= $invoice->gst_percent;
                         //$gst_charge =  ($invoice_amount+$extra_amount)*($gst_percent/100);
                         //$total_amount = round($invoice_amount+$gst_charge+$extra_amount);

                          $gst_charge =  ($original_invoice_amount_without_gst+$extra_amount)*($gst_percent/100);

                          $total_amount = round($original_invoice_amount_without_gst+$gst_charge+$extra_amount);
                         }else{
                           $gst_percent =0;
                           $gst_charge = 0;
                           $total_amount = round($extra_amount+$invoice_amount);
                         }
                                       //GST Calculation//
                        $charge = $this->createAdvancedExtraPaymentStripeChargePendingInvoice($invoice,$user,$total_amount,$extra_amount,$invoice_amount);
                        //update pending Invoice //
                        if(isset($charge->code)){
                          $invoice->stripe_payment_id = $charge->json['id'];
                          $invoice->total_amount = $charge->json['amount']/100;
                          $invoice->gst_charge = $gst_charge;
                          $invoice->gst_percent = $gst_percent;
                          $invoice->allocated_lead = $subscription->packageModel->number_of_leads;
                          $invoice->extra_lead_consumed = $extralead;
                          $invoice->advance_payment_status = $subscription->advance_payment;
                          $invoice->transaction_currency = $invoice->transaction_currency;
                          $invoice->invoice_status = 1;
                          $invoice->update_at = $charge->json['created'];
                          $invoice->payment_date = $charge->json['created'];
                          $invoice->pending_invoice = 0;
                          $invoice->subscription_id = $subscription->id;
                          $invoice->cron_updated = 1;
                          //Send Mail//
                          if($invoice->save()){
                            try {
                              $sendMail = Subscription::sendPackagePaymentSuccessMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->id);
                            }
                            catch(\Exception $e) {
                            	//do nothing
                            }

                          }
                          //Send Mail//
                        }else{
                          //error
                          $invoice->stripe_payment_id = "Null";
                          $invoice->total_amount = $total_amount;
                          $invoice->gst_charge = $gst_charge;
                          $invoice->gst_percent = $gst_percent;
                          $invoice->allocated_lead = $subscription->packageModel->number_of_leads;
                          $invoice->extra_lead_consumed = $extralead;
                          $invoice->advance_payment_status = $subscription->advance_payment;
                          $invoice->transaction_currency = $invoice->transaction_currency;
                          $invoice->invoice_status = 0;
                          $invoice->update_at = strtotime("now");
                          //$invoice->payment_date = strtotime("now");
                          $invoice->payment_date = 0;
                          $invoice->pending_invoice = 1;
                          $invoice->failed_invoice = 1;
                          $invoice->subscription_id = $subscription->id;
                          $invoice->cron_updated = 1;

                          // $invoice->message = $charge["message"];
                          // if(isset($charge["charge"])){
                          //   $invoice->charge = $charge["charge"];
                          //   $invoice->decline_code = $charge["decline_code"];
                          //   $invoice->doc_url = $charge["doc_url"];
                          //   $invoice->code = $charge["code"];
                          //   $invoice->type = $charge["type"];
                          // }

                          if(isset($charge["message"])){
                            $invoice->message = $charge["message"];
                          }
                          if(isset($charge["charge"])){
                            $invoice->charge = $charge["charge"];
                          }
                          if(isset($charge["decline_code"])){
                            $invoice->decline_code = $charge["decline_code"];
                          }
                          if(isset($charge["doc_url"])){
                            $invoice->doc_url = $charge["doc_url"];
                          }if(isset($charge["code"])){
                            $invoice->code = $charge["code"];
                          }if(isset($charge["type"])){
                            $invoice->type = $charge["type"];
                          }

                          //Send Mail//
                          if($invoice->save()){
                            //Update in the user//
                            $user->payment_status = 1;
                            $user->save();
                            //Update in User Table User//
                            try {
                              //$sendMail = Subscription::sendFailMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->message);
                              $sendMail = Subscription::sendPackagePaymentFailMail($invoice->total_amount,$invoice->created_at,$user->email,$invoice->transaction_currency,$invoice->message);
                            }
                            catch(\Exception $e) {
                            	//do nothing
                            }

                          }
                          //Send Mail//
                        }
                        //update pending Invoice //
                       }
                       //Pay for Extra Leads+Pending invoice//
                     //Pay for pending Invoice //
                     $maxDays=date('t'); //Total Day of This Month;
                     $currentDayOfMonth=date('j'); //Current date of Month;
                     $totalDaysLeft = ($maxDays-$currentDayOfMonth)+1;
                     $singleDayCharge = $subscription->packageModel->amount/$maxDays;
                     $currentChargeAmount = $singleDayCharge*$totalDaysLeft;
                     //Create new Pending Invoice//
                        //Calculate GST//
                     if($subscription->gst_status==1){
                       $gst_charge = $currentChargeAmount*($subscription->gst_percent/100);
                       $finalCharge = $currentChargeAmount+$gst_charge;
                       $finalChargeAmount = round($finalCharge)*100;
                     }else{
                       $gst_charge = 0;
                       $finalChargeAmount = round($currentChargeAmount)*100;
                     }

                     //Calculate GST//
                     //echo '<br>0->1 insert extra';
                     $invoice = new Invoice();
                     $invoice->user_id = $user->id;
                     $invoice->total_amount = $finalChargeAmount/100;
                     $invoice->gst_charge = $gst_charge;
                     $invoice->gst_percent = $subscription->gst_percent;
                     $invoice->package_id = $subscription->packageModel->id;
                     $invoice->stripe_payment_id = "Null";
                     $invoice->advanced_payment = $subscription->packageModel->amount;
                     $invoice->allocated_lead = $subscription->packageModel->number_of_leads;
                     $invoice->extra_per_lead_amount = $subscription->extra_per_lead_amount;
                     $invoice->extra_lead_consumed = 0;
                     $invoice->advance_payment_status = $subscription->advance_payment;
                     $invoice->transaction_currency = $subscription->packageModel->currency;
                     $invoice->invoice_status = 0;
                     $invoice->automatic_charge_status = 1;
                     $invoice->created_at = strtotime("now");
                     $invoice->billing_date = strtotime("now");
                     $invoice->payment_date = 0;
                     $invoice->pending_invoice = 1;
                     $invoice->subscription_id = $subscription->id;
                     $invoice->cron_added = 1;
                     $invoice->cron_updated = 0;
                     $invoice->remaining_days = $totalDaysLeft;//total days remaining as per pro rata
                     $invoice->remaining_days_total_amount = $currentChargeAmount;//actual charge based on pro rata without gst
                     $invoice->single_day_charge = $singleDayCharge;//single day charege based on pro rata
                     $invoice->save();
                        //Create new Pending Invoice//
                    }
                  }
                //} //closing bracket of $lead_count!="0" && $lead_count!=""
              }
            }else {
              echo 'None found';
            }

          }


                ///Genetate Stripe Charge For Advance Payment////
      protected function createAdvancedPaymentStripeChargePendingInvoice($invoice,$user,$amount){
      try{
      \Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);

      $charge = \Stripe\Charge::create(array(
      "amount" => $amount,
      "currency" => $invoice->transaction_currency,
      "description" => "Charge for Advanced Payment ".$user->email,
      "customer"    => $user->stripeToken,
      "metadata" => [
      "Advanced Payment" => $invoice->advanced_payment,
      "Payment-type" => "",
      "Extra Lead Charge"=> "Null"
      ]
      ));
      }catch(\Stripe\Error\Card $e) {
      // Since it's a decline, \Stripe\Error\Card will be caught
      $body = $e->getJsonBody();
      $err  = $body['error'];

      return [
          'success'=>0,
          'message'=> isset($err['message'])?$err['message']:'',
          'charge'=> isset($err['charge'])?$err['charge']:'',
          'decline_code'=> isset($err['decline_code'])?$err['decline_code']:'',
          'code'=> isset($err['code'])?$err['code']:'',
          'doc_url'=> isset($err['doc_url'])?$err['doc_url']:'',
          'type'=> isset($err['type'])?$err['type']:''
      ];

      } catch (\Stripe\Error\RateLimit $e) {
      // Too many requests made to the API too quickly
       return ['success' => 0, 'message' => 'Rate Limit Error'];

      } catch (\Stripe\Error\InvalidRequest $e) {
      // Invalid parameters were supplied to Stripe's API
       return ['success' => 0, 'message' => 'Invalid parameters were supplied to Stripes API'];

      } catch (\Stripe\Error\Authentication $e) {
      // Authentication with Stripe's API failed
       return ['success' => 0, 'message' => 'Maybe you changed API keys recently'];

      // (maybe you changed API keys recently)
      } catch (\Stripe\Error\ApiConnection $e) {

       return ['success' => 0, 'message' => 'Network communication with Stripe failed'];

      // Network communication with Stripe failed
      } catch (\Stripe\Error\Base $e) {

      return ['success' => 0, 'message' => 'Some error happened while payment'];

      // Display a very generic error to the user, and maybe send
      // yourself an email
      } catch (\Exception $e) {

       return ['success' => 0, 'message' => 'Something else happened, completely unrelated to Stripe'];

      // Something else happened, completely unrelated to Stripe
      }
      return $charge->getLastResponse();
      //$this->createStripeInvoice($charge);
      }

     protected function createFailStripeCharge($invoice){
       try{
         \Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);
         $amount = $invoice->total_amount*100;
         $charge = \Stripe\Charge::create(array(
         "amount" => $amount,
         "currency" => $invoice->transaction_currency,
         "description" => "Charge for Failed Payment ".$invoice->userModel->email,
         "customer"    => $invoice->userModel->stripeToken,
         "metadata" => [
           "Advanced Payment" => "Null",
           "Payment-type" => "Failed Bill",
           "Extra Lead Charge"=> $invoice->extra_lead_consumed*$invoice->extra_per_lead_amount
           ]
         ));
       } catch(\Stripe\Error\Card $e) {

      $body = $e->getJsonBody();
      $err  = $body['error'];

       return [
           'success'=>0,
           'message'=> isset($err['message'])?$err['message']:'',
           'charge'=> isset($err['charge'])?$err['charge']:'',
           'decline_code'=> isset($err['decline_code'])?$err['decline_code']:'',
           'code'=> isset($err['code'])?$err['code']:'',
           'doc_url'=> isset($err['doc_url'])?$err['doc_url']:'',
           'type'=> isset($err['type'])?$err['type']:''
       ];

     } catch (\Stripe\Error\RateLimit $e) {
     // Too many requests made to the API too quickly
      return ['success' => 0, 'message' => 'Rate Limit Error'];

     } catch (\Stripe\Error\InvalidRequest $e) {
     // Invalid parameters were supplied to Stripe's API
      return ['success' => 0, 'message' => 'Invalid parameters were supplied to Stripe API'];

     } catch (\Stripe\Error\Authentication $e) {
     // Authentication with Stripe's API failed
      return ['success' => 0, 'message' => 'maybe you changed API keys recently'];

     // (maybe you changed API keys recently)
     } catch (\Stripe\Error\ApiConnection $e) {

      return ['success' => 0, 'message' => 'Network communication with Stripe failed'];

     // Network communication with Stripe failed
     } catch (\Stripe\Error\Base $e) {

     return ['success' => 0, 'message' => 'Some error happened while payment'];

     // Display a very generic error to the user, and maybe send
     // yourself an email
     } catch (\Exception $e) {

      return ['success' => 0, 'message' => 'Something else happened, completely unrelated to Stripe'];

     // Something else happened, completely unrelated to Stripe
     }
     return $charge->getLastResponse();
      //$this->createStripeInvoice($charge);
     }

                      ///Genetate Stripe Charge For Advance Payment////
      protected function createAdvancedPaymentStripeCharge($subscription,$user,$amount){
        try{
          \Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);

        $charge = \Stripe\Charge::create(array(
          "amount" => $amount,
          "currency" => $subscription->packageModel->currency,
          "description" => "Charge for Advanced Payment ".$user->email,
          "customer"    => $user->stripeToken,
          "metadata" => [
            "Advanced Payment" =>$subscription->packageModel->amount,
            "Payment-type" => "Billing",
            "Extra Lead Charge"=> "Null"
            ]
          ));
        }catch(\Stripe\Error\Card $e) {
        // Since it's a decline, \Stripe\Error\Card will be caught
       $body = $e->getJsonBody();
       $err  = $body['error'];

        return [
            'success'=>0,
            'message'=> isset($err['message'])?$err['message']:'',
            'charge'=> isset($err['charge'])?$err['charge']:'',
            'decline_code'=> isset($err['decline_code'])?$err['decline_code']:'',
            'code'=> isset($err['code'])?$err['code']:'',
            'doc_url'=> isset($err['doc_url'])?$err['doc_url']:'',
            'type'=> isset($err['type'])?$err['type']:''
        ];

      } catch (\Stripe\Error\RateLimit $e) {
      // Too many requests made to the API too quickly
       return ['success' => 0, 'message' => 'Rate Limit Error'];

      } catch (\Stripe\Error\InvalidRequest $e) {
      // Invalid parameters were supplied to Stripe's API
       return ['success' => 0, 'message' => 'Invalid parameters were supplied to Stripes API'];

      } catch (\Stripe\Error\Authentication $e) {
      // Authentication with Stripe's API failed
       return ['success' => 0, 'message' => 'maybe you changed API keys recently'];

      // (maybe you changed API keys recently)
      } catch (\Stripe\Error\ApiConnection $e) {

       return ['success' => 0, 'message' => 'Network communication with Stripe failed'];

      // Network communication with Stripe failed
      } catch (\Stripe\Error\Base $e) {

      return ['success' => 0, 'message' => 'Some error happened while payment'];

      // Display a very generic error to the user, and maybe send
      // yourself an email
      } catch (\Exception $e) {

       return ['success' => 0, 'message' => 'Something else happened, completely unrelated to Stripe'];

      // Something else happened, completely unrelated to Stripe
      }
      return $charge->getLastResponse();
       //$this->createStripeInvoice($charge);
    }

    protected function createAdvancedExtraPaymentStripeChargePendingInvoice($invoice,$user,$total_amount,$extra_amount,$advanced){
            try{
             \Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);
             $amount = $total_amount*100;
             $charge = \Stripe\Charge::create(array(
               "amount" => $amount,
               "currency" => $invoice->transaction_currency,
               "description" => "Charge for Advanced Payment ".$user->email,
               "customer"    => $user->stripeToken,
               "metadata" => [
               "Advanced Payment" => $advanced,
               "Payment-type" => "Billing",
               "Extra Lead Charge"=> $extra_amount,
                 ]
               ));
             }catch(\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];

            return [
                'success'=>0,
                'message'=> isset($err['message'])?$err['message']:'',
                'charge'=> isset($err['charge'])?$err['charge']:'',
                'decline_code'=> isset($err['decline_code'])?$err['decline_code']:'',
                'code'=> isset($err['code'])?$err['code']:'',
                'doc_url'=> isset($err['doc_url'])?$err['doc_url']:'',
                'type'=> isset($err['type'])?$err['type']:''
            ];

           } catch (\Stripe\Error\RateLimit $e) {
           // Too many requests made to the API too quickly
            return ['success' => 0, 'message' => 'Rate Limit Error'];

           } catch (\Stripe\Error\InvalidRequest $e) {
           // Invalid parameters were supplied to Stripe's API
            return ['success' => 0, 'message' => 'Invalid parameters were supplied to Stripe API'];

           } catch (\Stripe\Error\Authentication $e) {
           // Authentication with Stripe's API failed
            return ['success' => 0, 'message' => 'Maybe you changed API keys recently'];

           // (maybe you changed API keys recently)
           } catch (\Stripe\Error\ApiConnection $e) {

            return ['success' => 0, 'message' => 'Network communication with Stripe failed'];

           // Network communication with Stripe failed
           } catch (\Stripe\Error\Base $e) {

           return ['success' => 0, 'message' => 'Some error happened while payment'];

           // Display a very generic error to the user, and maybe send
           // yourself an email
           } catch (\Exception $e) {

            return ['success' => 0, 'message' => 'Something else happened, completely unrelated to Stripe'];

           // Something else happened, completely unrelated to Stripe
           }
           return $charge->getLastResponse();
    }
///////////////////////////////////////////////////////////////////////////////////////
////////////////////////Advanced+Extra////////////////////////////////////////////////
protected function createAdvancedExtraPaymentStripeCharge($subscription,$user,$total_amount,$extra_amount,$advanced){
        try{
         \Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);
         $amount = $total_amount*100;
         $charge = \Stripe\Charge::create(array(
           "amount" => $amount,
           "currency" => $subscription->packageModel->currency,
           "description" => "Charge for Advanced Payment ".$user->email,
           "customer"    => $user->stripeToken,
           "metadata" => [
             "Advanced Payment" => $advanced,
             "Payment-type" => "Billing",
             "Extra Lead Charge"=> $extra_amount,
             ]
           ));
         } catch(\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];

            return [
                'success'=>0,
                'message'=> isset($err['message'])?$err['message']:'',
                'charge'=> isset($err['charge'])?$err['charge']:'',
                'decline_code'=> isset($err['decline_code'])?$err['decline_code']:'',
                'code'=> isset($err['code'])?$err['code']:'',
                'doc_url'=> isset($err['doc_url'])?$err['doc_url']:'',
                'type'=> isset($err['type'])?$err['type']:''
            ];

       } catch (\Stripe\Error\RateLimit $e) {
       // Too many requests made to the API too quickly
        return ['success' => 0, 'message' => 'Rate Limit Error'];

       } catch (\Stripe\Error\InvalidRequest $e) {
       // Invalid parameters were supplied to Stripe's API
        return ['success' => 0, 'message' => 'Invalid parameters were supplied to Stripes API'];

       } catch (\Stripe\Error\Authentication $e) {
       // Authentication with Stripe's API failed
        return ['success' => 0, 'message' => 'maybe you changed API keys recently'];

       // (maybe you changed API keys recently)
       } catch (\Stripe\Error\ApiConnection $e) {

        return ['success' => 0, 'message' => 'Network communication with Stripe failed'];

       // Network communication with Stripe failed
       } catch (\Stripe\Error\Base $e) {

       return ['success' => 0, 'message' => 'Some error happened while payment'];

       // Display a very generic error to the user, and maybe send
       // yourself an email
       } catch (\Exception $e) {

        return ['success' => 0, 'message' => 'Something else happened, completely unrelated to Stripe'];

       // Something else happened, completely unrelated to Stripe
       }
       return $charge->getLastResponse();
}

   public function createStripeInvoice($charge){
        echo "<pre>"; print_r($charge->getLastResponse());exit;
   }




}
