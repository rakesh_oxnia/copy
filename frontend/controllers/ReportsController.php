<?php

namespace frontend\controllers;

use Yii;
use app\models\User2;
use common\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\PasswordResetRequestForm;
use common\models\LiveChat_API;
use yii\filters\AccessControl;
use common\models\LoginForm;

//require(\Yii::getAlias('@app') . '/../g_php/LiveChat_API.php');

/**
 * UserController implements the CRUD actions for User2 model.
 */
class ReportsController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
				'class'=> AccessControl::className(),				
				'rules'=>[
					[
						'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                          return Yii::$app->user->can('viewReport');                     
                        }  					
                    ]
				]
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
	
	public $layout = "dashboard";

    public function beforeAction($action)
    { 
        if(!parent::beforeAction($action)) {
            return false;
        }
        LoginForm::userAssitLogin();

        return true;
    }

    public function render($view, $params)
    {
      $params = LoginForm::userAssitLogout($params);
        
		if(Yii::$app->user->identity->role == User::ROLE_OUTLET) {  
			unset($params['website_users']);                        
		}   
        // before render
        return parent::render($view, $params);
    }	

    /**
     * Lists all User2 models.
     * @return mixed
     */
    public function actionIndex()
    {
        try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			$_GET["group"] = Yii::$app->user->identity->user_group;
			
			if(!isset($_GET["date_from"])){
				$_GET['date_from'] = date('Y-m-d');
				$_GET['date_to'] = date('Y-m-d');
				$_GET['group_by'] = 'hour';
			}
			
			$API = new LiveChat_API();
			$reports = $API->reports->get("chats",$_GET);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}

		return $this->render('index', [
            'reports' => $reports,
        ]);
    }

    /**
     * Displays a single User2 model.
     * @param integer $id
     * @return mixed
     */
	public function actionView($id)
    {
		
        return $this->render('view');
    }
	
	public function actionStats()
    {
    	// set website filter
        if(!empty(Yii::$app->request->post('website_filter'))) {
          Yii::$app->session->set('website_filter', Yii::$app->request->post('website_filter')); 
          Yii::$app->session->remove('outlet_filter');    
          $_GET["group"] = Yii::$app->session->get('website_filter');     
        } else {
        	$_GET["group"] = Yii::$app->user->identity->user_group;
        }

		$website_users = User::find()->where(['email' => Yii::$app->user->identity->email])->all();
        if(count($website_users) > 1 && !Yii::$app->session->has('website_filter'))
        {
          Yii::$app->session->set('website_filter', Yii::$app->user->identity->user_group); 
        }
        

		$this->layout = "kosmoouter";
		try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;			

			if(!isset($_GET['date_from'])){
				$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
				$_GET['date_to'] = date('Y-m-d');
				$_GET['group_by'] = 'day';
			}
			$API = new LiveChat_API();
			$data = $API->reports->get("chats/total_chats",$_GET);
			$engagement = $API->reports->get("chats/engagement",$_GET);
			
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}

		
		//echo '<pre>';print_r($engagement);echo '</pre>';die;
		//echo '<pre>';print_r($data);echo '</pre>';
        return $this->render('graph',[
			'data'=>$data,
			'engagement'=>$engagement,
        	'website_users' => $website_users,
		]);
    }
    
	
	public function actionSatisfy(){
        try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			$_GET["group"] = Yii::$app->user->identity->user_group;
			
			if(!isset($_GET["date_from"])){
				$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
				$_GET['date_to'] = date('Y-m-d');
				$_GET['group_by'] = 'day';
			}
			
			$_GET['group'] = Yii::$app->user->identity->user_group;
			
			$API = new LiveChat_API();
			$data = $API->reports->get("chats/ratings",$_GET);
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}
		//echo '<pre>';print_r($data);die;		
        return $this->render('satisfy',[
			'data'=>$data
		]);      
    }
	
	public function actionQueued(){
        try {
			$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
			$_GET["group"] = Yii::$app->user->identity->user_group;
			
			if(!isset($_GET["date_from"])){
				$_GET['date_from'] = date('Y-m-d',strtotime("-6 days"));
				$_GET['date_to'] = date('Y-m-d');
				$_GET['group_by'] = 'day';
			}
			
			$_GET['group'] = Yii::$app->user->identity->user_group;
			
			$API = new LiveChat_API();
			$data = $API->reports->get("chats/queued_visitors",$_GET);
			$waitings = $API->reports->get("chats/queued_visitors/waiting_times",$_GET);
			
		}
		catch (Exception $e) {
			throw new NotFoundHttpException($e->getMessage());
		}
		
		//echo '<pre>';print_r($waitings);echo '</pre>';
		
        return $this->render('queued',[
			'data'=>$data,
			'waitings'=>$waitings,
		]);      
    }
	
}
