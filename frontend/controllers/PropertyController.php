<?php

namespace frontend\controllers;

use Yii;
use app\models\Property;
use app\models\Room;
use app\models\PropertySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PropertyController implements the CRUD actions for Property model.
 */
class PropertyController extends Controller
{
    public function behaviors()
    {
        return [
			'access' => [
				'class'=> AccessControl::className(),
				'only' => ['index','create', 'update','delete','finish','addanother'],
				'rules'=>[
					[
						'actions' => ['create','finish','addanother'],
						'allow' => true,
						'roles'=>['@'],
					]
				]
			],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Property models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PropertySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Property model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Property model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Property();
		$model2 = new Room();
		$propertyall = Property::find()->where(['user_id' => Yii::$app->user->identity->id])->orderBy('id')->all();

        if ($model->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post()) && $model->validate() && $model2->validate()) {
			
			$model->user_id = yii::$app->user->identity->id;
			$model2->attributes = $_POST['Room'];
			
			$model->save();
			
			$username = yii::$app->user->identity->username;
			$p_id = sprintf("%05d", $model->attributes['id']);
			$path = Yii::getAlias('@webroot');
			
			$fullpath = $path.'/allusers/'.$username.'/'.$p_id;
			$temp1 = $path.'/allusers/'.$username.'/temp1';
			$temp2 = $path.'/allusers/'.$username.'/temp2';

			mkdir($fullpath);

			if (!is_dir($temp1)) {
				mkdir($temp1);
			}

			if (!is_dir($temp2)) {
				mkdir($temp2);
			}

			$model2->property_id = $p_id;
			$model2->save();
			
            return $this->redirect(['room/create', 'id' => $model->id]);
        }
		elseif($model->load(Yii::$app->request->get())){
			return $this->redirect(['room/another', 'id' => $model->house_no]);
		}
		else {
            return $this->render('create', [
                'model' => $model,
                'model2' => $model2,
                'propertyall' => $propertyall,
            ]);
        }
    }

    /**
     * Updates an existing Property model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
	
	/**
     * Add Another Room an existing Property model.
     * 
     * @return mixed
     */
    public function actionAddanother()
    {
		$model = new Property();
        $model2 = Property::find()->where(['user_id' => Yii::$app->user->identity->id])->orderBy('id')->all();

        if ($model->load(Yii::$app->request->post())) {
			$modelres = Property::find()->where(['house_no'=>$model->house_no,'postcode'=>$model->postcode])->one();
			
			if(!$modelres){
				\Yii::$app->getSession()->setFlash('error', 'Property Not Found');
				return $this->render('addanother', ['model' => $model,'model2' => $model2,]);
			}
			
            return $this->redirect(['room/another', 'id' => $modelres->id]);
        } else {
            return $this->render('addanother', [
                'model' => $model,
                'model2' => $model2,
            ]);
        }
    }

    /**
     * Deletes an existing Property model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Property model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Property the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Property::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionFinish($id=false)
    {
        if($id){
			$model2 = Room::findOne(['id' => $id]);	
			
			$pid = $model2->property_id;
			$model = $this->findModel($pid);
			
			return $this->render('finish',['model'=>$model,'model2'=>$model2]);
		}
		
		return $this->goHome();
    }
}
