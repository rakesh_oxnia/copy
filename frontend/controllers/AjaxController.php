<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\User;
use app\models\User2;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Packages;
use frontend\models\Invoices;
use app\models\Leads;
use common\models\LiveChat_API;
use app\models\Subscription;
use common\models\AdminSetting;
use common\models\StripeLog;

/**
 * Site controller
 */
class AjaxController extends Controller
{
	public  $layout;
	public $enableCsrfValidation = false;

   public function actionPayPerLeadBySavedCard(){
		    $customer_stripe_token = Yii::$app->user->identity->stripeToken;
	        $chat_id = Yii::$app->request->post('chat_id');
			//$single_charge = Yii::$app->user->identity->per_lead_cost;
			if(Yii::$app->user->identity->role == User::ROLE_OUTLET){
				$franchiseuser = User::findOne(['id'=>Yii::$app->user->identity->parent_id]);
				$per_lead_cost = $franchiseuser->per_lead_cost;
				$Currency = $franchiseuser->per_lead_cost_currency;
			}else{
				$per_lead_cost = Yii::$app->user->identity->per_lead_cost;
				$Currency = Yii::$app->user->identity->per_lead_cost_currency;
			}
			if(empty($per_lead_cost)){
				$adminSetting	= AdminSetting::findOne(1);
				$single_charge = $adminSetting->global_per_lead_cost;
				$transactionCurrency = $adminSetting->global_per_lead_cost_currency;
			}else{
				$single_charge = $per_lead_cost;
				$transactionCurrency = $Currency;
			}
			if($customer_stripe_token==''){
			  return json_encode(['success'=>'setting','data'=>'Please Register Your Card in Settings "Update New Credit Card Details" section']);
			}

			if($single_charge==''){
			  return json_encode(['success'=>false,'data'=>'Please Ask Your Admin to Set Per Lead Charge']);
			}

            $allowed_minimum_charge = User::validMinimumStripeCharge($single_charge, $transactionCurrency);
            if(!$allowed_minimum_charge) {
               return json_encode(['success'=>false,'data'=>'Stripe does not allow such a small amount transaction']);
               exit;
            }
            
            if(!empty(Yii::$app->user->identity->tax)) {
                $tax = $single_charge * Yii::$app->user->identity->tax / 100;
                $single_charge = $single_charge + $tax;
            } 
            
			$charge = $this->MakeAStripeCharge($single_charge,$customer_stripe_token,$chat_id,$transactionCurrency);
			$bought_chat_ids = array();
			if(isset($charge->code)){
							$model = new Invoices;
							$model->user_id = Yii::$app->user->id;
							$model->single_amount = $single_charge;
							$model->amount = $single_charge;
							$model->currency = $transactionCurrency;
							$model->transaction_id =$charge->json['balance_transaction'];
							$model->invoice_id = $charge->json['id'];
							$model->package_id = 0;
							$model->chat_id = $chat_id;
							$model->status = 1;
							$model->payment_date = date('Y-m-d H:i:s');
							//Send Mail payment Success//
	 					 if($model->save(false)){
							 $bought_chat_ids[] = $chat_id;
	 						 $sendMail = Subscription::sendPayAsYouGoSuccessMail($model->amount,$charge->json['created'],Yii::$app->user->identity->email,$transactionCurrency,$bought_chat_ids,$model->id);
	 					 }
	 					 //Send Mail Success//
						 //Send Email For New Leads//

						 //Send Email For New Leads//
						 return json_encode(['success'=>true,'data'=>'Payment Charge Successfully']);
			}else{
				return $charge;
			}
	 }

	 public function actionPayMultipleLeads(){
		 $customer_stripe_token = Yii::$app->user->identity->stripeToken;
		 $chat_ids = Yii::$app->request->post('chat_ids');
		 //print_r($chat_ids);exit;
		 $leadCount = Yii::$app->request->post('leadCount');
		 //$single_charge = Yii::$app->user->identity->per_lead_cost;
		 if(Yii::$app->user->identity->role == User::ROLE_OUTLET){
				$franchiseuser = User::findOne(['id'=>Yii::$app->user->identity->parent_id]);
				$per_lead_cost = $franchiseuser->per_lead_cost;
			$Currency = $franchiseuser->per_lead_cost_currency;
			}else{
			$per_lead_cost = Yii::$app->user->identity->per_lead_cost;
			$Currency = Yii::$app->user->identity->per_lead_cost_currency;
			}

			if(empty($per_lead_cost)){
				$adminSetting	= AdminSetting::findOne(1);
				$single_charge = $adminSetting->global_per_lead_cost;
				$transactionCurrency = $adminSetting->global_per_lead_cost_currency;
			}else{
				$single_charge = $per_lead_cost;
				$transactionCurrency = $Currency;
			}

		 $totalcharge = $single_charge*$leadCount;
		 if($customer_stripe_token==''){
			 return json_encode(['success'=>'setting','data'=>'Please Register Your Card in Settings "Update New Credit Card Details" section']);
		 }
		 if($single_charge==''){
			 return json_encode(['success'=>false,'data'=>'Please Ask Your Admin to Set Per Lead Charge']);
		 }

         $allowed_minimum_charge = User::validMinimumStripeCharge($totalcharge, $transactionCurrency);
         if(!$allowed_minimum_charge) {
             return json_encode(['success'=>false,'data'=>'Stripe does not allow such a small amount transaction']);
             exit;
         }
         
        if(!empty(Yii::$app->user->identity->tax)) {
            $tax = $totalcharge * Yii::$app->user->identity->tax / 100;
            $totalcharge = $totalcharge + $tax;
        }

		 $bought_chat_ids = array();
		 $charge = $this->MakeAStripeCharge($totalcharge,$customer_stripe_token,$chat_ids,$transactionCurrency);
      if(isset($charge->code)){
				$all_chat_ids = implode(',',$chat_ids);
				//foreach($chat_ids as $chat_id){
									$model = new Invoices;
									$model->user_id = Yii::$app->user->id;
									$model->single_amount = $single_charge;
									$model->amount = $totalcharge;//$single_charge;
									$model->currency = $transactionCurrency;
									$model->transaction_id =$charge->json['balance_transaction'];
									$model->invoice_id = $charge->json['id'];
									$model->package_id = 0;
									$model->chat_id = $all_chat_ids;
									$model->status = 1;
									$model->payment_date = date('Y-m-d H:i:s');
									$model->save(false);

			 // foreach($chat_ids as $chat_id){
				// 					$bought_chat_ids[] = $chat_id;
			 // }
			 //Send Mail payment Success//
				$sendMail = Subscription::sendPayAsYouGoSuccessMail($totalcharge,$charge->json['created'],Yii::$app->user->identity->email,$transactionCurrency,$chat_ids,$model->id);
			//Send Mail Success//
			 return json_encode(['success'=>true,'data'=>'Payment Charge Successfully']);
			}else{
            return $charge;
			}
	 }

	 protected function SaveStripeLog($user_id,$amount,$currency,$status,$stripe_token,$log)
	 {
		 try {
			 $log = json_encode($log);

			 $stripe_log = new StripeLog();
			 $stripe_log->user_id = $user_id;
			 $stripe_log->amount = $amount/100;//amount is getting passed in cents, paise etc
			 $stripe_log->currency = $currency;
			 $stripe_log->status = $status;
			 $stripe_log->stripe_token = $stripe_token;
			 $stripe_log->log = $log;
			 $stripe_log->error_type = 'pay_as_you_go';//error occured while making pay as you go payment
			 $stripe_log->created_at = time();
			 $stripe_log->save(false);
		 }catch(Exception $e) {

		 }
	 }

	 protected function MakeAStripeCharge($single_charge,$customer_stripe_token,$chat_id,$transactionCurrency){
		 							$user_id = Yii::$app->user->id;
                  try{
                    \Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);
										$amount = $single_charge*100;
                    $charge = \Stripe\Charge::create(array(
                           "amount" => $amount,
                           "currency" => $transactionCurrency,
                           "description" => "Charge for pay as you go per lead to ".Yii::$app->user->identity->email,
                           "customer"    => $customer_stripe_token,
                           "metadata" => [
                           "Payment-type" => "Pay As You Go",
                           "Per Lead Charge" => $single_charge,
													 "chat-id" =>json_encode($chat_id)
                        ]
                      ));
                  } catch(\Stripe\Error\Card $e) {
                  // Since it's a decline, \Stripe\Error\Card will be caught
                  $body = $e->getJsonBody();
                  $err  = $body['error'];

									$this->SaveStripeLog($user_id,$amount,$transactionCurrency,0,$customer_stripe_token,$err);

                 return json_encode(['success'=>false,'data'=>$err['message']]);
                } catch (\Stripe\Error\RateLimit $e) {
                  // Too many requests made to the API too quickly
									$this->SaveStripeLog($user_id,$amount,$transactionCurrency,0,$customer_stripe_token,$e);

                 return json_encode(['success'=>false,'data'=>'Headers are missing']);
                } catch (\Stripe\Error\InvalidRequest $e) {
                  // Invalid parameters were supplied to Stripe's API
										$this->SaveStripeLog($user_id,$amount,$transactionCurrency,0,$customer_stripe_token,$e);

                   return json_encode(['success'=>false,'data'=>'Invalid parameters were supplied to Stripe\'s API']);
                } catch (\Stripe\Error\Authentication $e) {
                  // Authentication with Stripe's API failed
									$this->SaveStripeLog($user_id,$amount,$transactionCurrency,0,$customer_stripe_token,$e);

                 return json_encode(['success'=>false,'data'=>'maybe you changed API keys recently']);
                  // (maybe you changed API keys recently)
                } catch (\Stripe\Error\ApiConnection $e) {
									$this->SaveStripeLog($user_id,$amount,$transactionCurrency,0,$customer_stripe_token,$e);

                 return json_encode(['success'=>false,'data'=>'Network communication with Stripe faile']);
                  // Network communication with Stripe failed
                } catch (\Stripe\Error\Base $e) {
									$this->SaveStripeLog($user_id,$amount,$transactionCurrency,0,$customer_stripe_token,$e);

                 return json_encode(['success'=>false,'data'=>'Some error happened while payment']);
                  // Display a very generic error to the user, and maybe send
                  // yourself an email
                } catch (Exception $e) {
									$this->SaveStripeLog($user_id,$amount,$transactionCurrency,0,$customer_stripe_token,$e);

                  return json_encode(['success'=>false,'data'=>'Something else happened, completely unrelated to Stripe']);
                  // Something else happened, completely unrelated to Stripe
                }
                return $charge->getLastResponse();
       }

			 public function actionCalculatePayMultipleLeads(){
				 $customer_stripe_token = Yii::$app->user->identity->stripeToken;
				 $chat_ids = Yii::$app->request->post('chat_ids');
				 //print_r($chat_ids);exit;
				 $leadCount = Yii::$app->request->post('leadCount');
				 //$single_charge = Yii::$app->user->identity->per_lead_cost;
				 if(Yii::$app->user->identity->role == User::ROLE_OUTLET){
						$franchiseuser = User::findOne(['id'=>Yii::$app->user->identity->parent_id]);
						$per_lead_cost = $franchiseuser->per_lead_cost;
						$Currency = $franchiseuser->per_lead_cost_currency;
					}else{
						$per_lead_cost = Yii::$app->user->identity->per_lead_cost;
						$Currency = Yii::$app->user->identity->per_lead_cost_currency;
					}

					if(empty($per_lead_cost)){
						$adminSetting	= AdminSetting::findOne(1);
						$single_charge = $adminSetting->global_per_lead_cost;
						$transactionCurrency = $adminSetting->global_per_lead_cost_currency;
					}else{
						$single_charge = $per_lead_cost;
						$transactionCurrency = $Currency;
					}

				 $totalcharge = $single_charge*$leadCount;
				 if($single_charge==''){
					 return json_encode(['success'=>false,'message'=>'Please Ask Your Admin to Set Per Lead Charge']);
				 }else {
					 return json_encode(['success'=>true,'currency'=>$transactionCurrency,'total_charge'=>$totalcharge]);
				 }
			 }


	public function actionSendmail($id, $showLead, $name, $email){

		$sendmail = new Leads();
	    $API = new LiveChat_API();
		$page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
		$chat = $API->chats->getSingleChat($id);

			$users = User2::find()->where(['user_group'=>$chat->group[0]])->all();


			foreach($users as $user){

				$leads = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();

				try {
					$API = new LiveChat_API();
					$chat2 = $API->chats->getSingleChat($chat->id);
				}
				catch (Exception $e) {
					throw new NotFoundHttpException($e->getMessage());
				}

				if($leads){
					//echo '<pre>';print_r($leads);die;
					$leads23 = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
					if($leads23){
						$sent = $sendmail->sendEmail($user,$chat,$chat2, $showLead, $name, $email);
						if($sent){
							$model = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
							$model->email = 1;
							$model->save();
						}
					}
				}else{
					$lead = new Leads();
					$lead->user_id = $user['id'];
					$lead->chat_id = $chat->id;
					$lead->c_time = $chat->started_timestamp;
					$lead->status = 'pending';
					$lead->user_group = Yii::$app->user->identity->user_group;
					$lead->email = 0;
					if($lead->save()){
						$sent = $sendmail->sendEmail($user,$chat,$chat2, $showLead, $name, $email);
						if($sent){
							$model = Leads::find()->where(['user_id'=>$user['id'],'chat_id'=>$chat->id])->one();
							$model->email = 1;
							$model->save();
						}
					}
				}
			}
	}

	// public function actionPayMultipleLeads(){
	// 	$post = Yii::$app->request->post('chat_ids');
	//
	//
	// 	require_once("../../vendor/stripe/stripe-php/init.php");
	// 	\Stripe\Stripe::setApiKey(Yii::$app->params['stripeKey']);
	//
	// 	$customer_id = Yii::$app->user->identity->stripeToken;
	//
	// 	foreach ($post as $key => $value) {
	// 		$charge = \Stripe\Charge::create(array(
	// 		  "amount" => 50,
	// 		  "currency" => "usd",
	// 		  "description" => "Pay Per Lead",
	// 		  //"source" => $token,
	// 		  "customer" => $customer_id
	// 		));
	//
	// 		$invoice = \Stripe\Invoiceitem::create(array(
	// 		  "customer" => $customer_id,
	// 		  "amount" => 50,
	// 		  "currency" => "usd",
	// 		  "description" => "Pay Per Lead",
	// 		));
	//
	// 		$model = new Invoices;
	// 		$model->user_id = Yii::$app->user->id;
	// 		$model->amount = $charge->amount;
	// 		$model->transaction_id = $charge->balance_transaction;
	// 		$model->invoice_id = $invoice->id;
	// 		$model->package_id = 0;
	// 		$model->chat_id = $value;
	// 		$model->status = 1;
	// 		$model->payment_date = date('Y-m-d H:i:s');
	// 		$model->save(false);
	// 	}
	// 	return json_encode(['success' => true]);
	// }

}
