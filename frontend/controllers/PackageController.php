<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use common\models\User;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Packages;
use common\components\paypal;
use app\models\Coupons;
use frontend\models\Invoices;
use common\models\CustomPackageRange;
use common\models\PackageUser;
/**
 * Package controller
 */
class PackageController extends Controller
{

	public function actionIndex(){
		$this->layout = 'kosmoouter';
		$error = "";
		$success = "";

		if( isset( $_POST['apply'] ) ){
			$coupon = $_POST['coupon'];
			$couponData	= Coupons::find()->where(['code' => $coupon])->one();
			if( !empty( $couponData ) ){
				if($couponData['status'] == '1'){
					$discount = $couponData['discount'];
					Yii::$app->session['discount'] = $discount;
					$success = "Coupon has been applied successfully.";
				}else{
					$error = "Sorry!! This Coupon has been expired now.";
				}

			}else{
					$error = "Sorry!! There is no such coupon.";
			}
		}

		$packages	 = Packages::find()->orderBy(['id' => SORT_DESC])->all();
		$invoiceData = Invoices::find()->where(['user_id' => Yii::$app->user->id, ])->andWhere(['<>','package_id', '0'])->orderBy(['id' => SORT_DESC])->one();
		$customPackageLeads = 0;
		//echo "<pre>"; print_r($invoiceData); exit;
		$packageName = "Pay as you go";
		if( !empty( $invoiceData ) ){
			$packageData	 = Packages::findOne( $invoiceData['package_id'] );
			if($packageData){
				$packageName 	 = $packageData['package_name'];
				if($packageName == 'Custom'){
					$packageData = PackageUser::findOne(['user_id' => Yii::$app->user->identity->id]);
					if($packageData){
						$customPackageLeads = $packageData->number_of_leads;
					}
				}
			}else{
					$packageName = "Pay as you go";
			}

		}
		return $this->render('index', ['packages' => $packages, 'error' => $error, 'success' => $success, 'packageName' => $packageName, 'customPackageLeads' => $customPackageLeads]);
	}

	public function actionPaynow( $id ){

		$packageData = Packages::findOne( $id );

		$discount 		 = Yii::$app->session['discount'];
		$discount_amount = ($packageData['amount'] * $discount) / 100;
		$amount   = $packageData['amount'] - $discount_amount;

		$p = new paypal();
        $response = $p->teszt($amount, "Leads : " . $packageData['number_of_leads'] . " Chats : " . $packageData['number_of_chats']);
		$response->links[1]->href;
		$this->redirect($response->links[1]->href);

	}

	public function actionPayasyougo(){
		Yii::$app->session->setFlash('success', "Great! You have successfully subscribed to Pay as you go Plan.");
		return $this->redirect(['/package/index']);
	}

	public function actionCalculatePackage()
	{
		$number_of_leads = Yii::$app->request->post('number_of_leads');
		$customPackageRange = CustomPackageRange::find()->all();
		$calculated_price = 1000;
		$rangeId = false;
		foreach ($customPackageRange as $custom) {
			if($number_of_leads == $custom->range_start || $number_of_leads == $custom->range_end || ($number_of_leads > $custom->range_start && $number_of_leads < $custom->range_end)){
				$calculated_price = $custom->amount;
				$number_of_leads = $custom->range_end;
				$rangeId = $custom->id;
			}
		}
		return json_encode(['success' => true, 'amount' => $calculated_price, 'number_of_leads' => $number_of_leads, 'number_of_chats' => $number_of_leads, 'range_id' => $rangeId]);
	}

}
