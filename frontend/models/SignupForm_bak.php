<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $website_url;
    public $parent_id;
    public $agency_name;
    public $theme_color;
    public $agency_logo
    //public $role;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['website_url', 'required'],
            [['parent_id', 'agency_name', 'agency_logo', 'theme_color', 'role'], 'safe'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['website_url', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->website_url = $this->website_url;
            $user->parent_id = $this->parent_id;
            $user->agency_name = $this->agency_name;
            $user->theme_color = $this->theme_color;
            $user->agency_logo = $this->agency_logo;
            $user->role = $this->role;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
