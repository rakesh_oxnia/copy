<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coupons".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $discount
 * @property integer $status
 */
class Coupons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupons';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'discount', 'status'], 'required'],
            [['discount', 'status'], 'integer'],
			[['name', 'discount'], 'unique'],
            [['name', 'code'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'discount' => 'Discount',
            'status' => 'Status',
        ];
    }
}
