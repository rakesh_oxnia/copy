<?php

namespace frontend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "invoices".
 *
 * @property integer $id
 * @property integer $package_id
 * @property integer $invoice_id
 * @property double $amount
 * @property string $transaction_id
 * @property integer $status
 */
class Invoices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_id', 'invoice_id', 'amount', 'transaction_id', 'status'], 'required'],
            [['package_id', 'invoice_id', 'status'], 'integer'],
            [['amount'], 'string'],
            [['transaction_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'package_id' => 'Package ID',
            'invoice_id' => 'Invoice ID',
            'amount' => 'Amount',
            'transaction_id' => 'Transaction ID',
            'status' => 'Status',
        ];
    }

    public function getUserModel()
    {
      return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
