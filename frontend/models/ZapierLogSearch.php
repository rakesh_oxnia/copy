<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ZapierLogs;

/**
 * EmailLogSearch represents the model behind the search form about `common\models\EmailLog`.
 */
class ZapierLogSearch extends ZapierLogs
{
    public function rules()
    {
        return [
            [['id', 'lead_id','updated_at'], 'integer'],
            [['lead_id', 'chat_id', 'email', 'type', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ZapierLogs::find()->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if($this->created_at != ''){
          $start_time = strtotime($this->created_at . '00:00:00');
          $end_time = strtotime($this->created_at . '23:59:59');
          $query->andFilterWhere(['>=', 'created_at', $start_time])->andFilterWhere(['<=', 'created_at', $end_time]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'lead_id' => $this->lead_id,
            'chat_id' => $this->chat_id,
            'email' => $this->email,
            'type' => $this->type
        ]);

        $query->andFilterWhere(['like', 'chat_id', $this->chat_id])
            ->andFilterWhere(['like', 'lead_id', $this->lead_id])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'type', $this->type]);

        //echo $query->createCommand()->getRawSql();exit;
        return $dataProvider;

    }
}
