<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\EmailLog;

/**
 * EmailLogSearch represents the model behind the search form about `common\models\EmailLog`.
 */
class EmailLogSearch extends EmailLog
{
    public function rules()
    {
        return [
            [['id', 'leads_id', 'sent_via', 'updated_at'], 'integer'],
            [['chat_id', 'sent_to', 'web_url', 'created_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = EmailLog::find()->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if($this->created_at != ''){
          $start_time = strtotime($this->created_at . '00:00:00');
          $end_time = strtotime($this->created_at . '23:59:59');
          $query->andFilterWhere(['>=', 'created_at', $start_time])->andFilterWhere(['<=', 'created_at', $end_time]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'leads_id' => $this->leads_id,
            'sent_via' => $this->sent_via,
            //'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'chat_id', $this->chat_id])
            ->andFilterWhere(['like', 'web_url', $this->web_url])
            ->andFilterWhere(['like', 'sent_to', $this->sent_to]);

        return $dataProvider;
    }
}
