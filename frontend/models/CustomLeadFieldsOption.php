<?php

namespace app\models;

use Yii;
use common\models\Packages;
use common\models\User;
/**
 * This is the model class for table "apps_countries".
 *
 * @property integer $id
 * @property string $country_code
 * @property string $country_name
 */
class CustomLeadFieldsOption extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'custom_leads_fields_option';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }

}
