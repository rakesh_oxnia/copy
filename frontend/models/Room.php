<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "room".
 *
 * @property integer $id
 * @property integer $property_id
 * @property string $room_name
 * @property string $image
 */
class Room extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'room';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'room_name'], 'safe'],
            [['property_id'], 'integer'],
            //[['image'], 'safe'],
            [['room_name'], 'string', 'max' => 200],
            [['image','image2'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxFiles' => 12],
			//['image', 'in', 'range' => [12]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'room_name' => 'Room Name',
            'image' => 'Upload your first set of 12 images',
            'image2' => 'Upload your Second set of 12 images',
        ];
    }
	
	public function upload()
    {
		$path = Yii::getAlias('@webroot');
		$fullpath = $path.'/allusers/'.yii::$app->user->identity->username;
		$files = glob($fullpath.'/temp1/*'); // get all file names
		$files2 = glob($fullpath.'/temp2/*'); // get all file names
		
		$result1 = $fullpath.'/result1';
		$result2 = $fullpath.'/result2';
		
		if (!is_dir($result1)) {
			mkdir($result1);
		}
		if (!is_dir($result2)) {
			mkdir($result2);
		}
		
		foreach($files as $file){ // iterate files
		  if(is_file($file))
			unlink($file); // delete file
		}
		foreach($files2 as $file){ // iterate files
		  if(is_file($file))
			unlink($file); // delete file
		}
		
        foreach ($this->image as $file) {
                $file->saveAs($fullpath.'/temp1/'.$file->baseName.'.'.$file->extension);
        }
		foreach ($this->image2 as $file) {
                $file->saveAs($fullpath.'/temp2/'.$file->baseName.'.'.$file->extension);
        }
		
		return true;
    }
	
}
