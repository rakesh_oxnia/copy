<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use yii\helpers\Url;
/**
 * Password reset request form
 */
class ServiceAccount extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
        ];
    }

}
