<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "chatstats".
 *
 * @property integer $id
 * @property string $business_name
 * @property integer $group
 * @property integer $invites
 * @property integer $chats
 * @property integer $leads
 * @property integer $greeting_chats
 * @property integer $visitor_chats
 * @property string $record_date
 */
class Chatstat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chatstats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group', 'record_date'], 'required'],
            [['group', 'invites', 'chats', 'leads', 'greeting_chats', 'visitor_chats'], 'integer'],
            [['record_date'], 'safe'],
            [['business_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business_name' => 'Business Name',
            'group' => 'Group',
            'invites' => 'Invites',
            'chats' => 'Chats',
            'leads' => 'Leads',
            'greeting_chats' => 'Greeting Chats',
            'visitor_chats' => 'Visitor Chats',
            'record_date' => 'Record Date',
        ];
    }
}
