<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use yii\helpers\Url;
/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with such email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail($type=false)
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if ($user->save(false)) {
            
               /* $flag = 0;
                $agency_name = \Yii::$app->user->identity->agency_name;
                $agency_logo = \Yii::$app->user->identity->agency_logo;
                $email = \Yii::$app->user->identity->email;
                $userrole = \Yii::$app->user->identity->role;
                $homeurl = \Yii::$app->homeUrl;
                if($userrole == User::ROLE_AGENCY)
                {
                    $flag = 1;
                    if($agency_logo != '')
                    {
                       $base64img = 'http://fubaco.com/frontend/web/logo_images/'.$agency_logo;
                    }
                    else
                    {
                        $base64img = 'http://oxnia.com/signatures/agencyleads-logo.png';
                    }

                    \Yii::$app->mailer->compose(['html' => 'agencyclient-html', 'text' => 'agencyclient-text'], ['agency_name' => $agency_name,'base64img' => $base64img,'email' => $email, 'homeurl' => $homeurl])
                        ->setFrom([$email => $agency_name. ' ']) // mail from
                        ->setTo($user->email)
                        ->setSubject($agency_name.'::Welcome to our Dashboard') // mail subject
                        ->send();
                }*/
                /*else
                {
                    $row = (new \yii\db\Query())
                    ->from('user')
                    ->where(['id' => $user->id])
                    ->one();
                    if($row['parent_id'] > 0)
                    {
                        $flag = 1;
                        $row2 = (new \yii\db\Query())
                            ->from('user')
                            ->where(['id' => $row['parent_id']])
                            ->one();

                        $email = $row2['email'];
                        $agency_name = $row2['agency_name'];
                        $agency_logo = $row2['agency_logo'];

                        if($agency_logo != '')
                        {
                           $base64img = 'http://fubaco.com/frontend/web/logo_images/'.$agency_logo;
                        }
                        else
                        {
                            $base64img = 'http://oxnia.com/signatures/agencyleads-logo.png';
                        }
                    }
                }*/


                /*if($flag ==0)
                {
                    $email = \Yii::$app->params['supportEmail'];
                    $agency_name = \Yii::$app->name;

                    $email => $agency_name.
                }*/

                //$base64img = 'http://oxnia.com/signatures/agencyleads-logo.png';
                $baseurl = Url::base();
                $base64img = 'https://'.$_SERVER['SERVER_NAME'].$baseurl.'/images/cm-logo-new.png';
                $flag = 0;

                $userrole = \Yii::$app->user->identity->role;
                $homeurl = \Yii::$app->homeUrl;
                if($userrole == User::ROLE_AGENCY)
                {
                    $flag = 1;
                }
                else
                {
                    $row = (new \yii\db\Query())
                    ->from('user')
                    ->where(['id' => $user->id])
                    ->one();
                    if($row['parent_id'] > 0)
                    {
                        $flag = 1;
                    }
                }


                if($flag ==1)
                {
                    if($type){
                        return \Yii::$app->mailer->compose(['html' => 'passwordResetToken2-html', 'text' => 'passwordResetToken2-text'], ['user' => $user,"type" => $type,'base64img' => $base64img,'password' =>$this->password])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name .' '])
                            ->setTo($this->email)
                            ->setSubject('Welcome to our Dashboard') // user create mail subject
                            ->send();
                    }else{
                        return \Yii::$app->mailer->compose(['html' => 'passwordResetToken2-html', 'text' => 'passwordResetToken2-text'], ['user' => $user,"type" => $type,'base64img' => $base64img,'password' =>$this->password])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name .' '])
                            ->setTo($this->email)
                            ->setSubject('Reset password request for your dashboard') // password reset mail subject
                            ->send();
                    }
                }
                else
                {
                    if($type){
                        return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user,"type" => $type,'base64img' => $base64img,'password' =>$this->password])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name .' '])
                            ->setTo($this->email)
                            ->setSubject('Welcome to our Dashboard') // user create mail subject
                            ->send();
                    }else{
                        return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user,"type" => $type,'base64img' => $base64img,'password' =>$this->password])
                            ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name .' '])
                            ->setTo($this->email)
                            ->setSubject('Reset password request for your dashboard') // password reset mail subject
                            ->send();
                    }
                }
            }
        }

        return false;
    }
}
