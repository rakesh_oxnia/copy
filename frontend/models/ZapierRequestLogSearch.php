<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ZapierRequestLogs;

/**
 * EmailLogSearch represents the model behind the search form about `common\models\EmailLog`.
 */
class ZapierRequestLogSearch extends ZapierRequestLogs
{
    public function rules()
    {
        return [
            [['id','updated_at'], 'integer'],
            [['type', 'api_key', 'username', 'user_group', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ZapierRequestLogs::find()->orderBy(['created_at' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if($this->created_at != ''){
          $start_time = strtotime($this->created_at . '00:00:00');
          $end_time = strtotime($this->created_at . '23:59:59');
          $query->andFilterWhere(['>=', 'created_at', $start_time])->andFilterWhere(['<=', 'created_at', $end_time]);
        }

        // $query->andFilterWhere([
        //     'id' => $this->id,
        //     'api_key' => $this->api_key,
        //     'username' => $this->username,
        //     'user_group' => $this->user_group,
        //     'type' => $this->type
        // ]);

        $query->andFilterWhere(['like', 'api_key', $this->api_key])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'user_group', $this->user_group])
            ->andFilterWhere(['like', 'type', $this->type]);

        //echo $query->createCommand()->getRawSql();exit;
        return $dataProvider;

    }
}
