<?php

namespace app\models;

use Yii;
use common\models\User;
use common\models\Packages;
use frontend\models\Invoices;
/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $role
 * @property string $website_url
 * @property integer $created_at
 * @property integer $updated_at
 */
class User2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

	  public $password;
    public $email2;
    public $agents;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'website_url', 'name', 'business_name'], 'required'],
      			//['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
      		['per_lead_cost','number', 'min' => 1,'max' => 99999999],
            [['parent_id', 'agency_name', 'agency_logo', 'theme_color', 'send_transcript', 'free_access','disable_payment_status','show_past_lead','show_lead_from','show_lead_to'], 'safe'],
      			['username', 'string', 'max' => 255],
            [['password_hash', 'password_reset_token', 'email', 'website_url', 'name'], 'string', 'max' => 255],
      			['email', 'email'],
            [['average_lead_value', 'average_lead_cost'], 'number'],
            [['show_past_lead', 'show_lead_from', 'show_lead_to'], 'number'],
      			['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
      			['website_url','url', 'defaultScheme' => 'http', 'message' => 'Website url is not valid (example: http://www.chat-application.com)'],
			//[['website_url'],'unique'],
            [['status', 'role', 'user_group', 'created_at', 'updated_at', 'free_access'], 'integer'],
			//['user_group', 'unique'],
            [['auth_key'], 'string', 'max' => 32],
            ['per_lead_cost','required',  'when' => function ($model) {
              return $model->per_lead_cost_currency != '';
             },'whenClient' => "function (attribute, value) {
    return $('#user2-per_lead_cost_currency').val() != '';
              }"],
          ['per_lead_cost_currency','required',  'when' => function ($model) {
            return $model->per_lead_cost != '';
           },'whenClient' => "function (attribute, value) {
    return $('#user2-per_lead_cost').val() != '';
          }"],
            /*['show_lead_from','required',  'when' => function ($model) {
                return $model->show_past_lead === true;
            },'whenClient' => "function (attribute, value) {
    return $('#user2-show_past_lead').val() == 1;
          }"],*/
          //  [['package'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'API Key',
            'parent_id' => 'Parent ID',
            'agency_name' => 'Agency',
            'agency_logo' => 'LOGO image dimenstions : 283x60 pixels',
            'theme_color' => 'Theme Color',
            'average_lead_value' => 'Average Lead Price($)',
            'average_lead_cost' => 'Average Lead Cost($)',
            'send_transcript' => 'Send transcript in leads',
            'disable_payment_status' => 'Disable On Payment Failure',
            'email2' => 'Other Email',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email / Username',
            'status' => 'Status',
            'role' => 'Role',
            'website_url' => 'Website Url',
            'created_at' => 'Created',
            'updated_at' => 'Updated',
        ];
    }

    public function getGroupIds()
    {
      if($this->role == User::ROLE_FRANCHISE)
      {
          return $this->user_group;
      }else {
          $users = User::find()->where(['parent_id' => $this->id])->all();
          $grpStr = '';
          $count = 0;
          foreach ($users as $user) {
              $grpStr = $grpStr . $user->user_group . ', ';
              $count++;
          }
          if ($count > 0) {
              $grpStr = $grpStr . $this->user_group;
              return $grpStr;
          }
          return $this->user_group;
      }
    }

    public function getPackageText()
    {
    //   $invoiceData = Invoices::find()->where(['user_id' => $this->id])->andWhere(['<>','package_id', '0'])->orderBy(['id' => SORT_DESC])->one();
  		// if( !empty( $invoiceData ) ){
  		// 	$packageData	 = Packages::findOne( $invoiceData['package_id'] );
  		// 	$packageName 	 = $packageData['package_name'];
  		// }else{
  		// 	$packageName = "Pay as you go";
  		// }
    //   return $packageName;
    $free_access = $users = User::find()->where(['id' => $this->id,'free_access'=>1])->all();
      if($free_access){
        $packageName = 'Free-access';
        return $packageName;
      }else{
        $check_subscribe = Subscription::findOne(['user_id'=>$this->id,'is_cancelled'=>0]);
        if($check_subscribe){
          $packageName = $check_subscribe->package_name;
          return $packageName;
        }else{
          $packageName = 'Pay as you Go';
          return $packageName;
        }
      }
    }

    public function getUserType()
    {
        $users = User::find()->where(['email' => $this->email])->all();
        if(count($users) > 1){
            return 'Multi site';
        }else{
            if($users[0]['role'] == User::ROLE_USER) {
                return 'Standard';
            }else if($users[0]['role'] == User::ROLE_FRANCHISE){
                return 'Franchise';
            }else {
                return '';
            }
        }
    }

    public function getAllocatedPackage()
    {
      return $this->hasOne(Subscription::className(), ['user_id' => 'id']);
    }

}
