<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $role
 * @property string $website_url
 * @property integer $created_at
 * @property integer $updated_at
 */
class User2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }
	
	public $password;
    public $email2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'email', 'user_group', 'website_url'], 'required'],
			['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            [['parent_id', 'agency_name', 'agency_logo', 'theme_color'], 'safe'],
			['username', 'string', 'min' => 3, 'max' => 255],
            [['password_hash', 'password_reset_token', 'email', 'website_url'], 'string', 'max' => 255],
			['email', 'email'],
			['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
			['website_url','url'],
			//[['website_url'],'unique'],
            [['status', 'role', 'user_group', 'created_at', 'updated_at'], 'integer'],
			//['user_group', 'unique'],
            [['auth_key'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'parent_id' => 'Parent ID',
            'agency_name' => 'Agency',
            'agency_logo' => 'LOGO image dimenstions : 283x60 pixels',
            'theme_color' => 'Theme Color',
            'email2' => 'Other Email',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'role' => 'Role',
            'website_url' => 'Website Url',
            'created_at' => 'Created',
            'updated_at' => 'Updated',
        ];
    }
}
