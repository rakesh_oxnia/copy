<?php

namespace app\models;

use Yii;
use common\models\User;
use common\models\Chat;
use common\models\Packages;
use common\models\PackageUser;
use frontend\models\Invoices;
use common\models\UserCategory;
use app\models\Subscription;
/**
 * This is the model class for table "leads".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $chat_id
 * @property string $status
 */
class Leads extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static $statuses = [
       'pending' => 'Pending',
       'approved' => 'Complete',
       'rejected' => 'In progress',
     ];

    public static function tableName()
    {
        return 'leads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'chat_id', 'status'], 'required'],
            [['visitor_email', 'created_by'], 'email'],
            [['visitor_name', 'visitor_phone', 'visitor_last_name', 'preferred_contact_time'], 'string', 'max' => 255],
            [['user_id', 'category_id', 'outlet_id', 'from_agent_app'], 'integer'],
            [['status', 'chat_summary'], 'string'],
			      [['email'], 'integer'],
            [['chat_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'chat_id' => 'Chat ID',
            'category_id' => 'Category',
            'outlet_id' => 'Outlet',
            'status' => 'Status',
			      'email' => 'Email',
        ];
    }

	public static function getStatus($chat_id) {
		if($lead = Leads::find()->where(['chat_id'=>$chat_id])->one()){
			return $lead->status;
		}

		return 'pending';
	}

	public function getCategory()
  {
    return $this->hasOne(UserCategory::className(), ['id' => 'category_id']);
  }

	public static function getMail($chat_id){
		if($lead = Leads::find()->where(['chat_id'=>$chat_id])->one()){
			return $lead->email;
		}

		return 'Not Sent';
	}

	public static function getTotal($data){
		$total = 0;
		foreach($data as $d){
			$total += $d['count'];
		}

		return $total;
	}

	public static function getTotal2($data){
		$total = 0;
		foreach($data as $d){

		}
	}

	public static function getTotal3($data){
		$total = 0;
		foreach($data as $d){

		}
	}

	public static function getTotal4($data){
		$total = 0;
		foreach($data as $d){

		}
	}

	public static function getData($group,$user_id,$filter=false,$date_form=false,$date_to=false,$where=false,$group_by=false,$outlet_id=false){

		$today = date('Y-m-d');
		$yesterday = date('Y-m-d',strtotime("-1 days"));
		$last7 = date('Y-m-d',strtotime("-7 days"));
		$last30 = date('Y-m-d',strtotime("-30 days"));

		/*
			SELECT COUNT(*), HOUR(FROM_UNIXTIME(c_time)) as Hour, DAY(FROM_UNIXTIME(c_time)) as Day,DATE_FORMAT(FROM_UNIXTIME(c_time), '%d-%m-%Y') AS 'date_formatted' FROM leads WHERE c_time BETWEEN UNIX_TIMESTAMP('2016-05-18 00:00:01') AND UNIX_TIMESTAMP('2016-05-21 09:59:59') GROUP BY HOUR(FROM_UNIXTIME(c_time))
		*/

		$connection=Yii::$app->db;
		$query = 'SELECT COUNT(*) as count, HOUR(FROM_UNIXTIME(c_time)) as Hour, DAY(FROM_UNIXTIME(c_time)) as Day,DATE_FORMAT(FROM_UNIXTIME(c_time), "%Y-%m-%d") AS "date_formatted",DATE_FORMAT(FROM_UNIXTIME(c_time), "%Y-%m-%d %h:%i:%s") AS "date_hour"
		FROM leads ';

		$query .= 'WHERE';

		if($where){
			$query .= ' status = "'.$where.'" AND';
		}

		if(Yii::$app->user->identity->role == User::ROLE_OUTLET){
			$query .= ' outlet_id = "'.Yii::$app->user->identity->id.'" AND';
		}

    if($outlet_id){
      $query .= ' outlet_id = "'.$outlet_id.'" AND';
    }
    	if(is_array($group)) {
    		$query .= ' user_group in ('.implode(',', $group).') AND';
    	} else {
    		$query .= ' user_group = '.$group.' AND';
    	}		

		$query .= ' c_time BETWEEN UNIX_TIMESTAMP("'.$date_form.' 00:00:01") AND UNIX_TIMESTAMP("'.$date_to.' 23:59:59")';

		//echo $query;die;

		if($group_by=='hour'){
			$query .=' GROUP BY HOUR(FROM_UNIXTIME(c_time))';
		}else{
			$query .=' GROUP BY DAY(FROM_UNIXTIME(c_time))';
		}

    //print_r($query); exit;

		$lead = $connection->createCommand($query)->queryAll();

		//echo '<pre>';print_r($lead);echo '</pre>';die;

		return $lead;

		/* if($lead = Leads::find()->where(['chat_id'=>$chat_id])->one()){
			return $lead->email;
		}

		return 'Not Sent'; */
	}

	public static function convert($data,$no,$format,$start,$end){
		$start2 = new \DateTime($start);

		$startTime = strtotime($start);
		$endTime = strtotime($end);

		// Loop between timestamps, 1 day at a time
		$i = 0;
		$return ="";
		do {
		   $newTime = strtotime('+'.$i++.' days',$startTime);
		   $res = Leads::in_array_r(date('Y-m-d',$newTime), $data, true);
		   $newTime2 = $newTime * 1000;
		   $return .= "[$newTime2,$res],";
		} while ($newTime <= $endTime);

		return $return;

	}

	public static function convertHour($data,$start){
		$start2 = new \DateTime($start);

		$startTime = strtotime($start);

		// Loop between timestamps, 1 day at a time
		$i = 0;
		$i2 = 1;
		$return ="";
		do {
			//echo sprintf("%02d", $i)."=";
		   $res = Leads::in_array_r2($i, $data, true);
		   //$newTime2 = $startTime * $i2 * 1000;
		   $newTime2 = strtotime($start.' '.sprintf("%02d", $i).':00:00')*1000;
		   $return .= "[$newTime2,$res],";
		   $i++;
		   $i2++;
		} while ($i < 24);

		return $return;

	}

	public static function in_array_r($needle, $haystack, $strict = false) {
		//echo $needle;
		foreach ($haystack as $item) {
			/* if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && 	Leads::in_array_r($needle, $item, $strict))) {
				echo '<pre>';print_r($item);echo '</pre>';
			} */
			if($item['date_formatted']==$needle){
				return $item['count'];
			}

		}

		return '0';

		//return "hi";
	}

	public static function in_array_r2($needle, $haystack, $strict = false) {
		foreach ($haystack as $item) {
			if($item['Hour']==$needle){
				return $item['count'];
			}

		}

		return '0';

		//return "hi";
	}

	/* public static function checkEmail($chat_id){
		if($lead = Leads::find()->where(['chat_id'=>$chat_id])->one()){
			return $lead->email;
		}
		return true;
	} */

	 public function sendEmail($user,$lead, $chat2, $showLead, $name, $email, $phone = 'xxxx', $chat_summary = 'xxxx', $preferred_contact_time = 'Not provided')
    {

		/*$row = (new \yii\db\Query())
                    ->from('user')
                    ->where(['id' => $user->id])
                    ->one();
         if($row['parent_id'] > 0)
         {

         		$row2 = (new \yii\db\Query())
                    ->from('user')
                    ->where(['id' => $row['parent_id']])
                    ->one();

                $email = $row2['email'];
                $agency_name = $row2['agency_name'];
		        $agency_logo = $row2['agency_logo'];

		        if($agency_logo != '')
	            {
	               $base64img = 'http://fubaco.com/frontend/web/logo_images/'.$agency_logo;
	            }
	            else
	            {
	                $base64img = 'http://oxnia.com/signatures/agencyleads-logo.png';
	            }

	            return \Yii::$app->mailer->compose(['html' => 'leadnewagencyclient-html', 'text' => 'leadnewagencyclient-text'], ['user' => $user,'lead'=>$lead,'base64img' => $base64img])
                    ->setFrom($email => $agency_name . ' ']) // mail from
                    ->setTo($user->email)
                    ->setSubject('You have a new lead ') // mail subject
                    ->send();

        }
		else
		{*/
			if($user->send_transcript == 1)
			{
				$data = \Yii::$app->mailer->compose(['html' => 'leadnew-html', 'text' => 'leadnew-text'], ['user' => $user,'lead'=>$lead, 'chat'=>$chat2, 'showLead' => $showLead, 'name' => $name, 'email' => $email, 'phone' => $phone, 'chat_summary' => $chat_summary, 'preferred_contact_time' => $preferred_contact_time ])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
                    ->setTo($user->email)
                    //->setTo('ravindra@webcubictechnologies.com')
                    //->setTo('chetan@webcubictechnologies.com')
                    ->setSubject('You have a new lead ') // mail subject
                    ->send();
			}
			else
			{
				$data = \Yii::$app->mailer->compose(['html' => 'leadnewchat-html', 'text' => 'leadnewchat-text'], ['user' => $user,'lead'=>$lead, 'showLead' => $showLead, 'name' => $name, 'email' => $email, 'phone' => $phone, 'chat_summary' => $chat_summary, 'preferred_contact_time' => $preferred_contact_time])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
                    ->setTo($user->email)
                    //->setTo('ravindra@webcubictechnologies.com')
                    //->setTo('chetan@webcubictechnologies.com')
                    ->setSubject('You have a new lead ') // mail subject
                    ->send();
			}


			// $connection = Yii::$app->getDb();
	    //     $command = $connection->createCommand('SELECT *
	    //     FROM  `user_associate`
	    //     WHERE  `user_id` ='.$user->id);
	    //     $bl = $command->queryAll();
      //
	    //     if(count($bl) > 0)
	    //     {
	    //         $i = 0;
	    //         foreach ($bl as $value) {
	    //             if($user->send_transcript == 1)
			// 		        {
	    //             	\Yii::$app->mailer->compose(['html' => 'leadnewchat-html', 'text' => 'leadnewchat-text'], ['user' => $user,'lead'=>$lead,'chat'=>$chat2, 'showLead' => $showLead, 'name' => $name, 'email' => $email])
	    //                 ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
	    //                 ->setTo($value['email'])
	    //                 ->setSubject('You have a new lead ') // mail subject
	    //                 ->send();
	    //             }
	    //             else
	    //             {
	    //             	\Yii::$app->mailer->compose(['html' => 'leadnew-html', 'text' => 'leadnew-text'], ['user' => $user,'lead'=>$lead, 'showLead' => $showLead, 'name' => $name, 'email' => $email])
	    //                 ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
	    //                 ->setTo($value['email'])
	    //                 ->setSubject('You have a new lead ') // mail subject
	    //                 ->send();
	    //             }
	    //         }
	    //     }
			return $data;
        	//echo '<pre>';print_r($lead);die;

		//}


    }

     public static function sendEmailAgentApp($user,$lead, $chat2, $showLead, $name, $email, $visitor_phone, $chat_summary)
     {
   			if($user->send_transcript == 1)
   			{
   				$data = \Yii::$app->mailer->compose(['html' => 'leadnewchat-html', 'text' => 'leadnew_agentapp-text'], ['user' => $user,'lead'=>$lead, 'chat'=>$chat2, 'showLead' => $showLead, 'name' => $name, 'email' => $email, 'visitor_phone' => $visitor_phone, 'chat_summary' => $chat_summary])
                       ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
                       ->setTo($user->email)
                       //->setTo('ravindra@webcubictechnologies.com')
                       ->setSubject('You have a new lead ') // mail subject
                       ->send();
   			}
   			else
   			{
   				$data = \Yii::$app->mailer->compose(['html' => 'leadnew-html', 'text' => 'leadnew_agentapp-text'], ['user' => $user,'lead'=>$lead, 'showLead' => $showLead, 'name' => $name, 'email' => $email, 'phone' => $visitor_phone, 'chat_summary' => $chat_summary ])
                       ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
                       ->setTo($user->email)
                       //->setTo('ravindra@webcubictechnologies.com')
                       ->setSubject('You have a new lead ') // mail subject
                       ->send();
   			}
        return $data;
     }

     public static function sendEmailAgent($user,$lead, $chat2, $showLead, $name, $email, $phone = 'xxxx', $chat_summary = 'xxxx', $preferred_contact_time = 'Not provided')
     {
        if($user->send_transcript == 1)
        {
          $data = \Yii::$app->mailer->compose(['html' => 'leadnewchat-html', 'text' => 'leadnewchat-text'], ['user' => $user,'lead'=>$lead, 'chat'=>$chat2, 'showLead' => $showLead, 'name' => $name, 'email' => $email, 'phone' => $phone, 'chat_summary' => $chat_summary, 'preferred_contact_time' => $preferred_contact_time])
                       ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
                       ->setTo($user->email)
                       //->setTo('ravindra@webcubictechnologies.com')
                       //->setTo('chetan@webcubictechnologies.com')
                       ->setSubject('You have a new lead ') // mail subject
                       ->send();
        }
        else
        {
          $data = \Yii::$app->mailer->compose(['html' => 'leadnew-html', 'text' => 'leadnew-text'], ['user' => $user,'lead'=>$lead, 'showLead' => $showLead, 'name' => $name, 'email' => $email, 'phone' => $phone, 'chat_summary' => $chat_summary, 'preferred_contact_time' => $preferred_contact_time])
                       ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
                       ->setTo($user->email)
                       //->setTo('ravindra@webcubictechnologies.com')
                       //->setTo('chetan@webcubictechnologies.com')
                       ->setSubject('You have a new lead ') // mail subject
                       ->send();
        }
        return $data;
     }

     public static function sendLeadEmail($user,$lead, $chat2, $showLead, $name, $email, $leadModel)
     {
        //$user->send_transcript = 0;
        if($user->send_transcript == 1)
		{
			$data = \Yii::$app->mailer->compose(['html' => 'leadChatEmailTemplate-html', 'text' => 'leadChatEmailTemplate-text'], ['user' => $user,'lead'=>$lead, 'chat'=>$chat2, 'showLead' => $showLead, 'name' => $name, 'email' => $email, 'leadModel' => $leadModel])
               ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
               //->setTo($user->email)
               ->setTo('ravindra@webcubictechnologies.com')
               //->setTo('chetan@webcubictechnologies.com')
               ->setSubject('You have a new lead ') // mail subject
               ->send();
		}
		else
		{
			$data = \Yii::$app->mailer->compose(['html' => 'leadEmailTemplate-html', 'text' => 'leadEmailTemplate-text'], ['user' => $user,'lead'=>$lead, 'showLead' => $showLead, 'name' => $name, 'email' => $email, 'leadModel' => $leadModel])
               ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
               //->setTo($user->email)
               ->setTo('ravindra@webcubictechnologies.com')
               //->setTo('chetan@webcubictechnologies.com')
               ->setSubject('You have a new lead ') // mail subject
               ->send();
		}
     }

     public static function sendForwardingEmail($user, $chat2, $showLead, $name, $email, $leadModel, $forwardEmail)
     {
     	$lead = $chat2;
     	$phone = $leadModel->visitor_phone;
     	$chat_summary = $leadModel->chat_summary;
     	if($user->send_transcript == 1)
  		{
  			// $data = \Yii::$app->mailer->compose(['html' => 'leadChatEmailTemplate-html', 'text' => 'leadChatEmailTemplate-text'], ['user' => $user, 'chat'=>$chat2, 'showLead' => $showLead, 'name' => $name, 'email' => $email, 'leadModel' => $leadModel])
     //             ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
     //             //->setTo($user->email)
     //             ->setTo($forwardEmail)
     //             //->setTo('chetan@webcubictechnologies.com')
     //             ->setSubject('You have a new lead ') // mail subject
     //             ->send();

                 $data = \Yii::$app->mailer->compose(['html' => 'leadnewchat-html', 'text' => 'leadnewchat-text'], ['user' => $user, 'lead' => $lead, 'chat'=>$chat2, 'showLead' => $showLead, 'phone' => $phone, 'chat_summary' => $chat_summary, 'name' => $name, 'email' => $email, 'leadModel' => $leadModel])
                 ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
                 //->setTo($user->email)
                 ->setTo($forwardEmail)
                 ->setSubject('You have a new lead ') // mail subject
                 ->send();
  		}
  		else
  		{
  			// $data = \Yii::$app->mailer->compose(['html' => 'leadEmailTemplate-html', 'text' => 'leadEmailTemplate-text'], ['user' => $user, 'showLead' => $showLead, 'name' => $name, 'email' => $email, 'leadModel' => $leadModel])
     //             ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
     //             //->setTo($user->email)
     //             ->setTo($forwardEmail)
     //             ->setSubject('You have a new lead ') // mail subject
     //             ->send();

              $data = \Yii::$app->mailer->compose(['html' => 'leadnew-html', 'text' => 'leadnew-text'], ['user' => $user, 'lead' => $lead, 'showLead' => $showLead, 'phone' => $phone, 'chat_summary' => $chat_summary, 'name' => $name, 'email' => $email, 'leadModel' => $leadModel])
                 ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
                 //->setTo($user->email)
                 ->setTo($forwardEmail)
                 ->setSubject('You have a new lead ') // mail subject
                 ->send();
  		}
      return $data;
     }

     //================================================================//
     public static function sendServicechatAllEmail($user, $chat2, $showLead, $name, $email, $leadModel, $forwardEmail)
     {
     	$lead = $chat2;
     	$phone = $leadModel->visitor_phone;
     	$chat_summary = $leadModel->chat_summary;
     	if($user->send_transcript == 1)
  		{
                 $data = \Yii::$app->mailer->compose(['html' => 'servicenewchat-html', 'text' => 'servicenewchat-text'], ['user' => $user, 'lead' => $lead, 'chat'=>$chat2, 'showLead' => $showLead, 'phone' => $phone, 'chat_summary' => $chat_summary, 'name' => $name, 'email' => $email, 'leadModel' => $leadModel])
                 ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
                 //->setTo($user->email)
                 ->setTo($forwardEmail)
                 ->setSubject('Service Chat') // mail subject
                 ->send();
  		}
  		else
  		{
              $data = \Yii::$app->mailer->compose(['html' => 'servicenew-html', 'text' => 'servicenew-text'], ['user' => $user, 'lead' => $lead, 'showLead' => $showLead, 'phone' => $phone, 'chat_summary' => $chat_summary, 'name' => $name, 'email' => $email, 'leadModel' => $leadModel])
                 ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
                 //->setTo($user->email)
                 ->setTo($forwardEmail)
                 ->setSubject('Service Chat') // mail subject
                 ->send();
  		}
  		return $data;
     }

     //======================================================================//
     public static function sendEmailServicechat($user,$lead, $chat2, $showLead, $name, $email, $phone = 'xxxx', $chat_summary = 'xxxx', $preferred_contact_time = 'Not provided'){
       if($user->send_transcript == 1)
       {
         $data = \Yii::$app->mailer->compose(['html' => 'servicenewchat-html', 'text' => 'servicenewchat-text'], ['user' => $user,'lead'=>$lead, 'chat'=>$chat2, 'showLead' => $showLead, 'name' => $name, 'email' => $email, 'phone' => $phone, 'chat_summary' => $chat_summary, 'preferred_contact_time' => $preferred_contact_time])
                      ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
                      ->setTo($user->email)
                      //->setTo('ravindra@webcubictechnologies.com')
                      ->setSubject('Service Chat') // mail subject
                      ->send();
       }
       else
       {
         $data = \Yii::$app->mailer->compose(['html' => 'servicenew-html', 'text' => 'servicenew-text'], ['user' => $user,'lead'=>$lead, 'showLead' => $showLead, 'name' => $name, 'email' => $email, 'phone' => $phone, 'chat_summary' => $chat_summary, 'preferred_contact_time' => $preferred_contact_time])
                      ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
                      ->setTo($user->email)
                      //->setTo('ravindra@webcubictechnologies.com')
                      ->setSubject('Service Chat') // mail subject
                      ->send();
       }
       return $data;

     }
     //======================================================================//

  	 public static function filterLeadPageData( $transcripts, $leads ){

  		foreach($transcripts as $key => $chat){

  			$column['id'] = $chat->id;
  			$column['messages'] = $chat->messages;
  			$column['started_timestamp'] = $chat->started_timestamp;
  			if(isset( $chat->visitor->email )){
  				$column['email'] = $chat->visitor->email;
  			}else{
  				$column['email'] = '';
  			}
  			$column['name'] = $chat->visitor->name;
  			$column['city'] = $chat->visitor->city;
  			$column['region'] = $chat->visitor->region;
  			$column['country'] = $chat->visitor->country;

        // --from here --
  			// $column['tags'] = $chat->tags[0];
  			// if(isset($chat->tags[1])){
  			// 	$column['tags'] = $chat->tags[1];
  			// }
        // -- to here --

        // edit by  ravindra
        $column['tags'] = isset($chat->tags[0]) ? $chat->tags[0] : '' ;
  			if(isset($chat->tags[1])){
  				$column['tags'] = $chat->tags[1];
  			}

  			$leads[] = $column;

  		}

  		return $leads;
  	}


	public static function getAllLeads( $leads, $packageInfo ){

    //echo "<pre>"; print_r($packageInfo); exit;
		$packageDataCount = [];
		$leadData = [];

		foreach($leads as $key => $chat){
			$name = '';
			$email = '';
			if(isset($chat['messages'])){
				foreach($chat['messages'] as $messages){
					$messageData = $messages->text;
					$namePos = stripos($messageData,"name:");
					if( $namePos > 0 ){
						$nameStartPos = $namePos + 5;
						$emailPos = stripos($messageData,"email:");
						$nameLen = $emailPos - $nameStartPos;
						$emailStartPos = $emailPos + 6;
						$phonePos = stripos($messageData,"phone:");
						$emailLen = $phonePos - $emailStartPos;
						$name = substr($messageData, $nameStartPos, $nameLen);
						$email = substr($messageData, $emailStartPos, $emailLen);
					}
				}
			}

			$showLead = 0;
			if( !empty( $packageInfo ) ){
				foreach($packageInfo as $packageDate => $leadCount){
          // Package filter code
					/*if((strtotime( $packageDate) <= $chat['started_timestamp']) && (strtotime( "$packageDate + 30 days") >= $chat['started_timestamp']) && $leadCount > $packageDataCount[$packageDate]){
						$showLead = 1;
						$packageDataCount[$packageDate] = $packageDataCount[$packageDate] + 1;
						break;
					}*/
          // Package filter code

          // Package code modification by Ravindra
          /*if((strtotime( $packageDate) <= $chat['started_timestamp']) && (strtotime( "$packageDate + 30 days") >= $chat['started_timestamp']) && $leadCount > $packageDataCount[$packageDate]){
						$showLead = 1;
						$packageDataCount[$packageDate] = $packageDataCount[$packageDate] + 1;
						break;
					}*/
          //$showLead = 1;
          //$packageDataCount[$packageDate] = $packageDataCount[$packageDate] + 1;
          // Package code modification by Ravindra
				}
			}

			if( !$showLead ){
				$isPaid = Invoices::find()->where(['chat_id' => $chat['id'], 'user_id' => Yii::$app->user->id])->one();
				if($isPaid){
					$showLead = 1;
				}
				else if( $key < 5 ){
					$showLead = 1;
				}
			}

			if($chat['email'] != ''){
				if(!$showLead){
					$column['name'] = Leads::obfuscate_name($chat['name']);
				}else{
					$column['name'] = $chat['name'];
				}
			}else{
				if(!$showLead){
					$column['name'] = Leads::obfuscate_name($name);
				}else{
					$column['name'] = $name;
				}
			}


			if($chat['email'] != ''){
				if(!$showLead){
					$column['email'] = preg_replace('/(?:^|@).\K|\.[^@]*$(*SKIP)(*F)|.(?=.*?\.)/', '*', $chat['email']);
				}else{
					$column['email'] = $chat['email'];
				}
			}else{
				if(!$showLead){
					$column['email'] = preg_replace('/(?:^|@).\K|\.[^@]*$(*SKIP)(*F)|.(?=.*?\.)/', '*', $email);
				}else{
					$column['email'] = $email;
				}
			}

			$column['id'] 				 = $chat['id'];
			$column['messages'] 		 = $chat['messages'];
			$column['started_timestamp'] = $chat['started_timestamp'];
			$column['city'] 			 = $chat['city'];
			$column['region'] 			 = $chat['region'];
			$column['country'] 			 = $chat['country'];
			$column['tags']				 = $chat['tags'];
			$column['showLead'] 		 = $showLead;

			$leadData[] = $column;

		}

		return $leadData;
	}

  public static function getAllLeadsData( $leads, $packageInfo, $user = false ){

    //echo "<pre>"; print_r($packageInfo); exit;
		$packageDataCount = [];
		$leadData = [];

    $packageShowCount = 0;
		foreach($leads as $key => $chat){
			$name = '';
			$email = '';
			if(isset($chat['messages'])){
				foreach($chat['messages'] as $messages){
					$messageData = $messages->text;
					$namePos = stripos($messageData,"name:");
					if( $namePos > 0 ){
						$nameStartPos = $namePos + 5;
						$emailPos = stripos($messageData,"email:");
						$nameLen = $emailPos - $nameStartPos;
						$emailStartPos = $emailPos + 6;
						$phonePos = stripos($messageData,"phone:");
						$emailLen = $phonePos - $emailStartPos;
						$name = substr($messageData, $nameStartPos, $nameLen);
						$email = substr($messageData, $emailStartPos, $emailLen);
					}
				}
			}

			$showLead = 0;
			if( !empty( $packageInfo ) ){
				foreach($packageInfo as $packageDate => $leadCount){
          // Package filter code
					if((strtotime( $packageDate) <= $chat['started_timestamp']) && (strtotime( "$packageDate + 30 days") >= $chat['started_timestamp']) && $leadCount > $packageShowCount){
						$showLead = 1;
            $packageShowCount++;
						break;
					}
          // Package filter code
				}
			}

			if( !$showLead ){
				$isPaid = Invoices::find()->where(['chat_id' => $chat['id'], 'user_id' => Yii::$app->user->id])->one();
				if($isPaid){
					$showLead = 1;
				}
				else if( $key < 5 ){
					$showLead = 1;
				}
			}

      // if(Yii::$app->user->identity->role == User::ROLE_FRANCHISE || Yii::$app->user->identity->role == User::ROLE_OUTLET ){
      //   $showLead = 1;
      // }

        if($user){
          if($user->free_access){
              $showLead = 1;
          }elseif ($user->role == User::ROLE_OUTLET && $user->parent->free_access) {
              $showLead = 1;
          }
        }else{
          if(Yii::$app->user->identity->free_access){
              $showLead = 1;
          }elseif (Yii::$app->user->identity->role == User::ROLE_OUTLET && Yii::$app->user->identity->parent->free_access) {
              $showLead = 1;
          }
        }

		    $leadModel = Leads::findOne(['chat_id' => $chat['id']]);

		    if($leadModel){
              $column['name'] = $leadModel->visitor_name == NULL ? '' : $leadModel->visitor_name;
              if($showLead != 1){
                $column['name'] = Leads::obfuscate_name($column['name']);
              }

              $column['email'] = $leadModel->visitor_email == NULL ? '' : $leadModel->visitor_email;
              if($showLead != 1){
                $column['email'] = Leads::obfuscate_email($column['email']);
              }
            }else{
            	$column['name'] = '';
            	$column['email'] = '';

				if(!$showLead){
					$column['name'] = Leads::obfuscate_name($chat['name']);
					$column['email'] = Leads::obfuscate_email($chat['email']);
				}else{
					$column['name'] = $chat['name'];
					$column['email'] = $chat['email'];
				}
            }

			$column['id'] 				 = $chat['id'];
			$column['messages'] 		 = $chat['messages'];
			$column['started_timestamp'] = $chat['started_timestamp'];
			$column['city'] 			 = $chat['city'];
			$column['region'] 			 = $chat['region'];
			$column['country'] 			 = $chat['country'];
			$column['tags']				 = $chat['tags'];
			$column['showLead'] 		 = $showLead;

			$leadData[] = $column;

		}
    //echo "<pre>"; print_r('Count: ' . $packageShowCount); exit;

		return $leadData;
	}

	public static function getAllChats( $chats, $packageInfo ){

		$packageDataCount = [];
		$chatData = [];
		$is_free_lead = 0;

		foreach($chats as $key => $chat){
			$name = '';
			$email = '';
			if(isset($chat['messages'])){
				foreach($chat['messages'] as $messages){
					$messageData = $messages->text;
					$namePos = stripos($messageData,"name:");
					if( $namePos > 0 ){
						$nameStartPos = $namePos + 5;
						$emailPos = stripos($messageData,"email:");
						$nameLen = $emailPos - $nameStartPos;
						$emailStartPos = $emailPos + 6;
						$phonePos = stripos($messageData,"phone:");
						$emailLen = $phonePos - $emailStartPos;
						$name = substr($messageData, $nameStartPos, $nameLen);
						$email = substr($messageData, $emailStartPos, $emailLen);
					}
				}
			}
			if($chat['tags'] == 'lead'){
				$showLead = 0;
				if( !empty( $packageInfo ) ){
					foreach($packageInfo as $packageDate => $leadCount){

						if((strtotime( $packageDate) <= $chat['started_timestamp']) && (strtotime( "$packageDate + 30 days") >= $chat['started_timestamp']) && isset($packageDataCount[$packageDate]) && $leadCount > $packageDataCount[$packageDate]){
							$showLead = 1;
							$packageDataCount[$packageDate] = $packageDataCount[$packageDate] + 1;
							break;
						}
					}
				}

				if( !$showLead ){
					$isPaid = Invoices::find()->where(['chat_id' => $chat['id'], 'user_id' => Yii::$app->user->id])->one();
					if($isPaid){
						$showLead = 1;
					}
					else if( $is_free_lead < 5 ){
						$showLead = 1;
					}
				}

        if(!$showLead){
				    if(Yii::$app->user->identity->free_access){
			        	$showLead = 1;
			        }elseif (Yii::$app->user->identity->role == User::ROLE_OUTLET && Yii::$app->user->identity->parent->free_access) {
				        $showLead = 1;
				    }
				}


				$is_free_lead++;
			}else{
				$showLead = 1;
			}

			if($chat['email'] != ''){
				if(!$showLead){
					$column['name'] = Leads::obfuscate_name($chat['name']);
				}else{
					$column['name'] = $chat['name'];
				}
			}else{
				if(!$showLead){
					$column['name'] = Leads::obfuscate_name($name);
				}else{
					$column['name'] = $name;
				}
			}


			if($chat['email'] != ''){
				if(!$showLead){
					$column['email'] = preg_replace('/(?:^|@).\K|\.[^@]*$(*SKIP)(*F)|.(?=.*?\.)/', '*', $chat['email']);
				}else{
					$column['email'] = $chat['email'];
				}
			}else{
				if(!$showLead){
					$column['email'] = preg_replace('/(?:^|@).\K|\.[^@]*$(*SKIP)(*F)|.(?=.*?\.)/', '*', $email);
				}else{
					$column['email'] = $email;
				}
			}

			$column['id'] 				 = $chat['id'];
			$column['messages'] 		 = $chat['messages'];
			$column['started_timestamp'] = $chat['started_timestamp'];
			$column['city'] 			 = $chat['city'];
			$column['region'] 			 = $chat['region'];
			$column['country'] 			 = $chat['country'];
			$column['tags'] 			 	 = $chat['tags'];
			$column['showLead'] 		 = $showLead;

			$chatData[] = $column;

		}

		return $chatData;
	}


	public static function findModel($id)
  {
      if (($model = Chat::findOne($id)) !== null) {
          return $model;
      } else {
          throw new NotFoundHttpException('The requested page does not exist.');
      }
  }
  public static function findPackage(){
       $user_id    = Yii::$app->user->identity->id;
       $invoiceData = Invoices::find()->where(['user_id' => $user_id])->all();
       $payment_date = '';
       $number_of_leads = '';
       $pack_type = '';
       if( !empty( $invoiceData ) ){
         foreach( $invoiceData as $key => $invoice){
           if($invoice['package_id'] > 0){
             $package_id = $invoice['package_id'];
             $packageData = Packages::findOne( $package_id );
             if( !empty( $packageData ) ){
               if($packageData->package_name == 'Custom' && ($pack_type == '' || $pack_type == 'Gold' || $pack_type == 'Startup')){
                 $packageUser = PackageUser::findOne(['user_id' => Yii::$app->user->identity->id]);
                 if($packageUser){
                     $payment_date = $invoice->payment_date;
                     $number_of_leads = $packageUser->number_of_leads;
                     $pack_type = 'Custom';
                 }
               }elseif ($packageData->package_name == 'Gold' && ($pack_type == '' || $pack_type == 'Startup')) {
                 $payment_date = $invoice->payment_date;
                 $number_of_leads = $packageData->number_of_leads;
                 $pack_type = 'Gold';
               }elseif ($packageData->package_name == 'Startup' && $pack_type == '') {
                 $payment_date = $invoice->payment_date;
                 $number_of_leads = $packageData->number_of_leads;
                 $pack_type = 'Startup';
               }
             }
           }
         }
       }
       if($pack_type != ''){
      return  [$payment_date => $number_of_leads];
       }
  }

  public static function findPackageInfo($packageInfo,$chatModel){
    $showLead = 0;
    $packageDataCount = [];
    $packageShowCount = 0;
    if( !empty( $packageInfo ) ){
      foreach($packageInfo as $packageDate => $leadCount){
        // Package filter code
        if((strtotime( $packageDate) <= $chatModel['started_timestamp']) && (strtotime( "$packageDate + 30 days") >= $chatModel['started_timestamp']) && $leadCount > $packageShowCount){
        $showLead = 1;
        $packageShowCount++;
        break;
        }
        // Package filter code
      }
    }
    //print_r($chatModel->chat_id);exit;
    if( !$showLead ){
      $isPaid = Invoices::find()->where(['chat_id' => $chatModel->chat_id, 'user_id' => Yii::$app->user->id])->one();
      if($isPaid){
        $showLead = 1;
      }
      // else if( $key < 5 ){
      // 	$showLead = 1;
      // }
    }

   $user = Yii::$app->user->identity;

    if($user){
      if($user->free_access){
          $showLead = 1;
      }elseif ($user->role == User::ROLE_OUTLET && $user->parent->free_access) {
          $showLead = 1;
      }
    }else{
      if(Yii::$app->user->identity->free_access){
          $showLead = 1;
      }elseif (Yii::$app->user->identity->role == User::ROLE_OUTLET && Yii::$app->user->identity->parent->free_access) {
          $showLead = 1;
      }
    }
    return $showLead;
  }

// 	public static function showleads($id){
//     $chatModel = Leads::findModel($id);
//     $packageInfo = Leads::findPackage();
//     $showLead = Leads::findPackageInfo($packageInfo,$chatModel);
//     return $showLead;
//   }

    public static function showleads($chat_id)
    {
        $lead = Leads::findOne(['chat_id' => $chat_id]);  //checking in leads Table//
        //echo date('Y-m-d H:i:s', $lead->c_time);
        if (isset($lead)) {
            if ($lead->is_service_chat == 0 && $lead->is_lead == 0) {
                return 1;            //anything except service chat and lead
            } else if ($lead->is_service_chat == 1) {
                return 1;            //checking for service chat
            } else {
                //its definately a lead
                if (Yii::$app->user->identity->id == 531 && $lead->c_time < 1551618000) {
                    return 1; //for user id 531: epcsolar show all leads till 2019-03-04 00:00:00 without any obfuscation
                }

                if ($lead->c_time < 1542200400) //strtotime('2018-11-15 00:00:00')
                {
                    return 1; //pay as you go goes live on 2018-11-15 00:00:00, so all leads created before this should be considered as free access
                }

                //check free access of logged in user: client, franchise or outlet
                //if (Yii::$app->user->identity->free_access == 1) {
                //    return 1;   //checking for free access
                //}

                //if i am client or franchise then check subscription right away
                if (Yii::$app->user->identity->role == User::ROLE_USER || Yii::$app->user->identity->role == User::ROLE_FRANCHISE) {
                    $user_id = empty(Yii::$app->user->identity->parent_id) ? Yii::$app->user->identity->id : Yii::$app->user->identity->parent_id;
                } else if ( in_array(Yii::$app->user->identity->role, [User::ROLE_OUTLET, User::ROLE_ASSIST]) ) {
                    //if i am outlet then check subscription of the parent franchise
                    $user_id = Yii::$app->user->identity->parent_id;
                }


                //check whether the user has free access
                $mainuser = User::findOne(['id' => $user_id]);
            	if ($mainuser->free_access == 1) {
                   return 1;   //checking for free access
                }                

                //$pay_as_you_go = 0;

                //find active subscription
                //$current_subscription = Subscription::find()->where(['user_id' => $user_id])->orderBy(['id' => SORT_DESC])->one(); //checking for subscription

                $query_subscription = "SELECT * FROM `subscription` WHERE `user_id` = $user_id ORDER BY id DESC LIMIT 1";
                $current_subscription = Subscription::findBySql($query_subscription)->one();

                //echo '<pre>';print_r($current_subscription);exit;
                if (isset($current_subscription)) {
                    if($current_subscription->is_cancelled == 0) {
                        // echo date('Y-m-d', $current_subscription->created_at).' == '.date('Y-m-d', $lead->c_time);
                        if (date('Y-m-d', $current_subscription->created_at) <= date('Y-m-d', $lead->c_time)) {
                            //lead generated after or on date of subscription
                            return 1;  //comparing lead date with subscribe on date
                        } else if (
                            $current_subscription->show_previous_lead == 1 &&
                            date('Y-m-d', $lead->c_time) < date('Y-m-d', $current_subscription->created_at)
                        ) {
                            //lead generated before subscription but show previous lead flag of subscription is ON
                            return 1;  //if show previous lead flag is true then all the leads which were generated prior to the subscription activation date must be visible to user
                        }
                    } else if($current_subscription->is_cancelled == 1) {
                        //Works only when already assigned package has been cancelled and user needs to be shown past leads)
                        //check whether show_past_lead flag is ON in user table
                        if($mainuser->show_past_lead == 1 && $mainuser->show_lead_from > 0 && $mainuser->show_lead_to > 0) {

                            if(
                                date('Y-m-d', $mainuser->show_lead_from) <= date('Y-m-d', $lead->c_time)    &&
                                date('Y-m-d', $mainuser->show_lead_to) >= date('Y-m-d', $lead->c_time)
                            )
                            {
                                return 1;
                            }
                        }
                    }
                }
//                else {
//                    $pay_as_you_go = 1;
//                }

                //find active subscription
//                $cancelled_subscription = Subscription::find()->where(['user_id' => $user_id, 'is_cancelled' => 1])->all(); //checking for subscription
//                if($cancelled_subscription) {
//                    foreach($cancelled_subscription as $subscription)
//                    {
//                        if(
//                            date('Y-m-d', $subscription->created_at) <= date('Y-m-d', $lead->c_time)    &&
//                            date('Y-m-d', $subscription->cancel_date) > date('Y-m-d', $lead->c_time)
//                        )
//                        {
//                            return 1;
//                            break;
//                        }
//                    }
//                }

                //if ($pay_as_you_go) {
                    //$invoice = Invoices::findOne(['chat_id'=>$chat_id]); //checking invoice for paid leads
                    //$invoice = Invoices::find()->where(['like', 'chat_id', $chat_id]); //checking invoice for paid leads
                    //echo $invoice->createCommand()->getRawSql();

                $query = "SELECT id FROM `invoices` WHERE `chat_id` LIKE '%$chat_id%' LIMIT 1";
                $invoice = Invoices::findBySql($query)->all();
                if (count($invoice) > 0) {
                    return 1;
                } else {
                    return 0;
                }
                //}
            }
        } else {
            return 1;  //lead or service chat or any other tagged chat not found.. return 1
        }
    }

  public static function shownewleads($chat_id,$user){
    $lead = Leads::findOne(['chat_id'=>$chat_id]);

    if($user->free_access==1){
              return 1;
    }

    $pay_as_you_go = 1;

    //if user is client or franchise then check subscription right away
    if($user->role == User::ROLE_USER || $user->role == User::ROLE_FRANCHISE)
    {
      $user_id = $user->id;
    }else if(in_array($user->role, [User::ROLE_OUTLET, User::ROLE_ASSIST]))
    {
      //if user is outlet then check subscription of the parent franchise
      $user_id = $user->parent_id;
    }
    $check_subscribe = Subscription::findOne(['user_id'=>$user_id, 'is_cancelled'=>0]);
    // if(isset($check_subscribe)){
    //   $pay_as_you_go = 0;
    // }

    if(isset($check_subscribe)){
        if(date('Y-m-d',$check_subscribe->created_at) <= date('Y-m-d',$lead->c_time)){
          //lead generated after or on date of subscription
          $pay_as_you_go = 0;  //comparing lead date with subscribe on date
        }else if(
          $check_subscribe->show_previous_lead == 1 &&
          date('Y-m-d',$lead->c_time) < date('Y-m-d',$check_subscribe->created_at)
        ){
          //lead generated before subscription but show previous lead flag is ON
          $pay_as_you_go = 0;  //if show previous lead flag is true then all the leads which were generated prior to the subscription activation date must be visible to user
        }else {
          $pay_as_you_go = 1; // show previous lead is OFF
        }
    }else {
      $pay_as_you_go = 1;
    }

    if($pay_as_you_go){
      if($chat_id){
         $invoice = Invoices::findOne(['chat_id'=>$chat_id]);
         if($invoice){
           return 1;
         }else{
           return 0;
         }
       }else{
         return 0;
       }
    }else{
      return 1;
    }
  }

	public static function obfuscate_email($email)
	{
		// $em   = explode("@",$email);
		// $name = implode(array_slice($em, 0, count($em)-1), '@');
		// $len  = floor(strlen($name)/2);
    //
		// return substr($name,0, $len) . str_repeat('*', $len) . "@" . end($em);

    $em   = explode("@",$email);
    $len_name  = strlen($em[0]);
    $len_domain = strlen($em[1]);

    if($len_name <= 1) {
    $obfuscated_name = '*';
    }else if($len_name == 2) {
    $obfuscated_name = '**';
    }else {
    $show_chars = substr($em[0],0,2);
    $obfuscated_chars = str_repeat('*', $len_name-2);
    $obfuscated_name = $show_chars.$obfuscated_chars;
    }

    return $obfuscated_name . '@' . str_repeat('*', $len_domain);
	}

	public static function obfuscate_name($name)
	{

		$len  = floor(strlen($name)/2);

		return substr($name,0, $len) . str_repeat('*', $len);
	}

  public function getFullName()
  {
    return ucwords($this->visitor_name . ' ' . $this->visitor_last_name);
  }

  public function getOutlet()
  {
      return $this->hasOne(User::className(), ['id' => 'outlet_id']);
  }

}
