<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User2;
use yii\data\SqlDataProvider;
use common\models\User;

/**
 * UserSearch represents the model behind the search form about `app\models\User2`.
 */
class UserSearch extends User2
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'role', 'parent_id', 'user_group', 'created_at', 'updated_at'], 'integer'],
            [['name', 'agency_name', 'username', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'website_url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $useraccess = false)
    { 
        $query = User2::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

//        if (!$this->validate()) {
//            // uncomment the following line if you do not want to return any records when validation fails
//            // $query->where('0=1');
//            return $dataProvider;
//        }

        if(Yii::$app->controller->action->id == 'client-franchise-index') {
            $query_multi_site_user = Yii::$app->db->createCommand("SELECT email, count(1) as cnt FROM user WHERE status != ".User::STATUS_DELETED." GROUP BY email HAVING cnt > 1")->queryAll();

            $arr_multi_site_user = array();
            if(count($query_multi_site_user) > 0) {
              foreach($query_multi_site_user as $user) {
                  $arr_multi_site_user[] = $user['email'];
              }
            }

            $query->andWhere(
                ['and',
                    ['role' => User::ROLE_USER],
                    //['parent_id' => 0]
                ]
            );

            $query->orWhere(
                ['and',
                    'role='.User::ROLE_FRANCHISE
                ]
            );

            $request = Yii::$app->request;
            //echo '<pre>';
            //print_r($request['UserSearch']);
            $get_param = $request->get();
            $role = $get_param['UserSearch']['role'];
            if ($role) {
                if ($role == 'standard') {
                    $query->andWhere(
                        ['and',
                            ['role' => User::ROLE_USER],
                            //['parent_id' => 0]
                        ]
                    );

                    if(count($arr_multi_site_user) > 0) {
                        $query->andFilterWhere(['not in', 'email', $arr_multi_site_user]);
                    }

                } else if ($role == 'franchise') {
                    $query->andWhere(
                        ['and',
                            ['role' => User::ROLE_FRANCHISE],
                            //['parent_id' => 0]
                        ]
                    );

                    if(count($arr_multi_site_user) > 0) {
                        $query->andFilterWhere(['not in', 'email', $arr_multi_site_user]);
                    }

                } else if ($role == 'multisite') {
                    $query->andFilterWhere(['in', 'email', $arr_multi_site_user]);
                }
            }

            $query->andFilterWhere(['parent_id' => 0]);
        }

        if(!empty($this->role))
            $query->andFilterWhere(['role' => $this->role]);
        
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            //'role' => $this->role,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user_group' => $this->user_group,
            'parent_id' => $this->parent_id,
        ]);

        //echo '<pre>';print_r($query);exit;

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'agency_name', $this->agency_name])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'website_url', $this->website_url]);

        if ($useraccess) {
            $query->andFilterWhere(['not in', 'email', Yii::$app->user->identity->email]);
            $query->andFilterWhere(['not in', 'role', [User::ROLE_ADMIN, User::ROLE_USER, User::ROLE_FRANCHISE, User::ROLE_OUTLET]]);
        } else {
            $query->andFilterWhere(['like', 'email', $this->email]);
        }

        $query->orderBy(['id' => SORT_DESC]);
        return $dataProvider;
    }
   
   public function getRolesData()
   {
        $count = Yii::$app->db->createCommand(
            "select count(*) from auth_item where name like '".$this->id."_%'")->queryScalar();

        $dataProvider = new SqlDataProvider([
            'sql' => "select name from auth_item where name like '".$this->id."_%'",
            'totalCount' => $count,
            'sort' => [
                'attributes' => [
                    'name',
                ],
            ],
            'key' => 'name',
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $dataProvider;
   }   

    public function getRoles()
   {
        $roles = Yii::$app->db->createCommand(
            "select name from auth_item where name like '".$this->id."_%'")->queryAll();

        return $roles;
   }   
}
