<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pay_as_you_go".
 *
 * @property integer $id
 * @property integer $business_category_id
 * @property string $charges_per_lead
 */
class Payasyougo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pay_as_you_go';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['business_category_id', 'charges_per_lead'], 'required'],
            [['business_category_id'], 'integer'],
            [['charges_per_lead'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'business_category_id' => 'Business Category',
            'charges_per_lead' => 'Charges Per Lead',
        ];
    }
	
	
	
	public function getBusinesscategory() {
                return $this->hasOne(BusinessCategory::className(), ['id' => 'business_category_id']);
    }
}
