<?php

namespace app\models;

use Yii;
//use yii\base\ErrorException;
use common\models\Packages;
use common\models\User;
/**
 * This is the model class for table "apps_countries".
 *
 * @property integer $id
 * @property string $country_code
 * @property string $country_name
 */
class Subscription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }

    public function getSubscribedUserModel()
    {
      return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getPackageModel()
    {
      return $this->hasOne(Packages::className(), ['id' => 'package_id']);
    }

    public static function sendPackageAllocationSuccessMail($amount,$data,$email,$currency){
      try {
        $date = date('d-m-Y', $data);
        $time = date('h:i a', $data);
        $mail = Yii::$app->mailer->compose(['html' => 'package-allocated-success-html'], ['amount' => $amount,'date'=>$date,'time'=>$time,'currency'=>$currency])
                     ->setFrom([\Yii::$app->params['noReplyBillingEmail'] => \Yii::$app->name . ' ']) // mail from
                      ->setTo($email)
                     ->setSubject('Package allocated successfully') // mail subject
                     ->send();
      }
      // catch (Exception $e) { //this does not work as backslash is not available
      //       echo '1 mail sending failed';
      // }
      catch(\Swift_TransportException $exception) {
        //mail sending failed
      }
      catch(\Exception $exception) {
        //some error occured
      }
    }

    public static function sendPackageAllocationPlanASuccessMail($invoice_id,$stripe_transaction_id,$amount,$data,$email,$currency){
      try
      {
        //$date = date('d-m-Y', $data);
        $date = date('d/m/Y', $data);
        $time = date('h:i a', $data);
        $mail = Yii::$app->mailer->compose(['html' => 'package-allocated-plan-a-success-html'], ['invoice_id' =>$invoice_id,'stripe_transaction_id'=>$stripe_transaction_id,'amount' => $amount, 'date'=>$date, 'time'=>$time, 'currency'=>$currency])
                     ->setFrom([\Yii::$app->params['noReplyBillingEmail'] => \Yii::$app->name . ' ']) // mail from
                      ->setTo($email)
                     ->setSubject('Invoice #'.$invoice_id) // mail subject
                     ->send();
      }
      catch(\Swift_TransportException $exception) {
        //mail sending failed
      }
      catch(\Exception $exception) {
        //some error occured
      }
    }

    public static function sendPackagePaymentSuccessMail($amount,$data,$email,$currency,$invoice_id=0){
      $date = date('d-m-Y', $data);
      $time = date('h:i a', $data);
      $mail = Yii::$app->mailer->compose(['html' => 'package-invoice-success-html'], ['amount' => $amount,'date'=>$date,'time'=>$time,'currency'=>$currency,'invoice_id'=>$invoice_id])
                   ->setFrom([\Yii::$app->params['noReplyBillingEmail'] => \Yii::$app->name . ' ']) // mail from
                    ->setTo($email)
                   ->setSubject('Package invoice generated successfully') // mail subject
                   ->send();
    }

    public static function sendPackagePaymentFailMail($amount,$data,$email,$currency,$message){
      $date = date('d-m-Y', $data);
      $time = date('h:i a', $data);
      $mail = Yii::$app->mailer->compose(['html' => 'package-invoice-fail-html'], ['amount' => $amount,'date'=>$date,'time'=>$time,'currency'=>$currency,'message'=>$message])
                   ->setFrom([\Yii::$app->params['noReplyBillingEmail'] => \Yii::$app->name . ' ']) // mail from
                    ->setTo($email)
                   ->setSubject('Package invoice - Credit card payment failed') // mail subject
                   ->send();
    }

    public static function sendSuccessMail($amount,$data,$email,$currency){
      $date = date('d-m-Y', $data);
      $time = date('h:i a', $data);
      $mail = Yii::$app->mailer->compose(['html' => 'success-mail-pay-html'], ['amount' => $amount,'date'=>$date,'time'=>$time,'currency'=>$currency])
                   ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
                    ->setTo($email)
                   ->setSubject('Your payment has been successfully done') // mail subject
                   ->send();
    }

    public static function sendPayAsYouGoSuccessMail($amount,$data,$email,$currency,$arr_chat_id,$invoice_id=0){
      $date = date('d-m-Y', $data);
      $time = date('h:i a', $data);
      $mail = Yii::$app->mailer->compose(['html' => 'pay-as-you-go-html'], ['amount' => $amount,'date'=>$date,'time'=>$time,'currency'=>$currency,'arr_chat_id'=>$arr_chat_id,'invoice_id'=>$invoice_id])
                   ->setFrom([\Yii::$app->params['noReplyBillingEmail'] => \Yii::$app->name . ' ']) // mail from
                    ->setTo($email)
                   ->setSubject('Your payment has been successfully done') // mail subject
                   ->send();
    }

    public static function sendFailMail($amount,$data,$email,$currency){
      $date = date('d/Y/m', $data);
      $time = date('H:i:s', $data);
      $mail = Yii::$app->mailer->compose(['html' => 'fail-mail-pay-html'], ['amount' => $amount,'date'=>$date,'time'=>$time])
                   ->setFrom([\Yii::$app->params['noReplyBillingEmail'] => \Yii::$app->name . ' ']) // mail from
                    ->setTo($email)
                   ->setSubject('Your credit card payment has been declined') // mail subject
                   ->send();
    }

    public static function sendTestCronEmail($email,$name){
      $mail = Yii::$app->mailer->compose(['html' => 'test-template-html'], ['name' => $name])
                   ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' '])
                    ->setTo($email)
                   ->setSubject('Test Email') // mail subject
                   ->send();
    }
}
