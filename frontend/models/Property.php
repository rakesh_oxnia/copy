<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "property".
 *
 * @property integer $id
 * @property string $property_ref
 * @property string $house_no
 * @property string $street_name
 * @property string $city
 * @property string $country
 * @property string $postcode
 * @property integer $user_id
 * @property integer $no_of_room
 * @property string $created
 * @property string $modified
 */
class Property extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_ref', 'street_name', 'city', 'country', 'postcode', 'no_of_room', /* 'created', 'modified' */], 'required'],
            [['user_id'], 'integer'],
            [['created', 'modified','user_id'], 'safe'],
            [['property_ref', 'city', 'country'], 'string', 'max' => 200],
            [['house_no', 'street_name'], 'string', 'max' => 500],
            [['postcode'], 'string', 'max' => 50],
			['house_no', 'unique'],
			['house_no', 'required','message' => 'This field is required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            //'id' => 'ID',
            'property_ref' => 'Property Referance',
            'house_no' => 'House Number',
            'street_name' => 'Street Name',
            'city' => 'City',
            'country' => 'Country',
            'postcode' => 'Postcode',
            'user_id' => 'User ID',
            'no_of_room' => 'No Of Room',
           // 'created' => 'Created',
           // 'modified' => 'Modified',
        ];
    }
}
