<?php

namespace frontend\models;

use Yii;
use yii\data\SqlDataProvider;

class ChatstatSearch
{

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params, $pagination=true)
    {

        if(isset($params['date_from']) && isset($params['date_to'])) {
            $params = [':from_date' => $params['date_from'], ':to_date' => $params['date_to']];
        } else {
            $params = [':from_date' => date('Y-m-d', strtotime('today - 30 days')), ':to_date' => date('Y-m-d')];
        }

        if($pagination) {
            $pagination = ['pageSize' => 10];
        }
        
        $countsql = "SELECT COUNT(DISTINCT `group`) FROM `chatstats` 
            where record_date BETWEEN :from_date and :to_date";   

        $sql = "SELECT business_name, `group`, sum(invites) as greetings, sum(chats) as total_chats, sum(leads) as total_leads, concat(round((sum(leads)/sum(chats) * 100 ),2),'%') AS chat_to_lead, concat(round((sum(chats)/sum(invites) * 100 ),2),'%') AS conversion_rate, sum(greeting_chats) as total_greeting_chats, sum(visitor_chats) as total_visitor_chats FROM `chatstats` where record_date BETWEEN :from_date and :to_date group by `group`,business_name";

        $count = Yii::$app->db->createCommand($countsql, $params)->queryScalar();

        $provider = new SqlDataProvider([
            'sql' => $sql,
            'params' => $params,
            'totalCount' => $count,
            'pagination' => $pagination,
            'sort' => [
                'attributes' => [
                    'group','greetings','chat_to_lead',
                    'conversion_rate',
                    'total_greeting_chats',
                    'total_visitor_chats',
                    'total_chats',
                    'total_leads',
                ],
            ],
        ]);

        // returns an array of data rows
        //$models = $provider->getModels();
        return $provider;
    }
}
