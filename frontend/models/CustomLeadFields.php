<?php

namespace app\models;

use Yii;
use common\models\Packages;
use common\models\User;
use app\models\CustomLeadFieldsOption;
/**
 * This is the model class for table "apps_countries".
 *
 * @property integer $id
 * @property string $country_code
 * @property string $country_name
 */
class CustomLeadFields extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'custom_leads_fields';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }

    public function getCreatedBy()
    {
      return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getCustomFieldsOptions()
    {
      return $this->hasMany(CustomLeadFieldsOption::className(), ['custom_leads_field_id' => 'id']) ->andOnCondition(['soft_delete' => '0']);
    }

}
