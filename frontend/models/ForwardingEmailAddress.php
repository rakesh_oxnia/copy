<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ForwardingEmailAddress as ForwardingEmailAddressModel;

/**
 * ForwardingEmailAddress represents the model behind the search form about `common\models\ForwardingEmailAddress`.
 */
class ForwardingEmailAddress extends ForwardingEmailAddressModel
{
    public function rules()
    {
        return [
            [['id', 'user_id', 'team', 'created_at', 'updated_at'], 'integer'],
            [['name', 'email'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ForwardingEmailAddressModel::find()->where(['user_id' => Yii::$app->user->identity->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'team' => $this->team,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
