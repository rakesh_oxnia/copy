<?php

namespace app\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "leads".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $chat_id
 * @property string $status
 */
class Leads extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'leads';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'chat_id', 'status'], 'required'],
            [['user_id'], 'integer'],
            [['status'], 'string'],
			[['email'], 'integer'],
            [['chat_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'chat_id' => 'Chat ID',
            'status' => 'Status',
			'email' => 'Email',
        ];
    }
	
	public static function getStatus($chat_id){
		if($lead = Leads::find()->where(['chat_id'=>$chat_id])->one()){
			return $lead->status;
		}
		
		return 'pending';
	}
	
	public static function getMail($chat_id){
		if($lead = Leads::find()->where(['chat_id'=>$chat_id])->one()){
			return $lead->email;
		}
		
		return 'Not Sent';
	}
	
	public static function getTotal($data){
		$total = 0;
		foreach($data as $d){
			$total += $d['count'];
		}
		
		return $total;
	}
	
	public static function getTotal2($data){
		$total = 0;
		foreach($data as $d){
			
		}
	}
	
	public static function getTotal3($data){
		$total = 0;
		foreach($data as $d){
			
		}
	}
	
	public static function getTotal4($data){
		$total = 0;
		foreach($data as $d){
			
		}
	}
	
	public static function getData($group,$user_id,$filter=false,$date_form=false,$date_to=false,$where=false,$group_by=false){
		
		$today = date('Y-m-d');
		$yesterday = date('Y-m-d',strtotime("-1 days"));
		$last7 = date('Y-m-d',strtotime("-7 days"));
		$last30 = date('Y-m-d',strtotime("-30 days"));

		/*
			SELECT COUNT(*), HOUR(FROM_UNIXTIME(c_time)) as Hour, DAY(FROM_UNIXTIME(c_time)) as Day,DATE_FORMAT(FROM_UNIXTIME(c_time), '%d-%m-%Y') AS 'date_formatted' FROM leads WHERE c_time BETWEEN UNIX_TIMESTAMP('2016-05-18 00:00:01') AND UNIX_TIMESTAMP('2016-05-21 09:59:59') GROUP BY HOUR(FROM_UNIXTIME(c_time))
		*/

		$connection=Yii::$app->db; 
		$query = 'SELECT COUNT(*) as count, HOUR(FROM_UNIXTIME(c_time)) as Hour, DAY(FROM_UNIXTIME(c_time)) as Day,DATE_FORMAT(FROM_UNIXTIME(c_time), "%Y-%m-%d") AS "date_formatted",DATE_FORMAT(FROM_UNIXTIME(c_time), "%Y-%m-%d %h:%i:%s") AS "date_hour"
		FROM leads ';
		
		$query .= 'WHERE';
		
		if($where){
			$query .= ' status = "'.$where.'" AND';
		}

		$query .= ' user_group = '.$group.' AND';
		
		$query .= ' c_time BETWEEN UNIX_TIMESTAMP("'.$date_form.' 00:00:01") AND UNIX_TIMESTAMP("'.$date_to.' 23:59:59")';
		
		//echo $query;die;
		
		if($group_by=='hour'){
			$query .=' GROUP BY HOUR(FROM_UNIXTIME(c_time))';
		}else{
			$query .=' GROUP BY DAY(FROM_UNIXTIME(c_time))';
		}


		$lead = $connection->createCommand($query)->queryAll();

		//echo '<pre>';print_r($lead);echo '</pre>';die;
		
		return $lead;

		/* if($lead = Leads::find()->where(['chat_id'=>$chat_id])->one()){
			return $lead->email;
		}
		
		return 'Not Sent'; */
	}
	
	public static function convert($data,$no,$format,$start,$end){
		$start2 = new \DateTime($start);
		
		$startTime = strtotime($start); 
		$endTime = strtotime($end); 

		// Loop between timestamps, 1 day at a time 
		$i = 0;
		$return ="";
		do {
		   $newTime = strtotime('+'.$i++.' days',$startTime); 
		   $res = Leads::in_array_r(date('Y-m-d',$newTime), $data, true);
		   $newTime2 = $newTime * 1000;
		   $return .= "[$newTime2,$res],";
		} while ($newTime <= $endTime);
		
		return $return;

	}
	
	public static function convertHour($data,$start){
		$start2 = new \DateTime($start);
		
		$startTime = strtotime($start); 

		// Loop between timestamps, 1 day at a time 
		$i = 0;
		$i2 = 1;
		$return ="";
		do {
			//echo sprintf("%02d", $i)."=";
		   $res = Leads::in_array_r2($i, $data, true);
		   //$newTime2 = $startTime * $i2 * 1000;
		   $newTime2 = strtotime($start.' '.sprintf("%02d", $i).':00:00')*1000;
		   $return .= "[$newTime2,$res],";
		   $i++;
		   $i2++;
		} while ($i < 24);
		
		return $return;

	}
	
	public static function in_array_r($needle, $haystack, $strict = false) {
		//echo $needle;
		foreach ($haystack as $item) {
			/* if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && 	Leads::in_array_r($needle, $item, $strict))) {
				echo '<pre>';print_r($item);echo '</pre>';
			} */
			if($item['date_formatted']==$needle){
				return $item['count'];
			}
			
		}
		
		return '0';

		//return "hi";
	}
	
	public static function in_array_r2($needle, $haystack, $strict = false) {
		foreach ($haystack as $item) {
			if($item['Hour']==$needle){
				return $item['count'];
			}
			
		}
		
		return '0';

		//return "hi";
	}
	
	/* public static function checkEmail($chat_id){
		if($lead = Leads::find()->where(['chat_id'=>$chat_id])->one()){
			return $lead->email;
		}
		return true;
	} */
	
	 public function sendEmail($user,$lead)
    {   
		
		/*$row = (new \yii\db\Query())
                    ->from('user')
                    ->where(['id' => $user->id])
                    ->one();
         if($row['parent_id'] > 0)
         {
         		
         		$row2 = (new \yii\db\Query())
                    ->from('user')
                    ->where(['id' => $row['parent_id']])
                    ->one();
                
                $email = $row2['email'];
                $agency_name = $row2['agency_name'];
		        $agency_logo = $row2['agency_logo'];
		       
		        if($agency_logo != '')
	            {
	               $base64img = 'http://fubaco.com/frontend/web/logo_images/'.$agency_logo;
	            }
	            else
	            {
	                $base64img = 'http://oxnia.com/signatures/agencyleads-logo.png';
	            }

	            return \Yii::$app->mailer->compose(['html' => 'leadnewagencyclient-html', 'text' => 'leadnewagencyclient-text'], ['user' => $user,'lead'=>$lead,'base64img' => $base64img])
                    ->setFrom($email => $agency_name . ' ']) // mail from
                    ->setTo($user->email)
                    ->setSubject('You have a new lead ') // mail subject
                    ->send();
		        
        }
		else
		{*/

			$data = \Yii::$app->mailer->compose(['html' => 'leadnew-html', 'text' => 'leadnew-text'], ['user' => $user,'lead'=>$lead])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
                    ->setTo($user->email)
                    ->setSubject('You have a new lead ') // mail subject
                    ->send();

			$connection = Yii::$app->getDb();
	        $command = $connection->createCommand('SELECT *
	        FROM  `user_associate` 
	        WHERE  `user_id` ='.$user->id);
	        $bl = $command->queryAll();   

	        if(count($bl) > 0)
	        {
	            $i = 0;
	            foreach ($bl as $value) {
	                \Yii::$app->mailer->compose(['html' => 'leadnew-html', 'text' => 'leadnew-text'], ['user' => $user,'lead'=>$lead])
	                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->name . ' ']) // mail from
	                    ->setTo($value['email'])
	                    ->setSubject('You have a new lead ') // mail subject
	                    ->send();
	            }
	        }
			return $data;
        	//echo '<pre>';print_r($lead);die;
        	
		//}
		
        
    }
	
}
