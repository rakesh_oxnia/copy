<?php

namespace frontend\models;
use common\models\User;
use Yii;
use common\models\Packages;
use app\models\Subscription;

/**
 * This is the model class for table "invoices".
 *
 * @property integer $id
 * @property integer $package_id
 * @property integer $invoice_id
 * @property double $amount
 * @property string $transaction_id
 * @property integer $status
 */
class Invoice extends \yii\db\ActiveRecord
{

  const STATUS_INVOICE_PAID = 1;
  const STATUS_INVOICE_PENDING = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

    public function getPackageDetails()
    {
      return $this->hasOne(Packages::className(), ['id' => 'package_id']);
    }

    public function getUserModel()
    {
      return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getSubscriptionDetail()
    {
      return $this->hasOne(Subscription::className(), ['id' => 'subscription_id']);
    }
}
