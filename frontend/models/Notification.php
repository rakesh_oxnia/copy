<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apps_countries".
 *
 * @property integer $id
 * @property string $country_code
 * @property string $country_name
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          ['notify_email', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'notify_lead' => 'Notify Lead',
            'notify_chat' => 'Notify Chat',
            'notify_email' => 'Notify Email',
        ];
    }
}
