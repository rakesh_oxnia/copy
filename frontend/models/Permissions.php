<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "permissions".
 *
 * @property integer $id
 * @property integer $is_lead
 * @property integer $is_chat
 * @property integer $is_report
 * @property integer $is_faq
 * @property integer $is_package
 * @property integer $is_pay_as_you_go
 * @property integer $is_email
 */
class Permissions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'permissions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['is_lead', 'is_chat', 'is_report', 'is_faq', 'is_package', 'is_pay_as_you_go', 'is_email'], 'required'],
            [['is_lead', 'is_chat', 'is_report', 'is_faq', 'is_package', 'is_pay_as_you_go', 'is_email'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_lead' => 'Is Lead',
            'is_chat' => 'Is Chat',
            'is_report' => 'Is Report',
            'is_faq' => 'Is Faq',
            'is_package' => 'Is Package',
            'is_pay_as_you_go' => 'Is Pay As You Go',
            'is_email' => 'Is Email',
        ];
    }
}
