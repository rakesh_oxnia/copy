<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Payasyougo;

/**
 * PayasyougoSearch represents the model behind the search form about `app\models\Payasyougo`.
 */
class PayasyougoSearch extends Payasyougo
{
	public $businesscategory;
	
    public function rules()
    {
        return [
            //[['id'], 'integer'],
            [['charges_per_lead'], 'number'],
			[['businesscategory'], 'safe']
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Payasyougo::find();
		
		$query->joinWith(['businesscategory']);
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'business_category_id' => $this->business_category_id,
            'charges_per_lead' => $this->charges_per_lead,
			'name' => $this->businesscategory
        ]);

        return $dataProvider;
    }
}
