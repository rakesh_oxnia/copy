<?php
/**
 * File Paypal.php.
 *
 * @author Marcio Camello <marciocamello@outlook.com>
 * @see https://github.com/paypal/rest-api-sdk-php/blob/master/sample/
 * @see https://developer.paypal.com/webapps/developer/applications/accounts
 */

namespace common\components;

use Yii;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\base\Component;

use PayPal\Api\Address;
use PayPal\Api\CreditCard;
use PayPal\Api\Amount;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Api\FundingInstrument;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\RedirectUrls;
use PayPal\Rest\ApiContext;



class paypal extends Component
{

    public function teszt($amount_cost,$description)
    {
		 
        $apiContext = new \PayPal\Rest\ApiContext(
            new \PayPal\Auth\OAuthTokenCredential(
                'ASGKj0deZj4temp_slcaEcgKz0qWzZ-1PhEQD9k4Uhnf1ZfSqfVX47IPVd30rRsvDXFqTIaIusHJYcNH',     // ClientID
                'EPaaekUJo5kftkpGLQWPZgv8XiDZE35Rb3l01rdHZ1sZkBjJcFvKIxSGLODBfS4fXb9Cu9jdgHBPLTLN'      // ClientSecret
            )
        );

			$payer = new Payer();
			$payer->setPaymentMethod("paypal");

			 
			$item1 = new Item();
			$item1->setName($description)
				->setCurrency('USD')
				->setQuantity(1)
				->setPrice($amount_cost);


			$itemList = new ItemList();
			$itemList->setItems(array($item1));

			 
			$details = new Details();
			$details->setShipping(0)
				->setTax(0)
				->setSubtotal($amount_cost);

			 
			$amount = new Amount();
			$amount->setCurrency("USD")
				->setTotal($amount_cost)
				->setDetails($details);

			 
			$transaction = new Transaction();
			$transaction->setAmount($amount)
				->setItemList($itemList)
				->setDescription($description)
				->setInvoiceNumber(uniqid());

			 
			$redirectUrls = new RedirectUrls();
			$redirectUrls->setReturnUrl("http://" . Yii::$app->getRequest()->serverName . Yii::$app->request->baseUrl . '/paypal/success')
				->setCancelUrl("http://" . Yii::$app->getRequest()->serverName . Yii::$app->request->baseUrl . '/paypal/error');

			 
			$payment = new Payment();
			$payment->setIntent("sale")
				->setPayer($payer)
				->setRedirectUrls($redirectUrls)
				->setTransactions(array($transaction));


			// For Sample Purposes Only.
			$request = clone $payment;

			 
			try {
				$payment->create($apiContext);
			} catch (Exception $ex) {
				ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex);
				exit(1);
			}

			 
			$approvalUrl = $payment->getApprovalLink();
			 
			// ResultPrinter::printResult("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", "<a href='$approvalUrl' >$approvalUrl</a>", $request, $payment);

			return $payment;
 
    }
  
}