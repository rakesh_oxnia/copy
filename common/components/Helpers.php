<?php
namespace common\components;

use Yii;
use DateTime;
use DateTimeZone;

class Helpers  {

   public static function change_timezone($time, $timezone) {
        $date = new DateTime($time, new DateTimeZone($timezone));
        $date->setTimezone(new DateTimeZone(Yii::$app->timezone));

        return $date->format('D, m/d/y h:i:s a');
    }

}
