<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
<style type="text/css">	body{background-image:url('http://www.webstack.com.au/wp/wp-content/plugins/myMail/assets/img/bg/climpek.png');background-repeat:repeat-y no-repeat;background-position:top center;}
	#outlook a{padding:0;}
	body{width:100% !important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0;}
	.ExternalClass{width:100%;}
	.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}
	.bodytbl{margin:0;padding:0;width:100% !important;}
	img{outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;display:block;max-width:100%;}
	a img{border:none;}
	p{margin:1em 0;}

	table{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}
	table td{border-collapse:collapse;}
	.o-fix table,.o-fix td{mso-table-lspace:0pt;mso-table-rspace:0pt;}

	body{background-color:#F3F4F4/*Background Color*/;}
	table{font-family:Helvetica,Arial,sans-serif;font-size:12px;color:#585858;}
	td,p{line-height:24px;color:#585858/*Text*/;}
	td,tr{padding:0;}
	ul,ol{margin-top:24px;margin-bottom:24px;}
	li{line-height:24px;}

	a{color:#5ca8cd/*Contrast*/;text-decoration:none;padding:2px 0px;}
	a:link{color:#5ca8cd;}
	a:visited{color:#5ca8cd;}
	a:hover{color:#5ca8cd;}

	.h1{font-family:Helvetica,Arial,sans-serif;font-size:26px;letter-spacing:-1px;margin-bottom:16px;margin-top:2px;line-height:30px;}
	.h2{font-family:Helvetica,Arial,sans-serif;font-size:20px;letter-spacing:0;margin-top:2px;line-height:30px;}
	h1,h2,h3,h4,h5,h6{font-family:Helvetica,Arial,sans-serif;font-weight:normal;}
	h1{font-size:20px;letter-spacing:-1px;margin-bottom:16px;margin-top:4px;line-height:24px;}
	h2{font-size:18px;margin-bottom:12px;margin-top:2px;line-height:24px;}
	h3{font-size:14px;margin-bottom:12px;margin-top:2px;line-height:24px;}
	h4{font-size:14px;font-weight:bold;}
	h5{font-size:12px;}
	h6{font-size:12px;font-weight:bold;}
	h1 a,h2 a,h3 a,h4 a,h5 a,h6 a{color:#5ca8cd;}
	h1 a:active,h2 a:active,h3 a:active,h4 a:active,h5 a:active,h6 a:active{color:#5ca8cd !important;}
	h1 a:visited,h2 a:visited,h3 a:visited,h4 a:visited,h5 a:visited,h6 a:visited{color:#5ca8cd !important;}

	.wrap.header{border-top:1px solid #FEFEFE/*Content Border*/;}
	.wrap.footer{border-bottom:1px solid #FEFEFE;}
	.wrap.body,.wrap.header,.wrap.footer{background-color:#FFFFFF/*Body Background*/;border-right:1px solid #FEFEFE;border-left:1px solid #FEFEFE;}
	.padd{width:24px;}

	.small{font-size:11px;line-height:18px;}
	.separator{border-top:1px dotted #E1E1E1/*Separator Line*/;}
	.btn{margin-top:10px;display:block;}
	.btn img{display:inline;}
	.subline{line-height:18px;font-size:16px;letter-spacing:-1px;}
	table.textbutton td{background:#efefef/*Text Button Background*/;padding:1px 14px 4px 14px;color:#585858;display:block;height:22px;border:1px solid #FEFEFE;vertical-align:top;}
	table.textbutton a{color:#585858;font-size:13px;font-weight:normal;line-height:22px;width:100%;display:inline-block;}
	div.preheader{line-height:0px;font-size:0px;height:0px;display:none !important;display:none;visibility:hidden;}

	@media only screen and (max-device-width: 480px) {
		body{-webkit-text-size-adjust:120% !important;-ms-text-size-adjust:120% !important;}
		table[class=bodytbl] .subline{float:left;}
		table[class=bodytbl] .padd{width:12px !important;}
		table[class=bodytbl] .wrap{width:470px !important;}
		table[class=bodytbl] .wrap table{width:100% !important;}
		table[class=bodytbl] .wrap img{max-width:100% !important;height:auto !important;}
		table[class=bodytbl] .wrap .m-100{width:100% !important;}
		table[class=bodytbl] .m-0{width:0;display:none;}
		table[class=bodytbl] .m-b{display:block;width:100% !important;}
		table[class=bodytbl] .m-b-b{margin-bottom:24px !important;}
		table[class=bodytbl] .m-1-2{max-width:264px !important;}
		table[class=bodytbl] .m-1-3{max-width:168px !important;}
		table[class=bodytbl] .m-1-4{max-width:120px !important;}
		table[class=bodytbl] .m-1-2 img{max-width:264px !important;}
		table[class=bodytbl] .m-1-3 img{max-width:168px !important;}
		table[class=bodytbl] .m-1-4 img{max-width:120px !important;}
	}

	@media only screen and (max-device-width: 320px) {
		table[class=bodytbl] .wrap{width:310px !important;}
	}

.preheader{
		display:none;
	}

</style>

  <table width="600" cellspacing="0" cellpadding="0" class="wrap body">
    <tbody><tr>
      <td height="12" colspan="3"></td>
    </tr>

<tr>
</tr>

    <tr><td width="24" class="padd">&nbsp;</td>
			<td valign="top" align="left">


						<multi label="Body"><div style="background: #f5f5f5; padding-bottom: 2em;">
<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td>&nbsp;</td>
<td style="width: 530px; max-width: 530px;" align="center" width="530">
<table style="max-width: 530px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td colspan="3">
<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td height="20">&nbsp;</td>
</tr>
<tr>
<td align="left">
<table border="0" cellspacing="0" cellpadding="0" align="left">
<tbody>
<tr>

<td width="10">&nbsp;</td>
<td style="margin: 0px; padding: 0px; text-align: left; color: #2b2b2b; line-height: 1.5em; letter-spacing: 0.03em; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 300; vertical-align: top;">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td height="10">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td colspan="3" bgcolor="#ffffff">
<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td style="margin: 0px; padding: 0px; text-align: center; color: #516091; line-height: 1.25em; letter-spacing: 0px; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 28px; font-weight: 400;" align="center">&nbsp;</td>
<td style="margin: 0px; padding: 0px; text-align: center; color: #516091; line-height: 1.25em; letter-spacing: 0px; font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; font-size: 28px; font-weight: 400;" align="center">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td colspan="3" bgcolor="#ffffff">
<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td>&nbsp;</td>
<td style="max-width: 480px;" width="480">
<table style="max-width: 480px;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td width="30">&nbsp;</td>
<td>
<p><img src="https://chat-application.com/frontend/web/images/reallogo.png" align="center" width="360" height="100"></p>
</td>
<td width="30">&nbsp;</td>
</tr>
<tr>
<td width="30">&nbsp;</td>
<td align="left">Hello team,</td>
<td width="30">&nbsp;</td>
</tr>
<tr>
<td width="30">&nbsp;</td>
<td align="left">
<p>Exciting, We have received another new signup to Chat Metrics.<br></p>
<table style="width: 392px;">
<tbody>

	<tr>
		<td style="width: 296px;">Client name : <?= Html::encode($model->your_name) ?></td>
	</tr>

	<tr>
		<td style="width: 296px;">Business name : <?= Html::encode($model->business_name) ?></td>
	</tr>

	<tr>
		<td style="width: 296px;">Email : <?= Html::encode($model->email) ?></td>
	</tr>

	<tr>
		<td style="width: 296px;">Website url : <?= Html::encode($model->website_url) ?></td>
	</tr>
	<?php
		if($model->industry > 0)
		{
	?>

			<tr>
				<td style="width: 296px;">Industry : <?= Html::encode($model->businessCategory->name) ?></td>
			</tr>

	<?php
		}
	?>

	<?php
		if($model->chatPurposeString != '')
		{
	?>

			<tr>
				<td style="width: 296px;">Chat purpose : <?= Html::encode($model->chatPurposeString) ?></td>
			</tr>

	<?php
		}
	?>

	<tr>
		<td style="width: 296px;">Number of employees : <?= Html::encode(User::$empCount[$model->number_of_employees]) ?></td>
	</tr>

	<tr>
		<td style="width: 296px;">Country : <?= Html::encode($model->countryModel->country_name) ?></td>
	</tr>

	<tr>
		<td style="width: 296px;">Phone : <?= Html::encode($model->telephone) ?></td>
	</tr>

	<?php
		if($model->audienceString != '')
		{
	?>

			<tr>
				<td style="width: 296px;">Audience : <?= Html::encode($model->audienceString) ?></td>
			</tr>

	<?php
		}
	?>

	  <tr>
			<td style="width: 296px;">LiveChatInc Group ID : <?= Html::encode($model->user_group) ?></td>
		</tr>

		<tr>
			<td style="width: 296px;">LiveChatInc Group Name : <?= Html::encode($model->business_name) ?></td>
		</tr>

</tbody>
</table>
</td>
<td width="30">&nbsp;</td>
</tr>
<tr>
<td colspan="3" height="20">&nbsp;</td>
</tr>
<tr>
<td colspan="3" height="40">&nbsp;</td>
</tr>
<tr>
<td width="30">&nbsp;</td>
<td align="center" >
<?php if(isset($type)){ ?>

<!-- [if mso]>
		                                                    <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://www.livechatinc.com/blog/irate-customer/" style="height:50px;v-text-anchor:middle;width:200px;margin:0 auto;" arcsize="4%" fillcolor="#EB586F" stroke="false">
		                                                        <w:anchorlock></w:anchorlock>
		                                                        <center style="color:#fff;font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;letter-spacing:0.04em;">READ THE POST</center>
		                                                    </v:roundrect>
		                                                <![endif]-->
														</td>
														</tr>
														</table>
<?php } ?>
</td>
<td width="30">&nbsp;</td>
</tr>
<tr>
<td colspan="3" height="40">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td colspan="3" align="center" bgcolor="#393e46">
<table border="0" width="100%" cellspacing="0" cellpadding="0" align="center" bgcolor="#393e46">
<tbody>

<tr>
<td colspan="13" height="40" style="color : #fff; text-align: center;" algin="center"><a href="http://chat-application.com" style="color : #fff; text-align: center;">Powered by Chat Metrics</a></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>

<?php
	$this->registerCss("
		td{
			font-size: 16px;
			font-weight: 600;
		}
	");
?>
