<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>

<img src="<?= Url::base(true);?>/lc.png" />

Hello <?= $user->username ?>,

<?php if($type){ ?>
Your Username is : <?= $user->email ?>
<?php } ?>
<?php if($type && $password!=''){ ?>
	Your Password is : <?= $password ?>
	<a href="https://chat-application.com"><button>Login Here</button></a>
<?php } ?>

<?php if($password==''){ ?>
	Follow the link below to reset your password:

	<?= Html::a(Html::encode($resetLink), $resetLink) ?>

	Note : This link is expired after one hour

	<?= $resetLink ?>
<?php } ?>
