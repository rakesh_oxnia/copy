<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\User;

class AgencyUserPasswordForm extends Model{
	public $user;
	public $newpass;
	public $repeatnewpass;
	public $emailNotification;

	public function rules(){
		return [
			[['user','newpass','repeatnewpass'],'required'],
			[['emailNotification'], 'integer'],
			['repeatnewpass','compare','compareAttribute'=>'newpass'],
		];
	}

	public function attributeLabels(){
		return [
			'user'=>'User',
			'newpass'=>'New Password',
			'repeatnewpass'=>'Repeat New Password',
			'emailNotification' => 'Notify via Email',
		];
	}
}
