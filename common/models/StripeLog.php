<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "zapier_logs".
 *
 * @property integer $id
 * @property string $stripe_token
 * @property integer $amount
 * @property string $currency
 * @property integer $user_id
 * @property integer $status
 * @property string $log
 * @property integer $created_at
 * @property integer $update_at
 */
class StripeLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stripe_logs';
    }

    // public function behaviors()
    // {
    //     return [
    //         TimestampBehavior::className(),
    //     ];
    // }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'status', 'created_at', 'update_at'], 'integer'],
            [['stripe_token', 'log', 'currency'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User Id',
            'amount' => 'Amount',
            'currency' => 'Currency',
            'status' => 'Status',
            'stripe_token' => 'Stripe Token',
            'log' => 'Data',
            'created_at' => 'Created At',
            'update_at' => 'Update At'
        ];
    }
}
