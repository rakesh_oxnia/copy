<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\models\User;

/**
 * This is the model class for table "zapier_request_logs".
 *
 * @property integer $id
 * @property string $type
 * @property string $api_key
 * @property string $username
 * @property string $password
 * @property integer $authenticated
 * @property integer $user_group
 * @property integer $created_at
 */
class ZapierRequestLogs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zapier_request_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['authenticated', 'user_group', 'created_at'], 'integer'],
            [['type', 'api_key', 'username', 'password'], 'string', 'max' => 255]
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'api_key' => 'Api Key',
            'username' => 'Username',
            'password' => 'Password',
            'authenticated' => 'Authenticated',
            'user_group' => 'User Group',
            'created_at' => 'Created At',
        ];
    }

    public static function getUserBusinessName($user_group,$api_key,$username)
    {
      if(isset($api_key))
      {
        $user = User::find()
        ->where(['auth_key'=>$api_key])
        ->andWhere(['user_group'=>$user_group])
        ->one();
      }else {
        $user = User::find()
        ->where(['user_group'=>$user_group])
        ->andWhere(['username'=>$username])
        ->one();
      }

      if(count($user) > 0)
      {
        if(!is_null($user->business_name) && $user->business_name !='')
        {
            return $user->business_name;
        }else {
          return 'NOT-SET';
        }
      }else {
        return 'NOT-SET';
      }
    }
}
