<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Chat;
use app\models\Leads;
use common\models\UserCategory;
use common\models\User;
/**
 * LeadListSearch represents the model behind the search form about `common\models\Chat`.
 */
class LeadListSearch extends Chat
{
    public $status, $filter, $date_from, $date_to,$category_id,$outlet_id;

    public function rules()
    {
        return [
            [['id', 'leads_id', 'duration', 'user_group', 'is_lead', 'started_timestamp', 'ended_timestamp'], 'integer'],
            [['chat_id', 'name', 'email', 'city', 'region', 'country', 'agent_email', 'agent_name', 'rate', 'chat_start_url', 'tags', 'status'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$pagination)
    {

        $query = Chat::find();

       $user_group = Yii::$app->user->identity->user_group;

        if (Yii::$app->session->has('website_filter')) {
          $user_group = Yii::$app->session->get('website_filter');
        } 

        $website_users = User::find()->select('user_group')->where(['email' => Yii::$app->user->identity->email])->asArray()->all(); 
        if(count($website_users) > 1 && !Yii::$app->session->has('website_filter'))
        {
          $user_group = array_column($website_users, 'user_group');         
        }

        $query->andFilterWhere(['user_group' => $user_group]);  


        if(!isset($params['LeadListSearch']['filter'])){
          $beginOfDay = strtotime('-6 days');
          $endOfDay   = time();
          $query->andFilterWhere(['between', 'ended_timestamp', $beginOfDay, $endOfDay]);
          //echo '<pre>' ; print_r($params); exit;
        }

        $query->orderBy(['ended_timestamp' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => $pagination,
        ]);

        //echo '<pre>' ; print_r($params); exit;

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'leads_id' => $this->leads_id,
            'duration' => $this->duration,
            'user_group' => $this->user_group,
            'is_lead' => $this->is_lead,
            'started_timestamp' => $this->started_timestamp,
            'ended_timestamp' => $this->ended_timestamp,
        ]);

        $query->andFilterWhere(['like', 'chat_id', $this->chat_id])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'region', $this->region])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'agent_email', $this->agent_email])
            ->andFilterWhere(['like', 'agent_name', $this->agent_name])
            ->andFilterWhere(['like', 'rate', $this->rate])
            ->andFilterWhere(['like', 'chat_start_url', $this->chat_start_url])
            ->andFilterWhere(['like', 'tags', $this->tags]);

        $query->orderBy(['ended_timestamp' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => $pagination,
        ]);

        if(isset($params['LeadListSearch']['filter'])){
            if($params['LeadListSearch']['filter'] == 'today'){
              $beginOfDay = strtotime("midnight", time());
              $endOfDay   = strtotime("tomorrow", $beginOfDay) - 1;
            }elseif($params['LeadListSearch']['filter'] == 'yesterday'){
              $beginOfDay = strtotime("midnight", strtotime('-1 days'));
              $endOfDay   = strtotime("tomorrow", $beginOfDay) - 1;
            }elseif($params['LeadListSearch']['filter'] == 'last7'){
              //echo '<pre>' ; print_r($params); exit;
              $beginOfDay = strtotime('-6 days');
              $endOfDay   = time();
            }elseif($params['LeadListSearch']['filter'] == 'last30'){
              $beginOfDay = strtotime('-30 days');
              $endOfDay   = time();
            }elseif($params['LeadListSearch']['filter'] == 'c_range'){
              //echo '<pre>' ; print_r($params); exit;
              $beginOfDay = strtotime($params['LeadListSearch']['date_from']);
              $endOfDay   = strtotime($params['LeadListSearch']['date_to']);
            }else{
              $beginOfDay = strtotime('-6 days');
              $endOfDay   = time();
            }
            $query->andFilterWhere(['between', 'ended_timestamp', $beginOfDay, $endOfDay]);
        }

        $query->orderBy(['ended_timestamp' => SORT_DESC]);

        return $dataProvider;
    }

    public function searchLeads($params,$pagination)
    {
       //echo '<pre>' ; print_r($params); exit;
      $query = Chat::find()->where(['is_lead' => 1]);

      $user_group = Yii::$app->user->identity->user_group;

      if (Yii::$app->session->has('website_filter')) {
        $user_group = Yii::$app->session->get('website_filter');
      } 

      $website_users = User::find()->select('user_group')->where(['email' => Yii::$app->user->identity->email])->asArray()->all(); 
      if(count($website_users) > 1 && !Yii::$app->session->has('website_filter'))
      {
        $user_group = array_column($website_users, 'user_group');         
      }

      $query->andFilterWhere(['user_group' => $user_group]);  

      $outlet_id = '';
      if(Yii::$app->session->has('outlet_filter'))
      {
        $outlet_id = Yii::$app->session->get('outlet_filter');
      }

      if (!empty($params['LeadListSearch']['outlet_id'])){  
        if($params['LeadListSearch']['outlet_id'] == 'all') {
          $outlet_id = '';
          unset($params['LeadListSearch']['outlet_id']);
          Yii::$app->session->remove('outlet_filter');
        } else {
          $outlet_id = $params['LeadListSearch']['outlet_id'];
          Yii::$app->session->set('outlet_filter', $outlet_id);
        }      

      }

      if (Yii::$app->user->identity->role == User::ROLE_OUTLET) {
        $outlet_id = Yii::$app->user->identity->id;
      }    



      if(!empty($outlet_id))
      {
        //if i am outlet then show only my leads
        $query->joinWith('leads')
        ->where(['chat.is_lead' => 1,
        'leads.user_group' => $user_group ,
        'leads.outlet_id' => $outlet_id,
        'leads.is_lead' => 1]);
      } else {
          $query->joinWith('leads')
          ->where(['chat.is_lead' => 1,
          'leads.user_group' => $user_group,
          'leads.is_lead' => 1]);
      }

      // $query->joinWith('leads')
      // ->where(['chat.is_lead' => 1,
      // 'leads.user_group' => Yii::$app->user->identity->user_group,
      // 'leads.is_service_chat' => 0]);

      if(!isset($params['LeadListSearch']['filter'])){
        $beginOfDay = strtotime('-6 days');
        $endOfDay   = time();
        $query->andFilterWhere(['between', 'ended_timestamp', $beginOfDay, $endOfDay]);
        //echo '<pre>' ; print_r($params); exit;
      }

      $query->orderBy(['ended_timestamp' => SORT_DESC]);

      $dataProvider = new ActiveDataProvider([
          'query' => $query,
          'pagination' => $pagination,
      ]);

       //echo $query->createCommand()->getRawSql();exit;
      if (!($this->load($params) && $this->validate())) { 
          return $dataProvider;
      }

      //echo '<pre>' ; print_r($this->ended_timestamp); exit;

      $query->andFilterWhere([
          'id' => $this->id,
          'leads_id' => $this->leads_id,
          'duration' => $this->duration,
          'user_group' => $this->user_group,
          'is_lead' => $this->is_lead,
          'started_timestamp' => $this->started_timestamp,
          'ended_timestamp' => $this->ended_timestamp,
      ]);

    $query->andFilterWhere(['like', 'chat_id', $this->chat_id])
          ->andFilterWhere(['like', 'email', $this->email])
          ->andFilterWhere(['like', 'city', $this->city])
          ->andFilterWhere(['like', 'region', $this->region])
          ->andFilterWhere(['like', 'country', $this->country])
          ->andFilterWhere(['like', 'agent_email', $this->agent_email])
          ->andFilterWhere(['like', 'agent_name', $this->agent_name])
          ->andFilterWhere(['like', 'rate', $this->rate])
          ->andFilterWhere(['like', 'chat_start_url', $this->chat_start_url])
          ->andFilterWhere(['like', 'tags', $this->tags]);

      //echo $query->createCommand()->getRawSql();exit;

      $dataProvider = new ActiveDataProvider([
          'query' => $query,
          'pagination' => $pagination,
      ]);

      if(isset($params['LeadListSearch']['filter'])){
          if($params['LeadListSearch']['filter'] == 'today'){
            $beginOfDay = strtotime("midnight", time());
            $endOfDay   = strtotime("tomorrow", $beginOfDay) - 1;
          }elseif($params['LeadListSearch']['filter'] == 'yesterday'){
            $beginOfDay = strtotime("midnight", strtotime('-1 days'));
            $endOfDay   = strtotime("tomorrow", $beginOfDay) - 1;
          }elseif($params['LeadListSearch']['filter'] == 'last7'){
            //echo '<pre>' ; print_r($params); exit;
            $beginOfDay = strtotime('-6 days');
            $endOfDay   = time();
          }elseif($params['LeadListSearch']['filter'] == 'last30'){
            $beginOfDay = strtotime('-30 days');
            $endOfDay   = time();
          }elseif($params['LeadListSearch']['filter'] == 'c_range'){
            //echo '<pre>' ; print_r($params); exit;
            $beginOfDay = strtotime($params['LeadListSearch']['date_from']);
            $endOfDay   = strtotime($params['LeadListSearch']['date_to']);
          }else{
            $beginOfDay = strtotime('-6 days');
            $endOfDay   = time();
          }
          $query->andFilterWhere(['between', 'ended_timestamp', $beginOfDay, $endOfDay]);
      }
      //echo '<pre>' ; print_r($params); exit;
      if(isset($params['LeadListSearch']['status'])){
        //echo '<pre>' ; print_r($params); exit;
        if(array_key_exists($params['LeadListSearch']['status'], Leads::$statuses)) {
          if(!empty($outlet_id))
          {
            $query->joinWith('leads')
            ->where(['chat.is_lead' => 1,
            'leads.user_group' => $user_group,
            'leads.outlet_id' => $outlet_id,
            'leads.is_lead' => 1,
            'leads.status' => $params['LeadListSearch']['status']])
            ->andFilterWhere(['between','ended_timestamp', $beginOfDay, $endOfDay]);
          }else {
            $query->joinWith('leads')
            ->where(['chat.is_lead' => 1,
            'leads.user_group' => $user_group,
            'leads.is_lead' => 1,
            'leads.status' => $params['LeadListSearch']['status']])
            ->andFilterWhere(['between','ended_timestamp', $beginOfDay, $endOfDay]);
          }
        }
      }

      if(isset($params['LeadListSearch']['category_id'])){
        if(!empty($params['LeadListSearch']['category_id'])){
          $query->joinWith('leads')
          ->where(['chat.is_lead' => 1,
          'leads.is_lead' => 1,
          'leads.user_group' => $user_group ])
          ->andFilterWhere(['leads.category_id' => $params['LeadListSearch']['category_id']]);

          if(!empty($outlet_id)) {
            $query->andFilterWhere(['leads.outlet_id' => $outlet_id]);
          }
          
          if(isset($params['LeadListSearch']['status'])){
            if(array_key_exists($params['LeadListSearch']['status'], Leads::$statuses)){
              $query->andFilterWhere(['leads.status' => $params['LeadListSearch']['status']]);
            }
          }
          $query->andFilterWhere(['between','ended_timestamp', $beginOfDay, $endOfDay]);
        }
      }

      if(isset($params['LeadListSearch']['outlet_id'])){
        //echo '<pre>' ; print_r($params); exit;
        if(!empty($params['LeadListSearch']['outlet_id'])){
          $query->joinWith('leads')
          ->where(['chat.is_lead' => 1,
          'leads.is_lead' => 1,
          'leads.user_group' => $user_group])
          ->andFilterWhere(['leads.outlet_id' => $params['LeadListSearch']['outlet_id']]);
          if(isset($params['LeadListSearch']['category_id'])){
          if(!empty($params['LeadListSearch']['category_id'])){
            $query->andFilterWhere(['leads.category_id' => $params['LeadListSearch']['category_id']]);
          }
          }
          if(isset($params['LeadListSearch']['status'])){
            if(array_key_exists($params['LeadListSearch']['status'], Leads::$statuses)){
              $query->andFilterWhere(['leads.status' => $params['LeadListSearch']['status']]);
            }
          }
          $query->andFilterWhere(['between','ended_timestamp', $beginOfDay, $endOfDay]);
        }
      }

      if(isset($params['LeadListSearch']['name'])){
        if(!empty($params['LeadListSearch']['name'])){
          if(!empty($outlet_id))
          {
            $query->joinWith('leads')
            ->where(['chat.is_lead' => 1,
            'leads.is_lead' => 1,
            'leads.outlet_id' => $outlet_id,
            'leads.user_group' => $user_group ])
            ->andFilterWhere(['like','leads.visitor_name', $params['LeadListSearch']['name']])
            ->andFilterWhere(['between','ended_timestamp', $beginOfDay, $endOfDay]);
          }else {
            $query->joinWith('leads')
            ->where(['chat.is_lead' => 1,
            'leads.is_lead' => 1,
            'leads.user_group' => $user_group ])
            ->andFilterWhere(['like','leads.visitor_name', $params['LeadListSearch']['name']])
            ->andFilterWhere(['between','ended_timestamp', $beginOfDay, $endOfDay]);
          }
        }
      }

      if(isset($params['LeadListSearch']['email'])){
        if(!empty($params['LeadListSearch']['email']))
          {
            if(!empty($outlet_id))
            {
              $query->joinWith('leads')
              ->where(['chat.is_lead' => 1,
              'leads.is_lead' => 1,
              'leads.outlet_id' => $outlet_id,
              'leads.user_group' => $user_group ])
              ->andFilterWhere(['like','leads.visitor_email', $params['LeadListSearch']['email']])
              ->andFilterWhere(['between','ended_timestamp', $beginOfDay, $endOfDay]);
            }else {
              $query->joinWith('leads')
              ->where(['chat.is_lead' => 1,
             'leads.is_lead' => 1,
              'leads.user_group' => $user_group ])
              ->andFilterWhere(['like','leads.visitor_email', $params['LeadListSearch']['email']])
              ->andFilterWhere(['between','ended_timestamp', $beginOfDay, $endOfDay]);
            }
          }
      }

      return $dataProvider;
    }

    public function searchServiceChats($params,$pagination)
    {
      $query = Chat::find()->where(['is_lead' => 1]);


      $user_group = Yii::$app->user->identity->user_group;

      if (Yii::$app->session->has('website_filter')) {
        $user_group = Yii::$app->session->get('website_filter');
      } 

      $website_users = User::find()->select('user_group')->where(['email' => Yii::$app->user->identity->email])->asArray()->all(); 
      if(count($website_users) > 1 && !Yii::$app->session->has('website_filter'))
      {
        $user_group = array_column($website_users, 'user_group');         
      }


      if(Yii::$app->user->identity->user_group > 0) {
        $query->andFilterWhere(['user_group' => $user_group]);   
      }


      $outlet_id = '';
      if(Yii::$app->session->has('outlet_filter'))
      {
        $outlet_id = Yii::$app->session->get('outlet_filter');
      }

      if (!empty($params['LeadListSearch']['outlet_id'])){         
        if($params['LeadListSearch']['outlet_id'] == 'all') {
          $outlet_id = '';
          unset($params['LeadListSearch']['outlet_id']);
          Yii::$app->session->remove('outlet_filter');
        } else {
          $outlet_id = $params['LeadListSearch']['outlet_id'];
          Yii::$app->session->set('outlet_filter', $outlet_id);
        }      

      }

      if (Yii::$app->user->identity->role == User::ROLE_OUTLET) {
        $outlet_id = Yii::$app->user->identity->id;
      }    

      if(!empty($outlet_id))
      {
        //if i am outlet then show only my leads
        $query->joinWith('leads')
        ->where(['chat.is_lead' => 1,
        'leads.user_group' => $user_group ,
        'leads.outlet_id' => $outlet_id,
        'leads.is_service_chat' => 1]);
      } else {
          $query->joinWith('leads')
          ->where(['chat.is_lead' => 1,
          'leads.user_group' => $user_group,
          'leads.is_service_chat' => 1]);
      }

      if(!isset($params['LeadListSearch']['filter'])) {
        $beginOfDay = strtotime('-6 days');
        $endOfDay   = time();
        $query->andFilterWhere(['between', 'ended_timestamp', $beginOfDay, $endOfDay]);
        //echo '<pre>' ; print_r($params); exit;
      }

      $query->orderBy(['ended_timestamp' => SORT_DESC]);

      $dataProvider = new ActiveDataProvider([
          'query' => $query,
          'pagination' => $pagination,
      ]);

       //echo $query->createCommand()->getRawSql();exit;
      if (!($this->load($params) && $this->validate())) {
          return $dataProvider;
      }

      //echo '<pre>' ; print_r($this->ended_timestamp); exit;

      $query->andFilterWhere([
          'id' => $this->id,
          'leads_id' => $this->leads_id,
          'duration' => $this->duration,
          'user_group' => $this->user_group,
          'is_lead' => $this->is_lead,
          'started_timestamp' => $this->started_timestamp,
          'ended_timestamp' => $this->ended_timestamp,
      ]);

    $query->andFilterWhere(['like', 'chat_id', $this->chat_id])
          ->andFilterWhere(['like', 'email', $this->email])
          ->andFilterWhere(['like', 'city', $this->city])
          ->andFilterWhere(['like', 'region', $this->region])
          ->andFilterWhere(['like', 'country', $this->country])
          ->andFilterWhere(['like', 'agent_email', $this->agent_email])
          ->andFilterWhere(['like', 'agent_name', $this->agent_name])
          ->andFilterWhere(['like', 'rate', $this->rate])
          ->andFilterWhere(['like', 'chat_start_url', $this->chat_start_url])
          ->andFilterWhere(['like', 'tags', $this->tags]);

      //echo $query->createCommand()->getRawSql();exit;

      $dataProvider = new ActiveDataProvider([
          'query' => $query,
          'pagination' => $pagination,
      ]);

      if(isset($params['LeadListSearch']['filter'])){
          if($params['LeadListSearch']['filter'] == 'today'){
            $beginOfDay = strtotime("midnight", time());
            $endOfDay   = strtotime("tomorrow", $beginOfDay) - 1;
          }elseif($params['LeadListSearch']['filter'] == 'yesterday'){
            $beginOfDay = strtotime("midnight", strtotime('-1 days'));
            $endOfDay   = strtotime("tomorrow", $beginOfDay) - 1;
          }elseif($params['LeadListSearch']['filter'] == 'last7'){
            //echo '<pre>' ; print_r($params); exit;
            $beginOfDay = strtotime('-6 days');
            $endOfDay   = time();
          }elseif($params['LeadListSearch']['filter'] == 'last30'){
            $beginOfDay = strtotime('-30 days');
            $endOfDay   = time();
          }elseif($params['LeadListSearch']['filter'] == 'c_range'){
            //echo '<pre>' ; print_r($params); exit;
            $beginOfDay = strtotime($params['LeadListSearch']['date_from']);
            $endOfDay   = strtotime($params['LeadListSearch']['date_to']);
          }else{
            $beginOfDay = strtotime('-6 days');
            $endOfDay   = time();
          }
          $query->andFilterWhere(['between', 'ended_timestamp', $beginOfDay, $endOfDay]);
      }
      //echo '<pre>' ; print_r($params); exit;
      if(isset($params['LeadListSearch']['status'])){
        //echo '<pre>' ; print_r($params); exit;
        if(array_key_exists($params['LeadListSearch']['status'], Leads::$statuses))
          $query->joinWith('leads')
          ->where(['chat.is_lead' => 1,
          'leads.user_group' => $user_group,
          'leads.is_service_chat' => 1,
          'leads.status' => $params['LeadListSearch']['status']])
          ->andFilterWhere(['between','ended_timestamp', $beginOfDay, $endOfDay]);
      }

      if(isset($params['LeadListSearch']['category_id'])){
        if(!empty($params['LeadListSearch']['category_id'])){
          $query->joinWith('leads')
          ->where(['chat.is_lead' => 1,
          'leads.is_service_chat' => 1,
          'leads.user_group' => $user_group])
          ->andFilterWhere(['leads.category_id' => $params['LeadListSearch']['category_id']]);

          if(!empty($outlet_id)) {
            $query->andFilterWhere(['leads.outlet_id' => $outlet_id]);
          }          
          

          if(isset($params['LeadListSearch']['status'])){
            if(array_key_exists($params['LeadListSearch']['status'], Leads::$statuses)){
              $query->andFilterWhere(['leads.status' => $params['LeadListSearch']['status']]);
            }
          }
          $query->andFilterWhere(['between','ended_timestamp', $beginOfDay, $endOfDay]);
        }
      }

      if(isset($params['LeadListSearch']['outlet_id'])){
        //echo '<pre>' ; print_r($params); exit;
        if(!empty($params['LeadListSearch']['outlet_id'])){
          $query->joinWith('leads')
          ->where(['chat.is_lead' => 1,
          'leads.is_service_chat' => 1,
          'leads.user_group' => $user_group])
          ->andFilterWhere(['leads.outlet_id' => $params['LeadListSearch']['outlet_id']]);
          if(isset($params['LeadListSearch']['category_id'])){
          if(!empty($params['LeadListSearch']['category_id'])){
            $query->andFilterWhere(['leads.category_id' => $params['LeadListSearch']['category_id']]);
          }
          }
          if(isset($params['LeadListSearch']['status'])){
            if(array_key_exists($params['LeadListSearch']['status'], Leads::$statuses)){
              $query->andFilterWhere(['leads.status' => $params['LeadListSearch']['status']]);
            }
          }
          $query->andFilterWhere(['between','ended_timestamp', $beginOfDay, $endOfDay]);
        }
      }

      if(isset($params['LeadListSearch']['name'])){
        if(!empty($params['LeadListSearch']['name'])){
          $query->joinWith('leads')
          ->where(['chat.is_lead' => 1,
          'leads.is_service_chat' => 1,
          'leads.user_group' => Yii::$app->user->identity->user_group])
          ->andFilterWhere(['like','leads.visitor_name', $params['LeadListSearch']['name']])
          ->andFilterWhere(['between','ended_timestamp', $beginOfDay, $endOfDay]);
        }
      }

      if(isset($params['LeadListSearch']['email'])){
        if(!empty($params['LeadListSearch']['email']))
          {
            $query->joinWith('leads')
            ->where(['chat.is_lead' => 1,
            'leads.is_service_chat' => 1,
            'leads.user_group' => $user_group])
            ->andFilterWhere(['like','leads.visitor_email', $params['LeadListSearch']['email']])
            ->andFilterWhere(['between','ended_timestamp', $beginOfDay, $endOfDay]);
          }
      }

      // if(isset($params['LeadListSearch']['category_name'])){
      //   if(!empty($params['LeadListSearch']['category_name'])){
      //         //$query->with('category');
      //   }
      // }
      //echo '<pre>' ; print_r($params); exit;
      //echo $query->createCommand()->getRawSql();exit;
      return $dataProvider;
    }
}
