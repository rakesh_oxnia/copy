<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "faqs".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $topic
 * @property string $answer
 */
class Faqs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faqs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'topic', 'answer'], 'required'],
            [['user_id'], 'integer'],
            [['answer'], 'string'],
            [['topic'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'topic' => 'Topic',
            'answer' => 'Answer',
        ];
    }
}
