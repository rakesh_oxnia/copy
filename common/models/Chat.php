<?php

namespace common\models;

use Yii;
use app\models\Leads;

/**
 * This is the model class for table "chat".
 *
 * @property integer $id
 * @property string $chat_id
 * @property string $name
 * @property string $email
 * @property string $city
 * @property string $region
 * @property string $country
 * @property string $agent_email
 * @property string $agent_name
 * @property string $rate
 * @property integer $duration
 * @property string $chat_start_url
 * @property integer $user_group
 * @property string $tags
 * @property integer $is_lead
 * @property integer $started_timestamp
 * @property integer $ended_timestamp
 *
 * @property ChatMessage[] $chatMessages
 */
class Chat extends \yii\db\ActiveRecord
{
    const TYPE_MISSED_CHAT = 0;
    const TYPE_CHAT = 1;

    public static $types = [
      self::TYPE_MISSED_CHAT => 'Missed chat',
      self::TYPE_CHAT => 'Chat'
    ];

    public static function tableName()
    {
        return 'chat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['duration', 'user_group', 'is_lead', 'started_timestamp', 'ended_timestamp', 'leads_id', 'type'], 'integer'],
            [['chat_start_url'], 'string'],
            [['chat_id', 'name', 'email', 'city', 'region', 'country', 'agent_email', 'agent_name', 'rate', 'tags'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chat_id' => 'Chat ID',
            'name' => 'Name',
            'email' => 'Email',
            'category_name' => 'Category',
            'city' => 'City',
            'region' => 'State',
            'country' => 'Country',
            'agent_email' => 'Agent Email',
            'agent_name' => 'Agent Name',
            'rate' => 'Rate',
            'duration' => 'Duration',
            'chat_start_url' => 'Chat Start Url',
            'user_group' => 'User Group',
            'tags' => 'Tags',
            'is_lead' => 'Is Lead',
            'started_timestamp' => 'Started Timestamp',
            'ended_timestamp' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChatMessages()
    {
        return $this->hasMany(ChatMessage::className(), ['chat_id' => 'id']);
    }

    public function getLeads()
    {
        return $this->hasOne(Leads::className(), ['id' => 'leads_id']);
    }

    public function hasLead()
    {
      if($this->leads_id != NULL){
        return true;
      }else{
        return false;
      }
    }

    public function getChatMessageArray()
    {
      $messageArray = [];
      foreach ($this->chatMessages as $cm) {
        $messageArray[] = ['author_name' => $cm->author_name, 'text' => $cm->text,'date' => $cm->date, 'timestamp' => $cm->timestamp, 'user_type' => $cm->user_type];
      }
      return $messageArray;
    }
}
