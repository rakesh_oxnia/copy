<?php

namespace common\models;

use Yii;
use common\models\CustomerAPI;

/* Sumo client API Library. Group no 97. */
class CustomerAPI97 implements CustomerAPI 
{
	private $auth_endpoint = "http://223.27.18.38:8080/api/auth/login";
	private $username = "apiuser@chatmetrics.com";
	private $password = "9y4Am+T&";

	private $lead_endpoint = "http://223.27.18.38:8080/api/leads";
	private $token = null;

	public function authenticate() {		
		$url = $this->auth_endpoint;

		$jsonData = array(
		    'username' => $this->username,
		    'password' => $this->password
		);

		$response = $this->post($url, $jsonData);

		if(isset($response['access_token'])) {
			$this->token = $response['access_token'];
			return true;			
		} 

		return false;	
	}

	public function pushNewLead($params) {
		$url = $this->lead_endpoint;

		$jsonData = [[
			 "address" => "", 
			 "address2" => "", 
			 "currentProviderElectricity" => "", 
			 "currentProviderGas" => "", 
			 "customerName" => $post['visitor_name'], 
			 "customerSurname" => $post['visitor_last_name'],
			 "dob" => "", 
			 "email" => $post['visitor_email'], 
			 "gender" => "", 
			 "intrestedIn" => "", 
			 "leadProvider" => "Chat Metrics", 
			 "ipAddress" => "", 
			 "notes" => $post['chat_summary'],
			 "phone" => empty($post['visitor_phone']) ? '1111111111' : $post['visitor_phone'],
			 "phone2" => "", 
			 "postCode" => "", 
			 "preferredContactTime" => $post['preferred_contact_time'],
			 "state" => "", 
			 "suburb" => "", 
			 "title" => ""  
		]];

		$response = $this->post($url, $jsonData, true);

		if(isset($response['status'])) {
			return $response;
		} 

		return false;	
	}

	protected function post($url, $jsonData, $send_token=false) {
		$ch = curl_init($url);
		
		$jsonDataEncoded = json_encode($jsonData);	

		if($send_token) {
			$authorization = "Authorization: Bearer ". $this->token;			
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization)); 
		} else {
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		}

		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);	
		 
		//Execute the request
		$json_response = curl_exec($ch);
		curl_close($ch);

		return json_decode($json_response, true);
	}
}