<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\UserCategory;

/**
 * UserCategorySearch represents the model behind the search form about `common\models\UserCategory`.
 */
class UserCategorySearch extends UserCategory
{
    public function rules()
    {
        return [
            [['id', 'user_id', 'value', 'created_at', 'updated_at', 'parent_id'], 'integer'],
            [['category_name'], 'safe'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        if(Yii::$app->user->identity->role == User::ROLE_ADMIN){
            $query = UserCategory::find()->orderBy(['created_at' => SORT_DESC]);
        }else{
            $query = UserCategory::find()->where(['user_id' => Yii::$app->user->identity->id])->orderBy(['created_at' => SORT_DESC]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'parent_id' => $this->parent_id,
            'value' => $this->value,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'category_name', $this->category_name]);

        return $dataProvider;
    }
}
