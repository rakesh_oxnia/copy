<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_category".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $category_name
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class UserCategory extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'user_category';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at', 'parent_id'], 'integer'],
            ['parent_id', 'exist', 'targetClass' => UserCategory::className(), 'targetAttribute' => 'id'],
            [['value'], 'number'],
            [['category_name'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'category_name' => 'Category Name',
            'value' => ' Lead Value ($)',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'parent_id' => 'Parent Category',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getParent()
    {
        return $this->hasOne(UserCategory::className(), ['id' => 'parent_id']);
    }

    public function getChildren()
    {
        return $this->hasMany(UserCategory::className(), ['parent_id' => 'id']);
    }
}
