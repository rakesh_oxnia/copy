<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "package_user".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $number_of_leads
 * @property integer $number_of_chats
 * @property double $amount
 * @property integer $validity
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class PackageUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package_user';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'number_of_leads', 'number_of_chats', 'validity', 'created_at', 'updated_at'], 'integer'],
            [['amount'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'number_of_leads' => 'Number Of Leads',
            'number_of_chats' => 'Number Of Chats',
            'amount' => 'Amount',
            'validity' => 'Validity',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
