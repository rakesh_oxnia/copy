<?php

namespace common\models;

interface CustomerAPI 
{
	public function authenticate();

	public function pushNewLead($params);
}