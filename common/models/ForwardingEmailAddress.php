<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "forwarding_email_address".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $email
 * @property integer $team
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class ForwardingEmailAddress extends \yii\db\ActiveRecord
{
    const TEAM_ACCOUNTS = 1;
    const TEAM_MARKETING = 2;

    public static $teams = [
        self::TEAM_ACCOUNTS => 'Accounts',
        self::TEAM_MARKETING => 'Marketing',
    ];

    public static function tableName()
    {
        return 'forwarding_email_address';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function rules()
    {
        return [
            [['user_id', 'team', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['email'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'email' => 'Email',
            'team' => 'Team',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getForwardingTeam()
    {
        return $this->hasOne(ForwardingEmailTeam::className(), ['id' => 'team']);
    }
}
