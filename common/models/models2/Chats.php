<?php

class Chats extends Model2
{

  public function get($page, $params=array())
  {
    $paramsString = $this->parseParams($params);
    $url = 'chats';
				$this->encodeParams($params);
				$paramsString = $this->parseParams($params);
    $url .= $paramsString != "" ? "?" . $paramsString : "";

    //print_r($url); exit;
    //$test_url = "chats?r=leads&date_from=2018-01-09&date_to=2018-01-23&tag%5B%5D=lead";
    //$test2_url = "chats?r=leads&date_from=2018-01-09&date_to=2018-01-20&";
    //$url = $test_url;
    //print_r($url); exit;
    //echo "<pre>"; print_r(parent::get($url)); exit;

    return parent::get($url);
  }

  public function getSingleChat($chatId)
  {
    $url = 'chats/' . $chatId;
    return parent::get($url);
  }

  public function getAllTags()
  {
      $url = 'tags';
      return parent::get($url);
  }

  public function updateTags($chatId, $arr_tags)
  {
    /*
    As per the conversion with LiveChat support, it must be passed in tags must be passed in URL.
    Chat Excerpt :
    "Here is an example: https://api.livechatinc.com/v2/chats/PO30E08FQO/tags?tag[]=chatbot&tag[]=test1
    Replace the chat ID and tags accordingly"
    */
    try
    {
      if(is_array($arr_tags) && count($arr_tags) > 0)
      {
        $queryString = '';
        foreach($arr_tags as $tag)
        {
          $queryString .= 'tag[]='.urlencode($tag).'&';
        }

        $queryString = rtrim($queryString, '&');//remove last charact if it is "&" from the string
        //echo $queryString;
        $url = 'chats/' . $chatId . '/tags?'.$queryString;
        return parent::put($url, $arr_tags);
      }
    }
    catch(\Exception $e) {
      //print_r($e);
    }
  }

  public function parseParams($params, $without = "")
  {
    $return = "";
    foreach ($params as $keyParam => $valueParam)
    {
      if (trim($valueParam) != "" && $keyParam != $without)
      {
        $return != "" ? $return.="&" : '';
        $return.=$keyParam . '=' . $valueParam;
      }
    }
    return $return;
  }

}
