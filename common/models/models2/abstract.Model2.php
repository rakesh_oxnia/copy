<?php

use yii\web\NotFoundHttpException;

abstract class Model2
{
	protected $_apiUrl = null;
	protected $_login = null;
	protected $_sessionId = null;
	protected $_returnResponse = true;

	public function __construct()
	{
		$this->_apiUrl = API_URL2;
	}

	protected function getApiUrl()
	{
		return $this->_apiUrl;
	}

	public function setLogin($login)
	{
		$this->_login = $login;
	}

	public function getLogin()
	{
		return $this->_login;
	}

	public function setSessionId($sessionId)
	{
		$this->_sessionId = $sessionId;
	}

	public function getSessionId()
	{
		return $this->_sessionId;
	}

	public function setReturnResponse($bReturn)
	{
		$this->_returnResponse = (bool)$bReturn;
	}

	public function getReturnResponse()
	{
		return $this->_returnResponse;
	}

	public function getCallback()
	{
		if (isset($_REQUEST['callback']))
		{
			return $_REQUEST['callback'];
		}

		return null;
	}

	protected function _buildUrl($url)
	{
		$url = $this->getApiUrl() . $url;

		$callback = $this->getCallback();
		if ($callback)
		{
			$url = $url . '?callback='.$callback;
		}

		return $url;
	}

	protected function _doRequest($method, $url, $vars=null)
	{

		$url = $this->_buildUrl($url);
		switch ($method)
		{
			case 'POST':
			case 'PUT':
				$request = new RestRequest2($url, $method, $vars);
				break;

			case 'GET':
			case 'DELETE':
				$request = new RestRequest2($url, $method);
				break;
		}
		$request->setUsername($this->getLogin());
		$request->setPassword($this->getSessionId());
		$request->execute();

		if ($this->getReturnResponse() === true)
		{
			$response = $request->getResponseInfo();
			$http_code = $response['http_code'];
			//echo '<pre>';print_r($response); exit;
			// Check if response HTTP code starts with `2` (200, 201, 202 codes)
			if (preg_match('/^2/', $http_code) == false)
			{
				throw new NotFoundHttpException(RestUtils2::getStatusCodeMessage($http_code), $http_code);
				//throw new NotFoundHttpException('Server is under maintainence');
			}

			return $request->getResponseBody();
		}
		else
		{
			$response = $request->getResponseInfo();
			g_RestUtils::sendResponse($response['http_code'], $request->getResponseBody());
		}
	}

	public function get($url)
	{
		$result = $this->_doRequest('GET', $url);
		$result = json_decode($result);

		return $result;
	}

	public function post($url, $vars)
	{
		return $this->_doRequest('POST', $url, $vars);
	}

	public function put($url, $vars)
	{
		return $this->_doRequest('PUT', $url, $vars);
	}

	public function delete($url)
	{
		return $this->_doRequest('DELETE', $url);
	}

	public function	encodeParams(&$params)
	{
					array_map("urlencode", 	$params);
	}

}
