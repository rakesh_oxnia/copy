<?php

class Agents extends Model2
{

  public function getAllAgents()
  {
    $url = 'agents';

    return parent::get($url);
  }

  public function getAgent($emailId)
  {
    $url = 'agents/' . $emailId;

    return parent::get($url);
  }
}

?>
