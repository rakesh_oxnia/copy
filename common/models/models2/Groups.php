<?php

class Groups extends Model2
{

  public function testGroup()
  {
    $url = 'groups';

    return parent::get($url);
  }

  public function get($group_id)
  {
    $url = 'groups/' . $group_id;

    return parent::get($url);
  }

  public function addGroup($group_name, $agents)
  {
    //print_r('api function called'); exit;
    $url = 'groups';
    $data = ['name' => $group_name, 'agents' => $agents];
    return parent::post($url, $data);
  }
  
  public function updateGroup($group_id,$group_name, $agents)
  {
    //print_r('api function called'); exit;
    $url = 'groups/'.$group_id;
    $data = ['name' => $group_name, 'agents' => $agents];
    return parent::put($url, $data);
  }

}

?>
