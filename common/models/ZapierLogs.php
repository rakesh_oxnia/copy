<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "zapier_logs".
 *
 * @property integer $id
 * @property integer $lead_id
 * @property string $chat_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $data
 */
class ZapierLogs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zapier_logs';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lead_id', 'created_at', 'updated_at'], 'integer'],
            [['data'], 'string'],
            [['chat_id','email'], 'string', 'max' => 255],
            [['email', 'type'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lead_id' => 'Lead / Service ID',
            'chat_id' => 'Chat ID',
            'created_at' => 'Sent On',
            'updated_at' => 'Update At',
            'data' => 'Data',
            'email' => 'Email',
            'type' => 'Type'
        ];
    }
}
