<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "chat_message".
 *
 * @property integer $id
 * @property integer $chat_id
 * @property string $author_name
 * @property string $text
 * @property string $date
 * @property integer $timestamp
 * @property string $user_type
 *
 * @property Chat $chat
 */
class ChatMessage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chat_id', 'timestamp'], 'integer'],
            [['text'], 'string'],
            [['author_name', 'date', 'user_type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chat_id' => 'Chat ID',
            'author_name' => 'Author Name',
            'text' => 'Text',
            'date' => 'Date',
            'timestamp' => 'Timestamp',
            'user_type' => 'User Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat()
    {
        return $this->hasOne(Chat::className(), ['id' => 'chat_id']);
    }
}
