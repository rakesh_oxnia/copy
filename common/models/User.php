<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\models\BusinessCategory;
use app\models\AppsCountries;
use app\models\Subscription;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */

class User extends ActiveRecord implements IdentityInterface
{
    const ADMIN_ROLE = 'admin';
    const CLIENT_ROLE = 'client';
    const FRANCHISE_ROLE = 'franchise';
    const OUTLET_ROLE = 'outlet';
    const SERVICE_ACCOUNT_ROLE = 'service_account';
    const AGENCY_ROLE = 'agency';
    const SUBADMIN_ROLE = 'sub_admin';
    const ASSIST_ROLE = 'assit';

    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 5;
    const STATUS_ACTIVE = 10;
    const ROLE_USER = 10;
    const ROLE_ADMIN = 50;
    const ROLE_AGENCY = 30;
    const ROLE_FRANCHISE = 40;
    const ROLE_OUTLET = 20;
    const ROLE_SERVICE_ACCOUNT = 60;
    const ROLE_SUBADMIN = 70;
    const ROLE_ASSIST = 80;

    const EMP_COUNT_10 = 1;
    const EMP_COUNT_100 = 5;
    const EMP_COUNT_100_PLUS = 10;

    public static $empCount = [
      self::EMP_COUNT_10 => '1 - 10 Employees',
      self::EMP_COUNT_100 => '10 - 100 Employees',
      self::EMP_COUNT_100_PLUS => '100 plus Employees',
    ];

    public static $statuses = [
      self::STATUS_DELETED => 'Deleted',
      self::STATUS_INACTIVE => 'Inactive',
      self::STATUS_ACTIVE => 'Active',
    ];

    //https://stripe.com/docs/currencies#minimum-and-maximum-charge-amounts
    public static $stripeUserCurrencies = [
      'AUD' => ['name'=>'Australian Dollar', 'minimum_allowed_charge'=> 0.5],
      'BRL' => ['name'=>'Brazilian Real', 'minimum_allowed_charge'=> 0.5],
      'CAD' => ['name'=>'Canadian Dollar', 'minimum_allowed_charge'=> 0.5],
      'DKK' => ['name'=>'Danish Krone', 'minimum_allowed_charge'=> 2.5],
      'EUR' => ['name'=>'Euro', 'minimum_allowed_charge'=> 0.5],
      'HKD' => ['name'=>'Hong Kong Dollar', 'minimum_allowed_charge'=> 4],
      'MXN' => ['name'=>'Mexican Peso', 'minimum_allowed_charge'=> 10],
      'NOK' => ['name'=>'Norwegian Krone', 'minimum_allowed_charge'=> 3],
      'NZD' => ['name'=>'New Zealand Dollar', 'minimum_allowed_charge'=> 0.5],
      'GBP' => ['name'=>'Pound Sterling', 'minimum_allowed_charge'=> 0.3],
      'SGD' => ['name'=>'Singapore Dollar', 'minimum_allowed_charge'=> 0.5],
      'SEK' => ['name'=>'Swedish Krona', 'minimum_allowed_charge'=> 3],
      'USD' => ['name'=>'U.S. Dollar', 'minimum_allowed_charge'=> 0.5]
    ];

    const SCENARIO_ADD_WEBSITE = 'add_website';

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADD_WEBSITE] = ['user_group', 'website_url'];
        return $scenarios;
    }

	//public $USER = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [	[[ 'email'], 'required'], 
            [['email'], 'email'],
            [['name', 'website_url', 'business_name'], 'string', 'max' => 255],
            [['chat_purpose_e_commerce'], 'integer'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'except' => ['add_website','update_password','update_user'], 'message' => 'This email address has already been taken.'],
            ['website_url','url', 'defaultScheme' => 'http', 'message' => 'Website url is not valid (example: http://www.chat-application.com)'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],

			//['role', 'default', 'value' => self::ROLE_USER],
            //['role', 'in', 'range' => [self::ROLE_USER]],//self::STATUS_ACTIV,
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

	public static function findByEmail($username)
    {
        return static::findOne(['email' => $username, 'status' => self::STATUS_ACTIVE]);
    }

	public static function findById($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    //https://stripe.com/docs/currencies#minimum-and-maximum-charge-amounts
    public static function validMinimumStripeCharge($charge, $currency)
    {
        $valid_minimum_limit = false;
        if(array_key_exists($currency, self::$stripeUserCurrencies))
        {
          if($charge >= self::$stripeUserCurrencies[$currency]['minimum_allowed_charge'])
          {
            $valid_minimum_limit = true;
          }
        }

        return $valid_minimum_limit;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function getParent()
    {
        return $this->hasOne(User::className(), ['id' => 'parent_id']);
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->username = $this->email;
        if($insert) {
          $this->tracker_id = rand(00000000,99999999);
          do {
            $this->tracker_id = rand(00000000,99999999);
          } while (self::findOne(['tracker_id' => $this->tracker_id]));

        }
        return true;
    }

  	public static function isUserAdmin($username) {
  		if(static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE,'role'=>self::ROLE_ADMIN])){
  			return true;
  		}

  		return false;
  	}

    /**
     * Check user is angency
    */

    public static function isUserAgency($username){
        if(static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE,'role'=>self::ROLE_AGENCY])){
            return true;
        }

        return false;
    }

  	public static function getCount(){
  		return User::find()->where(['role'=>10])->count();
  	}

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

  	public static function getAllUsers(){
  		$userData	= [];
  		$users	= User::find()->all();
  		if( !empty( $users ) ){
  			foreach($users as $user){
  				$userData[$user['id']] = $user['username'];
  			}
  		}

  		return $userData;
  	}

    public static function loginAsAgent($id)
    {
      $agentUser = User::findOne($id);
      if($agentUser) {
        return Yii::$app->user->login($agentUser, 0);
      }
      return false;
    }

    public static function loginAsAdmin($id)
    {
      $adminUser = User::findOne($id);
      if($adminUser){
        return Yii::$app->user->login($adminUser, 0);
      }
      return false;
    }

    public function setDefaultValues()
    {
      $this->send_transcript = 0;
      $this->parent_id = 0;
      $this->agency_name = '';
      $this->agency_logo = '';
      $this->theme_color = '';
      $this->welcome_message = '';
      $this->your_name = '';
      $this->company_name = '';
      $this->company_url = '';
      $this->telephone = '';
      $this->chat_purpose_sales = 0;
      $this->chat_purpose_support = 0;
      $this->chat_purpose_e_commerce = 0;
      $this->industry = 0;
      $this->number_of_employees = 0;
      $this->country = 0;
      $this->b2b = 0;
      $this->b2c = 0;
      $this->internal_use = 0;
      $this->overview = '';
      $this->timezone = 0;
      $this->website_javascript = '';
      $this->email_for_leads = '';
      $this->email_for_chats = '';
      $this->stripeToken = '';
    }

    public function loadModelData($model)
    {
      $this->username = $model->username;
      //$this->user_group = $model->user_group;
      $this->email = $model->email;
      $this->website_url = $model->website_url;
      $this->per_lead_cost = $model->per_lead_cost;
      $this->send_transcript = $model->send_transcript;
      $this->disable_payment_status = $model->disable_payment_status;
      $this->status = $model->status;
      $this->name = $model->name;
      $this->free_access = $model->free_access;
    }

    public function notifySignupEmail()
    {
      \Yii::$app->mailer->compose(['html' => 'newSignupNotification-html', 'text' => 'newSignupNotification-text'], ['model' => $this])
                        ->setFrom([\Yii::$app->params['supportEmail'] => 'Chat Metrics'])
                        ->setTo('cyr.freaxxx@gmail.com')
                        ->setSubject('New Client Signup') // user create mail subject
                        ->send();
    }

    public function getBusinessCategory()
    {
      return $this->hasOne(BusinessCategory::className(), ['id' => 'industry']);
    }

    public function getCountryModel()
    {
      return $this->hasOne(AppsCountries::className(), ['id' => 'country']);
    }
    
    public function getAllocatedPackage()
    {
      return $this->hasOne(Subscription::className(), ['user_id' => 'id']);
    }

    public function getChatPurposeString()
    {
      $str = '';
      if($this->chat_purpose_sales){
        $str .= 'Lead Generation';
      }
      if($this->chat_purpose_support){
        if($str != ''){
          $str .= ', ' . 'Support/Service';
        }else{
          $str .= 'Support/Service';
        }
      }
      if($this->chat_purpose_e_commerce){
        if($str != ''){
          $str .= ', ' . 'E-commerce';
        }else{
          $str .= 'E-commerce';
        }
      }
      return $str;
   }

    public function getAudienceString()
    {
      $str = '';
      if($this->b2b){
        $str .= 'B2B';
      }
      if($this->b2c){
        if($str != ''){
          $str .= ', ' . 'B2C';
        }else{
          $str .= 'B2C';
        }
      }
      if($this->internal_use){
        if($str != ''){
          $str .= ', ' . 'Internal Use';
        }else{
          $str .= 'Internal Use';
        }
      }
      return $str;
    }
    
    public static function showSubcription(){
      if(Yii::$app->user->identity->free_access==1){
        return $subscription_type = 'free_access';
      }else{
        $check_subscribe = Subscription::findOne(['user_id'=>Yii::$app->user->identity->id]);
        if($check_subscribe)
        {
        return  $subscription_type = $check_subscribe->package_name;
        }else{
          return $subscription_type = 'pay_as_you_go';
        }
      }
    }

}
