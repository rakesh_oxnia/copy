<?php

namespace common\models;
use common\models\LiveChat_API;
use yii\helpers\json;
use yii\widgets\Pjax;


use Yii;

/**
 * This is the model class for table "packages".
 *
 * @property integer $id
 * @property string $package_name
 * @property integer $number_of_leads
 * @property integer $number_of_chats
 * @property string $amount
 */
class Packages extends \yii\db\ActiveRecord
{
  //  public $package_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'packages';
    }



    /**
     * @inheritdoc
     */

    public function rules()
    {

        return [
            [['package_name','number_of_leads', 'amount'], 'required'],
          //  ['package_name','unique','message'=> 'Package Name Must be unique'],
            [['number_of_leads' ], 'integer'],
            [['amount'], 'number'],
        //    [['package_name'], 'required','on' => 'Check_package' ],
          //  [['package_name'],'string', 'max' => 255]
          //  ['package_name', 'validatepackage'],
          //[['package_name'], 'unique'],
        // [[ 'package_name'],'required', 'on' => 'packages_name'],
          //['package_name', 'validatepackage', 'on' => 'packages_name' ,'skipOnEmpty' => false ],
          ];
        }

        // const SCENARIO_CHECK_PACKAGE = 'Check_package';
        //
        // public function scenarios()
        // {
        //     $scenarios = parent::scenarios();
        //     $scenarios[self::SCENARIO_CHECK_PACKAGE] = ['package_name'];
        //     return $scenarios;
        // }


        //   public function scenarios(){
        //   $scenarios = parent::scenarios();
        //   $scenarios['packages_name'] = ['package_name'];//Scenario Values Only Accepted
        //   return $scenarios;
        // }
   //      public function validatepackage($attribute,$params){
   //        $package_name = $this->attribute;
   //        $connection = Yii::$app->db;
   //        $query="select * from packages where package_name = '$package_name'";
   //        $package_name=$connection->createCommand($query)->queryAll();
   //        if($package_name)
   //        {
   //          $this->addError($attribute,"There is allready package with name of,".$package_name.".");
   //        }
   //        if (!$this->hasErrors()) {
   //     echo '<pre>'; print_r($params); die;
   // }
   //
   //      }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'package_name' => 'Package Name',
            'number_of_leads' => 'Number Of Leads',
            'amount' => 'Amount',
            'currency' => 'Currency',
            'interval' => 'Interval',
            'created_at' => 'Date',
        ];
    }

	// Fetchs free leads for the chats page
	public static function getFreeLeads($data){
		error_reporting(0);
		$freeLeadsChatids = [];

		$data['tag%5B%5D'] = 'lead';
		$API = new LiveChat_API();
		$leads = $API->chats->get(1, $data);
		$pages = $leads->pages;
		$data['page'] = $pages;

		// Get record count for last page.
		$API = new LiveChat_API();
		$lastpagechats 		= $API->chats->get($pages, $data);
		$lastchatpagecount  = count($lastpagechats->chats);

		if($lastchatpagecount < 5){
			$data['page'] = $pages - 1;
			$API = new LiveChat_API();
			$lastpagechats 		= $API->chats->get(($pages - 1), $data);
			$leadchats = array_reverse($lastpagechats->chats);
			foreach($leadchats as $key => $chat){
				if($key < 5 && count($freeLeadsChatids) <= 5){
					$freeLeadsChatids[] = $chat->id;
				}else{
					break;
				}
			}
		}else{

			$leadchats = array_reverse($lastpagechats->chats);
			foreach($leadchats as $key => $chat){
				if($key < 5){
					$freeLeadsChatids[] = $chat->id;
				}else{
					break;
				}
			}
		}

		return $freeLeadsChatids;

	}
}
