<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "admin_setting".
 *
 * @property integer $id
 * @property string $manager_email
 * @property string $login_iframe_tag
 * @property string $login_bg_color
 * @property string $help_me_iframe_tag
 */
class AdminSetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_setting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login_iframe_tag', 'help_me_iframe_tag'], 'string'],
            [['manager_email', 'login_bg_color'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'manager_email' => 'Manager Email',
            'login_iframe_tag' => 'Login Iframe Tag',
            'login_bg_color' => 'Login Bg Color',
            'help_me_iframe_tag' => 'Help Me Iframe Tag',
        ];
    }
}
