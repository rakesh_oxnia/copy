<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "email_log".
 *
 * @property integer $id
 * @property integer $leads_id
 * @property string $chat_id
 * @property string $sent_to
 * @property integer $sent_via
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Leads $leads
 */
class EmailLog extends \yii\db\ActiveRecord
{
    const VIA_LEAD_SENDER = 1;
    const VIA_FORWARDING_EMAIL = 2;
    const VIA_QUICK_SEND = 3;
    const VIA_SEND_TO_ME = 4;

    public static $via = [
      self::VIA_LEAD_SENDER => 'Lead Sender',
      self::VIA_FORWARDING_EMAIL => 'Forwarding Email',
      self::VIA_QUICK_SEND => 'Quick Send',
      self::VIA_SEND_TO_ME => 'Send to Me',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_log';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leads_id', 'sent_via', 'created_at', 'updated_at'], 'integer'],
            [['sent_to', 'web_url'], 'string'],
            [['chat_id'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leads_id' => 'Leads ID',
            'chat_id' => 'Chat ID',
            'sent_to' => 'Sent To',
            'web_url' => 'Web Url',
            'sent_via' => 'Sent Via',
            'created_at' => 'Email Sent At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeads()
    {
        return $this->hasOne(Leads::className(), ['id' => 'leads_id']);
    }
}
