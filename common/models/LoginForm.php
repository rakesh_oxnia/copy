<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
	public $email;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],
			['email', 'email'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect email or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

	public function switchuser( $id )
    {

        return Yii::$app->user->login(User::findById($id), $this->rememberMe ? 3600 * 24 * 30 : 0);

    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);

        }

        return $this->_user;
    }

    public static function userAssitLogin()
    {
        if(Yii::$app->user->identity->role == User::ROLE_ASSIST) {     
            $user_id = Yii::$app->user->identity->id;
            $parent = User::findOne(['user_group' => Yii::$app->user->identity->user_group]);

            if(!empty(Yii::$app->user->identity->outlet)) {
              $parent = User::findOne(Yii::$app->user->identity->outlet);
            } 
            
            yii::$app->user->login($parent); 
            Yii::$app->session->set('user_assist', $user_id);              
        }

    }
    
    public static function userAssitLogout($params = array())
    {
        if(Yii::$app->session->has('user_assist')) {           
          $user = User::findOne(Yii::$app->session->get('user_assist'));  
          Yii::$app->session->remove('user_assist');             
          yii::$app->user->login($user);
          if(empty(Yii::$app->user->identity->multisiteaccess)) {
            unset($params['website_users']);
            unset($params['outlets']);
          }                                          
        }
        return $params;
    }
}
