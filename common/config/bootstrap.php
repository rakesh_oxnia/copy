<?php
ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 0);
ini_set('error_reporting', 'E_ALL | E_DEPRECATED' );


Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
