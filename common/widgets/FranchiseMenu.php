<?php
namespace common\widgets;

use frontend\assets\AppAsset;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\models\User;
use app\models\Permissions;
use yii\helpers\Url;

class FranchiseMenu extends Widget{

public $html;

public function init(){
		parent::init();
		$model = Permissions::findOne(1);
		$this->html = ''; ?>
 <ul class="nav nav-pills nav-stacked">

      <li class="nav-item">
          <a class="nav-link"  href="<?php echo Url::to(['/site/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
              <span class="ks-icon la la-dashboard"></span>
              <span>Dashboard</span>
          </a>
      </li>


			<li class="nav-item">
          <a class="nav-link"  href="<?php echo Url::to(['/lead-list/leads']);?>" role="button" aria-haspopup="true" aria-expanded="false">
              <span class="ks-icon la la-briefcase"></span>
              <span>Leads</span>
          </a>
      </li>

			<li class="nav-item">
					<a class="nav-link"  href="<?php echo Url::to(['/lead-list/service-chats']);?>" role="button" aria-haspopup="true" aria-expanded="false">
							<span class="ks-icon la la-commenting"></span>
							<span>Service chats</span>
					</a>
			</li>
			
			<li class="nav-item">
									<a class="nav-link"  href="<?php echo Url::to(['/invoice/invoice-listing']);?>" role="button" aria-haspopup="true" aria-expanded="false">
											<span class="ks-icon la la-cubes"></span>
											<span>Invoice</span>
									</a>
				</li>
			
			<!--<li class="nav-item">-->
			<!--		<a class="nav-link"  href="<?php //echo Url::to(['/service-chat-email/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">-->
			<!--			<span class="ks-icon la la-briefcase"></span>-->
			<!--			<span>Service Chats Emails</span>-->
			<!--		</a>-->
		 <!--</li>-->

		 <?php
			if(Yii::$app->user->identity->role != User::ROLE_OUTLET){
		 ?>
				<li class="nav-item">
						<a class="nav-link"  href="<?php echo Url::to(['/lead-list/chats']);?>" role="button" aria-haspopup="true" aria-expanded="false">
								<span class="ks-icon la la-comments"></span>
								<span>Chat Archives</span>
						</a>
				</li>
		<?php
			}
		?>

			<!--<li class="nav-item">-->
   <!--       <a class="nav-link"  href="<?php //echo Url::to(['/chat/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">-->
   <!--           <span class="ks-icon la la-comments"></span>-->
   <!--           <span>Chats</span>-->
   <!--       </a>-->
   <!--   </li>-->

			<li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
              <span class="ks-icon la la-desktop"></span>
              <span>Reports</span>
          </a>
          <div class="dropdown-menu">
              <a class="dropdown-item" href="<?php echo Url::to(['/reports/stats']);?>">Total Chats</a>
							<a class="dropdown-item" href="<?php echo Url::to(['/leads/total']);?>">Total Leads</a>
							<a class="dropdown-item" href="<?php echo Url::to(['/goals/index']);?>">Goals</a>
          </div>
      </li>

			<?php
				$showSubcription = User::showSubcription();
				if($showSubcription == 'pay_as_you_go'){
			?>
			<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
							<span class="ks-icon la la-money"></span>
							<span>Billing</span>
					</a>

					<div class="dropdown-menu">
							<a class="dropdown-item" href="<?php echo Url::to(['/packages/subscription']);?>">Subscriptions</a>
							<a class="dropdown-item" href="<?php echo Url::to(['/invoice/index']);?>">Invoices</a>
					</div>
				</li>
			<?php
				}
			?>

			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
					<span class="ks-icon la la-tasks"></span>
					<span>Manage Outlets</span>
				</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="<?php echo Url::to(['/user/outlet-index', 'id' => Yii::$app->user->identity->id]);?>">Outlet Listing</a>
					<a class="dropdown-item" href="<?php echo Url::to(['/user/outlet-create', 'id' => Yii::$app->user->identity->id]);?>">Create Outlet</a>
				</div>
			</li>

			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
					<span class="ks-icon la la-tasks"></span>
					<span>Manage Categories</span>
				</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="<?php echo Url::to(['/user-category/index']);?>">Category Listing</a>
					<a class="dropdown-item" href="<?php echo Url::to(['/user-category/create']);?>">Create Category</a>
				</div>
			</li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-users"></span>
                    <span>Access Control</span>
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?php echo Url::to(['/user-access/index']);?>">Users</a>
                    <a class="dropdown-item" href="<?php echo Url::to(['/role/index']);?>">Roles</a>
                </div>
             </li>
            
			<li class="nav-item">
          <a class="nav-link"  href="<?php echo Url::to(['/settings/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
              <span class="ks-icon la la-wrench"></span>
              <span>Settings</span>
          </a>
      </li>


  </ul>

<?php
}

	public function run(){
		return Html::encode($this->html);
	}
}
?>
