<?php
namespace common\widgets;

use frontend\assets\AppAsset;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;

class CustomMenu extends Widget{

public $html;

public function init(){
		parent::init();
		$this->html = ''; ?>

		<ul class="nav nav-pills nav-stacked">
            
            <li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/site/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-dashboard"></span>
                    <span>Dashboard</span>
                </a>
            </li>            

            <?php if (Yii::$app->user->can('manageClients') || Yii::$app->user->can('manageFranchises')) { ?>
			<li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-users"></span>
                    <span>Manage Client</span>
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?php echo Url::to(['/user/client-franchise-index']);?>">Client Listing</a>
					<a class="dropdown-item" href="<?php echo Url::to(['/user/create']);?>">Create Client</a>
					<a class="dropdown-item" href="<?php echo Url::to(['/user/franchise-create']);?>">Create Franchise</a>
                </div>
            </li>
            <?php } ?>

            <?php if (Yii::$app->user->can('manageCategories')) { ?>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
					<span class="ks-icon la la-tasks"></span>
					<span>Manage Categories</span>
				</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="<?php echo Url::to(['/user-category/index']);?>">Category Listing</a>
					<a class="dropdown-item" href="<?php echo Url::to(['/user-category/create']);?>">Create Category</a>
				</div>
			</li>
             <?php } ?>

            <?php if (Yii::$app->user->can('manageFeedbacks')) { ?> 
			<li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-comment"></span>
                    <span>Feedback</span>
                </a>
				<div class="dropdown-menu">
                    <a class="dropdown-item" href="<?php echo Url::to(['/feedback/index']);?>">List</a>
					<a class="dropdown-item" href="<?php echo Url::to(['/feedback/add']);?>">Add</a>
                </div>
            </li>
            <?php } ?>

            <?php if (Yii::$app->user->can('manageEmailLogs')) { ?> 
            <li class="nav-item">
            <a class="nav-link"  href="<?php echo Url::to(['/email-log/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
            <span class="ks-icon la la-database"></span>
            <span>Email Logs</span>
            </a>
            </li>
            <?php } ?>

            <?php if (Yii::$app->user->can('manageZapierLogs')) { ?> 
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                  <span class="ks-icon la la-database"></span>
                  <span>Integration</span>
              </a>
              <div class="dropdown-menu">
                   <a class="dropdown-item" href="<?php echo Url::to(['/zapier-request-logs/index']);?>">Integration Request</a>
            					 <a class="dropdown-item" href="<?php echo Url::to(['/zapier-logs/index']);?>">Integration Log</a>
              </div>
            </li>
            <?php } ?>

            <?php if (Yii::$app->user->can('managePackages')) { ?> 
			<li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/packages/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-cubes"></span>
                    <span>Packages</span>
                </a>
            </li>
            <?php } ?>

            <?php if (Yii::$app->user->can(User::SUBADMIN_ROLE) && Yii::$app->user->can('manageInvoices')) { ?> 
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-comment"></span>
                    <span>Invoice</span>
                </a>
								<div class="dropdown-menu">
                    <a class="dropdown-item" href="<?php echo Url::to(['/invoice/invoice-listing']);?>">Package invoice</a>
					<!-- <a class="dropdown-item" href="<?php echo Url::to(['/invoice/failed-listing']);?>">Failed Invoice</a> -->
										<a class="dropdown-item" href="<?php echo Url::to(['/invoice/index']);?>">Pay as you go Invoice</a>
                </div>
             </li>
            <?php } ?>

            <?php if (Yii::$app->user->can(User::SUBADMIN_ROLE) && Yii::$app->user->can('viewReport')) { ?> 
            <li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/chatstat/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-th"></span>
                    <span>Chat Stats Report</span>
                </a>
            </li>
            <?php } ?>

            <?php if (Yii::$app->user->can('manageBusinessCategories')) { ?> 
            <li class="nav-item">
			<li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/business-category/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-qrcode"></span>
                    <span>Business Category</span>
                </a>
            </li>
            <?php } ?>

            <?php if (Yii::$app->user->can('manageCoupons')) { ?> 
			<li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/coupons/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-calculator"></span>
                    <span>Coupons</span>
                </a>
            </li>
            <?php } ?>

            <?php if (Yii::$app->user->can('manageLeads')) { ?> 
            <li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/lead-list/leads']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-briefcase"></span>
                    <span>Leads</span>
                </a>
             </li>
             <?php } ?>

            <?php if (Yii::$app->user->can('manageServiceChats')) { ?> 
            <li class="nav-item">
               <a class="nav-link"  href="<?php echo Url::to(['/lead-list/service-chats']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                   <span class="ks-icon la la-commenting"></span>
                   <span>Service chats</span>
               </a>
           </li>
           <?php } ?>

           <?php if (Yii::$app->user->can('manageChats')) { ?> 
            <li class="nav-item">
                 <a class="nav-link"  href="<?php echo Url::to(['/lead-list/chats']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                         <span class="ks-icon la la-comments"></span>
                         <span>Chat Archives</span>
                 </a>
            </li>
            <?php } ?>

            <?php if (Yii::$app->user->can('manageForwardingEmails')) { ?> 
            <li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/forwarding-email-address/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-envelope"></span>
                    <span>Forwarding emails</span>
                </a>
            </li>
            <?php } ?>

           <?php if (!Yii::$app->user->can(User::SUBADMIN_ROLE) && Yii::$app->user->can('viewReport')) { ?> 
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="ks-icon la la-desktop"></span>
                        <span>Reports</span>
                    </a>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="<?php echo Url::to(['/reports/stats']);?>">Total Chats</a>
                                <a class="dropdown-item" href="<?php echo Url::to(['/leads/total']);?>">Total Leads</a>
                                <a class="dropdown-item" href="<?php echo Url::to(['/goals/index']);?>">Goals</a>
                    </div>
                </li>
            <?php } ?>  

            <?php 
            $roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->parent_id);
            if (array_key_exists(User::CLIENT_ROLE,$roles) && Yii::$app->user->can('manageInvoices')) { 
                $parent = User::findOne(['user_group' => Yii::$app->user->identity->user_group]);
                if(empty($parent->parent_id)) {
            ?> 

            <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="ks-icon la la-money"></span>
                            <span>Invoice</span>
                    </a>

                    <div class="dropdown-menu">
                            <a class="dropdown-item" href="<?php echo Url::to(['/invoice/invoice-listing']);?>">Package Invoice</a>
                            <a class="dropdown-item" href="<?php echo Url::to(['/invoice/index']);?>">Pay as you go Invoice</a>
                            <a class="dropdown-item" href="<?php echo Url::to(['/packages/subscription']);?>">Subscriptions</a>
                    </div>
            </li>

            <?php 
                }
            } elseif (array_key_exists(User::FRANCHISE_ROLE,$roles) && Yii::$app->user->can('manageInvoices')) { 
                $parent = User::findOne(['user_group' => Yii::$app->user->identity->user_group]);
                if(empty($parent->parent_id)) {
            ?>

            <li class="nav-item">
                                    <a class="nav-link"  href="<?php echo Url::to(['/invoice/invoice-listing']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                                            <span class="ks-icon la la-cubes"></span>
                                            <span>Invoice</span>
                                    </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="ks-icon la la-money"></span>
                        <span>Billing</span>
                </a>

                <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?php echo Url::to(['/packages/subscription']);?>">Subscriptions</a>
                        <a class="dropdown-item" href="<?php echo Url::to(['/invoice/index']);?>">Invoices</a>
                </div>
            </li>

            <?php
                }
             } 
             ?>   

            <?php if (!Yii::$app->user->can(User::SUBADMIN_ROLE) && Yii::$app->user->can('manageOutlets')) { 
                $parent = User::findOne(['user_group' => Yii::$app->user->identity->user_group]);
            ?> 
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-tasks"></span>
                    <span>Manage Outlets</span>
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?php echo Url::to(['/user/outlet-index', 'id' => $parent->id]);?>">Outlet Listing</a>
                    <a class="dropdown-item" href="<?php echo Url::to(['/user/outlet-create', 'id' => $parent->id]);?>">Create Outlet</a>
                </div>
            </li>
            <?php } ?>   

           
            <li class="nav-item">
              <a class="nav-link"  href="<?php echo Url::to(['/settings/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                  <span class="ks-icon la la-wrench"></span>
                  <span>Settings</span>
              </a>
            </li>           


        </ul>



<?php
}

	public function run(){
		return Html::encode($this->html);
	}
}
?>
