<?php
namespace common\widgets;

use frontend\assets\AppAsset;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\models\User;
use app\models\Permissions;
use yii\helpers\Url;

class Menu2Old extends Widget{

public $html;
	
public function init(){
		parent::init();
		$model = Permissions::findOne(1);
		$this->html = ''; ?>
<ul class="nav nav-pills nav-stacked" id="side-menu">
	<li>
		<a href="<?= Yii::$app->homeUrl;?>">
			<i class="fa fa-home fa-fw"></i><span>Dashboard</span>
		</a>
	</li>
	<?php if($model->is_lead == '1'){?>
	<li>
		<a href="<?= Yii::$app->homeUrl;?>leads/index">
			<i class="fa fa-globe fa-fw"></i><span>Leads</span>
		</a>
	</li>
	<?php }?>
	<?php if($model->is_chat == '1'){?>
	<li>
		<a class="dropdown-toggle" href="#">
			<i class="fa fa-comments fa-fw"></i><span>Chats</span><i class="fa fa-angle-right drop-icon"></i>
		</a>
		<ul class="submenu">
			<li>
				<a href="<?= Yii::$app->homeUrl;?>chat/index">History</a>
			</li>
			<!--li>
				<a href="<?= Url::to(['chat/total']); ?>">Total Chats</a>
			</li-->
		</ul>
	</li>
	<?php }?>
	<?php if($model->is_report == '1'){?>
	<li>
		<a href="#" class="dropdown-toggle">
			<i class="fa fa-table fa-fw"></i><span>Reports</span><i class="fa fa-angle-right drop-icon"></i>
		</a>
		<ul class="submenu">
			<li>
				<a href="<?= Yii::$app->homeUrl;?>reports/stats">Total Chats</a>
			</li>
			<li>
				<a href="<?= Yii::$app->homeUrl;?>leads/total">Total Leads</a>
			</li>
			<li>
				<a href="<?= Url::to(['goals/index']); ?>">Goals</a>
			</li>
			<!--<li>
				<a href="<?= Url::to(['reports/satisfy']); ?>">Chat Satisfaction</a>
			</li>
			<li>
				<a href="<?= Url::to(['reports/queued']); ?>">Queued Visitors</a>
			</li>
			<li>
				<a href="<?= Url::to(['chat/availability']); ?>">Availability</a>
			</li>-->
		</ul>
	</li>
	<?php }?>
	<?php if($model->is_faq == '1'){?>
	<li>
		<a href="<?= Yii::$app->homeUrl;?>faq/index">
			<i class="fa fa-question-circle"></i><span>Faq</span></a>
	</li>
	<?php }?>
	<?php if($model->is_package == '1'){?>
	<li>
		<a href="<?= Yii::$app->homeUrl;?>package/index">
			<i class="fa fa-cubes"></i><span>Packages</span></a>
	</li>
	<?php }?>
	<?php if($model->is_pay_as_you_go == '1'){?>
	<li>
		<a href="<?= Yii::$app->homeUrl;?>payasyougo/list">
			<i class="fa fa-money"></i><span>Pay as you go</span></a>
	</li>
	<?php }?>
	<?php if($model->is_email == '1'){?>
	<li>
		<a href="<?= Yii::$app->homeUrl;?>email/index">
			<i class="fa fa-envelope"></i><span>Email</span></a>
	</li>
	<?php }?>
	
	<li>
		<a href="<?= Yii::$app->homeUrl;?>feedback/index">
			<i class="fa fa-comment"></i><span>Feedback</span></a>
	</li>
</ul>

<?php
}	

	public function run(){
		return Html::encode($this->html);
	}
}
?>
