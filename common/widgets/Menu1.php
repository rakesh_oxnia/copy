<?php
namespace common\widgets;

use frontend\assets\AppAsset;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class Menu1 extends Widget{

public $html;

public function init(){
		parent::init();
		$this->html = ''; ?>

		<ul class="nav nav-pills nav-stacked">

            <li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/site/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-dashboard"></span>
                    <span>Dashboard</span>
                </a>
            </li>


			<!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-users"></span>
                    <span>Manage Agency</span>
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?php echo Url::to(['user/indexagency']);?>">Agency Listing</a>
					<a class="dropdown-item" href="<?php echo Url::to(['user/createagency']);?>">Create Agency</a>
                </div>
            </li> -->

			<li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-users"></span>
                    <span>Manage Client</span>
                </a>
                <div class="dropdown-menu">
<!--                    <a class="dropdown-item" href="--><?php //echo Url::to(['/user/index']);?><!--">Client Listing</a>-->
                    <a class="dropdown-item" href="<?php echo Url::to(['/user/client-franchise-index']);?>">Client Listing</a>
					<a class="dropdown-item" href="<?php echo Url::to(['/user/create']);?>">Create Client</a>
                    <a class="dropdown-item" href="<?php echo Url::to(['/user/franchise-create']);?>">Create Franchise</a>
                </div>
            </li>

<!--			<li class="nav-item dropdown">-->
<!--        <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">-->
<!--            <span class="ks-icon la la-tty"></span>-->
<!--            <span>Manage Franchise</span>-->
<!--        </a>-->
<!--        <div class="dropdown-menu">-->
<!--          <a class="dropdown-item" href="--><?php //echo Url::to(['/user/franchise-index']);?><!--">Franchise Listing</a>-->
<!--					<a class="dropdown-item" href="--><?php //echo Url::to(['/user/franchise-create']);?><!--">Create Franchise</a>-->
<!--        </div>-->
<!--      </li>-->
            <!--
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
					<span class="ks-icon la la-tasks"></span>
					<span>Manage Categories</span>
				</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="<?php echo Url::to(['/user-category/index']);?>">Category Listing</a>
					<a class="dropdown-item" href="<?php echo Url::to(['/user-category/create']);?>">Create Category</a>
				</div>
			</li> -->

            <!--<li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-question-circle"></span>
                    <span>FAQ</span>
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?php echo Url::to(['/faq/index']);?>">List</a>
					<a class="dropdown-item" href="<?php echo Url::to(['/faq/add']);?>">Add</a>
                </div>
            </li>-->

			<li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-comment"></span>
                    <span>Feedback</span>
                </a>
				<div class="dropdown-menu">
                    <a class="dropdown-item" href="<?php echo Url::to(['/feedback/index']);?>">List</a>
					<a class="dropdown-item" href="<?php echo Url::to(['/feedback/add']);?>">Add</a>
                </div>
            </li>

            <li class="nav-item">
            <a class="nav-link"  href="<?php echo Url::to(['/email-log/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
            <span class="ks-icon la la-database"></span>
            <span>Email Logs</span>
            </a>
            </li>

						<li class="nav-item dropdown">
			          <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
			              <span class="ks-icon la la-database"></span>
			              <span>Integration</span>
			          </a>
			          <div class="dropdown-menu">
			               <a class="dropdown-item" href="<?php echo Url::to(['/zapier-request-logs/index']);?>">Integration Request</a>
										 <a class="dropdown-item" href="<?php echo Url::to(['/zapier-logs/index']);?>">Integration Log</a>
			          </div>
			      </li>

						<li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/chat/unmark-lead']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-qrcode"></span>
                    <span>Unmark Lead</span>
                </a>
            </li>

						<li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/packages/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-cubes"></span>
                    <span>Packages</span>
                </a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-comment"></span>
                    <span>Invoice</span>
                </a>
								<div class="dropdown-menu">
                    <a class="dropdown-item" href="<?php echo Url::to(['/invoice/invoice-listing']);?>">Package invoice</a>
					<a class="dropdown-item" href="<?php echo Url::to(['/invoice/failed-listing']);?>">Failed Invoice</a>
										<a class="dropdown-item" href="<?php echo Url::to(['/invoice/index']);?>">Pay as you go Invoice</a>
                </div>
             </li>

			<!--<li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/payasyougo/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-money"></span>
                    <span>Pay as you go</span>
                </a>
            </li>-->

            <li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/chatstat/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-th"></span>
                    <span>Chat Stats Report</span>
                </a>
            </li>

			<li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/business-category/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-qrcode"></span>
                    <span>Business Category</span>
                </a>
            </li>

			<!--<li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/email/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-envelope"></span>
                    <span>Email</span>
                </a>
            </li>-->

			<li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/coupons/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-calculator"></span>
                    <span>Coupons</span>
                </a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-users"></span>
                    <span>Access Control</span>
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?php echo Url::to(['/user-access/index']);?>">Users</a>
                    <a class="dropdown-item" href="<?php echo Url::to(['/role/index']);?>">Roles</a>
                </div>
             </li>


			<!--<li class="nav-item">
                <a class="nav-link"  href="<?php //echo Url::to(['/permission/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-question-circle"></span>
                    <span>User Permissions</span>
                </a>
            </li>-->

        </ul>



<?php
}

	public function run(){
		return Html::encode($this->html);
	}
}
?>
