<?php
namespace common\widgets;

use frontend\assets\AppAsset;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\models\User;
use yii\helpers\Url;

class Menu3Old extends Widget{

public $html;
	
public function init(){
		parent::init();
		$this->html = ''; ?>
<ul class="nav nav-pills nav-stacked" id="side-menu">
	<li>
		<a href="<?= Yii::$app->homeUrl;?>">
			<i class="fa fa-home fa-fw"></i><span>Dashboard</span>
		</a>
	</li>
	<li>
		<a class="dropdown-toggle" href="#">
			<i class="fa fa-users fa-fw"></i> <span>Manage Client </span><i class="fa fa-angle-right drop-icon"></i>
		</a>
		<ul class="submenu">
			<li>
				<a href="<?= Yii::$app->homeUrl;?>user/index">Client Listing</a>
			</li>
			<li>
				<a href="<?= Yii::$app->homeUrl;?>user/create">Create Client</a>
			</li>
		</ul>
	</li>
	 
	 
</ul>

<?php
}	

	public function run(){
		return Html::encode($this->html);
	}
}
?>
