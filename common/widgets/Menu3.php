<?php
namespace common\widgets;

use frontend\assets\AppAsset;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\models\User;
use yii\helpers\Url;

class Menu3 extends Widget{

public $html;

public function init(){
		parent::init();
		$this->html = ''; ?>
<ul class="nav nav-pills nav-stacked" id="side-menu">
	 <li class="nav-item">
                <a class="nav-link"  href="<?php echo Url::to(['/site/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class="ks-icon la la-dashboard"></span>
                    <span>Dashboard</span>
                </a>
            </li>


	<li class="nav-item dropdown">
		<a class="nav-link dropdown-toggle"  href="#" role="button" aria-haspopup="true" aria-expanded="false">
			<span class="ks-icon la la-users"></span>
			<span>Manage Client</span>
		</a>
		<div class="dropdown-menu">
			<a class="dropdown-item" href="<?php echo Url::to(['/user/index']);?>">Client Listing</a>
			<a class="dropdown-item" href="<?php echo Url::to(['/user/create']);?>">Create Client</a>
		</div>
	</li>


</ul>

<?php
}

	public function run(){
		return Html::encode($this->html);
	}
}
?>
