<?php
namespace common\widgets;

use frontend\assets\AppAsset;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class Menu1Old extends Widget{

public $html;	
	
public function init(){
		parent::init();
		$this->html = ''; ?>
<ul class="nav nav-pills nav-stacked" id="side-menu">
	<li>
		<a href="<?= Yii::$app->homeUrl;?>">
			<i class="fa fa-home fa-fw"></i><span>Dashboard</span>
		</a>
	</li>
	<li>
		<a class="dropdown-toggle" href="#">
			<i class="fa fa-users fa-fw"></i> <span>Manage Agency </span><i class="fa fa-angle-right drop-icon"></i>
		</a>
		<ul class="submenu">
			<li>
				<a href="<?= Yii::$app->homeUrl;?>user/indexagency">Agency Listing</a>
			</li>
			<li>
				<a href="<?= Yii::$app->homeUrl;?>user/createagency">Create Agency</a>
			</li>
		</ul>
	</li>
	<li>
		<a class="dropdown-toggle" href="#">
			<i class="fa fa-users fa-fw"></i> <span>Manage Client </span><i class="fa fa-angle-right drop-icon"></i>
		</a>
		<ul class="submenu">
			<li>
				<a href="<?= Yii::$app->homeUrl;?>user/index">Client Listing</a>
			</li>
			<li>
				<a href="<?= Yii::$app->homeUrl;?>user/create">Create Client</a>
			</li>
		</ul>
	</li>
	<li>
		<a href="<?= Yii::$app->homeUrl;?>faq/index">
			<i class="fa fa-question-circle"></i><span>Faq</span></a>
	</li>
	<li>
		<a href="<?= Yii::$app->homeUrl;?>packages/index">
			<i class="fa fa-cubes"></i><span>Packages</span></a>
	</li>
	<li>
		<a href="<?= Yii::$app->homeUrl;?>business-category/index">
			<i class="fa fa-qrcode"></i><span>Business Category</span></a>
	</li>
	<li>
		<a href="<?= Yii::$app->homeUrl;?>coupons/index">
			<i class="fa fa-calculator"></i><span>Coupons</span></a>
	</li>
	<!--<li>
		<a href="<?= Yii::$app->homeUrl;?>payasyougo/index">
			<i class="fa fa-money"></i><span>Pay as you go</span></a>
	</li>
	<li>
		<a href="<?= Yii::$app->homeUrl;?>email/index">
			<i class="fa fa-envelope"></i><span>Email</span></a>
	</li>
	<li>
		<a href="<?= Yii::$app->homeUrl;?>permission/index">
			<i class="fa fa-question-circle"></i><span>User Permissions</span></a>
	</li>-->
	<li>
		<a href="<?= Yii::$app->homeUrl;?>feedback/index">
			<i class="fa fa-comment"></i><span>Feedback</span></a>
	</li>
	
</ul>

<?php
}	

	public function run(){
		return Html::encode($this->html);
	}
}
?>
