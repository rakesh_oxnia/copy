<?php
namespace common\widgets;

use frontend\assets\AppAsset;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\models\User;
use app\models\Permissions;
use yii\helpers\Url;

class ServiceMenu extends Widget{

public $html;

public function init(){
		parent::init();
		$model = Permissions::findOne(1);
		$this->html = ''; ?>
 <ul class="nav nav-pills nav-stacked">

			<li class="nav-item">
          <a class="nav-link"  href="<?php echo Url::to(['/lead-list/service-chats']);?>" role="button" aria-haspopup="true" aria-expanded="false">
              <span class="ks-icon la la-commenting"></span>
              <span>Service chats</span>
          </a>
      </li>
			<!--<li class="nav-item">-->
			<!--		<a class="nav-link"  href="<?php //echo Url::to(['/service-chat-email/index']);?>" role="button" aria-haspopup="true" aria-expanded="false">-->
			<!--			<span class="ks-icon la la-briefcase"></span>-->
			<!--			<span>Service Chats Emails</span>-->
			<!--		</a>-->
		 <!--</li>-->

  </ul>

<?php
}

	public function run(){
		return Html::encode($this->html);
	}
}
?>
